set(ECMASCRIPT_RUNTIME_YAML ${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/ecma_runtime.yaml)

set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/js_locale.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/js_date_time_format.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/js_relative_time_format.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/js_number_format.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/global_env.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/ecma_vm.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/mem/verification.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/mem/semi_space_collector.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/snapshot/mem/snapshot_serialize.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_string.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_locale.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_intl.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_number_format.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_number.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_relative_time_format.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_date.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)
set_source_files_properties(${PANDA_ECMASCRIPT_PLUGIN_SOURCE}/runtime/builtins/builtins_date_time_format.cpp PROPERTIES COMPILE_FLAGS -Wno-shadow)