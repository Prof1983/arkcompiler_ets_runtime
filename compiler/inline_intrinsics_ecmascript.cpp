/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "compiler/optimizer/optimizations/inline_intrinsics.h"
#include "optimizer/analysis/loop_analyzer.h"
#include "compiler/optimizer/ir/analysis.h"
#include "runtime/include/coretypes/tagged_value.h"
#include "irtoc_ir_inline.h"
#include "plugins/ecmascript/runtime/runtime_call_id.h"

namespace panda::compiler {
template <typename T>
bool InlineLdConstant(IntrinsicInst *intrinsic, AnyBaseType any_type, T value)
{
    auto current_block = intrinsic->GetBasicBlock();
    auto graph = current_block->GetGraph();
    auto cnst = graph->FindOrCreateConstant(value);

    auto cast_to_any_inst = graph->CreateInstCastValueToAnyType(intrinsic->GetPc(), any_type, cnst);
    current_block->InsertAfter(cast_to_any_inst, intrinsic);

    intrinsic->ReplaceUsers(cast_to_any_inst);
    current_block->RemoveInst(intrinsic);
    return true;
}

bool InlineLdConstant(IntrinsicInst *intrinsic, DataType::Any value)
{
    auto current_block = intrinsic->GetBasicBlock();
    auto graph = current_block->GetGraph();
    auto cnst = graph->FindOrCreateConstant(value);

    intrinsic->ReplaceUsers(cnst);

    current_block->RemoveInst(intrinsic);
    return true;
}

using TaggedValue = panda::coretypes::TaggedValue;

bool InlineIntrinsics::InlineLdTrue(IntrinsicInst *intrinsic)
{
    return InlineLdConstant(intrinsic, AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, 1U);
}

bool InlineIntrinsics::InlineLdFalse(IntrinsicInst *intrinsic)
{
    return InlineLdConstant(intrinsic, AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, 0U);
}

bool InlineIntrinsics::InlineLdHole(IntrinsicInst *intrinsic)
{
    return InlineLdConstant(intrinsic, DataType::Any(TaggedValue::VALUE_HOLE));
}

bool InlineIntrinsics::InlineLdNull(IntrinsicInst *intrinsic)
{
    return InlineLdConstant(intrinsic, DataType::Any(TaggedValue::VALUE_NULL));
}

bool InlineIntrinsics::InlineLdUndefined(IntrinsicInst *intrinsic)
{
    return InlineLdConstant(intrinsic, DataType::Any(TaggedValue::VALUE_UNDEFINED));
}

bool InlineIntrinsics::InlineLdInfinity(IntrinsicInst *intrinsic)
{
    return InlineLdConstant(intrinsic, AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, TaggedValue::VALUE_INFINITY);
}

bool InlineIntrinsics::InlineLdNan(IntrinsicInst *intrinsic)
{
    return InlineLdConstant(intrinsic, AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, TaggedValue::VALUE_NAN);
}

bool InlineIntrinsics::InlineTypeOf(IntrinsicInst *intrinsic)
{
    auto any_type = intrinsic->GetInput(0).GetInst();
    if (any_type->GetOpcode() != Opcode::AnyTypeCheck) {
        return false;
    }
    auto type = any_type->CastToAnyTypeCheck()->GetAnyType();

    if (type == AnyBaseType::UNDEFINED_TYPE) {
        return false;
    }
    auto any_name = GetGraph()->CreateInstGetAnyTypeName(intrinsic->GetPc(), type);
    intrinsic->InsertAfter(any_name);
    intrinsic->ReplaceUsers(any_name);
    return true;
}

void InlineIntrinsics::CreateCompareClass(uint32_t pc, Inst *get_cls_inst, RuntimeInterface::ClassPtr receiver,
                                          BasicBlock *load_bb)
{
    auto load_cls_inst = GetGraph()->CreateInstLoadImmediate(DataType::REFERENCE, pc, receiver);
    auto cmp_inst = GetGraph()->CreateInstCompare(DataType::BOOL, pc, load_cls_inst, get_cls_inst, DataType::REFERENCE,
                                                  ConditionCode::CC_EQ);
    auto if_inst = GetGraph()->CreateInstIfImm(DataType::BOOL, pc, cmp_inst, 0, DataType::BOOL, ConditionCode::CC_NE);
    // We insert LoadImmediate in Dominate blockm because VN can be applied
    get_cls_inst->InsertAfter(load_cls_inst);
    load_bb->AppendInst(cmp_inst);
    load_bb->AppendInst(if_inst);
}

Inst *InlineIntrinsics::CreateCompareClassWithDeopt(uint32_t pc, Inst *get_cls_inst,
                                                    RuntimeInterface::ClassPtr receiver, Inst *save_state)
{
    auto load_cls_inst = GetGraph()->CreateInstLoadImmediate(DataType::REFERENCE, pc, receiver);
    auto cmp_inst = GetGraph()->CreateInstCompare(DataType::BOOL, pc, get_cls_inst, load_cls_inst, DataType::REFERENCE,
                                                  ConditionCode::CC_NE);
    auto deopt_inst =
        GetGraph()->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, cmp_inst, save_state, DeoptimizeType::INLINE_IC);
    get_cls_inst->InsertAfter(deopt_inst);
    get_cls_inst->InsertAfter(cmp_inst);
    get_cls_inst->InsertAfter(load_cls_inst);
    return deopt_inst;
}

Inst *InsertLoadObject(Inst *input, uint32_t type_id, RuntimeInterface::MethodPtr method, uint32_t pc,
                       RuntimeInterface::FieldPtr field, ObjectType obj_type, Inst *insert_after,
                       DataType::Type data_type)
{
    auto load_object =
        input->GetBasicBlock()->GetGraph()->CreateInstLoadObject(data_type, pc, input, type_id, method, field);
    load_object->SetObjectType(obj_type);
    insert_after->InsertAfter(load_object);
    return load_object;
}

void InsertCheckJSArrayLength(Inst *obj, Inst *index, Inst *insert_after, RuntimeInterface::MethodPtr method,
                              uint32_t pc)
{
    auto curr_bb = insert_after->GetBasicBlock();
    auto graph = curr_bb->GetGraph();
    // Insert Load Object length field for JsArray object
    auto length = InsertLoadObject(obj, TypeIdMixin::MEM_DYN_ARRAY_LENGTH_ID, method, pc, nullptr,
                                   ObjectType::MEM_DYN_ARRAY_LENGTH, insert_after, DataType::INT32);

    auto cmp_inst = graph->CreateInstCompare(DataType::BOOL, pc, index, length, DataType::INT32, ConditionCode::CC_LT);
    auto if_inst = graph->CreateInstIfImm(DataType::BOOL, pc, cmp_inst, 0, DataType::BOOL, ConditionCode::CC_NE);
    length->InsertAfter(cmp_inst);
    cmp_inst->InsertAfter(if_inst);
    // Split block by call instruction
    auto curr_cont_bb = curr_bb->SplitBlockAfterInstruction(if_inst, true);
    auto store_bb = graph->CreateEmptyBlock(curr_bb);
    curr_bb->GetLoop()->AppendBlock(store_bb);
    curr_bb->AddSucc(store_bb);
    store_bb->AddSucc(curr_cont_bb);
    // calculate new length
    auto add = graph->CreateInstAdd(DataType::INT32, pc, index, graph->FindOrCreateConstant(1));
    // Store new length
    auto store_length = graph->CreateInstStoreObject(DataType::INT32, pc, obj, add,
                                                     TypeIdMixin::MEM_DYN_ARRAY_LENGTH_ID, method, nullptr);
    store_length->SetObjectType(ObjectType::MEM_DYN_ARRAY_LENGTH);
    store_bb->AppendInst(add);
    add->InsertAfter(store_length);
}

template <bool IS_LOAD>
Inst *InsertMemFromFieldInlined(IntrinsicInst *intrinsic, Inst *obj_inst, Inst *insert_after, uint32_t offset)
{
    auto graph = obj_inst->GetBasicBlock()->GetGraph();
    auto save_state = intrinsic->GetSaveState();
    auto pc = obj_inst->GetPc();
    if constexpr (IS_LOAD) {
        auto load_object = InsertLoadObject(obj_inst, offset, save_state->GetMethod(), pc, nullptr,
                                            ObjectType::MEM_DYN_INLINED, insert_after, DataType::ANY);
        return load_object;
    } else {
        auto store_object = graph->CreateInstStoreObject(DataType::ANY, pc, obj_inst, intrinsic->GetInput(2).GetInst(),
                                                         offset, save_state->GetMethod(), nullptr, false, true);
        store_object->SetObjectType(ObjectType::MEM_DYN_INLINED);
        insert_after->InsertAfter(store_object);

        return store_object;
    }
}

Inst *InsertBoundsCheck(Inst *array, Inst *index, Inst *save_state, uint32_t pc, Inst *insert_after)
{
    auto graph = array->GetBasicBlock()->GetGraph();
    auto array_length = graph->CreateInstLenArray(DataType::INT32, pc, array);

    // Create BoundCheck instruction
    auto bounds_check = graph->CreateInstBoundsCheck(DataType::INT32, pc, array_length, index, save_state);
    bounds_check->SetFlag(inst_flags::CAN_DEOPTIMIZE);
    insert_after->InsertAfter(bounds_check);
    insert_after->InsertAfter(array_length);

    return bounds_check;
}

template <bool IS_LOAD, bool INSERT_BOUNDS_CHECK>
Inst *InsertMemFromField(IntrinsicInst *intrinsic, Inst *obj_inst, Inst *insert_after, uint32_t offset)
{
    auto graph = obj_inst->GetBasicBlock()->GetGraph();
    auto save_state = intrinsic->GetSaveState();
    auto pc = obj_inst->GetPc();
    auto load_object = InsertLoadObject(obj_inst, TypeIdMixin::MEM_DYN_PROPS_ID, save_state->GetMethod(), pc, nullptr,
                                        ObjectType::MEM_DYN_PROPS, insert_after, DataType::REFERENCE);
    insert_after = load_object;
    Inst *index = graph->FindOrCreateConstant(offset);
    if constexpr (INSERT_BOUNDS_CHECK) {
        auto bounds_check = InsertBoundsCheck(load_object, index, save_state, pc, insert_after);
        insert_after = bounds_check;

        index = bounds_check;
    }

    Inst *array = nullptr;
    if constexpr (IS_LOAD) {
        auto load_array = graph->CreateInstLoadArray(DataType::ANY, pc, load_object, index);
        array = load_array;
    } else {
        auto store_array =
            graph->CreateInstStoreArray(DataType::ANY, pc, load_object, index, intrinsic->GetInput(2).GetInst(), true);
        array = store_array;
    }
    insert_after->InsertAfter(array);

    return array;
}

Inst *InsertChangeClassInst(IntrinsicInst *intrinsic, Inst *obj_inst, Inst *insert_after, uintptr_t klass_addr)
{
    auto graph = obj_inst->GetBasicBlock()->GetGraph();
    auto pc = obj_inst->GetPc();
    auto klass = reinterpret_cast<RuntimeInterface::ClassPtr>(klass_addr);
    auto load_cls_inst = graph->CreateInstLoadImmediate(DataType::REFERENCE, pc, klass);
    auto store_class =
        graph->CreateInstStoreObject(DataType::REFERENCE, pc, obj_inst, load_cls_inst, TypeIdMixin::MEM_DYN_CLASS_ID,
                                     intrinsic->GetSaveState()->GetMethod(), nullptr, false, true);
    store_class->SetObjectType(ObjectType::MEM_DYN_CLASS);
    insert_after->InsertAfter(store_class);
    insert_after->InsertAfter(load_cls_inst);
    return store_class;
}

Inst *InsertPrototypeCheckInst(IntrinsicInst *intrinsic, uintptr_t proto_mem, Inst **insert_after)
{
    auto graph = intrinsic->GetBasicBlock()->GetGraph();
    auto save_state = intrinsic->GetSaveState();
    auto pc = intrinsic->GetPc();
    // Get Prototype handler
    auto proto_handler = graph->CreateInstLoadObjFromConst(DataType::REFERENCE, pc, proto_mem);
    (*insert_after)->InsertAfter(proto_handler);

    // Get Prototype object
    auto prototype = InsertLoadObject(proto_handler, TypeIdMixin::MEM_DYN_PROTO_HOLDER_ID, save_state->GetMethod(), pc,
                                      nullptr, ObjectType::MEM_DYN_PROTO_HOLDER, proto_handler, DataType::REFERENCE);

    // Get Prototype change marker
    auto prototype_marker =
        InsertLoadObject(proto_handler, TypeIdMixin::MEM_DYN_PROTO_CELL_ID, save_state->GetMethod(), pc, nullptr,
                         ObjectType::MEM_DYN_PROTO_CELL, prototype, DataType::REFERENCE);

    // Get change field from the marker
    auto is_change = InsertLoadObject(prototype_marker, TypeIdMixin::MEM_DYN_CHANGE_FIELD_ID, save_state->GetMethod(),
                                      pc, nullptr, ObjectType::MEM_DYN_CHANGE_FIELD, prototype_marker, DataType::BOOL);

    auto deopt_inst =
        graph->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, is_change, save_state, DeoptimizeType::INLINE_IC);

    is_change->InsertAfter(deopt_inst);

    *insert_after = deopt_inst;
    return prototype;
}

template <bool IS_LOAD>
Inst *InsertAccessInst(IntrinsicInst *intrinsic, Inst *obj_inst, Inst *insert_after,
                       RuntimeInterface::NamedAccessProfileData profile)
{
    uint32_t offset = profile.offset;
    RuntimeInterface::NamedAccessProfileType type = profile.type;
    switch (type) {
        case RuntimeInterface::NamedAccessProfileType::FIELD_INLINED:
            return InsertMemFromFieldInlined<IS_LOAD>(intrinsic, obj_inst, insert_after, offset);
        case RuntimeInterface::NamedAccessProfileType::FIELD:
            return InsertMemFromField<IS_LOAD, false>(intrinsic, obj_inst, insert_after, offset);
        case RuntimeInterface::NamedAccessProfileType::TRANSITION_INLINED: {
            ASSERT(!IS_LOAD);
            auto store_class = InsertChangeClassInst(intrinsic, obj_inst, insert_after, profile.cached_value);
            return InsertMemFromFieldInlined<IS_LOAD>(intrinsic, obj_inst, store_class, offset);
        }
        case RuntimeInterface::NamedAccessProfileType::TRANSITION: {
            ASSERT(!IS_LOAD);
            auto store = InsertMemFromField<IS_LOAD, true>(intrinsic, obj_inst, insert_after, offset);
            InsertChangeClassInst(intrinsic, obj_inst, store->GetPrev(), profile.cached_value);
            return store;
        }
        case RuntimeInterface::NamedAccessProfileType::PROTOTYPE_INLINED: {
            auto load_proto = InsertPrototypeCheckInst(intrinsic, profile.cached_value, &insert_after);
            return InsertMemFromFieldInlined<IS_LOAD>(intrinsic, load_proto, insert_after, offset);
        }
        case RuntimeInterface::NamedAccessProfileType::PROTOTYPE: {
            auto load_proto = InsertPrototypeCheckInst(intrinsic, profile.cached_value, &insert_after);
            return InsertMemFromField<IS_LOAD, false>(intrinsic, load_proto, insert_after, offset);
        }
        default:
            UNREACHABLE();
    }
}

void InsertDeoptimizeInst(uint32_t pc, SaveStateInst *ss, BasicBlock *load_bb)
{
    ASSERT(load_bb != nullptr);
    // NOLINTNEXTLINE(clang-analyzer-core.CallAndMessage)
    auto graph = load_bb->GetGraph();
    ASSERT(graph != nullptr);

    // If last class compare returns false we need to deoptimize the method.
    // So we construct instruction DeoptimizeIf and insert instead of IfImm inst.
    auto if_inst = load_bb->GetLastInst();
    ASSERT(if_inst != nullptr && if_inst->GetOpcode() == Opcode::IfImm);
    ASSERT(if_inst->CastToIfImm()->GetImm() == 0 && if_inst->CastToIfImm()->GetCc() == ConditionCode::CC_NE);

    auto compare_inst = if_inst->GetInput(0).GetInst()->CastToCompare();
    ASSERT(compare_inst != nullptr && compare_inst->GetCc() == ConditionCode::CC_EQ);
    compare_inst->SetCc(ConditionCode::CC_NE);

    auto deopt_inst = graph->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, compare_inst, ss, DeoptimizeType::INLINE_IC);

    load_bb->RemoveInst(if_inst);
    load_bb->AppendInst(deopt_inst);
}

bool InlineIntrinsics::InlineLdObjByName(IntrinsicInst *intrinsic)
{
    return InlineObjByName<true>(intrinsic);
}

bool InlineIntrinsics::InlineStObjByName(IntrinsicInst *intrinsic)
{
    return InlineObjByName<false>(intrinsic);
}

bool InlineIntrinsics::InlineLdObjByValue(IntrinsicInst *intrinsic)
{
    return InlineObjByValue<true>(intrinsic);
}

bool InlineIntrinsics::InlineStObjByValue(IntrinsicInst *intrinsic)
{
    return InlineObjByValue<false>(intrinsic);
}

bool InlineIntrinsics::GetICForMemNamedAccess(IntrinsicInst *intrinsic)
{
    auto pc = intrinsic->GetPc();
    auto runtime = GetGraph()->GetRuntime();
    auto save_state = intrinsic->GetSaveState();
    ASSERT(save_state != nullptr);
    auto caller_inst = save_state->GetCallerInst();
    if (caller_inst != nullptr) {
        ASSERT(caller_inst->IsInlined());
        auto func = caller_inst->GetFunctionObject();
        if (func != 0) {
            return runtime->GetProfileDataForNamedAccess(GetGraph()->GetMethod(), func, pc, &named_access_profile_);
        }
        return runtime->GetProfileDataForNamedAccess(caller_inst->GetCallMethod(), pc, &named_access_profile_);
    }
    return runtime->GetProfileDataForNamedAccess(GetGraph()->GetMethod(), pc, &named_access_profile_);
}

bool InlineIntrinsics::GetICForMemValueAccess(IntrinsicInst *intrinsic)
{
    auto pc = intrinsic->GetPc();
    auto runtime = GetGraph()->GetRuntime();
    auto save_state = intrinsic->GetSaveState();
    ASSERT(save_state != nullptr);
    auto caller_inst = save_state->GetCallerInst();
    if (caller_inst != nullptr) {
        ASSERT(caller_inst->IsInlined());
        auto func = caller_inst->GetFunctionObject();
        if (func != 0) {
            return runtime->GetProfileDataForValueAccess(GetGraph()->GetMethod(), func, pc, &named_access_profile_);
        }
        return runtime->GetProfileDataForValueAccess(caller_inst->GetCallMethod(), pc, &named_access_profile_);
    }
    return runtime->GetProfileDataForValueAccess(GetGraph()->GetMethod(), pc, &named_access_profile_);
}

template <bool BY_NAME>
Inst *InlineIntrinsics::InsertCheckAndCastInstructions(IntrinsicInst *intrinsic)
{
    auto pc = intrinsic->GetPc();
    auto save_state = intrinsic->GetSaveState();
    auto obj_inst = intrinsic->GetInput(BY_NAME ? 1 : 0).GetInst();

    auto any_check = GetGraph()->CreateInstAnyTypeCheck(DataType::ANY, pc, obj_inst, save_state,
                                                        AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE);
    intrinsic->InsertBefore(any_check);

    auto cast_value = GetGraph()->CreateInstCastAnyTypeValue(pc, any_check, AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE);
    // NOLINTNEXTLINE(readability-magic-numbers)
    cast_value->SetFlag(inst_flags::NO_HOIST);
    any_check->InsertAfter(cast_value);

    return cast_value;
}

template <bool IS_LOAD>
void InlineIntrinsics::InlineObjByNameMonomorphic(IntrinsicInst *intrinsic, Inst *get_cls_inst, Inst *obj_inst)
{
    auto pc = intrinsic->GetPc();
    auto save_state = intrinsic->GetSaveState();
    auto prof = named_access_profile_[0];
    auto deopt = CreateCompareClassWithDeopt(pc, get_cls_inst, prof.klass, save_state);
    Inst *obj = obj_inst;
    if (prof.type == RuntimeInterface::NamedAccessProfileType::PROTOTYPE ||
        prof.type == RuntimeInterface::NamedAccessProfileType::PROTOTYPE_INLINED) {
        obj = get_cls_inst;
    }
    [[maybe_unused]] auto load_inst = InsertAccessInst<IS_LOAD>(intrinsic, obj, deopt, prof);

    if constexpr (IS_LOAD) {
        intrinsic->ReplaceUsers(load_inst);
    }

    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
}

template <bool IS_LOAD>
void InlineIntrinsics::InlineObjByNamePolymorphic(IntrinsicInst *intrinsic, Inst *get_cls_inst, Inst *obj_inst)
{
    auto pc = intrinsic->GetPc();
    auto save_state = intrinsic->GetSaveState();
    PhiInst *phi_inst = nullptr;
    BasicBlock *load_bb = nullptr;
    BasicBlock *load_cont_bb = nullptr;
    Inst *load_inst = intrinsic;
    for (auto &prof : named_access_profile_) {
        if (load_bb == nullptr) {
            // Split block by call instruction
            load_bb = load_inst->GetBasicBlock();
            load_cont_bb = load_bb->SplitBlockAfterInstruction(load_inst, false);
            if constexpr (IS_LOAD) {
                phi_inst = GetGraph()->CreateInstPhi(load_inst->GetType(), load_inst->GetPc());
                phi_inst->ReserveInputs(named_access_profile_.size());
                load_cont_bb->AppendPhi(phi_inst);
            }
        } else {
            auto new_load_bb = GetGraph()->CreateEmptyBlock(load_bb);
            load_bb->GetLoop()->AppendBlock(new_load_bb);
            load_bb->AddSucc(new_load_bb);
            load_bb = new_load_bb;
        }

        CreateCompareClass(pc, get_cls_inst, prof.klass, load_bb);
        // Create load_inserting_block
        auto load_inserting_block = GetGraph()->CreateEmptyBlock(load_bb);
        load_bb->GetLoop()->AppendBlock(load_inserting_block);
        load_bb->AddSucc(load_inserting_block);
        auto last_ss = CopySaveState(GetGraph(), save_state);
        load_inserting_block->PrependInst(last_ss);
        Inst *obj = obj_inst;
        if (prof.type == RuntimeInterface::NamedAccessProfileType::PROTOTYPE ||
            prof.type == RuntimeInterface::NamedAccessProfileType::PROTOTYPE_INLINED) {
            obj = get_cls_inst;
        }

        [[maybe_unused]] auto load = InsertAccessInst<IS_LOAD>(intrinsic, obj, last_ss, prof);

        if constexpr (IS_LOAD) {
            phi_inst->AppendInput(load);
        }

        load_inserting_block->AddSucc(load_cont_bb);
    }

    InsertDeoptimizeInst(pc, save_state, load_bb);

    if constexpr (IS_LOAD) {
        intrinsic->ReplaceUsers(phi_inst);
    }

    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
}

void InsertCheckKeyInstructions(IntrinsicInst *intrinsic, uintptr_t key_mem)
{
    auto graph = intrinsic->GetBasicBlock()->GetGraph();
    auto pc = intrinsic->GetPc();
    auto save_state = intrinsic->GetSaveState();
    auto key_ic = graph->CreateInstLoadObjFromConst(DataType::ANY, pc, key_mem);
    auto cmp_inst = graph->CreateInstCompare(DataType::BOOL, pc, key_ic, intrinsic->GetInput(1).GetInst(),
                                             DataType::ANY, ConditionCode::CC_NE);
    auto deopt_inst =
        graph->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, cmp_inst, save_state, DeoptimizeType::INLINE_IC);
    intrinsic->InsertBefore(key_ic);
    intrinsic->InsertBefore(cmp_inst);
    intrinsic->InsertBefore(deopt_inst);
}

template <bool IS_LOAD>
void InlineIntrinsics::InlineObjByValueWithKey(IntrinsicInst *intrinsic)
{
    auto pc = intrinsic->GetPc();
    auto save_state = intrinsic->GetSaveState();
    auto prof = named_access_profile_[0];
    InsertCheckKeyInstructions(intrinsic, prof.key);
    auto cast_value = InsertCheckAndCastInstructions<false>(intrinsic);
    auto get_cls_inst =
        InsertLoadObject(cast_value, TypeIdMixin::MEM_DYN_CLASS_ID, intrinsic->GetSaveState()->GetMethod(), pc, nullptr,
                         ObjectType::MEM_DYN_CLASS, cast_value, DataType::REFERENCE);

    auto deopt = CreateCompareClassWithDeopt(pc, get_cls_inst, prof.klass, save_state);
    Inst *obj = cast_value;
    if (prof.type == RuntimeInterface::NamedAccessProfileType::PROTOTYPE ||
        prof.type == RuntimeInterface::NamedAccessProfileType::PROTOTYPE_INLINED) {
        obj = get_cls_inst;
    }
    [[maybe_unused]] auto load_inst = InsertAccessInst<IS_LOAD>(intrinsic, obj, deopt, prof);

    if constexpr (IS_LOAD) {
        intrinsic->ReplaceUsers(load_inst);
    }

    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
}

template <bool IS_LOAD>
void InlineIntrinsics::InlineObjByValueFromElements(IntrinsicInst *intrinsic)
{
    auto pc = intrinsic->GetPc();
    auto save_state = intrinsic->GetSaveState();

    // Check that input is heap object
    auto cast_value = InsertCheckAndCastInstructions<false>(intrinsic);

    // Insert Load class for the object
    auto get_cls_inst = InsertLoadObject(cast_value, TypeIdMixin::MEM_DYN_CLASS_ID, save_state->GetMethod(), pc,
                                         nullptr, ObjectType::MEM_DYN_CLASS, cast_value, DataType::REFERENCE);

    // check objects classes with first class from IC
    auto load_cls_inst = GetGraph()->CreateInstLoadImmediate(DataType::REFERENCE, pc, named_access_profile_[0].klass);
    auto cmp_inst = GetGraph()->CreateInstCompare(DataType::BOOL, pc, get_cls_inst, load_cls_inst, DataType::REFERENCE,
                                                  ConditionCode::CC_NE);
    get_cls_inst->InsertAfter(load_cls_inst);
    load_cls_inst->InsertAfter(cmp_inst);
    Inst *deopt_input = cmp_inst;
    if (named_access_profile_.size() == 2) {
        // check objects classes with second class from IC
        auto load_cls_inst_1 =
            GetGraph()->CreateInstLoadImmediate(DataType::REFERENCE, pc, named_access_profile_[1].klass);
        auto cmp_inst_1 = GetGraph()->CreateInstCompare(DataType::BOOL, pc, get_cls_inst, load_cls_inst_1,
                                                        DataType::REFERENCE, ConditionCode::CC_NE);
        auto and_inst = GetGraph()->CreateInstAnd(DataType::BOOL, pc, cmp_inst, cmp_inst_1);
        deopt_input = and_inst;
        cmp_inst->InsertAfter(load_cls_inst_1);
        load_cls_inst_1->InsertAfter(cmp_inst_1);
        cmp_inst_1->InsertAfter(and_inst);
    }
    // Insert deoptimize
    auto deopt_inst =
        GetGraph()->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, deopt_input, save_state, DeoptimizeType::INLINE_IC);
    deopt_input->InsertAfter(deopt_inst);

    auto key = intrinsic->GetInput(1).GetInst();
    auto any_check =
        GetGraph()->CreateInstAnyTypeCheck(DataType::ANY, pc, key, save_state, AnyBaseType::ECMASCRIPT_INT_TYPE);
    intrinsic->InsertBefore(any_check);

    auto cast_value_int = GetGraph()->CreateInstCastAnyTypeValue(pc, any_check, AnyBaseType::ECMASCRIPT_INT_TYPE);
    // NOLINTNEXTLINE(readability-magic-numbers)
    cast_value_int->SetFlag(inst_flags::NO_HOIST);
    any_check->InsertAfter(cast_value_int);

    // Insert Load Object for Elements array
    auto load_object = InsertLoadObject(cast_value, TypeIdMixin::MEM_DYN_ELEMENTS_ID, save_state->GetMethod(), pc,
                                        nullptr, ObjectType::MEM_DYN_ELEMENTS, cast_value_int, DataType::REFERENCE);

    // Insert LenArray and BoundChecks
    auto bounds_check = InsertBoundsCheck(load_object, cast_value_int, save_state, pc, load_object);

    if constexpr (IS_LOAD) {
        auto load_array = GetGraph()->CreateInstLoadArray(DataType::ANY, pc, load_object, bounds_check);

        intrinsic->ReplaceUsers(load_array);
        bounds_check->InsertAfter(load_array);
        auto cmp_hole_inst = GetGraph()->CreateInstCompare(
            DataType::BOOL, pc, load_array,
            GetGraph()->FindOrCreateConstant(DataType::Any(panda::coretypes::TaggedValue::VALUE_HOLE)), DataType::ANY,
            ConditionCode::CC_EQ);
        load_array->InsertAfter(cmp_hole_inst);

        auto deopt_hole_inst =
            GetGraph()->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, cmp_hole_inst, save_state, DeoptimizeType::HOLE);
        cmp_hole_inst->InsertAfter(deopt_hole_inst);
    } else {
        auto store_array = GetGraph()->CreateInstStoreArray(DataType::ANY, pc, load_object, bounds_check,
                                                            intrinsic->GetInput(2).GetInst(), true);

        bounds_check->InsertAfter(store_array);
        if (named_access_profile_[0].type == RuntimeInterface::NamedAccessProfileType::ARRAY_ELEMENT) {
            InsertCheckJSArrayLength(cast_value, cast_value_int, bounds_check, save_state->GetMethod(), pc);
        }
    }
    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
}

template <bool IS_LOAD>
bool InlineIntrinsics::InlineObjByValue(IntrinsicInst *intrinsic)
{
    if (GetGraph()->IsAotMode()) {
        return false;
    }
    ASSERT(intrinsic->GetSaveState() != nullptr);
    if (!GetICForMemValueAccess(intrinsic)) {
        return false;
    }
    ASSERT(!named_access_profile_.empty());
    if (named_access_profile_[0].key == 0) {
        // we need to fix length for ARRAY_ELEMENT
        if (named_access_profile_.size() == 2 && named_access_profile_[0].type != named_access_profile_[1].type) {
            return false;
        }
        InlineObjByValueFromElements<IS_LOAD>(intrinsic);
        return true;
    }

    InlineObjByValueWithKey<IS_LOAD>(intrinsic);
    return false;
}

template <bool IS_LOAD>
bool InlineIntrinsics::InlineObjByName(IntrinsicInst *intrinsic)
{
    if (GetGraph()->IsAotMode()) {
        return false;
    }
    auto pc = intrinsic->GetPc();
    ASSERT(intrinsic->GetSaveState() != nullptr);
    if (!GetICForMemNamedAccess(intrinsic)) {
        return false;
    }
    ASSERT(!named_access_profile_.empty());

    auto cast_value = InsertCheckAndCastInstructions<true>(intrinsic);
    auto get_cls_inst =
        InsertLoadObject(cast_value, TypeIdMixin::MEM_DYN_CLASS_ID, intrinsic->GetSaveState()->GetMethod(), pc, nullptr,
                         ObjectType::MEM_DYN_CLASS, cast_value, DataType::REFERENCE);

    if (named_access_profile_.size() == 1) {
        InlineObjByNameMonomorphic<IS_LOAD>(intrinsic, get_cls_inst, cast_value);
        return true;
    }

    InlineObjByNamePolymorphic<IS_LOAD>(intrinsic, get_cls_inst, cast_value);
    return true;
}

bool InlineIntrinsics::InlineResolveAllocResult(IntrinsicInst *intrinsic)
{
    auto ctor_res = intrinsic->GetInput(2U).GetInst();
    if (ctor_res->GetOpcode() != Opcode::CastValueToAnyType) {
        return false;
    }
    if (ctor_res->CastToCastValueToAnyType()->GetAnyType() != AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE) {
        return false;
    }
    auto alloc_inst = intrinsic->GetInput(1U).GetInst();
    intrinsic->ReplaceUsers(alloc_inst);

    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
    return true;
}

profiling::AnyInputType InlineIntrinsics::GetAllowedTypeForInput(IntrinsicInst *intrinsic, size_t index)
{
    auto input = intrinsic->GetInput(index).GetInst();
    if (input->GetOpcode() == Opcode::AnyTypeCheck) {
        return input->CastToAnyTypeCheck()->GetAllowedInputType();
    }
    return profiling::AnyInputType::DEFAULT;
}

bool InlineIntrinsics::InlineStrictCompareDifferentTypes(IntrinsicInst *intrinsic, AnyBaseType type1, AnyBaseType type2)
{
    ASSERT(intrinsic->GetIntrinsicId() == RuntimeInterface::IntrinsicId::INTRINSIC_STRICT_EQ_DYN ||
           intrinsic->GetIntrinsicId() == RuntimeInterface::IntrinsicId::INTRINSIC_STRICT_NOT_EQ_DYN);
    if ((type1 == AnyBaseType::ECMASCRIPT_INT_TYPE || type1 == AnyBaseType::ECMASCRIPT_DOUBLE_TYPE) &&
        (type2 == AnyBaseType::ECMASCRIPT_INT_TYPE || type2 == AnyBaseType::ECMASCRIPT_DOUBLE_TYPE)) {
        return false;
    }

    auto input1 = intrinsic->GetInput(0).GetInst();
    auto input2 = intrinsic->GetInput(0).GetInst();
    auto allowed_type1 = input1->GetOpcode() == Opcode::AnyTypeCheck
                             ? input1->CastToAnyTypeCheck()->GetAllowedInputType()
                             : profiling::AnyInputType::DEFAULT;
    auto allowed_type2 = input2->GetOpcode() == Opcode::AnyTypeCheck
                             ? input1->CastToAnyTypeCheck()->GetAllowedInputType()
                             : profiling::AnyInputType::DEFAULT;

    if (IsAnyTypeCanBeSubtypeOf(SourceLanguage::ECMASCRIPT, type1, type2, allowed_type1, allowed_type2) == false) {
        auto result = intrinsic->GetIntrinsicId() == RuntimeInterface::IntrinsicId::INTRINSIC_STRICT_EQ_DYN ? 0U : 1U;
        return InlineLdConstant(intrinsic, AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, result);
    }
    return false;
}

bool InlineIntrinsics::InlineStrictEqDyn(IntrinsicInst *intrinsic, AnyBaseType type1, AnyBaseType type2)
{
    return InlineStrictCompareDifferentTypes(intrinsic, type1, type2) ||
           IrtocInlineCompareStrictEqDyn(intrinsic, type1, type2);
}

bool InlineIntrinsics::InlineStrictNotEqDyn(IntrinsicInst *intrinsic, AnyBaseType type1, AnyBaseType type2)
{
    return InlineStrictCompareDifferentTypes(intrinsic, type1, type2) ||
           IrtocInlineCompareStrictNotEqDyn(intrinsic, type1, type2);
}

bool InlineIntrinsics::InlineCompareWithNull(IntrinsicInst *intrinsic, AnyBaseType type1, AnyBaseType type2)
{
    ASSERT(intrinsic->GetIntrinsicId() == RuntimeInterface::IntrinsicId::INTRINSIC_EQ_DYN ||
           intrinsic->GetIntrinsicId() == RuntimeInterface::IntrinsicId::INTRINSIC_NOT_EQ_DYN);
    if (type1 == AnyBaseType::UNDEFINED_TYPE || type2 == AnyBaseType::UNDEFINED_TYPE) {
        return false;
    }
    bool first_null = (type1 == AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE || type1 == AnyBaseType::ECMASCRIPT_NULL_TYPE);
    bool second_null = (type2 == AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE || type2 == AnyBaseType::ECMASCRIPT_NULL_TYPE);
    auto eq_result = intrinsic->GetIntrinsicId() == RuntimeInterface::IntrinsicId::INTRINSIC_EQ_DYN ? 1U : 0U;
    if (first_null && second_null) {
        return InlineLdConstant(intrinsic, AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, eq_result);
    }
    Inst *other_input;
    if (first_null) {
        other_input = intrinsic->GetInput(1).GetInst();
    } else if (second_null) {
        other_input = intrinsic->GetInput(0).GetInst();
    } else {
        return false;
    }
    if (other_input->GetOpcode() == Opcode::AnyTypeCheck && other_input->CastToAnyTypeCheck()->IsSpecialWasSeen()) {
        return false;
    }
    return InlineLdConstant(intrinsic, AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, 1 - eq_result);
}

bool InlineIntrinsics::InlineEqDyn(IntrinsicInst *intrinsic, AnyBaseType type1, AnyBaseType type2)
{
    return InlineCompareWithNull(intrinsic, type1, type2) || IrtocInlineCompareEqDyn(intrinsic, type1, type2);
}

bool InlineIntrinsics::InlineNotEqDyn(IntrinsicInst *intrinsic, AnyBaseType type1, AnyBaseType type2)
{
    return InlineCompareWithNull(intrinsic, type1, type2) || IrtocInlineCompareNeDyn(intrinsic, type1, type2);
}

bool InlineIntrinsics::InlineToNumber(IntrinsicInst *intrinsic, AnyBaseType type1)
{
    auto special_allowed = (GetAllowedTypeForInput(intrinsic, 0) & profiling::AnyInputType::SPECIAL) != 0;
    return panda::compiler::IrtocInlineToNumber(intrinsic, type1, special_allowed);
}

bool InlineIntrinsics::InlineLdLexDyn(IntrinsicInst *intrinsic)
{
    auto *save_state = intrinsic->GetSaveState();
    ASSERT(save_state != nullptr);

    // level, slot
    ASSERT(intrinsic->GetImms().size() == 2U);
    // Name, LexEnv, SaveState
    ASSERT(intrinsic->GetInputsCount() == 3U);
    auto *load_lex_var = intrinsic;
    load_lex_var->SetIntrinsicId(RuntimeInterface::IntrinsicId::INTRINSIC_LD_LEX_VAR_DYN);
    AdjustFlags(RuntimeInterface::IntrinsicId::INTRINSIC_LD_LEX_VAR_DYN, load_lex_var);
    ASSERT(load_lex_var->GetInput(2U).GetInst()->GetOpcode() == Opcode::SaveState);
    load_lex_var->RemoveInput(2U);
    load_lex_var->RemoveInput(0U);
    load_lex_var->ClearFlag(inst_flags::REQUIRE_STATE);
    InstAppender appender(load_lex_var->GetBasicBlock(), load_lex_var);

    auto pc = intrinsic->GetPc();
    auto *hole = GetGraph()->FindOrCreateConstant(DataType::Any(panda::coretypes::TaggedValue::VALUE_HOLE));
    auto *compare =
        GetGraph()->CreateInstCompare(DataType::BOOL, pc, load_lex_var, hole, DataType::ANY, ConditionCode::CC_EQ);
    appender.Append(compare);

    auto *deopt_if =
        GetGraph()->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, compare, save_state, DeoptimizeType::HOLE);
    appender.Append(deopt_if);

    InlineLdLexVarDyn(load_lex_var);
    return true;
}

Inst *InlineIntrinsics::GetParentLexEnv(InstAppender *appender, Inst *current_lex_env, uint32_t level, uint32_t pc)
{
    auto *runtime = GetGraph()->GetRuntime();
    for (; level > 0U; --level) {
        auto *load_parent =
            GetGraph()->CreateInstLoadArray(DataType::ANY, pc, current_lex_env,
                                            GetGraph()->FindOrCreateConstant(runtime->GetLexicalEnvParentEnvIndex()));
        appender->Append(load_parent);

        auto *parent_ref = GetGraph()->CreateInstCastAnyTypeValue(pc, load_parent, AnyBaseType::ECMASCRIPT_ARRAY_TYPE);
        parent_ref->SetFlag(inst_flags::NO_HOIST);
        appender->Append(parent_ref);
        current_lex_env = parent_ref;
    }
    return current_lex_env;
}

bool InlineIntrinsics::InlineStLexDyn(IntrinsicInst *intrinsic)
{
    ASSERT(intrinsic->GetImms().size() == 2U);
    // Name, Acc, LexEnv, SaveState
    ASSERT(intrinsic->GetInputsCount() == 4U);

    auto level = intrinsic->GetImms()[0U];
    if (level > GetLdStLexVarDynLevelThreshold()) {
        return false;
    }

    auto slot = intrinsic->GetImms()[1U];

    auto *store_acc_val = intrinsic->GetInput(1U).GetInst();
    auto *store_lex_env = intrinsic->GetInput(2U).GetInst();

    InstAppender appender(intrinsic->GetBasicBlock(), intrinsic);

    auto pc = intrinsic->GetPc();
    auto *env_ref = GetGraph()->CreateInstCastAnyTypeValue(pc, store_lex_env, AnyBaseType::ECMASCRIPT_ARRAY_TYPE);
    env_ref->SetFlag(inst_flags::NO_HOIST);
    appender.Append(env_ref);

    Inst *current_lex_env = env_ref;
    current_lex_env = GetParentLexEnv(&appender, current_lex_env, level, pc);

    auto start = GetGraph()->GetRuntime()->GetLexicalEnvStartDataIndex();
    auto elem_offset = start + slot;

    auto save_state = intrinsic->GetSaveState();
    ASSERT(save_state != nullptr);

    auto *get_prop = GetGraph()->CreateInstLoadArray(DataType::ANY, pc, current_lex_env,
                                                     GetGraph()->FindOrCreateConstant(elem_offset));
    appender.Append(get_prop);

    auto *hole = GetGraph()->FindOrCreateConstant(DataType::Any(panda::coretypes::TaggedValue::VALUE_HOLE));
    auto *compare =
        GetGraph()->CreateInstCompare(DataType::BOOL, pc, get_prop, hole, DataType::ANY, ConditionCode::CC_EQ);
    appender.Append(compare);

    auto *deopt_if =
        GetGraph()->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, compare, save_state, DeoptimizeType::HOLE);
    appender.Append(deopt_if);

    auto *store_lex_var = GetGraph()->CreateInstStoreArray(
        DataType::ANY, pc, current_lex_env, GetGraph()->FindOrCreateConstant(elem_offset), store_acc_val, true);
    appender.Append(store_lex_var);

    if (intrinsic->HasUsers()) {
        intrinsic->ReplaceUsers(store_acc_val);
    }
    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
    return true;
}

bool InlineIntrinsics::InlineLdLexVarDyn(IntrinsicInst *intrinsic)
{
    auto level = intrinsic->GetImms()[0U];
    if (level > GetLdStLexVarDynLevelThreshold()) {
        return false;
    }

    auto slot = intrinsic->GetImms()[1U];

    auto *load_lex_env = intrinsic->GetInput(0U).GetInst();

    InstAppender appender(intrinsic->GetBasicBlock(), intrinsic);

    auto pc = intrinsic->GetPc();
    auto *env_ref = GetGraph()->CreateInstCastAnyTypeValue(pc, load_lex_env, AnyBaseType::ECMASCRIPT_ARRAY_TYPE);
    env_ref->SetFlag(inst_flags::NO_HOIST);
    appender.Append(env_ref);

    Inst *current_lex_env = env_ref;
    current_lex_env = GetParentLexEnv(&appender, current_lex_env, level, pc);

    auto start = GetGraph()->GetRuntime()->GetLexicalEnvStartDataIndex();
    auto elem_offset = start + slot;
    auto *load_lex_var = GetGraph()->CreateInstLoadArray(DataType::ANY, pc, current_lex_env,
                                                         GetGraph()->FindOrCreateConstant(elem_offset));
    appender.Append(load_lex_var);

    intrinsic->ReplaceUsers(load_lex_var);
    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
    return true;
}

bool InlineIntrinsics::InlineLdlexenvDyn([[maybe_unused]] IntrinsicInst *intrinsic)
{
    auto pc = intrinsic->GetPc();
    auto load_lexical_env = GetGraph()->CreateInstLoadLexicalEnv(DataType::ANY, pc, intrinsic->GetInput(0).GetInst());
    intrinsic->InsertBefore(load_lexical_env);
    intrinsic->ReplaceUsers(load_lexical_env);
    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
    return true;
}

bool InlineIntrinsics::InlineStLexVarDyn(IntrinsicInst *intrinsic)
{
    auto level = intrinsic->GetImms()[0U];
    if (level > GetLdStLexVarDynLevelThreshold()) {
        return false;
    }

    auto slot = intrinsic->GetImms()[1U];

    auto *store_acc_val = intrinsic->GetInput(0U).GetInst();
    auto *store_lex_env = intrinsic->GetInput(1U).GetInst();

    InstAppender appender(intrinsic->GetBasicBlock(), intrinsic);

    auto pc = intrinsic->GetPc();
    auto *env_ref = GetGraph()->CreateInstCastAnyTypeValue(pc, store_lex_env, AnyBaseType::ECMASCRIPT_ARRAY_TYPE);
    env_ref->SetFlag(inst_flags::NO_HOIST);
    appender.Append(env_ref);

    Inst *current_lex_env = env_ref;
    current_lex_env = GetParentLexEnv(&appender, current_lex_env, level, pc);

    auto start = GetGraph()->GetRuntime()->GetLexicalEnvStartDataIndex();
    auto elem_offset = start + slot;

    auto *store_lex_var = GetGraph()->CreateInstStoreArray(
        DataType::ANY, pc, current_lex_env, GetGraph()->FindOrCreateConstant(elem_offset), store_acc_val, true);
    appender.Append(store_lex_var);

    if (intrinsic->HasUsers()) {
        intrinsic->ReplaceUsers(store_acc_val);
    }
    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
    return true;
}

bool InlineIntrinsics::InlinePopLexenvDyn(IntrinsicInst *intrinsic)
{
    InstAppender appender(intrinsic->GetBasicBlock(), intrinsic);

    auto pc = intrinsic->GetPc();
    auto *env_ref = GetGraph()->CreateInstCastAnyTypeValue(pc, intrinsic->GetInput(0).GetInst(),
                                                           AnyBaseType::ECMASCRIPT_ARRAY_TYPE);
    env_ref->SetFlag(inst_flags::NO_HOIST);
    appender.Append(env_ref);

    auto *load_parent = GetGraph()->CreateInstLoadArray(
        DataType::ANY, pc, env_ref,
        GetGraph()->FindOrCreateConstant(GetGraph()->GetRuntime()->GetLexicalEnvParentEnvIndex()));
    appender.Append(load_parent);

    intrinsic->ReplaceUsers(load_parent);
    intrinsic->GetBasicBlock()->RemoveInst(intrinsic);
    return true;
}

bool InlineIntrinsics::InlineTryLdGlobalByName(IntrinsicInst *inst)
{
    if (GetGraph()->IsAotMode()) {
        return false;
    }
    ASSERT(inst->GetSaveState() != nullptr);
    auto runtime = GetGraph()->GetRuntime();
    auto load_from_cp = inst->GetInput(0).GetInst();
    auto type_id = load_from_cp->CastToLoadFromConstantPool()->GetTypeId();
    auto pc = inst->GetPc();
    auto save_state = inst->GetSaveState();
    ASSERT(save_state != nullptr);
    auto caller_inst = save_state->GetCallerInst();
    RuntimeInterface::GlobalVarInfo inline_info;
    if (caller_inst != nullptr) {
        ASSERT(caller_inst->IsInlined());
        auto func = caller_inst->GetFunctionObject();
        if (func != 0) {
            inline_info = runtime->GetGlobalVarInfo(GetGraph()->GetMethod(), func, type_id, pc);
        } else {
            inline_info = runtime->GetGlobalVarInfo(caller_inst->GetCallMethod(), type_id, pc);
        }
    } else {
        inline_info = runtime->GetGlobalVarInfo(GetGraph()->GetMethod(), type_id, pc);
    }
    switch (inline_info.type) {
        case RuntimeInterface::GlobalVarInfo::Type::CONSTANT: {
            auto constant = GetGraph()->FindOrCreateConstant(DataType::Any(inline_info.value));
            inst->ReplaceUsers(constant);
            inst->GetBasicBlock()->RemoveInst(inst);
            return true;
        }
        case RuntimeInterface::GlobalVarInfo::Type::NON_CONFIGURABLE:
            InlineTryLdGlobalField<false>(inst, type_id, static_cast<uintptr_t>(inline_info.value));
            return true;
        case RuntimeInterface::GlobalVarInfo::Type::FIELD:
            InlineTryLdGlobalField<true>(inst, type_id, static_cast<uintptr_t>(inline_info.value));
            return true;
        default:
            return false;
    }
}

template <bool NEED_GUARD>
void InlineIntrinsics::InlineTryLdGlobalField(IntrinsicInst *inst, uint32_t type_id, uintptr_t address)
{
    auto pc = inst->GetPc();
    auto save_state = inst->GetSaveState();

    auto get_address = GetGraph()->CreateInstGetGlobalVarAddress(DataType::REFERENCE, pc, inst->GetInput(2).GetInst(),
                                                                 save_state, type_id, GetGraph()->GetMethod(), address);
    inst->InsertBefore(get_address);

    auto load_object = GetGraph()->CreateInstLoadObject(DataType::ANY, pc, get_address, TypeIdMixin::MEM_DYN_GLOBAL_ID,
                                                        GetGraph()->GetMethod(), nullptr);
    load_object->SetObjectType(ObjectType::MEM_DYN_GLOBAL);
    inst->InsertBefore(load_object);

    if constexpr (NEED_GUARD) {
        auto cmp = GetGraph()->CreateInstCompareAnyType(pc, load_object, AnyBaseType::ECMASCRIPT_HOLE_TYPE);
        auto deopt = GetGraph()->CreateInstDeoptimizeIf(DataType::BOOL, pc, cmp, save_state, DeoptimizeType::INLINE_IC);
        inst->InsertBefore(cmp);
        inst->InsertBefore(deopt);
    }

    inst->ReplaceUsers(load_object);
    inst->GetBasicBlock()->RemoveInst(inst);
}

}  // namespace panda::compiler

#include "plugins/ecmascript/runtime/builtins/generated/builtins_inline_intrinsics_gen.inl"
