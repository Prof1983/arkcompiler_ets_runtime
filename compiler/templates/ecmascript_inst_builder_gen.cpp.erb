/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "compiler_logger.h"
#include "optimizer/ir_builder/inst_builder.h"
#include "optimizer/ir_builder/ir_builder.h"
#include "optimizer/ir/inst.h"
#include "bytecode_instruction.h"
#include "bytecode_instruction-inl.h"
#include "include/coretypes/tagged_value.h"

#include "irtoc_builder.cpp"

namespace panda::compiler {

// Currently we support two strategies for building IR from ecma.* instructions:
//   1) Each ecma.* instruction is translated to a corresponding intrinsic call.
//      This is used for bytecode optimizer and slow paths during compiling
//      to native code (in both JIT and AOT modes).
//   2) Semantics of each ecma.* instruction is taken from the corresponding
//      IRtoC handler and is inlined into compiled function.
//      This is used only for native compilation (again, both JIT and AOT).
// InstBuilder::BuildEcma selects proper strategy and calls relevant builder.

// NOLINTNEXTLINE(readability-function-size)
void InstBuilder::BuildEcma([[maybe_unused]] const BytecodeInstruction* bc_inst)
{
#ifdef ENABLE_BYTECODE_OPT
    switch (bc_inst->GetOpcode()) {
% Panda::instructions.select{|b| b.namespace == "ecmascript"}.each do |inst|
        case BytecodeInstruction::Opcode::<%= inst.opcode.upcase %>: {
%   if inst.compilable? && inst.inlinable?
            // +compilable, +inlinable: ecma.* -> intrinsics for BCO, inline IRtoC otherwise:
            if (GetGraph()->IsBytecodeOptimizer()) {
                BuildEcmaAsIntrinsics(bc_inst);
            } else {
                if (OPTIONS.IsCompilerInlineFullIntrinsics()) {
                    BuildEcmaFromIrtoc(bc_inst);
                } else {
                    BuildEcmaAsIntrinsics<true>(bc_inst);
                }
            }
%   elsif inst.compilable?
            // +compilable, -inlinable: ecma.* -> intrinsics for all scenarios:
            BuildEcmaAsIntrinsics(bc_inst);
%   else
%      abort "isa.yaml inconsistency: #{inst.opcode.upcase} is not compilable, but inlinable" if inst.inlinable?
            // -compilable, -inlinable: ecma.* -> intrinsics for BCO, fail IR builder otherwise:
            if (GetGraph()->IsBytecodeOptimizer()) {
                BuildEcmaAsIntrinsics(bc_inst);
            } else {
                failed_ = true;
            }
%   end
            break;
        }
% end
        default: {
            failed_ = true;
            LOG(ERROR, COMPILER) << "Unknown ecma.* opcode: " << static_cast<int>(bc_inst->GetOpcode());
            return;
        }
    }
#endif
}

template <bool WITH_SPECULATIVE>
void InstBuilder::BuildEcmaAsIntrinsics(const BytecodeInstruction* bc_inst) // NOLINT(readability-function-size)
{
    switch (bc_inst->GetOpcode()) {
% Panda::instructions.select{|b| b.namespace == "ecmascript"}.each do |inst|
%   opc = inst.opcode.upcase
%   name = opc.split('_')[1]
%     acc_read = inst.acc.include?("in")
%     acc_write = inst.acc.include?("out")
%     ret_type = acc_write ? "compiler::DataType::ANY" : "compiler::DataType::VOID"
%     iname = inst.intrinsic_name ? inst.intrinsic_name : opc
        case BytecodeInstruction::Opcode::<%= opc %>: {
            auto intrinsic_id = <%= "compiler::RuntimeInterface::IntrinsicId::" + iname %>;
            auto inst = GetGraph()->CreateInstIntrinsic(<%= ret_type %>, GetPc(bc_inst->GetAddress()), intrinsic_id);
%     if inst.throwing?
            inst->SetFlag(inst_flags::CAN_THROW);
%     end
%     if inst.exceptions.include?('x_throw') || inst.properties.include?('return')
            inst->SetFlag(inst_flags::CF);
            inst->SetFlag(inst_flags::TERMINATOR);
%     end
%     has_profile = inst.profiled?
%     if has_profile
            [[maybe_unused]] auto profile = GetRuntime()->GetBytecodeProfile(method_profile_, bc_inst->GetAddress(), inst->GetPc());
%     end
%     params_arr = inst.operands
%     format = "BytecodeInstruction::Format::" + inst.format.pretty.upcase
%     range_should_exclude_last = name == "NEWOBJDYNRANGE" || name == "SUPERCALL" || name == "CREATEOBJECTWITHEXCLUDEDKEYS"
%     is_range_call = name == "CALLIRANGEDYN" || name == "CALLITHISRANGEDYN" || range_should_exclude_last
%     need_newtarget = name == "SUPERCALLSPREAD" || name == "SUPERCALL"
%     num_vregs = params_arr.select{|b| b.reg?}.length
%     num_imms = params_arr.select{|b| b.imm?}.length
%     num_ids = params_arr.select{|b| b.id?}.length
%
%     use_this_func = inst.properties.include? 'func'
%     use_cp = inst.properties.include? 'cp'
%     use_lex_env = inst.properties.include? 'lex_env'
%     write_lex_env = inst.properties.include? 'write_lex_env'
%
%     num_inputs = acc_read ? num_vregs + 2 : num_vregs + 1
%     num_inputs += use_this_func ? 1 : 0
%     num_inputs += use_cp ? 1 : 0
%     num_inputs += use_lex_env ? 1 : 0
%     use_ic = inst.properties.include? 'use_ic'
%     if range_should_exclude_last
%       num_inputs = num_inputs - 1
%     end
%     if need_newtarget
%       num_inputs = num_inputs + 1
%     end
%     if is_range_call
            size_t args_count = <%= num_inputs %>U + static_cast<size_t>(bc_inst->GetImm<<%= format %>, 0>());
%     else
            size_t args_count {<%= num_inputs %>U};
%     end
            if (!inst->RequireState()) {
                --args_count;
            }
%     if need_newtarget
            if (GetGraph()->IsBytecodeOptimizer()) {
                --args_count;
            }
%     end
%     if use_ic
%       num_inputs = num_inputs + 1
            if (!GetGraph()->IsBytecodeOptimizer()) {
              args_count++;  // Instruction supports IC: +1 input for IC slot index
            }
%     end
            inst->ReserveInputs(args_count);
            inst->AllocateInputTypes(GetGraph()->GetAllocator(), args_count);
            inst->SetInlined(true);

%     imm_index = 0
%     vreg_index = 0
%     id16_index = 0
%     id32_index = 0
            auto inst_save_state = CreateSaveState(Opcode::SaveState, GetPc(bc_inst->GetAddress()));
%     if num_ids > 0
%           abort "Too many ids in inputs: #{opc}" if num_ids > 1
            LoadFromPoolDynamic *load_from_pool = nullptr;
            if (!GetGraph()->IsBytecodeOptimizer()) {
                load_from_pool = graph_->CreateInstLoadFromConstantPool(DataType::ANY, inst->GetPc());
                load_from_pool->SetInput(0, GetEnvDefinition(CONST_POOL_IDX));
                inst_save_state->AppendBridge(load_from_pool);
                AddInstruction(load_from_pool);
                inst->AppendInput(load_from_pool);
                inst->AddInputType(DataType::ANY);
                inst->SetHasIdInput();
            }
%     end
            AddInstruction(inst_save_state);
%     params_arr.each_with_index do |param, input_index|
%       if param.imm?
            auto imm<%= imm_index %> = static_cast<uint32_t>(bc_inst->GetImm<<%= format %>, <%= imm_index %>>());
            inst->AddImm(GetGraph()->GetAllocator(), imm<%= imm_index %>);
%           imm_index = imm_index + 1
%       elsif param.reg?
            {
                auto input = GetDefinition(bc_inst->GetVReg<<%= format %>, <%= vreg_index %>>());
%         if has_profile && (inst.profile.properties.include?('operand_types_1') || inst.profile.properties.include?('operand_types_2'))
                // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
                if constexpr (WITH_SPECULATIVE) {
                    profiling::AnyInputType allowed_input_type {};
                    bool is_type_profiled = false;
                    auto operand_type = GetRuntime()->GetProfilingAnyType(profile, bc_inst, <%= input_index %>, &allowed_input_type, &is_type_profiled);
                    input = BuildAnyTypeCheckInst(GetPc(bc_inst->GetAddress()), input, inst_save_state, operand_type, is_type_profiled, allowed_input_type);
                    if (OPTIONS.IsCompilerEcmaReplaceIntrinsicsToDeopt() && !is_type_profiled) {
                        inst->SetReplaceOnDeoptimize();
                    }
                }
%         end
                inst->AppendInput(input);
                inst->AddInputType(DataType::ANY);
            }
%           vreg_index = vreg_index + 1
%       elsif param.id?
%         abort "Id is not the first input: #{opc}" if input_index > 0
%         if inst.properties.include?("method_id")
            auto m_idx<%= id16_index %> = bc_inst->template GetId<<%= format %>>().AsRawValue();
            if (GetGraph()->IsBytecodeOptimizer()) {
                m_idx<%= id16_index %> = GetRuntime()->ResolveMethodIndex(GetGraph()->GetMethod(), m_idx<%= id16_index %>);
                inst->AddImm(GetGraph()->GetAllocator(), m_idx<%= id16_index %>);
            } else {
                load_from_pool->SetTypeId(m_idx<%= id16_index %>);
            }
%           id16_index = id16_index + 1
%         elsif inst.properties.include?("literalarray_id")
            auto literalarray_id<%= id16_index %> = bc_inst->template GetId<<%= format %>>().AsIndex();
            if (GetGraph()->IsBytecodeOptimizer()) {
                inst->AddImm(GetGraph()->GetAllocator(), literalarray_id<%= id16_index %>);
            } else {
                load_from_pool->SetTypeId(literalarray_id<%= id16_index %>);
            }
%           id16_index = id16_index + 1
%         elsif inst.properties.include?("string_id")
            auto string_id<%= id32_index %> = bc_inst->template GetId<<%= format %>>().AsFileId().GetOffset();
            if (GetGraph()->IsBytecodeOptimizer()) {
                inst->AddImm(GetGraph()->GetAllocator(), string_id<%= id32_index %>);
            } else {
                load_from_pool->SetTypeId(string_id<%= id32_index %>);
            }
%           id32_index = id32_index + 1
%         end
%       end
%     end
%     if need_newtarget
            if (!GetGraph()->IsBytecodeOptimizer()) {
                auto input = GetDefinition(GetGraph()->GetRuntime()->GetMethodRegistersCount(GetMethod()) + 1);
                // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
                if constexpr (WITH_SPECULATIVE) {
                    input = BuildAnyTypeCheckInst(GetPc(bc_inst->GetAddress()), input, inst_save_state);
                }
                inst->AppendInput(input);
                inst->AddInputType(DataType::ANY);
            }
%     end
%     if is_range_call
%           range_reg_idx = name == "CREATEOBJECTWITHEXCLUDEDKEYS" ? 1 : 0
%           num_actual_vregs = range_should_exclude_last ? "imm0" : "imm0 + 1"
            size_t start_reg = bc_inst->GetVReg<<%= format %>, <%= range_reg_idx %>>();
            for (uint32_t i = 1; i < <%= num_actual_vregs %>; ++i) {
                auto input = GetDefinition(start_reg + i);
                // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
                if constexpr (WITH_SPECULATIVE) {
                    input = BuildAnyTypeCheckInst(GetPc(bc_inst->GetAddress()), input, inst_save_state);
                }
                inst->AppendInput(input);
                inst->AddInputType(DataType::ANY);
            }
%     end
%     if acc_read
            {
                auto input = GetDefinitionAcc();
%       if has_profile && (inst.profile.properties.include?('operand_types_1') || inst.profile.properties.include?('operand_types_2'))

                // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
                if constexpr (WITH_SPECULATIVE) {
                    profiling::AnyInputType allowed_input_type {};
                    bool is_type_profiled = false;
%         idx = inst.profile.properties.include?('operand_types_2') ? 1 : 0
                    auto operand_type = GetRuntime()->GetProfilingAnyType(profile, bc_inst, <%= idx %>, &allowed_input_type, &is_type_profiled);
                    input = BuildAnyTypeCheckInst(GetPc(bc_inst->GetAddress()), input, inst_save_state, operand_type, is_type_profiled, allowed_input_type);
                    if (OPTIONS.IsCompilerEcmaReplaceIntrinsicsToDeopt() && !is_type_profiled) {
                        inst->SetReplaceOnDeoptimize();
                    }
                }
%       end
                inst->AppendInput(input);
                inst->AddInputType(DataType::ANY);
                inst->SetFlag(compiler::inst_flags::ACC_READ);
            }
%     end
%     if use_ic
            if (!GetGraph()->IsBytecodeOptimizer()) {
              inst->AppendInput(FindOrCreate32BitConstant(GetPc(bc_inst->GetAddress())));
              inst->AddInputType(DataType::UINT16);
            }
%     end
%     if use_this_func
            if (!GetGraph()->IsBytecodeOptimizer())
            {
              inst->AppendInput(GetDefinition(vregs_and_args_count_ + 1 + THIS_FUNC_IDX));
              inst->AddInputType(DataType::ANY);
            }
%    end
%     if use_cp
            if (!GetGraph()->IsBytecodeOptimizer())
            {
              inst->AppendInput(GetEnvDefinition(CONST_POOL_IDX));
              inst->AddInputType(DataType::ANY);
            }
%     end
%     if use_lex_env
            if (!GetGraph()->IsBytecodeOptimizer())
            {
              inst->AppendInput(GetEnvDefinition(LEX_ENV_IDX));
              inst->AddInputType(DataType::ANY);
            }
%     end
            if (inst->RequireState()) {
                inst->AppendInput(inst_save_state);
                inst->AddInputType(DataType::NO_TYPE);
            }
%     if write_lex_env
            if (!GetGraph()->IsBytecodeOptimizer())
            {
                UpdateDefinitionLexEnv(inst);
            }
%     end
            AddInstruction(inst);
%     if acc_write
            UpdateDefinitionAcc(inst);
            inst->SetFlag(compiler::inst_flags::ACC_WRITE);
%     end
            break;
        }
% end
        default:
            failed_ = true;
            LOG(ERROR,COMPILER) << "unknown Ecma opcode!" << static_cast<int>(bc_inst->GetOpcode());
            return;
  }
}

// NOLINTNEXTLINE(readability-function-size)
void InstBuilder::BuildEcmaFromIrtoc([[maybe_unused]] const BytecodeInstruction* bc_inst)
{
#ifdef ENABLE_BYTECODE_OPT
    ASSERT(!GetGraph()->IsBytecodeOptimizer());  // Not applicable for optimizing bytecode
    switch (bc_inst->GetOpcode()) {
% Panda::instructions.select{|b| b.namespace == "ecmascript"}.each do |inst|
%   format = "BytecodeInstruction::Format::" + inst.format.pretty.upcase
%   vreg_index = 0
%   imm_index = 0
%   id_index = 0
%   inputs = []
%
%   # Corner case, no inlinable IRtoC handler found:
%   unless inst.inlinable?
        case BytecodeInstruction::Opcode::<%= inst.opcode.upcase %>: {
            UNREACHABLE();  // Inlinable IRtoC handler is not implemented yet
            break;
        }
%     next
%   end
%
%   # Generate code for inlining IRtoC handler:
        case BytecodeInstruction::Opcode::<%= inst.opcode.upcase %>: {
%   inst.operands.each do |param|
%     if param.reg?
            auto vreg<%= vreg_index %> = GetDefinition(bc_inst->GetVReg<<%= format %>, <%= vreg_index %>>());
%       inputs.push("vreg#{vreg_index}")
%       vreg_index = vreg_index + 1
%     elsif param.imm?
            auto imm<%= imm_index %>_payload = static_cast<uint32_t>(bc_inst->GetImm<<%= format %>, <%= imm_index %>>());
            auto imm<%= imm_index %> = GetGraph()->FindOrCreateConstant(imm<%= imm_index %>_payload);
%       inputs.push("imm#{imm_index}")
%       imm_index = imm_index + 1
%     elsif param.id?
            auto id<%= id_index %>_payload = static_cast<uint32_t>(bc_inst->template GetId<<%= format %>>().AsIndex());
            auto load_from_pool = graph_->CreateInstLoadFromConstantPool(DataType::ANY, GetPc(bc_inst->GetAddress()));
            load_from_pool->SetInput(0, GetEnvDefinition(CONST_POOL_IDX));
            load_from_pool->SetTypeId(id<%= id_index %>_payload);
            additional_def_ = load_from_pool;
            AddInstruction(load_from_pool);
%       inputs.push("load_from_pool")
%       id_index = id_index + 1
%     elsif param.prof?
%       # Just skip profile id
%     else
%       abort 'Unexpected param type'
%     end
%   end
%   if inst.acc.include?('in')
            auto acc = GetDefinitionAcc();  // According to ecma.* convention, always goes last
%     inputs.push('acc')
%   end
%   if inst.properties.include?('use_ic')
%     inputs.push('GetGraph()->FindOrCreateConstant(GetPc(bc_inst->GetAddress()))')
%   end
%   if inst.properties.include?('func')
%     inputs.push('GetEnvDefinition(THIS_FUNC_IDX)')
%   end
%   if inst.properties.include?('cp')
%     inputs.push('GetEnvDefinition(CONST_POOL_IDX)')
%   end
%   if inst.properties.include?('lex_env')
%     inputs.push('GetEnvDefinition(LEX_ENV_IDX)')
%   end

%   builder = 'Build' + inst.opcode.split('_')[0..1].map do |_| _.capitalize end.join()
            [[maybe_unused]] auto inst = <%= builder %>(GetGraph(), this, &current_bb_, <%= inputs.empty? ? '' : inputs.join(', ') + ', ' %>GetPc(bc_inst->GetAddress()), visited_block_marker_);
%   if id_index > 0
            additional_def_ = nullptr;
%   end
%   if inst.acc.include?('out')
            UpdateDefinitionAcc(inst);
%   end
            SyncWithGraph();
            break;
        }
% end
        default: {
            failed_ = true;
            LOG(ERROR, COMPILER) << "Unknown ecma.* opcode: " << static_cast<int>(bc_inst->GetOpcode());
            return;
        }
    }
#endif
}

}  // namespace panda::compiler
