/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_COMPILER_ECMASCRIPT_CALL_PARAMS
#define PANDA_COMPILER_ECMASCRIPT_CALL_PARAMS

#include "compiler/optimizer/code_generator/callconv.h"
#include "plugins/ecmascript/runtime/ecma_call_params.h"

// NOLINTNEXTLINE(modernize-concat-nested-namespaces)
namespace panda::compiler {

namespace ecmascript_call_params {
enum : uint8_t {
    SLOT_FUNCTION = CallConvDynInfo::SLOT_CALLEE,
    SLOT_NEWTARGET = ::panda::ecmascript::js_method_args::NEW_TARGET_IDX,
    SLOT_THIS = ::panda::ecmascript::js_method_args::THIS_IDX,
    SLOT_FIRST_ARG = ::panda::ecmascript::js_method_args::FIRST_ARG_IDX,
};
}  // namespace ecmascript_call_params
static_assert(static_cast<uint8_t>(ecmascript_call_params::SLOT_FUNCTION) ==
              static_cast<uint8_t>(::panda::ecmascript::js_method_args::FUNC_IDX));

}  // namespace panda::compiler

#endif  // PANDA_COMPILER_ECMASCRIPT_CALL_PARAMS