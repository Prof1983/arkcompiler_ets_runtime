/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_COMPILER_ECMASCRIPT_IRTOC_INTERFACE_H
#define PANDA_COMPILER_ECMASCRIPT_IRTOC_INTERFACE_H

size_t GetGlobalConstStringOffsetForAnyType(compiler::AnyBaseType type, Arch arch) const override
{
    auto global_const_array_offset =
        cross_values::GetJsthreadGlobalConstantsOffset(arch) + cross_values::GetGlobalConstConstantsOffset(arch);
    switch (type) {
        case compiler::AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::UNDEFINED_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_STRING_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::STRING_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_INT_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::NUMBER_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_DOUBLE_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::NUMBER_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_SYMBOL_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::SYMBOL_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_CALLABLE_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::FUNCTION_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::BOOLEAN_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_NULL_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::OBJECT_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_BIGINT_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::BIGINT_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::OBJECT_STRING_INDEX);
        default:
            UNREACHABLE();
    }
    return 0;
}

#endif  // PANDA_COMPILER_ECMASCRIPT_IRTOC_INTERFACE_H