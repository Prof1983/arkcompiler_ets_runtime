/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_COMPILER_THREAD_ENVIRONMENT_API_H
#define PANDA_COMPILER_THREAD_ENVIRONMENT_API_H

#include "ecmascript_environment.h"
#include "plugins/ecmascript/runtime/class_linker/program_object-inl.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"

namespace panda::ecmascript {

#ifdef PANDA_QEMU_AARCH64_GCC_8
// Build PANDA_QEMU_AARCH64_GCC_8 hangs without this workaround. Please check issue #8094.
JSTaggedValue GetGlobalObject(JSThread *thread);
#else
inline JSTaggedValue GetGlobalObject(JSThread *thread)
{
    return thread->GetGlobalObject();
}
#endif

inline JSHandle<panda::ecmascript::GlobalEnv> GetGlobalEnv(JSThread *thread)
{
    return thread->GetEcmaVM()->GetGlobalEnv();
}

inline ObjectFactory *GetFactory(JSThread *thread)
{
    return thread->GetEcmaVM()->GetFactory();
}

inline JSThread *GetJSThread()
{
    return JSThread::Cast(ManagedThread::GetCurrent());
}

}  // namespace panda::ecmascript

#endif  // PANDA_COMPILER_THREAD_ENVIRONMENT_API_H
