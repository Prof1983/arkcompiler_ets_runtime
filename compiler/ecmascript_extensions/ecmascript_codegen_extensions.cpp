/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "compiler/optimizer/code_generator/codegen.h"
#include "ecmascript_environment.h"
#include "plugins/ecmascript/compiler/ecmascript_call_params.h"

namespace panda::compiler {

bool Codegen::GenerateLoadObjectDynamic(Inst *inst)
{
    auto dst_reg = ConvertRegister(inst->GetDstReg(), inst->GetType());
    auto obj_reg = ConvertRegister(inst->GetSrcReg(0), inst->GetInputType(0));
    auto key_reg = ConvertRegister(inst->GetSrcReg(1U), inst->GetInputType(1U));
    auto this_func = ConvertRegister(inst->GetSrcReg(2U), inst->GetInputType(2U));

    auto type = inst->CastToLoadObjectDynamic()->GetAccessType();
    auto mode = inst->CastToLoadObjectDynamic()->GetAccessMode();
    auto use_ic = type == DynObjectAccessType::BY_NAME;

    EntrypointId id = EntrypointId::LOAD_OBJECT_DYNAMIC;

    if (type == DynObjectAccessType::BY_INDEX) {
        if (mode == DynObjectAccessMode::DICTIONARY) {
            id = EntrypointId::LOAD_OBJECT_DYNAMIC_BY_INDEX_DICTIONARY;
        }
    } else if (type == DynObjectAccessType::BY_NAME) {
        id = EntrypointId::LOAD_OBJECT_DYNAMIC_BY_NAME;
    }

    if (use_ic) {
        CallFastPath(inst, id, dst_reg, {}, obj_reg, key_reg, TypedImm(inst->GetPc()));
    } else {
        if (type != DynObjectAccessType::BY_INDEX) {
            CallFastPath(inst, id, dst_reg, {}, obj_reg, key_reg, this_func);
        } else {
            CallFastPath(inst, id, dst_reg, {}, obj_reg, key_reg);
        }
    }

    return true;
}

bool Codegen::GenerateStoreObjectDynamic(Inst *inst)
{
    auto dst_reg = ConvertRegister(inst->GetDstReg(), inst->GetType());
    auto obj_reg = ConvertRegister(inst->GetSrcReg(0), inst->GetInputType(0));
    auto key_reg = ConvertRegister(inst->GetSrcReg(1U), inst->GetInputType(1U));
    auto val_reg = ConvertRegister(inst->GetSrcReg(2U), inst->GetInputType(2U));
    auto this_func = ConvertRegister(inst->GetSrcReg(3U), inst->GetInputType(3U));

    auto type = inst->CastToStoreObjectDynamic()->GetAccessType();
    auto mode = inst->CastToStoreObjectDynamic()->GetAccessMode();
    auto use_ic = type == DynObjectAccessType::BY_NAME;

    EntrypointId id = EntrypointId::STORE_OBJECT_DYNAMIC;

    if (type == DynObjectAccessType::BY_INDEX) {
        if (mode == DynObjectAccessMode::DICTIONARY) {
            id = EntrypointId::STORE_OBJECT_DYNAMIC_BY_INDEX_DICTIONARY;
        }
    } else if (type == DynObjectAccessType::BY_NAME) {
        id = EntrypointId::STORE_OBJECT_DYNAMIC_BY_NAME;
    }

    if (use_ic) {
        CallFastPath(inst, id, dst_reg, {}, obj_reg, key_reg, val_reg, TypedImm(inst->GetPc()));
    } else {
        if (type != DynObjectAccessType::BY_INDEX) {
            CallFastPath(inst, id, dst_reg, {}, obj_reg, key_reg, val_reg, this_func);
        } else {
            CallFastPath(inst, id, dst_reg, {}, obj_reg, key_reg, val_reg);
        }
    }

    return true;
}

}  // namespace panda::compiler
