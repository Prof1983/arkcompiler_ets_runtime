/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_COMPILER_ECMASCRIPT_ENVIRONMENT_H
#define PANDA_COMPILER_ECMASCRIPT_ENVIRONMENT_H

#include <cstddef>
#include "runtime/include/object_header.h"
#include "runtime/mem/object_helpers.h"
#include "libpandabase/mem/mem.h"
#include "runtime/include/coretypes/tagged_value.h"

namespace panda {

class ObjectHeader;

namespace ecmascript {

template <typename T>
class JSHandle;

class EcmascriptEnvironment {
public:
    EcmascriptEnvironment(ObjectHeader *cp, ObjectHeader *le, ObjectHeader *tf)
        : constant_pool_(cp), lexical_env_(le), this_func_(tf)
    {
    }

    static EcmascriptEnvironment *Cast(void *data)
    {
        return static_cast<EcmascriptEnvironment *>(data);
    }

    static constexpr size_t GetConstantPoolOffset()
    {
        return MEMBER_OFFSET(EcmascriptEnvironment, constant_pool_);
    }

    static constexpr size_t GetLexicalEnvOffset()
    {
        return MEMBER_OFFSET(EcmascriptEnvironment, lexical_env_);
    }

    static constexpr size_t GetThisFuncOffset()
    {
        return MEMBER_OFFSET(EcmascriptEnvironment, this_func_);
    }

    ObjectHeader *GetConstantPool() const
    {
        return constant_pool_.GetHeapObject();
    }

    ObjectHeader *GetLexicalEnv() const
    {
        return lexical_env_.GetHeapObject();
    }

    void SetLexicalEnv(ObjectHeader *le)
    {
        lexical_env_ = panda::coretypes::TaggedValue(le);
    }

    void SetConstantPool(ObjectHeader *cp)
    {
        constant_pool_ = panda::coretypes::TaggedValue(cp);
    }

    ObjectHeader *GetThisFunc() const
    {
        return this_func_.GetHeapObject();
    }

    void SetThisFunc(ObjectHeader *this_func)
    {
        this_func_ = panda::coretypes::TaggedValue(this_func);
    }

    static constexpr size_t GetSize()
    {
        return AlignUp(sizeof(EcmascriptEnvironment), GetAlignmentInBytes(DEFAULT_FRAME_ALIGNMENT));
    }

private:
    panda::coretypes::TaggedValue constant_pool_;
    panda::coretypes::TaggedValue lexical_env_;
    panda::coretypes::TaggedValue this_func_;
};

}  // namespace ecmascript

}  // namespace panda

#endif  // PANDA_COMPILER_ECMASCRIPT_ENVIRONMENT_H
