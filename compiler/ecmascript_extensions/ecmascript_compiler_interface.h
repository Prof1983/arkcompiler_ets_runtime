/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_COMPILER_ECMASCRIPT_COMPILER_INTERFACE_H
#define PANDA_COMPILER_ECMASCRIPT_COMPILER_INTERFACE_H

struct NewObjDynInfo {
    enum class AllocatorType {
        DEFAULT,    // try to allocate object with fallback into slow path
        ALLOC_OBJ,  // allocate regular object
        UNDEFINED,  // use undefined value as allocation result
        SLOW_PATH   // slow path allocation is required
    };

    enum class ResolverType {
        NONE,            // don't need a resolver at all - used with slow path allocation
        RESOLVE,         // result should be resoled based on returned value and allocated object
        USE_CTOR_RESULT  // always use value returned from a constructor
    };

    AllocatorType allocation;
    ResolverType resolution;
};

virtual NewObjDynInfo GetNewObjDynInfo([[maybe_unused]] uintptr_t method) const
{
    return {NewObjDynInfo::AllocatorType::DEFAULT, NewObjDynInfo::ResolverType::NONE};
}

virtual bool GetProfileDataForNamedAccess(
    [[maybe_unused]] RuntimeInterface::MethodPtr m, [[maybe_unused]] uintptr_t slot_id,
    [[maybe_unused]] ArenaVector<RuntimeInterface::NamedAccessProfileData> *profile)
{
    return false;
}

virtual bool GetProfileDataForNamedAccess(
    [[maybe_unused]] RuntimeInterface::MethodPtr m, [[maybe_unused]] uintptr_t func, [[maybe_unused]] uintptr_t slot_id,
    [[maybe_unused]] ArenaVector<RuntimeInterface::NamedAccessProfileData> *profile)
{
    return false;
}

virtual bool GetProfileDataForValueAccess(
    [[maybe_unused]] RuntimeInterface::MethodPtr m, [[maybe_unused]] uintptr_t slot_id,
    [[maybe_unused]] ArenaVector<RuntimeInterface::NamedAccessProfileData> *profile)
{
    return false;
}

virtual bool GetProfileDataForValueAccess(
    [[maybe_unused]] RuntimeInterface::MethodPtr m, [[maybe_unused]] uintptr_t func, [[maybe_unused]] uintptr_t slot_id,
    [[maybe_unused]] ArenaVector<RuntimeInterface::NamedAccessProfileData> *profile)
{
    return false;
}

virtual size_t GetLexicalEnvParentEnvIndex() const
{
    return 0;
}

virtual size_t GetLexicalEnvStartDataIndex() const
{
    return 0;
}

virtual size_t GetTaggedArrayElementSize() const
{
    return 0;
}

virtual size_t GetNumMandatoryArgs() const
{
    return 3U;
}

struct GlobalVarInfo {
    enum class Type {
        DEFAULT = 0,       // do not inline intrinsic
        CONSTANT,          // writable = false, configurable = false, not heap object. Inline as constant
        NON_CONFIGURABLE,  // writable = true, configurable = false. Can safely read from the same PropertyBox
        FIELD              // configurable = true, only simple own property reads seen in IC,
                           // read from PropertyBox and check not hole
    };

    Type type {Type::DEFAULT};
    // value for CONSTANT, address of PropertyBox for NON_CONFIGURABLE or FIELD
    uint64_t value {0};
};

virtual GlobalVarInfo GetGlobalVarInfo([[maybe_unused]] MethodPtr method, [[maybe_unused]] size_t id,
                                       [[maybe_unused]] uintptr_t slot_id) const
{
    return {GlobalVarInfo::Type::DEFAULT, 0};
}

virtual GlobalVarInfo GetGlobalVarInfo([[maybe_unused]] MethodPtr method, [[maybe_unused]] uintptr_t func,
                                       [[maybe_unused]] size_t id, [[maybe_unused]] uintptr_t slot_id) const
{
    return {GlobalVarInfo::Type::DEFAULT, 0};
}

virtual uintptr_t GetGlobalConstStringOffsetForAnyType([[maybe_unused]] compiler::AnyBaseType type,
                                                       [[maybe_unused]] Arch arch) const
{
    return 0;
}
#endif  // PANDA_COMPILER_ECMASCRIPT_COMPILER_INTERFACE_H