/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_ECMA_INLINING_H
#define PANDA_ECMA_INLINING_H

#include "compiler/optimizer/optimizations/inlining.h"
#include "compiler/optimizer/ir/runtime_interface.h"

namespace panda::compiler::ecmascript {

class EcmaInlining : public Inlining {
public:
    using Inlining::Inlining;

    explicit EcmaInlining(Graph *graph, uint32_t instructions_count, uint32_t inline_depth, uint32_t methods_inlined);
    explicit EcmaInlining(Graph *graph) : EcmaInlining(graph, 0, 0, 0) {};

    NO_MOVE_SEMANTIC(EcmaInlining);
    NO_COPY_SEMANTIC(EcmaInlining);
    ~EcmaInlining() override = default;
    static void BuildGuard(CallInst *call_inst, RuntimeInterface::MethodPtr method_ptr);

private:
    void RunOptimizations() const override;
    bool IsInstSuitableForInline(Inst *inst) const override;
    bool SkipBlock(const BasicBlock *block) const override;

    bool TryInline(CallInst *call_inst) override;
    bool DoEcmaMonomorphicInilning(CallInst *call_inst, InlineContext &ctx);
    bool DoEcmaPolymorphicInilning(CallInst *call_inst);
    bool ResolveTargets(CallInst *call_inst);
    RuntimeInterface::MethodPtr TryResolveTargetStatic(CallInst *call_inst) const;
    bool IsInstSuitableForEcmaStaticInlining(Inst *define_func) const;
    bool CheckCallKind(CallInst *call_inst);
    bool CheckMethod(CallInst *call_inst, InlineContext *ctx);
    Graph *BuildGraph(InlineContext *ctx, CallInst *call_inst, CallInst *new_call_inst = nullptr);
    void CreateCompareFunctions(CallInst *call_inst, RuntimeInterface::MethodPtr method_ptr, BasicBlock *call_bb);
    void InsertGraph(CallInst *call_inst, const InlineContext &ctx, Graph *graph_inl);
    bool IsStaticInlining() const
    {
        return js_functions_.empty();
    }
    bool CheckInlinableBuiltin(CallInst *call_inst, RuntimeInterface::MethodPtr native_method);

    bool IsMonomorphicInlining() const
    {
        return js_functions_.size() == 1U;
    }

private:
    // This vector contains pointers to element in EcmaCallProfilingTable
    // This element contains pointer to js function
    ArenaVector<uintptr_t> js_functions_;
};

}  // namespace panda::compiler::ecmascript

#endif  // PANDA_ECMA_INLINING_H