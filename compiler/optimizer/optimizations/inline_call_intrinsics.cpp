/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "inline_call_intrinsics.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "optimizer/ir/basicblock.h"
#include "optimizer/analysis/dominators_tree.h"
#include "optimizer/analysis/loop_analyzer.h"
#include "runtime/include/coretypes/tagged_value.h"

namespace panda::compiler {

bool InlineCallIntrinsics::RunImpl()
{
    bool success = false;
    for (auto *bb : GetGraph()->GetVectorBlocks()) {
        if (bb == nullptr) {
            continue;
        }

        for (auto inst : bb->InstsSafeReverse()) {
            if (!inst->IsIntrinsic()) {
                continue;
            }
            auto intrin_inst = inst->CastToIntrinsic();
            success |= Expand(intrin_inst);
        }
    }
    return success;
}

bool InlineCallIntrinsics::Expand(IntrinsicInst *inst)
{
    auto id = inst->GetIntrinsicId();
    switch (id) {
        case RuntimeInterface::IntrinsicId::INTRINSIC_NEWOBJ_DYNRANGE:
        case RuntimeInterface::IntrinsicId::INTRINSIC_NEWOBJ_DYNRANGE_HANDLED:
            return ExpandNewObjDynRange(inst);
        default:
            return false;
    }
}

Inst *InlineCallIntrinsics::CreateAllocDynObjIntrinsic(Inst *ctor, SaveStateInst *ss, uint32_t pc)
{
    auto alloc_obj =
        GetGraph()->CreateInstIntrinsic(DataType::ANY, pc, RuntimeInterface::IntrinsicId::INTRINSIC_ALLOC_DYN_OBJECT);
    alloc_obj->ReserveInputs(2);
    alloc_obj->AllocateInputTypes(GetGraph()->GetAllocator(), 2);
    alloc_obj->AppendInput(ctor);
    alloc_obj->AddInputType(DataType::ANY);
    alloc_obj->AppendInput(ss);
    alloc_obj->AddInputType(DataType::NO_TYPE);
    return alloc_obj;
}

Inst *InlineCallIntrinsics::NewObjCreateConstructorCall(Inst *orig_call, Inst *alloc_obj, SaveStateInst *ss,
                                                        uint32_t pc)
{
    auto call_ctor = GetGraph()->CreateInstCallDynamic(DataType::ANY, pc);
    call_ctor->ReserveInputs(orig_call->GetInputsCount() + 1);
    call_ctor->AllocateInputTypes(GetGraph()->GetAllocator(), orig_call->GetInputsCount() + 1);
    call_ctor->AppendInput(orig_call->GetInput(1));
    call_ctor->AddInputType(DataType::ANY);
    call_ctor->AppendInput(orig_call->GetInput(2));
    call_ctor->AddInputType(DataType::ANY);
    call_ctor->AppendInput(alloc_obj);
    call_ctor->AddInputType(DataType::ANY);
    for (size_t i = 3; i < orig_call->GetInputsCount() - 1; ++i) {
        call_ctor->AppendInput(orig_call->GetInput(i));
        call_ctor->AddInputType(orig_call->GetInputType(i));
    }
    call_ctor->AppendInput(ss);
    call_ctor->AddInputType(DataType::NO_TYPE);
    return call_ctor;
}

void InlineCallIntrinsics::NewObjFillCurrBlock(InstAppender *appender, Inst *alloc_obj, uint32_t pc)
{
    auto cmp = GetGraph()->CreateInstCompareAnyType(pc, alloc_obj, AnyBaseType::ECMASCRIPT_NULL_TYPE);
    // if alloc returned null -> jump into a slow path
    auto ifimm = GetGraph()->CreateInstIfImm(DataType::BOOL, pc, cmp, 0, DataType::BOOL, ConditionCode::CC_NE);
    appender->Append({cmp, ifimm});
}

void InlineCallIntrinsics::NewObjFillSlowPathBlock(InstAppender *appender, Inst *orig_alloc)
{
    auto curr_block = orig_alloc->GetBasicBlock();
    auto ss = orig_alloc->CastToIntrinsic()->GetSaveState();
    auto ss_copy = static_cast<SaveStateInst *>(ss->Clone(GetGraph()));
    for (size_t input_idx = 0; input_idx < ss->GetInputsCount(); ++input_idx) {
        ss_copy->AppendInput(ss->GetInput(input_idx));
        ss_copy->SetVirtualRegister(input_idx, ss->GetVirtualRegister(input_idx));
    }
    orig_alloc->SetSaveState(ss_copy);
    // move original intrinsic and its SaveState into a slow path
    curr_block->EraseInst(orig_alloc, true);
    appender->Append({ss_copy, orig_alloc});
}

Inst *InlineCallIntrinsics::NewObjFillCallConstructorBlock(InstAppender *appender, Inst *orig_alloc, Inst *alloc_obj,
                                                           uint32_t pc)
{
    auto ss = orig_alloc->GetSaveState();
    auto ss_copy = static_cast<SaveStateInst *>(ss->Clone(GetGraph()));
    for (size_t input_idx = 0; input_idx < ss->GetInputsCount(); ++input_idx) {
        ss_copy->AppendInput(ss->GetInput(input_idx));
        ss_copy->SetVirtualRegister(input_idx, ss->GetVirtualRegister(input_idx));
    }
    ss_copy->AppendBridge(alloc_obj);
    auto call_ctor = NewObjCreateConstructorCall(orig_alloc, alloc_obj, ss_copy, pc);

    appender->Append({ss_copy, call_ctor});

    return call_ctor;
}

Inst *InlineCallIntrinsics::NewObjResolveCtorResult(InstAppender *appender, Inst *orig_alloc, Inst *alloc_obj,
                                                    Inst *call_ctor, uint32_t pc)
{
    auto ss = call_ctor->GetSaveState();
    auto ss_copy_2 = static_cast<SaveStateInst *>(ss->Clone(GetGraph()));
    for (size_t input_idx = 0; input_idx < ss->GetInputsCount(); ++input_idx) {
        ss_copy_2->AppendInput(ss->GetInput(input_idx));
        ss_copy_2->SetVirtualRegister(input_idx, ss->GetVirtualRegister(input_idx));
    }
    ss_copy_2->AppendBridge(call_ctor);

    auto resolve_result = GetGraph()->CreateInstIntrinsic(
        DataType::ANY, pc, RuntimeInterface::IntrinsicId::INTRINSIC_RESOLVE_ALLOC_RESULT);
    resolve_result->ReserveInputs(4);
    resolve_result->AllocateInputTypes(GetGraph()->GetAllocator(), 4);
    resolve_result->AppendInput(orig_alloc->GetInput(1));
    resolve_result->AddInputType(DataType::ANY);
    resolve_result->AppendInput(alloc_obj);
    resolve_result->AddInputType(DataType::ANY);
    resolve_result->AppendInput(call_ctor);
    resolve_result->AddInputType(DataType::ANY);
    resolve_result->AppendInput(ss_copy_2);
    resolve_result->AddInputType(DataType::NO_TYPE);
    resolve_result->SetInlined(true);

    appender->Append({ss_copy_2, resolve_result});

    return resolve_result;
}

/*
 * Expands NewObjDynRange intrinsics into a fast path consisting of memory
 * allocation via AllocDynObject intrinsic, constructor's call, resolution
 * of a result via ResolveAllocResult intrinsic and a slow path consisting of
 * NewObjDynRange intrinsic call.
 *
 * Expanded IR has following shape:
 *
 *     allocated_value := AllocDynObject
 *        if allocated_value is null
 *        /                         \
 *       /                           \
 * slow_path_res := NewObjDynRange    |
 *      |         ctor_res := call Ctor(allocated_value)
 *      |         fast_path_res := ResolveAllocResult(Ctor, allocated_value, ctor_res)
 *       \                           /
 *        \_________________________/
 *                    |
 *    res := Phi(slow_path_res, fast_path_res)
 *
 * Such expansion improves performance of object allocation by using specialized
 * stubs outperforming generic runtime code and allows inlining of a constructor.
 *
 * Depending on profiling info shape of the emitted IR may change by pruning one of the branches.
 */
bool InlineCallIntrinsics::ExpandNewObjDynRange(IntrinsicInst *inst)
{
    Inst *ctor_inst = inst->GetInput(1).GetInst();
    Inst *new_target_inst = inst->GetInput(2).GetInst();

    if (ctor_inst != new_target_inst) {
        return false;
    }

    auto runtime = GetGraph()->GetRuntime();
    auto kind = panda::profiling::CallKind::UNKNOWN;
    if (OPTIONS.IsCompilerEcmaNewobjProfiling()) {
        functions_.clear();
        kind = runtime->GetCallProfile(GetGraph()->GetMethod(), inst->GetPc(), &functions_, GetGraph()->IsAotMode());
    }
    RuntimeInterface::NewObjDynInfo expansion_info {RuntimeInterface::NewObjDynInfo::AllocatorType::DEFAULT,
                                                    RuntimeInterface::NewObjDynInfo::ResolverType::RESOLVE};
    if (kind == panda::profiling::CallKind::MONOMORPHIC) {
        expansion_info = runtime->GetNewObjDynInfo(functions_.front());
    }

    switch (expansion_info.allocation) {
        // allocation always require slow path, so it doesn't make sense to expand the intrinsic
        case RuntimeInterface::NewObjDynInfo::AllocatorType::SLOW_PATH:
            assert(expansion_info.resolution == RuntimeInterface::NewObjDynInfo::ResolverType::RESOLVE);
            return false;
        // emit simplified fast path, but only for monomorphic call sites
        case RuntimeInterface::NewObjDynInfo::AllocatorType::UNDEFINED:
        case RuntimeInterface::NewObjDynInfo::AllocatorType::ALLOC_OBJ:
            if (kind == panda::profiling::CallKind::MONOMORPHIC) {
                ExpandNewObjDynFastPath(inst, expansion_info, functions_.front());
                return true;
            } else {
                ExpandNewObjDynDefault(inst);
                return true;
            }
        default:
            ExpandNewObjDynDefault(inst);
            return true;
    }

    return false;
}

void InlineCallIntrinsics::ExpandNewObjDynDefault(Inst *inst)
{
    auto curr_block = inst->GetBasicBlock();
    auto succ_block = curr_block->SplitBlockAfterInstruction(inst, false);
    auto pc = inst->GetPc();

    auto slow_path_block = GetGraph()->CreateEmptyBlock(curr_block);
    auto call_ctor_block = GetGraph()->CreateEmptyBlock(curr_block);

    InstAppender curr_block_appender {curr_block};
    auto alloc_obj = CreateAllocDynObjIntrinsic(inst->GetInput(1).GetInst(), inst->GetSaveState(), pc);
    curr_block_appender.Append({alloc_obj});
    NewObjFillCurrBlock(&curr_block_appender, alloc_obj, pc);

    InstAppender slow_path_block_appender {slow_path_block};
    NewObjFillSlowPathBlock(&slow_path_block_appender, inst);

    InstAppender call_ctor_block_appender {call_ctor_block};
    auto ctor = NewObjFillCallConstructorBlock(&call_ctor_block_appender, inst, alloc_obj, pc);
    auto ctor_res = NewObjResolveCtorResult(&call_ctor_block_appender, inst, alloc_obj, ctor, pc);

    curr_block->AddSucc(slow_path_block);
    curr_block->AddSucc(call_ctor_block);
    slow_path_block->AddSucc(succ_block);
    call_ctor_block->AddSucc(succ_block);

    auto phi = GetGraph()->CreateInstPhi(DataType::ANY, pc);
    inst->ReplaceUsers(phi);
    phi->AppendInput(inst);
    phi->AppendInput(ctor_res);
    succ_block->AppendPhi(phi);

    GetGraph()->InvalidateAnalysis<LoopAnalyzer>();
    GetGraph()->InvalidateAnalysis<DominatorsTree>();
    InvalidateBlocksOrderAnalyzes(GetGraph());
}

void InlineCallIntrinsics::ExpandNewObjDynFastPath(Inst *inst, RuntimeInterface::NewObjDynInfo info, uintptr_t target)
{
    auto curr_block = inst->GetBasicBlock();
    auto pc = inst->GetPc();

    BuildGuard(inst, target);

    Inst *alloc_obj;
    InstAppender appender {curr_block, inst};
    if (info.allocation == RuntimeInterface::NewObjDynInfo::AllocatorType::ALLOC_OBJ) {
        alloc_obj = CreateAllocDynObjIntrinsic(inst->GetInput(1).GetInst(), inst->GetSaveState(), pc);
        appender.Append({alloc_obj});
    } else {
        ASSERT(info.allocation == RuntimeInterface::NewObjDynInfo::AllocatorType::UNDEFINED);
        alloc_obj = GetGraph()->FindOrCreateConstant(DataType::Any(panda::coretypes::TaggedValue::VALUE_UNDEFINED));
    }

    auto ctor_res = NewObjFillCallConstructorBlock(&appender, inst, alloc_obj, pc);

    Inst *res = ctor_res;

    bool need_resolver = info.resolution == RuntimeInterface::NewObjDynInfo::ResolverType::RESOLVE;
    if (need_resolver) {
        res = NewObjResolveCtorResult(&appender, inst, alloc_obj, ctor_res, pc);
    }
    inst->ReplaceUsers(res);
    curr_block->RemoveInst(inst);
}

void InlineCallIntrinsics::BuildGuard(Inst *inst, uintptr_t target)
{
    auto pc = inst->GetPc();
    auto load_function = GetGraph()->CreateInstFunctionImmediate(DataType::ANY, pc, target);
    auto cmp_inst = GetGraph()->CreateInstCompare(DataType::BOOL, pc, load_function, inst->GetInput(1).GetInst(),
                                                  DataType::ANY, ConditionCode::CC_NE);
    auto deopt_inst = GetGraph()->CreateInstDeoptimizeIf(DataType::BOOL, pc, cmp_inst, inst->GetSaveState(),
                                                         DeoptimizeType::INLINE_DYN);

    auto save_state = inst->GetSaveState();
    ASSERT(save_state);
    save_state->InsertBefore(load_function);
    save_state->AppendBridge(load_function);
    inst->InsertBefore(cmp_inst);
    inst->InsertBefore(deopt_inst);
}

}  // namespace panda::compiler
