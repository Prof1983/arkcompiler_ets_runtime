/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ecma_inlining.h"
#include "inline_call_intrinsics.h"
#include "events/events.h"
#include "optimizer/ir/graph.h"
#include "optimizer/analysis/loop_analyzer.h"
#include "optimizer/optimizations/cleanup.h"
#include "optimizer/optimizations/branch_elimination.h"
#include "optimizer/optimizations/peepholes.h"
#include "runtime/profiling/profiling.h"

namespace panda::compiler::ecmascript {

EcmaInlining::EcmaInlining(Graph *graph, uint32_t instructions_count, uint32_t inline_depth, uint32_t methods_inlined)
    : Inlining(graph, instructions_count, inline_depth, methods_inlined),
      js_functions_(graph->GetLocalAllocator()->Adapter())
{
    instructions_limit_ = OPTIONS.GetCompilerEcmaInliningMaxInsts();
}

void EcmaInlining::RunOptimizations() const
{
    if (GetGraph()->RunPass<InlineCallIntrinsics>()) {
        GetGraph()->RunPass<LoopAnalyzer>();
    }
    if (GetGraph()->RunPass<Peepholes>()) {
        GetGraph()->RunPass<BranchElimination>();
    }
}

bool EcmaInlining::SkipBlock(const BasicBlock *block) const
{
    return Inlining::SkipBlock(block) || block->IsTry();
}

bool EcmaInlining::IsInstSuitableForInline(Inst *inst) const
{
    return inst->GetOpcode() == Opcode::CallDynamic;
}

bool EcmaInlining::ResolveTargets(CallInst *call_inst)
{
    ASSERT(js_functions_.empty());
    auto runtime = GetGraph()->GetRuntime();
    auto kind =
        runtime->GetCallProfile(GetGraph()->GetMethod(), call_inst->GetPc(), &js_functions_, GetGraph()->IsAotMode());
    if (kind == profiling::CallKind::MEGAMORPHIC || kind == profiling::CallKind::UNKNOWN) {
        LOG_INLINING(DEBUG) << "Call have " << CallKindToString(kind) << " type";
        EVENT_INLINE(runtime->GetMethodFullName(GetGraph()->GetMethod()), "-", call_inst->GetId(),
                     events::InlineKind::DYNAMIC_POLYMORPHIC,
                     kind == profiling::CallKind::MEGAMORPHIC ? events::InlineResult::FAIL_MEGAMORPHIC
                                                              : events::InlineResult::FAIL_RESOLVE);
        return false;
    }
    return true;
}

bool EcmaInlining::IsInstSuitableForEcmaStaticInlining(Inst *define_func) const
{
    if (define_func->GetOpcode() != Opcode::Intrinsic) {
        return false;
    }
    switch (define_func->CastToIntrinsic()->GetIntrinsicId()) {
        case RuntimeInterface::IntrinsicId::INTRINSIC_DEFINEFUNC_DYN:
        case RuntimeInterface::IntrinsicId::INTRINSIC_DEFINE_NC_FUNC_DYN:
        case RuntimeInterface::IntrinsicId::INTRINSIC_DEFINE_METHOD:
            return true;
        default:
            return false;
    }
}

RuntimeInterface::MethodPtr EcmaInlining::TryResolveTargetStatic(CallInst *call_inst) const
{
    auto define_func = call_inst->GetDataFlowInput(0);
    if (IsInstSuitableForEcmaStaticInlining(define_func)) {
        auto load_from_cp = define_func->CastToIntrinsic()->GetInput(0).GetInst()->CastToLoadFromConstantPool();
        return GetGraph()->GetRuntime()->GetMethodByIdAndSaveJsFunction(GetGraph()->GetMethod(),
                                                                        load_from_cp->GetTypeId());
    }
    return nullptr;
}

bool EcmaInlining::CheckMethod(CallInst *call_inst, InlineContext *ctx)
{
    if (!CheckMethodCanBeInlined<false>(call_inst, ctx)) {
        return false;
    }

    if (!CheckBytecode(call_inst, *ctx, nullptr)) {
        return false;
    }
    return true;
}

Graph *EcmaInlining::BuildGraph(InlineContext *ctx, CallInst *call_inst, CallInst *new_call_inst)
{
    auto graph_inl = GetGraph()->CreateChildGraph(ctx->method);

    // Propagate instruction id counter to inlined graph, thereby avoid instructions id duplication
    graph_inl->SetCurrentInstructionId(GetGraph()->GetCurrentInstructionId());
    graph_inl->SetMaxInliningDepth(depth_);

    auto stats = GetGraph()->GetPassManager()->GetStatistics();
    auto saved_pbc_inst_num = stats->GetPbcInstNum();
    if (!TryBuildGraph(*ctx, graph_inl, call_inst, new_call_inst)) {
        stats->SetPbcInstNum(saved_pbc_inst_num);
        return nullptr;
    }

    // Run basic optimizations
    graph_inl->RunPass<InlineCallIntrinsics>();
    graph_inl->RunPass<Cleanup>(false);
    if (graph_inl->RunPass<Peepholes>()) {
        graph_inl->RunPass<BranchElimination>();
        graph_inl->RunPass<Cleanup>();
    }

    // Don't inline if we reach the limit of instructions and method is big enough.
    auto inlined_insts_count = CalculateInstructionsCount(graph_inl);
    if (!CheckInstructionLimit(call_inst, ctx, inlined_insts_count)) {
        stats->SetPbcInstNum(saved_pbc_inst_num);
        [[maybe_unused]] auto runtime = GetGraph()->GetRuntime();
        EVENT_INLINE(runtime->GetMethodFullName(GetGraph()->GetMethod()), runtime->GetMethodFullName(ctx->method),
                     call_inst->GetId(), events::InlineKind::DYNAMIC_MONOMORPHIC, events::InlineResult::LIMIT);
        LOG_INLINING(DEBUG) << "Reach the limit of instructions and method is big enough.";
        return nullptr;
    }
    if ((depth_ + 1) < OPTIONS.GetCompilerInliningMaxDepth()) {
        graph_inl->RunPass<EcmaInlining>(instructions_count_ + inlined_insts_count, depth_ + 1, methods_inlined_ + 1);
    }
    instructions_count_ += CalculateInstructionsCount(graph_inl);
    GetGraph()->SetMaxMarkerIdx(graph_inl->GetCurrentMarkerIdx());
    GetGraph()->SetCurrentInstructionId(graph_inl->GetCurrentInstructionId());
    GetGraph()->SetMaxInliningDepth(graph_inl->GetMaxInliningDepth() + 1);
    return graph_inl;
}

void EcmaInlining::BuildGuard(CallInst *call_inst, RuntimeInterface::MethodPtr method_ptr)
{
    auto graph = call_inst->GetBasicBlock()->GetGraph();
    auto method_cnst = graph->CreateInstLoadImmediate(DataType::POINTER, call_inst->GetPc(), method_ptr,
                                                      LoadImmediateInst::ObjectType::METHOD);
    auto load_method =
        graph->CreateInstLoadObject(DataType::POINTER, call_inst->GetPc(), call_inst->GetInput(0).GetInst(),
                                    TypeIdMixin::MEM_DYN_METHOD_ID, nullptr, nullptr);
    load_method->SetObjectType(ObjectType::MEM_DYN_METHOD);
    auto cmp_inst = graph->CreateInstCompare(DataType::BOOL, call_inst->GetPc(), load_method, method_cnst,
                                             DataType::POINTER, ConditionCode::CC_NE);
    auto deopt_inst = graph->CreateInstDeoptimizeIf(DataType::BOOL, call_inst->GetPc(), cmp_inst,
                                                    call_inst->GetSaveState(), DeoptimizeType::INLINE_DYN);
    call_inst->InsertBefore(method_cnst);
    call_inst->InsertBefore(load_method);
    call_inst->InsertBefore(cmp_inst);
    call_inst->InsertBefore(deopt_inst);
}

void EcmaInlining::InsertGraph(CallInst *call_inst, const InlineContext &ctx, Graph *graph_inl)
{
    if (!IsStaticInlining()) {
        BuildGuard(call_inst, ctx.method);
    }

    auto call_bb = call_inst->GetBasicBlock();
    auto call_cont_bb = call_bb->SplitBlockAfterInstruction(call_inst, false);

    UpdateParameterDataflow(graph_inl, call_inst);
    UpdateDataflow(graph_inl, call_inst, call_cont_bb);
    MoveConstants(graph_inl);
    UpdateControlflow(graph_inl, call_bb, call_cont_bb);

    if (call_cont_bb->GetPredsBlocks().empty()) {
        GetGraph()->RemoveUnreachableBlocks();
    } else {
        return_blocks_.push_back(call_cont_bb);
    }

    bool need_barriers = GetGraph()->GetRuntime()->IsMemoryBarrierRequired(ctx.method);
    ProcessCallReturnInstructions(call_inst, call_cont_bb, true, need_barriers);

    call_inst->SetCallMethod(ctx.method);
    call_inst->SetCallMethodId(GetGraph()->GetRuntime()->GetMethodId(ctx.method));
    if (!IsStaticInlining()) {
        call_inst->SetFunctionObject(js_functions_[0]);
    }
}

bool EcmaInlining::DoEcmaMonomorphicInilning(CallInst *call_inst, InlineContext &ctx)
{
    ASSERT(js_functions_.size() <= 1U);
    LOG_INLINING(DEBUG) << "Try inline monomorphic" << (IsStaticInlining() ? "(static)" : "") << " CallDynamic:";
    LOG_INLINING(DEBUG) << "  instruction: " << *call_inst;
    LOG_INLINING(DEBUG) << "Method: " << GetGraph()->GetRuntime()->GetMethodFullName(ctx.method, true);

    if (OPTIONS.IsCompilerEcmaInlineBuiltins() && CheckInlinableBuiltin(call_inst, ctx.method)) {
        // Actual inlining will happen during InlineIntrinsics:
        return false;
    }

    if (!CheckMethod(call_inst, &ctx)) {
        LOG_INLINING(DEBUG) << "Unsuitable bytecode";
        return false;
    }

    // Build graph for current target and run EcmaInlining recursively
    auto graph_inl = BuildGraph(&ctx, call_inst);
    if (graph_inl == nullptr) {
        return false;
    }
    InsertGraph(call_inst, ctx, graph_inl);
    GetGraph()->GetPassManager()->GetStatistics()->AddInlinedMethods(1);
    methods_inlined_++;
    EVENT_INLINE(GetGraph()->GetRuntime()->GetMethodFullName(GetGraph()->GetMethod()),
                 GetGraph()->GetRuntime()->GetMethodFullName(ctx.method), call_inst->GetId(),
                 IsStaticInlining() ? events::InlineKind::STATIC : events::InlineKind::DYNAMIC_MONOMORPHIC,
                 events::InlineResult::SUCCESS);
    LOG_INLINING(DEBUG) << "Successfully monomorophic " << (IsStaticInlining() ? "(static)" : "")
                        << " inlined:" << GetGraph()->GetRuntime()->GetMethodFullName(ctx.method);
    return true;
}

void EcmaInlining::CreateCompareFunctions(CallInst *call_inst, RuntimeInterface::MethodPtr method_ptr,
                                          BasicBlock *call_bb)
{
    auto method_cnst = GetGraph()->CreateInstLoadImmediate(DataType::POINTER, call_inst->GetPc(), method_ptr,
                                                           LoadImmediateInst::ObjectType::METHOD);
    auto load_method =
        GetGraph()->CreateInstLoadObject(DataType::POINTER, call_inst->GetPc(), call_inst->GetInput(0).GetInst(),
                                         TypeIdMixin::MEM_DYN_METHOD_ID, nullptr, nullptr);
    load_method->SetObjectType(ObjectType::MEM_DYN_METHOD);
    auto cmp_inst = GetGraph()->CreateInstCompare(DataType::BOOL, call_inst->GetPc(), load_method, method_cnst,
                                                  DataType::POINTER, ConditionCode::CC_EQ);
    auto if_inst = GetGraph()->CreateInstIfImm(DataType::BOOL, call_inst->GetPc(), cmp_inst, 0, DataType::BOOL,
                                               ConditionCode::CC_NE);
    call_bb->AppendInst(method_cnst);
    call_bb->AppendInst(load_method);
    call_bb->AppendInst(cmp_inst);
    call_bb->AppendInst(if_inst);
}

bool EcmaInlining::DoEcmaPolymorphicInilning(CallInst *call_inst)
{
    ASSERT(js_functions_.size() > 1U);
    ASSERT(js_functions_.size() <= profiling::MAX_FUNC_NUMBER);
    LOG_INLINING(DEBUG) << "Try inline polymorphic call(" << js_functions_.size() << " receivers):";
    LOG_INLINING(DEBUG) << "  instruction: " << *call_inst;

    bool has_unreachable_blocks = false;
    PhiInst *phi_inst = nullptr;
    BasicBlock *call_bb = nullptr;
    BasicBlock *call_cont_bb = nullptr;
    auto runtime = GetGraph()->GetRuntime();
    auto inlined_methods = methods_inlined_;
    for (uintptr_t func : js_functions_) {
        InlineContext ctx {runtime->GetMethodFromFunction(func)};
        LOG_INLINING(DEBUG) << "Method: " << GetGraph()->GetRuntime()->GetMethodFullName(ctx.method, true);
        if (!CheckMethod(call_inst, &ctx)) {
            continue;
        }
        CallInst *new_call_inst = call_inst->Clone(GetGraph())->CastToCallDynamic();
        new_call_inst->SetCallMethodId(runtime->GetMethodId(ctx.method));
        new_call_inst->SetCallMethod(ctx.method);

        // Build graph for current target and run EcmaInlining recursively
        auto graph_inl = BuildGraph(&ctx, call_inst, new_call_inst);
        if (graph_inl == nullptr) {
            continue;
        }
        if (call_bb == nullptr) {
            // Split block by call instruction
            call_bb = call_inst->GetBasicBlock();
            call_cont_bb = call_bb->SplitBlockAfterInstruction(call_inst, false);
            if (call_inst->GetType() != DataType::VOID) {
                phi_inst = GetGraph()->CreateInstPhi(call_inst->GetType(), call_inst->GetPc());
                phi_inst->ReserveInputs(js_functions_.size() << 1U);
                call_cont_bb->AppendPhi(phi_inst);
            }
        } else {
            auto new_call_bb = GetGraph()->CreateEmptyBlock(call_bb);
            call_bb->GetLoop()->AppendBlock(new_call_bb);
            call_bb->AddSucc(new_call_bb);
            call_bb = new_call_bb;
        }
        CreateCompareFunctions(call_inst, ctx.method, call_bb);

        // Create call_inlined_block
        auto call_inlined_block = GetGraph()->CreateEmptyBlock(call_bb);
        call_bb->GetLoop()->AppendBlock(call_inlined_block);
        call_bb->AddSucc(call_inlined_block);

        // Insert Call.inlined in call_inlined_block
        new_call_inst->AppendInput(call_inst->GetObjectInst());
        new_call_inst->AppendInput(call_inst->GetSaveState());
        new_call_inst->SetInlined(true);
        new_call_inst->SetFlag(inst_flags::NO_DST);
        new_call_inst->SetFunctionObject(func);
        call_inlined_block->PrependInst(new_call_inst);

        // Create return_inlined_block and inster PHI for non void functions
        auto return_inlined_block = GetGraph()->CreateEmptyBlock(call_bb);
        call_bb->GetLoop()->AppendBlock(return_inlined_block);
        PhiInst *local_phi_inst = nullptr;
        if (call_inst->GetType() != DataType::VOID) {
            local_phi_inst = GetGraph()->CreateInstPhi(call_inst->GetType(), call_inst->GetPc());
            local_phi_inst->ReserveInputs(js_functions_.size());
            return_inlined_block->AppendPhi(local_phi_inst);
        }

        // Inlined graph between call_inlined_block and return_inlined_block
        UpdateParameterDataflow(graph_inl, call_inst);
        UpdateDataflow(graph_inl, call_inst, local_phi_inst, phi_inst);
        MoveConstants(graph_inl);
        UpdateControlflow(graph_inl, call_inlined_block, return_inlined_block);
        if (!return_inlined_block->GetPredsBlocks().empty()) {
            auto inlined_return = GetGraph()->CreateInstReturnInlined(DataType::VOID, INVALID_PC);
            return_inlined_block->PrependInst(inlined_return);
            inlined_return->SetInput(0, new_call_inst->GetSaveState());
            if (call_inst->GetType() != DataType::VOID) {
                ASSERT(phi_inst);
                // clang-tidy think that phi_inst can be nullptr
                phi_inst->AppendInput(local_phi_inst);  // NOLINT
            }
            return_inlined_block->AddSucc(call_cont_bb);
        } else {
            // We need remove return_inlined_block if inlined graph doesn't have Return inst(only Throw or Deoptimize)
            has_unreachable_blocks = true;
        }
        GetGraph()->GetPassManager()->GetStatistics()->AddInlinedMethods(1);
        EVENT_INLINE(runtime->GetMethodFullName(GetGraph()->GetMethod()), runtime->GetMethodFullName(ctx.method),
                     call_inst->GetId(), events::InlineKind::DYNAMIC_POLYMORPHIC, events::InlineResult::SUCCESS);
        LOG_INLINING(DEBUG) << "Successfully polymorphic inlined: " << GetMethodFullName(GetGraph(), ctx.method);
        methods_inlined_++;
    }
    if (call_bb == nullptr) {
        // Nothing was inlined
        return false;
    }
    if (call_cont_bb->GetPredsBlocks().empty() || has_unreachable_blocks) {
        GetGraph()->RemoveUnreachableBlocks();
    }
    if (methods_inlined_ - inlined_methods == js_functions_.size()) {
        InsertDeoptimizeInst(call_inst, call_bb, DeoptimizeType::INLINE_DYN);
    } else {
        InsertCallInst(call_inst, call_bb, call_cont_bb, phi_inst);
    }

    if (call_inst->GetType() != DataType::VOID) {
        call_inst->ReplaceUsers(phi_inst);
    }

    ProcessCallReturnInstructions(call_inst, call_cont_bb, true);
    call_inst->GetBasicBlock()->RemoveInst(call_inst);
    return true;
}

bool EcmaInlining::TryInline(CallInst *call_inst)
{
    js_functions_.clear();
    auto method = TryResolveTargetStatic(call_inst);
    if (method != nullptr) {
        InlineContext ctx {method};
        return DoEcmaMonomorphicInilning(call_inst, ctx);
    }
    if (!ResolveTargets(call_inst)) {
        LOG_INLINING(DEBUG) << "Target methods were not found for CallDynaimc(id = " << call_inst->GetId() << ")";
        return false;
    }
    ASSERT(!js_functions_.empty());
    if (IsMonomorphicInlining()) {
        InlineContext ctx {GetGraph()->GetRuntime()->GetMethodFromFunction(js_functions_[0])};
        return DoEcmaMonomorphicInilning(call_inst, ctx);
    }
    return DoEcmaPolymorphicInilning(call_inst);
}

bool EcmaInlining::CheckInlinableBuiltin(CallInst *call_inst, RuntimeInterface::MethodPtr method)
{
    if (GetGraph()->GetRuntime()->IsInlinableNativeMethod(method)) {
        call_inst->SetCallMethod(method);
        ASSERT(!js_functions_.empty());
        return true;
    }
    return false;
}

}  // namespace panda::compiler::ecmascript
