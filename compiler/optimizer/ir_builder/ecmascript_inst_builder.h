/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PLUGINS_ECMASCRIPT_COMPILER_OPTIMIZER_IR_BUILDER_ECMASCRIPT_INST_BUILDER_H
#define PLUGINS_ECMASCRIPT_COMPILER_OPTIMIZER_IR_BUILDER_ECMASCRIPT_INST_BUILDER_H

void BuildEcma([[maybe_unused]] const BytecodeInstruction *bc_inst);
template <bool WITH_SPECULATIVE = false>
void BuildEcmaAsIntrinsics([[maybe_unused]] const BytecodeInstruction *bc_inst);
void BuildEcmaFromIrtoc([[maybe_unused]] const BytecodeInstruction *bc_inst);
template <bool IS_ACC_READ>
void BuildStGlobalVar(const BytecodeInstruction *bc_inst, size_t type_id);
template <bool IS_ACC_WRITE>
void BuildLdGlobalVar(const BytecodeInstruction *bc_inst, size_t type_id);

void BuildStObjByIndex(const BytecodeInstruction *bc_inst, uint64_t imm);
void BuildLdObjByIndex(const BytecodeInstruction *bc_inst, uint64_t imm);

void BuildLdStObjByIndex(const BytecodeInstruction *bc_inst, Inst *obj_inst, uint64_t imm, Inst *store_def);
void BuildEcmaFnCall(const BytecodeInstruction *bc_inst, bool is_range, bool call_this, uint64_t num_args = 0);
void FinalizeEcmaFnCall(SaveStateInst *save_state, CallInst *call_inst);

void BuildEcmaNewobjdynrange(const BytecodeInstruction *bc_inst);
void BuildEcmaGetunmappedargs(const BytecodeInstruction *bc_inst);

#endif  // PLUGINS_ECMASCRIPT_COMPILER_OPTIMIZER_IR_BUILDER_ECMASCRIPT_INST_BUILDER_H
