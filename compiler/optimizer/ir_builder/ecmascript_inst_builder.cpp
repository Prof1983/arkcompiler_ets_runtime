/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdint>
#include "compiler_logger.h"
#include "optimizer/ir_builder/inst_builder.h"
#include "optimizer/ir_builder/ir_builder.h"
#include "optimizer/ir/inst.h"
#include "bytecode_instruction.h"
#include "bytecode_instruction-inl.h"
#include "include/coretypes/tagged_value.h"
#include "optimizer/ir_builder/pbc_iterator.h"
namespace panda::compiler {

void InstBuilder::FinalizeEcmaFnCall(SaveStateInst *save_state, CallInst *call_inst)
{
    call_inst->AppendInput(save_state);
    call_inst->AddInputType(DataType::NO_TYPE);
    AddInstruction(call_inst);
    UpdateDefinitionAcc(call_inst);
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
void InstBuilder::BuildEcmaFnCall(const BytecodeInstruction *bc_inst, bool is_range, bool call_this, uint64_t num_args)
{
    auto bc_pc = GetPc(bc_inst->GetAddress());
    auto save_state = CreateSaveState(Opcode::SaveState, bc_pc);
    AddInstruction(save_state);

    Inst *callee {};

    switch (bc_inst->GetOpcode()) {
        case BytecodeInstruction::Opcode::ECMA_CALL0DYN_PREF_NONE_PROF16: {
            callee = GetDefinitionAcc();
            break;
        }
        default: {
            callee = GetDefinition(bc_inst->GetVReg(0));
            break;
        }
    }

    // Check callee is heap object
    auto type_check = BuildAnyTypeCheckInst(bc_pc, callee, save_state, AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE);
    callee = type_check;

    {  // Load class (it is movable object in js)
        auto load_class = GetGraph()->CreateInstLoadObject(
            DataType::REFERENCE, bc_pc, callee, TypeIdMixin::MEM_DYN_CLASS_ID, GetGraph()->GetMethod(), nullptr);
        load_class->SetObjectType(ObjectType::MEM_DYN_CLASS);
        AddInstruction(load_class);

        // Load Jshclass
        auto load_hclass = GetGraph()->CreateInstLoadObject(
            DataType::REFERENCE, bc_pc, load_class, TypeIdMixin::MEM_DYN_HCLASS_ID, GetGraph()->GetMethod(), nullptr);
        load_hclass->SetObjectType(ObjectType::MEM_DYN_HCLASS);
        AddInstruction(load_hclass);

        auto hclass_check = GetGraph()->CreateInstHclassCheck(DataType::NO_TYPE, bc_pc, load_hclass, save_state);
        hclass_check->SetCheckIsFunction(true);
        hclass_check->SetCheckFunctionIsNotClassConstructor(true);
        AddInstruction(hclass_check);
    }

    auto call_inst = GetGraph()->CreateInstCallDynamic(DataType::ANY, bc_pc);
    call_inst->SetCanNativeException(true);

    // func, new_target, this?, args..., ss
    uint64_t args_count = 2U + (call_this ? 0 : 1) + (is_range ? bc_inst->GetImm64() : num_args) + 1U;
    call_inst->ReserveInputs(args_count);
    call_inst->AllocateInputTypes(GetGraph()->GetAllocator(), args_count);

    // Append func, new_target, this
    call_inst->AppendInput(callee);
    call_inst->AddInputType(DataType::ANY);
    call_inst->AppendInput(FindOrCreateAnyConstant(DataType::Any(coretypes::TaggedValue::VALUE_UNDEFINED)));
    call_inst->AddInputType(DataType::ANY);

    if (!call_this) {  // Assume strict mode
        call_inst->AppendInput(FindOrCreateAnyConstant(DataType::Any(coretypes::TaggedValue::VALUE_UNDEFINED)));
        call_inst->AddInputType(DataType::ANY);
    } else {
        num_args++;
    }

    switch (bc_inst->GetOpcode()) {
        case BytecodeInstruction::Opcode::ECMA_CALL0DYN_PREF_NONE_PROF16: {
            FinalizeEcmaFnCall(save_state, call_inst);
            return;
        }
        case BytecodeInstruction::Opcode::ECMA_CALLIRANGEDYN_PREF_IMM16_V8_PROF16:
        case BytecodeInstruction::Opcode::ECMA_CALLITHISRANGEDYN_PREF_IMM16_V8_PROF16: {
            auto range_start = bc_inst->GetVReg(0) + 1;
            auto range_size = bc_inst->GetImm64();

            for (int64_t i = 0; i < range_size; ++i) {
                call_inst->AppendInput(GetDefinition(range_start + i));
                call_inst->AddInputType(DataType::ANY);
            }

            FinalizeEcmaFnCall(save_state, call_inst);
            return;
        }
        default: {
            break;
        }
    }

    for (uint64_t i = 1; i < num_args; ++i) {
        call_inst->AppendInput(GetDefinition(bc_inst->GetVReg(i)));
        call_inst->AddInputType(DataType::ANY);
    }

    call_inst->AppendInput(GetDefinitionAcc());
    call_inst->AddInputType(DataType::ANY);

    FinalizeEcmaFnCall(save_state, call_inst);
}

void InstBuilder::BuildEcmaNewobjdynrange(const BytecodeInstruction *bc_inst)
{
    auto graph = GetGraph();
    auto bc_pc = GetPc(bc_inst->GetAddress());
    auto start_reg = bc_inst->GetVReg(0);

    auto inst = graph->CreateInstIntrinsic(DataType::ANY, bc_pc,
                                           RuntimeInterface::IntrinsicId::INTRINSIC_NEWOBJ_DYNRANGE_HANDLED);
    AdjustFlags(inst->GetIntrinsicId(), inst);
    inst->SetFlag(inst_flags::CAN_THROW);

    auto save_state = CreateSaveState(Opcode::SaveState, bc_pc);
    AddInstruction(save_state);

    uint32_t constexpr FIXED_INPUTS = 2U;
    auto args_count = bc_inst->GetImm64();
    inst->ReserveInputs(args_count + FIXED_INPUTS);
    inst->AllocateInputTypes(graph->GetAllocator(), args_count + FIXED_INPUTS);

    inst->AppendInput(FindOrCreateConstant(args_count - 2));
    inst->AddInputType(DataType::UINT16);

    for (int64_t i = 0; i < args_count; ++i) {
        inst->AppendInput(GetDefinition(start_reg + i));
        inst->AddInputType(DataType::ANY);
    }
    inst->AppendInput(save_state);
    inst->AddInputType(DataType::NO_TYPE);
    inst->SetFlag(inst_flags::ACC_WRITE);
    AddInstruction(inst);
    UpdateDefinitionAcc(inst);
}

void InstBuilder::BuildEcmaGetunmappedargs(const BytecodeInstruction *bc_inst)
{
    // TODO(pishin) support for inlined graph
    if (caller_inst_ != nullptr) {
        failed_ = true;
        return;
    }
    auto graph = GetGraph();
    // If more parameters are passed to the function than in its description,
    // then the GC can move the "implicit" parameters.
    // So we remove SafePoint from the start block and check SaveState instruction between
    for (auto sp : graph->GetStartBlock()->AllInsts()) {
        if (sp->GetOpcode() != Opcode::SafePoint) {
            continue;
        }
        sp->ClearFlag(inst_flags::NO_DCE);
    }
    auto bb = GetCurrentBlock();
    // Conservative check that there is no GC call from beggining to the instruction
    if (bb->GetPredsBlocks().size() != 1 || bb->GetPredecessor(0) != graph->GetStartBlock()) {
        failed_ = true;
        return;
    }
    auto curr_inst = bb->GetLastInst();
    while (curr_inst != nullptr) {
        if (curr_inst->IsSaveState()) {
            failed_ = true;
            return;
        }
        curr_inst = curr_inst->GetPrev();
    }
    auto bc_pc = GetPc(bc_inst->GetAddress());
    auto inst =
        graph->CreateInstIntrinsic(DataType::ANY, bc_pc, RuntimeInterface::IntrinsicId::INTRINSIC_GET_UNMAPPED_ARGS);
    AdjustFlags(inst->GetIntrinsicId(), inst);
    inst->SetFlag(inst_flags::CAN_THROW);

    auto save_state = CreateSaveState(Opcode::SaveState, bc_pc);
    AddInstruction(save_state);

    // first input is actual num args, second save state
    uint32_t constexpr FIXED_INPUTS = 2U;
    inst->ReserveInputs(FIXED_INPUTS);
    inst->AllocateInputTypes(graph->GetAllocator(), FIXED_INPUTS);
    auto num_args_inst = graph->FindParameter(ParameterInst::DYNAMIC_NUM_ARGS);
    ASSERT(num_args_inst != nullptr);
    inst->AppendInput(num_args_inst);
    inst->AddInputType(DataType::UINT32);

    inst->AppendInput(save_state);
    inst->AddInputType(DataType::NO_TYPE);
    inst->SetFlag(inst_flags::ACC_WRITE);
    AddInstruction(inst);
    UpdateDefinitionAcc(inst);
}

}  // namespace panda::compiler

#include "ecmascript_inst_builder_gen.cpp"

namespace panda::compiler {

template <bool IS_ACC_READ>
void InstBuilder::BuildStGlobalVar(const BytecodeInstruction *bc_inst, size_t type_id)
{
    auto pc = GetPc(bc_inst->GetAddress());
    auto save_state = CreateSaveState(Opcode::SaveState, pc);
    auto get_address = graph_->CreateInstGetGlobalVarAddress(DataType::REFERENCE, pc, GetEnvDefinition(CONST_POOL_IDX),
                                                             save_state, type_id, GetGraph()->GetMethod(), 0);

    auto store_object =
        graph_->CreateInstStoreObject(DataType::ANY, pc, get_address, nullptr, TypeIdMixin::MEM_DYN_GLOBAL_ID,
                                      GetGraph()->GetMethod(), nullptr, false, true);
    store_object->SetObjectType(ObjectType::MEM_DYN_GLOBAL);
    Inst *store_val = nullptr;
    if constexpr (IS_ACC_READ) {
        store_val = GetDefinitionAcc();
    } else {
        store_val = GetDefinition(bc_inst->GetVReg(0));
    }

    store_object->SetInput(1, store_val);
    AddInstruction(save_state);
    AddInstruction(get_address);
    AddInstruction(store_object);
}

template <bool IS_ACC_WRITE>
void InstBuilder::BuildLdGlobalVar(const BytecodeInstruction *bc_inst, size_t type_id)
{
    auto pc = GetPc(bc_inst->GetAddress());
    auto save_state = CreateSaveState(Opcode::SaveState, pc);
    auto get_address = graph_->CreateInstGetGlobalVarAddress(DataType::REFERENCE, pc, GetEnvDefinition(CONST_POOL_IDX),
                                                             save_state, type_id, GetGraph()->GetMethod(), 0);

    auto load_object = graph_->CreateInstLoadObject(DataType::ANY, pc, get_address, TypeIdMixin::MEM_DYN_GLOBAL_ID,
                                                    GetGraph()->GetMethod(), nullptr);
    load_object->SetObjectType(ObjectType::MEM_DYN_GLOBAL);
    AddInstruction(save_state);
    AddInstruction(get_address);
    AddInstruction(load_object);

    if constexpr (IS_ACC_WRITE) {
        UpdateDefinitionAcc(load_object);
    } else {
        UpdateDefinition(bc_inst->GetVReg(0), load_object);
    }
}

void InstBuilder::BuildStObjByIndex(const BytecodeInstruction *bc_inst, uint64_t imm)
{
    auto pc = GetPc(bc_inst->GetAddress());
    if (GetRuntime()->CanInlineLdStObjByIndex(bc_inst, pc, method_profile_)) {
        BuildLdStObjByIndex(bc_inst, GetDefinition(bc_inst->GetVReg(0)), imm, GetDefinitionAcc());
    } else {
        BuildEcma(bc_inst);
    }
}

void InstBuilder::BuildLdObjByIndex(const BytecodeInstruction *bc_inst, uint64_t imm)
{
    auto pc = GetPc(bc_inst->GetAddress());
    if (GetRuntime()->CanInlineLdStObjByIndex(bc_inst, pc, method_profile_)) {
        BuildLdStObjByIndex(bc_inst, GetDefinitionAcc(), imm, nullptr);
    } else {
        BuildEcma(bc_inst);
    }
}

void InstBuilder::BuildLdStObjByIndex(const BytecodeInstruction *bc_inst, Inst *obj_inst, uint64_t imm, Inst *store_def)
{
    auto pc = GetPc(bc_inst->GetAddress());
    auto check_obj = graph_->CreateInstObjByIndexCheck(DataType::ANY, pc, obj_inst, nullptr);

    auto load_object = graph_->CreateInstLoadObject(DataType::REFERENCE, pc, check_obj,
                                                    TypeIdMixin::MEM_DYN_ELEMENTS_ID, GetGraph()->GetMethod(), nullptr);
    load_object->SetObjectType(ObjectType::MEM_DYN_ELEMENTS);

    Inst *save_state = nullptr;
    Inst *null_check = nullptr;
    Inst *array_length = nullptr;
    Inst *bounds_check = nullptr;
    BuildChecksBeforeArray(pc, load_object, &save_state, &null_check, &array_length, &bounds_check, false);
    ASSERT(save_state != nullptr && null_check == load_object && array_length != nullptr && bounds_check != nullptr);

    check_obj->SetInput(1, save_state);

    // Create instruction
    auto inst = graph_->CreateInstLoadArray(DataType::ANY, pc, null_check, bounds_check);
    bounds_check->SetInput(1, FindOrCreateConstant(imm));
    bounds_check->SetFlag(inst_flags::CAN_DEOPTIMIZE);

    auto cmp_inst = graph_->CreateInstCompare(
        DataType::BOOL, pc, inst,
        graph_->FindOrCreateConstant(DataType::Any(panda::coretypes::TaggedValue::VALUE_HOLE)), DataType::ANY,
        ConditionCode::CC_EQ);
    auto deopt_inst = graph_->CreateInstDeoptimizeIf(DataType::NO_TYPE, pc, cmp_inst, save_state, DeoptimizeType::HOLE);

    AddInstruction(save_state, check_obj, load_object, array_length, bounds_check, inst, cmp_inst, deopt_inst);

    if (store_def != nullptr) {
        auto store = graph_->CreateInstStoreArray(DataType::ANY, pc, load_object, bounds_check, store_def, true);
        AddInstruction(store);
    } else {
        UpdateDefinitionAcc(inst);
    }
}

template void InstBuilder::BuildStGlobalVar<true>(const BytecodeInstruction *bc_inst, size_t type_id);
template void InstBuilder::BuildStGlobalVar<false>(const BytecodeInstruction *bc_inst, size_t type_id);

template void InstBuilder::BuildLdGlobalVar<true>(const BytecodeInstruction *bc_inst, size_t type_id);
template void InstBuilder::BuildLdGlobalVar<false>(const BytecodeInstruction *bc_inst, size_t type_id);

}  // namespace panda::compiler
