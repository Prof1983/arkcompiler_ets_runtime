/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PLUGINS_ECMASCRIPT_COMPILER_OPTIMIZER_CODE_GENERATOR_COMPILER_BASE_TYPES_H
#define PLUGINS_ECMASCRIPT_COMPILER_OPTIMIZER_CODE_GENERATOR_COMPILER_BASE_TYPES_H
#include "compiler/optimizer/code_generator/encode.h"
#include "source_languages.h"

namespace panda::compiler {

class Inst;
class CompareAnyTypeInst;
class CastAnyTypeValueInst;
class CastValueToAnyTypeInst;
class AnyTypeCheckInst;
class GetAnyTypeNameInst;
class FixedInputsInst2;
class EncodeVisitor;
class HclassCheckInst;

namespace ecmascript {

bool CompareAnyTypeGen(const CompareAnyTypeInst *cati, EncodeVisitor *enc_v);
bool CastAnyTypeValueGen(const CastAnyTypeValueInst *cati, EncodeVisitor *enc_v);
bool CastValueToAnyTypeGen(const CastValueToAnyTypeInst *cvai, EncodeVisitor *enc_v);
bool AnyTypeCheckGen(AnyTypeCheckInst *check_inst, EncodeVisitor *enc_v);
bool ObjByIndexCheckGen(const FixedInputsInst2 *check_inst, EncodeVisitor *enc_v, LabelHolder::LabelId id);
bool DynamicCallCheckGen(FixedInputsInst2 *check_inst, EncodeVisitor *enc_v);
bool LoadConstantPoolGen(const Inst *inst, EncodeVisitor *enc_v);
bool LoadLexicalEnvGen(const Inst *inst, EncodeVisitor *enc_v);
bool GetAnyTypeNameGen(const GetAnyTypeNameInst *inst, EncodeVisitor *enc_v);
bool HclassCheckGen(HclassCheckInst *check_inst, EncodeVisitor *enc_v);
bool IsDynHeapObject(AnyBaseType type);

}  // namespace ecmascript
}  // namespace panda::compiler

#endif  // PLUGINS_ECMASCRIPT_COMPILER_OPTIMIZER_CODE_GENERATOR_COMPILER_BASE_TYPES_H
