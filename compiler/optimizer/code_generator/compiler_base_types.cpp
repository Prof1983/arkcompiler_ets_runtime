/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "compiler_base_types.h"

#include "compiler/optimizer/ir/datatype.h"
#include "compiler/optimizer/code_generator/codegen.h"
#include "ir-dyn-base-types.h"
#include "runtime/include/coretypes/tagged_value.h"
#include "profiling/profiling.h"

namespace panda::compiler {

static void IsHasTagMaskGen(Encoder *enc, Reg dst, Reg src, Imm val_tag, Imm val_mask,
                            LabelHolder::LabelId id = LabelHolder::INVALID_LABEL)
{
    ScopedTmpReg reg_val_tag(enc, TypeInfo(TypeInfo::TypeId::INT64));
    auto dst_ext = dst.As(TypeInfo(TypeInfo::TypeId::INT64));

    enc->EncodeMov(reg_val_tag, val_tag);
    if (val_tag != val_mask) {
        enc->EncodeAnd(dst_ext, src, val_mask);
    } else {
        enc->EncodeAnd(dst_ext, src, reg_val_tag);
    }
    if (id == LabelHolder::INVALID_LABEL) {
        enc->EncodeCompare(dst, dst_ext, reg_val_tag, Condition::EQ);
    } else {
        enc->EncodeJump(id, dst_ext, reg_val_tag, Condition::NE);
    }
}

static void CompareAnyTypeGenDouble(Encoder *enc, Reg dst, Reg src, profiling::AnyInputType allowed_input_type,
                                    LabelHolder::LabelId not_number_label = LabelHolder::INVALID_LABEL,
                                    LabelHolder::LabelId double_with_int_label = LabelHolder::INVALID_LABEL)
{
    static constexpr uint16_t CMP_VAL = static_cast<uint16_t>(UINT16_MAX) >> 1U;  // 0x7fff
    static constexpr uint8_t SHIFT_VAL = coretypes::TaggedValue::TAG_BITS_SHIFT + 1;

    auto dst_ext = dst.As(TypeInfo(TypeInfo::TypeId::INT64));

    auto jump = not_number_label != LabelHolder::INVALID_LABEL;
    ASSERT(allowed_input_type == profiling::AnyInputType::DEFAULT || jump);
    if (allowed_input_type == profiling::AnyInputType::SPECIAL_INT) {
        constexpr auto MIN_OBJECT = coretypes::TaggedValue::VALUE_UNDEFINED + 1;
        constexpr auto MIN_NUMBER = 1ULL << coretypes::TaggedValue::TAG_BITS_SHIFT;
        static_assert(coretypes::TaggedValue::VALUE_TRUE < MIN_OBJECT);
        static_assert(coretypes::TaggedValue::TAG_EXCEPTION >= MIN_OBJECT);
        enc->EncodeSub(dst_ext, src, Imm(MIN_OBJECT));
        enc->EncodeJump(not_number_label, dst_ext, Imm(MIN_NUMBER - MIN_OBJECT), Condition::LO);
        return;
    }
    if (allowed_input_type == profiling::AnyInputType::INTEGER) {
        enc->EncodeShr(dst_ext, src, Imm(coretypes::TaggedValue::TAG_BITS_SHIFT));
        enc->EncodeJump(not_number_label, dst_ext, Imm(0), Condition::EQ);
        return;
    }
    LabelHolder::LabelId end_label = enc->CreateLabel();
    if (allowed_input_type == profiling::AnyInputType::SPECIAL) {
        enc->EncodeJump(end_label, src, Imm(coretypes::TaggedValue::VALUE_UNDEFINED), Condition::EQ);
    } else {
        ASSERT(allowed_input_type == profiling::AnyInputType::DEFAULT);
    }
    // (u16) tag + 0xffff is [0xffff for Object, 0xfffe for Int, other - Double]
    enc->EncodeAdd(dst_ext, src, Imm(panda::coretypes::TaggedValue::TAG_MASK));
    // Examine tag and shift it by one -> 0x7fff for both Object and Int
    enc->EncodeShr(dst_ext, dst_ext, Imm(SHIFT_VAL));
    ScopedTmpReg reg_cmp(enc, TypeInfo(TypeInfo::TypeId::INT64));
    enc->EncodeMov(reg_cmp, Imm(CMP_VAL));
    // check if not Object and not Int
    if (!jump) {
        enc->EncodeCompare(dst, dst_ext, reg_cmp, Condition::LO);
    } else if (double_with_int_label != LabelHolder::INVALID_LABEL) {
        enc->EncodeJump(end_label, dst_ext, reg_cmp, Condition::LT);
        auto bit = coretypes::TaggedValue::TAG_BITS_SIZE + coretypes::TaggedValue::TAG_BITS_SHIFT - 1;
        enc->EncodeBitTestAndBranch(double_with_int_label, src, bit, true);
        enc->EncodeJump(not_number_label);
    } else {
        enc->EncodeJump(not_number_label, dst_ext, reg_cmp, Condition::GE);
    }
    enc->BindLabel(end_label);
}

static void CompareAnyTypeGenInt(Encoder *enc, Reg dst, Reg src, profiling::AnyInputType allowed_input_type,
                                 LabelHolder::LabelId id = LabelHolder::INVALID_LABEL)
{
    if (allowed_input_type == profiling::AnyInputType::DEFAULT) {
        IsHasTagMaskGen(enc, dst, src, Imm(panda::coretypes::TaggedValue::TAG_INT),
                        Imm(panda::coretypes::TaggedValue::TAG_MASK), id);
        return;
    }
    ASSERT(allowed_input_type == profiling::AnyInputType::SPECIAL);
    constexpr auto MIN_FAIL = coretypes::TaggedValue::VALUE_UNDEFINED;
    // accept values < VALUE_UNDEFINED or >= TAG_INT
    auto dst_ext = dst.As(TypeInfo(TypeInfo::TypeId::INT64));
    enc->EncodeSub(dst_ext, src, Imm(MIN_FAIL));
    if (id == LabelHolder::INVALID_LABEL) {
        ScopedTmpReg reg_cmp(enc, TypeInfo(TypeInfo::TypeId::INT64));
        enc->EncodeMov(reg_cmp, Imm(coretypes::TaggedValue::TAG_INT - MIN_FAIL));
        enc->EncodeCompare(dst_ext, dst_ext, reg_cmp, Condition::HS);
    } else {
        enc->EncodeJump(id, dst_ext, Imm(coretypes::TaggedValue::TAG_INT - MIN_FAIL), Condition::LO);
    }
}

static void CompareAnyTypeGenBool(Encoder *enc, Reg dst, Reg src, LabelHolder::LabelId id = LabelHolder::INVALID_LABEL)
{
    static constexpr uint64_t BOOL_DIFF_MASK =
        ~(panda::coretypes::TaggedValue::VALUE_FALSE ^ panda::coretypes::TaggedValue::VALUE_TRUE);
    static constexpr uint64_t BOOL_MASK =
        panda::coretypes::TaggedValue::VALUE_FALSE & panda::coretypes::TaggedValue::VALUE_TRUE;

    ScopedTmpReg reg_cmp(enc, TypeInfo(TypeInfo::TypeId::INT64));
    auto dst_ext = dst.As(TypeInfo(TypeInfo::TypeId::INT64));

    enc->EncodeAnd(dst_ext, src, Imm(BOOL_DIFF_MASK));
    enc->EncodeMov(reg_cmp, Imm(BOOL_MASK));
    if (id == LabelHolder::INVALID_LABEL) {
        enc->EncodeCompare(dst, dst_ext, reg_cmp, Condition::EQ);
    } else {
        enc->EncodeJump(id, dst_ext, reg_cmp, Condition::NE);
    }
}

static void IsEqualToValGen(Encoder *enc, Reg dst, Reg src, Imm val,
                            LabelHolder::LabelId id = LabelHolder::INVALID_LABEL)
{
    if (id == LabelHolder::INVALID_LABEL) {
        ScopedTmpReg reg_val(enc, TypeInfo(TypeInfo::TypeId::INT64));
        enc->EncodeMov(reg_val, val);
        enc->EncodeCompare(dst, src, reg_val, Condition::EQ);
    } else {
        enc->EncodeJump(id, src, val, Condition::NE);
    }
}

// Jump out if !IsHeapObject()
static void CheckAnyTypeGenObject(Encoder *enc, const Reg &src, LabelHolder::LabelId label)
{
    ScopedTmpReg tmp_reg(enc, TypeInfo(TypeInfo::TypeId::INT64));

    // IsObject()
    enc->EncodeAnd(tmp_reg, src, Imm(panda::coretypes::TaggedValue::TAG_MASK));
    enc->EncodeJump(label, tmp_reg, Imm(panda::coretypes::TaggedValue::TAG_OBJECT), Condition::NE);

    // !IsSpecial
    // It's enough to check that `src` > `TAG_SPECIAL_MASK`, as it's guaranteed that special values
    // are not in object address space
    ASSERT(!IsAddressInObjectsHeapOrNull(panda::coretypes::TaggedValue::TAG_SPECIAL_MASK));
    enc->EncodeJump(label, src, Imm(panda::coretypes::TaggedValue::TAG_SPECIAL_MASK), Condition::LE);
}

// Jump out if (!IsHeapObject() or <fail_cc> object.type, type)
static void CheckAnyTypeGenObjectType(Codegen *codegen, Encoder *enc, const Reg &src, uint32_t type,
                                      LabelHolder::LabelId label, Condition fail_cc = Condition::NE)
{
    CheckAnyTypeGenObject(enc, src, label);

    auto arch = codegen->GetArch();
    ScopedTmpReg tmp_reg(enc, codegen->ConvertDataType(DataType::REFERENCE, arch));

    enc->EncodeLdr(tmp_reg, false, MemRef(src, codegen->GetRuntime()->GetObjClassOffset(arch)));
    auto type_start_bit = cross_values::GetJshclassBitfieldTypeStartBit(arch);
    auto bit_mask = static_cast<uint64_t>(cross_values::GetJshclassBitfieldTypeMask(arch));
    enc->EncodeLdr(tmp_reg, false, MemRef(tmp_reg, cross_values::GetHclassDataOffset(arch)));
    if (type_start_bit != 0) {
        enc->EncodeShr(tmp_reg, tmp_reg, Imm(type_start_bit));
    }
    enc->EncodeAnd(tmp_reg, tmp_reg, Imm(bit_mask));
    enc->EncodeJump(label, tmp_reg, Imm(type), fail_cc);
}

// dst = IsHeapObject()
static void CompareAnyTypeGenObject(Encoder *enc, const Reg &dst, const Reg &src)
{
    auto label_false = enc->CreateLabel();
    enc->EncodeMov(dst, Imm(0));
    CheckAnyTypeGenObject(enc, src, label_false);
    enc->EncodeMov(dst, Imm(1U));
    enc->BindLabel(label_false);
}

static void CompareOrCheckAnyTypeCallable(Codegen *codegen, Encoder *enc, const Reg &dst, const Reg &src,
                                          LabelHolder::LabelId id = LabelHolder::INVALID_LABEL)
{
    auto label_false = enc->CreateLabel();
    auto arch = codegen->GetArch();
    Reg res_reg = dst;
    bool need_temp = (dst.GetId() == src.GetId());
    ScopedTmpRegLazy tmp_dst(enc);
    if (need_temp) {
        tmp_dst.AcquireIfInvalid();
        res_reg = tmp_dst.GetReg().As(codegen->ConvertDataType(DataType::ANY, arch));
    }

    if (id == LabelHolder::INVALID_LABEL) {
        enc->EncodeMov(res_reg, Imm(0));
    }
    LabelHolder::LabelId label = (id == LabelHolder::INVALID_LABEL) ? label_false : id;
    CheckAnyTypeGenObject(enc, src, label);
    ScopedTmpReg tmp_reg(enc, codegen->ConvertDataType(DataType::REFERENCE, arch));

    enc->EncodeLdr(tmp_reg, false, MemRef(src, codegen->GetRuntime()->GetObjClassOffset(arch)));
    Reg tmp32_reg = tmp_reg.GetReg().As(TypeInfo(TypeInfo::TypeId::INT32));
    enc->EncodeLdr(tmp32_reg, false, MemRef(tmp_reg, codegen->GetRuntime()->GetBaseClassFlagsOffset(arch)));
    auto is_callable_mask = codegen->GetRuntime()->GetCallableMask();
    enc->EncodeJumpTest(label, tmp32_reg, Imm(is_callable_mask), Condition::TST_EQ);
    if (id == LabelHolder::INVALID_LABEL) {
        enc->EncodeMov(res_reg, Imm(1U));
        enc->BindLabel(label_false);
        if (need_temp) {
            enc->EncodeMov(dst, res_reg);
        }
    }
}

// dst = (IsHeapObject() and !<fail_cc> object.type, type)
static void CompareAnyTypeGenObjectType(Codegen *codegen, Encoder *enc, const Reg &dst, const Reg &src, uint32_t type,
                                        Condition fail_cc = Condition::NE)
{
    Reg res_reg = dst;
    auto arch = codegen->GetArch();
    bool need_temp = (dst.GetId() == src.GetId());
    ScopedTmpRegLazy tmp_reg(enc);
    if (need_temp) {
        tmp_reg.AcquireIfInvalid();
        res_reg = tmp_reg.GetReg().As(codegen->ConvertDataType(DataType::ANY, arch));
    }
    auto label_false = enc->CreateLabel();
    enc->EncodeMov(res_reg, Imm(0));
    CheckAnyTypeGenObjectType(codegen, enc, src, type, label_false, fail_cc);
    enc->EncodeMov(res_reg, Imm(1U));
    enc->BindLabel(label_false);
    if (need_temp) {
        enc->EncodeMov(dst, res_reg);
    }
}

bool ecmascript::GetAnyTypeNameGen(const GetAnyTypeNameInst *inst, EncodeVisitor *enc_v)
{
    Encoder *enc = enc_v->GetEncoder();
    Codegen *codegen = enc_v->GetCodegen();
    auto dst = codegen->ConvertRegister(inst->GetDstReg(), DataType::Type::ANY);

    auto graph = codegen->GetGraph();
    auto offset = graph->GetRuntime()->GetGlobalConstStringOffsetForAnyType(inst->GetAnyType(), graph->GetArch());
    enc->EncodeLdr(dst, false, MemRef(codegen->ThreadReg(), offset));

    return true;
}

bool ecmascript::CompareAnyTypeGen(const CompareAnyTypeInst *cati, EncodeVisitor *enc_v)
{
    auto *codegen = enc_v->GetCodegen();
    auto dst = enc_v->GetCodegen()->ConvertRegister(cati->GetDstReg(), DataType::Type::BOOL);
    auto src = enc_v->GetCodegen()->ConvertRegister(cati->GetSrcReg(0), DataType::Type::INT64);

    Encoder *enc = enc_v->GetEncoder();
    switch (cati->GetAnyType()) {
        case AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE:
            IsEqualToValGen(enc, dst, src, Imm(panda::coretypes::TaggedValue::VALUE_UNDEFINED));
            return true;
        case AnyBaseType::ECMASCRIPT_INT_TYPE:
            CompareAnyTypeGenInt(enc, dst, src, cati->GetAllowedInputType());
            return true;
        case AnyBaseType::ECMASCRIPT_OBJECT_TYPE:
            IsHasTagMaskGen(enc, dst, src, Imm(panda::coretypes::TaggedValue::TAG_OBJECT),
                            Imm(panda::coretypes::TaggedValue::TAG_MASK));
            return true;
        case AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE:
            CompareAnyTypeGenObject(enc, dst, src);
            return true;
        case AnyBaseType::ECMASCRIPT_DOUBLE_TYPE:
            CompareAnyTypeGenDouble(enc, dst, src, cati->GetAllowedInputType());
            return true;
        case AnyBaseType::ECMASCRIPT_STRING_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src, cross_values::GetJstypeString(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_SYMBOL_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src, cross_values::GetJstypeSymbol(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_BIGINT_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src, cross_values::GetJstypeBigint(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_ARRAY_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src, cross_values::GetJstypeJsArray(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_TRANSITION_HANDLER_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src,
                                        cross_values::GetJstypeTransitionHandler(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_PROTOTYPE_HANDLER_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src,
                                        cross_values::GetJstypePrototypeHandler(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_SPECIAL_INDEXED_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src, cross_values::GetJstypeJsArray(codegen->GetArch()),
                                        Condition::LE);
            return true;
        case AnyBaseType::ECMASCRIPT_FUNCTION_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, dst, src, cross_values::GetJstypeJsFunction(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_CALLABLE_TYPE:
            CompareOrCheckAnyTypeCallable(codegen, enc, dst, src);
            return true;
        case AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE: {
            CompareAnyTypeGenBool(enc, dst, src);
            return true;
        }
        case AnyBaseType::ECMASCRIPT_NULL_TYPE:
            IsEqualToValGen(enc, dst, src, Imm(panda::coretypes::TaggedValue::VALUE_NULL));
            return true;
        case AnyBaseType::ECMASCRIPT_HOLE_TYPE:
            IsEqualToValGen(enc, dst, src, Imm(panda::coretypes::TaggedValue::VALUE_HOLE));
            return true;
        default:
            return false;
    }

    return false;
}

// NOLINTNEXTLINE(readability-function-size)
bool ecmascript::CastAnyTypeValueGen(const CastAnyTypeValueInst *cati, EncodeVisitor *enc_v)
{
    auto dst_reg_type = cati->GetType();
    dst_reg_type = DataType::IsFloatType(dst_reg_type) ? DataType::FLOAT64 : dst_reg_type;
    auto dst = enc_v->GetCodegen()->ConvertRegister(cati->GetDstReg(), dst_reg_type);
    auto src = enc_v->GetCodegen()->ConvertRegister(cati->GetSrcReg(0), DataType::INT64);

    auto *enc = enc_v->GetEncoder();
    switch (cati->GetAnyType()) {
        case AnyBaseType::ECMASCRIPT_NULL_TYPE:
        case AnyBaseType::ECMASCRIPT_HOLE_TYPE:
        case AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE:
            enc->EncodeMov(dst, Imm(0));
            return true;
        case AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE:
            enc->EncodeAnd(dst, src, Imm(1));
            return true;
        case AnyBaseType::ECMASCRIPT_INT_TYPE: {
            if (DataType::GetTypeSize(dst_reg_type, RUNTIME_ARCH) >
                DataType::GetTypeSize(DataType::INT32, RUNTIME_ARCH)) {
                ASSERT(false && "sign-extension by dst type is not supported");
            }
            Reg res_reg = src;
            ScopedTmpRegLazy tmp_dst(enc);
            if (cati->GetAllowedInputType() == profiling::AnyInputType::SPECIAL) {
                if (dst.GetId() == src.GetId()) {
                    tmp_dst.AcquireIfInvalid();
                    res_reg = tmp_dst;
                } else {
                    res_reg = dst.As(INT64_TYPE);
                }
                enc->EncodeAShr(res_reg, src, Imm(BitNumbers<coretypes::TaggedType>() - 1));
                enc->EncodeOr(res_reg, res_reg, Imm(1));
                // here res_reg contains 1 for special values and -1 for tagged ints
                enc->EncodeAnd(res_reg, res_reg, src);
            }
            ASSERT(res_reg.GetSize() == DOUBLE_WORD_SIZE);
            ASSERT(!dst.IsFloat() && !src.IsFloat());
            enc->EncodeCast(dst, false, res_reg, false);
            return true;
        }
        case AnyBaseType::ECMASCRIPT_DOUBLE_TYPE: {
            ASSERT(dst.IsFloat());
            ScopedTmpReg reg_tmp(enc, TypeInfo(TypeInfo::INT64));
            auto end_label = enc->CreateLabel();
            auto return_label = enc->CreateLabel();
            if ((cati->GetAllowedInputType() & profiling::AnyInputType::SPECIAL) != 0) {
                // We allow Hole value in AnyTypeCheck with SPECIAL flag, but assume it cannot be
                // CastValueToAnyType input, so it is cast to 0 instead of NaN here
                SCOPED_DISASM_STR(enc_v->GetCodegen(), "Try cast from Undefined");
                enc->EncodeMov(reg_tmp,
                               Imm(coretypes::ReinterpretDoubleToTaggedType(coretypes::TaggedValue::VALUE_NAN)));
                enc->EncodeJump(return_label, src, Imm(coretypes::TaggedValue::VALUE_UNDEFINED), Condition::EQ);
            }
            if (cati->GetAllowedInputType() == profiling::AnyInputType::SPECIAL_INT) {
                SCOPED_DISASM_STR(enc_v->GetCodegen(), "Try cast from Boolean or Null");
                auto int_double_label = enc->CreateLabel();
                enc->EncodeShr(reg_tmp, src, Imm(coretypes::TaggedValue::TAG_BITS_SHIFT));
                enc->EncodeJump(int_double_label, reg_tmp, Imm(0), Condition::NE);
                enc->EncodeAnd(dst.As(INT32_TYPE), src, Imm(1));
                enc->EncodeCast(dst.As(FLOAT64_TYPE), true, dst.As(INT32_TYPE), true);
                enc->EncodeJump(end_label);
                enc->BindLabel(int_double_label);
            }
            if ((cati->GetAllowedInputType() & profiling::AnyInputType::INTEGER) != 0) {
                SCOPED_DISASM_STR(enc_v->GetCodegen(), "Try cast from SMI");
                auto not_int_label = enc->CreateLabel();
                enc->EncodeShr(reg_tmp, src, Imm(coretypes::TaggedValue::TAG_BITS_SHIFT));
                enc->EncodeJump(not_int_label, reg_tmp,
                                Imm(coretypes::TaggedValue::TAG_INT >> coretypes::TaggedValue::TAG_BITS_SHIFT),
                                Condition::NE);
                enc->EncodeCast(dst.As(FLOAT64_TYPE), true, src.As(INT32_TYPE), true);
                enc->EncodeJump(end_label);
                enc->BindLabel(not_int_label);
            }
            enc->EncodeSub(reg_tmp, src, Imm(panda::coretypes::TaggedValue::DOUBLE_ENCODE_OFFSET));
            enc->BindLabel(return_label);
            enc->EncodeMov(dst, reg_tmp);
            enc->BindLabel(end_label);
            return true;
        }
        case AnyBaseType::ECMASCRIPT_OBJECT_TYPE:
        case AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE:
        case AnyBaseType::ECMASCRIPT_STRING_TYPE:
        case AnyBaseType::ECMASCRIPT_SYMBOL_TYPE:
        case AnyBaseType::ECMASCRIPT_BIGINT_TYPE:
        case AnyBaseType::ECMASCRIPT_ARRAY_TYPE:
        case AnyBaseType::ECMASCRIPT_TRANSITION_HANDLER_TYPE:
        case AnyBaseType::ECMASCRIPT_PROTOTYPE_HANDLER_TYPE:
        case AnyBaseType::ECMASCRIPT_FUNCTION_TYPE:
        case AnyBaseType::ECMASCRIPT_CALLABLE_TYPE:
        case AnyBaseType::ECMASCRIPT_SPECIAL_INDEXED_TYPE: {
            enc->EncodeMov(dst, src);
            return true;
        }
        default:
            return false;
    }

    return false;
}

bool ecmascript::CastValueToAnyTypeGen(const CastValueToAnyTypeInst *cvai, EncodeVisitor *enc_v)
{
    auto src_reg_type = cvai->GetInputType(0);
    src_reg_type = DataType::IsFloatType(src_reg_type) ? DataType::FLOAT64 : src_reg_type;
    auto dst = enc_v->GetCodegen()->ConvertRegister(cvai->GetDstReg(), DataType::INT64);
    auto src = enc_v->GetCodegen()->ConvertRegister(cvai->GetSrcReg(0), src_reg_type);

    auto *enc = enc_v->GetEncoder();
    switch (cvai->GetAnyType()) {
        case AnyBaseType::ECMASCRIPT_NULL_TYPE:
            enc->EncodeMov(dst, Imm(panda::coretypes::TaggedValue::VALUE_NULL));
            return true;
        case AnyBaseType::ECMASCRIPT_HOLE_TYPE:
            enc->EncodeMov(dst, Imm(panda::coretypes::TaggedValue::VALUE_HOLE));
            return true;
        case AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE:
            enc->EncodeMov(dst, Imm(panda::coretypes::TaggedValue::VALUE_UNDEFINED));
            return true;
        case AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE:
            enc->EncodeOr(dst, src, Imm(panda::coretypes::TaggedValue::VALUE_FALSE));
            return true;
        case AnyBaseType::ECMASCRIPT_INT_TYPE: {
            auto src_32 = enc_v->GetCodegen()->ConvertRegister(cvai->GetSrcReg(0), DataType::INT32);
            enc->EncodeCast(dst, false, src_32, false);
            enc->EncodeOr(dst, dst, Imm(panda::coretypes::TaggedValue::TAG_INT));
            return true;
        }
        case AnyBaseType::ECMASCRIPT_DOUBLE_TYPE:
            enc->EncodeMov(dst, src);
            enc->EncodeAdd(dst, dst, Imm(panda::coretypes::TaggedValue::DOUBLE_ENCODE_OFFSET));
            return true;
        case AnyBaseType::ECMASCRIPT_STRING_TYPE:
        case AnyBaseType::ECMASCRIPT_SYMBOL_TYPE:
        case AnyBaseType::ECMASCRIPT_BIGINT_TYPE:
        case AnyBaseType::ECMASCRIPT_OBJECT_TYPE:
        case AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE:
        case AnyBaseType::ECMASCRIPT_ARRAY_TYPE:
        case AnyBaseType::ECMASCRIPT_TRANSITION_HANDLER_TYPE:
        case AnyBaseType::ECMASCRIPT_PROTOTYPE_HANDLER_TYPE:
        case AnyBaseType::ECMASCRIPT_FUNCTION_TYPE:
        case AnyBaseType::ECMASCRIPT_CALLABLE_TYPE:
        case AnyBaseType::ECMASCRIPT_SPECIAL_INDEXED_TYPE:
            // With current boxing scheme, we must guarantee that boxed objects have upper 16 bits set to 0.
            // Boxed values are no less than 64 bits wide (to hold double-precision floating point numbers).
            // As long as pointers are no more than 32 bits wide, a move from a 32-bit source to a 64-bit
            // destination will zero out upper 32 bits of the destination. Otherwise we fallback to the safe
            // path where we explicitly zero-out upper 16 bits by applying a mask.
            if (src.GetSize() <= WORD_SIZE) {
                ASSERT(dst.GetSize() >= DOUBLE_WORD_SIZE);
                enc->EncodeMov(dst, src);
            } else {
                enc->EncodeAnd(dst, src, Imm(panda::coretypes::TaggedValue::OBJECT_MASK));
            }
            return true;
        default:
            return false;
    }

    return false;
}

bool ecmascript::ObjByIndexCheckGen(const FixedInputsInst2 *check_inst, EncodeVisitor *enc_v, LabelHolder::LabelId id)
{
    auto src = enc_v->GetCodegen()->ConvertRegister(check_inst->GetSrcReg(0), DataType::Type::INT64);
    Encoder *enc = enc_v->GetEncoder();
    Codegen *codegen = enc_v->GetCodegen();
    // Check that the value is Heap Object
    CheckAnyTypeGenObject(enc, src, id);

    auto arch = codegen->GetArch();
    ScopedTmpReg property_reg(enc, codegen->ConvertDataType(DataType::INT64, arch));
    ScopedTmpReg tmp_reg(enc, codegen->ConvertDataType(DataType::REFERENCE, arch));

    enc->EncodeLdr(tmp_reg, false, MemRef(src, codegen->GetRuntime()->GetObjClassOffset(arch)));
    enc->EncodeLdr(property_reg, false, MemRef(tmp_reg, cross_values::GetHclassDataOffset(arch)));
    auto type_start_bit = cross_values::GetJshclassBitfieldTypeStartBit(arch);
    if (type_start_bit != 0) {
        enc->EncodeShr(property_reg, property_reg, Imm(type_start_bit));
    }
    // Check that te object isn't special
    enc->EncodeAnd(tmp_reg, property_reg, Imm(static_cast<uint64_t>(cross_values::GetJshclassBitfieldTypeMask(arch))));
    enc->EncodeJump(id, tmp_reg, Imm(cross_values::GetJstypeJsArray(codegen->GetArch())), Condition::GT);
    auto bit_offset = static_cast<size_t>(cross_values::GetJshclassBitfieldIsDictionaryStartBit(codegen->GetArch()));
    // Check that the object isn't dictionary
    enc->EncodeJumpTest(id, tmp_reg, Imm(1U << bit_offset), Condition::TST_NE);
    return true;
}

static bool AnyTypeCheckGenCustomDeoptimization(Encoder *enc, Codegen *codegen, AnyTypeCheckInst *check_inst, Reg src)
{
    ScopedTmpReg tmp_reg(enc, TypeInfo(TypeInfo::TypeId::INT64));
    switch (check_inst->GetAnyType()) {
        case AnyBaseType::ECMASCRIPT_INT_TYPE: {
            auto id =
                codegen->CreateSlowPath<SlowPathDeoptimize>(check_inst, DeoptimizeType::NOT_SMALL_INT)->GetLabel();
            CompareAnyTypeGenInt(enc, tmp_reg, src, check_inst->GetAllowedInputType(), id);
            return true;
        }
        case AnyBaseType::ECMASCRIPT_DOUBLE_TYPE: {
            auto not_number =
                codegen->CreateSlowPath<SlowPathDeoptimize>(check_inst, DeoptimizeType::NOT_NUMBER)->GetLabel();
            auto double_with_int =
                codegen->CreateSlowPath<SlowPathDeoptimize>(check_inst, DeoptimizeType::DOUBLE_WITH_INT)->GetLabel();
            CompareAnyTypeGenDouble(enc, tmp_reg, src, check_inst->GetAllowedInputType(), not_number, double_with_int);
            return true;
        }
        default:
            return false;
    }
}

bool ecmascript::AnyTypeCheckGen(AnyTypeCheckInst *check_inst, EncodeVisitor *enc_v)
{
    auto src = enc_v->GetCodegen()->ConvertRegister(check_inst->GetSrcReg(0), DataType::Type::INT64);
    Encoder *enc = enc_v->GetEncoder();
    Codegen *codegen = enc_v->GetCodegen();

    auto graph = codegen->GetGraph();
    auto custom_deoptimize = graph->IsAotMode() || graph->GetRuntime()->GetMethodProfile(graph->GetMethod(), true) !=
                                                       profiling::INVALID_PROFILE;
    // avoid excessive recompilations for methods which were created while "eval" because such methods are not profiled
    // in current implementation
    if (custom_deoptimize && AnyTypeCheckGenCustomDeoptimization(enc, codegen, check_inst, src)) {
        return true;
    }

    ScopedTmpReg tmp_reg(enc, TypeInfo(TypeInfo::TypeId::INT64));

    auto id = codegen->CreateSlowPath<SlowPathDeoptimize>(check_inst, DeoptimizeType::ANY_TYPE_CHECK)->GetLabel();
    switch (check_inst->GetAnyType()) {
        case AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE:
            IsEqualToValGen(enc, tmp_reg, src, Imm(panda::coretypes::TaggedValue::VALUE_UNDEFINED), id);
            return true;
        case AnyBaseType::ECMASCRIPT_INT_TYPE: {
            CompareAnyTypeGenInt(enc, tmp_reg, src, check_inst->GetAllowedInputType(), id);
            return true;
        }
        case AnyBaseType::ECMASCRIPT_OBJECT_TYPE:
            IsHasTagMaskGen(enc, tmp_reg, src, Imm(panda::coretypes::TaggedValue::TAG_OBJECT),
                            Imm(panda::coretypes::TaggedValue::TAG_MASK), id);
            return true;
        case AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE:
            CheckAnyTypeGenObject(enc, src, id);
            return true;
        case AnyBaseType::ECMASCRIPT_DOUBLE_TYPE:
            CompareAnyTypeGenDouble(enc, tmp_reg, src, check_inst->GetAllowedInputType(), id);
            return true;
        case AnyBaseType::ECMASCRIPT_STRING_TYPE:
            CheckAnyTypeGenObjectType(codegen, enc, src, cross_values::GetJstypeString(codegen->GetArch()), id);
            return true;
        case AnyBaseType::ECMASCRIPT_SYMBOL_TYPE:
            CompareAnyTypeGenObjectType(codegen, enc, tmp_reg, src, cross_values::GetJstypeSymbol(codegen->GetArch()));
            return true;
        case AnyBaseType::ECMASCRIPT_BIGINT_TYPE:
            CheckAnyTypeGenObjectType(codegen, enc, src, cross_values::GetJstypeBigint(codegen->GetArch()), id);
            return true;
        case AnyBaseType::ECMASCRIPT_ARRAY_TYPE:
            CheckAnyTypeGenObjectType(codegen, enc, src, cross_values::GetJstypeJsArray(codegen->GetArch()), id);
            return true;
        case AnyBaseType::ECMASCRIPT_TRANSITION_HANDLER_TYPE:
            CheckAnyTypeGenObjectType(codegen, enc, src, cross_values::GetJstypeTransitionHandler(codegen->GetArch()),
                                      id);
            return true;
        case AnyBaseType::ECMASCRIPT_PROTOTYPE_HANDLER_TYPE:
            CheckAnyTypeGenObjectType(codegen, enc, src, cross_values::GetJstypePrototypeHandler(codegen->GetArch()),
                                      id);
            return true;
        case AnyBaseType::ECMASCRIPT_SPECIAL_INDEXED_TYPE:
            CheckAnyTypeGenObjectType(codegen, enc, src, cross_values::GetJstypeJsArray(codegen->GetArch()), id,
                                      Condition::LE);
            return true;
        case AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE: {
            CompareAnyTypeGenBool(enc, tmp_reg, src, id);
            return true;
        }
        case AnyBaseType::ECMASCRIPT_NULL_TYPE:
            IsEqualToValGen(enc, tmp_reg, src, Imm(panda::coretypes::TaggedValue::VALUE_NULL), id);
            return true;
        case AnyBaseType::ECMASCRIPT_HOLE_TYPE:
            IsEqualToValGen(enc, tmp_reg, src, Imm(panda::coretypes::TaggedValue::VALUE_HOLE), id);
            return true;
        case AnyBaseType::ECMASCRIPT_FUNCTION_TYPE:
            CheckAnyTypeGenObjectType(codegen, enc, src, cross_values::GetJstypeJsFunction(codegen->GetArch()), id);
            return true;
        case AnyBaseType::ECMASCRIPT_CALLABLE_TYPE:
            CompareOrCheckAnyTypeCallable(codegen, enc, INVALID_REGISTER, src, id);
            return true;
        default:
            return false;
    }

    return false;
}

bool ecmascript::LoadConstantPoolGen(const Inst *inst, EncodeVisitor *enc_v)
{
    auto *codegen = enc_v->GetCodegen();
    auto src = codegen->ConvertRegister(inst->GetSrcReg(0), inst->GetType());
    auto dst = codegen->ConvertRegister(inst->GetDstReg(), inst->GetType());
    codegen->GetEncoder()->EncodeLdr(dst, false,
                                     MemRef(src, cross_values::GetJsfunctionConstantPoolOffset(codegen->GetArch())));
    return true;
}

bool ecmascript::LoadLexicalEnvGen(const Inst *inst, EncodeVisitor *enc_v)
{
    auto *codegen = enc_v->GetCodegen();
    auto src = codegen->ConvertRegister(inst->GetSrcReg(0), inst->GetType());
    auto dst = codegen->ConvertRegister(inst->GetDstReg(), inst->GetType());
    codegen->GetEncoder()->EncodeLdr(dst, false,
                                     MemRef(src, cross_values::GetJsfunctionLexicalEnvOffset(codegen->GetArch())));
    return true;
}

bool ecmascript::HclassCheckGen(HclassCheckInst *check_inst, EncodeVisitor *enc_v)
{
    auto enc = enc_v->GetEncoder();
    auto codegen = enc_v->GetCodegen();
    auto arch = codegen->GetArch();
    auto src = codegen->ConvertRegister(check_inst->GetSrcReg(0), DataType::Type::REFERENCE);
    auto exit_label =
        codegen->CreateSlowPath<SlowPathDeoptimize>(check_inst, DeoptimizeType::ANY_TYPE_CHECK)->GetLabel();

    ScopedTmpReg tmp_reg(enc, codegen->ConvertDataType(DataType::REFERENCE, arch));
    auto bit_mask = codegen->GetRuntime()->GetHClassBitfieldTypeMask(arch);
    auto type = codegen->GetRuntime()->GetJstypeJsFunction(arch);

    if (check_inst->GetCheckIsFunction()) {
        enc->EncodeAnd(tmp_reg, src, Imm(bit_mask));
        enc->EncodeJump(exit_label, tmp_reg, Imm(type), Condition::NE);
    }

    if (check_inst->GetCheckFunctionIsNotClassConstructor()) {
        auto constructor_start_bit = codegen->GetRuntime()->GetJshclassBitfieldClassConstructorStartBit(arch);
        enc->EncodeBitTestAndBranch(exit_label, src, constructor_start_bit, true);
    }

    return true;
}

bool ecmascript::IsDynHeapObject(AnyBaseType type)
{
    return type == compiler::AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE;
}

}  // namespace panda::compiler
