/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function testfn() {
    let a = 1;
    function testfn_in() {
        a = 2;
    }
    testfn_in();
    if (a != 2) {
        throw "Wrong result: " + a;
    }
}

function testfn_l0(x) {
    return function(a) { return a + x; }
}

class NumberWrapper {
    constructor(n) {
        this.number = n;
    }
}

function testfn_level0(x) {
    let a = new NumberWrapper(1);
    return function testfn_level1(xx) {
        a = new NumberWrapper(2);
        let b = new NumberWrapper(1 + a.number);
        return function testfn_level2(xxx) {
            a = new NumberWrapper(3);
            let c = new NumberWrapper(1 + a.number);
            return function testfn_level3(xxxx) {
                a = new NumberWrapper(4);
                let d = new NumberWrapper(1 + a.number);
                return function testfn_level4(xxxx) {
                    a = new NumberWrapper(5);
                    let e = new NumberWrapper(1 + a.number);
                    return a.number + b.number + c.number + d.number + e.number;
                }
            }
        }
    }
}

//! CHECKER      Inline StLexDyn/StLexVarDyn intrinsics
//! RUN          options: "--compiler-hotness-threshold=0 --no-async-jit=true --compiler-regex='_GLOBAL::testfn.*'", entry: "_GLOBAL::func_main_0"
//! METHOD       "testfn"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.StLexVarDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.StLexDyn.*/
//! INST_NOT     /Intrinsic.StLexVarDyn.*/
//! METHOD       "testfn_l0"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.StLexVarDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.StLexDyn.*/
//! INST_NOT     /Intrinsic.StLexVarDyn.*/
//! METHOD       "testfn_in"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.StLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.StLexDyn.*/
//! INST_NOT     /Intrinsic.StLexVarDyn.*/
//! METHOD       "testfn_level0"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.StLexVarDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.StLexDyn.*/
//! INST_NOT     /Intrinsic.StLexVarDyn.*/
//! METHOD       "testfn_level1"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.StLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.StLexDyn.*/
//! INST_NOT     /Intrinsic.StLexVarDyn.*/
//! METHOD       "testfn_level2"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.StLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.StLexDyn.*/
//! INST_NOT     /Intrinsic.StLexVarDyn.*/
//! METHOD       "testfn_level3"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.StLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST         /Intrinsic.StLexDyn.*/

testfn();
var a = testfn_l0(3)(2);
if (a != 5) {
    throw "Wrong result of capt: " + a;
}
testfn_level0(1)(2)(3)(4);
