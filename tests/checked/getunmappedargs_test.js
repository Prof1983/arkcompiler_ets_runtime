/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Chech GetUnmappedArgs intrinsic
//! RUN          force_jit: true, options: " --compiler-regex _GLOBAL::foo", entry: "_GLOBAL::func_main_0"
//! METHOD       "foo"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.GetUnmappedArgs.*/

function foo() {
    return arguments[2];
}

if (foo(1, 2, 3, 4, 5) != 3) {
    throw "wrong value";
}