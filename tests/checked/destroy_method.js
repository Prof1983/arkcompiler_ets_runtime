/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER       Test check store and load by index
//! RUN           options: "--no-async-jit --compiler-hotness-threshold=0 --compiler-regex _GLOBAL::destoy_method.*", entry: "_GLOBAL::func_main_0"
//! METHOD        "destoy_method"
//! PASS_AFTER    "Codegen"
//! INST          "AnyTypeCheck ECMASCRIPT_INT_TYPE"
//! INST_NEXT     "Add"
//! EVENT          /Compilation,_GLOBAL::destoy_method,.*,COMPILED/
//! EVENT_NEXT     /DeoptimizationReason,.*destoy_method.*SMALL_INT/
//! EVENT_NEXT     /Compilation,_GLOBAL::destoy_method,.*,COMPILED/
//! EVENT_NEXT_NOT /DeoptimizationReason,.*destoy_method.*SMALL_INT/

function destoy_method(a) {
    return a + 4
}

   
for (let i = 0; i < 10; i++) {
    let a = destoy_method("5.5");
    if (a != "5.54") {
        print(a);
        throw "destoy_method is failed";
    } 
}