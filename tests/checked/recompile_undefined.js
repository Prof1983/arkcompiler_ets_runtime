/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Test deoptimization/recompilation which is caused due to incomplete profiling
//! RUN          options: "--compiler-enable-osr=false --compiler-regex='_GLOBAL::test' --no-async-jit=true --compiler-hotness-threshold=2", entry: "_GLOBAL::func_main_0"
//! EVENT /Compilation,_GLOBAL::test/
//! EVENT_NEXT /Deoptimization,_GLOBAL::func_test_1/
//! EVENT_NEXT /Compilation,_GLOBAL::test/
//! EVENT_NEXT_NOT /Deoptimization,_GLOBAL::func_test_1/
//! METHOD          "_GLOBAL::func_test_1"
//! PASS_AFTER      "IrBuilder"
//! INST            "AnyTypeCheck UNDEFINED"
//! INST_NEXT       "AnyTypeCheck UNDEFINED"
//! PASS_AFTER_NEXT "IrBuilder"
//! INST_NOT        "AnyTypeCheck UNDEFINED"

var d = 1.1;

function test() {
  var s = 0;
  for (let i = 0; i < 10; i += d) {
    for (let j = 0; j < 10; j++) {
      s += i;
    }
  }
  return s;
}

test();
// test was compiled but profiling was not completed
// deduced type is int, but actual is double - deoptimize and recompile
test();
test();
