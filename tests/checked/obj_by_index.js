/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Test check store and load by index
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=10 --compiler-regex _GLOBAL::test_load_store_by_index", entry: "_GLOBAL::func_main_0"
//! METHOD       "test_load_store_by_index"
//! PASS_AFTER   "IrBuilder"
//! INST_COUNT    "LoadObject", 2
//! INST_COUNT    "BoundsCheck", 2
//! INST_COUNT    "ObjByIndexCheck", 2
//! INST_COUNT    "LoadArray", 2
//! EVENT         /Compilation,_GLOBAL::test_load_store_by_index,.*,COMPILED/
//! EVENT_NEXT    /DeoptimizationReason,.*test_load_store_by_index.*BOUNDS_CHECK/
//! EVENT_NEXT    /DeoptimizationReason,.*test_load_store_by_index.*HOLE/
//! EVENT_NEXT    /DeoptimizationReason,.*test_load_store_by_index.*ANY_TYPE_CHECK/

function test_load_store_by_index(a) {
    a[9] = 6
    return a[8]
}

var arr = Array(10)
for (let i = 0 ; i < 10; i++) {
    arr[i] = 5;
}

for (let i = 0 ; i < 1000; i++) {
    if (test_load_store_by_index(arr) != 5) {
        throw "test_load_store_by_index is failed - incorrect return value";
    }
    if (arr[9] != 6) {
        throw "test_load_store_by_index is failed - incorrect load value";
    }
}
var arr1 = Array(5)
for (let i = 0 ; i < 5; i++) {
    arr1[i] = 4;
}
test_load_store_by_index(arr1)

var arr2 = Array(20)
arr2[20] = 4;
test_load_store_by_index(arr2)
var get_type_error = false;
try {
    test_load_store_by_index(4)
} catch (e) {
    if (e instanceof TypeError) {
        get_type_error = true;
    }
}
if (!get_type_error) {
    throw "don't catch type error";
}



