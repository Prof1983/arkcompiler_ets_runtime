/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER         Test hclass check non callable function
//! RUN             options: "--compiler-regex='_GLOBAL::main' --compiler-enable-jit=true --compiler-inlining=true --no-async-jit=true --compiler-hotness-threshold=0", entry: "_GLOBAL::func_main_0", result: 1
//! EVENT           /Compilation,_GLOBAL::main,.*,COMPILED/
//! EVENT           /DeoptimizationReason,_GLOBAL::func_main_0,ANY_TYPE_CHECK/
//! METHOD          "main"
//! PASS_BEFORE     "ChecksElimination"
//! INST            "AnyTypeCheck ECMASCRIPT_HEAP_OBJECT_TYPE"
//! INST            "HclassCheck [IsFunc, IsNotClassConstr]"
//!
//! PASS_AFTER      "ChecksElimination"
//! INST            "AnyTypeCheck ECMASCRIPT_HEAP_OBJECT_TYPE"
//! INST            "HclassCheck [IsFunc, IsNotClassConstr]"


function foo() {}

let a = foo;
for (let i = 0; i < 2; i++) {
    a();
    a = 1;
    // This is necessary so that at the first iteration of the loop it is known
    // that the object was a function, and after that it is not
}
