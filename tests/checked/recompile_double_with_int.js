/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Test deoptimization/recompilation starting with double_with_int deoptimization
//! RUN          options: "--compiler-regex='_GLOBAL::sum' --no-async-jit=true --compiler-hotness-threshold=2", entry: "_GLOBAL::func_main_0"
//! EVENT /Compilation,_GLOBAL::sum/
//! EVENT_NEXT /Deoptimization,_GLOBAL::func_sum_1/
//! EVENT_NEXT /Compilation,_GLOBAL::sum/
//! EVENT_NEXT /Deoptimization,_GLOBAL::func_sum_1/
//! EVENT_NEXT_NOT /Deoptimization,_GLOBAL::func_sum_1/
//! EVENT_NEXT_NOT /Compilation,_GLOBAL::sum/
//! METHOD     "_GLOBAL::func_sum_1"
//! PASS_AFTER      "IrBuilder"
//! INST            "AnyTypeCheck ECMASCRIPT_DOUBLE_TYPE"
//! INST_NEXT       "AnyTypeCheck ECMASCRIPT_INT_TYPE"
//! PASS_AFTER_NEXT "IrBuilder"
//! INST            "AnyTypeCheck ECMASCRIPT_DOUBLE_TYPE i"
//! INST_NEXT       "AnyTypeCheck ECMASCRIPT_INT_TYPE"

function sum(a, b) {
  return a + b;
}

sum(1.1, 2);
sum(1.1, 3);
sum(1.1, 3);
// sum was compiled
sum(1, 3); // double_with_int deoptimization
sum(1.2, 3);
sum(1.2, 3);
sum(1.2, 3);
// sum was compiled again
sum(null, 3); // not_number deoptimization
sum(1.2, 3);
sum(1, 3);
sum(1.2, 3);
sum(1.2, 3);
