/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Test check TypeOfDyn
//! RUN          options: "--no-async-jit=true --compiler-hotness-threshold=10 --compiler-regex _GLOBAL::test_type.*", entry: "_GLOBAL::func_main_0"
//! METHOD       "test_type_undefined"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_UNDEFINED_TYPE"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_UNDEFINED_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_string"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_STRING_TYPE"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_STRING_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_boolean"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_BOOLEAN_TYPE"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_BOOLEAN_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_int"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_DOUBLE_TYPE i"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_DOUBLE_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_double"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_DOUBLE_TYPE i"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_DOUBLE_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_bigint"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_BIGINT_TYPE"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_BIGINT_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_symbol"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_SYMBOL_TYPE"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_SYMBOL_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_function"
//! PASS_BEFORE  "Scheduler"
//! INST         "AnyTypeCheck ECMASCRIPT_CALLABLE_TYPE"
//! INST_NEXT    "GetAnyTypeName ECMASCRIPT_CALLABLE_TYPE"
//! INST_NOT     "Intrinsic.TypeofDyn"
//! METHOD       "test_type_object"
//! PASS_BEFORE  "Scheduler"
//! INST         "Intrinsic.TypeofDyn"
//! INST_NOT     "AnyTypeCheck ECMASCRIPT_HEAP_OBJECT_TYPE"
//! INST_NOT     "GetAnyTypeName"

//! CHECKER      Test check AOT TypeOfDyn
//! RUN_PAOC     options: "--compiler-regex _GLOBAL::test_type.*"
//! RUN          options: "--compiler-enable-jit=false", entry: "_GLOBAL::func_main_0"

function test_type_undefined(a)
{
    return (typeof a === "undefined");
}

function test_type_string(a)
{
    return (typeof a === "string");
}

function test_type_boolean(a)
{
    return (typeof a === "boolean");
}

function test_type_int(a)
{
    return (typeof a === "number");
}

function test_type_double(a)
{
    return (typeof a === "number");
}

function test_type_bigint(a)
{
    return (typeof a === "bigint");
}

function test_type_symbol(a)
{
    return (typeof a === "symbol");
}

function test_type_function(a)
{
    return (typeof a === "function");
}

function test_type_object(a)
{
    return (typeof a === "object");
}
function foo () {}

for (let i = 0 ; i < 30; i++) {
    if (!test_type_undefined(undefined)) {
        throw "test_type_undefined is failed ";
    }
    if (!test_type_string("foo")) {
        throw "test_type_string is failed ";
    }
    if (!test_type_boolean(false)) {
         throw "test_type_boolean false is failed ";
    }
    if (!test_type_boolean(true)) {
        throw "test_type_boolean true is failed ";
    }
    if (!test_type_int(10)) {
        throw "test_type_int is failed ";
    }
    if (!test_type_double(1.1)) {
        throw "test_type_double is failed ";
    }
    if (!test_type_bigint(1n)) {
        throw "test_type_bigint is failed ";
    }
    if (!test_type_symbol(Symbol())) {
        throw "test_type_symbol is failed ";
    }
    if (!test_type_function(foo)) {
        throw "test_type_function is failed ";
    }
    var obj = {};
    if (!test_type_object(obj)) {
        throw "test_type_object is failed ";
    }
    if (!test_type_object(null)) {
        throw "test_type_object null is failed ";
    }
}
