/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function one() {
    return 1;
}

function two() {
    return 2;
}

function three() {
    return 3;
}

function four() {
    try {
        return 4;
    } catch {
        return -1;
    }
}

function five() {
    return 5;
}

function test(func) {
    return func();
}

var  funcs = [one, two, three, four, five];

function test_inlining(N, except_res) {
    var sum = 0;
    for (var i = 0; i < 20; i++) {
        sum += test(funcs[i % N]);
    }
    if (sum != except_res) {
        throw "Wrong result for N = " + N + ". Result = " + sum + ", excpected = " + except_res;
    }
}

//! CHECKER      Ecma Inlining. Must inline, polymorphic call.
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=10 --compiler-regex _GLOBAL::test ", entry: "_GLOBAL::func_main_0"
//! EVENT        /Inline,_GLOBAL::test,_GLOBAL::one,.*DYNAMIC_POLYMORPHIC,SUCCESS/
//! EVENT        /Inline,_GLOBAL::test,_GLOBAL::two,.*DYNAMIC_POLYMORPHIC,SUCCESS/
//! EVENT        /Inline,_GLOBAL::test,_GLOBAL::three,.*DYNAMIC_POLYMORPHIC,SUCCESS/
//! EVENT        /Compilation,_GLOBAL::test,.*,COMPILED/
//! EVENT_NOT    /Deoptimization,_GLOBAL::.*test.*,.*,IFRAME/
test_inlining(4, 50)

