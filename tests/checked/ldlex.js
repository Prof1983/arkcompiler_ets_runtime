/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class NumberWrapper {
    constructor(n) {
        this.number = n;
    }
}

function get_level0() {
    return function compile_level0() {
        let a = new NumberWrapper(1);
        return a.number;
    }
}

function get_level1() {
    let a = new NumberWrapper(1);
    return function compile_level1() {
        let b = new NumberWrapper(2);
        return a.number + b.number;
    };
}

function get_level2() {
    let a = new NumberWrapper(1);
    return function level1() {
        let b = new NumberWrapper(2);
        return function compile_level2() {
            let c = new NumberWrapper(3);
            return a.number + b.number + c.number;
        };
    };
}

function get_level3() {
    let a = new NumberWrapper(1);
    return function level1() {
        let b = new NumberWrapper(2);
        return function level2() {
            let c = new NumberWrapper(3);
            return function compile_level3() {
                let d = new NumberWrapper(4);
                return a.number + b.number + c.number + d.number;
            };
        };
    };
}

//! CHECKER      Inline LdLexDyn/LdLexVarDyn intrinsics
//! RUN          options: "--compiler-hotness-threshold=0 --no-async-jit=true --compiler-regex='_GLOBAL::compile_.*'", entry: "_GLOBAL::func_main_0"
//! METHOD       "compile_level0"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.LdLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.LdLexDyn.*/
//! INST_NOT     /Intrinsic.LdLexVarDyn.*/
//! METHOD       "compile_level1"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.LdLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.LdLexDyn.*/
//! INST_NOT     /Intrinsic.LdLexVarDyn.*/
//! METHOD       "compile_level2"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.LdLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.LdLexDyn.*/
//! INST_NOT     /Intrinsic.LdLexVarDyn.*/
//! METHOD       "compile_level3"
//! PASS_AFTER   "IrBuilder"
//! INST         /Intrinsic.LdLexDyn.*/
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.LdLexDyn.*/
//! INST         /Intrinsic.LdLexVarDyn.*/
get_level0()();
get_level1()();
get_level2()()();
get_level3()()()();
