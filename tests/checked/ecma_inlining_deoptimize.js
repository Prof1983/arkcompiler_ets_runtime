/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


//! CHECKER      Ecma Inlining. Must deoptimize.
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=10 --compiler-regex _GLOBAL::test ", entry: "_GLOBAL::func_main_0"
//! EVENT        /Inline,_GLOBAL::test,_GLOBAL::one,.*DYNAMIC_MONOMORPHIC,SUCCESS/
//! EVENT        /Compilation,_GLOBAL::test,.*,COMPILED/
//! EVENT        /Deoptimization,_GLOBAL::.*tes.*,.*,IFRAME/
function one() {
    return 1;
}

function two() {
    return 2;
}

function test(i) {
    var a = one;
    if (i >= 10) 
        a = two;
    return a();
}

var sum = 0;
for (var i = 0; i < 20; i++) {
    sum += test(i);
}
if (sum != 30) {
    throw "Wrong result: " + sum;
}