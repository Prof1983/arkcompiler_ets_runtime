/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      String equals with profiled type - call fastpath EcmaStringEquals
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=1 --compiler-ecma-replace-intrinsics-to-deopt=false --compiler-regex _GLOBAL::test_.*", entry: "_GLOBAL::func_main_0"
//! EVENT_NOT    /Deoptimization.*/
//! METHOD       "test_eq"
//! PASS_BEFORE  "InlineIntrinsics"
//! INST         "Intrinsic.EqDyn"
//! INST_NEXT    "Intrinsic.NotEqDyn"
//! INST_NEXT    "Intrinsic.StrictEqDyn"
//! INST_NEXT    "Intrinsic.StrictNotEqDyn"
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Intrinsic.*EqDyn/
//! INST_COUNT   "Intrinsic.EcmaStringEquals", 4
//! PASS_AFTER   "Codegen"
//! INST_COUNT   "Intrinsic.EcmaStringEquals", 1

//! CHECKER      String equals without profiled type - call fastpath StrictEq/StrictNotEq
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=0 --compiler-ecma-replace-intrinsics-to-deopt=false --compiler-regex _GLOBAL::test_.*", entry: "_GLOBAL::func_main_0"
//! METHOD       "test_eq"
//! PASS_AFTER   "Codegen"
//! INST_NOT     "Intrinsic.EcmaStringEquals"
//! INST_COUNT   /Intrinsic.*EqDyn/, 4

//! CHECKER      String equals in interpreter
//! RUN          options: "--interpreter-type irtoc --compiler-enable-jit=false", entry: "_GLOBAL::func_main_0"


function test_eq(a, b) {
    if (!(a == b) || a != b || !(a === b) || a !== b) {
        print(1);
        throw "strings should be equal: " + a + " " + b;
    }
}

function test_ne(a, b) {
    if (!(a != b) || a == b || !(a !== b) || a === b) {
        throw "strings should be not equal: " + a + " " + b;
    }
}

test_eq("", "");
test_ne("a", "b");
let u1 = String.fromCharCode(1000)
let u2 = String.fromCharCode(1001)
test_eq(u1, String.fromCharCode(1000));
test_ne(u1, u2);
print('a');

for (let len of [1, 2, 3, 4, 7, 8, 9, 16, 20, 32, 63, 64, 65, 128]) {
    let a = 'a'.repeat(len);
    let b = 'a'.repeat(len - 1) + 'b';
    let c = 'b' + 'a'.repeat(len - 1);
    let l = Math.floor(len / 2);
    let d = 'a'.repeat(l) + 'b' + 'a'.repeat(len - l - 1);
    test_ne(a, a + 'a');
    test_ne(a, b);
    test_ne(a, c);
    test_ne(a, d);
    test_ne(a + u1, a + u2);
    test_eq(a + u1, a + u1);
    test_eq(a + 'b', 'a' + b);
}
