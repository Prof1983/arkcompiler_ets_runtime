/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Test accumulator value after deoptimization
//! RUN          options: "--interpreter-type=irtoc --compiler-profiling-threshold=100001 --compiler-regex='_GLOBAL::test_acc'", entry: "_GLOBAL::func_main_0"
//! EVENT        /Compilation,_GLOBAL::test_acc/
//! EVENT        /Deoptimization,_GLOBAL::func_test_acc_1/

function test_acc() {
    var a = new Number(42);
    if (a != 42) {
        throw "invalid value: " + a
    }
}

for (var i = 0; i < 100000; ++i) {
    test_acc()
}
