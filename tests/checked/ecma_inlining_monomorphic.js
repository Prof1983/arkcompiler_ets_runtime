/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Ecma Inlining. Must inline, monomorphic call.
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=10 --compiler-regex _GLOBAL::test", entry: "_GLOBAL::func_main_0"
//! EVENT        /Inline,_GLOBAL::test,_GLOBAL::one,.*DYNAMIC_MONOMORPHIC,SUCCESS/
//! EVENT        /Compilation,_GLOBAL::test,.*,COMPILED/
//! EVENT_NOT    /Deoptimization,_GLOBAL::.*test.*,.*,IFRAME/

//! CHECKER      Remove HclassCheck for inlined ecma method
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=10 --compiler-regex _GLOBAL::test", entry: "_GLOBAL::func_main_0"
//! EVENT        /Inline,_GLOBAL::test,_GLOBAL::one,.*DYNAMIC_MONOMORPHIC,SUCCESS/
//! METHOD       "test"
//! PASS_AFTER   "IrBuilder"
//! INST         "AnyTypeCheck"
//! INST         "HclassCheck"
//! INST         "CallDynamic"
//!
//! PASS_AFTER   "ChecksElimination"
//! INST         "AnyTypeCheck"
//! INST         "HclassCheck"
//! INST_NOT     "CallDynamic"


function one() {
    return 1;
}

function test() {
    return one();
}

var sum = 0;
for (var i = 0; i < 20; i++) {
    sum += test();
}
if (sum != 20) {
    throw "Wrong result: " + sum;
}