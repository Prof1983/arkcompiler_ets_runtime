/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER         Test hclass check with constructor
//! RUN             options: "--compiler-regex='_GLOBAL::main' --compiler-enable-jit=true --no-async-jit=true --compiler-hotness-threshold=0", entry: "_GLOBAL::func_main_0", result: 1
//! EVENT           /Compilation,_GLOBAL::main,.*,COMPILED/
//! EVENT           /DeoptimizationReason,_GLOBAL::func_main_0,ANY_TYPE_CHECK/


class Test {}

for (let i = 0; i < 10; i++) {
    Test();
}
