/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Peephole for NegOverflowAndZeroCheck
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=10", entry: "_GLOBAL::func_main_0"
//! METHOD       "__noinline__test_peephole_success_shl"
//! PASS_AFTER   "InlineIntrinsics"
//! INST         /NegOverflowAndZeroCheck /
//! INST_NOT     /Neg /
//! PASS_BEFORE  "Lowering"
//! INST_NOT     /NegOverflowAndZeroCheck /
//! INST         /Neg /
//! METHOD       "__noinline__test_peephole_success_and"
//! PASS_AFTER   "InlineIntrinsics"
//! INST         /NegOverflowAndZeroCheck /
//! INST_NOT     /Neg /
//! PASS_BEFORE  "Lowering"
//! INST_NOT     /NegOverflowAndZeroCheck /
//! INST         /Neg /
//! METHOD       "__noinline__test_peephole_reject_add"
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     /Neg /
//! INST         /NegOverflowAndZeroCheck /
//! PASS_BEFORE  "Lowering"
//! INST_NOT     /Neg /
//! INST         /NegOverflowAndZeroCheck /

function __noinline__test_peephole_success_shl(a) {
    a = -a;
    a <<= 2;
    return a;
}

function __noinline__test_peephole_success_and(a) {
    a = -a;
    a &= 2;
    return a;
}

function __noinline__test_peephole_reject_add(a) {
    a = -a;
    a += 2;
    return a;
}

var a1 = 10;
for (var i = 0; i < 100; ++i) {
    a1 = __noinline__test_peephole_success_shl(a1);
}

var a2 = 20;
for (var i = 0; i < 100; ++i) {
    a2 = __noinline__test_peephole_success_and(a2);
}

var a3 = 30;
for (var i = 0; i < 100; ++i) {
    a3 = __noinline__test_peephole_reject_add(a3);
}
