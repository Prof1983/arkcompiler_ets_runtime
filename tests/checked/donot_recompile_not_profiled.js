/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Test deoptimization does not cause recompilation for method which was not profiled
//! RUN          options: "--compiler-regex='_GLOBAL::func__1' --no-async-jit=true --compiler-hotness-threshold=2", entry: "_GLOBAL::func_main_0"
//! EVENT          /Compilation,_GLOBAL::func__1/
//! EVENT_NEXT_NOT /Compilation,_GLOBAL::func__1/
//! EVENT_NEXT     /Deoptimization,_GLOBAL::func__1/
//! METHOD         "_GLOBAL::func__1"
//! PASS_AFTER     "IrBuilder"
//! INST           "AnyTypeCheck UNDEFINED_TYPE v"
//! INST_NEXT      "AnyTypeCheck UNDEFINED_TYPE v"
//! INST_NEXT      "Intrinsic.Add2Dyn"
//! PASS_AFTER     "Codegen"
//! INST           "AddOverflowCheck"

function Test() {
}

eval("Test.prototype.func = function (a) { return a + 2 }");

var test = new Test();
test.func(1);
test.func(3);
test.func(4);

test.func(4.2);
