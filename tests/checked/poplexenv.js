/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Inline PopLexenvDyn intrinsic
//! RUN          options: "--compiler-hotness-threshold=0 --no-async-jit=true --compiler-enable-osr=false --compiler-regex='_GLOBAL::level1'", entry: "_GLOBAL::func_main_0"
//! METHOD       "level1"
//! PASS_AFTER   "IrBuilder"
//! INST         "Intrinsic.PopLexenvDyn"
//! INST_NEXT    "Intrinsic.LdLexDyn"
//! PASS_AFTER   "InlineIntrinsics"
//! INST_NOT     "Intrinsic.PopLexenvDyn"
//! INST         "CastAnyTypeValue ECMASCRIPT_ARRAY_TYPE"
//! INST_NEXT    "LoadArray"

function test() {
    let x = 1;
    return function level1() {
        // function definition inside loop is needed for PopLexenv to be created
        for (let i = 0; i < 5; i++) {
            function level2() { return i }
        }
        // LdLexDyn with PopLexenvDyn input
        return x;
    }
}

if (test()() !== 1) {
    throw "Loaded wrong value from environment returned by Poplexenv";
}
