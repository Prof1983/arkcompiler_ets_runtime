/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


//! CHECKER      Clear call profile after deoptimize. Deoptimize happend with PC for DynamicCall.
//! RUN_PAOC     options: ""
//! RUN          options: "--compiler-enable-jit=false", entry: "_GLOBAL::func_main_0"
//! EVENT        /DeoptimizationReason,_GLOBAL::.*test.*,DOUBLE_WITH_INT/
//! EVENT        /Deoptimization,_GLOBAL::.*test.*,.*,CFRAME/

function construct(len) {
    return new Array(len);
}

function test(len) {
    var blockSize = 16;
    var blockCount = Math.ceil(len/blockSize);
    var ciphertext = construct(blockCount);
    var sum = 0;
    for (var b = 0;b < blockCount; b++) {
        sum += (sum * b * len);
    }
    return sum;
}

test(17);