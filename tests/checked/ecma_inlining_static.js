/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! CHECKER      Ecma Static Inlining. Must inline.
//! RUN          options: "--no-async-jit --compiler-hotness-threshold=0", entry: "_GLOBAL::func_main_0"
//! EVENT_NOT    /Inline,_GLOBAL::main,_GLOBAL::global,.*STATIC,SUCCESS/
//! EVENT        /Inline,_GLOBAL::global,_GLOBAL::test_inside_func1,.*STATIC,SUCCESS/
//! EVENT        /Inline,_GLOBAL::global,_GLOBAL::test_inside_func2,.*STATIC,SUCCESS/
//! EVENT        /Inline,_GLOBAL::main,_GLOBAL::test_global_anonimus,.*STATIC,SUCCESS/
//! EVENT        /Inline,_GLOBAL::main,_GLOBAL::arrow_func,.*STATIC,SUCCESS/
//! EVENT        /Compilation,_GLOBAL::main,.*,COMPILED/
//! EVENT_NOT    /Deoptimization,.*,.*,.*/

// we can't statically inline global functions
// because we need to eliminate StoreGlobalVar/LoadGlobalVar befor
function global() {
    let x = 1;
    let test_inside_func1 = function() {
        return x + 10;
    }
    let t1 = test_inside_func1()
    if (t1 != 11) {
        throw new Error("Wrong anonimus inside function result")
    }

    function test_inside_func2() {
        return x + 9;
    }
    let t2 = test_inside_func2()
    if (t2 != 10) {
        throw new Error("Wrong anonimus inside function result")
    }

    return 21 + t1 + t2;
}
if (global() != 42) {
    throw new Error("Wrong named function result")
}


let test_global_anonimus = function() {
    return 42;
}
if (test_global_anonimus() != 42) {
    throw new Error("Wrong anonimus function result")
}


let a = 10;
let arrow_func = (a) => (a + 21)
if (arrow_func(21) != 42) {
    throw new Error("Wrong lambda function result")
}