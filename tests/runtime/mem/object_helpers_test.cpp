/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#include <gtest/gtest.h>

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "runtime/include/runtime.h"
#include "runtime/include/panda_vm.h"
#include "runtime/include/thread_scopes.h"
#include "runtime/mem/object_helpers-inl.h"

namespace panda::ecmascript {

inline std::string Separator()
{
#ifdef _WIN32
    return "\\";
#else
    return "/";
#endif
}

class DynamicObjectHelpersTest : public testing::Test {
public:
    NO_COPY_SEMANTIC(DynamicObjectHelpersTest);
    NO_MOVE_SEMANTIC(DynamicObjectHelpersTest);

    DynamicObjectHelpersTest()
    {
        RuntimeOptions options;
        options.SetLoadRuntimes({"ecmascript"});
        options.SetGcType("epsilon");
        options.SetGcTriggerType("debug-never");
        auto exec_path = panda::os::file::File::GetExecutablePath();
        std::string panda_std_lib =
            exec_path.Value() + Separator() + ".." + Separator() + "pandastdlib" + Separator() + "pandastdlib.bin";
        options.SetBootPandaFiles({panda_std_lib});

        Runtime::Create(options);

        vm_ = EcmaVM::Cast(Runtime::GetCurrent()->GetPandaVM());
        thread_ = vm_->GetAssociatedJSThread();
        factory_ = vm_->GetFactory();
        handle_scope_ = new EcmaHandleScope(thread_);
        vm_->GetMutatorLock()->ReadLock();
        initial_dyn_class_ = factory_->NewEcmaDynClassClass(nullptr, JSHClass::SIZE, JSType::HCLASS);
        JSHClass *dynclass = reinterpret_cast<JSHClass *>(initial_dyn_class_.GetTaggedValue().GetTaggedObject());
        dynclass->SetClass(dynclass);
        dynclass->GetHClass()->SetNativeFieldMask(JSHClass::NATIVE_FIELDS_MASK);
    }

    ~DynamicObjectHelpersTest() override
    {
        vm_->GetMutatorLock()->Unlock();
        delete handle_scope_;
        Runtime::Destroy();
    }

    JSHandle<JSHClass> NewDynClass()
    {
        return factory_->CreateDynClass<JSHClass>(*initial_dyn_class_, JSType::HCLASS, HClass::HCLASS);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    ObjectFactory *factory_ {};

private:
    EcmaHandleScope *handle_scope_ {};
    EcmaVM *vm_ {};
    JSHandle<JSHClass> initial_dyn_class_;
};

TEST_F(DynamicObjectHelpersTest, TestDynClass)
{
    JSHandle<JSHClass> dyn_class = NewDynClass();
    JSHClass *hclass = *dyn_class;

    JSHandle<JSObject> proto = factory_->NewEmptyJSObject();
    hclass->SetProto(thread_, proto);
    JSHandle<JSObject> attributes = factory_->NewEmptyJSObject();
    hclass->SetLayout(thread_, attributes);
    JSHandle<JSObject> transitions = factory_->NewEmptyJSObject();
    hclass->SetTransitions(thread_, transitions);
    JSHandle<JSObject> parent = factory_->NewEmptyJSObject();
    hclass->SetParent(thread_, parent);
    JSHandle<JSObject> proto_changed_cell = factory_->NewEmptyJSObject();
    hclass->SetProtoChangeMarker(thread_, proto_changed_cell);
    JSHandle<JSObject> proto_change_details = factory_->NewEmptyJSObject();
    hclass->SetProtoChangeDetails(thread_, proto_change_details);
    JSHandle<JSObject> enum_cache = factory_->NewEmptyJSObject();
    hclass->SetEnumCache(thread_, enum_cache);

    PandaVector<std::pair<ObjectHeader *, size_t>> objects_seen = {
        {proto.GetObject<ObjectHeader>(), JSHClass::GetProtoOffset()},
        {attributes.GetObject<ObjectHeader>(), JSHClass::GetLayoutOffset()},
        {transitions.GetObject<ObjectHeader>(), JSHClass::GetTransitionsOffset()},
        {parent.GetObject<ObjectHeader>(), JSHClass::GetParentOffset()},
        {proto_changed_cell.GetObject<ObjectHeader>(), JSHClass::GetProtoChangeMarkerOffset()},
        {proto_change_details.GetObject<ObjectHeader>(), JSHClass::GetProtoChangeDetailsOffset()},
        {enum_cache.GetObject<ObjectHeader>(), JSHClass::GetEnumCacheOffset()}};
    auto handler = [&objects_seen]([[maybe_unused]] ObjectHeader *obj, ObjectHeader *field, uint32_t offset,
                                   [[maybe_unused]] bool is_volatile) {
        auto it =
            std::find_if(objects_seen.begin(), objects_seen.end(),
                         [field](const std::pair<ObjectHeader *, size_t> &entry) { return entry.first == field; });
        EXPECT_NE(objects_seen.end(), it);
        if (it != objects_seen.end()) {
            EXPECT_NE(nullptr, it->first);
            EXPECT_EQ(it->second, offset);
            it->first = nullptr;
        }
        return true;
    };
    mem::GCDynamicObjectHelpers::TraverseAllObjectsWithInfo<false>(dyn_class.GetObject<ObjectHeader>(), handler);
    size_t count = 0;
    for (auto entry : objects_seen) {
        ASSERT_EQ(nullptr, entry.first) << "Object " << count << " was not seen";
        ++count;
    }
}

TEST_F(DynamicObjectHelpersTest, TestDynObject)
{
    JSHandle<JSObject> object = factory_->NewEmptyJSObject();
    JSHandle<EcmaString> key = factory_->NewFromStdString("key");
    JSHandle<JSObject> value = factory_->NewEmptyJSObject();
    JSObject::SetProperty(thread_, object, JSHandle<JSTaggedValue>(thread_, key.GetTaggedValue()),
                          JSHandle<JSTaggedValue>(thread_, value.GetTaggedValue()));

    PandaQueue<ObjectHeader *> queue;
    bool value_seen = false;
    auto handler = [&value, &value_seen, &queue]([[maybe_unused]] ObjectHeader *obj, ObjectHeader *field,
                                                 [[maybe_unused]] uint32_t offset, [[maybe_unused]] bool is_volatile) {
        if (field == value.GetObject<ObjectHeader>()) {
            value_seen = true;
        } else {
            queue.push(field);
        }
        return true;
    };
    queue.push(object.GetObject<ObjectHeader>());
    while (!queue.empty()) {
        ObjectHeader *front = queue.front();
        queue.pop();
        mem::GCDynamicObjectHelpers::TraverseAllObjectsWithInfo<false>(front, handler);
    }
    ASSERT_TRUE(value_seen);
}

TEST_F(DynamicObjectHelpersTest, TestDynArray)
{
    JSHandle<JSArray> object = factory_->NewJSArray();
    JSHandle<JSObject> value = factory_->NewEmptyJSObject();
    JSArray::FastSetPropertyByValue(thread_, JSHandle<JSTaggedValue>(thread_, object.GetTaggedValue()), 0,
                                    JSHandle<JSTaggedValue>(thread_, value.GetTaggedValue()));

    PandaQueue<ObjectHeader *> queue;
    bool value_seen = false;
    auto handler = [&value, &value_seen, &queue]([[maybe_unused]] ObjectHeader *obj, ObjectHeader *field,
                                                 [[maybe_unused]] uint32_t offset, [[maybe_unused]] bool is_volatile) {
        if (field == value.GetObject<ObjectHeader>()) {
            value_seen = true;
        } else {
            queue.push(field);
        }
        return true;
    };
    queue.push(object.GetObject<ObjectHeader>());
    while (!queue.empty()) {
        ObjectHeader *front = queue.front();
        queue.pop();
        mem::GCDynamicObjectHelpers::TraverseAllObjectsWithInfo<false>(front, handler);
    }
    ASSERT_TRUE(value_seen);
}

}  // namespace panda::ecmascript
