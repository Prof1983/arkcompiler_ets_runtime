let res = 0;
let expected = 5050;

// lessdyn, add2dyn, incdyn
for (let i = 0; i < 101; i++) {
    res += i;
}

if (res != expected)
    throw "res = " + res + ", expected = " + expected;

res = 0;
expected = 5005;

// greatereqdyn, add2dyn, decdyn
for (let i = 100; i >= 10; i--) {
    res += i;
}

if (res != expected)
    throw "res = " + res + ", expected = " + expected;

res = 5050;
expected = 0;

// sub2dyn, lesseqdyn
for (let i = 0; i <= 100; i++) {
    res -= i;
}

if (res != expected)
    throw "res = " + res + ", expected = " + expected;

res = 0;
expected = 9;

// mod2dyn
for (let i = 100; i > 10; i--) {
    if (i % 11 == 0) {
        res++;
    }
}

if (res != expected)
    throw "res = " + res + ", expected = " + expected;

res = 1;
expected = 479001600;

// mul2dyn
for (let i = 1; i < 13; i++) {
    res = res * i;
}

if (res != expected)
    throw "res = " + res + ", expected = " + expected;


let left = 0;
let right = false;

if (left === right) {
    throw "left = " + left + ", right = " + right;
}

if (left != right) {
    throw "left = " + left + ", right = " + right;
}

left = '0'

if (left != right) {
    throw "left = " + left + ", right = " + right;
}

// ldundefined, ldnull
left = undefined
right = null

if (left != right) {
    throw "left = " + left + ", right = " + right;
}

left = undefined
right = undefined

// strictnoteq
if (left !== right) {
    throw "left = " + left + ", right = " + right;
}

left = 100
right = 10

// div2dyn
res = left / right
expected = 10

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

left = 1
right = 2

// div2dyn
res = left / right
expected = 0.5

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

left = true
right = true

if (left !== right) {
    throw "left = " + left + ", right = " + right;
}

left = 3
right = 4
expected = 81

// expdyn
res = left ** right

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

// shl2dyn, shr2dyn
res = left << right
expected = 48

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

res = left >> right
expected = 0

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

res = 100 >> 3
expected = 12

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

// ldlexenvdyn, ldlexvardyn 
function capt(x) {
    return function(a) { return a + x;};
}

res = capt(1)(2);
expected = 3;

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

// ecma.negate
let x = 0;
res = !x;
expected = true;

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

x = 10;
res = !x;
expected = false;

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

x = 0.1;
res = !x;
expected = false;

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

// ecma.throwdyn and exception handling
function foo() {
    try {
        throw 1;
    } catch(e) {
        return String("foo1 caught" + e);
    }
}
res = foo();
expected = "foo1 caught1";

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}

function bar1() {
    try {
        function bar2() {
            throw 1;
        }
        bar2();
    } catch(e) {
        return String("bar1 caught" + e);
    }
}
res = bar1();
expected = "bar1 caught1";

if (res != expected) {
    throw "res = " + res + ", expected = " + expected;
}