function(ecmascript_irtoc_interpreter_tests)
    set(prefix ARG)
    set(noValues)
    set(singleValues TARGET TEST_NAME)
    cmake_parse_arguments(${prefix}
                      "${noValues}"
                      "${singleValues}"
                      "${multiValues}"
                      ${ARGN})
    set(TEST_OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${ARG_TEST_NAME}.txt)
    set(TEST_BIN ${CMAKE_CURRENT_BINARY_DIR}/${ARG_TEST_NAME}.abc)
    set(TEST_PA ${CMAKE_CURRENT_BINARY_DIR}/${ARG_TEST_NAME}.pa)
    set(TEST_JS ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_TEST_NAME}.js)
    set(RUNTIME_ARGUMENTS --interpreter-type=irtoc --load-runtimes=\"ecmascript\" --gc-type=g1-gc --run-gc-in-place ${TEST_BIN} _GLOBAL::func_main_0)
    add_custom_command(
        OUTPUT ${TEST_OUTPUT}
        DEPENDS ${TEST_JS}
        COMMENT "running irtoc ${TEST_NAME} testcase"
        COMMAND ${PANDA_RUN_PREFIX} $<TARGET_FILE:es2panda> ${TEST_JS} --dump-assembly --output ${TEST_BIN} > ${TEST_PA}
        COMMAND rm -f ${TEST_OUTPUT}
        COMMAND ${PANDA_RUN_PREFIX} $<TARGET_FILE:ark> ${RUNTIME_ARGUMENTS} > ${TEST_OUTPUT} 2>&1 || (cat ${TEST_OUTPUT} && false)
    )
    add_custom_target(${ARG_TARGET} DEPENDS ${TEST_OUTPUT})
    add_dependencies(${ARG_TARGET} es2panda ark)
    add_dependencies(ecmascript_tests ${ARG_TARGET})
endfunction()

add_subdirectory(basic)
add_subdirectory(advanced)
