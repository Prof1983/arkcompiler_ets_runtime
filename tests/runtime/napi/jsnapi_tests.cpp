/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

#include <cstddef>

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/napi/include/jsnapi.h"
#include "plugins/ecmascript/runtime/napi/jsnapi_helper-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"

#include "libpandabase/os/thread.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

// NOLINTBEGIN(readability-magic-numbers,modernize-avoid-c-arrays)

namespace panda::test {
class JSNApiTests : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        RuntimeOption option;
        option.SetGcType(RuntimeOption::GcType::STW);
        option.SetLogLevel(RuntimeOption::LogLevel::ERROR);
        vm_ = JSNApi::CreateJSVM(option);
        ASSERT_TRUE(vm_ != nullptr) << "Cannot create Runtime";
        thread_ = vm_->GetAssociatedJSThread();
        vm_->GetFactory()->SetTriggerGc(true);
    }

    void TearDown() override
    {
        vm_->GetFactory()->SetTriggerGc(false);
        JSNApi::DestroyJSVM(vm_);
    }

    Local<ObjectRef> NewObjectByConstructor(Local<FunctionRef> ctor)
    {
        ScopedManagedCodeThread scope(thread_);
        auto factory = vm_->GetFactory();
        JSHandle<JSTaggedValue> ctor_handle = JSNApiHelper::ToJSHandle(ctor);
        JSHandle<JSObject> obj =
            factory->NewJSObjectByConstructor(JSHandle<JSFunction>::Cast(ctor_handle), ctor_handle);
        return JSNApiHelper::ToLocal<ObjectRef>(JSHandle<JSTaggedValue>(obj));
    }

    Local<FunctionRef> NewFunctionByHClass(JSHandle<JSHClass> hclass, JSMethod *method)
    {
        ScopedManagedCodeThread scope(thread_);
        auto factory = vm_->GetFactory();
        JSHandle<JSFunction> proto_func = factory->NewJSFunctionByDynClass(method, hclass);
        return JSNApiHelper::ToLocal<FunctionRef>(JSHandle<JSTaggedValue>(proto_func));
    }

    Local<JSValueRef> GetMethod(Local<ObjectRef> obj, Local<ObjectRef> key)
    {
        JSHandle<JSTaggedValue> val =
            JSObject::GetMethod(thread_, JSNApiHelper::ToJSHandle(obj), JSNApiHelper::ToJSHandle(key));
        return JSNApiHelper::ToLocal<JSValueRef>(val);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ = nullptr;
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    EcmaVM *vm_ = nullptr;
};

Local<JSValueRef> FunctionCallback(EcmaVM *vm, Local<JSValueRef> /*unused*/, const Local<JSValueRef> * /*unused*/,
                                   int32_t length, void * /*unused*/)
{
    EscapeLocalScope scope(vm);
    return scope.Escape(ArrayRef::New(vm, length));
}

void ThreadCheck(const EcmaVM *vm)
{
    EXPECT_TRUE(vm->GetAssociatedJSThread()->GetId() != os::thread::GetCurrentThreadId());
}

TEST_F(JSNApiTests, GetGlobalObject)
{
    LocalScope scope(vm_);
    Local<ObjectRef> global_object = JSNApi::GetGlobalObject(vm_);
    ASSERT_FALSE(global_object.IsEmpty());
    ASSERT_TRUE(global_object->IsObject());
}

TEST_F(JSNApiTests, ThreadIdCheck)
{
    EXPECT_TRUE(vm_->GetAssociatedJSThread()->GetId() == os::thread::GetCurrentThreadId());
    std::thread test_thread(ThreadCheck, vm_);
    test_thread.join();
}

TEST_F(JSNApiTests, RegisterFunction)
{
    LocalScope scope(vm_);
    Local<FunctionRef> callback = FunctionRef::New(vm_, FunctionCallback, nullptr);
    ASSERT_TRUE(!callback.IsEmpty());
    std::vector<Local<JSValueRef>> arguments;
    arguments.emplace_back(JSValueRef::Undefined(vm_));
    Local<JSValueRef> result = callback->Call(vm_, JSValueRef::Undefined(vm_), arguments.data(), arguments.size());
    ASSERT_TRUE(result->IsArray(vm_));
    Local<ArrayRef> array(result);
    ASSERT_EQ(array->Length(vm_), arguments.size());
}

TEST_F(JSNApiTests, GetProperty)
{
    LocalScope scope(vm_);
    Local<ObjectRef> global_object = JSNApi::GetGlobalObject(vm_);
    ASSERT_FALSE(global_object.IsEmpty());
    ASSERT_TRUE(global_object->IsObject());

    Local<ObjectRef> key = StringRef::NewFromUtf8(vm_, "Number");
    Local<ObjectRef> property = global_object->Get(vm_, key);
    ASSERT_TRUE(property->IsFunction());
}

TEST_F(JSNApiTests, SetProperty)
{
    LocalScope scope(vm_);
    Local<ObjectRef> global_object = JSNApi::GetGlobalObject(vm_);
    ASSERT_FALSE(global_object.IsEmpty());
    ASSERT_TRUE(global_object->IsObject());

    Local<ArrayRef> property = ArrayRef::New(vm_, 3);  // 3 : length
    ASSERT_TRUE(property->IsArray(vm_));
    ASSERT_EQ(property->Length(vm_), 3);  // 3 : test case of input

    Local<ObjectRef> key = StringRef::NewFromUtf8(vm_, "Test");
    bool result = global_object->Set(vm_, key, property);
    ASSERT_TRUE(result);

    Local<ObjectRef> property_get = global_object->Get(vm_, key);
    ASSERT_TRUE(property_get->IsArray(vm_));
    ASSERT_EQ(Local<ArrayRef>(property_get)->Length(vm_), 3);  // 3 : test case of input
}

TEST_F(JSNApiTests, JsonParser)
{
    LocalScope scope(vm_);
    Local<ObjectRef> global_object = JSNApi::GetGlobalObject(vm_);
    ASSERT_FALSE(global_object.IsEmpty());
    ASSERT_TRUE(global_object->IsObject());

    const char *const test {R"({"orientation": "portrait"})"};
    Local<ObjectRef> json_string = StringRef::NewFromUtf8(vm_, test);

    Local<JSValueRef> result = JSON::Parse(vm_, json_string);
    ASSERT_TRUE(result->IsObject());

    Local<ObjectRef> key_string = StringRef::NewFromUtf8(vm_, "orientation");
    Local<JSValueRef> property = Local<ObjectRef>(result)->Get(vm_, key_string);
    ASSERT_TRUE(property->IsString());
}

TEST_F(JSNApiTests, StrictEqual)
{
    LocalScope scope(vm_);
    Local<StringRef> origin = StringRef::NewFromUtf8(vm_, "1");
    Local<StringRef> target1 = StringRef::NewFromUtf8(vm_, "1");
    Local<NumberRef> target = NumberRef::New(vm_, 1);

    ASSERT_FALSE(origin->IsStrictEquals(vm_, target));
    ASSERT_TRUE(origin->IsStrictEquals(vm_, target1));
}

TEST_F(JSNApiTests, InstanceOf)
{
    LocalScope scope(vm_);
    Local<FunctionRef> target = FunctionRef::New(vm_, nullptr, nullptr);
    Local<ArrayRef> origin = ArrayRef::New(vm_, 1);

    ASSERT_FALSE(origin->InstanceOf(vm_, target));
}

TEST_F(JSNApiTests, TypeOf)
{
    LocalScope scope(vm_);
    Local<StringRef> origin = StringRef::NewFromUtf8(vm_, "1");
    Local<StringRef> type_string = origin->Typeof(vm_);
    ASSERT_EQ(type_string->ToString(), "string");

    Local<NumberRef> target = NumberRef::New(vm_, 1);
    type_string = target->Typeof(vm_);
    ASSERT_EQ(type_string->ToString(), "number");
}

TEST_F(JSNApiTests, Symbol)
{
    LocalScope scope(vm_);
    Local<StringRef> description = StringRef::NewFromUtf8(vm_, "test");
    Local<SymbolRef> symbol = SymbolRef::New(vm_, description);

    ASSERT_FALSE(description->IsSymbol());
    ASSERT_TRUE(symbol->IsSymbol());
}

TEST_F(JSNApiTests, StringUtf8_001)
{
    LocalScope scope(vm_);
    std::string test = "Hello world";
    Local<StringRef> test_string = StringRef::NewFromUtf8(vm_, test.c_str());

    EXPECT_TRUE(test_string->Utf8Length() == 12);           // 12 : length of testString("Hello World")
    char buffer[12];                                        // 12 : length of testString
    EXPECT_TRUE(test_string->WriteUtf8(buffer, 12) == 12);  // 12 : length of testString("Hello World")
    std::string res(buffer);
    ASSERT_EQ(res, test);
}

TEST_F(JSNApiTests, StringUtf8_002)
{
    LocalScope scope(vm_);
    std::string test = "年";
    Local<StringRef> test_string = StringRef::NewFromUtf8(vm_, test.c_str());

    EXPECT_TRUE(test_string->Utf8Length() == 4);          // 4 : length of testString("年")
    char buffer[4];                                       // 4 : length of testString
    EXPECT_TRUE(test_string->WriteUtf8(buffer, 4) == 4);  // 4 : length of testString("年")
    std::string res(buffer);
    ASSERT_EQ(res, test);
}

TEST_F(JSNApiTests, ToType)
{
    LocalScope scope(vm_);
    Local<StringRef> to_string = StringRef::NewFromUtf8(vm_, "-123.3");
    Local<JSValueRef> to_value(to_string);

    ASSERT_EQ(to_string->ToNumber(vm_)->Value(), -123.3);  // -123 : test case of input
    ASSERT_EQ(to_string->ToBoolean(vm_)->Value(), true);
    ASSERT_EQ(to_value->ToString(vm_)->ToString(), "-123.3");
    ASSERT_TRUE(to_value->ToObject(vm_)->IsObject());
}

TEST_F(JSNApiTests, TypeValue)
{
    LocalScope scope(vm_);
    Local<StringRef> to_string = StringRef::NewFromUtf8(vm_, "-123");
    Local<JSValueRef> to_value(to_string);

    ASSERT_EQ(to_string->Int32Value(vm_), -123);  // -123 : test case of input
    ASSERT_EQ(to_string->BooleaValue(), true);
    ASSERT_EQ(to_string->Uint32Value(vm_), 4294967173);  // 4294967173 : test case of input
    ASSERT_EQ(to_string->IntegerValue(vm_), -123);       // -123 : test case of input
}

TEST_F(JSNApiTests, DefineProperty)
{
    LocalScope scope(vm_);
    Local<ObjectRef> object = ObjectRef::New(vm_);
    Local<JSValueRef> key = StringRef::NewFromUtf8(vm_, "TestKey");
    Local<JSValueRef> value = ObjectRef::New(vm_);
    PropertyAttribute attribute(value, true, true, true);

    ASSERT_TRUE(object->DefineProperty(vm_, key, attribute));
    Local<JSValueRef> value1 = object->Get(vm_, key);
    ASSERT_TRUE(value->IsStrictEquals(vm_, value1));
}

TEST_F(JSNApiTests, HasProperty)
{
    LocalScope scope(vm_);
    Local<ObjectRef> object = ObjectRef::New(vm_);
    Local<JSValueRef> key = StringRef::NewFromUtf8(vm_, "TestKey");
    Local<JSValueRef> value = ObjectRef::New(vm_);
    PropertyAttribute attribute(value, true, true, true);

    ASSERT_TRUE(object->DefineProperty(vm_, key, attribute));
    ASSERT_TRUE(object->Has(vm_, key));
}

TEST_F(JSNApiTests, DeleteProperty)
{
    LocalScope scope(vm_);
    Local<ObjectRef> object = ObjectRef::New(vm_);
    Local<JSValueRef> key = StringRef::NewFromUtf8(vm_, "TestKey");
    Local<JSValueRef> value = ObjectRef::New(vm_);
    PropertyAttribute attribute(value, true, true, true);

    ASSERT_TRUE(object->DefineProperty(vm_, key, attribute));
    ASSERT_TRUE(object->Delete(vm_, key));
    ASSERT_FALSE(object->Has(vm_, key));
}

TEST_F(JSNApiTests, GetProtoType)
{
    LocalScope scope(vm_);
    Local<FunctionRef> function = FunctionRef::New(vm_, nullptr, nullptr);
    Local<JSValueRef> proto_type = function->GetPrototype(vm_);
    ASSERT_TRUE(proto_type->IsObject());

    Local<FunctionRef> object = ObjectRef::New(vm_);
    proto_type = object->GetPrototype(vm_);
    ASSERT_TRUE(proto_type->IsObject());
}

void CheckReject(EcmaVM * /*unused*/, Local<JSValueRef> /*unused*/, const Local<JSValueRef> argv[], int32_t length,
                 void * /*unused*/)
{
    ASSERT_EQ(length, 1);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    Local<JSValueRef> reason = argv[0];
    ASSERT_TRUE(reason->IsString());
    ASSERT_EQ(Local<StringRef>(reason)->ToString(), "Reject");
}

Local<JSValueRef> RejectCallback(EcmaVM *vm, Local<JSValueRef> this_arg, const Local<JSValueRef> argv[], int32_t length,
                                 void *data)
{
    LocalScope scope(vm);
    CheckReject(vm, this_arg, argv, length, data);
    return JSValueRef::Undefined(vm);
}

TEST_F(JSNApiTests, PromiseCatch)
{
    LocalScope scope(vm_);
    Local<PromiseCapabilityRef> capability = PromiseCapabilityRef::New(vm_);

    Local<PromiseRef> promise = capability->GetPromise(vm_);
    Local<FunctionRef> reject = FunctionRef::New(vm_, RejectCallback, nullptr);
    Local<PromiseRef> catch_promise = promise->Catch(vm_, reject);
    ASSERT_TRUE(promise->IsPromise());
    ASSERT_TRUE(catch_promise->IsPromise());

    Local<StringRef> reason = StringRef::NewFromUtf8(vm_, "Reject");
    ASSERT_TRUE(capability->Reject(vm_, reason));

    JSNApi::ExecutePendingJob(vm_);
}

void CheckResolve(EcmaVM * /*unused*/, Local<JSValueRef> /*unused*/, const Local<JSValueRef> argv[], int32_t length,
                  void * /*unused*/)
{
    ASSERT_EQ(length, 1);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    Local<JSValueRef> value = argv[0];
    ASSERT_TRUE(value->IsNumber());
    ASSERT_EQ(Local<NumberRef>(value)->Value(), 300.3);  // 300.3 : test case of input
}

Local<JSValueRef> ResolvedCallback(EcmaVM *vm, Local<JSValueRef> this_arg, const Local<JSValueRef> argv[],
                                   int32_t length, void *data)
{
    LocalScope scope(vm);
    CheckResolve(vm, this_arg, argv, length, data);
    return JSValueRef::Undefined(vm);
}

TEST_F(JSNApiTests, PromiseThen)
{
    LocalScope scope(vm_);
    Local<PromiseCapabilityRef> capability = PromiseCapabilityRef::New(vm_);

    Local<PromiseRef> promise = capability->GetPromise(vm_);
    Local<FunctionRef> resolve = FunctionRef::New(vm_, ResolvedCallback, nullptr);
    Local<FunctionRef> reject = FunctionRef::New(vm_, RejectCallback, nullptr);
    Local<PromiseRef> then_promise = promise->Then(vm_, resolve, reject);
    ASSERT_TRUE(promise->IsPromise());
    ASSERT_TRUE(then_promise->IsPromise());

    Local<StringRef> value = NumberRef::New(vm_, 300.3);  // 300.3 : test case of input
    ASSERT_TRUE(capability->Resolve(vm_, value));
    JSNApi::ExecutePendingJob(vm_);
}

TEST_F(JSNApiTests, Constructor)
{
    LocalScope scope(vm_);
    Local<ObjectRef> object = JSNApi::GetGlobalObject(vm_);
    Local<StringRef> key = StringRef::NewFromUtf8(vm_, "Number");
    Local<FunctionRef> number_constructor = object->Get(vm_, key);
    Local<JSValueRef> argv[1];
    argv[0] = NumberRef::New(vm_, 1.3);  // 1.3 : test case of input
    Local<JSValueRef> result = number_constructor->Constructor(vm_, argv, 1);
    ASSERT_TRUE(result->IsObject());
    ASSERT_EQ(result->ToNumber(vm_)->Value(), 1.3);  // 1.3 : size of arguments
}

TEST_F(JSNApiTests, ArrayBuffer)
{
    LocalScope scope(vm_);
    const int32_t length = 15;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());
    ASSERT_EQ(array_buffer->ByteLength(vm_), length);
    ASSERT_NE(array_buffer->GetBuffer(), nullptr);
    JSNApi::TriggerGC(vm_);
}

TEST_F(JSNApiTests, ArrayBufferWithBuffer)
{
    static bool is_free = false;
    struct Data {
        int32_t length;
    };
    const int32_t length = 15;
    Data *data = new Data();
    data->length = length;
    Deleter deleter = [](void *buffer, void *data_ptr) -> void {
        delete[] reinterpret_cast<uint8_t *>(buffer);
        Data *current_data = reinterpret_cast<Data *>(data_ptr);
        ASSERT_EQ(current_data->length, 15);  // 5 : size of arguments
        delete current_data;
        is_free = true;
    };
    {
        LocalScope scope(vm_);
        auto *buffer = new uint8_t[length]();
        Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, buffer, length, deleter, data);
        ASSERT_TRUE(array_buffer->IsArrayBuffer());
        ASSERT_EQ(array_buffer->ByteLength(vm_), length);
        ASSERT_EQ(array_buffer->GetBuffer(), buffer);
    }
    JSNApi::TriggerGC(vm_);
    ASSERT_TRUE(is_free);
}

TEST_F(JSNApiTests, DataView)
{
    LocalScope scope(vm_);
    const int32_t length = 15;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    JSNApi::TriggerGC(vm_);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 5 : offset of byte, 7 : length
    Local<DataViewRef> data_view = DataViewRef::New(vm_, array_buffer, 5, 7);
    ASSERT_TRUE(data_view->IsDataView());
    ASSERT_EQ(data_view->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
    ASSERT_EQ(data_view->ByteLength(), 7);  // 7 : size of arguments
    ASSERT_EQ(data_view->ByteOffset(), 5);  // 5 : size of arguments

    // 5 : offset of byte, 11 : length
    data_view = DataViewRef::New(vm_, array_buffer, 5, 11);
    ASSERT_TRUE(data_view->IsException());
}

TEST_F(JSNApiTests, Int8Array)
{
    LocalScope scope(vm_);
    const int32_t length = 15;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 5 : offset of byte, 6 : length
    Local<Int8ArrayRef> typed_array = Int8ArrayRef::New(vm_, array_buffer, 5, 6);
    ASSERT_TRUE(typed_array->IsInt8Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 6);   // 6 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 5);   // 5 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Uint8Array)
{
    LocalScope scope(vm_);
    const int32_t length = 15;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 5 : offset of byte, 6 : length
    Local<Uint8ArrayRef> typed_array = Uint8ArrayRef::New(vm_, array_buffer, 5, 6);
    ASSERT_TRUE(typed_array->IsUint8Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 6);   // 6 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 5);   // 5 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Uint8ClampedArray)
{
    LocalScope scope(vm_);
    const int32_t length = 15;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 5 : offset of byte, 6 : length
    Local<Uint8ClampedArrayRef> typed_array = Uint8ClampedArrayRef::New(vm_, array_buffer, 5, 6);
    ASSERT_TRUE(typed_array->IsUint8ClampedArray());
    ASSERT_EQ(typed_array->ByteLength(vm_), 6);   // 6 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 5);   // 5 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Int16Array)
{
    LocalScope scope(vm_);
    const int32_t length = 30;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 4 : offset of byte, 6 : length
    Local<Int16ArrayRef> typed_array = Int16ArrayRef::New(vm_, array_buffer, 4, 6);
    ASSERT_TRUE(typed_array->IsInt16Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 12);  // 12 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 4);   // 4 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Uint16Array)
{
    LocalScope scope(vm_);
    const int32_t length = 30;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 4 : offset of byte, 6 : length
    Local<Uint16ArrayRef> typed_array = Uint16ArrayRef::New(vm_, array_buffer, 4, 6);
    ASSERT_TRUE(typed_array->IsUint16Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 12);  // 12 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 4);   // 4 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Uint32Array)
{
    LocalScope scope(vm_);
    const int32_t length = 30;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 4 : offset of byte, 6 : length
    Local<Uint32ArrayRef> typed_array = Uint32ArrayRef::New(vm_, array_buffer, 4, 6);
    ASSERT_TRUE(typed_array->IsUint32Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 24);  // 24 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 4);   // 4 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Int32Array)
{
    LocalScope scope(vm_);
    const int32_t length = 30;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 4 : offset of byte, 6 : length
    Local<Int32ArrayRef> typed_array = Int32ArrayRef::New(vm_, array_buffer, 4, 6);
    ASSERT_TRUE(typed_array->IsInt32Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 24);  // 24 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 4);   // 4 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Float32Array)
{
    LocalScope scope(vm_);
    const int32_t length = 30;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 4 : offset of byte, 6 : length
    Local<Float32ArrayRef> typed_array = Float32ArrayRef::New(vm_, array_buffer, 4, 6);
    ASSERT_TRUE(typed_array->IsFloat32Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 24);  // 24 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 4);   // 4 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Float64Array)
{
    LocalScope scope(vm_);
    const int32_t length = 57;
    Local<ArrayBufferRef> array_buffer = ArrayBufferRef::New(vm_, length);
    ASSERT_TRUE(array_buffer->IsArrayBuffer());

    // 8 : offset of byte, 6 : length
    Local<Float64ArrayRef> typed_array = Float64ArrayRef::New(vm_, array_buffer, 8, 6);
    ASSERT_TRUE(typed_array->IsFloat64Array());
    ASSERT_EQ(typed_array->ByteLength(vm_), 48);  // 48 : length of bytes
    ASSERT_EQ(typed_array->ByteOffset(vm_), 8);   // 8 : offset of byte
    ASSERT_EQ(typed_array->ArrayLength(vm_), 6);  // 6 : length of array
    ASSERT_EQ(typed_array->GetArrayBuffer(vm_)->GetBuffer(), array_buffer->GetBuffer());
}

TEST_F(JSNApiTests, Error)
{
    LocalScope scope(vm_);
    Local<StringRef> message = StringRef::NewFromUtf8(vm_, "ErrorTest");
    Local<JSValueRef> error = Exception::Error(vm_, message);
    ASSERT_TRUE(error->IsError());

    JSNApi::ThrowException(vm_, error);
    ASSERT_TRUE(thread_->HasPendingException());
}

TEST_F(JSNApiTests, RangeError)
{
    LocalScope scope(vm_);
    Local<StringRef> message = StringRef::NewFromUtf8(vm_, "ErrorTest");
    Local<JSValueRef> error = Exception::RangeError(vm_, message);
    ASSERT_TRUE(error->IsError());

    JSNApi::ThrowException(vm_, error);
    ASSERT_TRUE(thread_->HasPendingException());
}

TEST_F(JSNApiTests, TypeError)
{
    LocalScope scope(vm_);
    Local<StringRef> message = StringRef::NewFromUtf8(vm_, "ErrorTest");
    Local<JSValueRef> error = Exception::TypeError(vm_, message);
    ASSERT_TRUE(error->IsError());

    JSNApi::ThrowException(vm_, error);
    ASSERT_TRUE(thread_->HasPendingException());
}

TEST_F(JSNApiTests, ReferenceError)
{
    LocalScope scope(vm_);
    Local<StringRef> message = StringRef::NewFromUtf8(vm_, "ErrorTest");
    Local<JSValueRef> error = Exception::ReferenceError(vm_, message);
    ASSERT_TRUE(error->IsError());

    JSNApi::ThrowException(vm_, error);
    ASSERT_TRUE(thread_->HasPendingException());
}

TEST_F(JSNApiTests, SyntaxError)
{
    LocalScope scope(vm_);
    Local<StringRef> message = StringRef::NewFromUtf8(vm_, "ErrorTest");
    Local<JSValueRef> error = Exception::SyntaxError(vm_, message);
    ASSERT_TRUE(error->IsError());

    JSNApi::ThrowException(vm_, error);
    ASSERT_TRUE(thread_->HasPendingException());
}

TEST_F(JSNApiTests, InheritPrototype_001)
{
    LocalScope scope(vm_);
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    // new with Builtins::Set Prototype
    JSHandle<JSTaggedValue> set = env->GetSetFunction();
    Local<FunctionRef> set_local = JSNApiHelper::ToLocal<FunctionRef>(set);
    // new with Builtins::Map Prototype
    JSHandle<JSTaggedValue> map = env->GetMapFunction();
    Local<FunctionRef> map_local = JSNApiHelper::ToLocal<FunctionRef>(map);
    JSHandle<JSTaggedValue> set_prototype(thread_, JSHandle<JSFunction>::Cast(set)->GetFunctionPrototype());
    JSHandle<JSTaggedValue> map_prototype(thread_, JSHandle<JSFunction>::Cast(map)->GetFunctionPrototype());
    JSHandle<JSTaggedValue> map_prototype_proto(thread_,
                                                JSHandle<JSObject>::Cast(map_prototype)->GetPrototype(thread_));
    bool same = JSTaggedValue::SameValue(set_prototype, map_prototype_proto);
    // before inherit, map.Prototype.__proto__ should be different from set.Prototype
    ASSERT_FALSE(same);
    // before inherit, map.__proto__ should be different from set
    JSHandle<JSTaggedValue> map_proto(thread_, JSHandle<JSObject>::Cast(map)->GetPrototype(thread_));
    bool same1 = JSTaggedValue::SameValue(set, map_proto);
    ASSERT_FALSE(same1);

    // Set property to Set Function
    Local<ObjectRef> default_string =
        JSNApiHelper::ToLocal<ObjectRef>(thread_->GlobalConstants()->GetHandledDefaultString());
    {
        PropertyAttribute attr(default_string, true, true, true);
        ASSERT_TRUE(set_local->DefineProperty(vm_, default_string, attr));
    }
    Local<ObjectRef> func = JSNApiHelper::ToLocal<ObjectRef>(env->GetTypedArrayFunction());
    Local<StringRef> property1_string = StringRef::NewFromUtf8(vm_, "property1");
    {
        PropertyAttribute attr(func, true, true, true);
        ASSERT_TRUE(set_local->DefineProperty(vm_, property1_string, attr));
    }

    map_local->Inherit(vm_, set_local);
    JSHandle<JSTaggedValue> son_handle = JSNApiHelper::ToJSHandle(map_local);
    JSHandle<JSTaggedValue> son_prototype(thread_, JSHandle<JSFunction>::Cast(son_handle)->GetFunctionPrototype());
    JSHandle<JSTaggedValue> son_prototype_proto(thread_,
                                                JSHandle<JSObject>::Cast(son_prototype)->GetPrototype(thread_));
    bool same2 = JSTaggedValue::SameValue(set_prototype, son_prototype_proto);
    ASSERT_TRUE(same2);
    JSHandle<JSTaggedValue> son_proto(thread_, JSHandle<JSObject>::Cast(map)->GetPrototype(thread_));
    bool same3 = JSTaggedValue::SameValue(set, son_proto);
    ASSERT_TRUE(same3);

    // son = new Son(), Son() inherit from Parent(), Test whether son.InstanceOf(Parent) is true
    Local<ObjectRef> son_obj = NewObjectByConstructor(map_local);
    bool is_instance = son_obj->InstanceOf(vm_, set_local);
    ASSERT_TRUE(is_instance);

    // Test whether son Function can access to property of parent Function
    Local<JSValueRef> res = map_local->Get(vm_, default_string);
    ASSERT_TRUE(default_string->IsStrictEquals(vm_, res));
    Local<JSValueRef> res1 = map_local->Get(vm_, property1_string);
    ASSERT_TRUE(func->IsStrictEquals(vm_, res1));

    // new with empty Function Constructor
    Local<FunctionRef> son1 = FunctionRef::New(vm_, FunctionCallback, nullptr);
    son1->Inherit(vm_, map_local);
    JSHandle<JSFunction> son1_handle = JSHandle<JSFunction>::Cast(JSNApiHelper::ToJSHandle(son1));
    ASSERT_TRUE(son1_handle->HasFunctionPrototype());
}

TEST_F(JSNApiTests, InheritPrototype_002)
{
    LocalScope scope(vm_);
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    // new with Builtins::weakSet Prototype
    JSHandle<JSTaggedValue> weak_set = env->GetWeakSetFunction();
    Local<FunctionRef> weak_set_local = JSNApiHelper::ToLocal<FunctionRef>(weak_set);
    // new with Builtins::weakMap Prototype
    JSHandle<JSTaggedValue> weak_map = env->GetWeakMapFunction();
    Local<FunctionRef> weak_map_local = JSNApiHelper::ToLocal<FunctionRef>(weak_map);

    weak_map_local->Inherit(vm_, weak_set_local);

    Local<StringRef> property1_string = StringRef::NewFromUtf8(vm_, "property1");
    Local<ObjectRef> func = JSNApiHelper::ToLocal<ObjectRef>(env->GetArrayFunction());
    {
        PropertyAttribute attr(func, true, true, true);
        ASSERT_TRUE(weak_map_local->DefineProperty(vm_, property1_string, attr));
    }

    Local<ObjectRef> son_obj = NewObjectByConstructor(weak_map_local);

    Local<ObjectRef> father_obj = NewObjectByConstructor(weak_set_local);

    Local<JSValueRef> son_method = GetMethod(son_obj, property1_string);
    Local<JSValueRef> father_method = GetMethod(father_obj, property1_string);
    ASSERT_TRUE(son_method->IsStrictEquals(vm_, father_method));
}

TEST_F(JSNApiTests, InheritPrototype_003)
{
    LocalScope scope(vm_);
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();

    JSMethod *invoke_self =
        vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::function::FunctionPrototypeInvokeSelf));
    // father type
    Local<FunctionRef> proto_local =
        NewFunctionByHClass(JSHandle<JSHClass>::Cast(env->GetFunctionClassWithProto()), invoke_self);
    // son type
    Local<FunctionRef> no_proto_local =
        NewFunctionByHClass(JSHandle<JSHClass>::Cast(env->GetFunctionClassWithoutProto()), invoke_self);

    JSHandle<JSFunction> son_handle = JSHandle<JSFunction>::Cast(JSNApiHelper::ToJSHandle(no_proto_local));
    EXPECT_FALSE(son_handle->HasFunctionPrototype());

    Local<ObjectRef> default_string =
        JSNApiHelper::ToLocal<ObjectRef>(thread_->GlobalConstants()->GetHandledDefaultString());
    {
        PropertyAttribute attr(default_string, true, true, true);
        proto_local->DefineProperty(vm_, default_string, attr);
    }

    no_proto_local->Inherit(vm_, proto_local);
    JSHandle<JSFunction> son1_handle = JSHandle<JSFunction>::Cast(JSNApiHelper::ToJSHandle(no_proto_local));
    EXPECT_TRUE(son1_handle->HasFunctionPrototype());

    Local<JSValueRef> res = no_proto_local->Get(vm_, default_string);
    EXPECT_TRUE(default_string->IsStrictEquals(vm_, res));

    Local<StringRef> property_string = StringRef::NewFromUtf8(vm_, "property");
    Local<ObjectRef> func = JSNApiHelper::ToLocal<ObjectRef>(env->GetArrayFunction());
    {
        PropertyAttribute attr(func, true, true, true);
        proto_local->DefineProperty(vm_, property_string, attr);
    }
    Local<JSValueRef> res1 = no_proto_local->Get(vm_, property_string);
    EXPECT_TRUE(func->IsStrictEquals(vm_, res1));
}

TEST_F(JSNApiTests, DISABLED_InheritPrototype_004)  // TODO(vpukhov)
{
    LocalScope scope(vm_);
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    auto factory = vm_->GetFactory();

    JSHandle<JSTaggedValue> weak_set = env->GetWeakSetFunction();
    JSHandle<JSTaggedValue> delete_string(factory->NewFromCanBeCompressString("delete"));
    JSHandle<JSTaggedValue> add_string(factory->NewFromCanBeCompressString("add"));
    JSHandle<JSTaggedValue> default_string = thread_->GlobalConstants()->GetHandledDefaultString();
    JSHandle<JSTaggedValue> delete_method = JSObject::GetMethod(thread_, weak_set, delete_string);
    JSHandle<JSTaggedValue> add_method = JSObject::GetMethod(thread_, weak_set, add_string);

    JSMethod *invoke_self =
        vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::function::FunctionPrototypeInvokeSelf));
    JSMethod *ctor = vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::function::FunctionConstructor));

    JSHandle<JSHClass> proto_dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithProto());
    JSHandle<JSFunction> func_func_prototype = factory->NewJSFunctionByDynClass(invoke_self, proto_dynclass);
    // add method in funcPrototype
    PropertyDescriptor desc = PropertyDescriptor(thread_, delete_method);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>::Cast(func_func_prototype), delete_string, desc);
    JSHandle<JSTaggedValue> func_func_prototype_value(func_func_prototype);

    JSHandle<JSHClass> func_func_proto_intance_dynclass =
        factory->CreateDynClass<JSFunction>(JSType::JS_FUNCTION, func_func_prototype_value);
    // new with NewJSFunctionByDynClass::function DynClass
    JSHandle<JSFunction> proto_func =
        factory->NewJSFunctionByDynClass(ctor, func_func_proto_intance_dynclass, FunctionKind::BUILTIN_CONSTRUCTOR);
    EXPECT_TRUE(*proto_func != nullptr);
    // add method in funcnction
    PropertyDescriptor desc1 = PropertyDescriptor(thread_, add_method);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>::Cast(proto_func), add_string, desc1);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>::Cast(proto_func), delete_string, desc);
    // father type
    Local<FunctionRef> proto_local = JSNApiHelper::ToLocal<FunctionRef>(JSHandle<JSTaggedValue>(proto_func));

    JSHandle<JSHClass> no_proto_dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithoutProto());
    JSHandle<JSFunction> func_func_no_proto_prototype =
        factory->NewJSFunctionByDynClass(invoke_self, no_proto_dynclass);
    JSHandle<JSTaggedValue> func_func_no_proto_prototype_value(func_func_no_proto_prototype);

    JSHandle<JSHClass> func_func_no_proto_proto_intance_dynclass =
        factory->CreateDynClass<JSFunction>(JSType::JS_FUNCTION, func_func_no_proto_prototype_value);
    // new with NewJSFunctionByDynClass::function DynClass
    JSHandle<JSFunction> no_proto_func = factory->NewJSFunctionByDynClass(
        ctor, func_func_no_proto_proto_intance_dynclass, FunctionKind::BUILTIN_CONSTRUCTOR);
    EXPECT_TRUE(*no_proto_func != nullptr);
    // set property that has same key with fater type
    PropertyDescriptor desc2 = PropertyDescriptor(thread_, default_string);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>::Cast(no_proto_func), add_string, desc2);
    // son type
    Local<FunctionRef> no_proto_local = JSNApiHelper::ToLocal<FunctionRef>(JSHandle<JSTaggedValue>(no_proto_func));

    no_proto_local->Inherit(vm_, proto_local);

    JSHandle<JSFunction> son_handle = JSHandle<JSFunction>::Cast(JSNApiHelper::ToJSHandle(no_proto_local));
    OperationResult res = JSObject::GetProperty(thread_, JSHandle<JSObject>::Cast(son_handle), delete_string);
    EXPECT_EQ(JSTaggedValue::SameValue(delete_method, res.GetValue()), true);
    // test if the property value changed after inherit
    OperationResult res1 = JSObject::GetProperty(thread_, JSHandle<JSObject>::Cast(son_handle), add_string);
    EXPECT_EQ(JSTaggedValue::SameValue(default_string, res1.GetValue()), true);
}

TEST_F(JSNApiTests, ClassFunction)
{
    LocalScope scope(vm_);
    Local<FunctionRef> cls = FunctionRef::NewClassFunction(vm_, nullptr, nullptr, nullptr);

    JSHandle<JSTaggedValue> cls_obj = JSNApiHelper::ToJSHandle(Local<JSValueRef>(cls));
    ASSERT_TRUE(cls_obj->IsClassConstructor());

    JSTaggedValue accessor =
        JSHandle<JSFunction>(cls_obj)->GetPropertyInlinedProps(JSFunction::CLASS_PROTOTYPE_INLINE_PROPERTY_INDEX);
    ASSERT_TRUE(accessor.IsInternalAccessor());
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers,modernize-avoid-c-arrays)
