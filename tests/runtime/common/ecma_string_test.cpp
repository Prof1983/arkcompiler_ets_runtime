/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_string-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

// NOLINTBEGIN(readability-magic-numbers,modernize-avoid-c-arrays)

namespace panda::test {
class EcmaStringTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    PandaVM *instance_ {nullptr};

private:
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

/*
 * @tc.name: SetCompressedStringsEnabled
 * @tc.desc: Check whether the bool returned through calling GetCompressedStringsEnabled function is within
 * expectations after calling SetCompressedStringsEnabled function.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, SetCompressedStringsEnabled)
{
    EXPECT_TRUE(EcmaString::GetCompressedStringsEnabled());
    EcmaString::SetCompressedStringsEnabled(false);
    EXPECT_FALSE(EcmaString::GetCompressedStringsEnabled());
    EcmaString::SetCompressedStringsEnabled(true);
    EXPECT_TRUE(EcmaString::GetCompressedStringsEnabled());
}

/*
 * @tc.name: CanBeCompressed
 * @tc.desc: Check whether the bool returned through calling CanBeCompressed function is within expectations before and
 * after calling SetCompressedStringsEnabled function with false.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CanBeCompressed)
{
    uint8_t array_u8[] = {12, 34, 77, 127, 99, 1};
    uint16_t array_u16_comp[] = {1, 4, 37, 91, 127, 1};
    uint16_t array_u16_not_comp[] = {72, 43, 337, 961, 1317, 65535};
    EXPECT_TRUE(EcmaString::CanBeCompressed(array_u8, sizeof(array_u8) / sizeof(array_u8[0])));
    EXPECT_TRUE(EcmaString::CanBeCompressed(array_u16_comp, sizeof(array_u16_comp) / sizeof(array_u16_comp[0])));
    EXPECT_FALSE(EcmaString::CanBeCompressed(array_u16_not_comp, sizeof(array_u16_comp) / sizeof(array_u16_comp[0])));

    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    EXPECT_FALSE(EcmaString::CanBeCompressed(array_u16_not_comp, sizeof(array_u16_comp) / sizeof(array_u16_comp[0])));
    /* Set compressed_strings_enabled_ default, because it is a static boolean that some other functions rely on.The
     * foll- owing TEST_F will come to an unexpected result if we do not set it default in the end of this TEST_F.
     */
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: CreateEmptyString
 * @tc.desc: Check whether the EcmaString created through calling CreateEmptyString function is within expectations
 * before and after calling SetCompressedStringsEnabled function with false.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CreateEmptyString)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    JSHandle<EcmaString> handle_ecma_str_empty(thread_, EcmaString::CreateEmptyString(ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_empty->GetLength(), 0);
    EXPECT_TRUE(handle_ecma_str_empty->IsUtf8());
    EXPECT_FALSE(handle_ecma_str_empty->IsUtf16());

    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    JSHandle<EcmaString> handle_ecma_str_empty_disable_comp(thread_, EcmaString::CreateEmptyString(ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_empty_disable_comp->GetLength(), 0);
    EXPECT_TRUE(handle_ecma_str_empty_disable_comp->IsUtf16());
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: AllocStringObject
 * @tc.desc: Check whether the EcmaString created through calling AllocStringObject function is within expectations
 * before and after calling SetCompressedStringsEnabled function with false.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, AllocStringObject)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // AllocStringObject( , true, ).
    size_t size_alloc_comp = 5;
    JSHandle<EcmaString> handle_ecma_str_alloc_comp(thread_,
                                                    EcmaString::AllocStringObject(size_alloc_comp, true, ecma_vm_ptr));
    for (size_t i = 0; i < size_alloc_comp; i++) {
        EXPECT_EQ(handle_ecma_str_alloc_comp->At(i), 0);
    }
    EXPECT_EQ(handle_ecma_str_alloc_comp->GetLength(), size_alloc_comp);
    EXPECT_TRUE(handle_ecma_str_alloc_comp->IsUtf8());
    EXPECT_FALSE(handle_ecma_str_alloc_comp->IsUtf16());

    // AllocStringObject( , false, ).
    size_t size_alloc_not_comp = 5;
    JSHandle<EcmaString> handle_ecma_str_alloc_not_comp(
        thread_, EcmaString::AllocStringObject(size_alloc_not_comp, false, ecma_vm_ptr));
    for (size_t i = 0; i < size_alloc_not_comp; i++) {
        EXPECT_EQ(handle_ecma_str_alloc_not_comp->At(i), 0);
    }
    EXPECT_EQ(handle_ecma_str_alloc_not_comp->GetLength(), size_alloc_not_comp);
    EXPECT_FALSE(handle_ecma_str_alloc_not_comp->IsUtf8());
    EXPECT_TRUE(handle_ecma_str_alloc_not_comp->IsUtf16());
    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    JSHandle<EcmaString> handle_ecma_str_alloc_not_comp_disable_comp(
        thread_, EcmaString::AllocStringObject(size_alloc_not_comp, false, ecma_vm_ptr));
    EXPECT_TRUE(handle_ecma_str_alloc_not_comp_disable_comp->IsUtf16());
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: CreateFromUtf8
 * @tc.desc: Check whether the EcmaString created through calling CreateFromUtf8 function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CreateFromUtf8)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);
    uint8_t array_u8[] = {"xyz123!@#"};
    size_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    for (size_t i = 0; i < length_ecma_str_u8; i++) {
        EXPECT_EQ(array_u8[i], handle_ecma_str_u8->At(i));
    }
    EXPECT_EQ(handle_ecma_str_u8->GetLength(), length_ecma_str_u8);
    EXPECT_TRUE(handle_ecma_str_u8->IsUtf8());
    EXPECT_FALSE(handle_ecma_str_u8->IsUtf16());
}

/*
 * @tc.name: CreateFromUtf16
 * @tc.desc: Check whether the EcmaString created through calling CreateFromUtf16 function is within expectations
 * before and after calling SetCompressedStringsEnabled function with false.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CreateFromUtf16)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // CreateFromUtf16( , , , true).
    uint16_t array_u16_comp[] = {1, 23, 45, 67, 127};
    size_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    EXPECT_EQ(handle_ecma_str_u16_comp->GetLength(), length_ecma_str_u16_comp);
    EXPECT_TRUE(handle_ecma_str_u16_comp->IsUtf8());
    EXPECT_FALSE(handle_ecma_str_u16_comp->IsUtf16());

    // CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp[] = {127, 33, 128, 12, 256, 11100, 65535};
    size_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u16_not_comp->GetLength(), length_ecma_str_u16_not_comp);
    EXPECT_FALSE(handle_ecma_str_u16_not_comp->IsUtf8());
    EXPECT_TRUE(handle_ecma_str_u16_not_comp->IsUtf16());
    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_disable_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_TRUE(handle_ecma_str_u16_not_comp_disable_comp->IsUtf16());
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: ComputeSizeUtf8
 * @tc.desc: Check whether the value returned through calling ComputeSizeUtf8 function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, ComputeSizeUtf8)
{
    uint32_t scale = 3333;
    for (uint32_t i = 0x40000000U - 1; i > scale; i = i - scale) {
        uint32_t length = i;
        EXPECT_EQ(EcmaString::ComputeSizeUtf8(length), length + EcmaString::SIZE);
    }
}

/*
 * @tc.name: ComputeDataSizeUtf16
 * @tc.desc: Check whether the value returned through calling ComputeDataSizeUtf16 function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, ComputeDataSizeUtf16)
{
    uint32_t scale = 3333;
    for (uint32_t i = 0x40000000U - 1; i > scale; i = i - scale) {
        uint32_t length = i;
        EXPECT_EQ(EcmaString::ComputeDataSizeUtf16(length), 2 * length);
    }
}

/*
 * @tc.name: ComputeSizeUtf16
 * @tc.desc: Check whether the value returned through calling ComputeSizeUtf16 function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, ComputeSizeUtf16)
{
    uint32_t scale = 3333;
    for (uint32_t i = 0x40000000U - 1; i > scale; i = i - scale) {
        uint32_t length = i;
        EXPECT_EQ(EcmaString::ComputeSizeUtf16(length), 2 * length + EcmaString::SIZE);
    }
}

/*
 * @tc.name: ObjectSize
 * @tc.desc: Check whether the value returned through calling ObjectSize function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, ObjectSize)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    JSHandle<EcmaString> handle_ecma_str_empty(thread_, EcmaString::CreateEmptyString(ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_empty->ObjectSize(), EcmaString::SIZE + 0);

    size_t length_ecma_str_alloc_comp = 5;
    JSHandle<EcmaString> handle_ecma_str_alloc_comp(
        thread_, EcmaString::AllocStringObject(length_ecma_str_alloc_comp, true, ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_alloc_comp->ObjectSize(),
              EcmaString::SIZE + sizeof(uint8_t) * length_ecma_str_alloc_comp);

    size_t length_ecma_str_alloc_not_comp = 5;
    JSHandle<EcmaString> handle_ecma_str_alloc_not_comp(
        thread_, EcmaString::AllocStringObject(length_ecma_str_alloc_not_comp, false, ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_alloc_not_comp->ObjectSize(),
              EcmaString::SIZE + sizeof(uint16_t) * length_ecma_str_alloc_not_comp);

    uint8_t array_u8[] = {"abcde"};
    size_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    EXPECT_EQ(handle_ecma_str_u8->ObjectSize(), EcmaString::SIZE + sizeof(uint8_t) * length_ecma_str_u8);

    // ObjectSize(). EcmaString made by CreateFromUtf16( , , , true).
    uint16_t array_u16_comp[] = {1, 23, 45, 67, 127};
    size_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    EXPECT_EQ(handle_ecma_str_u16_comp->ObjectSize(), EcmaString::SIZE + sizeof(uint8_t) * length_ecma_str_u16_comp);

    // ObjectSize(). EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp[] = {127, 128, 256, 11100, 65535};
    size_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u16_not_comp->ObjectSize(),
              EcmaString::SIZE + sizeof(uint16_t) * length_ecma_str_u16_not_comp);
}

/*
 * @tc.name: Compare_001
 * @tc.desc: Check whether the value returned through calling Compare function between EcmaStrings made by
 * CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Compare_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Compare(). Between EcmaStrings made by CreateFromUtf8().
    uint8_t array_u8_no1[3] = {1, 23};
    uint8_t array_u8_no2[4] = {1, 23, 49};
    uint8_t array_u8_no3[6] = {1, 23, 45, 97, 127};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u8_no2 = sizeof(array_u8_no2) - 1;
    uint32_t length_ecma_str_u8_no3 = sizeof(array_u8_no3) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no2(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no2[0], length_ecma_str_u8_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no3(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no3[0], length_ecma_str_u8_no3, ecma_vm_ptr, true));
    EXPECT_EQ(handle_ecma_str_u8_no1->Compare(*handle_ecma_str_u8_no2), -1);
    EXPECT_EQ(handle_ecma_str_u8_no2->Compare(*handle_ecma_str_u8_no1), 1);
    EXPECT_EQ(handle_ecma_str_u8_no2->Compare(*handle_ecma_str_u8_no3), 49 - 45);
    EXPECT_EQ(handle_ecma_str_u8_no3->Compare(*handle_ecma_str_u8_no2), 45 - 49);
}

/*
 * @tc.name: Compare_002
 * @tc.desc: Check whether the value returned through calling Compare function between EcmaStrings made by
 * CreateFromUtf16( , , , true) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Compare_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Compare(). Between EcmaStrings made by CreateFromUtf16( , , , true).
    uint16_t array_u16_comp_no1[] = {1, 23};
    uint16_t array_u16_comp_no2[] = {1, 23, 49};
    uint16_t array_u16_comp_no3[] = {1, 23, 45, 97, 127};
    uint32_t length_ecma_str_u16_comp_no1 = sizeof(array_u16_comp_no1) / sizeof(array_u16_comp_no1[0]);
    uint32_t length_ecma_str_u16_comp_no2 = sizeof(array_u16_comp_no2) / sizeof(array_u16_comp_no2[0]);
    uint32_t length_ecma_str_u16_comp_no3 = sizeof(array_u16_comp_no3) / sizeof(array_u16_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no1(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no1[0], length_ecma_str_u16_comp_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no2(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no2[0], length_ecma_str_u16_comp_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no3(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no3[0], length_ecma_str_u16_comp_no3, ecma_vm_ptr, true));
    EXPECT_EQ(handle_ecma_str_u16_comp_no1->Compare(*handle_ecma_str_u16_comp_no2), -1);
    EXPECT_EQ(handle_ecma_str_u16_comp_no2->Compare(*handle_ecma_str_u16_comp_no1), 1);
    EXPECT_EQ(handle_ecma_str_u16_comp_no2->Compare(*handle_ecma_str_u16_comp_no3), 49 - 45);
    EXPECT_EQ(handle_ecma_str_u16_comp_no3->Compare(*handle_ecma_str_u16_comp_no2), 45 - 49);
}

/*
 * @tc.name: Compare_003
 * @tc.desc: Check whether the value returned through calling Compare function between EcmaString made by
 * CreateFromUtf8() and EcmaString made by CreateFromUtf16( , , , true) made by CreateFromUtf16( , , , true) is within
 * expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Compare_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Compare(). EcmaString made by CreateFromUtf8() and EcmaString made by CreateFromUtf16( , , , true).
    uint8_t array_u8_no1[3] = {1, 23};
    uint8_t array_u8_no2[4] = {1, 23, 49};
    uint16_t array_u16_comp_no1[] = {1, 23};
    uint16_t array_u16_comp_no2[] = {1, 23, 49};
    uint16_t array_u16_comp_no3[] = {1, 23, 45, 97, 127};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u8_no2 = sizeof(array_u8_no2) - 1;
    uint32_t length_ecma_str_u16_comp_no1 = sizeof(array_u16_comp_no1) / sizeof(array_u16_comp_no1[0]);
    uint32_t length_ecma_str_u16_comp_no2 = sizeof(array_u16_comp_no2) / sizeof(array_u16_comp_no2[0]);
    uint32_t length_ecma_str_u16_comp_no3 = sizeof(array_u16_comp_no3) / sizeof(array_u16_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no2(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no2[0], length_ecma_str_u8_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no1(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no1[0], length_ecma_str_u16_comp_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no2(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no2[0], length_ecma_str_u16_comp_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no3(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no3[0], length_ecma_str_u16_comp_no3, ecma_vm_ptr, true));
    EXPECT_EQ(handle_ecma_str_u8_no1->Compare(*handle_ecma_str_u16_comp_no1), 0);
    EXPECT_EQ(handle_ecma_str_u16_comp_no1->Compare(*handle_ecma_str_u8_no1), 0);
    EXPECT_EQ(handle_ecma_str_u8_no1->Compare(*handle_ecma_str_u16_comp_no2), -1);
    EXPECT_EQ(handle_ecma_str_u16_comp_no2->Compare(*handle_ecma_str_u8_no1), 1);
    EXPECT_EQ(handle_ecma_str_u8_no2->Compare(*handle_ecma_str_u16_comp_no3), 49 - 45);
    EXPECT_EQ(handle_ecma_str_u16_comp_no3->Compare(*handle_ecma_str_u8_no2), 45 - 49);
}

/*
 * @tc.name: Compare_004
 * @tc.desc: Check whether the value returned through calling Compare function between EcmaStrings made by
 * CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Compare_004)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Compare(). Between EcmaStrings made by CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp_no1[] = {1, 23};
    uint16_t array_u16_not_comp_no2[] = {1, 23, 49};
    uint16_t array_u16_not_comp_no3[] = {1, 23, 456, 6789, 65535, 127};
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    uint32_t length_ecma_str_u16_not_comp_no2 = sizeof(array_u16_not_comp_no2) / sizeof(array_u16_not_comp_no2[0]);
    uint32_t length_ecma_str_u16_not_comp_no3 = sizeof(array_u16_not_comp_no3) / sizeof(array_u16_not_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no2(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no2[0], length_ecma_str_u16_not_comp_no2, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no3(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no3[0], length_ecma_str_u16_not_comp_no3, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no1->Compare(*handle_ecma_str_u16_not_comp_no2), -1);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no2->Compare(*handle_ecma_str_u16_not_comp_no1), 1);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no2->Compare(*handle_ecma_str_u16_not_comp_no3), 49 - 456);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no3->Compare(*handle_ecma_str_u16_not_comp_no2), 456 - 49);
}

/*
 * @tc.name: Compare_005
 * @tc.desc: Check whether the value returned through calling Compare function between EcmaString made by
 * CreateFromUtf8() and EcmaString made by CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Compare_005)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Compare(). EcmaString made by CreateFromUtf8() and EcmaString made by CreateFromUtf16( , , , false).
    uint8_t array_u8_no1[3] = {1, 23};
    uint8_t array_u8_no2[4] = {1, 23, 49};
    uint16_t array_u16_not_comp_no1[] = {1, 23};
    uint16_t array_u16_not_comp_no2[] = {1, 23, 49};
    uint16_t array_u16_not_comp_no3[] = {1, 23, 456, 6789, 65535, 127};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u8_no2 = sizeof(array_u8_no2) - 1;
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    uint32_t length_ecma_str_u16_not_comp_no2 = sizeof(array_u16_not_comp_no2) / sizeof(array_u16_not_comp_no2[0]);
    uint32_t length_ecma_str_u16_not_comp_no3 = sizeof(array_u16_not_comp_no3) / sizeof(array_u16_not_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no2(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no2[0], length_ecma_str_u8_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no2(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no2[0], length_ecma_str_u16_not_comp_no2, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no3(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no3[0], length_ecma_str_u16_not_comp_no3, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u8_no1->Compare(*handle_ecma_str_u16_not_comp_no1), 0);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no1->Compare(*handle_ecma_str_u8_no1), 0);
    EXPECT_EQ(handle_ecma_str_u8_no1->Compare(*handle_ecma_str_u16_not_comp_no2), -1);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no2->Compare(*handle_ecma_str_u8_no1), 1);
    EXPECT_EQ(handle_ecma_str_u8_no2->Compare(*handle_ecma_str_u16_not_comp_no3), 49 - 456);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no3->Compare(*handle_ecma_str_u8_no2), 456 - 49);
}

/*
 * @tc.name: Compare_006
 * @tc.desc: Check whether the value returned through calling Compare function between EcmaString made by
 * CreateFromUtf16( , , , true) and EcmaString made by CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Compare_006)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Compare(). EcmaString made by CreateFromUtf16( , , , true) and EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_u16_comp_no1[] = {1, 23};
    uint16_t array_u16_comp_no2[] = {1, 23, 49};
    uint16_t array_u16_not_comp_no1[] = {1, 23};
    uint16_t array_u16_not_comp_no2[] = {1, 23, 49};
    uint16_t array_u16_not_comp_no3[] = {1, 23, 456, 6789, 65535, 127};
    uint32_t length_ecma_str_u16_comp_no1 = sizeof(array_u16_comp_no1) / sizeof(array_u16_comp_no1[0]);
    uint32_t length_ecma_str_u16_comp_no2 = sizeof(array_u16_comp_no2) / sizeof(array_u16_comp_no2[0]);
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    uint32_t length_ecma_str_u16_not_comp_no2 = sizeof(array_u16_not_comp_no2) / sizeof(array_u16_not_comp_no2[0]);
    uint32_t length_ecma_str_u16_not_comp_no3 = sizeof(array_u16_not_comp_no3) / sizeof(array_u16_not_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no1(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no1[0], length_ecma_str_u16_comp_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no2(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no2[0], length_ecma_str_u16_comp_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no2(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no2[0], length_ecma_str_u16_not_comp_no2, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no3(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no3[0], length_ecma_str_u16_not_comp_no3, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u16_comp_no1->Compare(*handle_ecma_str_u16_not_comp_no1), 0);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no1->Compare(*handle_ecma_str_u16_comp_no1), 0);
    EXPECT_EQ(handle_ecma_str_u16_comp_no1->Compare(*handle_ecma_str_u16_not_comp_no2), -1);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no2->Compare(*handle_ecma_str_u16_comp_no1), 1);
    EXPECT_EQ(handle_ecma_str_u16_comp_no2->Compare(*handle_ecma_str_u16_not_comp_no3), 49 - 456);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_no3->Compare(*handle_ecma_str_u16_comp_no2), 456 - 49);
}

/*
 * @tc.name: Concat_001
 * @tc.desc: Check whether the EcmaString returned through calling Concat function between EcmaString made by
 * CreateFromUtf8() and EcmaString made by CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Concat_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Concat(). EcmaString made by CreateFromUtf8() and EcmaString made by CreateFromUtf8().
    uint8_t array_front_u8[] = {"abcdef"};
    uint8_t array_back_u8[] = {"ABCDEF"};
    uint32_t length_ecma_str_front_u8 = sizeof(array_front_u8) - 1;
    uint32_t length_ecma_str_back_u8 = sizeof(array_back_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_front_u8(
        thread_, EcmaString::CreateFromUtf8(&array_front_u8[0], length_ecma_str_front_u8, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_back_u8(
        thread_, EcmaString::CreateFromUtf8(&array_back_u8[0], length_ecma_str_back_u8, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_concat_u8(
        thread_, EcmaString::Concat(handle_ecma_str_front_u8, handle_ecma_str_back_u8, ecma_vm_ptr));
    EXPECT_TRUE(handle_ecma_str_concat_u8->IsUtf8());
    for (size_t i = 0; i < length_ecma_str_front_u8; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u8->At(i), array_front_u8[i]);
    }
    for (size_t i = 0; i < length_ecma_str_back_u8; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u8->At(i + length_ecma_str_front_u8), array_back_u8[i]);
    }
    EXPECT_EQ(handle_ecma_str_concat_u8->GetLength(), length_ecma_str_front_u8 + length_ecma_str_back_u8);
}

/*
 * @tc.name: Concat_002
 * @tc.desc: Check whether the EcmaString returned through calling Concat function between EcmaString made by
 * CreateFromUtf16( , , , false) and EcmaString made by CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Concat_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Concat(). EcmaString made by CreateFromUtf16( , , , false) and EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_front_u16_not_comp[] = {128, 129, 256, 11100, 65535, 100};
    uint16_t array_back_u16_not_comp[] = {88, 768, 1, 270, 345, 333};
    uint32_t length_ecma_str_front_u16_not_comp =
        sizeof(array_front_u16_not_comp) / sizeof(array_front_u16_not_comp[0]);
    uint32_t length_ecma_str_back_u16_not_comp = sizeof(array_back_u16_not_comp) / sizeof(array_back_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_front_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_front_u16_not_comp[0], length_ecma_str_front_u16_not_comp,
                                             ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_back_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_back_u16_not_comp[0], length_ecma_str_back_u16_not_comp,
                                             ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_concat_u16_not_comp(
        thread_,
        EcmaString::Concat(handle_ecma_str_front_u16_not_comp, handle_ecma_str_back_u16_not_comp, ecma_vm_ptr));
    EXPECT_TRUE(handle_ecma_str_concat_u16_not_comp->IsUtf16());
    for (size_t i = 0; i < length_ecma_str_front_u16_not_comp; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u16_not_comp->At(i), array_front_u16_not_comp[i]);
    }
    for (size_t i = 0; i < length_ecma_str_back_u16_not_comp; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u16_not_comp->At(i + length_ecma_str_front_u16_not_comp),
                  array_back_u16_not_comp[i]);
    }
    EXPECT_EQ(handle_ecma_str_concat_u16_not_comp->GetLength(),
              length_ecma_str_front_u16_not_comp + length_ecma_str_back_u16_not_comp);
}

/*
 * @tc.name: Concat_003
 * @tc.desc: Check whether the EcmaString returned through calling Concat function between EcmaString made by
 * CreateFromUtf8() and EcmaString made by CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Concat_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // Concat(). EcmaString made by CreateFromUtf8() and EcmaString made by CreateFromUtf16( , , , false).
    uint8_t array_front_u8[] = {"abcdef"};
    uint16_t array_back_u16_not_comp[] = {88, 768, 1, 270, 345, 333};
    uint32_t length_ecma_str_front_u8 = sizeof(array_front_u8) - 1;
    uint32_t length_ecma_str_back_u16_not_comp = sizeof(array_back_u16_not_comp) / sizeof(array_back_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_front_u8(
        thread_, EcmaString::CreateFromUtf8(&array_front_u8[0], length_ecma_str_front_u8, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_back_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_back_u16_not_comp[0], length_ecma_str_back_u16_not_comp,
                                             ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_concat_u8_u16_not_comp(
        thread_, EcmaString::Concat(handle_ecma_str_front_u8, handle_ecma_str_back_u16_not_comp, ecma_vm_ptr));
    EXPECT_TRUE(handle_ecma_str_concat_u8_u16_not_comp->IsUtf16());
    for (size_t i = 0; i < length_ecma_str_front_u8; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u8_u16_not_comp->At(i), array_front_u8[i]);
    }
    for (size_t i = 0; i < length_ecma_str_back_u16_not_comp; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u8_u16_not_comp->At(i + length_ecma_str_front_u8), array_back_u16_not_comp[i]);
    }
    EXPECT_EQ(handle_ecma_str_concat_u8_u16_not_comp->GetLength(),
              length_ecma_str_front_u8 + length_ecma_str_back_u16_not_comp);
}

/*
 * @tc.name: Concat_004
 * @tc.desc: Call SetCompressedStringsEnabled() function with false, check whether the EcmaString returned through
 * calling Concat function between EcmaString made by CreateFromUtf8() and EcmaString made by
 * CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, Concat_004)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    /* Concat() after SetCompressedStringsEnabled(false). EcmaString made by CreateFromUtf16( , , , false) and
     * EcmaString made by CreateFromUtf16( , , , false).
     */
    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    uint16_t array_front_u16_not_comp[] = {128, 129, 256, 11100, 65535, 100};
    uint16_t array_back_u16_not_comp[] = {88, 768, 1, 270, 345, 333};
    uint32_t length_ecma_str_front_u16_not_comp =
        sizeof(array_front_u16_not_comp) / sizeof(array_front_u16_not_comp[0]);
    uint32_t length_ecma_str_back_u16_not_comp = sizeof(array_back_u16_not_comp) / sizeof(array_back_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_front_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_front_u16_not_comp[0], length_ecma_str_front_u16_not_comp,
                                             ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_back_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_back_u16_not_comp[0], length_ecma_str_back_u16_not_comp,
                                             ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_concat_u16_not_comp_after_set_false(
        thread_,
        EcmaString::Concat(handle_ecma_str_front_u16_not_comp, handle_ecma_str_back_u16_not_comp, ecma_vm_ptr));
    EXPECT_TRUE(handle_ecma_str_concat_u16_not_comp_after_set_false->IsUtf16());
    for (size_t i = 0; i < length_ecma_str_front_u16_not_comp; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u16_not_comp_after_set_false->At(i), array_front_u16_not_comp[i]);
    }
    for (size_t i = 0; i < length_ecma_str_back_u16_not_comp; i++) {
        EXPECT_EQ(handle_ecma_str_concat_u16_not_comp_after_set_false->At(i + length_ecma_str_front_u16_not_comp),
                  array_back_u16_not_comp[i]);
    }
    EXPECT_EQ(handle_ecma_str_concat_u16_not_comp_after_set_false->GetLength(),
              length_ecma_str_front_u16_not_comp + length_ecma_str_back_u16_not_comp);
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: FastSubString_001
 * @tc.desc: Check whether the EcmaString returned through calling FastSubString function from EcmaString made by
 * CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, FastSubString_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // FastSubString(). From EcmaString made by CreateFromUtf8().
    uint8_t array_u8[6] = {3, 7, 19, 54, 99};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    uint32_t index_start_sub_u8 = 2;
    uint32_t length_sub_u8 = 2;
    JSHandle<EcmaString> handle_ecma_str_sub_u8(
        thread_, EcmaString::FastSubString(handle_ecma_str_u8, index_start_sub_u8, length_sub_u8, ecma_vm_ptr));
    for (size_t i = 0; i < length_sub_u8; i++) {
        EXPECT_EQ(handle_ecma_str_sub_u8->At(i), handle_ecma_str_u8->At(i + index_start_sub_u8));
    }
    EXPECT_EQ(handle_ecma_str_sub_u8->GetLength(), length_sub_u8);
}

/*
 * @tc.name: FastSubString_002
 * @tc.desc: Check whether the EcmaString returned through calling FastSubString function from EcmaString made by
 * CreateFromUtf16( , , , true) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, FastSubString_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // FastSubString(). From EcmaString made by CreateFromUtf16( , , , true).
    uint16_t array_u16_comp[] = {1, 12, 34, 56, 127};
    uint32_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    uint32_t index_start_sub_u16_comp = 0;
    uint32_t length_sub_u16_comp = 2;
    JSHandle<EcmaString> handle_ecma_str_sub_u16_comp(
        thread_, EcmaString::FastSubString(handle_ecma_str_u16_comp, index_start_sub_u16_comp, length_sub_u16_comp,
                                           ecma_vm_ptr));
    for (size_t i = 0; i < length_sub_u16_comp; i++) {
        EXPECT_EQ(handle_ecma_str_sub_u16_comp->At(i), handle_ecma_str_u16_comp->At(i + index_start_sub_u16_comp));
    }
    EXPECT_EQ(handle_ecma_str_sub_u16_comp->GetLength(), length_sub_u16_comp);
}

/*
 * @tc.name: FastSubString_003
 * @tc.desc: Check whether the EcmaString returned through calling FastSubString function from EcmaString made by
 * CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, FastSubString_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // FastSubString(). From EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp[] = {19, 54, 256, 11100, 65535};
    uint32_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    uint32_t index_start_sub_u16_not_comp = 0;
    uint32_t length_sub_u16_not_comp = 2;
    JSHandle<EcmaString> handle_ecma_str_sub_u16_not_comp(
        thread_, EcmaString::FastSubString(handle_ecma_str_u16_not_comp, index_start_sub_u16_not_comp,
                                           length_sub_u16_not_comp, ecma_vm_ptr));
    for (size_t i = 0; i < length_sub_u16_not_comp; i++) {
        EXPECT_EQ(handle_ecma_str_sub_u16_not_comp->At(i),
                  handle_ecma_str_u16_not_comp->At(i + index_start_sub_u16_not_comp));
    }
    EXPECT_EQ(handle_ecma_str_sub_u16_not_comp->GetLength(), length_sub_u16_not_comp);
}

/*
 * @tc.name: WriteData_001
 * @tc.desc: Check whether the target EcmaString made by AllocStringObject( , true, ) changed through calling WriteData
 * function with a source EcmaString made by CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, WriteData_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // WriteData(). From EcmaString made by CreateFromUtf8() to EcmaString made by AllocStringObject( , true, ).
    uint8_t array_u8_write_from[6] = {1, 12, 34, 56, 127};
    uint32_t length_ecma_str_u8_write_from = sizeof(array_u8_write_from) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8_write_from(
        thread_, EcmaString::CreateFromUtf8(&array_u8_write_from[0], length_ecma_str_u8_write_from, ecma_vm_ptr, true));
    size_t size_ecma_str_u8_write_to = 5;
    JSHandle<EcmaString> handle_ecma_str_alloc_true_write_to(
        thread_, EcmaString::AllocStringObject(size_ecma_str_u8_write_to, true, ecma_vm_ptr));
    uint32_t index_start_write_from_array_u8 = 2;
    uint32_t length_write_from_array_u8 = 2;
    handle_ecma_str_alloc_true_write_to->WriteData(*handle_ecma_str_u8_write_from, index_start_write_from_array_u8,
                                                   size_ecma_str_u8_write_to, length_write_from_array_u8);
    for (size_t i = 0; i < length_write_from_array_u8; i++) {
        EXPECT_EQ(handle_ecma_str_alloc_true_write_to->At(i + index_start_write_from_array_u8),
                  handle_ecma_str_u8_write_from->At(i));
    }
}

/*
 * @tc.name: WriteData_002
 * @tc.desc: Check whether the target EcmaString made by AllocStringObject( , true, ) changed through calling WriteData
 * function from a source char is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, WriteData_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // WriteData(). From char to EcmaString made by AllocStringObject( , true, ).
    char u8_write = 'a';
    size_t size_ecma_str_u8_write_to = 5;
    JSHandle<EcmaString> handle_ecma_str_alloc_true_write_to(
        thread_, EcmaString::AllocStringObject(size_ecma_str_u8_write_to, true, ecma_vm_ptr));
    uint32_t index_at_write_from_u8 = 4;
    handle_ecma_str_alloc_true_write_to->WriteData(u8_write, index_at_write_from_u8);
    EXPECT_EQ(handle_ecma_str_alloc_true_write_to->At(index_at_write_from_u8), u8_write);
}

/*
 * @tc.name: WriteData_003
 * @tc.desc: Check whether the target EcmaString made by AllocStringObject( , false, ) changed through calling
 * WriteData function with a source EcmaString made by CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, WriteData_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    /* WriteData(). From EcmaString made by CreateFromUtf16( , , , false) to EcmaStringU16 made by
     * AllocStringObject( , false, ).
     */
    uint16_t array_u16_write_from[10] = {67, 777, 1999, 1, 45, 66, 23456, 65535, 127, 333};
    uint32_t length_ecma_str_u16_write_from = sizeof(array_u16_write_from) / sizeof(array_u16_write_from[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_write_from(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_write_from[0], length_ecma_str_u16_write_from, ecma_vm_ptr, false));
    size_t size_ecma_str_u16_write_to = 10;
    JSHandle<EcmaString> handle_ecma_str_u16_write_to(
        thread_, EcmaString::AllocStringObject(size_ecma_str_u16_write_to, false, ecma_vm_ptr));
    uint32_t index_start_write_from_array_u16 = 3;
    uint32_t num_bytes_write_from_array_u16 = 2 * 3;
    handle_ecma_str_u16_write_to->WriteData(*handle_ecma_str_u16_write_from, index_start_write_from_array_u16,
                                            size_ecma_str_u16_write_to, num_bytes_write_from_array_u16);
    for (size_t i = 0; i < (num_bytes_write_from_array_u16 / 2); i++) {
        EXPECT_EQ(handle_ecma_str_u16_write_to->At(i + index_start_write_from_array_u16),
                  handle_ecma_str_u16_write_from->At(i));
    }
}

/*
 * @tc.name: WriteData_004
 * @tc.desc: Check whether the target EcmaString made by AllocStringObject( , false, ) changed through calling
 * WriteData function with a source EcmaString made by CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, WriteData_004)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // WriteData(). From EcmaString made by CreateFromUtf8() to EcmaString made by AllocStringObject( , false, ).
    uint8_t array_u8_write_from[6] = {1, 12, 34, 56, 127};
    uint32_t length_ecma_str_u8_write_from = sizeof(array_u8_write_from) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8_write_from(
        thread_, EcmaString::CreateFromUtf8(&array_u8_write_from[0], length_ecma_str_u8_write_from, ecma_vm_ptr, true));
    size_t size_ecma_str_u16_write_to = 10;
    JSHandle<EcmaString> handle_ecma_str_u16_write_to(
        thread_, EcmaString::AllocStringObject(size_ecma_str_u16_write_to, false, ecma_vm_ptr));
    uint32_t index_start_write_from_u8_to_u16 = 1;
    uint32_t num_bytes_write_from_u8_to_u16 = 4;
    handle_ecma_str_u16_write_to->WriteData(*handle_ecma_str_u8_write_from, index_start_write_from_u8_to_u16,
                                            size_ecma_str_u16_write_to, num_bytes_write_from_u8_to_u16);
    for (size_t i = 0; i < num_bytes_write_from_u8_to_u16; i++) {
        EXPECT_EQ(handle_ecma_str_u16_write_to->At(i + index_start_write_from_u8_to_u16),
                  handle_ecma_str_u8_write_from->At(i));
    }
}

/*
 * @tc.name: WriteData_005
 * @tc.desc: Check whether the target EcmaString made by AllocStringObject( , false, ) changed through calling
 * WriteData function with a source char is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, WriteData_005)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // WriteData(). From char to EcmaString made by AllocStringObject( , false, ).
    size_t size_ecma_str_u16_write_to = 10;
    JSHandle<EcmaString> handle_ecma_str_u16_write_to(
        thread_, EcmaString::AllocStringObject(size_ecma_str_u16_write_to, false, ecma_vm_ptr));
    char u8_write = 'a';
    uint32_t index_at = 4;
    handle_ecma_str_u16_write_to->WriteData(u8_write, index_at);
    EXPECT_EQ(handle_ecma_str_u16_write_to->At(index_at), u8_write);
}

/*
 * @tc.name: GetUtf8Length
 * @tc.desc: Check whether the value returned through calling GetUtf8Length function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetUtf8Length)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);
    uint8_t array_u8[6] = {3, 7, 19, 54, 99};
    uint16_t array_u16_comp[] = {1, 12, 34, 56, 127};
    uint16_t array_u16_not_comp[] = {19, 54, 256, 11100, 65535};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    uint32_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    uint32_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u8->GetUtf8Length(), length_ecma_str_u8 + 1);
    EXPECT_EQ(handle_ecma_str_u16_comp->GetUtf8Length(), length_ecma_str_u16_comp + 1);
    EXPECT_EQ(handle_ecma_str_u16_not_comp->GetUtf8Length(), 2 * length_ecma_str_u16_not_comp + 1);
}

/*
 * @tc.name: GetUtf16Length
 * @tc.desc: Check whether the value returned through calling GetUtf16Length function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetUtf16Length)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    uint8_t array_u8[6] = {3, 7, 19, 54, 99};
    uint16_t array_u16_comp[] = {1, 12, 34, 56, 127};
    uint16_t array_u16_not_comp[] = {19, 54, 256, 11100, 65535};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    uint32_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    uint32_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u8->GetUtf16Length(), length_ecma_str_u8);
    EXPECT_EQ(handle_ecma_str_u16_comp->GetUtf16Length(), length_ecma_str_u16_comp);
    EXPECT_EQ(handle_ecma_str_u16_not_comp->GetUtf16Length(), length_ecma_str_u16_not_comp);
}

/*
 * @tc.name: GetDataUtf8
 * @tc.desc: Check whether the pointer returned through calling GetDataUtf8 function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetDataUtf8)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // From EcmaString made by CreateFromUtf8().
    uint8_t array_u8[] = {"abcde"};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    for (size_t i = 0; i < length_ecma_str_u8; i++) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        EXPECT_EQ(*(handle_ecma_str_u8->GetDataUtf8() + i), array_u8[i]);
    }

    // From EcmaString made by CreateFromUtf16( , , , true).
    uint16_t array_u16_comp[] = {3, 1, 34, 123, 127, 111, 42, 3, 20, 10};
    uint32_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    for (size_t i = 0; i < sizeof(array_u16_comp) / array_u16_comp[0]; i++) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        EXPECT_EQ(*(handle_ecma_str_u16_comp->GetDataUtf8() + i), array_u16_comp[i]);
    }
}

/*
 * @tc.name: GetDataUtf16
 * @tc.desc: Check whether the pointer returned through calling GetDataUtf16 function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetDataUtf16)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // From EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp[] = {67, 777, 1999, 1, 45, 66, 23456, 65535, 127, 333};
    uint32_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    for (size_t i = 0; i < length_ecma_str_u16_not_comp; i++) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        EXPECT_EQ(*(handle_ecma_str_u16_not_comp->GetDataUtf16() + i), array_u16_not_comp[i]);
    }
}

/*
 * @tc.name: CopyDataRegionUtf8
 * @tc.desc: Check whether the returned value and the changed array through a source EcmaString's calling
 * CopyDataRegionUtf8 function are within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CopyDataRegionUtf8)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // CopyDataRegionUtf8(). From EcmaString made by CreateFromUtf8().
    uint8_t array_u8_copy_from[6] = {1, 12, 34, 56, 127};
    uint32_t length_ecma_str_u8_copy_from = sizeof(array_u8_copy_from) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8_copy_from(
        thread_, EcmaString::CreateFromUtf8(&array_u8_copy_from[0], length_ecma_str_u8_copy_from, ecma_vm_ptr, true));
    const size_t length_array_u8_target = 7;
    uint8_t default_byte_for_u8_copy_to = 1;
    uint8_t array_u8_copy_to[length_array_u8_target];
    memset_s(&array_u8_copy_to[0], length_array_u8_target, default_byte_for_u8_copy_to, length_array_u8_target);

    size_t index_start_from_array_u8 = 2;
    size_t length_copy_to_ecma_str_u8 = 3;
    size_t length_return_u8 = handle_ecma_str_u8_copy_from->CopyDataRegionUtf8(
        array_u8_copy_to, index_start_from_array_u8, length_copy_to_ecma_str_u8, length_array_u8_target);

    EXPECT_EQ(length_return_u8, length_copy_to_ecma_str_u8);
    for (size_t i = 0; i < length_copy_to_ecma_str_u8; i++) {
        EXPECT_EQ(array_u8_copy_to[i], handle_ecma_str_u8_copy_from->At(i + index_start_from_array_u8));
    }
    for (size_t i = length_copy_to_ecma_str_u8; i < length_array_u8_target; i++) {
        EXPECT_EQ(array_u8_copy_to[i], default_byte_for_u8_copy_to);
    }

    // CopyDataRegionUtf8(). From EcmaString made by CreateFromUtf16( , , , true).
    uint16_t array_u16_comp_copy_from[] = {1, 12, 34, 56, 127};
    uint32_t length_ecma_str_u16_comp_copy_from =
        sizeof(array_u16_comp_copy_from) / sizeof(array_u16_comp_copy_from[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_copy_from(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_copy_from[0], length_ecma_str_u16_comp_copy_from,
                                             ecma_vm_ptr, true));
    const size_t length_array_u16_target = 8;
    uint8_t default_byte_for_u16_comp_copy_to = 1;
    uint8_t array_u16_comp_copy_to[length_array_u16_target];
    memset_s(&array_u16_comp_copy_to[0], length_array_u16_target, default_byte_for_u16_comp_copy_to,
             length_array_u16_target);

    size_t index_start_from_array_u16_comp = 2;
    size_t length_copy_to_ecma_str_u16_comp = 3;
    size_t length_return_u16_comp = handle_ecma_str_u16_comp_copy_from->CopyDataRegionUtf8(
        &array_u16_comp_copy_to[0], index_start_from_array_u16_comp, length_copy_to_ecma_str_u16_comp,
        length_array_u16_target);

    EXPECT_EQ(length_return_u16_comp, length_copy_to_ecma_str_u16_comp);
    for (size_t i = 0; i < length_return_u16_comp; i++) {
        EXPECT_EQ(array_u16_comp_copy_to[i],
                  handle_ecma_str_u16_comp_copy_from->At(i + index_start_from_array_u16_comp));
    }
    for (size_t i = length_return_u16_comp; i < length_array_u16_target; i++) {
        EXPECT_EQ(array_u16_comp_copy_to[i], default_byte_for_u16_comp_copy_to);
    }
}

/*
 * @tc.name: CopyDataUtf8
 * @tc.desc: Check whether the returned value and the changed array through a source EcmaString's calling
 * CopyDataUtf8 function are within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CopyDataUtf8)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // CopyDataUtf8(). From EcmaString made by CreateFromUtf8().
    uint8_t array_u8_copy_from[6] = {1, 12, 34, 56, 127};
    uint32_t length_ecma_str_u8_copy_from = sizeof(array_u8_copy_from) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8_copy_from(
        thread_, EcmaString::CreateFromUtf8(&array_u8_copy_from[0], length_ecma_str_u8_copy_from, ecma_vm_ptr, true));
    const size_t length_array_u8_target = 6;
    uint8_t array_u8_copy_to[length_array_u8_target];

    size_t length_return_u8 = handle_ecma_str_u8_copy_from->CopyDataUtf8(&array_u8_copy_to[0], length_array_u8_target);

    EXPECT_EQ(length_return_u8, length_array_u8_target);
    for (size_t i = 0; i < length_return_u8 - 1; i++) {
        EXPECT_EQ(array_u8_copy_to[i], array_u8_copy_from[i]);
    }
    EXPECT_EQ(array_u8_copy_to[length_return_u8 - 1], 0);

    // CopyDataUtf8(). From EcmaString made by CreateFromUtf16( , , , true).
    uint16_t array_u16_comp_copy_from[] = {1, 12, 34, 56, 127};
    uint32_t length_ecma_str_u16_comp_copy_from =
        sizeof(array_u16_comp_copy_from) / sizeof(array_u16_comp_copy_from[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_copy_from(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_copy_from[0], length_ecma_str_u16_comp_copy_from,
                                             ecma_vm_ptr, true));
    const size_t length_array_u16_target = 6;
    uint8_t array_u8_comp_copy_to[length_array_u16_target];

    size_t length_return_u16_comp =
        handle_ecma_str_u16_comp_copy_from->CopyDataUtf8(&array_u8_comp_copy_to[0], length_array_u16_target);

    EXPECT_EQ(length_return_u16_comp, length_array_u16_target);
    for (size_t i = 0; i < length_return_u16_comp - 1; i++) {
        EXPECT_EQ(array_u8_comp_copy_to[i], array_u16_comp_copy_from[i]);
    }
    EXPECT_EQ(array_u8_comp_copy_to[length_return_u16_comp - 1], 0);
}

/*
 * @tc.name: CopyDataRegionUtf16
 * @tc.desc: Check whether the returned value and the changed array through a source EcmaString's calling
 * CopyDataRegionUtf16 function are within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CopyDataRegionUtf16)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // CopyDataRegionUtf16(). From EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp_copy_from[10] = {67, 777, 1999, 1, 45, 66, 23456, 65535, 127, 333};
    uint32_t length_ecma_str_u16_not_comp_copy_from =
        sizeof(array_u16_not_comp_copy_from) / sizeof(array_u16_not_comp_copy_from[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_copy_from(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp_copy_from[0], length_ecma_str_u16_not_comp_copy_from,
                                             ecma_vm_ptr, false));
    const size_t length_array_u16_target = 13;
    uint16_t array_u16_not_comp_copy_to[length_array_u16_target];
    uint8_t default_one_byte_value_of_array_u16_not_comp_copy_to = 244;
    memset_s(&array_u16_not_comp_copy_to[0], sizeof(uint16_t) * length_array_u16_target,
             default_one_byte_value_of_array_u16_not_comp_copy_to, sizeof(uint16_t) * length_array_u16_target);

    size_t start_index_from_array_u16_not_comp = 2;
    size_t length_copy_from_array_u16_not_comp = 3;
    size_t length_return_u16_not_comp = handle_ecma_str_u16_not_comp_copy_from->CopyDataRegionUtf16(
        &array_u16_not_comp_copy_to[0], start_index_from_array_u16_not_comp, length_copy_from_array_u16_not_comp,
        length_array_u16_target);

    EXPECT_EQ(length_return_u16_not_comp, length_copy_from_array_u16_not_comp);
    for (size_t i = 0; i < length_return_u16_not_comp; i++) {
        EXPECT_EQ(array_u16_not_comp_copy_to[i],
                  handle_ecma_str_u16_not_comp_copy_from->At(i + start_index_from_array_u16_not_comp));
    }
    for (size_t i = length_return_u16_not_comp; i < length_array_u16_target; i++) {
        EXPECT_EQ(array_u16_not_comp_copy_to[i],
                  ((uint16_t)default_one_byte_value_of_array_u16_not_comp_copy_to) * (1U + (1U << 8U)));
    }
}

/*
 * @tc.name: CopyDataUtf16
 * @tc.desc: Check whether the returned value and the changed array through a source EcmaString's calling
 * CopyDataUtf16 function are within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, CopyDataUtf16)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // CopyDataUtf16(). From EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp_copy_from[10] = {67, 777, 1999, 1, 45, 66, 23456, 65535, 127, 333};
    uint32_t length_ecma_str_u16_not_comp_copy_from =
        sizeof(array_u16_not_comp_copy_from) / sizeof(array_u16_not_comp_copy_from[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_copy_from(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp_copy_from[0], length_ecma_str_u16_not_comp_copy_from,
                                             ecma_vm_ptr, false));
    const size_t length_array_u16_target = 13;
    uint16_t array_u16_not_comp_copy_to[length_array_u16_target];
    uint8_t default_one_byte_value_of_array_u16_not_comp_copy_to = 244;
    memset_s(&array_u16_not_comp_copy_to[0], sizeof(uint16_t) * length_array_u16_target,
             default_one_byte_value_of_array_u16_not_comp_copy_to, sizeof(uint16_t) * length_array_u16_target);

    size_t length_return_u16_not_comp =
        handle_ecma_str_u16_not_comp_copy_from->CopyDataUtf16(&array_u16_not_comp_copy_to[0], length_array_u16_target);

    EXPECT_EQ(length_return_u16_not_comp, length_ecma_str_u16_not_comp_copy_from);
    for (size_t i = 0; i < length_return_u16_not_comp; i++) {
        EXPECT_EQ(array_u16_not_comp_copy_to[i], handle_ecma_str_u16_not_comp_copy_from->At(i));
    }
    for (size_t i = length_return_u16_not_comp; i < length_array_u16_target; i++) {
        EXPECT_EQ(array_u16_not_comp_copy_to[i],
                  ((uint16_t)default_one_byte_value_of_array_u16_not_comp_copy_to) * (1U + (1U << 8U)));
    }
}

/*
 * @tc.name: IndexOf_001
 * @tc.desc: Check whether the value returned through a source EcmaString made by CreateFromUtf8() calling IndexOf
 * function with a target EcmaString made by CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, IndexOf_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // IndexOf(). Find EcmaString made by CreateFromUtf8() From EcmaString made by CreateFromUtf8().
    uint8_t array_u8_from[7] = {23, 25, 1, 3, 39, 80};
    uint8_t array_u8_target[4] = {1, 3, 39};
    uint32_t length_ecma_str_u8_from = sizeof(array_u8_from) - 1;
    uint32_t length_ecma_str_u8_target = sizeof(array_u8_target) - 1;
    JSHandle<EcmaString> handle_ecma_str(
        thread_, EcmaString::CreateFromUtf8(&array_u8_from[0], length_ecma_str_u8_from, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_target[0], length_ecma_str_u8_target, ecma_vm_ptr, true));
    int32_t pos_start = 0;
    EXPECT_EQ(handle_ecma_str->IndexOf(*handle_ecma_str1, pos_start), 2);
    EXPECT_EQ(handle_ecma_str1->IndexOf(*handle_ecma_str, pos_start), -1);
    pos_start = -1;
    EXPECT_EQ(handle_ecma_str->IndexOf(*handle_ecma_str1, pos_start), 2);
    pos_start = 1;
    EXPECT_EQ(handle_ecma_str->IndexOf(*handle_ecma_str1, pos_start), 2);
    pos_start = 2;
    EXPECT_EQ(handle_ecma_str->IndexOf(*handle_ecma_str1, pos_start), 2);
    pos_start = 3;
    EXPECT_EQ(handle_ecma_str->IndexOf(*handle_ecma_str1, pos_start), -1);
}

/*
 * @tc.name: IndexOf_002
 * @tc.desc: Check whether the value returned through a source EcmaString made by CreateFromUtf16( , , , false) calling
 * IndexOf function with a target EcmaString made by CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, IndexOf_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // IndexOf(). Find EcmaString made by CreateFromUtf8() From EcmaString made by CreateFromUtf16( , , , false).
    uint8_t array_u8_target[4] = {1, 3, 39};
    uint16_t array_u16_not_comp_from_no1[] = {67, 65535, 127, 777, 1453, 44, 1, 3, 39, 80, 333};
    uint32_t length_ecma_str_u8_target = sizeof(array_u8_target) - 1;
    uint32_t length_ecma_str_u16_not_comp_from_no1 =
        sizeof(array_u16_not_comp_from_no1) / sizeof(array_u16_not_comp_from_no1[0]);
    JSHandle<EcmaString> handle_ecma_str(
        thread_, EcmaString::CreateFromUtf8(&array_u8_target[0], length_ecma_str_u8_target, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str1(thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp_from_no1[0],
                                                                               length_ecma_str_u16_not_comp_from_no1,
                                                                               ecma_vm_ptr, false));
    int32_t pos_start = 0;
    EXPECT_EQ(handle_ecma_str1->IndexOf(*handle_ecma_str, pos_start), 6);
    EXPECT_EQ(handle_ecma_str->IndexOf(*handle_ecma_str1, pos_start), -1);
    pos_start = -1;
    EXPECT_EQ(handle_ecma_str1->IndexOf(*handle_ecma_str, pos_start), 6);
    pos_start = 1;
    EXPECT_EQ(handle_ecma_str1->IndexOf(*handle_ecma_str, pos_start), 6);
    pos_start = 6;
    EXPECT_EQ(handle_ecma_str1->IndexOf(*handle_ecma_str, pos_start), 6);
    pos_start = 7;
    EXPECT_EQ(handle_ecma_str1->IndexOf(*handle_ecma_str, pos_start), -1);
}

/*
 * @tc.name: IndexOf_003
 * @tc.desc: Check whether the value returned through a source EcmaString made by CreateFromUtf16( , , , false) calling
 * IndexOf function with a target EcmaString made by CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, IndexOf_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    /* IndexOf(). Find EcmaString made by CreateFromUtf16( , , , false) From EcmaString made by
     * CreateFromUtf16( , , , false).
     */
    uint16_t array_u16_not_comp_target[] = {1453, 44};
    uint16_t array_u16_not_comp_from[] = {67, 65535, 127, 777, 1453, 44, 1, 3, 39, 80, 333};
    uint32_t length_ecma_str_u16_not_comp_target =
        sizeof(array_u16_not_comp_target) / sizeof(array_u16_not_comp_target[0]);
    uint32_t length_ecma_str_u16_not_comp_from = sizeof(array_u16_not_comp_from) / sizeof(array_u16_not_comp_from[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_target(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp_target[0], length_ecma_str_u16_not_comp_target,
                                             ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_from(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp_from[0], length_ecma_str_u16_not_comp_from,
                                             ecma_vm_ptr, false));
    int32_t pos_start = 0;
    EXPECT_EQ(handle_ecma_str_u16_not_comp_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 4);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_target->IndexOf(*handle_ecma_str_u16_not_comp_from, pos_start), -1);
    pos_start = -1;
    EXPECT_EQ(handle_ecma_str_u16_not_comp_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 4);
    pos_start = 1;
    EXPECT_EQ(handle_ecma_str_u16_not_comp_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 4);
    pos_start = 4;
    EXPECT_EQ(handle_ecma_str_u16_not_comp_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 4);
    pos_start = 5;
    EXPECT_EQ(handle_ecma_str_u16_not_comp_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), -1);
}

/*
 * @tc.name: IndexOf_004
 * @tc.desc: Check whether the value returned through a source EcmaString made by CreateFromUtf8() calling IndexOf
 * function with a target EcmaString made by CreateFromUtf16( , , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, IndexOf_004)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // IndexOf(). Find EcmaString made by CreateFromUtf16( , , , false) From EcmaString made by CreateFromUtf8().
    uint16_t ecma_str_u16_not_comp_target[] = {3, 39, 80};
    uint8_t array_u8_from[7] = {23, 25, 1, 3, 39, 80};
    uint32_t length_ecma_str_u16_not_comp_target =
        sizeof(ecma_str_u16_not_comp_target) / sizeof(ecma_str_u16_not_comp_target[0]);
    uint32_t length_ecma_str_u8_from = sizeof(array_u8_from) - 1;
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_target(
        thread_, EcmaString::CreateFromUtf16(&ecma_str_u16_not_comp_target[0], length_ecma_str_u16_not_comp_target,
                                             ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u8_from(
        thread_, EcmaString::CreateFromUtf8(&array_u8_from[0], length_ecma_str_u8_from, ecma_vm_ptr, true));
    int32_t pos_start = 0;
    EXPECT_EQ(handle_ecma_str_u8_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 3);
    EXPECT_EQ(handle_ecma_str_u16_not_comp_target->IndexOf(*handle_ecma_str_u8_from, pos_start), -1);
    pos_start = -1;
    EXPECT_EQ(handle_ecma_str_u8_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 3);
    pos_start = 1;
    EXPECT_EQ(handle_ecma_str_u8_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 3);
    pos_start = 3;
    EXPECT_EQ(handle_ecma_str_u8_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), 3);
    pos_start = 4;
    EXPECT_EQ(handle_ecma_str_u8_from->IndexOf(*handle_ecma_str_u16_not_comp_target, pos_start), -1);
}

/*
 * @tc.name: StringsAreEqual_001
 * @tc.desc: Check whether the bool returned through calling StringsAreEqual function with two EcmaStrings made by
 * CreateFromUtf8() is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqual_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqual().
    uint8_t array_u8_no1[4] = {45, 92, 78};
    uint8_t array_u8_no2[4] = {45, 92, 78};
    uint8_t array_u8_no3[5] = {45, 92, 78, 1};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u8_no2 = sizeof(array_u8_no2) - 1;
    uint32_t length_ecma_str_u8_no3 = sizeof(array_u8_no3) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no2(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no2[0], length_ecma_str_u8_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no3(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no3[0], length_ecma_str_u8_no3, ecma_vm_ptr, true));
    EXPECT_TRUE(EcmaString::StringsAreEqual(*handle_ecma_str_u8_no1, *handle_ecma_str_u8_no2));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u8_no1, *handle_ecma_str_u8_no3));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u8_no3, *handle_ecma_str_u8_no1));
}

/*
 * @tc.name: StringsAreEqual_002
 * @tc.desc: Check whether the bool returned through calling StringsAreEqual function with a EcmaString made by
 * CreateFromUtf8() and a EcmaString made by CreateFromUtf16(, , , true) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqual_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqual().
    uint8_t array_u8_no1[4] = {45, 92, 78};
    uint16_t array_u16_comp_no2[] = {45, 92, 78};
    uint16_t array_u16_comp_no3[] = {45, 92, 78, 1};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u16_comp_no2 = sizeof(array_u16_comp_no2) / sizeof(array_u16_comp_no2[0]);
    uint32_t length_ecma_str_u16_comp_no3 = sizeof(array_u16_comp_no3) / sizeof(array_u16_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no2(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no2[0], length_ecma_str_u16_comp_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no3(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no3[0], length_ecma_str_u16_comp_no3, ecma_vm_ptr, true));
    EXPECT_TRUE(EcmaString::StringsAreEqual(*handle_ecma_str_u8_no1, *handle_ecma_str_u16_comp_no2));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u8_no1, *handle_ecma_str_u16_comp_no3));
}

/*
 * @tc.name: StringsAreEqual_003
 * @tc.desc: Check whether the bool returned through calling StringsAreEqual function with two EcmaStrings made by
 * CreateFromUtf16(, , , true) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqual_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqual().
    uint16_t array_u16_comp_no1[] = {45, 92, 78};
    uint16_t array_u16_comp_no2[] = {45, 92, 78};
    uint16_t array_u16_comp_no3[] = {45, 92, 78, 1};
    uint32_t length_ecma_str_u16_comp_no1 = sizeof(array_u16_comp_no1) / sizeof(array_u16_comp_no1[0]);
    uint32_t length_ecma_str_u16_comp_no2 = sizeof(array_u16_comp_no2) / sizeof(array_u16_comp_no2[0]);
    uint32_t length_ecma_str_u16_comp_no3 = sizeof(array_u16_comp_no3) / sizeof(array_u16_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no1(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no1[0], length_ecma_str_u16_comp_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no2(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no2[0], length_ecma_str_u16_comp_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no3(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no3[0], length_ecma_str_u16_comp_no3, ecma_vm_ptr, true));
    EXPECT_TRUE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_comp_no1, *handle_ecma_str_u16_comp_no2));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_comp_no1, *handle_ecma_str_u16_comp_no3));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_comp_no3, *handle_ecma_str_u16_comp_no1));
}

/*
 * @tc.name: StringsAreEqual_004
 * @tc.desc: Check whether the bool returned through calling StringsAreEqual function with a EcmaString made by
 * CreateFromUtf8() and a EcmaString made by CreateFromUtf16(, , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqual_004)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqual().
    uint8_t array_u8_no1[4] = {45, 92, 78};
    uint16_t array_u16_not_comp_no1[] = {45, 92, 78};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u8_no1, *handle_ecma_str_u16_not_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_not_comp_no1, *handle_ecma_str_u8_no1));
}

/*
 * @tc.name: StringsAreEqual_005
 * @tc.desc: Check whether the bool returned through calling StringsAreEqual function with a EcmaString made by
 * CreateFromUtf16(, , , true) and a EcmaString made by CreateFromUtf16(, , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqual_005)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqual().
    uint16_t array_u16_comp_no1[] = {45, 92, 78};
    uint16_t array_u16_not_comp_no1[] = {45, 92, 78};
    uint32_t length_ecma_str_u16_comp_no1 = sizeof(array_u16_comp_no1) / sizeof(array_u16_comp_no1[0]);
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no1(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no1[0], length_ecma_str_u16_comp_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_comp_no1, *handle_ecma_str_u16_not_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_not_comp_no1, *handle_ecma_str_u16_comp_no1));
}

/*
 * @tc.name: StringsAreEqual_006
 * @tc.desc: Check whether the bool returned through calling StringsAreEqual function with two EcmaStrings made by
 * CreateFromUtf16(, , , false) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqual_006)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqual().
    uint16_t array_u16_not_comp_no1[] = {234, 345, 127, 2345, 65535, 5};
    uint16_t array_u16_not_comp_no2[] = {234, 345, 127, 2345, 65535, 5};
    uint16_t array_u16_not_comp_no3[] = {1, 234, 345, 127, 2345, 65535, 5};
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    uint32_t length_ecma_str_u16_not_comp_no2 = sizeof(array_u16_not_comp_no2) / sizeof(array_u16_not_comp_no2[0]);
    uint32_t length_ecma_str_u16_not_comp_no3 = sizeof(array_u16_not_comp_no3) / sizeof(array_u16_not_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no2(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no2[0], length_ecma_str_u16_not_comp_no2, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no3(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no3[0], length_ecma_str_u16_not_comp_no3, ecma_vm_ptr, false));
    EXPECT_TRUE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_not_comp_no1, *handle_ecma_str_u16_not_comp_no2));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_not_comp_no1, *handle_ecma_str_u16_not_comp_no3));
    EXPECT_FALSE(EcmaString::StringsAreEqual(*handle_ecma_str_u16_not_comp_no3, *handle_ecma_str_u16_not_comp_no1));
}

/*
 * @tc.name: StringsAreEqualUtf8_001
 * @tc.desc: Check whether the bool returned through calling StringsAreEqualUtf8 function with an EcmaString made by
 * CreateFromUtf8() and an Array(uint8_t) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqualUtf8_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqualUtf8(). EcmaString made by CreateFromUtf8(), Array:U8.
    uint8_t array_u8_no1[4] = {45, 92, 78};
    uint8_t array_u8_no2[5] = {45, 92, 78, 24};
    uint8_t array_u8_no3[3] = {45, 92};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u8_no2 = sizeof(array_u8_no2) - 1;
    uint32_t length_ecma_str_u8_no3 = sizeof(array_u8_no3) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no2(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no2[0], length_ecma_str_u8_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no3(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no3[0], length_ecma_str_u8_no3, ecma_vm_ptr, true));
    EXPECT_TRUE(
        EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u8_no1, &array_u8_no1[0], length_ecma_str_u8_no1, true));
    EXPECT_FALSE(
        EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u8_no1, &array_u8_no1[0], length_ecma_str_u8_no1, false));
    EXPECT_FALSE(
        EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u8_no2, &array_u8_no1[0], length_ecma_str_u8_no1, true));
    EXPECT_FALSE(
        EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u8_no3, &array_u8_no1[0], length_ecma_str_u8_no1, true));
}

/*
 * @tc.name: StringsAreEqualUtf8_002
 * @tc.desc: Check whether the bool returned through calling StringsAreEqualUtf8 function with an EcmaString made by
 * CreateFromUtf16( , , , true) and an Array(uint8_t) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqualUtf8_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqualUtf8(). EcmaString made by CreateFromUtf16( , , , true), Array:U8.
    uint8_t array_u8_no1[4] = {45, 92, 78};
    uint16_t array_u16_comp_no1[] = {45, 92, 78};
    uint16_t array_u16_comp_no2[] = {45, 92, 78, 24};
    uint16_t array_u16_comp_no3[] = {45, 92};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u16_comp_no1 = sizeof(array_u16_comp_no1) / sizeof(array_u16_comp_no1[0]);
    uint32_t length_ecma_str_u16_comp_no2 = sizeof(array_u16_comp_no2) / sizeof(array_u16_comp_no2[0]);
    uint32_t length_ecma_str_u16_comp_no3 = sizeof(array_u16_comp_no3) / sizeof(array_u16_comp_no3[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no1(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no1[0], length_ecma_str_u16_comp_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no2(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no2[0], length_ecma_str_u16_comp_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no3(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no3[0], length_ecma_str_u16_comp_no3, ecma_vm_ptr, true));
    EXPECT_TRUE(
        EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_comp_no1, &array_u8_no1[0], length_ecma_str_u8_no1, true));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_comp_no1, &array_u8_no1[0],
                                                 length_ecma_str_u8_no1, false));
    EXPECT_FALSE(
        EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_comp_no2, &array_u8_no1[0], length_ecma_str_u8_no1, true));
    EXPECT_FALSE(
        EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_comp_no3, &array_u8_no1[0], length_ecma_str_u8_no1, true));
}

/*
 * @tc.name: StringsAreEqualUtf8_003
 * @tc.desc: Check whether the bool returned through calling StringsAreEqualUtf8 function with an EcmaString made by
 * CreateFromUtf16( , , , false) and an Array(uint8_t) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqualUtf8_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqualUtf8(). EcmaString made by CreateFromUtf16( , , , false), Array:U8.
    uint8_t array_u8_no1[4] = {45, 92, 78};
    uint16_t array_u16_not_comp_no1[] = {45, 92, 78};
    uint16_t array_u16_not_comp_no2[] = {45, 92, 78, 24};
    uint16_t array_u16_not_comp_no3[] = {45, 92};
    uint16_t array_u16_not_comp_no4[] = {25645, 25692, 25678};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    uint32_t length_ecma_str_u16_not_comp_no2 = sizeof(array_u16_not_comp_no2) / sizeof(array_u16_not_comp_no2[0]);
    uint32_t length_ecma_str_u16_not_comp_no3 = sizeof(array_u16_not_comp_no3) / sizeof(array_u16_not_comp_no3[0]);
    uint32_t length_ecma_str_u16_not_comp_no4 = sizeof(array_u16_not_comp_no4) / sizeof(array_u16_not_comp_no4[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no2(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no2[0], length_ecma_str_u16_not_comp_no2, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no3(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no3[0], length_ecma_str_u16_not_comp_no3, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no4(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no4[0], length_ecma_str_u16_not_comp_no4, ecma_vm_ptr, false));
    EXPECT_TRUE(EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_not_comp_no1, &array_u8_no1[0],
                                                length_ecma_str_u8_no1, false));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_not_comp_no1, &array_u8_no1[0],
                                                 length_ecma_str_u8_no1, true));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_not_comp_no2, &array_u8_no1[0],
                                                 length_ecma_str_u8_no1, false));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_not_comp_no3, &array_u8_no1[0],
                                                 length_ecma_str_u8_no1, false));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf8(*handle_ecma_str_u16_not_comp_no4, &array_u8_no1[0],
                                                 length_ecma_str_u8_no1, false));
}

/*
 * @tc.name: StringsAreEqualUtf16_001
 * @tc.desc: Check whether the bool returned through calling StringsAreEqualUtf16 function with an EcmaString made by
 * CreateFromUtf8() and an Array(uint16_t) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqualUtf16_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqualUtf16(). EcmaString made by CreateFromUtf8, Array:U16(1-127).
    uint8_t array_u8_no1[4] = {45, 92, 78};
    uint8_t array_u8_no2[5] = {45, 92, 78, 24};
    uint8_t array_u8_no3[3] = {45, 92};
    uint16_t array_u16_not_comp_no1[] = {45, 92, 78};
    uint32_t length_ecma_str_u8_no1 = sizeof(array_u8_no1) - 1;
    uint32_t length_ecma_str_u8_no2 = sizeof(array_u8_no2) - 1;
    uint32_t length_ecma_str_u8_no3 = sizeof(array_u8_no3) - 1;
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    JSHandle<EcmaString> handle_ecma_str_u8_no1(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no1[0], length_ecma_str_u8_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no2(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no2[0], length_ecma_str_u8_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u8_no3(
        thread_, EcmaString::CreateFromUtf8(&array_u8_no3[0], length_ecma_str_u8_no3, ecma_vm_ptr, true));
    EXPECT_TRUE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u8_no1, &array_u16_not_comp_no1[0],
                                                 length_ecma_str_u16_not_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u8_no2, &array_u16_not_comp_no1[0],
                                                  length_ecma_str_u16_not_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u8_no3, &array_u16_not_comp_no1[0],
                                                  length_ecma_str_u16_not_comp_no1));
}

/*
 * @tc.name: StringsAreEqualUtf16_002
 * @tc.desc: Check whether the bool returned through calling StringsAreEqualUtf16 function with an EcmaString made by
 * CreateFromUtf16( , , , true) and an Array(uint16_t) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqualUtf16_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqualUtf16(). EcmaString made by CreateFromUtf16( , , , true), Array:U16(1-127).
    uint16_t array_u16_comp_no1[] = {45, 92, 78};
    uint16_t array_u16_comp_no2[] = {45, 92, 78, 24};
    uint16_t array_u16_comp_no3[] = {45, 92};
    uint16_t array_u16_comp_no4[] = {25645, 25692, 25678};  // 25645 % 256 == 45...
    uint32_t length_ecma_str_u16_comp_no1 = sizeof(array_u16_comp_no1) / sizeof(array_u16_comp_no1[0]);
    uint32_t length_ecma_str_u16_comp_no2 = sizeof(array_u16_comp_no2) / sizeof(array_u16_comp_no2[0]);
    uint32_t length_ecma_str_u16_comp_no3 = sizeof(array_u16_comp_no3) / sizeof(array_u16_comp_no3[0]);
    uint32_t length_ecma_str_u16_comp_no4 = sizeof(array_u16_comp_no4) / sizeof(array_u16_comp_no4[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no1(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no1[0], length_ecma_str_u16_comp_no1, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no2(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no2[0], length_ecma_str_u16_comp_no2, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no3(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no3[0], length_ecma_str_u16_comp_no3, ecma_vm_ptr, true));
    JSHandle<EcmaString> handle_ecma_str_u16_comp_no4(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp_no4[0], length_ecma_str_u16_comp_no4, ecma_vm_ptr, true));
    EXPECT_TRUE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_comp_no1, &array_u16_comp_no1[0],
                                                 length_ecma_str_u16_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_comp_no2, &array_u16_comp_no1[0],
                                                  length_ecma_str_u16_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_comp_no3, &array_u16_comp_no1[0],
                                                  length_ecma_str_u16_comp_no1));
    EXPECT_TRUE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_comp_no4, &array_u16_comp_no1[0],
                                                 length_ecma_str_u16_comp_no1));
}

/*
 * @tc.name: StringsAreEqualUtf16_003
 * @tc.desc: Check whether the bool returned through calling StringsAreEqualUtf16 function with an EcmaString made by
 * CreateFromUtf16( , , , false) and an Array(uint16_t) is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, StringsAreEqualUtf16_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // StringsAreEqualUtf16(). EcmaString made by CreateFromUtf16( , , , false), Array:U16(0-65535).
    uint16_t array_u16_not_comp_no1[] = {25645, 25692, 25678};
    uint16_t array_u16_not_comp_no2[] = {25645, 25692, 78};  // 25645 % 256 == 45...
    uint16_t array_u16_not_comp_no3[] = {25645, 25692, 25678, 65535};
    uint16_t array_u16_not_comp_no4[] = {25645, 25692};
    uint32_t length_ecma_str_u16_not_comp_no1 = sizeof(array_u16_not_comp_no1) / sizeof(array_u16_not_comp_no1[0]);
    uint32_t length_ecma_str_u16_not_comp_no2 = sizeof(array_u16_not_comp_no2) / sizeof(array_u16_not_comp_no2[0]);
    uint32_t length_ecma_str_u16_not_comp_no3 = sizeof(array_u16_not_comp_no3) / sizeof(array_u16_not_comp_no3[0]);
    uint32_t length_ecma_str_u16_not_comp_no4 = sizeof(array_u16_not_comp_no4) / sizeof(array_u16_not_comp_no4[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no1(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no1[0], length_ecma_str_u16_not_comp_no1, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no2(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no2[0], length_ecma_str_u16_not_comp_no2, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no3(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no3[0], length_ecma_str_u16_not_comp_no3, ecma_vm_ptr, false));
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_no4(
        thread_,
        EcmaString::CreateFromUtf16(&array_u16_not_comp_no4[0], length_ecma_str_u16_not_comp_no4, ecma_vm_ptr, false));
    EXPECT_TRUE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_not_comp_no1, &array_u16_not_comp_no1[0],
                                                 length_ecma_str_u16_not_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_not_comp_no1, &array_u16_not_comp_no2[0],
                                                  length_ecma_str_u16_not_comp_no2));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_not_comp_no2, &array_u16_not_comp_no1[0],
                                                  length_ecma_str_u16_not_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_not_comp_no3, &array_u16_not_comp_no1[0],
                                                  length_ecma_str_u16_not_comp_no1));
    EXPECT_FALSE(EcmaString::StringsAreEqualUtf16(*handle_ecma_str_u16_not_comp_no4, &array_u16_not_comp_no1[0],
                                                  length_ecma_str_u16_not_comp_no1));
}

/*
 * @tc.name: ComputeHashcodeUtf8
 * @tc.desc: Check whether the value returned through calling ComputeHashcodeUtf8 function with an Array(uint8_t) is
 * within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, ComputeHashcodeUtf8)
{
    uint8_t array_u8[] = {"abc"};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    uint32_t hash_expect = 0;
    for (uint32_t i = 0; i < length_ecma_str_u8; i++) {
        hash_expect = hash_expect * 31 + array_u8[i];
    }
    EXPECT_EQ(EcmaString::ComputeHashcodeUtf8(&array_u8[0], length_ecma_str_u8, true),
              static_cast<int32_t>(hash_expect));
}

/*
 * @tc.name: ComputeHashcodeUtf16
 * @tc.desc: Check whether the value returned through calling ComputeHashcodeUtf16 function with an Array(uint16_t) is
 * within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, ComputeHashcodeUtf16)
{
    uint16_t array_u16[] = {199, 1, 256, 65535, 777};
    uint32_t length_ecma_str_u16 = sizeof(array_u16) / sizeof(array_u16[0]);
    uint32_t hash_expect = 0;
    for (uint32_t i = 0; i < length_ecma_str_u16; i++) {
        hash_expect = hash_expect * 31 + array_u16[i];
    }
    EXPECT_EQ(EcmaString::ComputeHashcodeUtf16(&array_u16[0], length_ecma_str_u16), static_cast<int32_t>(hash_expect));
}

/*
 * @tc.name: GetHashcode_001
 * @tc.desc: Check whether the value returned through an EcmaString made by CreateFromUtf8() calling GetHashcode
 * function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetHashcode_001)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // GetHashcode(). EcmaString made by CreateFromUtf8().
    uint8_t array_u8[] = {"abc"};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    uint32_t hash_expect = 0;
    for (uint32_t i = 0; i < length_ecma_str_u8; i++) {
        hash_expect = hash_expect * 31 + array_u8[i];
    }
    EXPECT_EQ(handle_ecma_str_u8->GetHashcode(), static_cast<int32_t>(hash_expect));
}

/*
 * @tc.name: GetHashcode_002
 * @tc.desc: Check whether the value returned through an EcmaString made by CreateFromUtf16( , , , true) calling
 * GetHashcode function is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetHashcode_002)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // GetHashcode(). EcmaString made by CreateFromUtf16( , , , true).
    uint16_t array_u16_comp[] = {45, 92, 78, 24};
    uint32_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    uint32_t hash_expect = 0;
    for (uint32_t i = 0; i < length_ecma_str_u16_comp; i++) {
        hash_expect = hash_expect * 31 + array_u16_comp[i];
    }
    EXPECT_EQ(handle_ecma_str_u16_comp->GetHashcode(), static_cast<int32_t>(hash_expect));
}

/*
 * @tc.name: GetHashcode_003
 * @tc.desc: Check whether the value returned through an EcmaString made by CreateFromUtf16( , , , false) calling
 * GetHashcode function is within expectations before and after calling SetCompressedStringsEnabled function with
 * false.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetHashcode_003)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // GetHashcode(). EcmaString made by CreateFromUtf16( , , , false).
    uint16_t array_u16_not_comp[] = {199, 1, 256, 65535, 777};
    uint32_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    uint32_t hash_expect = 0;
    for (uint32_t i = 0; i < length_ecma_str_u16_not_comp; i++) {
        hash_expect = hash_expect * 31 + array_u16_not_comp[i];
    }
    EXPECT_EQ(handle_ecma_str_u16_not_comp->GetHashcode(), static_cast<int32_t>(hash_expect));

    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp_disable_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_EQ(handle_ecma_str_u16_not_comp_disable_comp->GetHashcode(), static_cast<int32_t>(hash_expect));
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: GetHashcode_004
 * @tc.desc: Check whether the value returned through an EcmaString made by CreateEmptyString() calling GetHashcode
 * function is within expectations before and after calling SetCompressedStringsEnabled function with false.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetHashcode_004)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // GetHashcode(). EcmaString made by CreateEmptyString().
    JSHandle<EcmaString> handle_ecma_str_empty(thread_, EcmaString::CreateEmptyString(ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_empty->GetHashcode(), 0);

    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    JSHandle<EcmaString> handle_ecma_str_empty_disable_comp(thread_, EcmaString::CreateEmptyString(ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_empty_disable_comp->GetHashcode(), 0);
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: GetHashcode_005
 * @tc.desc: Check whether the value returned through an EcmaString made by AllocStringObject(, true/false, ) calling
 * GetHashcode function is within expectations before and after calling SetCompressedStringsEnabled function with
 * false.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetHashcode_005)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    // GetHashcode(). EcmaString made by AllocStringObject().
    size_t size_alloc = 5;
    JSHandle<EcmaString> handle_ecma_str_alloc_comp(thread_,
                                                    EcmaString::AllocStringObject(size_alloc, true, ecma_vm_ptr));
    JSHandle<EcmaString> handle_ecma_str_alloc_not_comp(thread_,
                                                        EcmaString::AllocStringObject(size_alloc, false, ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_alloc_comp->GetHashcode(), 0);
    EXPECT_EQ(handle_ecma_str_alloc_not_comp->GetHashcode(), 0);

    EcmaString::SetCompressedStringsEnabled(false);  // Set compressed_strings_enabled_ false.
    JSHandle<EcmaString> handle_ecma_str_alloc_not_comp_disable_comp(
        thread_, EcmaString::AllocStringObject(size_alloc, false, ecma_vm_ptr));
    EXPECT_EQ(handle_ecma_str_alloc_not_comp_disable_comp->GetHashcode(), 0);
    EcmaString::SetCompressedStringsEnabled(true);  // Set compressed_strings_enabled_ true(default).
}

/*
 * @tc.name: GetCString
 * @tc.desc: Check whether the std::unique_ptr<char[]> returned through calling GetCString function is within
 * expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, GetCString)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    uint8_t array_u8[] = {"abc"};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    EXPECT_STREQ(PandaString(handle_ecma_str_u8->GetCString().get()).c_str(), "abc");

    uint16_t array_u16_comp[] = {97, 98, 99};
    uint32_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    EXPECT_STREQ(PandaString(handle_ecma_str_u16_comp->GetCString().get()).c_str(), "abc");

    uint16_t array_u16_not_comp[] = {97, 98, 99};
    uint32_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_STREQ(PandaString(handle_ecma_str_u16_not_comp->GetCString().get()).c_str(), "abc");
}

/*
 * @tc.name: SetIsInternString
 * @tc.desc: Call SetIsInternString function, check whether the bool returned through calling IsInternString function
 * is within expectations.
 * @tc.type: FUNC
 * @tc.require:
 */
TEST_F(EcmaStringTest, SetIsInternString)
{
    EcmaVM *ecma_vm_ptr = EcmaVM::Cast(instance_);

    uint8_t array_u8[] = {"abc"};
    uint32_t length_ecma_str_u8 = sizeof(array_u8) - 1;
    JSHandle<EcmaString> handle_ecma_str_u8(
        thread_, EcmaString::CreateFromUtf8(&array_u8[0], length_ecma_str_u8, ecma_vm_ptr, true));
    EXPECT_FALSE(handle_ecma_str_u8->IsInternString());
    handle_ecma_str_u8->SetIsInternString();
    EXPECT_TRUE(handle_ecma_str_u8->IsInternString());

    uint16_t array_u16_comp[] = {97, 98, 99};
    uint32_t length_ecma_str_u16_comp = sizeof(array_u16_comp) / sizeof(array_u16_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_comp[0], length_ecma_str_u16_comp, ecma_vm_ptr, true));
    EXPECT_FALSE(handle_ecma_str_u16_comp->IsInternString());
    handle_ecma_str_u16_comp->SetIsInternString();
    EXPECT_TRUE(handle_ecma_str_u16_comp->IsInternString());

    uint16_t array_u16_not_comp[] = {97, 98, 99};
    uint32_t length_ecma_str_u16_not_comp = sizeof(array_u16_not_comp) / sizeof(array_u16_not_comp[0]);
    JSHandle<EcmaString> handle_ecma_str_u16_not_comp(
        thread_, EcmaString::CreateFromUtf16(&array_u16_not_comp[0], length_ecma_str_u16_not_comp, ecma_vm_ptr, false));
    EXPECT_FALSE(handle_ecma_str_u16_not_comp->IsInternString());
    handle_ecma_str_u16_not_comp->SetIsInternString();
    EXPECT_TRUE(handle_ecma_str_u16_not_comp->IsInternString());
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers,modernize-avoid-c-arrays)
