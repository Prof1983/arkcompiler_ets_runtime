/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_module.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_locale.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

using panda::ecmascript::EcmaModule;
using panda::ecmascript::EcmaString;
using panda::ecmascript::JSFunction;
using panda::ecmascript::JSHandle;
using panda::ecmascript::JSLocale;
using panda::ecmascript::ModuleManager;
using panda::ecmascript::NameDictionary;
using panda::ecmascript::ObjectFactory;
using panda::ecmascript::PropertyAttributes;

namespace panda::test {
class EcmaModuleTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

EcmaModule *EcmaModuleCreate(JSThread *thread)
{
    ObjectFactory *object_factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaModule> handle_ecma_module = object_factory->NewEmptyEcmaModule();
    return *handle_ecma_module;
}

/*
 * Feature: EcmaModule
 * Function: AddItem
 * SubFunction: GetItem
 * FunctionPoints: Add Item To EcmaModule
 * CaseDescription: Add an item for a EcmaModule that has called "SetNameDictionary" function in the TEST_F, and
 *                  check its value through 'GetItem' function.
 */
TEST_F(EcmaModuleTest, AddItem_001)
{
    int num_of_elements_dict = 4;
    PandaString c_str_item_name = "key1";
    int int_item_value = 1;
    JSHandle<EcmaModule> handle_ecma_module(thread_, EcmaModuleCreate(thread_));
    JSHandle<NameDictionary> handle_name_dict(NameDictionary::Create(thread_, num_of_elements_dict));
    JSHandle<JSTaggedValue> handle_tag_val_item_name(
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(c_str_item_name));
    JSHandle<JSTaggedValue> handle_tag_val_item_value(thread_, JSTaggedValue(int_item_value));

    handle_ecma_module->SetNameDictionary(thread_, handle_name_dict);  // Call SetNameDictionary in TEST_F
    EcmaModule::AddItem(thread_, handle_ecma_module, handle_tag_val_item_name, handle_tag_val_item_value);
    EXPECT_EQ(handle_ecma_module->GetItem(thread_, handle_tag_val_item_name)->GetNumber(), int_item_value);
}

/*
 * Feature: EcmaModule
 * Function: AddItem
 * SubFunction: GetItem
 * FunctionPoints: Add Item To EcmaModule
 * CaseDescription: Add an item for a EcmaModule that has not called "SetNameDictionary" function in the TEST_F,
 *                  and check its value through 'GetItem' function.
 */
TEST_F(EcmaModuleTest, AddItem_002)
{
    PandaString c_str_item_name = "cStrItemName";
    int int_item_value = 1;
    JSHandle<EcmaModule> handle_ecma_module(thread_, EcmaModuleCreate(thread_));
    JSHandle<JSTaggedValue> handle_tag_val_item_name(
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(c_str_item_name));
    JSHandle<JSTaggedValue> handle_tag_val_item_value(thread_, JSTaggedValue(int_item_value));

    // This EcmaModule calls 'SetNameDictionary' function through 'AddItem' function of "ecma_module.cpp".
    EcmaModule::AddItem(thread_, handle_ecma_module, handle_tag_val_item_name, handle_tag_val_item_value);
    EXPECT_EQ(handle_ecma_module->GetItem(thread_, handle_tag_val_item_name)->GetNumber(), int_item_value);
}

/*
 * Feature: EcmaModule
 * Function: RemoveItem
 * SubFunction: AddItem/GetItem
 * FunctionPoints: Remove Item From EcmaModule
 * CaseDescription: Add an item for an empty EcmaModule through 'AddItem' function, then remove the item from the
 *                  EcmaModule through 'RemoveItem' function, finally check whether the item obtained from the
 *                  EcmaModule through 'GetItem' function is undefined.
 */
TEST_F(EcmaModuleTest, RemoveItem)
{
    ObjectFactory *obj_factory = thread_->GetEcmaVM()->GetFactory();

    PandaString c_str_item_name = "cStrItemName";
    int int_item_value = 1;
    JSHandle<EcmaModule> handle_ecma_module(thread_, EcmaModuleCreate(thread_));
    JSHandle<JSTaggedValue> handle_tag_val_item_name(obj_factory->NewFromCanBeCompressString(c_str_item_name));
    JSHandle<JSTaggedValue> handle_tag_val_item_value(thread_, JSTaggedValue(int_item_value));

    EcmaModule::AddItem(thread_, handle_ecma_module, handle_tag_val_item_name, handle_tag_val_item_value);
    EcmaModule::RemoveItem(thread_, handle_ecma_module, handle_tag_val_item_name);
    EXPECT_TRUE(handle_ecma_module->GetItem(thread_, handle_tag_val_item_name)->IsUndefined());
}

/*
 * Feature: EcmaModule
 * Function: SetNameDictionary
 * SubFunction: NameDictionary::Put/GetNameDictionary
 * FunctionPoints: Set NameDictionary For EcmaModule
 * CaseDescription: Create a source key, a source value, a source NameDictionary and a target EcmaModule, change the
 *                  NameDictionary through 'NameDictionary::Put' function, set the changed source NameDictionary as
 *                  this target EcmaModule's NameDictionary through 'SetNameDictionary' function, check whether the
 *                  result returned through 'GetNameDictionary' function from the target EcmaModule are within
 *                   expectations.
 */
TEST_F(EcmaModuleTest, SetNameDictionary)
{
    ObjectFactory *obj_factory = thread_->GetEcmaVM()->GetFactory();

    int num_of_elements_dict = 4;
    JSHandle<NameDictionary> handle_name_dict(NameDictionary::Create(thread_, num_of_elements_dict));
    JSHandle<JSTaggedValue> handle_obj_func = thread_->GetEcmaVM()->GetGlobalEnv()->GetObjectFunction();
    PandaString key_array1 = "hello1";
    JSHandle<EcmaString> string_key1 = obj_factory->NewFromCanBeCompressString(key_array1);
    JSHandle<JSTaggedValue> key1(string_key1);
    JSHandle<JSTaggedValue> value1(
        obj_factory->NewJSObjectByConstructor(JSHandle<JSFunction>(handle_obj_func), handle_obj_func));
    JSHandle<NameDictionary> handle_name_dictionary_from(
        NameDictionary::Put(thread_, handle_name_dict, key1, value1, PropertyAttributes::Default()));
    JSHandle<EcmaModule> handle_ecma_module(thread_, EcmaModuleCreate(thread_));

    handle_ecma_module->SetNameDictionary(thread_, handle_name_dictionary_from);
    JSHandle<NameDictionary> handle_name_dictionary_to(
        thread_, NameDictionary::Cast(handle_ecma_module->GetNameDictionary().GetTaggedObject()));
    EXPECT_EQ(handle_name_dictionary_to->EntriesCount(), 1);
    int entry1 = handle_name_dictionary_to->FindEntry(key1.GetTaggedValue());
    EXPECT_TRUE(key1.GetTaggedValue() == handle_name_dictionary_to->GetKey(entry1));
    EXPECT_TRUE(value1.GetTaggedValue() == handle_name_dictionary_to->GetValue(entry1));
}

/*
 * Feature: ModuleManager
 * Function: AddModule
 * SubFunction: AddItem/GetModule/GetItem
 * FunctionPoints: Add EcmaModule To ModuleManager
 * CaseDescription: Create 2 source EcmaModules that both add an item, create a ModuleManager that add the 2 source
 *                  EcmaModule, check whether the items of EcmaModules obtained from the ModuleManager through
 *                  'GetModule' function and 'GetItem' function are within expectations.
 */
TEST_F(EcmaModuleTest, ModuleManager_AddModule)
{
    ObjectFactory *obj_factory = thread_->GetEcmaVM()->GetFactory();
    ModuleManager *module_manager = thread_->GetEcmaVM()->GetModuleManager();

    int num_of_elements_dict1 = 4;
    int num_of_elements_dict2 = 4;
    PandaString c_str_item_name1 = "cStrItemName1";
    PandaString c_str_item_name2 = "cStrItemName2";
    int int_item_value1 = 1;
    int int_item_value2 = 2;
    JSHandle<NameDictionary> handle_name_dict1(NameDictionary::Create(thread_, num_of_elements_dict1));
    JSHandle<NameDictionary> handle_name_dict2(NameDictionary::Create(thread_, num_of_elements_dict2));
    JSHandle<JSTaggedValue> handle_item_name1(
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(c_str_item_name1));
    JSHandle<JSTaggedValue> handle_item_name2(
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(c_str_item_name2));
    JSHandle<JSTaggedValue> handle_item_value1(thread_, JSTaggedValue(int_item_value1));
    JSHandle<JSTaggedValue> handle_item_value2(thread_, JSTaggedValue(int_item_value2));
    JSHandle<EcmaModule> handle_ecma_module_add_from1(thread_, EcmaModuleCreate(thread_));
    JSHandle<EcmaModule> handle_ecma_module_add_from2(thread_, EcmaModuleCreate(thread_));
    handle_ecma_module_add_from1->SetNameDictionary(thread_, handle_name_dict1);
    handle_ecma_module_add_from2->SetNameDictionary(thread_, handle_name_dict2);

    EcmaModule::AddItem(thread_, handle_ecma_module_add_from1, handle_item_name1, handle_item_value1);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_add_from1(thread_,
                                                                 handle_ecma_module_add_from1.GetTaggedValue());
    std::string std_str_name_ecma_module_add1 = "NameEcmaModule1";
    JSHandle<JSTaggedValue> handle_tag_val_name_ecma_module_add1(
        obj_factory->NewFromStdString(std_str_name_ecma_module_add1));
    EcmaModule::AddItem(thread_, handle_ecma_module_add_from2, handle_item_name2, handle_item_value2);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_add_from2(thread_,
                                                                 handle_ecma_module_add_from2.GetTaggedValue());
    std::string std_str_name_ecma_module_add2 = "NameEcmaModule2";
    JSHandle<JSTaggedValue> handle_tag_val_name_ecma_module_add2(
        obj_factory->NewFromStdString(std_str_name_ecma_module_add2));

    module_manager->AddModule(handle_tag_val_name_ecma_module_add1, handle_tag_val_ecma_module_add_from1);
    module_manager->AddModule(handle_tag_val_name_ecma_module_add2, handle_tag_val_ecma_module_add_from2);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_get1 =
        module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add1);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_get2 =
        module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add2);
    EXPECT_EQ(
        JSHandle<EcmaModule>::Cast(handle_tag_val_ecma_module_get1)->GetItem(thread_, handle_item_name1)->GetNumber(),
        int_item_value1);
    EXPECT_EQ(
        JSHandle<EcmaModule>::Cast(handle_tag_val_ecma_module_get2)->GetItem(thread_, handle_item_name2)->GetNumber(),
        int_item_value2);
}

/*
 * Feature: ModuleManager
 * Function: RemoveModule
 * SubFunction: AddItem/GetModule/AddModule/GetItem
 * FunctionPoints: Remove EcmaModule From ModuleManager
 * CaseDescription: Create two source EcmaModules that add different items, create a ModuleManager that add the two
 *                  source EcmaModules, check whether the properties of the EcmaModules obtained from the ModuleManager
 *                  through 'GetModule' function are within expectations while removing EcmaModules from the
 *                  ModuleManager through 'RemoveModule' function one by one.
 */
TEST_F(EcmaModuleTest, ModuleManager_RemoveModule)
{
    ObjectFactory *obj_factory = thread_->GetEcmaVM()->GetFactory();
    ModuleManager *module_manager = thread_->GetEcmaVM()->GetModuleManager();

    std::string std_str_name_ecma_module_add1 = "NameEcmaModule1";
    std::string std_str_name_ecma_module_add2 = "NameEcmaModule2";
    int int_item_value1 = 1;
    int int_item_value2 = 2;
    int num_of_elements_dict1 = 4;
    int num_of_elements_dict2 = 4;
    JSHandle<JSTaggedValue> handle_tag_val_name_ecma_module_add1(
        obj_factory->NewFromStdString(std_str_name_ecma_module_add1));
    JSHandle<JSTaggedValue> handle_tag_val_name_ecma_module_add2(
        obj_factory->NewFromStdString(std_str_name_ecma_module_add2));
    JSHandle<JSTaggedValue> handle_tag_val_item_name1(obj_factory->NewFromCanBeCompressString("name1"));
    JSHandle<JSTaggedValue> handle_tag_val_item_name2(obj_factory->NewFromCanBeCompressString("name2"));
    JSHandle<JSTaggedValue> handle_tag_val_item_value1(thread_, JSTaggedValue(int_item_value1));
    JSHandle<JSTaggedValue> handle_tag_val_item_value2(thread_, JSTaggedValue(int_item_value2));
    JSHandle<EcmaModule> handle_ecma_module_add_from1(thread_, EcmaModuleCreate(thread_));
    JSHandle<EcmaModule> handle_ecma_module_add_from2(thread_, EcmaModuleCreate(thread_));
    JSHandle<NameDictionary> handle_name_dict1(NameDictionary::Create(thread_, num_of_elements_dict1));
    JSHandle<NameDictionary> handle_name_dict2(NameDictionary::Create(thread_, num_of_elements_dict2));
    handle_ecma_module_add_from1->SetNameDictionary(thread_, handle_name_dict1);
    handle_ecma_module_add_from2->SetNameDictionary(thread_, handle_name_dict2);
    EcmaModule::AddItem(thread_, handle_ecma_module_add_from1, handle_tag_val_item_name1, handle_tag_val_item_value1);
    EcmaModule::AddItem(thread_, handle_ecma_module_add_from2, handle_tag_val_item_name2, handle_tag_val_item_value2);
    JSHandle<JSTaggedValue> handle_tagged_value_ecma_module_add_from1(thread_,
                                                                      handle_ecma_module_add_from1.GetTaggedValue());
    JSHandle<JSTaggedValue> handle_tagged_value_ecma_module_add_from2(thread_,
                                                                      handle_ecma_module_add_from2.GetTaggedValue());

    module_manager->AddModule(handle_tag_val_name_ecma_module_add1, handle_tagged_value_ecma_module_add_from1);
    module_manager->AddModule(handle_tag_val_name_ecma_module_add2, handle_tagged_value_ecma_module_add_from2);
    EXPECT_EQ(JSHandle<EcmaModule>::Cast(module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add1))
                  ->GetItem(thread_, handle_tag_val_item_name1)
                  ->GetNumber(),
              int_item_value1);
    EXPECT_EQ(JSHandle<EcmaModule>::Cast(module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add2))
                  ->GetItem(thread_, handle_tag_val_item_name2)
                  ->GetNumber(),
              int_item_value2);

    module_manager->RemoveModule(handle_tag_val_name_ecma_module_add1);
    EXPECT_TRUE(module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add1)->IsUndefined());
    EXPECT_EQ(JSHandle<EcmaModule>::Cast(module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add2))
                  ->GetItem(thread_, handle_tag_val_item_name2)
                  ->GetNumber(),
              int_item_value2);

    module_manager->RemoveModule(handle_tag_val_name_ecma_module_add2);
    EXPECT_TRUE(module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add1)->IsUndefined());
    EXPECT_TRUE(module_manager->GetModule(thread_, handle_tag_val_name_ecma_module_add2)->IsUndefined());
}

/*
 * Feature: ModuleManager
 * Function: SetCurrentExportModuleName
 * SubFunction: GetCurrentExportModuleName
 * FunctionPoints: Get Current ExportModuleName Of ModuleManager
 * CaseDescription: Create a ModuleManager, check whether the ExportModuleName obtained from the ModuleManager through
 *                  'GetCurrentExportModuleName' function is within expectations while changing the Current
 *                  ExportModuleName of the ModuleManager through 'SetCurrentExportModuleName' function.
 */
TEST_F(EcmaModuleTest, ModuleManager_SetCurrentExportModuleName)
{
    ModuleManager *module_manager = thread_->GetEcmaVM()->GetModuleManager();

    std::string_view str_view_name_ecma_module1 = "NameEcmaModule1";
    std::string_view str_view_name_ecma_module2 = "NameEcmaModule2";
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module1);
    EXPECT_STREQ(module_manager->GetCurrentExportModuleName().c_str(), PandaString(str_view_name_ecma_module1).c_str());
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module2);
    EXPECT_STREQ(module_manager->GetCurrentExportModuleName().c_str(), PandaString(str_view_name_ecma_module2).c_str());
}

/*
 * Feature: ModuleManager
 * Function: GetPrevExportModuleName
 * SubFunction: SetCurrentExportModuleName
 * FunctionPoints: Get Previous ExportModuleName Of ModuleManager
 * CaseDescription: Create a ModuleManager, check whether the previous ExportModuleName obtained from the ModuleManager
 *                  through 'GetPrevExportModuleName' function is within expectations while changing the Current
 *                  ExportModuleName of the ModuleManager through 'SetCurrentExportModuleName' function.
 */
TEST_F(EcmaModuleTest, ModuleManager_GetPrevExportModuleName)
{
    ModuleManager *module_manager = thread_->GetEcmaVM()->GetModuleManager();

    std::string_view str_view_name_ecma_module1 = "NameEcmaModule1";
    std::string_view str_view_name_ecma_module2 = "NameEcmaModule2";
    std::string_view str_view_name_ecma_module3 = "NameEcmaModule3";
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module1);
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module2);
    EXPECT_STREQ(module_manager->GetPrevExportModuleName().c_str(), PandaString(str_view_name_ecma_module1).c_str());
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module3);
    EXPECT_STREQ(module_manager->GetPrevExportModuleName().c_str(), PandaString(str_view_name_ecma_module2).c_str());
}

/*
 * Feature: ModuleManager
 * Function: RestoreCurrentExportModuleName
 * SubFunction: SetCurrentExportModuleName/GetCurrentExportModuleName
 * FunctionPoints: Restore Current ExportModuleName Of ModuleManager
 * CaseDescription: Create a ModuleManager, check whether the current ExportModuleName obtained from the ModuleManager
 *                  through 'GetCurrentExportModuleName' function is within expectations while changing the Current
 *                  ExportModuleName of the ModuleManager through 'SetCurrentExportModuleName' function and
 *                  'RestoreCurrentExportModuleName' function.
 */
TEST_F(EcmaModuleTest, ModuleManager_RestoreCurrentExportModuleName)
{
    ModuleManager *module_manager = thread_->GetEcmaVM()->GetModuleManager();

    std::string_view str_view_name_ecma_module1 = "NameEcmaModule1";
    std::string_view str_view_name_ecma_module2 = "NameEcmaModule2";
    std::string_view str_view_name_ecma_module3 = "NameEcmaModule3";
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module1);
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module2);
    module_manager->SetCurrentExportModuleName(str_view_name_ecma_module3);
    EXPECT_STREQ(module_manager->GetCurrentExportModuleName().c_str(), PandaString(str_view_name_ecma_module3).c_str());
    module_manager->RestoreCurrentExportModuleName();
    EXPECT_STREQ(module_manager->GetCurrentExportModuleName().c_str(), PandaString(str_view_name_ecma_module2).c_str());
    module_manager->RestoreCurrentExportModuleName();
    EXPECT_STREQ(module_manager->GetCurrentExportModuleName().c_str(), PandaString(str_view_name_ecma_module1).c_str());
}

/*
 * Feature: ModuleManager
 * Function: AddModuleItem
 * SubFunction: SetCurrentExportModuleName/GetModule/GetModuleItem
 * FunctionPoints: Add ModuleItem For Current EcmaModule Of ModuleManager
 * CaseDescription: Create a ModuleManager, set the current EcmaModule for the ModuleManager through
 *                  'SetCurrentExportModuleName' function, add source ModuleItems for the current EcmaModule Of the
 *                  ModuleManager, check whether the ModuleItems obtained through 'GetModuleItem' function from the
 *                  ModuleManager are within expectations.
 */
TEST_F(EcmaModuleTest, ModuleManager_AddModuleItem)
{
    ObjectFactory *obj_factory = thread_->GetEcmaVM()->GetFactory();
    ModuleManager *module_manager = thread_->GetEcmaVM()->GetModuleManager();

    constexpr int INT_ITEM_VALUE_11 = 11;
    constexpr int INT_ITEM_VALUE_12 = 12;
    constexpr int INT_ITEM_VALUE_21 = 21;
    constexpr int INT_ITEM_VALUE_22 = 22;
    JSHandle<JSTaggedValue> handle_tag_val_item_name11(obj_factory->NewFromCanBeCompressString("cStrItemName11"));
    JSHandle<JSTaggedValue> handle_tag_val_item_name12(obj_factory->NewFromCanBeCompressString("cStrItemName12"));
    JSHandle<JSTaggedValue> handle_tag_val_item_name21(obj_factory->NewFromCanBeCompressString("cStrItemName21"));
    JSHandle<JSTaggedValue> handle_tag_val_item_name22(obj_factory->NewFromCanBeCompressString("cStrItemName22"));
    JSHandle<JSTaggedValue> handle_tag_val_item_value11(thread_, JSTaggedValue(INT_ITEM_VALUE_11));
    JSHandle<JSTaggedValue> handle_tag_val_item_value12(thread_, JSTaggedValue(INT_ITEM_VALUE_12));
    JSHandle<JSTaggedValue> handle_tag_val_item_value21(thread_, JSTaggedValue(INT_ITEM_VALUE_21));
    JSHandle<JSTaggedValue> handle_tag_val_item_value22(thread_, JSTaggedValue(INT_ITEM_VALUE_22));
    JSHandle<EcmaString> handle_ecma_str_name_ecma_module1 = obj_factory->NewFromString("cStrNameEcmaModule1");
    JSHandle<EcmaString> handle_ecma_str_name_ecma_module2 = obj_factory->NewFromString("cStrNameEcmaModule2");
    std::string std_str_module_file_name1 = JSLocale::ConvertToStdString(handle_ecma_str_name_ecma_module1);
    std::string std_str_module_file_name2 = JSLocale::ConvertToStdString(handle_ecma_str_name_ecma_module2);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_name1(handle_ecma_str_name_ecma_module1);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_name2(handle_ecma_str_name_ecma_module2);

    // Test when the module is created through 'NewEmptyEcmaModule' function called at TEST_F.
    JSHandle<EcmaModule> handle_ecma_module1(thread_, EcmaModuleCreate(thread_));
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module1(thread_, handle_ecma_module1.GetTaggedValue());
    module_manager->AddModule(handle_tag_val_ecma_module_name1, handle_tag_val_ecma_module1);
    module_manager->SetCurrentExportModuleName(std_str_module_file_name1);
    module_manager->AddModuleItem(thread_, handle_tag_val_item_name11, handle_tag_val_item_value11);
    module_manager->AddModuleItem(thread_, handle_tag_val_item_name12, handle_tag_val_item_value12);

    EXPECT_EQ(
        module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module1, handle_tag_val_item_name11)->GetNumber(),
        INT_ITEM_VALUE_11);
    EXPECT_EQ(
        module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module1, handle_tag_val_item_name12)->GetNumber(),
        INT_ITEM_VALUE_12);

    // Test when the module is created through 'NewEmptyEcmaModule' function called at "ecma_module.cpp".
    module_manager->SetCurrentExportModuleName(std_str_module_file_name2);
    module_manager->AddModuleItem(thread_, handle_tag_val_item_name21, handle_tag_val_item_value21);
    module_manager->AddModuleItem(thread_, handle_tag_val_item_name22, handle_tag_val_item_value22);

    JSHandle<JSTaggedValue> handle_tag_val_ecma_module2 =
        module_manager->GetModule(thread_, handle_tag_val_ecma_module_name2);
    EXPECT_EQ(
        module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module1, handle_tag_val_item_name11)->GetNumber(),
        INT_ITEM_VALUE_11);
    EXPECT_EQ(
        module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module1, handle_tag_val_item_name12)->GetNumber(),
        INT_ITEM_VALUE_12);
    EXPECT_EQ(
        module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module2, handle_tag_val_item_name21)->GetNumber(),
        INT_ITEM_VALUE_21);
    EXPECT_EQ(
        module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module2, handle_tag_val_item_name22)->GetNumber(),
        INT_ITEM_VALUE_22);
}

/*
 * Feature: ModuleManager
 * Function: CopyModule
 * SubFunction: AddItem/SetCurrentExportModuleName/GetModule/GetModuleItem
 * FunctionPoints: Copy EcmaModule To ModuleManager
 * CaseDescription: Create two source EcmaModules and one target ModuleManager, prepare the two source EcmaModules
 *                  through 'AddItem' function, check whether the the ModuleItems obtained through 'GetModuleItem' from
 *                  the target ModuleManager are within expectations while changing the target ModuleManager through
 *                  'SetCurrentExportModuleName' function and 'CopyModule' function.
 */
TEST_F(EcmaModuleTest, ModuleManager_CopyModule)
{
    ObjectFactory *obj_factory = thread_->GetEcmaVM()->GetFactory();
    ModuleManager *module_manager = thread_->GetEcmaVM()->GetModuleManager();

    constexpr int INT_ITEM_VALUE_11 = 11;
    constexpr int INT_ITEM_VALUE_12 = 12;
    constexpr int INT_ITEM_VALUE_21 = 21;
    constexpr int INT_ITEM_VALUE_22 = 22;
    std::string_view file_name_ecma_module_copy_to1 = "fileNameEcmaModuleCopyTo1";
    std::string_view file_name_ecma_module_copy_to2 = "fileNameEcmaModuleCopyTo2";
    JSHandle<JSTaggedValue> handle_tag_val_item_name11(obj_factory->NewFromCanBeCompressString("ItemName11"));
    JSHandle<JSTaggedValue> handle_tag_val_item_name12(obj_factory->NewFromCanBeCompressString("ItemName12"));
    JSHandle<JSTaggedValue> handle_tag_val_item_name21(obj_factory->NewFromCanBeCompressString("ItemName21"));
    JSHandle<JSTaggedValue> handle_tag_val_item_name22(obj_factory->NewFromCanBeCompressString("ItemName22"));
    JSHandle<JSTaggedValue> handle_tag_val_item_value11(thread_, JSTaggedValue(INT_ITEM_VALUE_11));
    JSHandle<JSTaggedValue> handle_tag_val_item_value12(thread_, JSTaggedValue(INT_ITEM_VALUE_12));
    JSHandle<JSTaggedValue> handle_tag_val_item_value21(thread_, JSTaggedValue(INT_ITEM_VALUE_21));
    JSHandle<JSTaggedValue> handle_tag_val_item_value22(thread_, JSTaggedValue(INT_ITEM_VALUE_22));
    JSHandle<EcmaModule> handle_ecma_module_copy_from1(thread_, EcmaModuleCreate(thread_));
    JSHandle<EcmaModule> handle_ecma_module_copy_from2(thread_, EcmaModuleCreate(thread_));
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_copy_from1(thread_,
                                                                  handle_ecma_module_copy_from1.GetTaggedValue());
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_copy_from2(thread_,
                                                                  handle_ecma_module_copy_from2.GetTaggedValue());
    EcmaModule::AddItem(thread_, handle_ecma_module_copy_from1, handle_tag_val_item_name11,
                        handle_tag_val_item_value11);
    EcmaModule::AddItem(thread_, handle_ecma_module_copy_from1, handle_tag_val_item_name12,
                        handle_tag_val_item_value12);
    EcmaModule::AddItem(thread_, handle_ecma_module_copy_from2, handle_tag_val_item_name21,
                        handle_tag_val_item_value21);
    EcmaModule::AddItem(thread_, handle_ecma_module_copy_from2, handle_tag_val_item_name22,
                        handle_tag_val_item_value22);

    module_manager->SetCurrentExportModuleName(file_name_ecma_module_copy_to1);
    module_manager->CopyModule(thread_, handle_tag_val_ecma_module_copy_from1);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_copy_to1 = module_manager->GetModule(
        thread_,
        JSHandle<JSTaggedValue>::Cast(obj_factory->NewFromString(PandaString(file_name_ecma_module_copy_to1))));
    EXPECT_EQ(INT_ITEM_VALUE_11,
              module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module_copy_to1, handle_tag_val_item_name11)
                  ->GetNumber());
    EXPECT_EQ(INT_ITEM_VALUE_12,
              module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module_copy_to1, handle_tag_val_item_name12)
                  ->GetNumber());

    module_manager->SetCurrentExportModuleName(file_name_ecma_module_copy_to2);
    module_manager->CopyModule(thread_, handle_tag_val_ecma_module_copy_from2);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_module_copy_to2 = module_manager->GetModule(
        thread_,
        JSHandle<JSTaggedValue>::Cast(obj_factory->NewFromString(PandaString(file_name_ecma_module_copy_to2))));
    EXPECT_EQ(INT_ITEM_VALUE_11,
              module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module_copy_to1, handle_tag_val_item_name11)
                  ->GetNumber());
    EXPECT_EQ(INT_ITEM_VALUE_12,
              module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module_copy_to1, handle_tag_val_item_name12)
                  ->GetNumber());
    EXPECT_EQ(INT_ITEM_VALUE_21,
              module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module_copy_to2, handle_tag_val_item_name21)
                  ->GetNumber());
    EXPECT_EQ(INT_ITEM_VALUE_22,
              module_manager->GetModuleItem(thread_, handle_tag_val_ecma_module_copy_to2, handle_tag_val_item_name22)
                  ->GetNumber());
}
}  // namespace panda::test
