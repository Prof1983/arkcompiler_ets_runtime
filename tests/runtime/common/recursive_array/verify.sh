#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.

set -eo pipefail

expected="
1,4,2,,3,5,1
2,1,4,,5,1,3
4,2,1,,1,3,5"

actual=`cat $1`
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mrestargstest failed\033[0m"
    exit 1;
fi
