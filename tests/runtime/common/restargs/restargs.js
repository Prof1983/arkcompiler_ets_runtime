function sum_va(...args) {
    if (!args.length) {
        return undefined;
    }
    let acc = args[0];
    for (let i = 1; i < args.length; ++i) {
        acc = acc + args[i];
    }
    return acc;
}

print(sum_va());
print(sum_va(1, 2, 3, 4));

let arr = [];
for (let i = 0; i < 4; ++i) {
    arr[i] = i;
}
print(sum_va(...arr));

arr = [];
for (let i = 0; i < 4; ++i) {
    arr[i] = i.toString();
}
print(sum_va(...arr));