let run = (f) => {
    try {
        f();
    } catch (e) {
        print(e);
    }
}

function f1() {f1();}
let f2 = () => f2();
function f3() {new f3();}

run(f1);
run(f2);
run(f3);
run(f1);
run(f2);
run(f3);

