#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.

set -eo pipefail

expected="RangeError: Maximum call stack size exceeded
RangeError: Maximum call stack size exceeded
RangeError: Maximum call stack size exceeded
RangeError: Maximum call stack size exceeded
RangeError: Maximum call stack size exceeded
RangeError: Maximum call stack size exceeded"

actual=$(cat "$1")
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mhelloworld test failed\033[0m"
    exit 1;
fi
