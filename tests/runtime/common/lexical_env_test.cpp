/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/lexical_env.h"
#include "plugins/ecmascript/runtime/object_factory.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class LexicalEnvTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

TEST_F(LexicalEnvTest, LexicalEnv_Create)
{
    JSHandle<LexicalEnv> lexical_env = thread_->GetEcmaVM()->GetFactory()->NewLexicalEnv(0);
    EXPECT_TRUE(lexical_env.GetTaggedValue().IsObject());
}
}  // namespace panda::test
