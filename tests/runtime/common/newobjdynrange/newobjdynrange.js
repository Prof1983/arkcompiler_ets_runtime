function foo(arg1, arg2) {
    this.arg1 = arg1
    this.arg2 = arg2
}
var p = new foo("arg1", "arg2")
print(p.arg1 === "arg1" && p.arg2 === "arg2")