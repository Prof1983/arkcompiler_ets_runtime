function *foo() {
    yield 1
    yield 2
}

var p = foo()
var a = p.next()
print(a.value, a.done)
var b = p.next()
print(b.value, b.done)
var c = p.next()
print(c.value, c.done)
var d = p.next()
print(d.value, d.done)