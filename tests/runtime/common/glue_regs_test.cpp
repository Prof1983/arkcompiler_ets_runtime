/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"

#include "plugins/ecmascript/runtime/builtins.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/global_env_constants-inl.h"

using panda::ecmascript::GlobalEnvConstants;

namespace panda::test {
class GlueRegsTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

TEST_F(GlueRegsTest, ConstantClassTest)
{
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    ASSERT_NE(global_const, nullptr);

    const JSTaggedValue *address = global_const->BeginSlot();
    while (address < global_const->EndSlot()) {
        EXPECT_TRUE(!(*address).IsNull());  // Visit barely
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        address += sizeof(JSTaggedValue);
    }
}

TEST_F(GlueRegsTest, ConstantSpecialTest)
{
    auto global_const = const_cast<GlobalEnvConstants *>(thread_->GlobalConstants());
    ASSERT_NE(global_const, nullptr);

    EXPECT_TRUE(global_const->GetUndefined().IsUndefined());
    EXPECT_TRUE(global_const->GetHandledUndefined()->IsUndefined());
    EXPECT_TRUE(global_const->GetNull().IsNull());
    EXPECT_TRUE(global_const->GetHandledNull()->IsNull());
    EXPECT_TRUE(global_const->GetEmptyString().IsString());
    EXPECT_TRUE(global_const->GetHandledEmptyString()->IsString());
}

TEST_F(GlueRegsTest, ConstantStringTest)
{
    auto global_const = const_cast<GlobalEnvConstants *>(thread_->GlobalConstants());
    ASSERT_NE(global_const, nullptr);

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CONSTANT_STRING_ITERATOR(Type, Name, Index, Desc)                \
    Type Name##value = globalConst->Get##Name();                         \
    ASSERT_TRUE(!Name##value.IsNull());                                  \
    JSHandle<Type> Name##handledValue = globalConst->GetHandled##Name(); \
    ASSERT_TRUE(!Name##handledValue->IsNull());                          \
    GLOBAL_ENV_CONSTANT_CONSTANT(CONSTANT_STRING_ITERATOR)
#undef CONSTANT_STRING_ITERATOR
}

TEST_F(GlueRegsTest, ConstantAccessorTest)
{
    auto global_const = const_cast<GlobalEnvConstants *>(thread_->GlobalConstants());
    ASSERT_NE(global_const, nullptr);

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CONSTANT_ACCESSOR_ITERATOR(Type, Name, Index, Desc)              \
    Type Name##value = globalConst->Get##Name();                         \
    ASSERT_TRUE(!Name##value.IsNull());                                  \
    JSHandle<Type> Name##handledValue = globalConst->GetHandled##Name(); \
    ASSERT_TRUE(!Name##handledValue->IsNull());                          \
    GLOBAL_ENV_CONSTANT_ACCESSOR(CONSTANT_ACCESSOR_ITERATOR)
#undef CONSTANT_ACCESSOR_ITERATOR
}
}  // namespace panda::test
