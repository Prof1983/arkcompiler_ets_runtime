#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.

set -eo pipefail

expected="true
true
false
false
true
true
true
true
false
false
true
true
false
false
false
false"
actual=$(cat "$1")
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mstrictequal test failed\033[0m"
    exit 1;
fi

