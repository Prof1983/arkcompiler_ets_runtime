print(1 === 1);
print('hello' === 'hello');
print('1' ===  1);
print(0 === false);

print(1 == 1);
print('hello' == 'hello');
print('1' ==  1);
print(0 == false);

print(1 !== 1);
print('hello' !== 'hello');
print('1' !==  1);
print(0 !== false);

print(1 != 1);
print('hello' != 'hello');
print('1' !=  1);
print(0 != false);