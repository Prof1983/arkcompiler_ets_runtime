/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"

#include "plugins/ecmascript/runtime/builtins.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"

using panda::ecmascript::GlobalEnv;
using panda::ecmascript::JSFunction;
using panda::ecmascript::JSHandle;
using panda::ecmascript::JSObject;
using panda::ecmascript::ObjectFactory;

namespace panda::test {
class BuiltinsTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

TEST_F(BuiltinsTest, ObjectInit)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> object_function(env->GetObjectFunction());
    ASSERT_NE(*object_function, nullptr);
}

TEST_F(BuiltinsTest, FunctionInit)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> function_function(env->GetFunctionFunction());
    ASSERT_NE(*function_function, nullptr);
}

TEST_F(BuiltinsTest, NumberInit)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> number_function(env->GetNumberFunction());
    ASSERT_NE(*number_function, nullptr);
}

TEST_F(BuiltinsTest, SetInit)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> set_function(env->GetSetFunction());
    ASSERT_NE(*set_function, nullptr);
}

TEST_F(BuiltinsTest, MapInit)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> map_function(env->GetMapFunction());
    ASSERT_NE(*map_function, nullptr);
}

TEST_F(BuiltinsTest, StrictModeForbiddenAccess)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSFunction> function = factory->NewJSFunction(env, static_cast<void *>(nullptr));

    JSHandle<JSTaggedValue> caller_key(factory->NewFromString("caller"));
    JSHandle<JSTaggedValue> arguments_key(factory->NewFromString("arguments"));

    JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(function), caller_key);
    ASSERT_EQ(thread_->HasPendingException(), true);

    JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(function), arguments_key);
    ASSERT_EQ(thread_->HasPendingException(), true);
}

}  // namespace panda::test
