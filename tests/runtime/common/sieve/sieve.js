function Sieve() {}

Sieve.prototype.nsieve = function(m, isPrime) {
  var i, k, count;

  for (i = 2; i <= m; i++) {
    isPrime[i] = true;
  }
  count = 0;
  for (i = 2; i <= m; i++) {
    if (isPrime[i]) {
      for (k = i + i; k <= m; k += i) isPrime[k] = false;
      count++;
    }
  }
  return count;
};

Sieve.prototype.sieve = function() {
  var sum = 0;
  for (var i = 1; i <= 3; i++) {
    var m = (1 << i) * 10000;
    var flags = Array(m + 1);
    sum += this.nsieve(m, flags);
  }
  return sum;
};

Sieve.prototype.run = function() {
  var expected = 14302;
  var result = this.sieve();
  if (result != expected)
    throw 'ERROR: bad result: expected ' + expected + ' but got ' + result;
};



function Controller() {};

Controller.prototype.execute = function() {
    benchmark.run();
};

var benchmark = new Sieve();
var controller = new Controller();
controller.execute();
print('OK')
