/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

// NOLINTBEGIN(readability-magic-numbers,modernize-avoid-c-arrays,modernize-loop-convert)

namespace panda::test {
class JSTypedArrayTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    const std::vector<JSType> c_vec_js_type_ {
        JSType::JS_INT8_ARRAY,   JSType::JS_UINT8_ARRAY,   JSType::JS_UINT8_CLAMPED_ARRAY,
        JSType::JS_INT16_ARRAY,  JSType::JS_UINT16_ARRAY,  JSType::JS_INT32_ARRAY,
        JSType::JS_UINT32_ARRAY, JSType::JS_FLOAT32_ARRAY, JSType::JS_FLOAT64_ARRAY};

    // PandaVector pushed with JSTaggedValue made from compatible input value for the JSType
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    const std::vector<JSTaggedValue> c_vec_handle_tag_val_value_for_typed_array_ {
        // Use "(S)(...)" cast to make v in "JSTaggedValue(T v) : coretypes::TaggedValue(v) {}" compatible with S
        JSTaggedValue((int8_t)(-111)), JSTaggedValue((uint8_t)(222)), JSTaggedValue((uint8_t)(222)),
        JSTaggedValue((int16_t)(-31111)), JSTaggedValue((uint16_t)(61111)),
        // int32 : -2147483648->2147483647, uint32 : 0->4294967295
        JSTaggedValue((int32_t)(2111111111)), JSTaggedValue((uint32_t)(4111111111)), JSTaggedValue((float)(4321.1234)),
        JSTaggedValue((double)(987654321.123456789))};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSHandle<JSTypedArray> CreateNumberTypedArray(JSThread *thread, JSType js_type)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> handle_tag_val_func = env->GetInt8ArrayFunction();
    switch (js_type) {
        case JSType::JS_INT8_ARRAY:
            break;
        case JSType::JS_UINT8_ARRAY:
            handle_tag_val_func = env->GetUint8ArrayFunction();
            break;
        case JSType::JS_UINT8_CLAMPED_ARRAY:
            handle_tag_val_func = env->GetUint8ClampedArrayFunction();
            break;
        case JSType::JS_INT16_ARRAY:
            handle_tag_val_func = env->GetInt16ArrayFunction();
            break;
        case JSType::JS_UINT16_ARRAY:
            handle_tag_val_func = env->GetUint16ArrayFunction();
            break;
        case JSType::JS_INT32_ARRAY:
            handle_tag_val_func = env->GetInt32ArrayFunction();
            break;
        case JSType::JS_UINT32_ARRAY:
            handle_tag_val_func = env->GetUint32ArrayFunction();
            break;
        case JSType::JS_FLOAT32_ARRAY:
            handle_tag_val_func = env->GetFloat32ArrayFunction();
            break;
        case JSType::JS_FLOAT64_ARRAY:
            handle_tag_val_func = env->GetFloat64ArrayFunction();
            break;
        default:
            ASSERT_PRINT(false, "the second argument is a wrong JSType for CreateNumberTypedArray function");
            break;
    }

    return JSHandle<JSTypedArray>::Cast(
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>::Cast(handle_tag_val_func), handle_tag_val_func));
}

/*
 * Feature: JSTypedArray
 * Function: ToPropKey
 * SubFunction: EcmaString::GetCString
 * FunctionPoints: TaggedType Signs To EcmaString Signs
 * CaseDescription: Check whether the EcmaStrings transformed through calling ToPropKey function from TaggedTypes are
 *                  within expectations.
 */
TEST_F(JSTypedArrayTest, ToPropKey_001)
{
    JSHandle<JSTaggedValue> handle_undefined(thread_, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> handle_hole(thread_, JSTaggedValue::Hole());
    JSHandle<JSTaggedValue> hnadle_tag_val_ecma_str_prop_key_to1 = JSTypedArray::ToPropKey(thread_, handle_undefined);
    JSHandle<JSTaggedValue> hnadle_tag_val_ecma_str_prop_key_to2 = JSTypedArray::ToPropKey(thread_, handle_hole);
    JSHandle<EcmaString> handle_ecma_str_prop_key_to1 =
        JSHandle<EcmaString>::Cast(hnadle_tag_val_ecma_str_prop_key_to1);
    JSHandle<EcmaString> handle_ecma_str_prop_key_to2 =
        JSHandle<EcmaString>::Cast(hnadle_tag_val_ecma_str_prop_key_to2);
    EXPECT_NE(0, sizeof(handle_undefined));
    EXPECT_NE(0, sizeof(handle_hole));
    std::unique_ptr<char[]> uni_char_arr_to1(handle_ecma_str_prop_key_to1->GetCString());
    std::unique_ptr<char[]> uni_char_arr_to2(handle_ecma_str_prop_key_to2->GetCString());
    EXPECT_EQ(uni_char_arr_to1[0], 'u');
    EXPECT_EQ(uni_char_arr_to1[1], 'n');
    EXPECT_EQ(uni_char_arr_to1[2], 'd');
    EXPECT_EQ(uni_char_arr_to1[3], 'e');
    EXPECT_EQ(uni_char_arr_to1[4], 'f');
    EXPECT_EQ(uni_char_arr_to1[5], 'i');
    EXPECT_EQ(uni_char_arr_to1[6], 'n');
    EXPECT_EQ(uni_char_arr_to1[7], 'e');
    EXPECT_EQ(uni_char_arr_to1[8], 'd');
    EXPECT_EQ(uni_char_arr_to1[9], 0);  // "undefined"
    EXPECT_EQ(uni_char_arr_to2[0], 0);  // ""
}

/*
 * Feature: JSTypedArray
 * Function: ToPropKey
 * SubFunction: EcmaString::GetCString
 * FunctionPoints: Number Signs To EcmaString Signs
 * CaseDescription: Check whether the EcmaStrings transformed through calling ToPropKey function from Numbers are
 *                  within expectations.
 */
TEST_F(JSTypedArrayTest, ToPropKey_002)
{
    JSHandle<JSTaggedValue> handle_tag_val1(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> handle_tag_val2(thread_, JSTaggedValue(-1));
    JSHandle<JSTaggedValue> handle_tag_val3(thread_, JSTaggedValue(1.789));
    JSHandle<JSTaggedValue> handle_tag_val4(thread_, JSTaggedValue(-789.1));
    JSHandle<JSTaggedValue> hnadle_tag_val_ecma_str_prop_key_to1 = JSTypedArray::ToPropKey(thread_, handle_tag_val1);
    JSHandle<JSTaggedValue> hnadle_tag_val_ecma_str_prop_key_to2 = JSTypedArray::ToPropKey(thread_, handle_tag_val2);
    JSHandle<JSTaggedValue> hnadle_tag_val_ecma_str_prop_key_to3 = JSTypedArray::ToPropKey(thread_, handle_tag_val3);
    JSHandle<JSTaggedValue> hnadle_tag_val_ecma_str_prop_key_to4 = JSTypedArray::ToPropKey(thread_, handle_tag_val4);
    JSHandle<EcmaString> handle_ecma_str_prop_key_to1 =
        JSHandle<EcmaString>::Cast(hnadle_tag_val_ecma_str_prop_key_to1);
    JSHandle<EcmaString> handle_ecma_str_prop_key_to2 =
        JSHandle<EcmaString>::Cast(hnadle_tag_val_ecma_str_prop_key_to2);
    JSHandle<EcmaString> handle_ecma_str_prop_key_to3 =
        JSHandle<EcmaString>::Cast(hnadle_tag_val_ecma_str_prop_key_to3);
    JSHandle<EcmaString> handle_ecma_str_prop_key_to4 =
        JSHandle<EcmaString>::Cast(hnadle_tag_val_ecma_str_prop_key_to4);
    std::unique_ptr<char[]> uni_char_arr_to1(handle_ecma_str_prop_key_to1->GetCString());
    std::unique_ptr<char[]> uni_char_arr_to2(handle_ecma_str_prop_key_to2->GetCString());
    std::unique_ptr<char[]> uni_char_arr_to3(handle_ecma_str_prop_key_to3->GetCString());
    std::unique_ptr<char[]> uni_char_arr_to4(handle_ecma_str_prop_key_to4->GetCString());
    EXPECT_EQ(uni_char_arr_to1[0], '0');
    EXPECT_EQ(uni_char_arr_to1[1], 0);  // "0"
    EXPECT_EQ(uni_char_arr_to2[0], '-');
    EXPECT_EQ(uni_char_arr_to2[1], '1');
    EXPECT_EQ(uni_char_arr_to2[2], 0);  // "-1"
    EXPECT_EQ(uni_char_arr_to3[0], '1');
    EXPECT_EQ(uni_char_arr_to3[1], '.');
    EXPECT_EQ(uni_char_arr_to3[2], '7');
    EXPECT_EQ(uni_char_arr_to3[3], '8');
    EXPECT_EQ(uni_char_arr_to3[4], '9');
    EXPECT_EQ(uni_char_arr_to3[5], 0);  // "1.789"
    EXPECT_EQ(uni_char_arr_to4[0], '-');
    EXPECT_EQ(uni_char_arr_to4[1], '7');
    EXPECT_EQ(uni_char_arr_to4[2], '8');
    EXPECT_EQ(uni_char_arr_to4[3], '9');
    EXPECT_EQ(uni_char_arr_to4[4], '.');
    EXPECT_EQ(uni_char_arr_to4[5], '1');
    EXPECT_EQ(uni_char_arr_to4[6], 0);  // "-789.1"
}

/*
 * Feature: JSTypedArray
 * Function: CreateJSTypedArray(GlobalEnv::GetInt8ArrayFunction.../ObjectFactory::NewJSObjectByConstructor)
 * SubFunction: JSTaggedValue::IsTypedArray/IsJSInt8Array...
 * FunctionPoints: Create JSTypedArray(JSInt8Array...)
 * CaseDescription: Check whether the bools returned through calling IsTypedArray/IsInt8Array... functions from the
 *                  JSTypedArrays created through calling NewJSObjectByConstructor function are within expectations.
 */
TEST_F(JSTypedArrayTest, TypedArrayCreate)
{
    JSHandle<JSTypedArray> handle_int8_array = CreateNumberTypedArray(thread_, JSType::JS_INT8_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int8_array = JSHandle<JSTaggedValue>::Cast(handle_int8_array);
    EXPECT_TRUE(handle_tag_val_int8_array->IsJSInt8Array() && handle_tag_val_int8_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_uint8_array = CreateNumberTypedArray(thread_, JSType::JS_UINT8_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint8_array = JSHandle<JSTaggedValue>::Cast(handle_uint8_array);
    EXPECT_TRUE(handle_tag_val_uint8_array->IsJSUint8Array() && handle_tag_val_uint8_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_uint8_clamped_array = CreateNumberTypedArray(thread_, JSType::JS_UINT8_CLAMPED_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint8_clamped_array =
        JSHandle<JSTaggedValue>::Cast(handle_uint8_clamped_array);
    EXPECT_TRUE(handle_tag_val_uint8_clamped_array->IsJSUint8ClampedArray() &&
                handle_tag_val_uint8_clamped_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_int16_array = CreateNumberTypedArray(thread_, JSType::JS_INT16_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int16_array = JSHandle<JSTaggedValue>::Cast(handle_int16_array);
    EXPECT_TRUE(handle_tag_val_int16_array->IsJSInt16Array() && handle_tag_val_int16_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_uint16_array = CreateNumberTypedArray(thread_, JSType::JS_UINT16_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint16_array = JSHandle<JSTaggedValue>::Cast(handle_uint16_array);
    EXPECT_TRUE(handle_tag_val_uint16_array->IsJSUint16Array() && handle_tag_val_uint16_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_int32_array = CreateNumberTypedArray(thread_, JSType::JS_INT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int32_array = JSHandle<JSTaggedValue>::Cast(handle_int32_array);
    EXPECT_TRUE(handle_tag_val_int32_array->IsJSInt32Array() && handle_tag_val_int32_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_uint32_array = CreateNumberTypedArray(thread_, JSType::JS_UINT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint32_array = JSHandle<JSTaggedValue>::Cast(handle_uint32_array);
    EXPECT_TRUE(handle_tag_val_uint32_array->IsJSUint32Array() && handle_tag_val_uint32_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_float32_array = CreateNumberTypedArray(thread_, JSType::JS_FLOAT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_float32_array = JSHandle<JSTaggedValue>::Cast(handle_float32_array);
    EXPECT_TRUE(handle_tag_val_float32_array->IsJSFloat32Array() && handle_tag_val_float32_array->IsTypedArray());

    JSHandle<JSTypedArray> handle_float64_array = CreateNumberTypedArray(thread_, JSType::JS_FLOAT64_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_float64_array = JSHandle<JSTaggedValue>::Cast(handle_float64_array);
    EXPECT_TRUE(handle_tag_val_float64_array->IsJSFloat64Array() && handle_tag_val_float64_array->IsTypedArray());
}

/*
 * Feature: JSTypedArray
 * Function: SetViewedArrayBuffer
 * SubFunction: GetViewedArrayBuffer/ObjectFactory::NewJSArrayBuffer
 * FunctionPoints: Set ViewedArrayBuffer
 * CaseDescription: Check whether the JSArrayBuffer returned through calling GetViewedArrayBuffer function from the
 *                  JSTypedArray changed through calling SetViewedArrayBuffer function is within expectations.
 */
TEST_F(JSTypedArrayTest, SetViewedArrayBuffer)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSArrayBuffer> handle_array_buffer_from = factory->NewJSArrayBuffer(10);
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from = JSHandle<JSTaggedValue>::Cast(handle_array_buffer_from);

    for (size_t i = 0; i < c_vec_js_type_.size(); i++) {
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(i));

        EXPECT_EQ(handle_typed_array->GetViewedArrayBuffer(), JSTaggedValue::Undefined());
        handle_typed_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
        EXPECT_EQ(handle_typed_array->GetViewedArrayBuffer(), handle_tag_val_array_buffer_from.GetTaggedValue());
    }
}

/*
 * Feature: JSTypedArray
 * Function: SetTypedArrayName
 * SubFunction: GetTypedArrayName
 * FunctionPoints: Set TypedArrayName
 * CaseDescription: Check whether the JSTaggedValue returned through calling GetTypedArrayName function from the
 *                  JSTypedArray changed through calling SetTypedArrayName function is within expectations.
 */
TEST_F(JSTypedArrayTest, SetTypedArrayName)
{
    PandaString c_str_name = "cStrName";
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> handle_ecma_str_name_from = factory->NewFromString(c_str_name);
    JSHandle<JSTaggedValue> handle_tag_val_ecma_str_name_from =
        JSHandle<JSTaggedValue>::Cast(handle_ecma_str_name_from);

    for (size_t i = 0; i < c_vec_js_type_.size(); i++) {
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(i));

        EXPECT_EQ(handle_typed_array->GetTypedArrayName(), JSTaggedValue::Undefined());
        handle_typed_array->SetTypedArrayName(thread_, handle_tag_val_ecma_str_name_from);
        EXPECT_EQ(handle_typed_array->GetTypedArrayName(), handle_tag_val_ecma_str_name_from.GetTaggedValue());
    }
}

/*
 * Feature: JSTypedArray
 * Function: SetByteLength
 * SubFunction: GetByteLength
 * FunctionPoints: Set ByteLength
 * CaseDescription: Check whether the Number returned through calling GetByteLength function from the JSTypedArray
 *                  changed through calling SetByteLength function is within expectations.
 */
TEST_F(JSTypedArrayTest, SetByteLength)
{
    uint32_t u32_byte_length = 2;
    JSHandle<JSTaggedValue> handle_tag_val_byte_length_from(thread_, JSTaggedValue(u32_byte_length));

    for (size_t i = 0; i < c_vec_js_type_.size(); i++) {
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(i));

        EXPECT_EQ(handle_typed_array->GetByteLength(), JSTaggedValue(0));
        handle_typed_array->SetByteLength(thread_, handle_tag_val_byte_length_from);
        EXPECT_EQ(handle_typed_array->GetByteLength(), handle_tag_val_byte_length_from.GetTaggedValue());
    }
}

/*
 * Feature: JSTypedArray
 * Function: SetByteOffset
 * SubFunction: GetByteOffset
 * FunctionPoints: Set ByteOffset
 * CaseDescription: Check whether the Number returned through calling GetByteOffset function from the JSTypedArray
 *                  changed through calling SetByteOffset function is within expectations.
 */
TEST_F(JSTypedArrayTest, SetByteOffset)
{
    uint32_t u32_byte_offset = 2;
    JSHandle<JSTaggedValue> handle_tag_val_byte_offset_from(thread_, JSTaggedValue(u32_byte_offset));

    for (size_t i = 0; i < c_vec_js_type_.size(); i++) {
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(i));

        EXPECT_EQ(handle_typed_array->GetByteOffset(), JSTaggedValue(0));
        handle_typed_array->SetByteOffset(thread_, handle_tag_val_byte_offset_from);
        EXPECT_EQ(handle_typed_array->GetByteOffset(), handle_tag_val_byte_offset_from.GetTaggedValue());
    }
}

/*
 * Feature: JSTypedArray
 * Function: SetArrayLength
 * SubFunction: GetArrayLength
 * FunctionPoints: Set ArrayLength
 * CaseDescription: Check whether the Number returned through calling GetArrayLength function from the JSTypedArray
 *                  changed through calling SetArrayLength function is within expectations.
 */
TEST_F(JSTypedArrayTest, SetArrayLength)
{
    uint32_t u32_array_length = 2;
    JSHandle<JSTaggedValue> handle_tag_val_array_length_from(thread_, JSTaggedValue(u32_array_length));

    for (size_t i = 0; i < c_vec_js_type_.size(); i++) {
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(i));

        EXPECT_EQ(handle_typed_array->GetArrayLength(), JSTaggedValue(0));
        handle_typed_array->SetArrayLength(thread_, handle_tag_val_array_length_from);
        EXPECT_EQ(handle_typed_array->GetArrayLength(), handle_tag_val_array_length_from.GetTaggedValue());
    }
}

/*
 * Feature: JSTypedArray
 * Function: IntegerIndexedElementSet
 * SubFunction: IntegerIndexedElementGet
 * FunctionPoints: Set Element At Integer Index(JSTaggedValue) Of JSTypedArray
 * CaseDescription: Check whether the OperationResults returned through calling IntegerIndexedElementGet function from
 *                  the JSTypedArray changed through calling IntegerIndexedElementSet function are within expectations.
 */
TEST_F(JSTypedArrayTest, DISABLED_IntegerIndexedElementSet_Int8Array_001)
{
    uint32_t num_elements_int8_array = 256;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_int8_array = CreateNumberTypedArray(thread_, JSType::JS_INT8_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int8_array = JSHandle<JSTaggedValue>::Cast(handle_int8_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_int8_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_int8_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_int8_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_int8_array->SetArrayLength(thread_, JSTaggedValue(num_elements_int8_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    for (size_t i = 0; i < num_elements_int8_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_int8_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(thread_, JSTaggedValue(std::numeric_limits<int8_t>::min() + i))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (size_t i = 0; i < num_elements_int8_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  std::numeric_limits<int8_t>::min() + i);
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array,
                                                                        JSTaggedValue(num_elements_int8_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int8_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int8_array,
                                                        JSTaggedValue(num_elements_int8_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

// Nonstandard input value for Int8Array
TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Int8Array_002)
{
    uint32_t num_elements_int8_array = 16;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_int8_array = CreateNumberTypedArray(thread_, JSType::JS_INT8_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int8_array = JSHandle<JSTaggedValue>::Cast(handle_int8_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_int8_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_int8_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_int8_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_int8_array->SetArrayLength(thread_, JSTaggedValue(num_elements_int8_array));

    int64_t value1 = -129;  // to int8 : 127
    int64_t value2 = 128;   // to int8 : -128
    double value3 = 13.4;   // to int8 : 13
    double value4 = 13.6;   // to int8 : 13
    JSHandle<JSTaggedValue> handle_tag_val_value_set1(thread_, JSTaggedValue(value1));
    JSHandle<JSTaggedValue> handle_tag_val_value_set2(thread_, JSTaggedValue(value2));
    JSHandle<JSTaggedValue> handle_tag_val_value_set3(thread_, JSTaggedValue(value3));
    JSHandle<JSTaggedValue> handle_tag_val_value_set4(thread_, JSTaggedValue(value4));

    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set1));
    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set2));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set3));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set4));
    OperationResult op_result4 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int8_array, JSTaggedValue(0));

    EXPECT_NE(value1, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value2, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value3, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value4, op_result4.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(127, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(-128, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result4.GetValue().GetTaggedValue().GetNumber());
}

TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint8Array_001)
{
    uint32_t num_elements_uint8_array = 256;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint8_array = CreateNumberTypedArray(thread_, JSType::JS_UINT8_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint8_array = JSHandle<JSTaggedValue>::Cast(handle_uint8_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint8_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_uint8_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint8_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint8_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint8_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    for (uint32_t i = 0; i < num_elements_uint8_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_uint8_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(thread_, JSTaggedValue(std::numeric_limits<uint8_t>::min() + i))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (uint32_t i = 0; i < num_elements_uint8_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  std::numeric_limits<uint8_t>::min() + i);
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array,
                                                                        JSTaggedValue(num_elements_uint8_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_array,
                                                        JSTaggedValue(num_elements_uint8_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

// Nonstandard input value for Uint8Array
TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint8Array_002)
{
    uint32_t num_elements_uint8_array = 16;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint8_array = CreateNumberTypedArray(thread_, JSType::JS_UINT8_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint8_array = JSHandle<JSTaggedValue>::Cast(handle_uint8_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint8_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_uint8_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint8_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint8_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint8_array));

    int64_t value1 = -1;   // to uint8 : 255
    int64_t value2 = 256;  // to uint8 : 0
    double value3 = 13.4;  // to uint8 : 13
    double value4 = 13.6;  // to uint8 : 13
    JSHandle<JSTaggedValue> handle_tag_val_value_set1(thread_, JSTaggedValue(value1));
    JSHandle<JSTaggedValue> handle_tag_val_value_set2(thread_, JSTaggedValue(value2));
    JSHandle<JSTaggedValue> handle_tag_val_value_set3(thread_, JSTaggedValue(value3));
    JSHandle<JSTaggedValue> handle_tag_val_value_set4(thread_, JSTaggedValue(value4));

    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set1));
    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set2));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set3));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set4));
    OperationResult op_result4 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_array, JSTaggedValue(0));

    EXPECT_NE(value1, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value2, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value3, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value4, op_result4.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(255, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(0, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result4.GetValue().GetTaggedValue().GetNumber());
}

TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint8ClampedArray_001)
{
    uint32_t num_elements_uint8_clamped_array = 256;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint8_clamped_array = CreateNumberTypedArray(thread_, JSType::JS_UINT8_CLAMPED_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint8_clamped_array =
        JSHandle<JSTaggedValue>::Cast(handle_uint8_clamped_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint8_clamped_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_uint8_clamped_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint8_clamped_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint8_clamped_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint8_clamped_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    for (uint32_t i = 0; i < num_elements_uint8_clamped_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(thread_, JSTaggedValue(std::numeric_limits<uint8_t>::min() + i))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (uint32_t i = 0; i < num_elements_uint8_clamped_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  std::numeric_limits<uint8_t>::min() + i);
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(
        thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(num_elements_uint8_clamped_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_clamped_array,
                                                        JSTaggedValue(num_elements_uint8_clamped_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

// Nonstandard input value for Uint8ClampedArray
TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint8ClampedArray_002)
{
    uint32_t num_elements_uint8_clamped_array = 16;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint8_clamped_array = CreateNumberTypedArray(thread_, JSType::JS_UINT8_CLAMPED_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint8_clamped_array =
        JSHandle<JSTaggedValue>::Cast(handle_uint8_clamped_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint8_clamped_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_uint8_clamped_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint8_clamped_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint8_clamped_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint8_clamped_array));

    int64_t value1 = -1;   // to uint8_clamped : 0
    int64_t value2 = 256;  // to uint8_clamped : 255
    double value3 = 13.4;  // to uint8_clamped : 13
    double value4 = 13.6;  // to uint8_clamped : 14
    JSHandle<JSTaggedValue> handle_tag_val_value_set1(thread_, JSTaggedValue(value1));
    JSHandle<JSTaggedValue> handle_tag_val_value_set2(thread_, JSTaggedValue(value2));
    JSHandle<JSTaggedValue> handle_tag_val_value_set3(thread_, JSTaggedValue(value3));
    JSHandle<JSTaggedValue> handle_tag_val_value_set4(thread_, JSTaggedValue(value4));

    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set1));
    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set2));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set3));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set4));
    OperationResult op_result4 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint8_clamped_array, JSTaggedValue(0));

    EXPECT_NE(value1, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value2, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value3, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value4, op_result4.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(0, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(255, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(14, op_result4.GetValue().GetTaggedValue().GetNumber());
}

TEST_F(JSTypedArrayTest, DISABLED_IntegerIndexedElementSet_Int16Array_001)
{
    uint32_t num_elements_int16_array = 100;
    int16_t scale_for_int16_value_set = 100;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_int16_array = CreateNumberTypedArray(thread_, JSType::JS_INT16_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int16_array = JSHandle<JSTaggedValue>::Cast(handle_int16_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_int16_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_int16_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_int16_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_int16_array->SetArrayLength(thread_, JSTaggedValue(num_elements_int16_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    for (size_t i = 0; i < num_elements_int16_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_int16_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(
                thread_, JSTaggedValue(std::numeric_limits<int16_t>::min() + i * scale_for_int16_value_set))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (size_t i = 0; i < num_elements_int16_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  std::numeric_limits<int16_t>::min() + i * scale_for_int16_value_set);
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array,
                                                                        JSTaggedValue(num_elements_int16_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int16_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int16_array,
                                                        JSTaggedValue(num_elements_int16_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

// Nonstandard input value for Int16Array
TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Int16Array_002)
{
    uint32_t num_elements_int16_array = 16;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_int16_array = CreateNumberTypedArray(thread_, JSType::JS_INT16_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int16_array = JSHandle<JSTaggedValue>::Cast(handle_int16_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_int16_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_int16_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_int16_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_int16_array->SetArrayLength(thread_, JSTaggedValue(num_elements_int16_array));

    int64_t value1 = -32769;  // to int16 : 32767
    int64_t value2 = 32768;   // to int16 : -32768
    double value3 = 13.4;     // to int16 : 13
    double value4 = 13.6;     // to int16 : 13
    JSHandle<JSTaggedValue> handle_tag_val_value_set1(thread_, JSTaggedValue(value1));
    JSHandle<JSTaggedValue> handle_tag_val_value_set2(thread_, JSTaggedValue(value2));
    JSHandle<JSTaggedValue> handle_tag_val_value_set3(thread_, JSTaggedValue(value3));
    JSHandle<JSTaggedValue> handle_tag_val_value_set4(thread_, JSTaggedValue(value4));

    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set1));
    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set2));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set3));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set4));
    OperationResult op_result4 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int16_array, JSTaggedValue(0));

    EXPECT_NE(value1, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value2, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value3, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value4, op_result4.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(32767, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(-32768, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result4.GetValue().GetTaggedValue().GetNumber());
}

TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint16Array_001)
{
    uint32_t num_elements_uint16_array = 100;
    uint32_t scale_for_uint16_value_set = 100;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint16_array = CreateNumberTypedArray(thread_, JSType::JS_UINT16_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint16_array = JSHandle<JSTaggedValue>::Cast(handle_uint16_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint16_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_uint16_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint16_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint16_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint16_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    for (uint32_t i = 0; i < num_elements_uint16_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_uint16_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(
                thread_, JSTaggedValue(std::numeric_limits<uint16_t>::min() + i * scale_for_uint16_value_set))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (uint32_t i = 0; i < num_elements_uint16_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  std::numeric_limits<uint16_t>::min() + i * scale_for_uint16_value_set);
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array,
                                                                        JSTaggedValue(num_elements_uint16_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint16_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint16_array,
                                                        JSTaggedValue(num_elements_uint16_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

// Nonstandard input value for Uint16Array
TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint16Array_002)
{
    uint32_t num_elements_uint16_array = 16;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint16_array = CreateNumberTypedArray(thread_, JSType::JS_UINT16_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint16_array = JSHandle<JSTaggedValue>::Cast(handle_uint16_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint16_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_uint16_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint16_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint16_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint16_array));

    int64_t value1 = -1;     // to uint16 : 65535
    int64_t value2 = 65536;  // to uint16 : 0
    double value3 = 13.4;    // to uint16 : 13
    double value4 = 13.6;    // to uint16 : 13
    JSHandle<JSTaggedValue> handle_tag_val_value_set1(thread_, JSTaggedValue(value1));
    JSHandle<JSTaggedValue> handle_tag_val_value_set2(thread_, JSTaggedValue(value2));
    JSHandle<JSTaggedValue> handle_tag_val_value_set3(thread_, JSTaggedValue(value3));
    JSHandle<JSTaggedValue> handle_tag_val_value_set4(thread_, JSTaggedValue(value4));

    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set1));
    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set2));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set3));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set4));
    OperationResult op_result4 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint16_array, JSTaggedValue(0));

    EXPECT_NE(value1, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value2, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value3, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value4, op_result4.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(65535, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(0, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result4.GetValue().GetTaggedValue().GetNumber());
}

TEST_F(JSTypedArrayTest, DISABLED_IntegerIndexedElementSet_Int32Array_001)
{
    uint32_t num_elements_int32_array = 100;
    int32_t scale_for_int32_value_set = 100000;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_int32_array = CreateNumberTypedArray(thread_, JSType::JS_INT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int32_array = JSHandle<JSTaggedValue>::Cast(handle_int32_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_int32_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_int32_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_int32_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_int32_array->SetArrayLength(thread_, JSTaggedValue(num_elements_int32_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    for (size_t i = 0; i < num_elements_int32_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_int32_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(
                thread_, JSTaggedValue(std::numeric_limits<int32_t>::min() + i * scale_for_int32_value_set))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (size_t i = 0; i < num_elements_int32_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  std::numeric_limits<int32_t>::min() + i * scale_for_int32_value_set);
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array,
                                                                        JSTaggedValue(num_elements_int32_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int32_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int32_array,
                                                        JSTaggedValue(num_elements_int32_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

// Nonstandard input value for Int32Array
TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Int32Array_002)
{
    uint32_t num_elements_int32_array = 16;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_int32_array = CreateNumberTypedArray(thread_, JSType::JS_INT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_int32_array = JSHandle<JSTaggedValue>::Cast(handle_int32_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_int32_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_int32_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_int32_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_int32_array->SetArrayLength(thread_, JSTaggedValue(num_elements_int32_array));

    int64_t value1 = -2147483649;  // to int32 : 2147483647
    int64_t value2 = 2147483648;   // to int32 : -2147483648
    double value3 = 13.4;          // to int32 : 13
    double value4 = 13.6;          // to int32 : 13
    JSHandle<JSTaggedValue> handle_tag_val_value_set1(thread_, JSTaggedValue(value1));
    JSHandle<JSTaggedValue> handle_tag_val_value_set2(thread_, JSTaggedValue(value2));
    JSHandle<JSTaggedValue> handle_tag_val_value_set3(thread_, JSTaggedValue(value3));
    JSHandle<JSTaggedValue> handle_tag_val_value_set4(thread_, JSTaggedValue(value4));

    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set1));
    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set2));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set3));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_int32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set4));
    OperationResult op_result4 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_int32_array, JSTaggedValue(0));

    EXPECT_NE(value1, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value2, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value3, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value4, op_result4.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(2147483647, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(-2147483648, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result4.GetValue().GetTaggedValue().GetNumber());
}

TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint32Array_001)
{
    uint32_t num_elements_uint32_array = 100;
    uint32_t scale_for_uint32_value_set = 100000;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint32_array = CreateNumberTypedArray(thread_, JSType::JS_UINT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint32_array = JSHandle<JSTaggedValue>::Cast(handle_uint32_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint32_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_uint32_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint32_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint32_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint32_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    for (uint32_t i = 0; i < num_elements_uint32_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_uint32_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(
                thread_, JSTaggedValue(std::numeric_limits<uint32_t>::min() + i * scale_for_uint32_value_set))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (uint32_t i = 0; i < num_elements_uint32_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  std::numeric_limits<uint32_t>::min() + i * scale_for_uint32_value_set);
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array,
                                                                        JSTaggedValue(num_elements_uint32_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint32_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint32_array,
                                                        JSTaggedValue(num_elements_uint32_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

// Nonstandard input value for Uint32Array
TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Uint32Array_002)
{
    int32_t num_elements_uint32_array = 16;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_uint32_array = CreateNumberTypedArray(thread_, JSType::JS_UINT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_uint32_array = JSHandle<JSTaggedValue>::Cast(handle_uint32_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_uint32_array));
    int32_t byte_length_viewd_array_buffer = size_element * num_elements_uint32_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_uint32_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_uint32_array->SetArrayLength(thread_, JSTaggedValue(num_elements_uint32_array));

    int64_t value1 = -1;          // to uint32 : 4294967295
    int64_t value2 = 4294967296;  // to uint32 : 0
    double value3 = 13.4;         // to uint32 : 13
    double value4 = 13.6;         // to uint32 : 13
    JSHandle<JSTaggedValue> handle_tag_val_value_set1(thread_, JSTaggedValue(value1));
    JSHandle<JSTaggedValue> handle_tag_val_value_set2(thread_, JSTaggedValue(value2));
    JSHandle<JSTaggedValue> handle_tag_val_value_set3(thread_, JSTaggedValue(value3));
    JSHandle<JSTaggedValue> handle_tag_val_value_set4(thread_, JSTaggedValue(value4));

    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set1));
    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set2));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set3));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0));
    EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0),
                                                       handle_tag_val_value_set4));
    OperationResult op_result4 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_uint32_array, JSTaggedValue(0));

    EXPECT_NE(value1, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value2, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value3, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_NE(value4, op_result4.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(4294967295, op_result1.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(0, op_result2.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result3.GetValue().GetTaggedValue().GetNumber());
    EXPECT_EQ(13, op_result4.GetValue().GetTaggedValue().GetNumber());
}

TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Float32Array)
{
    uint32_t num_elements_float32_array = 100;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_float32_array = CreateNumberTypedArray(thread_, JSType::JS_FLOAT32_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_float32_array = JSHandle<JSTaggedValue>::Cast(handle_float32_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_float32_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_float32_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_float32_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_float32_array->SetArrayLength(thread_, JSTaggedValue(num_elements_float32_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    float float_max_value = std::numeric_limits<float>::max();
    for (uint32_t i = 0; i < num_elements_float32_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_float32_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(
                thread_, JSTaggedValue(float_max_value - (i * (float_max_value / num_elements_float32_array))))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float32_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (uint32_t i = 0; i < num_elements_float32_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  float_max_value - (i * (float_max_value / num_elements_float32_array)));
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float32_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float32_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float32_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float32_array,
                                                                        JSTaggedValue(num_elements_float32_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_float32_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_float32_array,
                                                        JSTaggedValue(num_elements_float32_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

TEST_F(JSTypedArrayTest, IntegerIndexedElementSet_Float64Array)
{
    uint32_t num_elements_float64_array = 100;
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTypedArray> handle_float64_array = CreateNumberTypedArray(thread_, JSType::JS_FLOAT64_ARRAY);
    JSHandle<JSTaggedValue> handle_tag_val_float64_array = JSHandle<JSTaggedValue>::Cast(handle_float64_array);

    uint32_t size_element =
        ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_float64_array));
    uint32_t byte_length_viewd_array_buffer = size_element * num_elements_float64_array;
    JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
        JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
    handle_float64_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
    handle_float64_array->SetArrayLength(thread_, JSTaggedValue(num_elements_float64_array));

    PandaVector<OperationResult> c_vec_op_result = {};
    double double_max_value = std::numeric_limits<double>::max();
    for (uint32_t i = 0; i < num_elements_float64_array; i++) {
        EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(
            thread_, handle_tag_val_float64_array, JSTaggedValue(i),
            JSHandle<JSTaggedValue>(
                thread_, JSTaggedValue(double_max_value - (i * (double_max_value / num_elements_float64_array))))));
        OperationResult op_result =
            JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float64_array, JSTaggedValue(i));
        c_vec_op_result.push_back(op_result);
    }
    for (uint32_t i = 0; i < num_elements_float64_array; i++) {
        EXPECT_EQ(c_vec_op_result.at(i).GetValue().GetTaggedValue().GetNumber(),
                  double_max_value - (i * (double_max_value / num_elements_float64_array)));
    }
    c_vec_op_result.clear();

    OperationResult op_result1 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float64_array, JSTaggedValue(-1));
    OperationResult op_result2 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float64_array, JSTaggedValue(-0.0));
    OperationResult op_result3 =
        JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float64_array, JSTaggedValue(1.1));
    OperationResult op_result4 = JSTypedArray::IntegerIndexedElementGet(thread_, handle_tag_val_float64_array,
                                                                        JSTaggedValue(num_elements_float64_array));
    EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result3.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    EXPECT_EQ(op_result4.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());

    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_float64_array, JSTaggedValue(-1),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_float64_array,
                                                        JSTaggedValue(num_elements_float64_array),
                                                        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
}

/*
 * Feature: JSTypedArray
 * Function: FastElementGet
 * SubFunction: IntegerIndexedElementSet
 * FunctionPoints: Get Element At Index(uint32_t) Of JSTypedArray
 * CaseDescription: Check whether the OperationResults returned through calling FastElementGet function from the
 *                  JSTypedArray changed through calling IntegerIndexedElementSet function are within expectations.
 */
TEST_F(JSTypedArrayTest, FastElementGet_TypedArray)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    for (uint32_t j = 0; j < c_vec_js_type_.size(); j++) {
        uint32_t num_elements_typed_array = 10;
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(j));
        JSHandle<JSTaggedValue> handle_tag_val_typed_array = JSHandle<JSTaggedValue>::Cast(handle_typed_array);

        uint32_t size_element =
            ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_typed_array));
        uint32_t byte_length_viewd_array_buffer = size_element * num_elements_typed_array;
        JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
            JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
        handle_typed_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
        handle_typed_array->SetArrayLength(thread_, JSTaggedValue(num_elements_typed_array));

        JSHandle<JSTaggedValue> handle_tag_val_value_set(
            thread_, JSTaggedValue(c_vec_handle_tag_val_value_for_typed_array_.at(j)));
        for (uint32_t i = 0; i < num_elements_typed_array; i++) {
            EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_typed_array, JSTaggedValue(i),
                                                               handle_tag_val_value_set));
        }
        for (uint32_t i = 0; i < num_elements_typed_array; i++) {
            OperationResult op_result = JSTypedArray::FastElementGet(thread_, handle_tag_val_typed_array, i);
            EXPECT_EQ(op_result.GetValue().GetTaggedValue().GetNumber(),
                      handle_tag_val_value_set.GetTaggedValue().GetNumber());
        }

        OperationResult op_result1 = JSTypedArray::FastElementGet(thread_, handle_tag_val_typed_array, -1);
        OperationResult op_result2 =
            JSTypedArray::FastElementGet(thread_, handle_tag_val_typed_array, num_elements_typed_array);
        EXPECT_EQ(op_result1.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
        EXPECT_EQ(op_result2.GetValue().GetTaggedValue(), JSTaggedValue::Undefined());
    }
}

/*
 * Feature: JSTypedArray
 * Function: DefineOwnProperty
 * SubFunction: GetOwnProperty/HasProperty
 *              PropertyDescriptor::HasWritable/HasEnumerable/HasConfigurable/IsWritable/IsEnumerable/IsConfigurable
 * FunctionPoints: Define Own Property For Element At Index(JSTaggedValue) Of JSTypedArray
 * CaseDescription: Call DefineOwnProperty function with a JSTypedArray, a index(JSTaggedValue) and a source
 *                  PropertyDescriptor, check whether the bool returned through calling HasProperty function with the
 *                  JSTypedArray and the index(JSTaggedValue) is within expectations, check whether the target
 *                  PropertyDescriptor changed through GetOwnProperty function is with expectations.
 */
TEST_F(JSTypedArrayTest, DefineOwnProperty_TypedArray)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    for (size_t j = 0; j < c_vec_js_type_.size(); j++) {
        int32_t num_elements_typed_array = 10;
        JSHandle<JSTaggedValue> handle_tag_val_value_def(thread_, c_vec_handle_tag_val_value_for_typed_array_.at(j));
        PropertyDescriptor desc_from1(thread_, handle_tag_val_value_def, true, true, true);
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(j));
        JSHandle<JSTaggedValue> handle_tag_val_typed_array = JSHandle<JSTaggedValue>::Cast(handle_typed_array);

        uint32_t size_element =
            ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_typed_array));
        int32_t byte_length_viewd_array_buffer = size_element * num_elements_typed_array;
        JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
            JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
        handle_typed_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
        handle_typed_array->SetArrayLength(thread_, JSTaggedValue(num_elements_typed_array));

        for (int32_t i = 0; i < num_elements_typed_array; i++) {
            JSHandle<JSTaggedValue> handle_tag_val_key(thread_, JSTaggedValue(i));
            EXPECT_FALSE(JSTypedArray::HasProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key));
            EXPECT_TRUE(
                JSTypedArray::DefineOwnProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key, desc_from1));
            EXPECT_TRUE(JSTypedArray::HasProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key));
            EXPECT_TRUE(JSTaggedValue::StrictEqual(
                thread_, handle_tag_val_value_def,
                JSTypedArray::GetProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key).GetValue()));

            PropertyDescriptor desc_to1(thread_);
            EXPECT_FALSE(desc_to1.HasWritable() || desc_to1.HasEnumerable() || desc_to1.HasConfigurable());
            EXPECT_TRUE(
                JSTypedArray::GetOwnProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key, desc_to1));
            EXPECT_TRUE(desc_to1.HasWritable() && desc_to1.HasEnumerable() && desc_to1.HasConfigurable());
            EXPECT_TRUE(desc_to1.IsWritable() && desc_to1.IsEnumerable() && desc_to1.IsConfigurable());
            EXPECT_TRUE(JSTaggedValue::StrictEqual(thread_, desc_to1.GetValue(), handle_tag_val_value_def));
        }
    }
}

/*
 * Feature: JSTypedArray
 * Function: SetProperty
 * SubFunction: GetProperty/HasProperty
 * FunctionPoints: Set Property For Element At Index(JSTaggedValue) Of JSTypedArray
 * CaseDescription: Call SetProperty function with a JSTypedArray, a index(JSTaggedValue) and a source
 *                  value(JSTaggedValue), check whether the bool returned through calling HasProperty function with the
 *                  JSTypedArray and the index(JSTaggedValue) is within expectations, check whether the
 *                  value(JSTaggedValue) of the OperationResult returned through calling GetProperty function with the
 *                  JSTypedArray and the index(JSTaggedValue) is the same with the source value(JSTaggedValue).
 */
TEST_F(JSTypedArrayTest, SetProperty_TypedArray)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    for (size_t j = 0; j < c_vec_js_type_.size(); j++) {
        int32_t num_elements_typed_array = 10;
        JSHandle<JSTaggedValue> handle_tag_val_value_set(thread_, c_vec_handle_tag_val_value_for_typed_array_.at(j));
        JSHandle<JSTypedArray> handle_typed_array = CreateNumberTypedArray(thread_, c_vec_js_type_.at(j));
        JSHandle<JSTaggedValue> handle_tag_val_typed_array = JSHandle<JSTaggedValue>::Cast(handle_typed_array);

        uint32_t size_element =
            ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_typed_array));
        int32_t byte_length_viewd_array_buffer = size_element * num_elements_typed_array;
        JSHandle<JSTaggedValue> handle_tag_val_array_buffer_from =
            JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
        handle_typed_array->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer_from);
        handle_typed_array->SetArrayLength(thread_, JSTaggedValue(num_elements_typed_array));

        for (int32_t i = 0; i < num_elements_typed_array; i++) {
            JSHandle<JSTaggedValue> handle_tag_val_key(thread_, JSTaggedValue(i));
            EXPECT_FALSE(JSTypedArray::HasProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key));
            EXPECT_TRUE(JSTypedArray::SetProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key,
                                                  handle_tag_val_value_set));
            EXPECT_TRUE(JSTypedArray::HasProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key));
            EXPECT_TRUE(JSTaggedValue::StrictEqual(
                thread_, handle_tag_val_value_set,
                JSTypedArray::GetProperty(thread_, handle_tag_val_typed_array, handle_tag_val_key).GetValue()));
        }
    }
}

/*
 * Feature: JSTypedArray
 * Function: FastCopyElementToArray
 * SubFunction: IntegerIndexedElementSet/TaggedArray::Get
 * FunctionPoints: Copy All Elements Of JSTypedArray To TaggedArray Fast
 * CaseDescription: Create a source JSTypedArray and a target TaggedArray, init the elements of the source JSTypedArray
 *                  with a certain value(JSTaggedValue) through calling IntegerIndexedElementSet function. Call
 *                  FastCopyElementToArray function with the source JSTypedArray and the target TaggedArray. Check
 *                  whether the values(JSTaggedValue) returned through Get(TaggedArray) function are the same with the
 *                  certain value(JSTaggedValue).
 */
TEST_F(JSTypedArrayTest, FastCopyElementToArray_TypedArray)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    for (size_t j = 0; j < c_vec_js_type_.size(); j++) {
        int32_t num_elements_typed_array = 10;
        JSHandle<JSTaggedValue> handle_tag_val_value_set(thread_, c_vec_handle_tag_val_value_for_typed_array_.at(j));
        JSHandle<JSTypedArray> handle_typed_array_from = CreateNumberTypedArray(thread_, c_vec_js_type_.at(j));
        JSHandle<JSTaggedValue> handle_tag_val_typed_array_from =
            JSHandle<JSTaggedValue>::Cast(handle_typed_array_from);

        uint32_t size_element =
            ecmascript::base::TypedArrayHelper::GetElementSize(JSHandle<JSObject>::Cast(handle_typed_array_from));
        int32_t byte_length_viewd_array_buffer = size_element * num_elements_typed_array;
        JSHandle<JSTaggedValue> handle_tag_val_array_buffer =
            JSHandle<JSTaggedValue>::Cast(factory->NewJSArrayBuffer(byte_length_viewd_array_buffer));
        JSHandle<TaggedArray> handle_tag_arr_to = factory->NewTaggedArray(byte_length_viewd_array_buffer);
        handle_typed_array_from->SetViewedArrayBuffer(thread_, handle_tag_val_array_buffer);
        handle_typed_array_from->SetArrayLength(thread_, JSTaggedValue(num_elements_typed_array));

        for (int32_t i = 0; i < num_elements_typed_array; i++) {
            EXPECT_TRUE(JSTypedArray::IntegerIndexedElementSet(thread_, handle_tag_val_typed_array_from,
                                                               JSTaggedValue(i), handle_tag_val_value_set));
        }
        EXPECT_TRUE(JSTypedArray::FastCopyElementToArray(thread_, handle_tag_val_typed_array_from, handle_tag_arr_to));
        for (int32_t i = 0; i < num_elements_typed_array; i++) {
            EXPECT_EQ(handle_tag_arr_to->Get(i), handle_tag_val_value_set.GetTaggedValue());
        }
    }
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers,modernize-avoid-c-arrays,modernize-loop-convert)
