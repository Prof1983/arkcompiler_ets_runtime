/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"

#include "handle_base.h"
#include "include/runtime.h"
#include "runtime/include/thread_scopes.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class LargeObjectTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
        thread_->GetEcmaVM()->GetFactory()->SetTriggerGc(false);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

#if !defined(NDEBUG)
static JSObject *JSObjectTestCreate(JSThread *thread)
{
    [[maybe_unused]] ecmascript::EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> js_func = global_env->GetObjectFunction();
    JSHandle<JSObject> new_obj =
        ecma_vm->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(js_func), js_func);
    return *new_obj;
}
#endif

#if !defined(NDEBUG)
static TaggedArray *LargeArrayTestCreate(JSThread *thread)
{
    [[maybe_unused]] ecmascript::EcmaHandleScope scope(thread);
    // NOLINTNEXTLINE(readability-magic-numbers)
    JSHandle<TaggedArray> array = thread->GetEcmaVM()->GetFactory()->NewTaggedArray(20 * 10000);
    return *array;
}
#endif

TEST_F(LargeObjectTest, LargeArrayKeep)
{
#if !defined(NDEBUG)
    TaggedArray *array = LargeArrayTestCreate(thread_);
    EXPECT_TRUE(array != nullptr);
    JSHandle<TaggedArray> array_handle(thread_, array);
    JSHandle<JSObject> new_obj(thread_, JSObjectTestCreate(thread_));
    array_handle->Set(thread_, 0, new_obj.GetTaggedValue());
    auto ecma_vm = thread_->GetEcmaVM();
    EXPECT_EQ(*array_handle, reinterpret_cast<ObjectHeader *>(array));
    ecma_vm->GetGC()->WaitForGCInManaged(GCTask(GCTaskCause::EXPLICIT_CAUSE));  // Trigger GC.
    ecma_vm->GetGC()->WaitForGCInManaged(GCTask(GCTaskCause::EXPLICIT_CAUSE));  // Trigger GC.
    EXPECT_EQ(*new_obj, array->Get(0).GetHeapObject());
    EXPECT_EQ(*array_handle, reinterpret_cast<ObjectHeader *>(array));
#endif
}

}  // namespace panda::test
