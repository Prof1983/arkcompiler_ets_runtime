/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_function_extra_info.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSFunctionTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

JSFunction *JSObjectCreate(JSThread *thread)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    return global_env->GetObjectFunction().GetObject<JSFunction>();
}

TEST_F(JSFunctionTest, Create)
{
    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSFunction> fun_handle = thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env);
    EXPECT_TRUE(*fun_handle != nullptr);
    EXPECT_EQ(fun_handle->GetProtoOrDynClass(), JSTaggedValue::Hole());

    JSHandle<LexicalEnv> lexical_env = thread_->GetEcmaVM()->GetFactory()->NewLexicalEnv(0);
    fun_handle->SetLexicalEnv(thread_, lexical_env.GetTaggedValue());
    EXPECT_EQ(fun_handle->GetLexicalEnv(), lexical_env.GetTaggedValue());
    EXPECT_TRUE(*lexical_env != nullptr);
}
TEST_F(JSFunctionTest, MakeConstructor)
{
    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSFunction> func = thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, static_cast<void *>(nullptr),
                                                                                  FunctionKind::BASE_CONSTRUCTOR);
    EXPECT_TRUE(*func != nullptr);
    JSHandle<JSTaggedValue> func_handle(func);
    func->GetJSHClass()->SetExtensible(true);

    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> obj = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSTaggedValue> obj_value(obj);

    JSFunction::MakeConstructor(thread_, func, obj_value);

    JSHandle<JSTaggedValue> constructor_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("constructor"));

    JSHandle<JSTaggedValue> proto_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("prototype"));
    JSTaggedValue proto = JSObject::GetProperty(thread_, func_handle, proto_key).GetValue().GetTaggedValue();
    JSTaggedValue constructor =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), constructor_key).GetValue().GetTaggedValue();
    EXPECT_EQ(constructor, func_handle.GetTaggedValue());
    EXPECT_EQ(proto, obj.GetTaggedValue());
    EXPECT_EQ(func->GetFunctionKind(), FunctionKind::BASE_CONSTRUCTOR);
}

TEST_F(JSFunctionTest, OrdinaryHasInstance)
{
    JSHandle<JSTaggedValue> obj_fun(thread_, JSObjectCreate(thread_));

    JSHandle<JSObject> jsobject =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> obj(thread_, jsobject.GetTaggedValue());
    EXPECT_TRUE(*jsobject != nullptr);

    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> constructor = global_env->GetObjectFunction();
    EXPECT_TRUE(ecmascript::JSFunction::OrdinaryHasInstance(thread_, constructor, obj));
}

JSTaggedValue TestInvokeInternal(EcmaRuntimeCallInfo *argv)
{
    if (argv->GetArgsNumber() == 1 && argv->GetCallArg(0).GetTaggedValue() == JSTaggedValue(1)) {
        return builtins_common::GetTaggedBoolean(true);
    }
    return builtins_common::GetTaggedBoolean(false);
}

TEST_F(JSFunctionTest, Invoke)
{
    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> dynclass(thread_, JSObjectCreate(thread_));
    JSHandle<JSTaggedValue> callee(
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>::Cast(dynclass), dynclass));
    EXPECT_TRUE(*callee != nullptr);

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    char key_array[] = "invoked";
    JSHandle<JSTaggedValue> callee_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(&key_array[0]));
    JSHandle<JSFunction> callee_func =
        thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, reinterpret_cast<void *>(TestInvokeInternal));
    JSHandle<JSTaggedValue> callee_value(callee_func);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(callee), callee_key, callee_value);

    auto info = NewRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), callee, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(JSTaggedValue(1));
    JSTaggedValue res = JSFunction::Invoke(info.Get(), callee_key);

    JSTaggedValue ruler = builtins_common::GetTaggedBoolean(true);
    EXPECT_EQ(res.GetRawData(), ruler.GetRawData());
}

TEST_F(JSFunctionTest, SetSymbolFunctionName)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> js_function = factory->NewJSFunction(env);
    JSHandle<JSSymbol> symbol = factory->NewPublicSymbolWithChar("name");
    JSHandle<EcmaString> name = factory->NewFromString("[name]");
    JSHandle<JSTaggedValue> prefix(thread_, JSTaggedValue::Undefined());
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(js_function), JSHandle<JSTaggedValue>(symbol),
                                prefix);
    JSHandle<JSTaggedValue> function_name =
        JSFunctionBase::GetFunctionName(thread_, JSHandle<JSFunctionBase>(js_function));
    EXPECT_TRUE(function_name->IsString());
    EXPECT_TRUE(EcmaString::StringsAreEqual(*(JSHandle<EcmaString>(function_name)), *name));
}

}  // namespace panda::test
