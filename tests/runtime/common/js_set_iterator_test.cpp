/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_set_iterator.h"
#include "plugins/ecmascript/runtime/js_set.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSSetIteratorTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    ecmascript::EcmaHandleScope *scope_ {nullptr};
    PandaVM *instance_ {nullptr};
};

JSSet *CreateSet(JSThread *thread)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();

    JSHandle<JSTaggedValue> constructor = env->GetSetFunction();
    JSHandle<JSSet> set =
        JSHandle<JSSet>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor));
    JSHandle<LinkedHashSet> hash_set = LinkedHashSet::Create(thread);
    set->SetLinkedSet(thread, hash_set);
    return JSSet::Cast(set.GetTaggedValue().GetTaggedObject());
}

/*
 * Feature: JSSetIterator
 * Function: CreateSetIterator
 * SubFunction: GetIterationKind,GetNextIndex
 * FunctionPoints: Create SetIterator
 * CaseDescription: Check whether the returned value through "CreateSetIterator" function is within expectations.
 */
TEST_F(JSSetIteratorTest, CreateSetIterator)
{
    JSHandle<JSSet> js_set(thread_, CreateSet(thread_));
    EXPECT_TRUE(*js_set != nullptr);

    JSHandle<JSTaggedValue> set_iterator_value1 =
        JSSetIterator::CreateSetIterator(thread_, JSHandle<JSTaggedValue>(js_set), IterationKind::KEY);

    EXPECT_EQ(set_iterator_value1->IsJSSetIterator(), true);
    JSHandle<JSSetIterator> set_iterator1(set_iterator_value1);
    EXPECT_EQ(JSTaggedValue::SameValue(set_iterator1->GetIteratedSet(), js_set->GetLinkedSet()), true);
    EXPECT_EQ(set_iterator1->GetNextIndex().GetInt(), 0);

    JSTaggedValue iteration_kind1 = set_iterator1->GetIterationKind();
    EXPECT_EQ(JSTaggedValue::SameValue(iteration_kind1, JSTaggedValue(static_cast<int>(IterationKind::KEY))), true);

    JSHandle<JSTaggedValue> set_iterator_value2 =
        JSSetIterator::CreateSetIterator(thread_, JSHandle<JSTaggedValue>(js_set), IterationKind::VALUE);

    EXPECT_EQ(set_iterator_value2->IsJSSetIterator(), true);
    JSHandle<JSSetIterator> set_iterator2(set_iterator_value2);
    EXPECT_EQ(JSTaggedValue::SameValue(set_iterator2->GetIteratedSet(), js_set->GetLinkedSet()), true);
    EXPECT_EQ(set_iterator2->GetNextIndex().GetInt(), 0);

    JSTaggedValue iteration_kind2 = set_iterator2->GetIterationKind();
    EXPECT_EQ(JSTaggedValue::SameValue(iteration_kind2, JSTaggedValue(static_cast<int>(IterationKind::VALUE))), true);
}

/*
 * Feature: JSSetIterator
 * Function: next
 * SubFunction: IteratorValue
 * FunctionPoints: get the next value in setiterator
 * CaseDescription: Check whether the return value obtained by the function is the next value in the array element.
 */
TEST_F(JSSetIteratorTest, Next)
{
    JSHandle<JSSet> js_set(thread_, CreateSet(thread_));
    EXPECT_TRUE(*js_set != nullptr);

    for (int i = 0; i < 3; i++) {
        JSHandle<JSTaggedValue> key(thread_, JSTaggedValue(i));
        JSSet::Add(thread_, js_set, key);
    }

    // set IterationKind(key or value)
    JSHandle<JSTaggedValue> set_iterator_value =
        JSSetIterator::CreateSetIterator(thread_, JSHandle<JSTaggedValue>(js_set), IterationKind::KEY);
    JSHandle<JSSetIterator> set_iterator(set_iterator_value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(set_iterator_value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());
    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());

    JSTaggedValue result1 = JSSetIterator::Next(ecma_runtime_call_info.get());
    EXPECT_EQ(set_iterator->GetNextIndex().GetInt(), 1);
    JSHandle<JSTaggedValue> result_obj1(thread_, result1);
    EXPECT_EQ(0, JSIterator::IteratorValue(thread_, result_obj1)->GetInt());

    JSTaggedValue result2 = JSSetIterator::Next(ecma_runtime_call_info.get());
    EXPECT_EQ(set_iterator->GetNextIndex().GetInt(), 2);
    JSHandle<JSTaggedValue> result_obj2(thread_, result2);
    EXPECT_EQ(1, JSIterator::IteratorValue(thread_, result_obj2)->GetInt());

    JSTaggedValue result3 = JSSetIterator::Next(ecma_runtime_call_info.get());
    EXPECT_EQ(set_iterator->GetNextIndex().GetInt(), 3);
    JSHandle<JSTaggedValue> result_obj3(thread_, result3);
    EXPECT_EQ(2, JSIterator::IteratorValue(thread_, result_obj3)->GetInt());

    JSTaggedValue result4 = JSSetIterator::Next(ecma_runtime_call_info.get());
    JSHandle<JSTaggedValue> result_obj4(thread_, result4);
    EXPECT_EQ(JSIterator::IteratorValue(thread_, result_obj4).GetTaggedValue(), JSTaggedValue::Undefined());

    TestHelper::TearDownFrame(thread_, prev);
}
}  // namespace panda::test
