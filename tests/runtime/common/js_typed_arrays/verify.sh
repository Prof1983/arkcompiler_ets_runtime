#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.

set -eo pipefail

expected="1
undefined
2
undefined
RangeError: Out of range
Not support function.toString() due to Runtime can not obtain Source Code yet.
Not support function.toString() due to Runtime can not obtain Source Code yet.
RangeError: Out of range"

actual=$(cat "$1")
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mrestargstest failed\033[0m"
    exit 1;
fi
