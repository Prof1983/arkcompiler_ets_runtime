let a = new Uint8Array(10);
a['aa'] = 1;
a[0] = 2;
a[20] = 3;
a[-1] = 4;
print(a['aa']);
print(a[-1]);
print(a[0]);
print(a[20]);

try {
    new Uint8ClampedArray(3122127621);
} catch (e) {
    print(e);
}

let b = new Float32Array();
b[Float32Array] = Float32Array;
for (let k of Object.keys(b)) {
  print(k);
  print(b[k]);
}

try {
    new Uint32Array(new Uint16Array(1000000000.0));
} catch (e) {
    print(e);
}
