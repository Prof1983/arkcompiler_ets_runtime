/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"

#include "plugins/ecmascript/runtime/ecma_string.h"
#include "include/coretypes/array.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_operator.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSPrimitiveRefTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

TEST_F(JSPrimitiveRefTest, StringCreate)
{
    JSHandle<JSTaggedValue> hello(thread_->GetEcmaVM()->GetFactory()->NewFromString("hello"));
    JSHandle<JSObject> str(JSPrimitiveRef::StringCreate(thread_, hello));

    JSHandle<JSTaggedValue> idx(thread_->GetEcmaVM()->GetFactory()->NewFromString("0"));
    bool status = JSPrimitiveRef::HasProperty(thread_, str, idx);
    ASSERT_TRUE(status);

    PropertyDescriptor desc(thread_);
    status = JSPrimitiveRef::GetOwnProperty(thread_, str, idx, desc);
    ASSERT_TRUE(status);
    JSHandle<EcmaString> h = thread_->GetEcmaVM()->GetFactory()->NewFromString("h");
    JSHandle<EcmaString> h2 = JSTaggedValue::ToString(thread_, desc.GetValue());
    ASSERT_TRUE(h->Compare(*h2) == 0);
}

}  // namespace panda::test
