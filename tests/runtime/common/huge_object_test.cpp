/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

namespace panda::test {
class HugeObjectTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

#if !defined(NDEBUG)
using panda::ecmascript::JSFunction;
using panda::ecmascript::JSHandle;
using panda::ecmascript::JSObject;
using panda::ecmascript::TaggedArray;
using panda::ecmascript::TaggedObject;
#endif

#if !defined(NDEBUG)
static JSObject *JSObjectTestCreate(JSThread *thread)
{
    [[maybe_unused]] ecmascript::EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    auto global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> js_func = global_env->GetObjectFunction();
    JSHandle<JSObject> new_obj =
        ecma_vm->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(js_func), js_func);
    return *new_obj;
}
#endif

#if !defined(NDEBUG)
static TaggedArray *LargeArrayTestCreate(JSThread *thread)
{
    [[maybe_unused]] ecmascript::EcmaHandleScope scope(thread);
    static constexpr size_t SIZE = 1024 * 1024;
    JSHandle<TaggedArray> array = thread->GetEcmaVM()->GetFactory()->NewTaggedArray(SIZE);
    return *array;
}
#endif

TEST_F(HugeObjectTest, LargeArrayKeep)
{
#if !defined(NDEBUG)
    TaggedArray *array = LargeArrayTestCreate(thread_);
    EXPECT_TRUE(array != nullptr);
    JSHandle<TaggedArray> array_handle(thread_, array);
    JSHandle<JSObject> new_obj(thread_, JSObjectTestCreate(thread_));
    array_handle->Set(thread_, 0, new_obj.GetTaggedValue());
    auto ecma_vm = thread_->GetEcmaVM();
    EXPECT_EQ(*array_handle, reinterpret_cast<TaggedObject *>(array));
    ecma_vm->CollectGarbage();  // Trigger GC.
    ecma_vm->CollectGarbage();  // Trigger GC.
    EXPECT_EQ(*new_obj, array->Get(0).GetTaggedObject());
    EXPECT_EQ(*array_handle, reinterpret_cast<TaggedObject *>(array));
#endif
}
}  // namespace panda::test
