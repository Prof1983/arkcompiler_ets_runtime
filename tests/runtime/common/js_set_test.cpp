/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"
#include "include/coretypes/dyn_objects.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "include/coretypes/tagged_value.h"
#include "include/runtime.h"
#include "include/runtime_options.h"
#include "plugins/ecmascript/runtime/tagged_hash_table-inl.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_set.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_set_iterator.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSSetTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

    JSSet *CreateSet()
    {
        ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
        JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

        JSHandle<JSTaggedValue> constructor = env->GetSetFunction();
        JSHandle<JSSet> set =
            JSHandle<JSSet>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor));
        JSHandle<LinkedHashSet> hash_set = LinkedHashSet::Create(thread_);
        set->SetLinkedSet(thread_, hash_set);
        return JSSet::Cast(set.GetTaggedValue().GetTaggedObject());
    }

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

TEST_F(JSSetTest, SetCreate)
{
    JSSet *set = CreateSet();
    EXPECT_TRUE(set != nullptr);
}

TEST_F(JSSetTest, AddAndHas)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // create js_set
    JSHandle<JSSet> set(thread_, CreateSet());

    //
    JSHandle<JSTaggedValue> key(factory->NewFromString("key"));
    JSSet::Add(thread_, set, key);
    int hash = LinkedHash::Hash(key.GetTaggedValue());
    EXPECT_TRUE(set->Has(key.GetTaggedValue(), hash));
}

TEST_F(JSSetTest, DeleteAndGet)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // create js_set
    JSHandle<JSSet> set(thread_, CreateSet());

    // add 40 keys
    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    char key_array[] = "key0";
    // NOLINTNEXTLINE(readability-magic-numbers)
    for (int i = 0; i < 40; i++) {
        key_array[3] = '1' + i;
        JSHandle<JSTaggedValue> key(factory->NewFromString(key_array));
        JSSet::Add(thread_, set, key);
        int hash = LinkedHash::Hash(key.GetTaggedValue());
        EXPECT_TRUE(set->Has(key.GetTaggedValue(), hash));
    }
    EXPECT_EQ(set->GetSize(), 40);
    // whether js_set has delete key
    key_array[3] = '1' + 8;
    JSHandle<JSTaggedValue> delete_key(factory->NewFromString(key_array));
    JSSet::Delete(thread_, set, delete_key);
    int hash = LinkedHash::Hash(delete_key.GetTaggedValue());
    EXPECT_FALSE(set->Has(delete_key.GetTaggedValue(), hash));
    EXPECT_EQ(set->GetSize(), 39);
}

TEST_F(JSSetTest, Iterator)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSSet> set(thread_, CreateSet());
    for (int i = 0; i < 5; i++) {
        JSHandle<JSTaggedValue> key(thread_, JSTaggedValue(i));
        JSSet::Add(thread_, set, key);
    }

    JSHandle<JSTaggedValue> key_iter(factory->NewJSSetIterator(set, IterationKind::KEY));
    JSHandle<JSTaggedValue> value_iter(factory->NewJSSetIterator(set, IterationKind::VALUE));

    JSHandle<JSTaggedValue> key_result0 = JSIterator::IteratorStep(thread_, key_iter);
    JSHandle<JSTaggedValue> value_result0 = JSIterator::IteratorStep(thread_, value_iter);

    EXPECT_EQ(0, JSIterator::IteratorValue(thread_, key_result0)->GetInt());
    EXPECT_EQ(0, JSIterator::IteratorValue(thread_, value_result0)->GetInt());

    JSHandle<JSTaggedValue> key_result1 = JSIterator::IteratorStep(thread_, key_iter);
    EXPECT_EQ(1, JSIterator::IteratorValue(thread_, key_result1)->GetInt());

    for (int i = 0; i < 3; i++) {
        JSHandle<JSTaggedValue> key(thread_, JSTaggedValue(i));
        JSSet::Delete(thread_, set, key);
    }

    JSHandle<JSTaggedValue> key_result2 = JSIterator::IteratorStep(thread_, key_iter);
    EXPECT_EQ(3, JSIterator::IteratorValue(thread_, key_result2)->GetInt());
    JSHandle<JSTaggedValue> key_result3 = JSIterator::IteratorStep(thread_, key_iter);
    EXPECT_EQ(4, JSIterator::IteratorValue(thread_, key_result3)->GetInt());
    JSHandle<JSTaggedValue> key(thread_, JSTaggedValue(5));
    JSSet::Add(thread_, set, key);
    JSHandle<JSTaggedValue> key_result4 = JSIterator::IteratorStep(thread_, key_iter);
    EXPECT_EQ(5, JSIterator::IteratorValue(thread_, key_result4)->GetInt());
    JSHandle<JSTaggedValue> key_result5 = JSIterator::IteratorStep(thread_, key_iter);
    EXPECT_EQ(JSTaggedValue::False(), key_result5.GetTaggedValue());
}

}  // namespace panda::test
