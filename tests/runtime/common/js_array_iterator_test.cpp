/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

using panda::ecmascript::IterationKind;
using panda::ecmascript::JSArray;
using panda::ecmascript::JSArrayIterator;
using panda::ecmascript::JSHandle;
using panda::ecmascript::JSObject;
using panda::ecmascript::ObjectFactory;
using panda::ecmascript::TaggedArray;

namespace panda::test {
class JSArrayIteratorTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

public:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread {nullptr};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

/*
 * Feature: JSArrayIterator
 * Function: SetIteratedArray
 * SubFunction: GetIteratedArray
 * FunctionPoints: Set Iterated Array
 * CaseDescription: Call the "SetIteratedArray" function, check whether the result returned through "GetIteratedArray"
 *                  function from the JSArrayIterator is within expectations.
 */
TEST_F(JSArrayIteratorTest, SetIteratedArray)
{
    EcmaVM *ecma_vm_ptr = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm_ptr->GetFactory();

    // NOLINTNEXTLINE(modernize-avoid-c-arrays,readability-magic-numbers)
    uint32_t array_from1[10] = {0, 6, 8, 99, 200, 1, static_cast<uint32_t>(-1), static_cast<uint32_t>(-199), 33, 100};
    // NOLINTNEXTLINE(modernize-avoid-c-arrays,readability-magic-numbers)
    uint32_t array_from2[10] = {1111, 3211, 737, 0, 1267, 174, 2763, 832, 11, 93};
    int num_array_from1 = sizeof(array_from1) / sizeof(array_from1[0]);
    int num_array_from2 = sizeof(array_from2) / sizeof(array_from2[0]);
    JSHandle<TaggedArray> handle_tagged_array_from1(factory->NewTaggedArray(num_array_from1));
    JSHandle<TaggedArray> handle_tagged_array_from2(factory->NewTaggedArray(num_array_from2));
    for (int i = 0; i < num_array_from1; i++) {
        handle_tagged_array_from1->Set(thread, i, JSTaggedValue(array_from1[i]));
    }
    for (int i = 0; i < num_array_from2; i++) {
        handle_tagged_array_from2->Set(thread, i, JSTaggedValue(array_from2[i]));
    }
    JSHandle<JSObject> handle_js_object_tagged_array_from1(
        JSArray::CreateArrayFromList(thread, handle_tagged_array_from1));
    JSHandle<JSObject> handle_js_object_tagged_array_from2(
        JSArray::CreateArrayFromList(thread, handle_tagged_array_from2));

    // Call "SetIteratedArray" function through "NewJSArrayIterator" function of "object_factory.cpp".
    JSHandle<JSArrayIterator> handle_js_array_iter =
        factory->NewJSArrayIterator(handle_js_object_tagged_array_from1, IterationKind::KEY);

    JSHandle<JSArray> handle_js_array_to1(thread,
                                          JSArray::Cast(handle_js_array_iter->GetIteratedArray().GetTaggedObject()));
    EXPECT_EQ(handle_js_array_to1->GetArrayLength(), num_array_from1);
    for (int i = 0; i < num_array_from1; i++) {
        EXPECT_EQ(JSArray::FastGetPropertyByValue(thread, JSHandle<JSTaggedValue>(handle_js_array_to1), i)->GetNumber(),
                  array_from1[i]);
    }

    // Call "SetIteratedArray" function in this TEST_F.
    handle_js_array_iter->SetIteratedArray(thread, handle_js_object_tagged_array_from2);

    JSHandle<JSArray> handle_js_array_to2(thread,
                                          JSArray::Cast(handle_js_array_iter->GetIteratedArray().GetTaggedObject()));
    EXPECT_EQ(handle_js_array_to2->GetArrayLength(), num_array_from2);
    for (int i = 0; i < num_array_from2; i++) {
        EXPECT_EQ(JSArray::FastGetPropertyByValue(thread, JSHandle<JSTaggedValue>(handle_js_array_to2), i)->GetNumber(),
                  array_from2[i]);
    }
}

/*
 * Feature: JSArrayIterator
 * Function: SetNextIndex
 * SubFunction: GetNextIndex
 * FunctionPoints: Set Next Index
 * CaseDescription: Call the "SetNextIndex" function, check whether the result returned through "GetNextIndex" function
 *                  from the JSArrayIterator is within expectations.
 */
TEST_F(JSArrayIteratorTest, SetNextIndex)
{
    EcmaVM *ecma_vm_ptr = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm_ptr->GetFactory();

    // NOLINTNEXTLINE(modernize-avoid-c-arrays,readability-magic-numbers)
    uint32_t array[10] = {0, 6, 8, 99, 200, 1, static_cast<uint32_t>(-1), static_cast<uint32_t>(-199), 33, 100};
    int num_array = sizeof(array) / sizeof(array[0]);
    JSHandle<TaggedArray> handle_tagged_array(factory->NewTaggedArray(num_array));
    for (int i = 0; i < num_array; i++) {
        handle_tagged_array->Set(thread, i, JSTaggedValue(array[i]));
    }
    JSHandle<JSObject> handle_js_object_tagged_array(JSArray::CreateArrayFromList(thread, handle_tagged_array));

    // Call "SetNextIndex" function through "NewJSArrayIterator" function of "object_factory.cpp".
    JSHandle<JSArrayIterator> handle_js_array_iter =
        factory->NewJSArrayIterator(handle_js_object_tagged_array, IterationKind::KEY);
    EXPECT_EQ(handle_js_array_iter->GetNextIndex().GetNumber(), 0);

    // NOLINTNEXTLINE(readability-magic-numbers)
    int test_quantity = 100;
    for (int i = 1; i <= test_quantity; i++) {
        JSHandle<JSTaggedValue> handle_tag_val_next_index(thread, JSTaggedValue(i));

        // Call "SetNextIndex" function in this TEST_F.
        handle_js_array_iter->SetNextIndex(thread, handle_tag_val_next_index);
        EXPECT_EQ(handle_js_array_iter->GetNextIndex().GetNumber(), i);
    }
}

/*
 * Feature: JSArrayIterator
 * Function: SetIterationKind
 * SubFunction: GetIterationKind
 * FunctionPoints: Set Iteration Kind
 * CaseDescription: Call the "SetIterationKind" function, check whether the result returned through "GetIterationKind"
 *                  function from the JSArrayIterator is within expectations.
 */
TEST_F(JSArrayIteratorTest, SetIterationKind)
{
    EcmaVM *ecma_vm_ptr = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm_ptr->GetFactory();

    // NOLINTNEXTLINE(modernize-avoid-c-arrays,readability-magic-numbers)
    uint32_t array[10] = {0, 6, 8, 99, 200, 1, static_cast<uint32_t>(-1), static_cast<uint32_t>(-199), 33, 100};
    int num_array = sizeof(array) / sizeof(array[0]);
    JSHandle<TaggedArray> handle_tagged_array(factory->NewTaggedArray(num_array));
    for (int i = 0; i < num_array; i++) {
        handle_tagged_array->Set(thread, i, JSTaggedValue(array[i]));
    }
    JSHandle<JSTaggedValue> handle_tag_val0(thread, JSTaggedValue(0));
    JSHandle<JSTaggedValue> handle_tag_val1(thread, JSTaggedValue(1));
    JSHandle<JSTaggedValue> handle_tag_val2(thread, JSTaggedValue(2));
    JSHandle<JSObject> handle_js_object_tagged_array(JSArray::CreateArrayFromList(thread, handle_tagged_array));

    // Call "SetIterationKind" function through "NewJSArrayIterator" function of "object_factory.cpp".
    JSHandle<JSArrayIterator> handle_js_array_iter =
        factory->NewJSArrayIterator(handle_js_object_tagged_array, IterationKind::KEY);
    EXPECT_EQ(handle_js_array_iter->GetIterationKind().GetNumber(), 0);
    handle_js_array_iter = factory->NewJSArrayIterator(handle_js_object_tagged_array, IterationKind::VALUE);
    EXPECT_EQ(handle_js_array_iter->GetIterationKind().GetNumber(), 1);
    handle_js_array_iter = factory->NewJSArrayIterator(handle_js_object_tagged_array, IterationKind::KEY_AND_VALUE);
    EXPECT_EQ(handle_js_array_iter->GetIterationKind().GetNumber(), 2);

    // Call "SetIterationKind" function in this TEST_F.
    handle_js_array_iter->SetIterationKind(thread, handle_tag_val0);
    EXPECT_EQ(handle_js_array_iter->GetIterationKind().GetNumber(), 0);
    handle_js_array_iter->SetIterationKind(thread, handle_tag_val1);
    EXPECT_EQ(handle_js_array_iter->GetIterationKind().GetNumber(), 1);
    handle_js_array_iter->SetIterationKind(thread, handle_tag_val2);
    EXPECT_EQ(handle_js_array_iter->GetIterationKind().GetNumber(), 2);
}
}  // namespace panda::test
