let a = 'spread_primitives'
let b = {...a};
print(b)
for (let i = 0; i < a.length; ++i) {
    print(b[i.toString()])
}

let c = {...322}
print(c)
print(Object.keys(c).length)

try {
    [...null]
} catch (e) {
    print(e)
}

try {
    [...undefined]
} catch(e) {
    print(e)
}
