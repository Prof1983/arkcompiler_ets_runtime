#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.

set -eo pipefail

expected="[object Object]
s
p
r
e
a
d
_
p
r
i
m
i
t
i
v
e
s
[object Object]
0
TypeError: null is not iterable
TypeError: undefined is not iterable"

actual=`cat $1`
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mrestargstest failed\033[0m"
    exit 1;
fi
