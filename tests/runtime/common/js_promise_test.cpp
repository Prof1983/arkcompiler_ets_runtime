/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"

#include "test_helper.h"

#include "include/runtime.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "include/runtime_options.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSPromiseTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

TEST_F(JSPromiseTest, CreateResolvingFunctions)
{
    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> promise_func = env->GetPromiseFunction();
    JSHandle<JSPromise> js_promise =
        JSHandle<JSPromise>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(promise_func), promise_func));
    JSHandle<ResolvingFunctionsRecord> reactions = JSPromise::CreateResolvingFunctions(thread_, js_promise);
    JSHandle<JSTaggedValue> resolve(thread_, reactions->GetResolveFunction());
    JSHandle<JSTaggedValue> reject(thread_, reactions->GetRejectFunction());
    EXPECT_EQ(resolve->IsCallable(), true);
    EXPECT_EQ(reject->IsCallable(), true);
}

TEST_F(JSPromiseTest, NewPromiseCapability)
{
    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> promise = env->GetPromiseFunction();

    JSHandle<PromiseCapability> capbility = JSPromise::NewPromiseCapability(thread_, promise);
    JSHandle<JSPromise> new_promise(thread_, capbility->GetPromise());
    EXPECT_EQ(static_cast<PromiseStatus>(new_promise->GetPromiseState().GetInt()), PromiseStatus::PENDING);

    JSHandle<JSPromiseReactionsFunction> resolve(thread_, capbility->GetResolve());
    JSHandle<JSPromiseReactionsFunction> reject(thread_, capbility->GetReject());
    EXPECT_EQ(resolve.GetTaggedValue().IsCallable(), true);
    EXPECT_EQ(resolve.GetTaggedValue().IsCallable(), true);

    JSHandle<JSPromise> resolve_promise(thread_, resolve->GetPromise());
    JSHandle<JSPromise> reject_promise(thread_, reject->GetPromise());
    EXPECT_EQ(JSTaggedValue::SameValue(new_promise.GetTaggedValue(), resolve_promise.GetTaggedValue()), true);
    EXPECT_EQ(JSTaggedValue::SameValue(new_promise.GetTaggedValue(), reject_promise.GetTaggedValue()), true);
}

TEST_F(JSPromiseTest, FullFillPromise)
{
    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> promise = env->GetPromiseFunction();
    JSHandle<PromiseCapability> capbility = JSPromise::NewPromiseCapability(thread_, promise);
    JSHandle<JSPromise> new_promise(thread_, capbility->GetPromise());
    EXPECT_EQ(static_cast<PromiseStatus>(new_promise->GetPromiseState().GetInt()), PromiseStatus::PENDING);
    EXPECT_EQ(new_promise->GetPromiseResult().IsUndefined(), true);

    JSHandle<JSTaggedValue> resolve(thread_, capbility->GetResolve());
    JSHandle<JSTaggedValue> undefined(thread_, JSTaggedValue::Undefined());

    auto info = NewRuntimeCallInfo(thread_, resolve, undefined, JSTaggedValue::Undefined(), 1);
    // NOLINTNEXTLINE(readability-magic-numbers)
    info->SetCallArgs(JSTaggedValue(33));
    JSFunction::Call(info.Get());
    EXPECT_EQ(static_cast<PromiseStatus>(new_promise->GetPromiseState().GetInt()), PromiseStatus::FULFILLED);
    EXPECT_EQ(JSTaggedValue::SameValue(new_promise->GetPromiseResult(), JSTaggedValue(33)), true);
}

TEST_F(JSPromiseTest, RejectPromise)
{
    EcmaVM *ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> promise = env->GetPromiseFunction();
    JSHandle<PromiseCapability> capbility = JSPromise::NewPromiseCapability(thread_, promise);
    JSHandle<JSPromise> new_promise(thread_, capbility->GetPromise());
    EXPECT_EQ(static_cast<PromiseStatus>(new_promise->GetPromiseState().GetInt()), PromiseStatus::PENDING);
    EXPECT_EQ(new_promise->GetPromiseResult().IsUndefined(), true);

    JSHandle<JSTaggedValue> reject(thread_, capbility->GetReject());
    JSHandle<JSTaggedValue> undefined(thread_, JSTaggedValue::Undefined());

    auto info = NewRuntimeCallInfo(thread_, reject, undefined, JSTaggedValue::Undefined(), 1);
    // NOLINTNEXTLINE(readability-magic-numbers)
    info->SetCallArgs(JSTaggedValue(44));
    JSFunction::Call(info.Get());
    EXPECT_EQ(static_cast<PromiseStatus>(new_promise->GetPromiseState().GetInt()), PromiseStatus::REJECTED);
    EXPECT_EQ(JSTaggedValue::SameValue(new_promise->GetPromiseResult(), JSTaggedValue(44)), true);
}
}  // namespace panda::test
