/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "runtime/include/runtime.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"

namespace panda::test {
class EcmaEmptyClassCheck : public testing::Test {
public:
    EcmaEmptyClassCheck()
    {
        auto exec_path = panda::os::file::File::GetExecutablePath();
        ecma_std_lib_ = exec_path.Value() + "/../plugins/ecmascript/ecmastdlib/ecmastdlib.abc";
    }

    const std::string &GetEcmaStdLibPath() const
    {
        return ecma_std_lib_;
    }

private:
    std::string ecma_std_lib_;
};

static void CheckAPINoCrash(Method *m)
{
    m->IsAbstract();
    m->IsConstructor();
    m->IsDefaultInterfaceMethod();
    m->IsFinal();
    m->IsInstanceConstructor();
    m->IsIntrinsic();
    m->IsNative();
    m->IsPrivate();
    m->IsProfiling();
    m->IsProfilingWithoutLock();
    m->IsProxy();
    m->IsProtected();
    m->IsPublic();
    m->IsStatic();
    m->IsStaticConstructor();
    m->IsSynchronized();
    m->IsSynthetic();
    m->IsVerified();
    m->GetClass();
    m->GetCompilationStatus();
    m->GetSingleImplementation();
    m->GetProfilingData();
    m->GetProto();
    m->GetProtoId();
    m->GetProfilingData();
    m->GetPandaFile();
    m->GetShorty();
    m->GetInstructions();
    m->GetCompiledEntryPoint();
    m->GetIntrinsic();
    m->GetCodeId();
    m->GetFileId();
    m->GetClassName();
    m->GetClassSourceFile();
    m->GetName();
    m->GetEffectiveReturnType();
    m->GetReturnType();
    m->GetFrameSize();
    m->GetHotnessCounter();
    m->GetVTableIndex();
    m->GetAccessFlags();
    m->GetCodeSize();
    m->GetNumArgs();
    m->GetNumVregs();
    m->GetUniqId();
    m->GetNativePointer();
    m->HasCompiledCode();
    m->HasSingleImplementation();
    m->GetCompilationStatus(0);
    m->GetFullName(false);
    m->GetFullName(true);
    m->GetLineNumFromBytecodeOffset(0);
    m->GetArgType(0);
    m->GetEffectiveArgType(0);
}

TEST_F(EcmaEmptyClassCheck, TestJSABC)
{
    RuntimeOptions options;
    options.SetLoadRuntimes({"ecmascript"});
    options.SetBootPandaFiles({});
    // NOLINTNEXTLINE(readability-magic-numbers)
    options.SetHeapSizeLimit(50_MB);
    options.SetGcType("epsilon");

    ASSERT_TRUE(Runtime::Create(options));

    auto *ecma_vm = reinterpret_cast<panda::ecmascript::EcmaVM *>(Runtime::GetCurrent()->GetPandaVM());

    for (Method *m : ecma_vm->GetNativeMethods()) {
        CheckAPINoCrash(m);
    }

    Runtime::Destroy();
}
}  // namespace panda::test
