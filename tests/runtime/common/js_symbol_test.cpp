/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/object_factory.h"

using DynClass = panda::coretypes::DynClass;
using JSSymbol = panda::ecmascript::JSSymbol;
using JSTaggedValue = panda::ecmascript::JSTaggedValue;
using LexicalEnv = panda::ecmascript::LexicalEnv;
using JSHClass = panda::ecmascript::JSHClass;
using ObjectFactory = panda::ecmascript::ObjectFactory;

template <typename T>
using JSHandle = panda::ecmascript::JSHandle<T>;

namespace panda::test {
class JSSymbolTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
    JSThread *thread_ {};
};

}  // namespace panda::test
