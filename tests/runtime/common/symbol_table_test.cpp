/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/symbol_table-inl.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/js_symbol.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class SymbolTableTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    ecmascript::EcmaHandleScope *scope_ {nullptr};
    PandaVM *instance_ {nullptr};
};

TEST_F(SymbolTableTest, GetKeyIndex)
{
    int entry = 0;
    EXPECT_EQ(SymbolTable::GetKeyIndex(entry), 3);
}

TEST_F(SymbolTableTest, GetValueIndex)
{
    int entry = 0;
    EXPECT_EQ(SymbolTable::GetValueIndex(entry), 4);
}

TEST_F(SymbolTableTest, GetEntryIndex)
{
    int entry = 0;
    EXPECT_EQ(SymbolTable::GetEntryIndex(entry), 3);
}

TEST_F(SymbolTableTest, GetEntrySize)
{
    EXPECT_EQ(SymbolTable::GetEntrySize(), 2);
}

/*
 * Feature: SymbolTableTest
 * Function: IsMatch
 * SubFunction: StringsAreEqual
 * FunctionPoints: Is Match
 * CaseDescription: Judge whether two string variables are equal. If they are equal,
 *                  it returns true, otherwise it returns false.
 */
TEST_F(SymbolTableTest, IsMatch)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<EcmaString> symbol_table_string = factory->NewFromCanBeCompressString("name");
    JSTaggedValue symbol_table_other = symbol_table_string.GetTaggedValue();

    JSTaggedValue symbol_table_name1 = JSTaggedValue::Hole();
    EXPECT_EQ(SymbolTable::IsMatch(symbol_table_name1, symbol_table_other), false);

    JSTaggedValue symbol_table_name2 = JSTaggedValue::Undefined();
    EXPECT_EQ(SymbolTable::IsMatch(symbol_table_name2, symbol_table_other), false);

    JSTaggedValue symbol_table_name3 = symbol_table_string.GetTaggedValue();
    EXPECT_EQ(SymbolTable::IsMatch(symbol_table_name3, symbol_table_other), true);
}

/*
 * Feature: SymbolTableTest
 * Function: Hash_Utf8
 * SubFunction: GetHashCode
 * FunctionPoints: Hash
 * CaseDescription: The hash code is obtained by passing in a character string array or an uint8_t
 *                  type array through a specific calculation formula.
 */
TEST_F(SymbolTableTest, Hash_Utf8)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // test obj is not string
    JSHandle<JSTaggedValue> js_ob_ject(factory->NewEmptyJSObject());
    EXPECT_EQ(SymbolTable::Hash(js_ob_ject.GetTaggedValue()), JSSymbol::ComputeHash());

    // the CompressedStringsEnabled must  be true
    bool flag = EcmaString::GetCompressedStringsEnabled();
    EXPECT_EQ(flag, true);

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    uint8_t utf8_array_name1[4] = {1, 2, 3};  // The last element is "\0"
    uint32_t utf8_array_name_len1 = sizeof(utf8_array_name1) - 1;
    JSHandle<EcmaString> name_string_utf8_obj1 = factory->NewFromUtf8(utf8_array_name1, utf8_array_name_len1);
    EXPECT_EQ(SymbolTable::Hash(name_string_utf8_obj1.GetTaggedValue()), 1026);  // 1026 = (1 << 5 - 1 + 2) << 5 - 2 + 3

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    uint8_t utf8_array_name2[] = "key";
    uint32_t utf8_array_name_len2 = sizeof(utf8_array_name2) - 1;
    JSHandle<EcmaString> name_string_utf8_obj2 = factory->NewFromUtf8(utf8_array_name2, utf8_array_name_len2);
    EXPECT_EQ(SymbolTable::Hash(name_string_utf8_obj2.GetTaggedValue()), 106079);
}

/*
 * Feature: SymbolTableTest
 * Function: Hash_Utf16
 * SubFunction: GetHashCode
 * FunctionPoints: Hash
 * CaseDescription: The hash code is obtained by passing in a character string array or an uint16_t
 *                  type array through a specific calculation formula.
 */
TEST_F(SymbolTableTest, Hash_Utf16)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    uint16_t utf16_array_name1[] = {1, 2, 3};
    uint32_t utf16_array_name_len1 = sizeof(utf16_array_name1) / sizeof(utf16_array_name1[0]);
    JSHandle<EcmaString> name_string_utf16_obj1 = factory->NewFromUtf16(utf16_array_name1, utf16_array_name_len1);
    EXPECT_EQ(SymbolTable::Hash(name_string_utf16_obj1.GetTaggedValue()),
              1026);  // 1026 = (1 << 5 - 1 + 2) << 5 - 2 + 3

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    uint16_t utf16_array_name2[] = {0, 1, 2};
    uint32_t utf16_array_name_len2 = sizeof(utf16_array_name2) / sizeof(utf16_array_name2[0]);
    JSHandle<EcmaString> name_string_utf16_obj2 = factory->NewFromUtf16(utf16_array_name2, utf16_array_name_len2);
    EXPECT_EQ(SymbolTable::Hash(name_string_utf16_obj2.GetTaggedValue()), 33);  // 33 = (0 << 5 - 0 + 1) << 5 - 1 + 2
}

/*
 * Feature: SymbolTableTest
 * Function: Create
 * SubFunction: *Value
 * FunctionPoints: Create
 * CaseDescription: A pointer variable of symboltable type is obtained by changing the function,
 *                  If it is created successfully, the pointer variable is not equal to null,
 *                  The prerequisite for creation is that compressedstringsenabled must be true.
 */
TEST_F(SymbolTableTest, Create)
{
    int number_of_elements = SymbolTable::DEFAULT_ELEMENTS_NUMBER;
    // the CompressedStringsEnabled must  be true
    bool flag = EcmaString::GetCompressedStringsEnabled();
    EXPECT_EQ(flag, true);

    JSHandle<SymbolTable> symbol_table = SymbolTable::Create(thread_, number_of_elements);
    EXPECT_TRUE(*symbol_table != nullptr);
}

/*
 * Feature: SymbolTableTest
 * Function: ContainsKey
 * SubFunction: Getkey
 * FunctionPoints: Contains Key
 * CaseDescription: Judge whether the key value can be found in the key value in your own created symbol
 *                  table.If you do not have a key value, you will return false.
 */
TEST_F(SymbolTableTest, ContainsKey)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> symbol_table_string_key1 = factory->NewFromCanBeCompressString("key");
    JSHandle<EcmaString> symbol_table_string_key2 = factory->NewFromCanBeCompressString("key1");
    JSHandle<EcmaString> symbol_table_string_key3 = factory->NewFromCanBeCompressString("value");

    int number_of_elements = 2;
    JSHandle<SymbolTable> symbol_table = SymbolTable::Create(thread_, number_of_elements);
    EXPECT_EQ(symbol_table->ContainsKey(thread_, symbol_table_string_key1.GetTaggedValue()), false);

    symbol_table->SetKey(thread_, 1, JSTaggedValue::Hole());
    EXPECT_EQ(symbol_table->ContainsKey(thread_, symbol_table_string_key1.GetTaggedValue()), false);

    symbol_table->SetKey(thread_, 1, JSTaggedValue::Undefined());
    EXPECT_EQ(symbol_table->ContainsKey(thread_, symbol_table_string_key1.GetTaggedValue()), false);

    symbol_table->SetKey(thread_, 1, symbol_table_string_key1.GetTaggedValue());
    EXPECT_EQ(symbol_table->ContainsKey(thread_, symbol_table_string_key1.GetTaggedValue()), true);

    // the key value has numbers
    symbol_table->SetKey(thread_, 1, symbol_table_string_key2.GetTaggedValue());
    EXPECT_EQ(symbol_table->ContainsKey(thread_, symbol_table_string_key2.GetTaggedValue()), false);

    symbol_table->SetKey(thread_, 1, symbol_table_string_key3.GetTaggedValue());
    EXPECT_EQ(symbol_table->ContainsKey(thread_, symbol_table_string_key3.GetTaggedValue()), true);
}

/*
 * Feature: SymbolTableTest
 * Function: GetSymbol
 * SubFunction: GetValue
 * FunctionPoints: Get Symbol
 * CaseDescription: This function obtains the value in the key of symbol table pointer variable created
 *                  by the create function. If the pointer variable has no value set, it returns false.
 */
TEST_F(SymbolTableTest, GetSymbol)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    int number_of_elements = 2;

    JSHandle<EcmaString> symbol_table_string_key = factory->NewFromCanBeCompressString("key");
    JSHandle<SymbolTable> symbol_table = SymbolTable::Create(thread_, number_of_elements);

    symbol_table->SetKey(thread_, 1, symbol_table_string_key.GetTaggedValue());
    EXPECT_EQ(symbol_table->GetSymbol(symbol_table_string_key.GetTaggedValue()), JSTaggedValue::Undefined());

    symbol_table->SetValue(thread_, 0, JSTaggedValue(1));
    EXPECT_EQ(symbol_table->GetSymbol(symbol_table_string_key.GetTaggedValue()), JSTaggedValue::Undefined());

    symbol_table->SetValue(thread_, 1, JSTaggedValue(1));
    EXPECT_EQ(symbol_table->GetSymbol(symbol_table_string_key.GetTaggedValue()).GetInt(), 1);
}

/*
 * Feature: SymbolTableTest
 * Function: FindSymbol
 * SubFunction: GetKey
 * FunctionPoints: Find Symbol
 * CaseDescription: This function compares the key value in the symboltable pointer variable created by the create
 *                  function with the description value in the jssymbol type variable. If they are equal, it indicates
 *                  that the find symbol is successful, and the return value is the key value. Before creating the
 *                  symboltable pointer, perform the newfromcanbecompressstring operation to obtain the array length.
 */
TEST_F(SymbolTableTest, FindSymbol)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> symbol_table_string_key1(factory->NewFromCanBeCompressString("key"));
    JSHandle<JSTaggedValue> symbol_table_string_key2(factory->NewFromCanBeCompressString("key1"));

    int number_of_elements = 2;
    JSHandle<JSSymbol> handle_symbol = factory->NewJSSymbol();
    JSHandle<SymbolTable> symbol_table = SymbolTable::Create(thread_, number_of_elements);

    JSTaggedValue result_value1 = symbol_table->FindSymbol(thread_, handle_symbol.GetTaggedValue());
    EXPECT_EQ(JSTaggedValue::SameValue(result_value1, JSTaggedValue::Undefined()), true);

    handle_symbol->SetDescription(thread_, symbol_table_string_key1.GetTaggedValue());
    JSTaggedValue result_value2 = symbol_table->FindSymbol(thread_, handle_symbol.GetTaggedValue());
    EXPECT_EQ(JSTaggedValue::SameValue(result_value2, JSTaggedValue::Undefined()), true);

    symbol_table->SetKey(thread_, 1, symbol_table_string_key1.GetTaggedValue());
    JSTaggedValue result_value3 = symbol_table->FindSymbol(thread_, handle_symbol.GetTaggedValue());
    EXPECT_EQ(result_value3.GetRawData() == symbol_table_string_key1.GetTaggedValue().GetRawData(), true);

    symbol_table->SetKey(thread_, 1, symbol_table_string_key2.GetTaggedValue());
    JSTaggedValue result_value4 = symbol_table->FindSymbol(thread_, handle_symbol.GetTaggedValue());
    EXPECT_EQ(JSTaggedValue::SameValue(result_value4, JSTaggedValue::Undefined()), true);
}
}  // namespace panda::test
