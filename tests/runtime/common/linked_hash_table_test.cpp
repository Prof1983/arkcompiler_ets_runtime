/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/linked_hash_table.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_hash_table-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

// NOLINTBEGIN(readability-magic-numbers,modernize-avoid-c-arrays)

namespace panda::test {
class LinkedHashTableTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

    JSHandle<GlobalEnv> GetGlobalEnv()
    {
        EcmaVM *ecma = thread_->GetEcmaVM();
        return ecma->GetGlobalEnv();
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

TEST_F(LinkedHashTableTest, MapCreate)
{
    int num_of_element = 64;
    JSHandle<LinkedHashMap> dict = LinkedHashMap::Create(thread_, num_of_element);
    EXPECT_TRUE(*dict != nullptr);
}

TEST_F(LinkedHashTableTest, SetCreate)
{
    int num_of_element = 64;
    JSHandle<LinkedHashSet> set = LinkedHashSet::Create(thread_, num_of_element);
    EXPECT_TRUE(*set != nullptr);
}

TEST_F(LinkedHashTableTest, addKeyAndValue)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // mock object needed in test
    int num_of_element = 64;
    JSHandle<LinkedHashMap> dict_handle = LinkedHashMap::Create(thread_, num_of_element);
    EXPECT_TRUE(*dict_handle != nullptr);
    JSHandle<JSTaggedValue> obj_fun = GetGlobalEnv()->GetObjectFunction();

    char key_array[] = "hello";
    JSHandle<EcmaString> string_key1 = factory->NewFromCanBeCompressString(key_array);
    JSHandle<JSTaggedValue> key1(string_key1);
    JSHandle<JSTaggedValue> value1(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun));

    char key2_array[] = "hello2";
    JSHandle<EcmaString> string_key2 = factory->NewFromCanBeCompressString(key2_array);
    JSHandle<JSTaggedValue> key2(string_key2);
    JSHandle<JSTaggedValue> value2(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun));

    // test set()
    dict_handle = LinkedHashMap::Set(thread_, dict_handle, key1, value1);
    EXPECT_EQ(dict_handle->NumberOfElements(), 1);

    // test find()
    int hash = LinkedHash::Hash(key1.GetTaggedValue());
    int entry1 = dict_handle->FindElement(key1.GetTaggedValue(), hash);
    EXPECT_EQ(key1.GetTaggedValue(), dict_handle->GetKey(entry1));
    EXPECT_EQ(value1.GetTaggedValue(), dict_handle->GetValue(entry1));

    dict_handle = LinkedHashMap::Set(thread_, dict_handle, key2, value2);
    EXPECT_EQ(dict_handle->NumberOfElements(), 2);
    // test remove()
    dict_handle = LinkedHashMap::Delete(thread_, dict_handle, key1);
    EXPECT_EQ(-1, dict_handle->FindElement(key1.GetTaggedValue(), hash));
    EXPECT_EQ(dict_handle->NumberOfElements(), 1);

    JSHandle<JSTaggedValue> undefined_key(thread_, JSTaggedValue::Undefined());
    dict_handle = LinkedHashMap::Set(thread_, dict_handle, undefined_key, value1);
    int undefined_hash = LinkedHash::Hash(undefined_key.GetTaggedValue());
    int entry2 = dict_handle->FindElement(undefined_key.GetTaggedValue(), undefined_hash);
    EXPECT_EQ(value1.GetTaggedValue(), dict_handle->GetValue(entry2));
}

TEST_F(LinkedHashTableTest, SetaddKeyAndValue)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // mock object needed in test
    int num_of_element = 64;
    JSHandle<LinkedHashSet> set_handle = LinkedHashSet::Create(thread_, num_of_element);
    EXPECT_TRUE(*set_handle != nullptr);
    JSHandle<JSTaggedValue> obj_fun = GetGlobalEnv()->GetObjectFunction();

    char key_array[] = "hello";
    JSHandle<EcmaString> string_key1 = factory->NewFromCanBeCompressString(key_array);
    JSHandle<JSTaggedValue> key1(string_key1);
    JSHandle<JSTaggedValue> value1(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun));

    char key2_array[] = "hello2";
    JSHandle<EcmaString> string_key2 = factory->NewFromCanBeCompressString(key2_array);
    JSHandle<JSTaggedValue> key2(string_key2);
    JSHandle<JSTaggedValue> value2(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun));

    // test set()
    set_handle = LinkedHashSet::Add(thread_, set_handle, key1);
    EXPECT_EQ(set_handle->NumberOfElements(), 1);

    // test has()
    int hash = LinkedHash::Hash(key1.GetTaggedValue());
    EXPECT_TRUE(set_handle->Has(key1.GetTaggedValue(), hash));

    set_handle = LinkedHashSet::Add(thread_, set_handle, key2);
    EXPECT_EQ(set_handle->NumberOfElements(), 2);
    // test remove()
    set_handle = LinkedHashSet::Delete(thread_, set_handle, key1);
    EXPECT_EQ(-1, set_handle->FindElement(key1.GetTaggedValue(), hash));
    EXPECT_EQ(set_handle->NumberOfElements(), 1);

    JSHandle<JSTaggedValue> undefined_key(thread_, JSTaggedValue::Undefined());
    set_handle = LinkedHashSet::Add(thread_, set_handle, undefined_key);
    int undefined_hash = LinkedHash::Hash(undefined_key.GetTaggedValue());
    EXPECT_TRUE(set_handle->Has(undefined_key.GetTaggedValue(), undefined_hash));
}

TEST_F(LinkedHashTableTest, GrowCapacity)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    int num_of_element = 8;
    JSHandle<LinkedHashMap> dict_handle = LinkedHashMap::Create(thread_, num_of_element);
    EXPECT_TRUE(*dict_handle != nullptr);
    JSHandle<JSFunction> obj_fun(GetGlobalEnv()->GetObjectFunction());
    char key_array[7] = "hello";
    for (int i = 0; i < 33; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(key_array));
        JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(i));

        // test insert()
        dict_handle = LinkedHashMap::Set(thread_, dict_handle, key, value);
        int hash = LinkedHash::Hash(key.GetTaggedValue());
        EXPECT_EQ(i, dict_handle->FindElement(key.GetTaggedValue(), hash));
    }

    // test order
    for (int i = 0; i < 33; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<EcmaString> string_key = factory->NewFromCanBeCompressString(key_array);
        // test insert()
        int hash = LinkedHash::Hash(string_key.GetTaggedValue());
        EXPECT_EQ(i, dict_handle->FindElement(string_key.GetTaggedValue(), hash));
    }
    EXPECT_EQ(dict_handle->NumberOfElements(), 33);
    EXPECT_EQ(dict_handle->Capacity(), 64);
}

TEST_F(LinkedHashTableTest, SetGrowCapacity)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    int num_of_element = 8;
    JSHandle<LinkedHashSet> set_handle = LinkedHashSet::Create(thread_, num_of_element);
    EXPECT_TRUE(*set_handle != nullptr);
    JSHandle<JSFunction> obj_fun(GetGlobalEnv()->GetObjectFunction());
    // create key and values
    char key_array[7] = "hello";
    for (int i = 0; i < 33; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<EcmaString> string_key = factory->NewFromCanBeCompressString(key_array);
        JSHandle<JSTaggedValue> key(string_key);

        // test insert()
        set_handle = LinkedHashSet::Add(thread_, set_handle, key);
        int hash = LinkedHash::Hash(key.GetTaggedValue());
        EXPECT_EQ(i, set_handle->FindElement(key.GetTaggedValue(), hash));
    }

    // test order
    for (int i = 0; i < 33; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<EcmaString> string_key = factory->NewFromCanBeCompressString(key_array);
        // test insert()
        int hash = LinkedHash::Hash(string_key.GetTaggedValue());
        EXPECT_EQ(i, set_handle->FindElement(string_key.GetTaggedValue(), hash));
    }
    EXPECT_EQ(set_handle->NumberOfElements(), 33);
    EXPECT_EQ(set_handle->Capacity(), 64);
}

TEST_F(LinkedHashTableTest, ShrinkCapacity)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    int num_of_element = 64;
    JSHandle<LinkedHashMap> dict_handle = LinkedHashMap::Create(thread_, num_of_element);
    EXPECT_TRUE(*dict_handle != nullptr);
    JSHandle<JSFunction> obj_fun(GetGlobalEnv()->GetObjectFunction());
    char key_array[7] = "hello";
    for (int i = 0; i < 10; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(key_array));
        JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(i));

        // test insert()
        dict_handle = LinkedHashMap::Set(thread_, dict_handle, key, value);
    }
    key_array[5] = '1' + 9;
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(key_array));
    dict_handle = LinkedHashMap::Delete(thread_, dict_handle, key);
    // test order
    for (int i = 0; i < 9; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<EcmaString> string_key = factory->NewFromCanBeCompressString(key_array);
        // test insert()
        int hash = LinkedHash::Hash(string_key.GetTaggedValue());
        EXPECT_EQ(i, dict_handle->FindElement(string_key.GetTaggedValue(), hash));
    }
    EXPECT_EQ(dict_handle->NumberOfElements(), 9);
    EXPECT_EQ(dict_handle->Capacity(), 16);
}

TEST_F(LinkedHashTableTest, SetShrinkCapacity)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    int num_of_element = 64;
    JSHandle<LinkedHashSet> set_handle = LinkedHashSet::Create(thread_, num_of_element);
    EXPECT_TRUE(*set_handle != nullptr);
    JSHandle<JSFunction> obj_fun(GetGlobalEnv()->GetObjectFunction());
    // create key and values
    char key_array[7] = "hello";
    for (int i = 0; i < 10; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(key_array));

        // test insert()
        set_handle = LinkedHashSet::Add(thread_, set_handle, key);
    }
    key_array[5] = '1' + 9;
    JSHandle<JSTaggedValue> key_handle(factory->NewFromCanBeCompressString(key_array));
    set_handle = LinkedHashSet::Delete(thread_, set_handle, key_handle);
    // test order
    for (int i = 0; i < 9; i++) {
        key_array[5] = '1' + i;
        key_array[6] = 0;
        JSHandle<EcmaString> string_key = factory->NewFromCanBeCompressString(key_array);
        // test insert()
        int hash = LinkedHash::Hash(string_key.GetTaggedValue());
        EXPECT_EQ(i, set_handle->FindElement(string_key.GetTaggedValue(), hash));
    }
    EXPECT_EQ(set_handle->NumberOfElements(), 9);
    EXPECT_EQ(set_handle->Capacity(), 16);
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers,modernize-avoid-c-arrays)
