/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"
#include "include/coretypes/dyn_objects.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "include/coretypes/tagged_value.h"
#include "include/runtime.h"
#include "include/runtime_options.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSIteratorTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

TEST_F(JSIteratorTest, GetIterator)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> data(factory->NewTaggedArray(2));
    data->Set(thread_, 0, JSTaggedValue(1));
    data->Set(thread_, 1, JSTaggedValue(1));
    JSHandle<JSTaggedValue> array(JSArray::CreateArrayFromList(thread_, data));
    EXPECT_TRUE(array->IsArray(thread_));
    JSHandle<JSArrayIterator> iter(JSIterator::GetIterator(thread_, array));
    EXPECT_TRUE(iter->IsJSArrayIterator());
    EXPECT_TRUE(iter->GetIteratedArray().IsArray(thread_));
}

TEST_F(JSIteratorTest, IteratorNext)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> value_str = thread_->GlobalConstants()->GetHandledValueString();

    JSHandle<TaggedArray> data(factory->NewTaggedArray(1));
    data->Set(thread_, 0, JSTaggedValue(1));
    JSHandle<JSTaggedValue> array(JSArray::CreateArrayFromList(thread_, data));
    JSHandle<JSTaggedValue> iter(JSIterator::GetIterator(thread_, array));
    JSHandle<JSTaggedValue> result(JSIterator::IteratorNextOld(thread_, iter));
    JSHandle<JSTaggedValue> result_value(JSObject::GetProperty(thread_, result, value_str).GetValue());
    EXPECT_EQ(result_value->GetInt(), 1);
}

TEST_F(JSIteratorTest, IteratorComplete)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> data(factory->NewTaggedArray(2));
    data->Set(thread_, 0, JSTaggedValue(1));
    data->Set(thread_, 1, JSTaggedValue(1));
    JSHandle<JSTaggedValue> array(JSArray::CreateArrayFromList(thread_, data));
    JSHandle<JSTaggedValue> iter(JSIterator::GetIterator(thread_, array));
    JSHandle<JSTaggedValue> result1(JSIterator::IteratorNextOld(thread_, iter));
    EXPECT_EQ(false, JSIterator::IteratorComplete(thread_, result1));
    JSHandle<JSTaggedValue> result2(JSIterator::IteratorNextOld(thread_, iter));
    EXPECT_EQ(false, JSIterator::IteratorComplete(thread_, result2));
    JSHandle<JSTaggedValue> result3(JSIterator::IteratorNextOld(thread_, iter));
    EXPECT_EQ(true, JSIterator::IteratorComplete(thread_, result3));
}

TEST_F(JSIteratorTest, IteratorValue)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<TaggedArray> data(factory->NewTaggedArray(3));
    data->Set(thread_, 0, JSTaggedValue(1));
    data->Set(thread_, 1, JSTaggedValue(1));
    data->Set(thread_, 2, JSTaggedValue(1));
    JSHandle<JSTaggedValue> array(JSArray::CreateArrayFromList(thread_, data));
    JSHandle<JSTaggedValue> iter(JSIterator::GetIterator(thread_, array));
    JSHandle<JSTaggedValue> result(JSIterator::IteratorNextOld(thread_, iter));
    JSHandle<JSTaggedValue> result_value(JSIterator::IteratorValue(thread_, result));
    EXPECT_EQ(result_value->GetInt(), 1);
}

TEST_F(JSIteratorTest, IteratorStep)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> data(factory->NewTaggedArray(2));
    data->Set(thread_, 0, JSTaggedValue(1));
    data->Set(thread_, 1, JSTaggedValue(2));
    JSHandle<JSTaggedValue> array(JSArray::CreateArrayFromList(thread_, data));
    JSHandle<JSTaggedValue> iter(JSIterator::GetIterator(thread_, array));
    JSHandle<JSTaggedValue> result1(JSIterator::IteratorStep(thread_, iter));
    EXPECT_EQ(JSIterator::IteratorValue(thread_, result1)->GetInt(), 1);
    JSHandle<JSTaggedValue> result2(JSIterator::IteratorStep(thread_, iter));
    EXPECT_EQ(JSIterator::IteratorValue(thread_, result2)->GetInt(), 2);
    JSHandle<JSTaggedValue> result3(JSIterator::IteratorStep(thread_, iter));
    EXPECT_EQ(result3.GetTaggedValue(), JSTaggedValue::False());
}

}  // namespace panda::test
