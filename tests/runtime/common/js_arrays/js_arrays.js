let a = Array(8);
print(a.slice());
print(a.join());

let b = [];
b.length = 1025;
print(b.length);

let c = {"__proto__": [], "length": "InvalidLength"};
print([...c]);
print(c.join());
print(c.length);
c.push(322);
print(c.join());
print(c.length);

let d = new Float64Array(9);
try {
    d.find(Float64Array);
} catch (e) {
    print(e);
}
