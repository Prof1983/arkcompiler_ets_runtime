#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.

set -eo pipefail

expected=",,,,,,,
,,,,,,,
1025


InvalidLength
322
1
TypeError: The NewTarget is undefined."

actual=$(cat "$1")
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mrestargstest failed\033[0m"
    exit 1;
fi
