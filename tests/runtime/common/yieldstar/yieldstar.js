function *foo1() {
    yield 1
    yield 2
}
  
function *foo2() {
    yield *foo1()
}

var p = foo2()
var a = p.next()
print(a.value, a.done)
var b = p.next()
print(b.value, b.done)
var c = p.next()
print(c.value, c.done)