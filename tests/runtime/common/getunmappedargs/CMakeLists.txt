# Huawei Technologies Co.,Ltd.

set(GETUNMAPPEDARGS_OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/getunmappedargs.txt)
set(GETUNMAPPEDARGS_BIN ${CMAKE_CURRENT_BINARY_DIR}/getunmappedargs.abc)
set(GETUNMAPPEDARGS_PA ${CMAKE_CURRENT_BINARY_DIR}/getunmappedargs.pa)
set(GETUNMAPPEDARGS_JS ${CMAKE_CURRENT_SOURCE_DIR}/getunmappedargs.js)
set(GETUNMAPPEDARGS_VERIFY ${CMAKE_CURRENT_SOURCE_DIR}/verify.sh)

set(RUNTIME_ARGUMENTS --boot-panda-files=${PANDA_BINARY_ROOT}/pandastdlib/arkstdlib.abc --load-runtimes=\"ecmascript\" --compiler-enable-jit=false ${GETUNMAPPEDARGS_BIN} _GLOBAL::func_main_0)

add_custom_command(
    OUTPUT ${GETUNMAPPEDARGS_OUTPUT}
    COMMENT "running javascript getunmappedargs testcase"
    COMMAND ${PANDA_RUN_PREFIX} $<TARGET_FILE:es2panda> ${GETUNMAPPEDARGS_JS} --dump-assembly --output ${GETUNMAPPEDARGS_BIN} > ${GETUNMAPPEDARGS_PA}
    COMMAND rm -f ${GETUNMAPPEDARGS_OUTPUT}
    COMMAND ${PANDA_RUN_PREFIX} $<TARGET_FILE:ark> ${RUNTIME_ARGUMENTS} > ${GETUNMAPPEDARGS_OUTPUT}
    COMMAND bash ${GETUNMAPPEDARGS_VERIFY} ${GETUNMAPPEDARGS_OUTPUT}
)
add_custom_target(getunmappedargs
    DEPENDS ${GETUNMAPPEDARGS_OUTPUT} ${GETUNMAPPEDARGS_VERIFY}
)
add_dependencies(getunmappedargs es2panda ark)
add_dependencies(ecmascript_common_tests getunmappedargs)
