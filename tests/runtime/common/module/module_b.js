export { Car };
export  * from  'module_c.abc';

var Car = {
    carInfo: function() {
        return this.name + ":" + this.type + ":" + this.price;
    }
}

var myCar = {
    name: "HwCar",
    type: "HW",
    price: "CNY:XXW"
}

var info = Car.carInfo.apply(myCar);