import { Car } from 'module_b.abc';   // Test direct Export & use after import
import { Star, myClass } from 'module_c.abc';  // Test indirect Export & use after import

let A = Car;

var myCar = {
    name: "HWCar_Test",
    type: "HW_Test",
    price: "CNY:XXW_Test"
}

var infoA = A.carInfo.apply(myCar);

let C = Star;

var myStar = {
    name: "Polaris_Test",
    type: "fixedStar_Test",
    color: "Yellow_Test"
}

var infoC = Star.starColor.apply(myStar);

if (infoA != "HWCar_Test:HW_Test:CNY:XXW_Test" ) {
    print("Direct Export Fail");
} else if (infoC != "Polaris_Test:fixedStar_Test:Yellow_Test") {
    print("Indirect Export Fail");
} else {
    print("Pass!!");
}

let myClassObj = new myClass(42);
if (myClassObj.x !== 42 || myClassObj.foo() !== 42) {
    throw "bad myClass";
}