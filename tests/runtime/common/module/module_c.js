var Star = {
    starColor: function () {
        return this.name + ":" + this.type + ":" + this.color;
    }
}

class myClass {
    constructor(val) {
        this.x = val;
    }
    foo() {
        return this.x;
    }
};

export { Star, myClass };

var myStar = {
    name: "Polaris",
    type: "FixedStar",
    color: "Yellow"
}

var info = Star.starColor.apply(myStar);