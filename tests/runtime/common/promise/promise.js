var p = new Promise((resolve, reject) => {
    resolve(1479);
})
var p1 = Promise.reject(1357);
var p2 = Promise.resolve(2468);
var p3 = Promise.race([p1, p2]);
p3.then(
    (value) => {
        print("resolve");
        print(value);
    },
    (value) => {
        print("reject");
        print(value);
    }
)

p3.catch((value) => {
    print("catch");
    print(value);
})

var p4 = Promise.all([p, p2]);
p4.then(
    (value) => {
        print("resolve");
        print(value);
    },
    (value) => {
        print("reject");
        print(value);
    }
)
