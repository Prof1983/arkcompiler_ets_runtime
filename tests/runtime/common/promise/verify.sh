#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.

set -eo pipefail

expected="reject
1357
catch
1357
resolve
1479,2468"
actual=$(cat "$1")
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mpromise test failed\033[0m"
    exit 1;
fi
