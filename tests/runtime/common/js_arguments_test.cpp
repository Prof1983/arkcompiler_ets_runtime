/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_arguments.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

using panda::ecmascript::EcmaString;
using panda::ecmascript::GlobalEnv;
using panda::ecmascript::JSArguments;
using panda::ecmascript::JSFunction;
using panda::ecmascript::JSHandle;
using panda::ecmascript::JSObject;
using panda::ecmascript::PropertyDescriptor;

namespace panda::test {
class JsArgumentsTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    ecmascript::EcmaHandleScope *scope_ {nullptr};
    PandaVM *instance_ {nullptr};
};

static JSFunction *JSObjectTestCreate(JSThread *thread)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    return global_env->GetObjectFunction().GetObject<JSFunction>();
}

TEST_F(JsArgumentsTest, SetProperty)
{
    JSHandle<JSTaggedValue> arg_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> jsarg =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(arg_func), arg_func);
    JSHandle<JSArguments> arg = thread_->GetEcmaVM()->GetFactory()->NewJSArguments();

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    // receive must be jsarg's conversion
    JSHandle<JSTaggedValue> receiver = JSHandle<JSTaggedValue>::Cast(jsarg);
    EXPECT_TRUE(JSArguments::SetProperty(thread_, arg, key, value, receiver));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsarg), key).GetValue()->GetInt(), 1);
    EXPECT_EQ(JSArguments::GetProperty(thread_, jsarg, key).GetValue()->GetInt(), 1);

    JSHandle<JSTaggedValue> value2(thread_, JSTaggedValue(2));
    EXPECT_TRUE(JSArguments::SetProperty(thread_, arg, key, value2, receiver));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsarg), key).GetValue()->GetInt(), 2);
    EXPECT_EQ(JSArguments::GetProperty(thread_, jsarg, key).GetValue()->GetInt(), 2);
}

TEST_F(JsArgumentsTest, GetProperty)
{
    JSHandle<JSTaggedValue> arg_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> jsarg =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(arg_func), arg_func);
    JSHandle<JSArguments> arg = thread_->GetEcmaVM()->GetFactory()->NewJSArguments();

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    JSHandle<JSTaggedValue> receiver = JSHandle<JSTaggedValue>::Cast(jsarg);
    JSArguments::SetProperty(thread_, arg, key, value, receiver);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsarg), key).GetValue()->GetInt(), 1);
    EXPECT_EQ(JSArguments::GetProperty(thread_, JSHandle<JSArguments>(jsarg), key, receiver).GetValue()->GetInt(), 1);

    JSHandle<JSTaggedValue> value2(thread_, JSTaggedValue(2));
    JSArguments::SetProperty(thread_, arg, key, value2, receiver);
    EXPECT_EQ(JSArguments::GetProperty(thread_, jsarg, key).GetValue()->GetInt(), 2);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsarg), key).GetValue()->GetInt(), 2);
}

TEST_F(JsArgumentsTest, DeleteProperty)
{
    JSHandle<JSTaggedValue> arg_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> jsarg =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(arg_func), arg_func);
    JSHandle<JSArguments> arg = thread_->GetEcmaVM()->GetFactory()->NewJSArguments();

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    char array[] = "delete";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> receiver = JSHandle<JSTaggedValue>::Cast(jsarg);
    JSArguments::SetProperty(thread_, arg, key, value, receiver);
    EXPECT_EQ(JSArguments::GetProperty(thread_, jsarg, key).GetValue()->GetInt(), 1);

    // test delete
    bool result = JSArguments::DeleteProperty(thread_, JSHandle<JSArguments>(jsarg), key);
    EXPECT_TRUE(result);
    EXPECT_TRUE(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsarg), key).GetValue()->IsUndefined());
}

TEST_F(JsArgumentsTest, DefineOwnProperty)
{
    JSHandle<JSTaggedValue> arg_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> jsarg =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(arg_func), arg_func);
    JSHandle<JSArguments> arg = thread_->GetEcmaVM()->GetFactory()->NewJSArguments();

    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("x"));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> value2(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> receiver = JSHandle<JSTaggedValue>::Cast(jsarg);
    JSArguments::SetProperty(thread_, arg, key, value2, receiver);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsarg), key).GetValue()->GetInt(), 2);

    PropertyDescriptor desc(thread_);
    // set value1
    desc.SetValue(value1);
    desc.SetWritable(false);
    EXPECT_TRUE(JSArguments::DefineOwnProperty(thread_, JSHandle<JSArguments>(jsarg), key, desc));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsarg), key).GetValue()->GetInt(), 1);
}

TEST_F(JsArgumentsTest, GetOwnProperty)
{
    JSHandle<JSTaggedValue> arg_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> jsarg =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(arg_func), arg_func);
    JSHandle<JSArguments> arg = thread_->GetEcmaVM()->GetFactory()->NewJSArguments();

    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("x"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> receiver = JSHandle<JSTaggedValue>::Cast(jsarg);
    JSArguments::SetProperty(thread_, arg, key, value, receiver);

    PropertyDescriptor desc(thread_);
    JSHandle<EcmaString> caller = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("caller");
    // key is not caller
    EXPECT_FALSE(JSTaggedValue::SameValue(key.GetTaggedValue(), caller.GetTaggedValue()));
    EXPECT_TRUE(JSArguments::GetOwnProperty(thread_, JSHandle<JSArguments>(jsarg), key, desc));
    EXPECT_EQ(desc.GetValue()->GetInt(), 1);
}
}  // namespace panda::test
