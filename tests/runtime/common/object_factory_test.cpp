/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_string-inl.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/lexical_env.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class ObjectFactoryTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

JSHandle<GlobalEnv> GetGlobal(JSThread *thread)
{
    return thread->GetEcmaVM()->GetGlobalEnv();
}

TEST_F(ObjectFactoryTest, DISABLED_NewJSObjectByConstructor)  // TODO(vpukhov)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> obj_fun = GetGlobal(thread_)->GetObjectFunction();

    // check mem alloc
    JSHandle<JSObject> new_obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSHClass> new_obj_cls(thread_, new_obj->GetJSHClass());
    EXPECT_TRUE(*new_obj != nullptr);
    EXPECT_TRUE(*new_obj_cls != nullptr);

    // check feild
    EXPECT_EQ(new_obj->GetProperties(), GetGlobal(thread_)->GetEmptyArray().GetTaggedValue());
    EXPECT_EQ(new_obj->GetElements(), GetGlobal(thread_)->GetEmptyArray().GetTaggedValue());
    EXPECT_TRUE(JSTaggedValue(*new_obj).IsECMAObject());

    // check jshclass
    JSHClass *cls = *new_obj_cls;
    EXPECT_TRUE(cls->GetObjectSize() ==
                JSObject::SIZE + JSHClass::DEFAULT_CAPACITY_OF_IN_OBJECTS * JSTaggedValue::TaggedTypeSize());
    EXPECT_TRUE(cls->GetPrototype() == GetGlobal(thread_)->GetObjectFunctionPrototype().GetTaggedValue());
    EXPECT_TRUE(cls->GetObjectType() == JSType::JS_OBJECT);

    // check gc handle update
    auto *prototype = cls->GetPrototype().GetTaggedObject();
    thread_->GetEcmaVM()->CollectGarbage();
    // CompressGC not the same
    EXPECT_TRUE(prototype != new_obj_cls->GetPrototype().GetTaggedObject());
}

TEST_F(ObjectFactoryTest, NewJSFunction)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    // check mem alloc
    JSHandle<JSFunction> new_fun = factory->NewJSFunction(env);
    JSHandle<JSHClass> new_fun_cls(thread_, new_fun->GetJSHClass());
    EXPECT_TRUE(*new_fun != nullptr);
    EXPECT_TRUE(*new_fun_cls != nullptr);

    // check feild
    EXPECT_EQ(new_fun->GetProperties(), GetGlobal(thread_)->GetEmptyArray().GetTaggedValue());
    EXPECT_EQ(new_fun->GetElements(), GetGlobal(thread_)->GetEmptyArray().GetTaggedValue());
    EXPECT_EQ(new_fun->GetProtoOrDynClass(), JSTaggedValue::Hole());
    EXPECT_EQ(new_fun->GetHomeObject(), JSTaggedValue::Undefined());
    EXPECT_TRUE(JSTaggedValue(*new_fun).IsJSFunction());

    // check jshclass
    JSHClass *cls = *new_fun_cls;
    EXPECT_TRUE(cls->GetObjectSize() ==
                JSFunction::SIZE + JSHClass::DEFAULT_CAPACITY_OF_IN_OBJECTS * JSTaggedValue::TaggedTypeSize());
    EXPECT_TRUE(cls->GetPrototype() == GetGlobal(thread_)->GetFunctionPrototype().GetTaggedValue());
    EXPECT_TRUE(cls->GetObjectType() == JSType::JS_FUNCTION);
    EXPECT_TRUE(cls->IsCallable());
    EXPECT_TRUE(cls->IsExtensible());
    EXPECT_TRUE(!cls->IsConstructor());
}

TEST_F(ObjectFactoryTest, NewJSBoundFunction)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // test prepare
    JSHandle<JSFunction> fun_fun(GetGlobal(thread_)->GetObjectFunction());
    JSHandle<JSTaggedValue> bound(thread_, GetGlobal(thread_)->GetObjectFunctionPrototype().GetTaggedValue());
    const JSHandle<TaggedArray> array(GetGlobal(thread_)->GetEmptyArray());

    // check mem alloc
    JSHandle<JSFunctionBase> target_func(fun_fun);
    JSHandle<JSBoundFunction> new_bound_fun = factory->NewJSBoundFunction(target_func, bound, array);
    JSHandle<JSHClass> new_bound_fun_cls(thread_, new_bound_fun->GetJSHClass());
    EXPECT_TRUE(*new_bound_fun != nullptr);
    EXPECT_TRUE(*new_bound_fun_cls != nullptr);
}

TEST_F(ObjectFactoryTest, NewJSPrimitiveRef)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // test prepare
    JSHandle<JSFunction> number_fun(GetGlobal(thread_)->GetNumberFunction());
    JSHandle<JSTaggedValue> primitive(thread_, JSTaggedValue(1));

    // check mem alloc
    JSHandle<JSPrimitiveRef> new_primitive = factory->NewJSPrimitiveRef(number_fun, primitive);
    JSHandle<JSHClass> new_primitive_cls(thread_, new_primitive->GetJSHClass());
    EXPECT_TRUE(*new_primitive != nullptr);
    EXPECT_TRUE(*new_primitive_cls != nullptr);

    EXPECT_TRUE(new_primitive->GetValue() == JSTaggedValue(1));
}

TEST_F(ObjectFactoryTest, NewLexicalEnv)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // check mem alloc
    JSHandle<LexicalEnv> new_lexical_env = factory->NewLexicalEnv(0);
    JSHandle<JSHClass> new_lexical_env_cls(thread_, new_lexical_env->GetClass());
    EXPECT_TRUE(*new_lexical_env != nullptr);
    EXPECT_TRUE(*new_lexical_env_cls != nullptr);
}

TEST_F(ObjectFactoryTest, NewJSArray)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // check mem alloc
    JSHandle<JSArray> new_js_aarray = factory->NewJSArray();
    JSHandle<JSHClass> new_js_array_cls(thread_, new_js_aarray->GetJSHClass());
    EXPECT_TRUE(*new_js_aarray != nullptr);
    EXPECT_TRUE(*new_js_array_cls != nullptr);
}
}  // namespace panda::test
