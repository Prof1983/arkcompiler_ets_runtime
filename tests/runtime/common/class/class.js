class Parent {
    constructor(x) {
        this.x = x;
    }

    static toString() {
        return 'parent';
    }
}

class Child extends Parent {
    constructor(x, y) {
        super(x);
        this.y = y;
    }

    value() {
        return this.x * this.y;
    }

    static toString() {
        return super.toString() + ' child';
    }
}

var c = new Child(2, 3);
print(c.value());
print(Child.toString());