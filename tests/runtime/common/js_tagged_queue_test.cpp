/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"

#include "plugins/ecmascript/runtime/tagged_queue.h"
#include "plugins/ecmascript/runtime/tagged_queue-inl.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/global_env.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSTaggedQueueTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

TEST_F(JSTaggedQueueTest, Create)
{
    JSHandle<TaggedQueue> queue = thread_->GetEcmaVM()->GetFactory()->NewTaggedQueue(0);
    EXPECT_TRUE(*queue != nullptr);
    EXPECT_TRUE(queue->Empty());
    EXPECT_TRUE(queue->Size() == 0);
    EXPECT_TRUE(queue->Front() == JSTaggedValue::Hole());
    EXPECT_TRUE(queue->Back() == JSTaggedValue::Hole());
}

TEST_F(JSTaggedQueueTest, PopAndPush)
{
    JSHandle<TaggedQueue> queue = thread_->GetEcmaVM()->GetFactory()->NewTaggedQueue(0);
    EXPECT_TRUE(queue->Empty());

    JSHandle<TaggedQueue> queue2(thread_,
                                 TaggedQueue::Push(thread_, queue, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))));
    EXPECT_FALSE(queue2->Empty());
    EXPECT_EQ(queue2->Size(), 1);
    EXPECT_EQ(queue2->Front(), JSTaggedValue(0));
    EXPECT_EQ(queue2->Back(), JSTaggedValue(0));

    JSHandle<TaggedQueue> queue3(
        thread_, TaggedQueue::Push(thread_, queue2, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1))));
    EXPECT_EQ(queue3->Size(), 2);
    EXPECT_EQ(queue3->Front(), JSTaggedValue(0));
    EXPECT_EQ(queue3->Back(), JSTaggedValue(1));
    EXPECT_NE(queue3.GetTaggedValue(), queue2.GetTaggedValue());

    JSHandle<TaggedQueue> queue4(
        thread_, TaggedQueue::Push(thread_, queue3, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2))));
    EXPECT_EQ(queue4->Size(), 3);
    EXPECT_EQ(queue4->Front(), JSTaggedValue(0));
    EXPECT_EQ(queue4->Back(), JSTaggedValue(2));
    EXPECT_NE(queue4.GetTaggedValue(), queue3.GetTaggedValue());

    JSHandle<TaggedQueue> queue5(
        thread_, TaggedQueue::Push(thread_, queue4, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3))));
    EXPECT_EQ(queue5->Size(), 4);
    EXPECT_EQ(queue5->Front(), JSTaggedValue(0));
    EXPECT_EQ(queue5->Back(), JSTaggedValue(3));
    EXPECT_NE(queue5.GetTaggedValue(), queue4.GetTaggedValue());

    EXPECT_EQ(queue5->Pop(thread_), JSTaggedValue(0));
    EXPECT_EQ(queue5->Size(), 3);
    EXPECT_EQ(queue5->Front(), JSTaggedValue(1));

    EXPECT_EQ(queue5->Pop(thread_), JSTaggedValue(1));
    EXPECT_EQ(queue5->Size(), 2);
    EXPECT_EQ(queue5->Front(), JSTaggedValue(2));

    EXPECT_EQ(queue5->Pop(thread_), JSTaggedValue(2));
    EXPECT_EQ(queue5->Size(), 1);
    EXPECT_EQ(queue5->Front(), JSTaggedValue(3));

    EXPECT_EQ(queue5->Pop(thread_), JSTaggedValue(3));
    EXPECT_EQ(queue5->Size(), 0);
    EXPECT_EQ(queue5->Front(), JSTaggedValue::Hole());
    EXPECT_TRUE(queue5->Empty());
}

}  // namespace panda::test
