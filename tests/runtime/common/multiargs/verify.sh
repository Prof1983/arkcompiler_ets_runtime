#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.

set -eo pipefail

expected="   0  000 000000000000
123456789
hello, world
aaabbbccc
111222333666
54321"
actual=$(cat "$1")
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mmultiargs test failed\033[0m"
    exit 1;
fi
