function zero()
{
    var a = '   0'
    var b = '  000'
    var c = ' 00000'
    var d = '0000000'
    print(a+b+c+d)
}

function one(x)
{
    print(x)
}

function two(x,y)
{
    print(x+y)
}

function three(x,y,z)
{
    print(x+y+z)
}

function four(x,y,z,t)
{
    print(x+y+z+t)
}

function five(x,y,z,t,a)
{
    let s = x + 10*y+ 100*z + 1000*t + 10000*a
    print(s.toString(10))
}

zero()
one(123456789)
two('hello,',' world')
three('aaa','bbb','ccc')

let x = 111
let y = 222
let z = 333
let a = 666

four(x.toString(10),y.toString(10),z.toString(10),a.toString(10))
five(1,2,3,4,5)