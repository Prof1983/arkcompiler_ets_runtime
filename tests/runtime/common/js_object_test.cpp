#include "gtest/gtest.h"
#include "mem/ecma_string.h"
#include "test_helper.h"

#include "plugins/ecmascript/runtime/ecma_string.h"
#include "include/coretypes/array.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_operator.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/runtime/weak_vector-inl.h"
#include "plugins/ecmascript/runtime/ic/proto_change_details.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

// NOLINTBEGIN(modernize-avoid-c-arrays,readability-magic-numbers)

namespace panda::test {
class JSObjectTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

static JSFunction *JSObjectTestCreate(JSThread *thread)
{
    JSHandle<GlobalEnv> global_env = thread->GetEcmaVM()->GetGlobalEnv();
    return global_env->GetObjectFunction().GetObject<JSFunction>();
}

TEST_F(JSObjectTest, Create)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> jsobject =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*jsobject != nullptr);
}

TEST_F(JSObjectTest, SetProperty)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> jsobject =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*jsobject != nullptr);

    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(jsobject), key, value);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsobject), key).GetValue()->GetInt(), 1);

    JSHandle<JSTaggedValue> value2(thread_, JSTaggedValue(2));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(jsobject), key, value2);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(jsobject), key).GetValue()->GetInt(), 2);
}

TEST_F(JSObjectTest, GetProperty)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*obj != nullptr);

    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    EXPECT_TRUE(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->IsUndefined());

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key, value);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->GetInt(), 1);
}

TEST_F(JSObjectTest, DeleteProperty)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*obj != nullptr);

    char array[] = "print";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    JSObject::DeleteProperty(thread_, (obj), key);
    EXPECT_TRUE(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->IsUndefined());

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key, value);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->GetInt(), 1);

    JSHandle<JSTaggedValue> key2(thread_->GetEcmaVM()->GetFactory()->NewFromString("print_test"));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key2,
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(10)));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key2).GetValue()->GetInt(), 10);

    JSObject::DeleteProperty(thread_, (obj), key);
    EXPECT_TRUE(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->IsUndefined());
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key2).GetValue()->GetInt(), 10);
}

TEST_F(JSObjectTest, DeletePropertyGlobal)
{
    JSHandle<GlobalEnv> global_env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> global(thread_, global_env->GetGlobalObject());
    JSHandle<JSTaggedValue> print_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("print"));
    JSHandle<JSTaggedValue> print_test_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("print_test"));

    JSHandle<JSTaggedValue> value = JSObject::GetProperty(thread_, global, print_key).GetValue();

    JSObject::SetProperty(thread_, global, print_test_key, value);

    JSTaggedValue val2 = JSObject::GetProperty(thread_, global, print_test_key).GetValue().GetTaggedValue();
    EXPECT_EQ(val2, value.GetTaggedValue());
    JSTaggedValue::DeletePropertyOrThrow(thread_, global, print_test_key);
    JSTaggedValue val3 = JSObject::GetProperty(thread_, global, print_key).GetValue().GetTaggedValue();
    EXPECT_NE(val3, JSTaggedValue::Undefined());
}

TEST_F(JSObjectTest, GetPropertyInPrototypeChain)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> grandfather = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> father = JSObject::ObjectCreate(thread_, grandfather);
    JSHandle<JSObject> son = JSObject::ObjectCreate(thread_, father);

    JSHandle<JSTaggedValue> son_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key1"));
    JSHandle<JSTaggedValue> father_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key2"));
    JSHandle<JSTaggedValue> grandfather_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key3"));
    JSHandle<JSTaggedValue> son_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> father_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> grandfather_value(thread_, JSTaggedValue(3));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(son), son_key, son_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(father), father_key, father_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(grandfather), grandfather_key, grandfather_value);

    EXPECT_EQ(son_value.GetTaggedValue(),
              JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(son), son_key).GetValue().GetTaggedValue());
    EXPECT_EQ(father_value.GetTaggedValue(),
              JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(son), father_key).GetValue().GetTaggedValue());
    EXPECT_EQ(
        grandfather_value.GetTaggedValue(),
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(son), grandfather_key).GetValue().GetTaggedValue());
}

TEST_F(JSObjectTest, PropertyAttribute)
{
    JSHandle<JSTaggedValue> constructor(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor);
    JSHandle<JSObject> obj2 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor);

    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString("key3"));
    JSHandle<JSTaggedValue> key2(thread_->GetEcmaVM()->GetFactory()->NewFromString("key3"));

    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> value2(thread_, JSTaggedValue(2));

    // test set property
    PropertyDescriptor desc(thread_);
    desc.SetValue(value1);
    desc.SetWritable(false);
    JSObject::DefineOwnProperty(thread_, obj1, key1, desc);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1, value2);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key1, value1);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key1, value2);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1).GetValue().GetTaggedValue(),
              value1.GetTaggedValue());
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key1).GetValue().GetTaggedValue(),
              value2.GetTaggedValue());

    // test delete property
    PropertyDescriptor desc1(thread_);
    desc1.SetValue(value1);
    desc1.SetConfigurable(false);
    JSObject::DefineOwnProperty(thread_, obj1, key2, desc1);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key2, value1);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key2, value1);
    JSObject::DeleteProperty(thread_, (obj1), key2);
    JSObject::DeleteProperty(thread_, (obj2), key2);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key2).GetValue().GetTaggedValue(),
              value1.GetTaggedValue());
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key2).GetValue().GetTaggedValue(),
              JSTaggedValue::Undefined());
}

TEST_F(JSObjectTest, CreateDataProperty)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*obj != nullptr);

    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    bool success = JSObject::CreateDataProperty(thread_, obj, key, value);
    EXPECT_TRUE(success);

    success = JSTaggedValue::HasOwnProperty(thread_, JSHandle<JSTaggedValue>::Cast(obj), key);
    EXPECT_TRUE(success);

    PropertyDescriptor desc(thread_);
    success = JSObject::GetOwnProperty(thread_, obj, key, desc);
    EXPECT_TRUE(success);
    EXPECT_EQ(true, desc.IsWritable());
    EXPECT_EQ(true, desc.IsEnumerable());
    EXPECT_EQ(true, desc.IsConfigurable());
}

TEST_F(JSObjectTest, CreateMethodProperty)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*obj != nullptr);

    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    bool success = JSObject::CreateMethodProperty(thread_, obj, key, value);
    EXPECT_TRUE(success);

    success = JSTaggedValue::HasOwnProperty(thread_, JSHandle<JSTaggedValue>::Cast(obj), key);
    EXPECT_TRUE(success);

    PropertyDescriptor desc(thread_);
    success = JSObject::GetOwnProperty(thread_, obj, key, desc);
    EXPECT_TRUE(success);
    EXPECT_EQ(true, desc.IsWritable());
    EXPECT_EQ(false, desc.IsEnumerable());
    EXPECT_EQ(true, desc.IsConfigurable());
}

TEST_F(JSObjectTest, DefinePropertyOrThrow)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*obj != nullptr);

    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString(array));

    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    bool success = JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc1);
    EXPECT_TRUE(success);
    PropertyDescriptor desc_res1(thread_);
    success = JSObject::GetOwnProperty(thread_, obj, key, desc_res1);
    EXPECT_TRUE(success);
    EXPECT_EQ(1, desc_res1.GetValue()->GetInt());
    EXPECT_EQ(true, desc_res1.IsWritable());
    EXPECT_EQ(true, desc_res1.IsEnumerable());
    EXPECT_EQ(true, desc_res1.IsConfigurable());

    PropertyDescriptor desc2(thread_, false, true, true);
    success = JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc2);
    EXPECT_TRUE(success);
    PropertyDescriptor desc_res2(thread_);
    success = JSObject::GetOwnProperty(thread_, obj, key, desc_res2);
    EXPECT_TRUE(success);
    EXPECT_EQ(1, desc_res2.GetValue()->GetInt());
    EXPECT_EQ(false, desc_res2.IsWritable());
    EXPECT_EQ(true, desc_res2.IsEnumerable());
    EXPECT_EQ(true, desc_res2.IsConfigurable());

    PropertyDescriptor desc3(thread_);
    desc3.SetWritable(false);
    desc3.SetEnumerable(false);
    desc3.SetConfigurable(false);
    success = JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc3);
    EXPECT_TRUE(success);
    PropertyDescriptor desc_res3(thread_);
    success = JSObject::GetOwnProperty(thread_, obj, key, desc_res3);
    EXPECT_TRUE(success);
    EXPECT_EQ(1, desc_res3.GetValue()->GetInt());
    EXPECT_EQ(false, desc_res3.IsWritable());
    EXPECT_EQ(false, desc_res3.IsEnumerable());
    EXPECT_EQ(false, desc_res3.IsConfigurable());

    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)));
    success = JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc4);
    EXPECT_FALSE(success);
}

TEST_F(JSObjectTest, HasProperty)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*obj != nullptr);

    char array[] = "x";
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString(array));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    bool flag = JSObject::HasProperty(thread_, obj, key);
    EXPECT_FALSE(flag);

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key, value);
    flag = JSObject::HasProperty(thread_, obj, key);
    EXPECT_TRUE(flag);

    JSObject::DeleteProperty(thread_, (obj), key);
    flag = JSObject::HasProperty(thread_, obj, key);
    EXPECT_FALSE(flag);
}

TEST_F(JSObjectTest, HasPropertyWithProtoType)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> grandfather = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> father = JSObject::ObjectCreate(thread_, grandfather);
    JSHandle<JSObject> son = JSObject::ObjectCreate(thread_, father);

    auto test_grand = grandfather->GetPrototype(thread_);
    auto test_father = father->GetPrototype(thread_);
    auto test_son = son->GetPrototype(thread_);
    EXPECT_TRUE(test_son != test_father);
    EXPECT_TRUE(test_grand != test_father);
    JSHandle<JSTaggedValue> son_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key1"));
    JSHandle<JSTaggedValue> father_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key2"));
    JSHandle<JSTaggedValue> grandfather_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key3"));
    JSHandle<JSTaggedValue> son_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> father_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> grandfather_value(thread_, JSTaggedValue(3));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(son), son_key, son_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(father), father_key, father_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(grandfather), grandfather_key, grandfather_value);

    bool flag = JSObject::HasProperty(thread_, son, son_key);
    EXPECT_TRUE(flag);
    flag = JSObject::HasProperty(thread_, son, father_key);
    EXPECT_TRUE(flag);
    flag = JSObject::HasProperty(thread_, son, grandfather_key);
    EXPECT_TRUE(flag);
}

TEST_F(JSObjectTest, HasOwnProperty)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> grandfather = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> father = JSObject::ObjectCreate(thread_, grandfather);
    JSHandle<JSObject> son = JSObject::ObjectCreate(thread_, father);

    JSHandle<JSTaggedValue> son_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key1"));
    JSHandle<JSTaggedValue> father_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key2"));
    JSHandle<JSTaggedValue> grandfather_key(thread_->GetEcmaVM()->GetFactory()->NewFromString("key3"));
    JSHandle<JSTaggedValue> son_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> father_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> grandfather_value(thread_, JSTaggedValue(3));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(son), son_key, son_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(father), father_key, father_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(grandfather), grandfather_key, grandfather_value);

    bool flag = JSTaggedValue::HasOwnProperty(thread_, JSHandle<JSTaggedValue>::Cast(son), son_key);
    EXPECT_TRUE(flag);
    flag = JSTaggedValue::HasOwnProperty(thread_, JSHandle<JSTaggedValue>::Cast(son), father_key);
    EXPECT_FALSE(flag);
    flag = JSTaggedValue::HasOwnProperty(thread_, JSHandle<JSTaggedValue>::Cast(son), grandfather_key);
    EXPECT_FALSE(flag);
}

TEST_F(JSObjectTest, GetOwnPropertyKeys)
{
    JSHandle<JSTaggedValue> constructor(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor);

    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString("x"));
    JSHandle<JSTaggedValue> key2(thread_->GetEcmaVM()->GetFactory()->NewFromString("y"));
    JSHandle<JSTaggedValue> key3(thread_->GetEcmaVM()->GetFactory()->NewFromString("3"));
    JSHandle<JSTaggedValue> key4(thread_->GetEcmaVM()->GetFactory()->NewFromString("4"));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> value2(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> value3(thread_, JSTaggedValue(3));
    JSHandle<JSTaggedValue> value4(thread_, JSTaggedValue(4));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key1, value1);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key2, value2);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key3, value3);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key4, value4);

    JSHandle<TaggedArray> array = JSObject::GetOwnPropertyKeys(thread_, obj);
    int length = array->GetLength();
    EXPECT_EQ(length, 4);
    int sum = 0;
    for (int i = 0; i < length; i++) {
        JSHandle<JSTaggedValue> key(thread_, array->Get(i));
        sum += JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->GetInt();
    }
    EXPECT_EQ(sum, 10);
}

TEST_F(JSObjectTest, ObjectCreateMethod)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> grandfather = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> father = JSObject::ObjectCreate(thread_, grandfather);
    JSHandle<JSObject> son = JSObject::ObjectCreate(thread_, father);

    EXPECT_EQ(son->GetPrototype(thread_), father.GetTaggedValue());
    EXPECT_EQ(father->GetPrototype(thread_), grandfather.GetTaggedValue());
    EXPECT_EQ(grandfather->GetPrototype(thread_), JSTaggedValue::Null());
}

TEST_F(JSObjectTest, GetMethod)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> obj = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> func(thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env));
    EXPECT_TRUE(*func != nullptr);
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString("1"));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key, func);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue().GetTaggedValue(),
              func.GetTaggedValue());
}

TEST_F(JSObjectTest, EnumerableOwnNames)
{
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(*obj != nullptr);

    PandaString tag_c_str = "x";
    JSHandle<EcmaString> tag_string = thread_->GetEcmaVM()->GetFactory()->NewFromString(&tag_c_str[0]);
    JSHandle<JSTaggedValue> key(tag_string);

    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key, value);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->GetInt(), 1);

    JSHandle<TaggedArray> names = JSObject::EnumerableOwnNames(thread_, obj);

    JSHandle<JSTaggedValue> key_from_names(thread_, JSTaggedValue(names->Get(0)));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key_from_names).GetValue()->GetInt(), 1);

    PropertyDescriptor desc_no_enum(thread_);
    desc_no_enum.SetEnumerable(false);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc_no_enum);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->GetInt(), 1);

    JSHandle<TaggedArray> names_no_enum = JSObject::EnumerableOwnNames(thread_, obj);
    EXPECT_TRUE(names_no_enum->GetLength() == 0);

    PropertyDescriptor desc_enum(thread_);
    desc_enum.SetConfigurable(false);
    desc_enum.SetEnumerable(true);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc_enum);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->GetInt(), 1);

    JSHandle<TaggedArray> names_no_config = JSObject::EnumerableOwnNames(thread_, obj);

    JSHandle<JSTaggedValue> key_no_config(thread_, JSTaggedValue(names_no_config->Get(0)));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key_no_config).GetValue()->GetInt(), 1);
}

TEST_F(JSObjectTest, SetIntegrityLevelSealed)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    EXPECT_TRUE(*obj1 != nullptr);
    PandaString undefined_c_str = "x";
    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString(&undefined_c_str[0]));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1, value1);

    // test SetIntegrityLevel::SEALED
    JSHandle<JSObject> jsobject(obj1);
    bool status1 = JSObject::SetIntegrityLevel(thread_, jsobject, IntegrityLevel::SEALED);
    EXPECT_TRUE(status1);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1).GetValue().GetTaggedValue(),
              value1.GetTaggedValue());
    PropertyDescriptor desc1(thread_);
    bool success1 = JSObject::GetOwnProperty(thread_, jsobject, key1, desc1);
    EXPECT_TRUE(success1);
    EXPECT_EQ(true, desc1.IsWritable());
    EXPECT_EQ(true, desc1.IsEnumerable());
    EXPECT_EQ(false, desc1.IsConfigurable());
}

TEST_F(JSObjectTest, SetIntegrityLevelFrozen)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    EXPECT_TRUE(*obj1 != nullptr);

    PandaString undefined_c_str = "x";
    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString(&undefined_c_str[0]));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1, value1);

    // test SetIntegrityLevel::FROZEN
    bool status1 = JSObject::SetIntegrityLevel(thread_, obj1, IntegrityLevel::FROZEN);
    EXPECT_TRUE(status1);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1).GetValue().GetTaggedValue(),
              value1.GetTaggedValue());
    PropertyDescriptor desc1(thread_);
    bool success1 = JSObject::GetOwnProperty(thread_, obj1, key1, desc1);
    EXPECT_TRUE(success1);
    EXPECT_EQ(false, desc1.IsWritable());
    EXPECT_EQ(true, desc1.IsEnumerable());
    EXPECT_EQ(false, desc1.IsConfigurable());
}

TEST_F(JSObjectTest, TestIntegrityLevelSealed)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    PandaString undefined_c_str = "level";
    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString(&undefined_c_str[0]));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1, value1);
    obj1->GetJSHClass()->SetExtensible(false);

    // test SetIntegrityLevel::SEALED
    bool status1 = JSObject::SetIntegrityLevel(thread_, obj1, IntegrityLevel::SEALED);
    EXPECT_TRUE(status1);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1).GetValue().GetTaggedValue(),
              value1.GetTaggedValue());

    PropertyDescriptor desc1(thread_);
    bool success1 = JSObject::GetOwnProperty(thread_, obj1, key1, desc1);
    EXPECT_TRUE(success1);
    EXPECT_EQ(true, JSObject::TestIntegrityLevel(thread_, obj1, IntegrityLevel::SEALED));
    EXPECT_EQ(false, JSObject::TestIntegrityLevel(thread_, obj1, IntegrityLevel::FROZEN));
}

TEST_F(JSObjectTest, TestIntegrityLevelFrozen)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    PandaString undefined_c_str = "level";
    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString(&undefined_c_str[0]));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1, value1);
    obj1->GetJSHClass()->SetExtensible(false);

    // test SetIntegrityLevel::FROZEN
    bool status1 = JSObject::SetIntegrityLevel(thread_, obj1, IntegrityLevel::FROZEN);
    EXPECT_TRUE(status1);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1).GetValue().GetTaggedValue(),
              value1.GetTaggedValue());

    PropertyDescriptor desc1(thread_);
    bool success1 = JSObject::GetOwnProperty(thread_, obj1, key1, desc1);
    EXPECT_TRUE(success1);
    EXPECT_EQ(true, JSObject::TestIntegrityLevel(thread_, obj1, IntegrityLevel::SEALED));
    EXPECT_EQ(true, JSObject::TestIntegrityLevel(thread_, obj1, IntegrityLevel::FROZEN));
}

TEST_F(JSObjectTest, TestIntegrityLevelWithoutProperty)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSTaggedValue> obj1(
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1));
    JSHandle<JSObject>::Cast(obj1)->GetJSHClass()->SetExtensible(false);
    PandaString undefined_c_str = "level";
    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString(&undefined_c_str[0]));

    // test SetIntegrityLevel::FROZEN
    JSHandle<JSObject> jsobject(obj1);
    bool status1 = JSObject::SetIntegrityLevel(thread_, jsobject, IntegrityLevel::SEALED);
    EXPECT_TRUE(status1);

    PropertyDescriptor desc1(thread_);
    bool success1 = JSObject::GetOwnProperty(thread_, jsobject, key1, desc1);
    EXPECT_TRUE(!success1);
    EXPECT_EQ(true, JSObject::TestIntegrityLevel(thread_, jsobject, IntegrityLevel::SEALED));
    EXPECT_EQ(true, JSObject::TestIntegrityLevel(thread_, jsobject, IntegrityLevel::FROZEN));
}

JSTaggedValue TestGetter(EcmaRuntimeCallInfo *argv)
{
    auto thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj(builtins_common::GetThis(argv));
    JSHandle<JSTaggedValue> key(factory->NewFromString("y"));
    JSTaggedValue value = JSObject::GetProperty(thread, JSHandle<JSTaggedValue>(obj), key).GetValue().GetTaggedValue();

    return JSTaggedValue(value.GetInt() + 1);
}

TEST_F(JSObjectTest, Getter)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    JSHandle<JSTaggedValue> key1(factory->NewFromString("x"));
    JSHandle<JSTaggedValue> key2(factory->NewFromString("y"));
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> getter =
        thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, reinterpret_cast<void *>(TestGetter));

    PropertyDescriptor desc1(thread_);
    desc1.SetGetter(JSHandle<JSTaggedValue>::Cast(getter));
    bool success1 = JSObject::DefineOwnProperty(thread_, obj, key1, desc1);
    EXPECT_TRUE(success1);

    PropertyDescriptor desc2(thread_);
    desc2.SetValue(JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)));
    success1 = JSObject::DefineOwnProperty(thread_, obj, key2, desc2);
    EXPECT_TRUE(success1);

    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key1).GetValue().GetTaggedValue(),
              JSTaggedValue(2));
}

JSTaggedValue TestSetter(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj(builtins_common::GetThis(argv));
    JSHandle<JSTaggedValue> key(factory->NewFromString("y"));
    JSTaggedValue value(JSObject::GetProperty(thread, JSHandle<JSTaggedValue>(obj), key).GetValue().GetTaggedValue());
    JSHandle<JSTaggedValue> value_handle(thread, JSTaggedValue(value.GetInt() + 1));
    JSObject::SetProperty(thread, JSHandle<JSTaggedValue>(obj), key, value_handle);

    return JSTaggedValue(JSTaggedValue::True());
}

TEST_F(JSObjectTest, Setter)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    JSHandle<JSTaggedValue> key1(factory->NewFromString("x"));
    JSHandle<JSTaggedValue> key2(factory->NewFromString("y"));
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> setter =
        thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, reinterpret_cast<void *>(TestSetter));

    PropertyDescriptor desc1(thread_);
    desc1.SetSetter(JSHandle<JSTaggedValue>::Cast(setter));
    bool success1 = JSObject::DefineOwnProperty(thread_, obj, key1, desc1);
    EXPECT_TRUE(success1);

    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    success1 = JSObject::DefineOwnProperty(thread_, obj, key2, desc2);
    EXPECT_TRUE(success1);

    JSHandle<JSTaggedValue> value_handle(thread_, JSTaggedValue::Undefined());
    EXPECT_TRUE(JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj), key1, value_handle));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key2).GetValue().GetTaggedValue(),
              JSTaggedValue(2));
}

TEST_F(JSObjectTest, SpeciesConstructor)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSFunction> constructor_func =
        factory->NewJSFunction(env, static_cast<void *>(nullptr), FunctionKind::BASE_CONSTRUCTOR);
    JSHandle<JSTaggedValue> constructor_func_value(constructor_func);
    constructor_func->GetJSHClass()->SetExtensible(true);
    JSFunction::NewJSFunctionPrototype(thread_, factory, constructor_func);

    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSTaggedValue> undefined_value(thread_, JSTaggedValue::Undefined());
    JSHandle<JSObject> proto_obj = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSTaggedValue> proto_obj_value(proto_obj);

    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    JSObject::SetProperty(thread_, proto_obj_value, constructor_key, constructor_func_value);

    factory->NewJSObjectByConstructor(constructor_func, JSHandle<JSTaggedValue>::Cast(constructor_func));
    JSHandle<JSFunction> species_construct =
        factory->NewJSFunction(env, static_cast<void *>(nullptr), FunctionKind::BASE_CONSTRUCTOR);
    JSHandle<JSTaggedValue> species_construct_value(species_construct);
    constructor_func->GetJSHClass()->SetExtensible(true);
    JSFunction::MakeConstructor(thread_, species_construct, undefined_value);

    JSHandle<JSTaggedValue> species_symbol = env->GetSpeciesSymbol();
    JSObject::SetProperty(thread_, constructor_func_value, species_symbol, species_construct_value);

    JSTaggedValue species_value =
        JSObject::SpeciesConstructor(thread_, proto_obj, constructor_func_value).GetTaggedValue();
    EXPECT_EQ(species_value, species_construct_value.GetTaggedValue());
}

JSTaggedValue TestUndefinedGetter([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    return JSTaggedValue(10);
}

JSTaggedValue TestUndefinedSetter([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    return JSTaggedValue(10);
}

TEST_F(JSObjectTest, GetterIsUndefined)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    JSHandle<JSTaggedValue> key(factory->NewFromString("property"));
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> getter =
        thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, reinterpret_cast<void *>(TestUndefinedGetter));
    JSHandle<JSFunction> setter =
        thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, reinterpret_cast<void *>(TestUndefinedSetter));
    JSHandle<JSTaggedValue> un_getter(thread_, JSTaggedValue::Undefined());

    PropertyDescriptor desc1(thread_);
    desc1.SetGetter(JSHandle<JSTaggedValue>::Cast(getter));
    desc1.SetSetter(JSHandle<JSTaggedValue>::Cast(setter));
    desc1.SetConfigurable(true);
    desc1.SetEnumerable(true);
    bool success1 = JSObject::DefineOwnProperty(thread_, obj, key, desc1);
    EXPECT_TRUE(success1);

    PropertyDescriptor desc2(thread_);
    desc2.SetGetter(un_getter);
    bool success2 = JSObject::DefineOwnProperty(thread_, obj, key, desc2);
    EXPECT_TRUE(success2);

    PropertyDescriptor desc(thread_);
    bool success = JSObject::GetOwnProperty(thread_, obj, key, desc);
    EXPECT_TRUE(success);
    EXPECT_TRUE(desc.GetSetter()->IsJSFunction());
    EXPECT_TRUE(desc.GetGetter()->IsUndefined());
}

TEST_F(JSObjectTest, SetterIsUndefined)
{
    JSHandle<JSTaggedValue> dynclass1(thread_, JSObjectTestCreate(thread_));
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass1), dynclass1);
    JSHandle<JSTaggedValue> key(factory->NewFromString("property"));
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> getter =
        thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, reinterpret_cast<void *>(TestUndefinedGetter));
    JSHandle<JSFunction> setter =
        thread_->GetEcmaVM()->GetFactory()->NewJSFunction(env, reinterpret_cast<void *>(TestUndefinedSetter));
    JSHandle<JSTaggedValue> un_setter(thread_, JSTaggedValue::Undefined());

    PropertyDescriptor desc1(thread_);
    desc1.SetGetter(JSHandle<JSTaggedValue>::Cast(getter));
    desc1.SetSetter(JSHandle<JSTaggedValue>::Cast(setter));
    desc1.SetConfigurable(true);
    desc1.SetEnumerable(true);
    bool success1 = JSObject::DefineOwnProperty(thread_, obj, key, desc1);
    EXPECT_TRUE(success1);

    PropertyDescriptor desc2(thread_);
    desc2.SetSetter(un_setter);
    bool success2 = JSObject::DefineOwnProperty(thread_, obj, key, desc2);
    EXPECT_TRUE(success2);

    PropertyDescriptor desc(thread_);
    bool success = JSObject::GetOwnProperty(thread_, obj, key, desc);
    EXPECT_TRUE(success);
    EXPECT_TRUE(desc.GetSetter()->IsUndefined());

    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue().GetTaggedValue(),
              JSTaggedValue(10));
}

TEST_F(JSObjectTest, HClass)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    JSHandle<JSHClass> hc0(thread_, obj1->GetJSHClass());

    JSHandle<JSTaggedValue> key1(factory->NewFromCanBeCompressString("x"));
    JSHandle<JSTaggedValue> key2(factory->NewFromCanBeCompressString("y"));
    JSHandle<JSTaggedValue> key3(factory->NewFromCanBeCompressString("z"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1, value);
    JSHandle<JSHClass> hc1(thread_, obj1->GetJSHClass());
    EXPECT_NE(hc0.GetTaggedValue(), hc1.GetTaggedValue());
    EXPECT_EQ(hc0.GetTaggedValue(), hc1->GetParent());

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key2, value);
    JSHandle<JSHClass> hc2(thread_, obj1->GetJSHClass());
    EXPECT_NE(hc1.GetTaggedValue(), hc2.GetTaggedValue());
    EXPECT_EQ(hc1.GetTaggedValue(), hc2->GetParent());

    JSHandle<JSObject> obj2 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_EQ(hc0.GetTaggedValue().GetTaggedObject(), obj2->GetJSHClass());

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key1, value);
    EXPECT_EQ(hc1.GetTaggedValue().GetTaggedObject(), obj2->GetJSHClass());

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key3, value);
    JSHandle<JSHClass> hc3(thread_, obj2->GetJSHClass());
    EXPECT_NE(hc1.GetTaggedValue().GetTaggedObject(), obj2->GetJSHClass());
    EXPECT_EQ(hc1.GetTaggedValue(), obj2->GetJSHClass()->GetParent());

    JSHandle<JSObject> obj3 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_EQ(hc0.GetTaggedValue().GetTaggedObject(), obj3->GetJSHClass());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), key1, value);
    EXPECT_EQ(hc1.GetTaggedValue().GetTaggedObject(), obj3->GetJSHClass());

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), key2, value);
    EXPECT_EQ(hc2.GetTaggedValue().GetTaggedObject(), obj3->GetJSHClass());

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), key3, value);
    EXPECT_NE(hc3.GetTaggedValue().GetTaggedObject(), obj3->GetJSHClass());
    EXPECT_EQ(hc2.GetTaggedValue(), obj3->GetJSHClass()->GetParent());
}

TEST_F(JSObjectTest, FastToSlow)
{
    auto ecma_vm = thread_->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);

    JSMutableHandle<EcmaString> key(factory->NewFromCanBeCompressString("x"));
    JSMutableHandle<JSTaggedValue> number(thread_, JSTaggedValue(0));
    JSMutableHandle<JSTaggedValue> newkey(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    for (uint32_t i = 0; i < PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES; i++) {
        number.Update(JSTaggedValue(i));
        number.Update(JSTaggedValue::ToString(thread_, number).GetTaggedValue());
        EcmaString *new_string = *factory->ConcatFromString(key, JSTaggedValue::ToString(thread_, number));
        newkey.Update(JSTaggedValue(new_string));
        JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), newkey, value);
    }

    EXPECT_FALSE(TaggedArray::Cast(obj1->GetProperties().GetTaggedObject())->IsDictionaryMode());

    number.Update(JSTaggedValue(PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES));
    number.Update(JSTaggedValue::ToString(thread_, number).GetTaggedValue());
    EcmaString *new_string = *factory->ConcatFromString(key, JSTaggedValue::ToString(thread_, number));
    newkey.Update(JSTaggedValue(new_string));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), newkey, value);

    EXPECT_TRUE(TaggedArray::Cast(obj1->GetProperties().GetTaggedObject())->IsDictionaryMode());
    NameDictionary *dict = NameDictionary::Cast(obj1->GetProperties().GetTaggedObject());
    EXPECT_EQ(dict->EntriesCount(), PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES + 1);
    EXPECT_EQ(dict->NextEnumerationIndex(thread_), PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES + 1);
}

TEST_F(JSObjectTest, DeleteMiddle)
{
    auto ecma_vm = thread_->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj1 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);

    JSMutableHandle<EcmaString> key(factory->NewFromCanBeCompressString("x"));
    JSMutableHandle<JSTaggedValue> number(thread_, JSTaggedValue(0));
    JSMutableHandle<JSTaggedValue> newkey(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    for (uint32_t i = 0; i < 10; i++) {
        number.Update(JSTaggedValue(i));
        number.Update(JSTaggedValue::ToString(thread_, number).GetTaggedValue());
        EcmaString *new_string = *factory->ConcatFromString(key, JSTaggedValue::ToString(thread_, number));
        newkey.Update(JSTaggedValue(new_string));
        JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), newkey, value);
    }

    EXPECT_FALSE(TaggedArray::Cast(obj1->GetProperties().GetTaggedObject())->IsDictionaryMode());

    JSMutableHandle<JSTaggedValue> key5(factory->NewFromCanBeCompressString("x5"));
    JSObject::DeleteProperty(thread_, (obj1), key5);

    EXPECT_TRUE(TaggedArray::Cast(obj1->GetProperties().GetTaggedObject())->IsDictionaryMode());
    NameDictionary *dict = NameDictionary::Cast(obj1->GetProperties().GetTaggedObject());
    EXPECT_EQ(dict->EntriesCount(), 9);
    EXPECT_FALSE(JSObject::HasProperty(thread_, obj1, key5));
}

TEST_F(JSObjectTest, ElementFastToSlow)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> key2000(thread_, JSTaggedValue(2000));
    JSHandle<JSTaggedValue> key_str(factory->NewFromCanBeCompressString("str"));

    // test dictionary [0,1,2,...,2000]
    JSHandle<JSObject> obj1 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    EXPECT_TRUE(!TaggedArray::Cast(obj1->GetElements().GetTaggedObject())->IsDictionaryMode());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key_str, key2);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key0, key0);
    EXPECT_TRUE(!TaggedArray::Cast(obj1->GetElements().GetTaggedObject())->IsDictionaryMode());
    JSHandle<JSHClass> dyn_class(thread_, obj1->GetJSHClass());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key1, key1);
    EXPECT_TRUE(!TaggedArray::Cast(obj1->GetElements().GetTaggedObject())->IsDictionaryMode());
    EXPECT_EQ(obj1->GetJSHClass(), *dyn_class);

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key2000, key2000);
    EXPECT_TRUE(TaggedArray::Cast(obj1->GetElements().GetTaggedObject())->IsDictionaryMode());
    JSTaggedValue value =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), key_str).GetValue().GetTaggedValue();
    EXPECT_EQ(value, key2.GetTaggedValue());
    // test holey [0,,2]
    JSHandle<JSObject> obj2 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key0, key0);
    EXPECT_TRUE(!TaggedArray::Cast(obj2->GetElements().GetTaggedObject())->IsDictionaryMode());
    JSHandle<JSHClass> dyn_class2(thread_, obj2->GetJSHClass());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), key2, key2);
    EXPECT_TRUE(!TaggedArray::Cast(obj2->GetElements().GetTaggedObject())->IsDictionaryMode());
    EXPECT_EQ(obj2->GetJSHClass(), *dyn_class2);
    // test change attr
    JSHandle<JSObject> obj3 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), key0, key0);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), key1, key1);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), key2, key2);
    EXPECT_TRUE(!TaggedArray::Cast(obj3->GetElements().GetTaggedObject())->IsDictionaryMode());
    PropertyDescriptor desc(thread_);
    desc.SetValue(key1);
    desc.SetWritable(false);
    JSObject::DefineOwnProperty(thread_, obj3, key1, desc);
    EXPECT_TRUE(TaggedArray::Cast(obj3->GetElements().GetTaggedObject())->IsDictionaryMode());
    // test delete element
    JSHandle<JSObject> obj4 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj4), key0, key0);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj4), key1, key1);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj4), key2, key2);
    EXPECT_TRUE(!TaggedArray::Cast(obj4->GetElements().GetTaggedObject())->IsDictionaryMode());
    JSObject::DeleteProperty(thread_, (obj4), key1);
    EXPECT_TRUE(TaggedArray::Cast(obj4->GetElements().GetTaggedObject())->IsDictionaryMode());

    JSHandle<JSTaggedValue> value1001(thread_, JSTaggedValue(1001));
    JSHandle<JSObject> obj100 = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    PropertyDescriptor desc1(thread_);
    desc1.SetValue(value1001);
    desc1.SetWritable(false);
    desc1.SetEnumerable(false);
    desc1.SetConfigurable(false);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj100), key0, key1);
    JSObject::DefineOwnProperty(thread_, obj100, key0, desc1);
    JSTaggedValue result1001 =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj100), key0).GetValue().GetTaggedValue();
    EXPECT_EQ(result1001, value1001.GetTaggedValue());
}

TEST_F(JSObjectTest, EnableProtoChangeMarker)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> obj1 = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> obj2 = JSObject::ObjectCreate(thread_, obj1);
    JSHandle<JSObject> obj3 = JSObject::ObjectCreate(thread_, obj2);

    JSHandle<JSTaggedValue> obj1_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key1"));
    JSHandle<JSTaggedValue> obj2_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key2"));
    JSHandle<JSTaggedValue> obj3_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key3"));
    JSHandle<JSTaggedValue> obj1_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> obj2_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> obj3_value(thread_, JSTaggedValue(3));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), obj1_key, obj1_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), obj2_key, obj2_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), obj3_key, obj3_value);
    JSHandle<JSHClass> obj3_dynclass(thread_, obj3->GetJSHClass());
    JSHandle<JSTaggedValue> result_marker = JSHClass::EnableProtoChangeMarker(thread_, obj3_dynclass);
    EXPECT_TRUE(result_marker->IsProtoChangeMarker());
    bool has_changed = ProtoChangeMarker::Cast(result_marker->GetTaggedObject())->GetHasChanged();
    EXPECT_TRUE(!has_changed);

    JSHandle<JSHClass> obj1_dynclass(thread_, obj1->GetJSHClass());
    JSHandle<JSHClass> obj2_dynclass(thread_, obj2->GetJSHClass());
    JSTaggedValue obj2_marker = obj2_dynclass->GetProtoChangeMarker();
    EXPECT_TRUE(obj2_marker.IsProtoChangeMarker());
    bool has_changed2 = ProtoChangeMarker::Cast(obj2_marker.GetTaggedObject())->GetHasChanged();
    EXPECT_TRUE(!has_changed2);

    JSTaggedValue obj1_marker = obj1_dynclass->GetProtoChangeMarker();
    EXPECT_TRUE(!obj1_marker.IsProtoChangeMarker());

    JSTaggedValue proto_details2 = obj2_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details2.IsProtoChangeDetails());
    JSTaggedValue proto_details1 = obj1_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details1.IsProtoChangeDetails());
    JSTaggedValue listeners1 = ProtoChangeDetails::Cast(proto_details1.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners1 != JSTaggedValue(0));
    JSTaggedValue listeners2 = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners2 == JSTaggedValue(0));
    JSTaggedValue index = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetRegisterIndex();
    JSTaggedValue listeners_result = ChangeListener::Cast(listeners1.GetTaggedObject())->Get(index.GetArrayLength());
    EXPECT_TRUE(listeners_result == obj2_dynclass.GetTaggedValue());
}

TEST_F(JSObjectTest, BuildRegisterTree)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> obj1 = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> obj2 = JSObject::ObjectCreate(thread_, obj1);
    JSHandle<JSObject> obj3 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj4 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj5 = JSObject::ObjectCreate(thread_, obj4);
    JSHandle<JSObject> obj6 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj7 = JSObject::ObjectCreate(thread_, obj6);

    JSHandle<JSTaggedValue> obj1_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key1"));
    JSHandle<JSTaggedValue> obj2_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key2"));
    JSHandle<JSTaggedValue> obj3_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key3"));
    JSHandle<JSTaggedValue> obj4_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key4"));
    JSHandle<JSTaggedValue> obj5_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key5"));
    JSHandle<JSTaggedValue> obj6_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key6"));
    JSHandle<JSTaggedValue> obj7_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key7"));

    JSHandle<JSTaggedValue> obj1_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> obj2_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> obj3_value(thread_, JSTaggedValue(3));
    JSHandle<JSTaggedValue> obj4_value(thread_, JSTaggedValue(4));
    JSHandle<JSTaggedValue> obj5_value(thread_, JSTaggedValue(5));
    JSHandle<JSTaggedValue> obj6_value(thread_, JSTaggedValue(6));
    JSHandle<JSTaggedValue> obj7_value(thread_, JSTaggedValue(7));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), obj1_key, obj1_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), obj2_key, obj2_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), obj3_key, obj3_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj4), obj4_key, obj4_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj5), obj5_key, obj5_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj6), obj6_key, obj6_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj7), obj7_key, obj7_value);

    JSHandle<JSHClass> obj1_dynclass(thread_, obj1->GetJSHClass());
    JSHandle<JSHClass> obj2_dynclass(thread_, obj2->GetJSHClass());
    JSHandle<JSHClass> obj3_dynclass(thread_, obj3->GetJSHClass());
    JSHandle<JSHClass> obj4_dynclass(thread_, obj4->GetJSHClass());
    JSHandle<JSHClass> obj5_dynclass(thread_, obj5->GetJSHClass());
    JSHandle<JSHClass> obj6_dynclass(thread_, obj6->GetJSHClass());
    JSHandle<JSHClass> obj7_dynclass(thread_, obj7->GetJSHClass());

    JSHandle<JSTaggedValue> result3_marker = JSHClass::EnableProtoChangeMarker(thread_, obj3_dynclass);
    JSHandle<JSTaggedValue> result5_marker = JSHClass::EnableProtoChangeMarker(thread_, obj5_dynclass);
    EXPECT_TRUE(result3_marker->IsProtoChangeMarker());
    EXPECT_TRUE(!(ProtoChangeMarker::Cast(result3_marker->GetTaggedObject())->GetHasChanged()));
    EXPECT_TRUE(result5_marker->IsProtoChangeMarker());
    EXPECT_TRUE(!(ProtoChangeMarker::Cast(result5_marker->GetTaggedObject())->GetHasChanged()));

    EXPECT_TRUE(obj4_dynclass->GetProtoChangeMarker().IsProtoChangeMarker());
    EXPECT_TRUE(!obj6_dynclass->GetProtoChangeMarker().IsProtoChangeMarker());

    JSHandle<JSTaggedValue> result7_marker = JSHClass::EnableProtoChangeMarker(thread_, obj7_dynclass);
    EXPECT_TRUE(result7_marker->IsProtoChangeMarker());
    EXPECT_TRUE(!(ProtoChangeMarker::Cast(result7_marker->GetTaggedObject())->GetHasChanged()));

    JSTaggedValue proto_details1 = obj1_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details1.IsProtoChangeDetails());
    JSTaggedValue listeners1_value = ProtoChangeDetails::Cast(proto_details1.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners1_value != JSTaggedValue(0));
    JSHandle<ChangeListener> listeners1(thread_, listeners1_value.GetTaggedObject());
    JSTaggedValue proto_details2 = obj2_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details2.IsProtoChangeDetails());
    JSTaggedValue index2 = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetRegisterIndex();
    EXPECT_TRUE(listeners1->Get(index2.GetArrayLength()) == obj2_dynclass.GetTaggedValue());

    JSTaggedValue listeners2_value = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners2_value != JSTaggedValue(0));
    JSHandle<ChangeListener> listeners2(thread_, listeners2_value.GetTaggedObject());
    JSTaggedValue proto_details4 = obj4_dynclass->GetProtoChangeDetails();
    JSTaggedValue proto_details6 = obj6_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details4.IsProtoChangeDetails());
    EXPECT_TRUE(proto_details6.IsProtoChangeDetails());
    JSTaggedValue index4 = ProtoChangeDetails::Cast(proto_details4.GetTaggedObject())->GetRegisterIndex();
    EXPECT_TRUE(listeners2->Get(index4.GetArrayLength()) == obj4_dynclass.GetTaggedValue());
    JSTaggedValue index6 = ProtoChangeDetails::Cast(proto_details6.GetTaggedObject())->GetRegisterIndex();
    EXPECT_TRUE(listeners2->Get(index6.GetArrayLength()) == obj6_dynclass.GetTaggedValue());

    EXPECT_TRUE(listeners1->GetEnd() == 1);
    EXPECT_TRUE(listeners2->GetEnd() == 2);
}

TEST_F(JSObjectTest, NoticeThroughChain)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> obj1 = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> obj2 = JSObject::ObjectCreate(thread_, obj1);
    JSHandle<JSObject> obj3 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj4 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj5 = JSObject::ObjectCreate(thread_, obj4);
    JSHandle<JSObject> obj6 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj7 = JSObject::ObjectCreate(thread_, obj6);

    JSHandle<JSTaggedValue> obj1_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key1"));
    JSHandle<JSTaggedValue> obj2_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key2"));
    JSHandle<JSTaggedValue> obj3_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key3"));
    JSHandle<JSTaggedValue> obj4_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key4"));
    JSHandle<JSTaggedValue> obj5_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key5"));
    JSHandle<JSTaggedValue> obj6_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key6"));
    JSHandle<JSTaggedValue> obj7_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key7"));

    JSHandle<JSTaggedValue> obj1_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> obj2_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> obj3_value(thread_, JSTaggedValue(3));
    JSHandle<JSTaggedValue> obj4_value(thread_, JSTaggedValue(4));
    JSHandle<JSTaggedValue> obj5_value(thread_, JSTaggedValue(5));
    JSHandle<JSTaggedValue> obj6_value(thread_, JSTaggedValue(6));
    JSHandle<JSTaggedValue> obj7_value(thread_, JSTaggedValue(7));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), obj1_key, obj1_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), obj2_key, obj2_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), obj3_key, obj3_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj4), obj4_key, obj4_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj5), obj5_key, obj5_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj6), obj6_key, obj6_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj7), obj7_key, obj7_value);

    JSHandle<JSHClass> obj1_dynclass(thread_, obj1->GetJSHClass());
    JSHandle<JSHClass> obj2_dynclass(thread_, obj2->GetJSHClass());
    JSHandle<JSHClass> obj3_dynclass(thread_, obj3->GetJSHClass());
    JSHandle<JSHClass> obj4_dynclass(thread_, obj4->GetJSHClass());
    JSHandle<JSHClass> obj5_dynclass(thread_, obj5->GetJSHClass());
    JSHandle<JSHClass> obj6_dynclass(thread_, obj6->GetJSHClass());
    JSHandle<JSHClass> obj7_dynclass(thread_, obj7->GetJSHClass());

    JSHClass::EnableProtoChangeMarker(thread_, obj3_dynclass);
    JSHClass::EnableProtoChangeMarker(thread_, obj7_dynclass);
    JSHClass::EnableProtoChangeMarker(thread_, obj5_dynclass);

    JSHClass::NoticeThroughChain(thread_, obj2_dynclass);
    JSHClass::UnregisterOnProtoChain(thread_, obj2_dynclass);
    JSTaggedValue proto_details1 = obj1_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details1.IsProtoChangeDetails());
    JSTaggedValue listeners1_value = ProtoChangeDetails::Cast(proto_details1.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners1_value != JSTaggedValue(0));
    JSHandle<ChangeListener> listeners1(thread_, listeners1_value.GetTaggedObject());
    uint32_t hole_index = ChangeListener::CheckHole(listeners1);
    EXPECT_TRUE(hole_index == 0);

    JSTaggedValue proto_details2 = obj2_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details2.IsProtoChangeDetails());
    JSTaggedValue listeners2_value = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners2_value != JSTaggedValue(0));
    JSTaggedValue index2 = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetRegisterIndex();
    EXPECT_TRUE(listeners1->Get(index2.GetArrayLength()) == JSTaggedValue::Hole());

    JSTaggedValue obj6_marker = obj6_dynclass->GetProtoChangeMarker();
    EXPECT_TRUE(obj6_marker.IsProtoChangeMarker());
    bool has_changed6 = ProtoChangeMarker::Cast(obj6_marker.GetTaggedObject())->GetHasChanged();
    EXPECT_TRUE(has_changed6);

    JSTaggedValue obj4_marker = obj4_dynclass->GetProtoChangeMarker();
    EXPECT_TRUE(obj4_marker.IsProtoChangeMarker());
    bool has_changed4 = ProtoChangeMarker::Cast(obj4_marker.GetTaggedObject())->GetHasChanged();
    EXPECT_TRUE(has_changed4);
}

TEST_F(JSObjectTest, ChangeProtoAndNoticeTheChain)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> obj1 = JSObject::ObjectCreate(thread_, null_handle);
    JSHandle<JSObject> obj2 = JSObject::ObjectCreate(thread_, obj1);
    JSHandle<JSObject> obj3 = JSObject::ObjectCreate(thread_, obj1);
    JSHandle<JSObject> obj4 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj5 = JSObject::ObjectCreate(thread_, obj4);
    JSHandle<JSObject> obj6 = JSObject::ObjectCreate(thread_, obj2);
    JSHandle<JSObject> obj7 = JSObject::ObjectCreate(thread_, obj6);

    JSHandle<JSTaggedValue> obj1_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key1"));
    JSHandle<JSTaggedValue> obj2_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key2"));
    JSHandle<JSTaggedValue> obj3_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key3"));
    JSHandle<JSTaggedValue> obj4_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key4"));
    JSHandle<JSTaggedValue> obj5_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key5"));
    JSHandle<JSTaggedValue> obj6_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key6"));
    JSHandle<JSTaggedValue> obj7_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("key7"));

    JSHandle<JSTaggedValue> obj1_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> obj2_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> obj3_value(thread_, JSTaggedValue(3));
    JSHandle<JSTaggedValue> obj4_value(thread_, JSTaggedValue(4));
    JSHandle<JSTaggedValue> obj5_value(thread_, JSTaggedValue(5));
    JSHandle<JSTaggedValue> obj6_value(thread_, JSTaggedValue(6));
    JSHandle<JSTaggedValue> obj7_value(thread_, JSTaggedValue(7));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj1), obj1_key, obj1_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj2), obj2_key, obj2_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj3), obj3_key, obj3_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj4), obj4_key, obj4_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj5), obj5_key, obj5_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj6), obj6_key, obj6_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj7), obj7_key, obj7_value);

    JSHandle<JSHClass> obj5_dynclass(thread_, obj5->GetJSHClass());
    JSHandle<JSHClass> obj7_dynclass(thread_, obj7->GetJSHClass());

    JSHClass::EnableProtoChangeMarker(thread_, obj7_dynclass);
    JSHClass::EnableProtoChangeMarker(thread_, obj5_dynclass);

    JSObject::SetPrototype(thread_, obj2, JSHandle<JSTaggedValue>(obj3));

    JSHandle<JSHClass> obj1_dynclass(thread_, obj1->GetJSHClass());
    JSHandle<JSHClass> obj2_dynclass(thread_, obj2->GetJSHClass());
    JSHandle<JSHClass> obj3_dynclass(thread_, obj3->GetJSHClass());
    JSHandle<JSHClass> obj4_dynclass(thread_, obj4->GetJSHClass());
    JSHandle<JSHClass> obj6_dynclass(thread_, obj6->GetJSHClass());

    JSTaggedValue obj6_marker = obj6_dynclass->GetProtoChangeMarker();
    EXPECT_TRUE(obj6_marker.IsProtoChangeMarker());
    bool has_changed6 = ProtoChangeMarker::Cast(obj6_marker.GetTaggedObject())->GetHasChanged();
    EXPECT_TRUE(has_changed6);

    JSTaggedValue obj4_marker = obj4_dynclass->GetProtoChangeMarker();
    EXPECT_TRUE(obj4_marker.IsProtoChangeMarker());
    bool has_changed4 = ProtoChangeMarker::Cast(obj4_marker.GetTaggedObject())->GetHasChanged();
    EXPECT_TRUE(has_changed4);

    JSTaggedValue proto_details1 = obj1_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details1.IsProtoChangeDetails());
    JSTaggedValue proto_details2 = obj2_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details2.IsProtoChangeDetails());
    JSTaggedValue proto_details3 = obj3_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details3.IsProtoChangeDetails());
    JSTaggedValue proto_details4 = obj4_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details4.IsProtoChangeDetails());
    JSTaggedValue proto_details6 = obj6_dynclass->GetProtoChangeDetails();
    EXPECT_TRUE(proto_details6.IsProtoChangeDetails());

    JSTaggedValue listeners1 = ProtoChangeDetails::Cast(proto_details1.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners1 != JSTaggedValue(0));
    JSTaggedValue listeners2 = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners2 != JSTaggedValue(0));
    JSTaggedValue listeners3 = ProtoChangeDetails::Cast(proto_details3.GetTaggedObject())->GetChangeListener();
    EXPECT_TRUE(listeners3 != JSTaggedValue(0));

    JSTaggedValue index2 = ProtoChangeDetails::Cast(proto_details2.GetTaggedObject())->GetRegisterIndex();
    JSTaggedValue index3 = ProtoChangeDetails::Cast(proto_details3.GetTaggedObject())->GetRegisterIndex();
    JSTaggedValue index4 = ProtoChangeDetails::Cast(proto_details4.GetTaggedObject())->GetRegisterIndex();
    JSTaggedValue index6 = ProtoChangeDetails::Cast(proto_details6.GetTaggedObject())->GetRegisterIndex();

    JSTaggedValue result2 = ChangeListener::Cast(listeners3.GetTaggedObject())->Get(index2.GetArrayLength());
    JSTaggedValue result3 = ChangeListener::Cast(listeners1.GetTaggedObject())->Get(index3.GetArrayLength());
    JSTaggedValue result4 = ChangeListener::Cast(listeners2.GetTaggedObject())->Get(index4.GetArrayLength());
    JSTaggedValue result6 = ChangeListener::Cast(listeners2.GetTaggedObject())->Get(index6.GetArrayLength());

    EXPECT_TRUE(result2 == obj2_dynclass.GetTaggedValue());
    EXPECT_TRUE(result3 == obj3_dynclass.GetTaggedValue());
    EXPECT_TRUE(result4 == obj4_dynclass.GetTaggedValue());
    EXPECT_TRUE(result6 == obj6_dynclass.GetTaggedValue());
}

TEST_F(JSObjectTest, NativePointerField)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> obj_func(thread_, JSObjectTestCreate(thread_));
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    obj->SetHash(87);
    EXPECT_TRUE(obj->GetHash() == 87);

    obj->SetNativePointerFieldCount(1);
    char array[] = "Hello World!";
    obj->SetNativePointerField(0, array, nullptr, nullptr);
    int32_t count = obj->GetNativePointerFieldCount();
    EXPECT_TRUE(count == 1);
    void *pointer = obj->GetNativePointerField(0);
    EXPECT_TRUE(pointer == array);
}

// NOLINTEND(modernize-avoid-c-arrays,readability-magic-numbers)

}  // namespace panda::test
