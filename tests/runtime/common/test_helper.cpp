/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_helper.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter-inl.h"

namespace panda::test {
using panda::ecmascript::EcmaRuntimeCallInfo;
using panda::ecmascript::JSTaggedValue;
using panda::ecmascript::JSThread;

thread_local bool TestHelper::is_leaf_ = true;
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
ecmascript::JSHandle<ecmascript::JSFunction> TestHelper::method_function_ {};

std::unique_ptr<EcmaRuntimeCallInfo> TestHelper::CreateEcmaRuntimeCallInfo(JSThread *thread, JSTaggedValue new_tgt,
                                                                           ArraySizeT argv_length)
{
    auto method = thread->GetEcmaVM()->GetMethodForNativeFunction(nullptr);
    if (is_leaf_ && thread->GetCurrentFrame() != nullptr) {
        method->ExitNativeMethodFrame(thread);
    }
    is_leaf_ = true;

    const uint8_t test_decoded_size = 2;
    // argvLength includes number of int64_t to store value and tag of function, 'this' and call args
    // It doesn't include new.target argument
    uint32_t num_actual_args = argv_length / test_decoded_size + 1;

    std::vector<TaggedValue> args;
    for (size_t i = 0; i < num_actual_args; ++i) {
        args.emplace_back(JSTaggedValue::VALUE_UNDEFINED);
    }

    Frame *frame =
        method->EnterNativeMethodFrame<panda::ecmascript::JSInvokeHelper>(thread, 0, num_actual_args, args.data());

    auto call_info = std::make_unique<EcmaRuntimeCallInfo>(thread, num_actual_args,
                                                           reinterpret_cast<JSTaggedValue *>(&frame->GetVReg(0)));

    call_info->SetFunction(method_function_.GetTaggedValue());
    call_info->SetNewTarget(new_tgt);
    return call_info;
}

Frame *TestHelper::SetupFrame(JSThread *thread, [[maybe_unused]] EcmaRuntimeCallInfo *info)
{
    is_leaf_ = false;
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-cstyle-cast)
    ASSERT((void *)info->GetArgAddress(0) == &thread->GetCurrentFrame()->GetVReg(0));
    return thread->GetCurrentFrame()->GetPrevFrame();
}

void TestHelper::TearDownFrame([[maybe_unused]] JSThread *thread, [[maybe_unused]] Frame *prev)
{
    is_leaf_ = true;
    auto frame = thread->GetCurrentFrame();
    while (frame != nullptr && frame->GetPrevFrame() != prev) {
        Method::ExitNativeMethodFrame(thread);
        frame = thread->GetCurrentFrame();
    }
}
}  // namespace panda::test
