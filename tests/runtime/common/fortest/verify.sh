#!/bin/bash
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.

set -eo pipefail

expected="*
**
***
****
*****
******
*******
********
*********
**********
10
9
8
7
6
5
4
3
2
1"
actual=$(cat "$1")
if [[ "$actual" == "$expected" ]];then
    exit 0;
else
    echo -e "expected:"$expected
    echo -e "actual:"$actual
    echo -e "\033[31mfortest test failed\033[0m"
    exit 1;
fi