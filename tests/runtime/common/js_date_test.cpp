/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/js_date.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/global_env.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::coretypes;

namespace panda::test {
class JSDateTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

JSDate *JSDateCreate(JSThread *thread)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> date_function = global_env->GetDateFunction();
    JSHandle<JSDate> date_object =
        JSHandle<JSDate>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(date_function), date_function));
    return *date_object;
}

TEST_F(JSDateTest, Create)
{
    double tm = 0.0;
    JSHandle<JSDate> js_date(thread_, JSDateCreate(thread_));
    EXPECT_EQ(js_date->GetTimeValue(), JSTaggedValue(tm));
    EXPECT_EQ(js_date->GetLocalOffset(), JSTaggedValue(JSDate::MAX_DOUBLE));
    // NOLINTNEXTLINE(readability-magic-numbers)
    tm = 28 * 60 * 60 * 1000;
    js_date->SetTimeValue(thread_, JSTaggedValue(tm));

    [[maybe_unused]] double temp = js_date->GetTimeValue().GetDouble();
    EXPECT_EQ(js_date->GetTimeValue(), JSTaggedValue(tm));
}

TEST_F(JSDateTest, MakeTime)
{
    double const day1 = ecmascript::JSDate::MakeDay(0, 11, 31);
    double const time1 = ecmascript::JSDate::MakeTime(0, 0, 0, 0);
    double ms1 = ecmascript::JSDate::TimeClip(ecmascript::JSDate::MakeDate(day1, time1));
    EXPECT_EQ(ms1, -62135683200000.0);

    double const day = ecmascript::JSDate::MakeDay(-1, 11, 31);
    double const time = ecmascript::JSDate::MakeTime(0, 0, 0, 0);
    double ms = ecmascript::JSDate::TimeClip(ecmascript::JSDate::MakeDate(day, time));
    EXPECT_EQ(ms, -62167305600000.0);
}

TEST_F(JSDateTest, IsoParseStringToMs)
{
    PandaString str = "2020-11-19T12:18:18.132Z";
    JSTaggedValue ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1605788298132.0);

    str = "2020-11-19Z";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1605744000000.0);

    str = "2020-11";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1604188800000.0);

    str = "+275760-09-13T00:00:00.000Z";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 8640000000000000.0);

    str = "-271821-04-20T00:00:00.000Z";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), -8640000000000000.0);

    str = "2020T12:18Z";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1577881080000.0);

    str = "2020T12:18:17.231Z";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1577881097231.0);

    str = "2020-11T12:18:17.231Z";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1604233097231.0);

    str = "1645-11T12:18:17.231+08:00";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), -10229658102769.0);

    str = "2020-11-19T12:18-08:12";
    ms = ecmascript::JSDate::IsoParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1605817800000.0);
}

TEST_F(JSDateTest, LocalParseStringToMs)
{
    PandaString str = "Thu Nov 19 2020 20:18:18 GMT+0800";
    JSTaggedValue ms = ecmascript::JSDate::LocalParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1605788298000.0);

    str = "Thu Nov 19 2020 20:18 GMT-0800";
    ms = ecmascript::JSDate::LocalParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1605845880000.0);

    str = "Thu Nov 03 2093 04:18 GMT";
    ms = ecmascript::JSDate::LocalParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 3908060280000.0);

    str = "Thu Nov 19 1820 GMT+1232";
    ms = ecmascript::JSDate::LocalParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), -4705734720000.0);
}

TEST_F(JSDateTest, UtcParseStringToMs)
{
    PandaString str = "Thu, 19 Nov 2020 20:18:18 GMT+0800";
    JSTaggedValue ms = ecmascript::JSDate::UtcParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1605788298000.0);

    str = "Thu, 19 Nov 2020 20:18 GMT-0800";
    ms = ecmascript::JSDate::UtcParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 1605845880000.0);

    str = "Thu 03 Jun 2093 04:18 GMT";
    ms = ecmascript::JSDate::UtcParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), 3894841080000.0);

    str = "Thu 19 Nov 1820 GMT+1232";
    ms = ecmascript::JSDate::UtcParseStringToMs(str);
    EXPECT_EQ(ms.GetDouble(), -4705734720000.0);
}

}  // namespace panda::test
