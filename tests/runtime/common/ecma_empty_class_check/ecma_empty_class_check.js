let foo_no_class = function(n) {
    return "foo_no_class_1";
}

print(foo_no_class());

class FooClass {
    foo_class(){
        return "foo_class";
    }
}

let obj_foo_class = new FooClass();
print(obj_foo_class.foo_class());

foo_no_class = function(n) {
    return "foo_no_class_2";
}

print(foo_no_class());
