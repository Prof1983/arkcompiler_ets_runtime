/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/runtime/tagged_hash_table-inl.h"
#include "plugins/ecmascript/runtime/tagged_hash_table.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

// NOLINTBEGIN(readability-magic-numbers,modernize-avoid-c-arrays)

namespace panda::test {
class NameDictionaryTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

static JSHandle<GlobalEnv> GetGlobalEnv(JSThread *thread)
{
    EcmaVM *ecma = thread->GetEcmaVM();
    return ecma->GetGlobalEnv();
}

TEST_F(NameDictionaryTest, createDictionary)
{
    int num_of_element = 64;
    JSHandle<NameDictionary> dict = NameDictionary::Create(thread_, num_of_element);
    EXPECT_TRUE(*dict != nullptr);
}

TEST_F(NameDictionaryTest, addKeyAndValue)
{
    // mock object needed in test
    int num_of_element = 64;
    JSHandle<NameDictionary> dict_j_shandle(NameDictionary::Create(thread_, num_of_element));
    EXPECT_TRUE(*dict_j_shandle != nullptr);
    JSMutableHandle<NameDictionary> dict_handle(dict_j_shandle);
    JSHandle<JSTaggedValue> obj_fun = GetGlobalEnv(thread_)->GetObjectFunction();

    // create key and values
    JSHandle<JSObject> js_object =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    EXPECT_TRUE(*js_object != nullptr);

    char key_array[] = "hello";
    JSHandle<EcmaString> string_key1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(key_array);
    JSHandle<JSTaggedValue> key1(string_key1);
    JSHandle<JSTaggedValue> taggedkey1(string_key1);
    JSHandle<JSTaggedValue> value1(
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun));
    PropertyAttributes meta_data1;

    char key2_array[] = "hello2";
    JSHandle<EcmaString> string_key2 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(key2_array);
    JSHandle<JSTaggedValue> key2(string_key2);
    JSHandle<JSTaggedValue> taggedkey2(string_key2);
    JSHandle<JSTaggedValue> value2(
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun));
    PropertyAttributes meta_data2;

    // test insert()
    JSHandle<NameDictionary> dict(NameDictionary::PutIfAbsent(thread_, dict_handle, key1, value1, meta_data1));
    dict_handle.Update(dict.GetTaggedValue());
    EXPECT_EQ(dict->EntriesCount(), 1);

    // test find() and lookup()
    int entry1 = dict->FindEntry(key1.GetTaggedValue());
    EXPECT_EQ(key1.GetTaggedValue(), JSTaggedValue(dict->GetKey(entry1).GetRawData()));
    EXPECT_EQ(value1.GetTaggedValue(), JSTaggedValue(dict->GetValue(entry1).GetRawData()));

    JSHandle<NameDictionary> dict2(NameDictionary::PutIfAbsent(thread_, dict_handle, key2, value2, meta_data2));
    EXPECT_EQ(dict2->EntriesCount(), 2);
    // test remove()
    dict = NameDictionary::Remove(thread_, dict_handle, entry1);
    EXPECT_EQ(-1, dict->FindEntry(key1.GetTaggedValue()));
    EXPECT_EQ(dict->EntriesCount(), 1);
}

TEST_F(NameDictionaryTest, GrowCapacity)
{
    int num_of_element = 8;
    JSHandle<NameDictionary> dict_handle(NameDictionary::Create(thread_, num_of_element));
    EXPECT_TRUE(*dict_handle != nullptr);
    JSHandle<JSTaggedValue> obj_fun = GetGlobalEnv(thread_)->GetObjectFunction();
    // create key and values
    JSHandle<JSObject> js_object =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    EXPECT_TRUE(*js_object != nullptr);
    char key_array[7] = "hello";
    for (int i = 0; i < 9; i++) {
        JSHandle<NameDictionary> temp_handle = dict_handle;
        key_array[5] = '1' + i;
        key_array[6] = 0;

        JSHandle<EcmaString> string_key = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(key_array);
        ecmascript::JSHandle<JSTaggedValue> key(string_key);
        JSHandle<JSTaggedValue> key_handle(key);
        ecmascript::JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(i));
        JSHandle<JSTaggedValue> value_handle(value);
        PropertyAttributes meta_data;

        // test insert()
        dict_handle = NameDictionary::PutIfAbsent(thread_, temp_handle, key_handle, value_handle, meta_data);
    }
    EXPECT_EQ(dict_handle->EntriesCount(), 9);
    EXPECT_EQ(dict_handle->Size(), 16);
}

TEST_F(NameDictionaryTest, ShrinkCapacity)
{
    int num_of_element = 64;
    JSMutableHandle<NameDictionary> dict_handle(NameDictionary::Create(thread_, num_of_element));
    EXPECT_TRUE(*dict_handle != nullptr);
    JSHandle<JSTaggedValue> obj_fun = GetGlobalEnv(thread_)->GetObjectFunction();
    // create key and values
    JSHandle<JSObject> js_object =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    EXPECT_TRUE(*js_object != nullptr);
    uint8_t key_array[7] = "hello";

    auto string_table = thread_->GetEcmaVM()->GetEcmaStringTable();
    for (int i = 0; i < 10; i++) {
        key_array[5] = '0' + i;
        key_array[6] = 0;

        JSHandle<JSTaggedValue> key(thread_,
                                    string_table->GetOrInternString(key_array, utf::Mutf8Size(key_array), true));
        JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(i));
        PropertyAttributes meta_data;

        // test insert()
        JSHandle<NameDictionary> new_dict = NameDictionary::PutIfAbsent(thread_, dict_handle, key, value, meta_data);
        dict_handle.Update(new_dict.GetTaggedValue());
    }

    key_array[5] = '2';
    key_array[6] = 0;
    JSHandle<JSTaggedValue> array_handle(thread_,
                                         string_table->GetOrInternString(key_array, utf::Mutf8Size(key_array), true));

    int entry = dict_handle->FindEntry(array_handle.GetTaggedValue());
    EXPECT_NE(entry, -1);

    JSHandle<NameDictionary> new_dict1 = NameDictionary::Remove(thread_, dict_handle, entry);
    dict_handle.Update(new_dict1.GetTaggedValue());
    EXPECT_EQ(dict_handle->EntriesCount(), 9);
    EXPECT_EQ(dict_handle->Size(), 16);
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers,modernize-avoid-c-arrays)
