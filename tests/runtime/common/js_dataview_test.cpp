/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_dataview.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class JSDataViewTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

/*
 * Feature: JSDataView
 * Function: GetElementSize
 * SubFunction: N/A
 * FunctionPoints: Get ElementSize
 * CaseDescription: Check whether the returned value through "GetElementSize" function is within expectations.
 */
TEST_F(JSDataViewTest, GetElementSize)
{
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::INT8), 1);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::UINT8), 1);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::UINT8_CLAMPED), 1);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::INT16), 2);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::UINT16), 2);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::INT32), 4);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::UINT32), 4);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::FLOAT32), 4);
    EXPECT_EQ(JSDataView::GetElementSize(DataViewType::FLOAT64), 8);
}

/*
 * Feature: JSDataView
 * Function: SetDataView
 * SubFunction: GetDataView
 * FunctionPoints: Set DataView
 * CaseDescription: Check whether the returned value through "GetDataView" function is within expectations after
 *                  calling "SetDataView" function.
 */
TEST_F(JSDataViewTest, SetDataView)
{
    EcmaVM *ecma_vm_ptr = thread_->GetEcmaVM();
    ObjectFactory *factory = ecma_vm_ptr->GetFactory();
    JSHandle<GlobalEnv> handle_global_env = ecma_vm_ptr->GetGlobalEnv();

    int32_t length_data_array_buf = 8;
    int32_t offset_data_view = 4;
    int32_t length_data_view = 4;
    JSHandle<JSFunction> handle_func_array_buf(handle_global_env->GetArrayBufferFunction());
    JSHandle<JSTaggedValue> handle_tag_val_func_array_buf(handle_func_array_buf);
    JSHandle<JSArrayBuffer> handle_array_buf(
        factory->NewJSObjectByConstructor(handle_func_array_buf, handle_tag_val_func_array_buf));
    handle_array_buf->SetArrayBufferByteLength(thread_, JSTaggedValue(length_data_array_buf));

    // Call "SetDataView" function through "NewJSDataView" function of "object_factory.cpp"
    JSHandle<JSDataView> handle_data_view =
        factory->NewJSDataView(handle_array_buf, offset_data_view, length_data_view);
    EXPECT_TRUE(handle_data_view->GetDataView().IsTrue());

    // Call "SetDataView" function in this TEST_F.
    handle_data_view->SetDataView(thread_, JSTaggedValue::False());
    EXPECT_TRUE(handle_data_view->GetDataView().IsFalse());
    handle_data_view->SetDataView(thread_, JSTaggedValue::True());
    EXPECT_TRUE(handle_data_view->GetDataView().IsTrue());
}

/*
 * Feature: JSDataView
 * Function: SetViewedArrayBuffer
 * SubFunction: GetViewedArrayBuffer
 * FunctionPoints: Set ViewedArrayBuffer
 * CaseDescription: Check whether the returned value through "GetViewedArrayBuffer" function is within expectations
 *                  after calling "SetViewedArrayBuffer" function.
 */
TEST_F(JSDataViewTest, SetViewedArrayBuffer)
{
    EcmaVM *ecma_vm_ptr = thread_->GetEcmaVM();
    ObjectFactory *factory = ecma_vm_ptr->GetFactory();
    JSHandle<JSFunction> handle_func_array_buf(ecma_vm_ptr->GetGlobalEnv()->GetArrayBufferFunction());
    JSHandle<JSTaggedValue> handle_tag_val_func_array_buf(handle_func_array_buf);

    int32_t length_data_array_buf1 = 8;
    // NOLINTNEXTLINE(readability-magic-numbers)
    int32_t length_data_array_buf2 = 16;
    int32_t offset_data_view = 4;
    int32_t length_data_view = 4;
    JSHandle<JSArrayBuffer> handle_array_buf1(
        factory->NewJSObjectByConstructor(handle_func_array_buf, handle_tag_val_func_array_buf));
    JSHandle<JSArrayBuffer> handle_array_buf2(
        factory->NewJSObjectByConstructor(handle_func_array_buf, handle_tag_val_func_array_buf));
    handle_array_buf1->SetArrayBufferByteLength(thread_, JSTaggedValue(length_data_array_buf1));
    handle_array_buf2->SetArrayBufferByteLength(thread_, JSTaggedValue(length_data_array_buf2));

    // Call "SetViewedArrayBuffer" function through "NewJSDataView" function of "object_factory.cpp"
    JSHandle<JSDataView> handle_data_view =
        factory->NewJSDataView(handle_array_buf1, offset_data_view, length_data_view);
    JSHandle<JSTaggedValue> handle_tag_val_data_view_from1(thread_, handle_array_buf1.GetTaggedValue());
    JSHandle<JSTaggedValue> handle_tag_val_data_view_to1(thread_, handle_data_view->GetViewedArrayBuffer());
    EXPECT_TRUE(JSTaggedValue::Equal(thread_, handle_tag_val_data_view_from1, handle_tag_val_data_view_to1));

    // Call "SetViewedArrayBuffer" function in this TEST_F.
    handle_data_view->SetViewedArrayBuffer(thread_, handle_array_buf2.GetTaggedValue());
    JSHandle<JSTaggedValue> handle_tag_val_data_view_from2(thread_, handle_array_buf2.GetTaggedValue());
    JSHandle<JSTaggedValue> handle_tag_val_data_view_to2(thread_, handle_data_view->GetViewedArrayBuffer());
    EXPECT_TRUE(JSTaggedValue::Equal(thread_, handle_tag_val_data_view_from2, handle_tag_val_data_view_to2));
    EXPECT_FALSE(JSTaggedValue::Equal(thread_, handle_tag_val_data_view_from1, handle_tag_val_data_view_from2));
}

/*
 * Feature: JSDataView
 * Function: SetByteLength
 * SubFunction: GetByteLength
 * FunctionPoints: Set ByteLength
 * CaseDescription: Check whether the returned value through "GetByteLength" function is within expectations after
 *                  calling "SetByteLength" function.
 */
TEST_F(JSDataViewTest, SetByteLength)
{
    EcmaVM *ecma_vm_ptr = thread_->GetEcmaVM();
    ObjectFactory *factory = ecma_vm_ptr->GetFactory();
    JSHandle<JSFunction> handle_func_array_buf(ecma_vm_ptr->GetGlobalEnv()->GetArrayBufferFunction());
    JSHandle<JSTaggedValue> handle_tag_val_func_array_buf(handle_func_array_buf);

    int32_t length_data_array_buf = 8;
    int32_t offset_data_view = 4;
    int32_t length_data_view1 = 4;
    int32_t length_data_view2 = 2;
    JSHandle<JSTaggedValue> handle_tag_val_length_data_view2(thread_, JSTaggedValue(length_data_view2));
    JSHandle<JSArrayBuffer> handle_array_buf(
        factory->NewJSObjectByConstructor(handle_func_array_buf, handle_tag_val_func_array_buf));
    handle_array_buf->SetArrayBufferByteLength(thread_, JSTaggedValue(length_data_array_buf));

    // Call "SetByteLength" function through "NewJSDataView" function of "object_factory.cpp"
    JSHandle<JSDataView> handle_data_view =
        factory->NewJSDataView(handle_array_buf, offset_data_view, length_data_view1);
    EXPECT_EQ(handle_data_view->GetByteLength().GetNumber(), length_data_view1);

    // Call "SetByteLength" function in this TEST_F.
    handle_data_view->SetByteLength(thread_, handle_tag_val_length_data_view2);
    EXPECT_EQ(handle_data_view->GetByteLength().GetNumber(), length_data_view2);
}

/*
 * Feature: JSDataView
 * Function: SetByteOffset
 * SubFunction: GetByteOffset
 * FunctionPoints: Set ByteOffset
 * CaseDescription: Check whether the returned value through "GetByteOffset" function is within expectations after
 *                  calling "SetByteOffset" function.
 */
TEST_F(JSDataViewTest, SetByteOffset)
{
    EcmaVM *ecma_vm_ptr = thread_->GetEcmaVM();
    ObjectFactory *factory = ecma_vm_ptr->GetFactory();
    JSHandle<JSFunction> handle_func_array_buf1(ecma_vm_ptr->GetGlobalEnv()->GetArrayBufferFunction());
    JSHandle<JSTaggedValue> handle_tag_val_func_array_buf1(handle_func_array_buf1);

    int32_t length_data_array_buf = 8;
    int32_t offset_data_view1 = 4;
    int32_t offset_data_view2 = 6;
    int32_t length_data_view = 2;
    JSHandle<JSTaggedValue> handle_tag_val_offset_data_view2(thread_, JSTaggedValue(offset_data_view2));
    JSHandle<JSArrayBuffer> handle_array_buf(
        factory->NewJSObjectByConstructor(handle_func_array_buf1, handle_tag_val_func_array_buf1));
    handle_array_buf->SetArrayBufferByteLength(thread_, JSTaggedValue(length_data_array_buf));

    // Call "SetByteOffset" function through "NewJSDataView" function of "object_factory.cpp"
    JSHandle<JSDataView> handle_data_view =
        factory->NewJSDataView(handle_array_buf, offset_data_view1, length_data_view);
    EXPECT_EQ(handle_data_view->GetByteOffset().GetNumber(), offset_data_view1);

    // Call "SetByteOffset" function in this TEST_F.
    handle_data_view->SetByteOffset(thread_, handle_tag_val_offset_data_view2);
    EXPECT_EQ(handle_data_view->GetByteOffset().GetNumber(), offset_data_view2);
}
}  // namespace panda::test
