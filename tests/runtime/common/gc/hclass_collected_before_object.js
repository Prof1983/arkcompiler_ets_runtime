/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Test that object's hclass collection during GC will not not interfere
 * with next object collection during full GC
 */

function objectCreateAndMove() {
    // create object with setter and getter which more likely to make test fail
    let fObject = {
        get name() {
            return this.name;
        },
        set name(input) {
            this.name = input;
        }
    };
    // start GC to move object to tenured space
    startGC("young");
}
objectCreateAndMove();
// initiate object hclass collection with threshold GC
let gc = startGC("threshold");
waitForFinishGC(gc);
// initiate object itself collection with full GC
gc = startGC("full");
waitForFinishGC(gc);
