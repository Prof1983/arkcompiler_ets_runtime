/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Test getObjectSpaceType builtin.
 */
import { assert } from 'common.abc';

function func() {

}

assert(getObjectSpaceType(new WeakMap()) === 'young', "Must be young space")
assert(getObjectSpaceType(func) === 'young', "Must be young space")
assert(getObjectSpaceType(new Array(1 << 20).fill("A"))  === 'tenured', "Must be tenured space")
assert(getObjectSpaceType('spaceTypeTest') === 'nonMovable', "Must be non movable space")

try {
    throw new Error(getObjectSpaceType(42))
} catch (err) {
    assert(err.message === 'Non Heap Object', "Must be non heap object")
}
