/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Call moving GC when we have pending exception. Check that
 * exception is correctly processed
 * (objects are wrapped with handles)
 */

// create object of error class
let obj = new Object("foo");

// adding a couple Full GC to be run without waiting for its completion
// in order to have GC running during exception processing
startGC("full");
startGC("full");
throw obj;
