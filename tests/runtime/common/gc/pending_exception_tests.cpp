/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

namespace panda::test {
class PendingExceptionTests : public testing::TestWithParam<int> {
public:
    void SetUp() override
    {
        RuntimeOptions options;
        options.SetLoadRuntimes({"ecmascript"});
        options.SetGcType("g1-gc");
        options.SetRunGcInPlace(false);
        options.SetCompilerEnableJit(false);
        options.SetGcTriggerType("debug-never");
        options.SetShouldLoadBootPandaFiles(false);
        bool success = Runtime::Create(options);
        ASSERT_TRUE(success) << "Cannot create Runtime";
    }

    void TearDown() override
    {
        bool success = Runtime::Destroy();
        ASSERT_TRUE(success) << "Cannot destroy Runtime";
    }
};

TEST_P(PendingExceptionTests, MovingGc)
{
    const std::string main_func = "_GLOBAL::func_main_0";
    const std::string file_name = "moving_gc_when_pending_exception.abc";
    auto ret2 = Runtime::GetCurrent()->ExecutePandaFile(file_name.c_str(), main_func.c_str(), {});
    ASSERT_TRUE(ret2.HasValue());
    ASSERT_EQ(ret2.Value(), 1);
}

INSTANTIATE_TEST_SUITE_P(MovingGcTests, PendingExceptionTests, ::testing::Range(0, 8));
}  // namespace panda::test
