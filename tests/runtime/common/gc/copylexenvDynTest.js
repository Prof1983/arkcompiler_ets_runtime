/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Test verify that copylexenvDyn function is correctly works when GC occurs
 * (objects are wrapped with handles)
 */

for (let a = 0; a < 1; a++) {
    scheduleGcAfterNthAlloc(1, "full");
    // To provoke copylexenvDyn call we need create and use in loop function
    // that somehow uses loop variable.
    // Thats because of for update loop implementation
    // according to 14.7.4.2 Runtime Semantics: ForLoopEvaluating from 262 ecma spec 
    function aaa() {
        var x = a;
    }
    aaa();
}