/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Test verify that write PreBarrier works correctly with primitive value
 */

let holder = new Object();
let obj2 = new Array(5).fill('abbcc');
// assign primitive value to object property
holder.prop = 42;
let gc = startGC("threshold", function(marker) {
    // now assign property a reference to heap object
    holder.prop = obj2;
});
waitForFinishGC(gc);