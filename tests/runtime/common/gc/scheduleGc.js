/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Test scheduleGcAfterNthAlloc builtin.
 */
import { assert, newWeakRefInYoung } from 'common.abc';

let ref = newWeakRefInYoung();
scheduleGcAfterNthAlloc(4, "young");
new Object(); // 2 allocations
assert(isScheduledGcTriggered() == false, "Expected scheduled GC is not triggered");
new Object(); // 2 allocations
assert(isScheduledGcTriggered() == true, "Expected scheduled GC is triggered");
assert(ref.deref() == undefined, "Expected WeakRef is undefined")
