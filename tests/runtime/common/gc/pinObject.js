/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { assert, newWeakRefInYoung } from 'common.abc';

let ref = newWeakRefInYoung();
{
    pinObject(ref);
    assert(getObjectSpaceType(ref) === "young", "The object before GC must be placed in young space")
    var addr_before_gc = getObjectAddress(ref);
    // Run GC
    let gc_id = startGC("mixed");
    waitForFinishGC(gc_id);
    var addr_after_gc = getObjectAddress(ref);
    assert(getObjectSpaceType(ref) === "tenured", "The pinned object after GC must be placed in tenured space")
    unpinObject(ref);
}
assert(addr_before_gc === addr_after_gc, "Pinned object must has a fixed address.\n" +
    "Address before gc = " + addr_before_gc + '\n' +
    "Address  after gc = " + addr_after_gc);
