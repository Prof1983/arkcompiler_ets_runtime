/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "test_helper.h"

#include "plugins/ecmascript/runtime/js_for_in_iterator.h"
#include "include/runtime.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_handle.h"

using panda::ecmascript::JSForInIterator;
using panda::ecmascript::JSHandle;
using panda::ecmascript::JSObject;
using panda::ecmascript::JSThread;

namespace panda::test {
class JSForinIteratorTest : public testing::Test {
public:
    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {};

private:
    PandaVM *instance_ {nullptr};
    ecmascript::EcmaHandleScope *scope_ {nullptr};
};

TEST_F(JSForinIteratorTest, Create)
{
    JSHandle<JSObject> null_handle(thread_, JSTaggedValue::Null());
    JSHandle<JSObject> grandfather = JSObject::ObjectCreate(thread_, null_handle);
    EXPECT_TRUE(grandfather->GetPrototype(thread_).IsNull());

    JSHandle<JSObject> father = JSObject::ObjectCreate(thread_, grandfather);

    JSHandle<JSObject> son = JSObject::ObjectCreate(thread_, father);

    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromString("key1"));
    JSHandle<JSTaggedValue> key2(thread_->GetEcmaVM()->GetFactory()->NewFromString("key2"));
    JSHandle<JSTaggedValue> key3(thread_->GetEcmaVM()->GetFactory()->NewFromString("key3"));
    JSHandle<JSTaggedValue> key1_value(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> key2_value(thread_, JSTaggedValue(2));
    JSHandle<JSTaggedValue> key3_value(thread_, JSTaggedValue(3));

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(grandfather), key3, key3_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(father), key2, key2_value);

    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(son), key1, key1_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(son), key2, key1_value);
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(son), key3, key1_value);

    JSHandle<JSForInIterator> it = thread_->GetEcmaVM()->GetFactory()->NewJSForinIterator(JSHandle<JSTaggedValue>(son));
    std::pair<JSTaggedValue, bool> n1 = JSForInIterator::NextInternal(thread_, it);
    EXPECT_EQ(n1.first, key1.GetTaggedValue());
    EXPECT_FALSE(n1.second);

    std::pair<JSTaggedValue, bool> n2 = JSForInIterator::NextInternal(thread_, it);
    EXPECT_EQ(n2.first, key2.GetTaggedValue());
    EXPECT_FALSE(n2.second);

    std::pair<JSTaggedValue, bool> n3 = JSForInIterator::NextInternal(thread_, it);
    EXPECT_EQ(n3.first, key3.GetTaggedValue());
    EXPECT_FALSE(n3.second);

    std::pair<JSTaggedValue, bool> n4 = JSForInIterator::NextInternal(thread_, it);
    EXPECT_EQ(n4.first, JSTaggedValue::Undefined());
    EXPECT_TRUE(n4.second);
}
}  // namespace panda::test
