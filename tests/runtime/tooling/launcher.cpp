/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "runtime/include/runtime.h"
#include "test_list.h"
#include "runtime/tests/tooling/test_extractor.h"
#include "generated/base_options.h"

namespace panda::tooling::test {
extern void SetExtractorFactoryForTest(TestExtractorFactory *test_extractor);

class BaseDebugApiTest : public testing::TestWithParam<const char *> {
public:
    NO_COPY_SEMANTIC(BaseDebugApiTest);
    NO_MOVE_SEMANTIC(BaseDebugApiTest);

    ~BaseDebugApiTest() override = default;
    BaseDebugApiTest() = default;

protected:
    void RunTest(RuntimeOptions &options, const char *test_name) const
    {
        std::cout << "Running " << test_name << std::endl;
        SetCurrentTestName(test_name);
        auto *factory = new TestExtractorFactory();
        SetExtractorFactoryForTest(factory);
        Logger::Initialize(base_options::Options(""));
        auto [pandaFile, entryPoint] = GetTestEntryPoint(test_name);
        auto boot_files = options.GetBootPandaFiles();
        boot_files.push_back(pandaFile);
        options.SetBootPandaFiles(boot_files);
        ASSERT_TRUE(Runtime::Create(options)) << test_name;
        auto res = Runtime::GetCurrent()->ExecutePandaFile(pandaFile, entryPoint, {});
        ASSERT_TRUE(res.HasValue());
        delete factory;
        ASSERT_TRUE(Runtime::Destroy());
    }
};

class EcmaScriptDebugApiTest : public BaseDebugApiTest {
protected:
    void RunEcmaScriptTest(const char *test_name) const
    {
        RuntimeOptions options;
        options.SetDebuggerLibraryPath(DEBUG_LIBRARY_PATH);
        options.SetBootPandaFiles({PANDA_STD_LIB});
        options.SetLoadRuntimes({"ecmascript"});
        options.SetRunGcInPlace(true);
        RunTest(options, test_name);
    }
};

TEST_P(EcmaScriptDebugApiTest, EcmaScriptSuite)
{
    const char *test_name = GetParam();
    RunEcmaScriptTest(test_name);
}

INSTANTIATE_TEST_SUITE_P(DebugApiTests, EcmaScriptDebugApiTest,
                         ::testing::ValuesIn(GetTestList(panda_file::SourceLang::ECMASCRIPT)),
                         [](const testing::TestParamInfo<EcmaScriptDebugApiTest::ParamType> &einfo) {
                             return einfo.param;
                         });

}  // namespace panda::tooling::test
