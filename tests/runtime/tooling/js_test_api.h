/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_PLUGINS_ECMASCRIPT_TESTS_RUNTIME_TOOLING_JS_TEST_API_H
#define PANDA_PLUGINS_ECMASCRIPT_TESTS_RUNTIME_TOOLING_JS_TEST_API_H

#include "runtime/include/tooling/debug_interface.h"
#include "plugins/ecmascript/runtime/include/tooling/pt_ecmascript_extension.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/global_env.h"

// NOLINTBEGIN(readability-redundant-string-cstr)

namespace panda::tooling::ecmascript {
using JSThread = panda::ecmascript::JSThread;
class JSTestApi {
public:
    static VRegValue StringToVRegValue(const PandaString &value)
    {
        JSThread *js_thread = JSThread::Cast(JSThread::GetCurrent());
        [[maybe_unused]] panda::ecmascript::EcmaHandleScope handle_scope(js_thread);
        auto ecma_vm = js_thread->GetEcmaVM();

        panda::ecmascript::ObjectFactory *factory = ecma_vm->GetFactory();
        panda::ecmascript::JSHandle<panda::ecmascript::JSTaggedValue> str_handle(
            factory->NewFromStdString(value.c_str()));

        return PtEcmaScriptExtension::TaggedValueToVRegValue(str_handle.GetTaggedValue());
    }
    static bool VRegValueToString(VRegValue value, PandaString *out)
    {
        panda::ecmascript::EcmaString *string =
            panda::ecmascript::EcmaString::Cast(PtEcmaScriptExtension::VRegValueToTaggedValue(value).GetHeapObject());

        *out = panda::ecmascript::base::StringHelper::ToStdString(string).c_str();
        return true;
    }
};
}  // namespace panda::tooling::ecmascript

// NOLINTEND(readability-redundant-string-cstr)

#endif  // PANDA_PLUGINS_ECMASCRIPT_TESTS_RUNTIME_TOOLING_JS_TEST_API_H
