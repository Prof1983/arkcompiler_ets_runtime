/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_list.h"

#include "api_tests/api_tests.h"
#include "test_util.h"

namespace panda::tooling::test {
static const char *G_CURRENT_TEST_NAME = nullptr;

static void RegisterTests()
{
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsBreakpoint", GetJsBreakpointTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsSingleStepTest", GetJsSingleStepTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsVMEvents", GetJsVmEventTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsMethodEvent", GetJsMethodEventTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsGetVariable", GetJsGetVariableTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsSetVariable", GetJsSetVariableTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsGetCurrentFrame", GetJsCurrentFrameTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsEnumerateFrames", JsEnumerateFramesTest());
    // TODO(maksenov): Enable tests with thread suspension for JS
    // TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsFramePopNotification", GetJsFramePopTest());
    // TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsRestartFrame", GetJsRestartFrameTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsSetNotification", GetJsSetNotificationTest());
    TestUtil::RegisterTest(panda_file::SourceLang::ECMASCRIPT, "JsExceptionCatchThrowEvents",
                           GetJsExceptionEventTest());
}

std::vector<const char *> GetTestList(panda_file::SourceLang language)
{
    RegisterTests();
    std::vector<const char *> res;
    auto &tests = TestUtil::GetTests();
    auto language_it = tests.find(language);
    if (language_it == tests.end()) {
        return {};
    }

    for (const auto &entry : language_it->second) {
        res.push_back(entry.first);
    }
    return res;
}

void SetCurrentTestName(const char *test_name)
{
    G_CURRENT_TEST_NAME = test_name;
}

const char *GetCurrentTestName()
{
    return G_CURRENT_TEST_NAME;
}

std::pair<const char *, const char *> GetTestEntryPoint(const char *test_name)
{
    return TestUtil::GetTest(test_name)->GetEntryPoint();
}
}  // namespace panda::tooling::test
