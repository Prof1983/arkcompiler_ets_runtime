/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef PANDA_RUNTIME_DEBUG_TEST_TEST_LIST_H_
#define PANDA_RUNTIME_DEBUG_TEST_TEST_LIST_H_

#include <utility>
#include <vector>

#include "libpandafile/file_items.h"

namespace panda::tooling::test {
std::vector<const char *> GetTestList(panda_file::SourceLang language);

void SetCurrentTestName(const char *test_name);

std::pair<const char *, const char *> GetTestEntryPoint(const char *test_name);
}  // namespace panda::tooling::test

#endif  // PANDA_RUNTIME_DEBUG_TEST_TEST_LIST_H_
