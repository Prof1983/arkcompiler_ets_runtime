/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "js/js_method_event_test.h"
#include "js/js_breakpoint_test.h"
#include "js/js_get_current_frame_test.h"
#include "js/js_enumerate_frames_test.h"
#include "js/js_frame_pop_test.h"
#include "js/js_single_step_test.h"
#include "js/js_restart_frame_test.h"
#include "js/js_vm_event_test.h"
#include "js/js_set_notification_test.h"
#include "js/js_exception_events_test.h"
#include "js/js_set_variable_test.h"
#include "js/js_get_variable_test.h"
