/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_BREAKPOINT_TEST_H
#define PANDA_TOOLING_TEST_JS_BREAKPOINT_TEST_H

#include "test_util.h"

namespace panda::tooling::test {
class JsBreakpointTest : public ApiTest {
public:
    JsBreakpointTest()
    {
        vm_start = [this] {
            // NOLINTNEXTLINE(readability-magic-numbers)
            location_ = TestUtil::GetLocation("Sample.js", 22, panda_file_.c_str());
            ASSERT_TRUE(location_.GetMethodId().IsValid());
            return true;
        };

        breakpoint = [this](PtThread, Method *, const PtLocation &location) {
            ASSERT_TRUE(location.GetMethodId().IsValid());
            ASSERT_LOCATION_EQ(location, location_);
            ++breakpoint_counter_;
            return true;
        };

        load_module = [this](std::string_view module_name) {
            if (module_name.find(panda_file_.c_str()) == std::string_view::npos) {
                return true;
            }
            ASSERT_SUCCESS(debug_interface->SetBreakpoint(location_));
            auto error = debug_interface->SetBreakpoint(location_);
            ASSERT_FALSE(!error);
            return true;
        };

        vm_death = [this]() {
            ASSERT_EQ(breakpoint_counter_, 2U);
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    std::string panda_file_ = "js/Sample.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";
    PtLocation location_ {nullptr, PtLocation::EntityId(0), 0};
    size_t breakpoint_counter_ = 0;
};

inline std::unique_ptr<ApiTest> GetJsBreakpointTest()
{
    return std::make_unique<JsBreakpointTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_BREAKPOINT_TEST_H
