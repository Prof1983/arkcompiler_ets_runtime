/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_VM_EVENT_TEST_H
#define PANDA_TOOLING_TEST_JS_VM_EVENT_TEST_H

#include "test_util.h"

namespace panda::tooling::test {
class JsVmEventTest : public ApiTest {
public:
    JsVmEventTest()
    {
        vm_start = [this]() {
            start_counter_++;
            return true;
        };

        vm_init = [this](PtThread thread) {
            init_thread_ = thread.GetId();
            init_counter_++;
            return true;
        };

        vm_death = [this]() {
            death_counter_++;
            ASSERT_NE(init_thread_, PtThread::NONE.GetId());
            ASSERT_EQ(start_counter_, 1U);
            ASSERT_EQ(init_counter_, 1U);
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    std::string panda_file_ = "js/Sample.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";
    size_t start_counter_ = 0;
    size_t init_counter_ = 0;
    size_t death_counter_ = 0;
    uint32_t init_thread_ = PtThread::NONE.GetId();
};

inline std::unique_ptr<ApiTest> GetJsVmEventTest()
{
    return std::make_unique<JsVmEventTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_VM_EVENT_TEST_H
