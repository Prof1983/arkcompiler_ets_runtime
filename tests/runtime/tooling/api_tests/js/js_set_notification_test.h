/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_SET_NOTIFICATION_TEST_H
#define PANDA_TOOLING_TEST_JS_SET_NOTIFICATION_TEST_H

#include "test_util.h"
#include "runtime/include/tooling/debug_interface.h"

namespace panda::tooling::test {
class JsSetNotificationTest : public ApiTest {
public:
    JsSetNotificationTest()
    {
        vm_death = [this]() {
            ASSERT_EQ(nn_, 5);
            ASSERT_EQ(entry_, 3);
            ASSERT_EQ(exit_, 5);
            return true;
        };

        method_entry = [this](PtThread /* thread */, Method *method) {
            if (method->GetFullName() == "_GLOBAL::func__2") {
                ++entry_;
            }
            return true;
        };

        method_exit = [this](PtThread thread, Method *method, bool, VRegValue) {
            if (method->GetFullName() == "_GLOBAL::func__2") {
                ++exit_;
                ++nn_;

                // NN |  hook | action | counter value
                // ---+-------+--------+---------------
                //  1 | entry |   +=1  |       1
                //    |  exit |   +=1  |       1
                //  2 | entry |  skip  |       1
                //    |  exit |   +=1  |       2
                //  3 | entry |   +=1  |       2
                //    |  exit |   +=1  |       3
                //  4 | entry |  skip  |       2
                //    |  exit |   +=1  |       4
                //  5 | entry |   +=1  |       3
                //    |  exit |   +=1  |       5

                if (nn_ == 1) {
                    ASSERT_EQ(entry_, 1);
                    ASSERT_EQ(exit_, 1);

                    // Disable the entry hook globaly
                    debug_interface->SetNotification(PtThread::NONE, false, PtHookType::PT_HOOK_TYPE_METHOD_ENTRY);
                } else if (nn_ == 2) {
                    ASSERT_EQ(entry_, 1);
                    ASSERT_EQ(exit_, 2);

                    // Enable the entry hook locally
                    debug_interface->SetNotification(thread, true, PtHookType::PT_HOOK_TYPE_METHOD_ENTRY);
                } else if (nn_ == 3) {
                    ASSERT_EQ(entry_, 2);
                    ASSERT_EQ(exit_, 3);

                    // Disable the entry hook locally
                    debug_interface->SetNotification(thread, false, PtHookType::PT_HOOK_TYPE_METHOD_ENTRY);
                } else if (nn_ == 4) {
                    ASSERT_EQ(entry_, 2);
                    ASSERT_EQ(exit_, 4);

                    // Enable the entry hook globaly
                    debug_interface->SetNotification(PtThread::NONE, true, PtHookType::PT_HOOK_TYPE_METHOD_ENTRY);
                } else if (nn_ == 5) {
                    ASSERT_EQ(entry_, 3);
                    ASSERT_EQ(exit_, 5);
                }
            }
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {"js/SetNotification.abc", "_GLOBAL::func_main_0"};
    }

private:
    int nn_ = 0;
    int entry_ = 0;
    int exit_ = 0;
};

inline std::unique_ptr<ApiTest> GetJsSetNotificationTest()
{
    return std::make_unique<JsSetNotificationTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_SET_NOTIFICATION_TEST_H
