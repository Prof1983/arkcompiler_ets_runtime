/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_ENUMERATE_FRAMES_TEST_H
#define PANDA_TOOLING_TEST_JS_ENUMERATE_FRAMES_TEST_H

#include "test_util.h"

namespace panda::tooling::test {
class JsEnumerateFrameTest : public ApiTest {
public:
    JsEnumerateFrameTest()
    {
        vm_death = [this]() {
            ASSERT_EQ(count_frames_, 9U);
            return true;
        };

        method_exit = [this](PtThread thread, Method *method, bool, VRegValue) {
            if (method->GetFullName() == "_GLOBAL::func_method3_3") {
                ASSERT_EQ(count_frames_, 0U);
                debug_interface->EnumerateFrames(thread, callback_);
                ASSERT_EQ(count_frames_, 4U);
            } else if (method->GetFullName() == "_GLOBAL::func_method2_2") {
                ASSERT_EQ(count_frames_, 4U);
                debug_interface->EnumerateFrames(thread, callback_);
                ASSERT_EQ(count_frames_, 7U);
            } else if (method->GetFullName() == "_GLOBAL::func_method1_1") {
                ASSERT_EQ(count_frames_, 7U);
                debug_interface->EnumerateFrames(thread, callback_);
                ASSERT_EQ(count_frames_, 9U);
            }
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    std::string panda_file_ = "js/GetFrame.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";
    size_t count_frames_ = 0;

    std::function<bool(const PtFrame &debug_frame)> callback_ {[this](const PtFrame &debug_frame) {
        this->count_frames_++;
        return debug_frame.GetArgumentNum() != 1;
    }};
};

inline std::unique_ptr<ApiTest> JsEnumerateFramesTest()
{
    return std::make_unique<JsEnumerateFrameTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_ENUMERATE_FRAMES_TEST_H
