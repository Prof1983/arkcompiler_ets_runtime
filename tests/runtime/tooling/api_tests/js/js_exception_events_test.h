/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_EXCEPTION_EVENTS_TEST_H
#define PANDA_TOOLING_TEST_JS_EXCEPTION_EVENTS_TEST_H

#include "test_util.h"
#include "runtime/include/tooling/pt_location.h"

namespace panda::tooling::test {
class JsExceptionEventTest : public ApiTest {
public:
    JsExceptionEventTest()
    {
        vm_death = [this]() {
            ASSERT_EQ(exception_counter_, 1U);
            ASSERT_EQ(exception_catch_counter_, 1U);
            return true;
        };

        method_entry = [this](PtThread thread, Method *method) {
            if (method->GetFullName() == "_GLOBAL::func_main_0") {
                ASSERT_TRUE(thread_id_ == PtThread::NONE.GetId());
                thread_id_ = thread.GetId();
            }
            return true;
        };

        method_exit = [this](PtThread, Method *method, bool, VRegValue) {
            if (method->GetFullName() == "_GLOBAL::func_main_0") {
                thread_id_ = PtThread::NONE.GetId();
            }
            return true;
        };

        exception = [this](PtThread thread, Method *, const PtLocation &, ObjectHeader *, Method *,
                           const PtLocation &catch_location) {
            if (thread_id_ != thread.GetId()) {
                return true;
            }

            ++exception_counter_;
            catch_location_ = catch_location;
            return true;
        };

        exception_catch = [this](PtThread thread, Method *, const PtLocation &location, ObjectHeader *) {
            if (thread_id_ != thread.GetId()) {
                return true;
            }

            ASSERT_LOCATION_EQ(location, catch_location_);
            ++exception_catch_counter_;
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    std::string panda_file_ = "js/ExceptionTest.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";
    size_t exception_counter_ = 0;
    size_t exception_catch_counter_ = 0;
    uint32_t thread_id_ = PtThread::NONE.GetId();
    PtLocation catch_location_ {nullptr, PtLocation::EntityId(0), 0};
};

inline std::unique_ptr<ApiTest> GetJsExceptionEventTest()
{
    return std::make_unique<JsExceptionEventTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_EXCEPTION_EVENTS_TEST_H
