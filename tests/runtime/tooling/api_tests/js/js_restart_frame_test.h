/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_SET_RESTART_FRAME_TEST_H
#define PANDA_TOOLING_TEST_JS_SET_RESTART_FRAME_TEST_H

#include "test_util.h"
#include "plugins/ecmascript/runtime/include/tooling/pt_ecmascript_extension.h"

namespace panda::tooling::test {
using JSTaggedValue = panda::ecmascript::JSTaggedValue;

class JsRestartFrameTest : public ApiTest {
public:
    JsRestartFrameTest()
    {
        vm_start = [this] {
            // NOLINTNEXTLINE(readability-magic-numbers)
            location_ = TestUtil::GetLocation("RestartFrame.js", 35, panda_file_.c_str());
            ASSERT_TRUE(location_.GetMethodId().IsValid());
            return true;
        };

        breakpoint = [this](PtThread thread, Method *, const PtLocation &location) {
            ASSERT_TRUE(location.GetMethodId().IsValid());
            ASSERT_LOCATION_EQ(location, location_);
            ++breakpoint_counter_;
            TestUtil::SuspendUntilContinue(DebugEvent::BREAKPOINT, thread, location);
            if (breakpoint_counter_ == 1) {
                ASSERT_SUCCESS(debug_interface->RestartFrame(thread, 2));
            }
            if (breakpoint_counter_ == 2) {
                ASSERT_SUCCESS(debug_interface->RestartFrame(thread, 0));
            }
            return true;
        };

        load_module = [this](std::string_view module_name) {
            if (module_name.find(panda_file_.c_str()) == std::string_view::npos) {
                return true;
            }
            ASSERT_SUCCESS(debug_interface->SetBreakpoint(location_));
            return true;
        };

        method_entry = [this](PtThread, Method *) {
            ++entry_exit_counter_;
            return true;
        };

        method_exit = [this](PtThread, Method *method, bool, VRegValue val) {
            auto module_name = method->GetFullName();
            if (module_name == "_GLOBAL::func_2") {
                // Force exit always zero
                auto tagged_value = ecmascript::PtEcmaScriptExtension::VRegValueToTaggedValue(val);
                if (tagged_value.IsInt()) {
                    result_ = tagged_value.GetInt();
                }
            }

            --entry_exit_counter_;
            return true;
        };

        scenario = [this]() {
            ASSERT_BREAKPOINT_SUCCESS(location_);
            TestUtil::Continue();

            ASSERT_BREAKPOINT_SUCCESS(location_);
            TestUtil::Continue();

            ASSERT_BREAKPOINT_SUCCESS(location_);
            TestUtil::Continue();

            ASSERT_EXITED();
            return true;
        };

        vm_death = [this] {
            // result_ indicate count of calls
            // frame0 +1
            // frame1 +10
            // frame2 +100    +100000
            // frame3 +1000   +100000
            // frame4 +10000  +100000
            ASSERT_EQ(result_, 322311);
            ASSERT_EQ(entry_exit_counter_, 0U);
            ASSERT_EQ(breakpoint_counter_, 3U);

            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    std::string panda_file_ = "js/RestartFrame.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";
    PtLocation location_ {nullptr, PtLocation::EntityId(0), 0};
    size_t breakpoint_counter_ = 0;
    size_t entry_exit_counter_ = 0;
    int64_t result_ = 0;
};

inline std::unique_ptr<ApiTest> GetJsRestartFrameTest()
{
    return std::make_unique<JsRestartFrameTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_SET_RESTART_FRAME_TEST_H
