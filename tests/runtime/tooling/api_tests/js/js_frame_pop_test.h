/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_FRAME_POP_TEST_H
#define PANDA_TOOLING_TEST_JS_FRAME_POP_TEST_H

#include "test_util.h"

namespace panda::tooling::test {
class JsFramePopTest : public ApiTest {
public:
    JsFramePopTest()
    {
        vm_start = [this] {
            // NOLINTNEXTLINE(readability-magic-numbers)
            location_ = TestUtil::GetLocation("FramePop.js", 33, panda_file_.c_str());
            ASSERT_TRUE(location_.GetMethodId().IsValid());
            return true;
        };

        vm_death = [this] {
            ASSERT_EQ(breakpoint_counter_, 2U);
            ASSERT_EQ(frame_pop_counter_, 2U);
            return true;
        };

        scenario = [this]() {
            PtThread suspended = TestUtil::WaitForBreakpoint(location_);
            ASSERT_NE(suspended.GetId(), PtThread::NONE.GetId());

            TestUtil::Continue();

            suspended = TestUtil::WaitForBreakpoint(location_);
            ASSERT_NE(suspended.GetId(), PtThread::NONE.GetId());

            // No more notifications
            TestUtil::Continue();

            ASSERT_EXITED();
            return true;
        };

        breakpoint = [this](PtThread thread, Method *, const PtLocation &location) {
            ASSERT_TRUE(location.GetMethodId().IsValid());
            ASSERT_LOCATION_EQ(location, location_);
            ++breakpoint_counter_;
            TestUtil::SuspendUntilContinue(DebugEvent::BREAKPOINT, thread, location);
            if (breakpoint_counter_ == 1) {
                ASSERT_SUCCESS(debug_interface->NotifyFramePop(thread, 0));
                ASSERT_SUCCESS(debug_interface->NotifyFramePop(thread, 2));
            }
            return true;
        };

        load_module = [this](std::string_view module_name) {
            if (module_name.find(panda_file_.c_str()) == std::string_view::npos) {
                return true;
            }
            ASSERT_SUCCESS(debug_interface->SetBreakpoint(location_));
            return true;
        };

        frame_pop = [this](PtThread thread_id, Method *method, bool was_popped_by_exception) {
            auto method_name = method->GetFullName();
            if (method_name == "_GLOBAL::func_frame0_4") {
                ASSERT_EQ(frame_pop_counter_, 0U);
            } else if (method_name == "_GLOBAL::func_frame2_2") {
                ASSERT_EQ(frame_pop_counter_, 1U);
            } else {
                // Not expected frame pop
                return false;
            }

            ASSERT_NE(thread_id.GetId(), PtThread::NONE.GetId());
            ASSERT_EQ(was_popped_by_exception, false);
            ++frame_pop_counter_;
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    std::string panda_file_ = "js/FramePop.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";
    PtLocation location_ {nullptr, PtLocation::EntityId(0), 0};
    size_t breakpoint_counter_ = 0;
    size_t frame_pop_counter_ = 0;
    PtMethod pop_method_ {nullptr};
};

inline std::unique_ptr<ApiTest> GetJsFramePopTest()
{
    return std::make_unique<JsFramePopTest>();
}

}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_FRAME_POP_TEST_H
