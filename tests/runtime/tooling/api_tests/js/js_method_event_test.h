/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_METHOD_EVENT_TEST_H
#define PANDA_TOOLING_TEST_JS_METHOD_EVENT_TEST_H

#include "test_util.h"

namespace panda::tooling::test {
class JsMethodEventTest : public ApiTest {
public:
    JsMethodEventTest()
    {
        vm_death = [this]() {
            ASSERT_EQ(method_entry_exit_count_, 0);
            ASSERT_NE(method_entry_count_, 0);
            return true;
        };

        method_entry = [this](PtThread, Method *) {
            method_entry_exit_count_++;
            method_entry_count_++;
            return true;
        };

        method_exit = [this](PtThread, Method *, bool, VRegValue) {
            method_entry_exit_count_--;
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    std::string panda_file_ = "js/Sample.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";

    int method_entry_exit_count_ = 0;
    int method_entry_count_ = 0;
};

inline std::unique_ptr<ApiTest> GetJsMethodEventTest()
{
    return std::make_unique<JsMethodEventTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_METHOD_EVENT_TEST_H
