/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_GET_VARIABLE_TEST_H
#define PANDA_TOOLING_TEST_JS_GET_VARIABLE_TEST_H

#include "test_util.h"
#include "plugins/ecmascript/runtime/include/tooling/pt_ecmascript_extension.h"
#include "plugins/ecmascript/tests/runtime/tooling/js_test_api.h"

#include <climits>
#include <cfloat>

namespace panda::tooling::test {
using JSTaggedValue = panda::ecmascript::JSTaggedValue;

class JsGetVariableTest : public ApiTest {
public:
    JsGetVariableTest()
    {
        vm_death = [this]() {
            const int expected_count = 1;
            ASSERT_EQ(count_checker_, expected_count);
            return true;
        };

        load_module = [this](std::string_view module_name) {
            if (module_name.find(panda_file_.c_str()) == std::string_view::npos) {
                return true;
            }
            // NOLINTNEXTLINE(readability-magic-numbers)
            auto location = TestUtil::GetLocation("GetVariable.js", 19, panda_file_.c_str());  // getVariable
            ASSERT_TRUE(location.GetMethodId().IsValid());
            ASSERT_SUCCESS(debug_interface->SetBreakpoint(location));
            return true;
        };

        breakpoint = [this](PtThread thread, Method *method, const PtLocation &location) {
            ASSERT_TRUE(CheckSetValues(thread, method, location.GetBytecodeOffset()));
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

    bool CheckSetValues(PtThread thread, Method *method, uint32_t offset)
    {
        // NOLINTNEXTLINE(modernize-avoid-c-arrays)
        std::function<bool(JSTaggedValue)> checkers_list[] = {
            [&](JSTaggedValue value) {  // boolean: false
                ASSERT_EQ(value.GetRawData(), JSTaggedValue::VALUE_FALSE);
                return true;
            },
            [&](JSTaggedValue value) {  // boolean: true
                ASSERT_EQ(value.GetRawData(), JSTaggedValue::VALUE_TRUE);
                return true;
            },
            [&](JSTaggedValue value) {  // int: -2147483648 (INT_MIN)
                ASSERT_TRUE(value.IsInteger());
                ASSERT_EQ(value.GetDouble(), INT_MIN);
                return true;
            },
            [&](JSTaggedValue value) {  // int: 2147483647 (INT_MAX)
                ASSERT_TRUE(value.IsInteger());
                ASSERT_EQ(value.GetInt(), INT_MAX);
                return true;
            },
            [&](JSTaggedValue value) {  // double: 1.5
                ASSERT_TRUE(value.IsDouble());
                // NOLINTNEXTLINE(readability-magic-numbers)
                ASSERT_EQ(value.GetDouble(), double(1.5));  // 1.5
                return true;
            },
            [&](JSTaggedValue value) {  // String: "new_string"
                PandaString checked_value;
                ASSERT_TRUE(value.IsHeapObject());
                auto vreg_value = ecmascript::PtEcmaScriptExtension::TaggedValueToVRegValue(value);
                ASSERT_TRUE(ecmascript::JSTestApi::VRegValueToString(vreg_value, &checked_value));
                // NOLINTNEXTLINE(readability-string-compare)
                ASSERT_TRUE(checked_value.compare("new_string") == 0);
                return true;
            }};

        ++count_checker_;
        uint32_t frame_depth = 0;
        int32_t arg_number = TestUtil::GetValueRegister(method, "bl1", offset);
        for (const auto &checker : checkers_list) {
            VRegValue vreg_value;
            ASSERT_SUCCESS(debug_interface->GetVariable(thread, frame_depth, arg_number++, &vreg_value));
            ASSERT_TRUE(checker(ecmascript::PtEcmaScriptExtension::VRegValueToTaggedValue(vreg_value)));
        }

        return true;
    }

private:
    std::string panda_file_ = "js/GetVariable.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";

    int count_checker_ = 0;
};

inline std::unique_ptr<ApiTest> GetJsGetVariableTest()
{
    return std::make_unique<JsGetVariableTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_GET_VARIABLE_TEST_H
