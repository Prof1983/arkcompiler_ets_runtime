/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_TEST_JS_SET_VARIABLE_TEST_H
#define PANDA_TOOLING_TEST_JS_SET_VARIABLE_TEST_H

#include "test_util.h"
#include "plugins/ecmascript/runtime/include/tooling/pt_ecmascript_extension.h"
#include "plugins/ecmascript/tests/runtime/tooling/js_test_api.h"
#include "runtime/include/mem/panda_string.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/global_env.h"

namespace panda::tooling::test {
using JSTaggedValue = panda::ecmascript::JSTaggedValue;
using JSThread = panda::ecmascript::JSThread;
class JsSetVariableTest : public ApiTest {
public:
    JsSetVariableTest()
    {
        vm_death = [this]() {
            ASSERT_TRUE(checked_);
            ASSERT_EQ(breakpoint_count_, 4);
            return true;
        };

        load_module = [this](std::string_view module_name) {
            if (module_name.find(panda_file_.c_str()) == std::string_view::npos) {
                return true;
            }
            // NOLINTNEXTLINE(readability-magic-numbers)
            SetBreakpoint(22);  // setBoolean
            // NOLINTNEXTLINE(readability-magic-numbers)
            SetBreakpoint(26);  // setInt
            // NOLINTNEXTLINE(readability-magic-numbers)
            SetBreakpoint(30);  // setDouble
            // NOLINTNEXTLINE(readability-magic-numbers)
            SetBreakpoint(34);  // setString
            return true;
        };

        // NOLINTBEGIN(readability-magic-numbers)
        breakpoint = [this](PtThread thread, Method *method, PtLocation location) {
            breakpoint_count_ += 1;
            auto method_name = method->GetFullName();
            int frame_depth = 0;
            uint32_t curr_offset = location.GetBytecodeOffset();
            if (method_name == "_GLOBAL::func_setBoolean_1") {
                VRegValue value = ecmascript::PtEcmaScriptExtension::TaggedValueToVRegValue(JSTaggedValue(true));
                ASSERT_SUCCESS(debug_interface->SetVariable(
                    thread, frame_depth, TestUtil::GetValueRegister(method, "value", curr_offset), value));
            } else if (method_name == "_GLOBAL::func_setInt_2") {
                VRegValue value =
                    ecmascript::PtEcmaScriptExtension::TaggedValueToVRegValue(JSTaggedValue(123456789));  // 123456789
                ASSERT_SUCCESS(debug_interface->SetVariable(
                    thread, frame_depth, TestUtil::GetValueRegister(method, "value", curr_offset), value));
            } else if (method_name == "_GLOBAL::func_setDouble_3") {
                VRegValue value =
                    ecmascript::PtEcmaScriptExtension::TaggedValueToVRegValue(JSTaggedValue(12345.6789));  // 12345.6789
                ASSERT_SUCCESS(debug_interface->SetVariable(
                    thread, frame_depth, TestUtil::GetValueRegister(method, "value", curr_offset), value));
            } else if (method_name == "_GLOBAL::func_setString_4") {
                VRegValue value = ecmascript::JSTestApi::StringToVRegValue("x2348x");
                ASSERT_SUCCESS(debug_interface->SetVariable(
                    thread, frame_depth, TestUtil::GetValueRegister(method, "value", curr_offset), value));
            }
            return true;
        };
        // NOLINTEND(readability-magic-numbers)

        method_entry = [this](PtThread thread, Method *method) {
            auto method_name = method->GetFullName();
            if (method_name == "_GLOBAL::func_checkData_5") {
                checked_ = CheckData(thread, method);
            }
            return true;
        };
    }

    std::pair<const char *, const char *> GetEntryPoint() override
    {
        return {panda_file_.c_str(), entry_point_.c_str()};
    }

private:
    static bool GetGlobalVariable(const PandaString &name, VRegValue *out)
    {
        JSThread *js_thread = JSThread::Cast(JSThread::GetCurrent());
        auto ecma_vm = js_thread->GetEcmaVM();
        panda::ecmascript::JSHandle<panda::ecmascript::GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
        auto global_object = global_env->GetGlobalObject();

        panda::ecmascript::ObjectFactory *factory = ecma_vm->GetFactory();
        // NOLINTBEGIN(readability-redundant-string-cstr)
        panda::ecmascript::JSHandle<panda::ecmascript::JSTaggedValue> exec_handle(
            factory->NewFromStdString(name.c_str()));
        // NOLINTEND(readability-redundant-string-cstr)
        panda::ecmascript::JSHandle<panda::ecmascript::JSObject> object_handle(js_thread, global_object);
        if (!panda::ecmascript::JSObject::HasProperty(js_thread, object_handle, exec_handle)) {
            return false;
        }

        auto property = panda::ecmascript::JSObject::GetProperty(js_thread, object_handle, exec_handle);
        *out = ecmascript::PtEcmaScriptExtension::TaggedValueToVRegValue(property.GetValue().GetTaggedValue());
        return true;
    }

    bool CheckData(PtThread /*unused*/, Method * /*unused*/)
    {
        {
            VRegValue pt_bool;
            ASSERT_TRUE(GetGlobalVariable("boolData", &pt_bool));
            ASSERT_EQ(ecmascript::PtEcmaScriptExtension::VRegValueToTaggedValue(pt_bool).GetRawData(),
                      JSTaggedValue::VALUE_TRUE);
        }
        {
            VRegValue pt_int;
            const int expected_value = 123456789;
            ASSERT_TRUE(GetGlobalVariable("intData", &pt_int));
            int int_value = ecmascript::PtEcmaScriptExtension::VRegValueToTaggedValue(pt_int).GetInt();
            ASSERT_EQ(int_value, expected_value);
        }
        {
            VRegValue pt_double;
            const double expected_value = 12345.6789;
            ASSERT_TRUE(GetGlobalVariable("doubleData", &pt_double));
            double double_value = ecmascript::PtEcmaScriptExtension::VRegValueToTaggedValue(pt_double).GetDouble();
            ASSERT_EQ(double_value, expected_value);
        }
        {
            VRegValue pt_string;
            PandaString string_value;
            const PandaString expected_value = "x2348x";
            ASSERT_TRUE(GetGlobalVariable("stringData", &pt_string));
            ASSERT_TRUE(ecmascript::JSTestApi::VRegValueToString(pt_string, &string_value));
            // NOLINTNEXTLINE(readability-string-compare)
            ASSERT_TRUE(string_value.compare(expected_value) == 0);
        }
        return true;
    }

    void SetBreakpoint(uint32_t line)
    {
        auto location = TestUtil::GetLocation("SetVariable.js", line, panda_file_.c_str());
        ASSERT_TRUE(location.GetMethodId().IsValid());
        ASSERT_SUCCESS(debug_interface->SetBreakpoint(location));
    }

    bool checked_ = false;
    uint32_t breakpoint_count_ = 0;
    std::string panda_file_ = "js/SetVariable.abc";
    std::string entry_point_ = "_GLOBAL::func_main_0";
};

inline std::unique_ptr<ApiTest> GetJsSetVariableTest()
{
    return std::make_unique<JsSetVariableTest>();
}
}  // namespace panda::tooling::test

#endif  // PANDA_TOOLING_TEST_JS_SET_VARIABLE_TEST_H
