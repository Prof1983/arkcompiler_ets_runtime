/**
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <vector>

#include "runtime/include/runtime_options.h"
#include "libpandabase/utils/pandargs.h"
#include "runtime/tests/options_test_base.h"

namespace panda::test {

class EcmascriptRuntimeOptionsTest : public RuntimeOptionsTestBase {
public:
    NO_COPY_SEMANTIC(EcmascriptRuntimeOptionsTest);
    NO_MOVE_SEMANTIC(EcmascriptRuntimeOptionsTest);

    EcmascriptRuntimeOptionsTest() = default;
    ~EcmascriptRuntimeOptionsTest() override = default;

private:
    void LoadCorrectOptionsList() override;
};

void EcmascriptRuntimeOptionsTest::LoadCorrectOptionsList()
{
    AddTestingOption("ecmascript.run-gc-in-place", "true");
    AddTestingOption("ecmascript.gc-dump-heap", "true");
    AddTestingOption("ecmascript.heap-verifier", "pre");
    AddTestingOption("gc-trigger-type", "no-gc-for-start-up");
}

TEST_F(EcmascriptRuntimeOptionsTest, TestLangSpecificOptions)
{
    ASSERT_TRUE(GetParser()->Parse(GetCorrectOptionsList()));
    ASSERT_EQ(GetRuntimeOptions()->GetGcTriggerType("core"), "no-gc-for-start-up");
    // Check that if we read JS specific option, it has the same value as a common one
    ASSERT_EQ(GetRuntimeOptions()->GetGcTriggerType("core"), GetRuntimeOptions()->GetGcTriggerType("ecmascript"));
}

}  // namespace panda::test
