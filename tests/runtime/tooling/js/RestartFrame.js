/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function MyClass () { // constructor function
    this.FrameValue = 0;  // Public variable 

    this.frame4 = function () {
        this.FrameValue += 10000;
        this.frame3();
        this.FrameValue += 100000;
        return this.FrameValue;
    }
    
    this.frame3 = function () {
        this.FrameValue += 1000;
        this.frame2();
        this.FrameValue += 100000;
    }
    
    this.frame2 = function () {
        this.FrameValue += 100;
        // Break point. And restart frame
        this.frame1();
        this.FrameValue += 100000;
    }
    
    this.frame1 = function () {
        this.FrameValue += 10;
        this.frame0();
    }
    
    this.frame0 = function () {
        this.FrameValue += 1;
    }
}

var myInstance = new MyClass();
var res = myInstance.frame4();
