/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


var g_a = 0;
var g_str = "";
var g_b = 0;

function method1(a_int, s_str, b_int) {
    g_a = a_int;
    if (method2(s_str, b_int))
        return a_int + 1;
    else
        return 0;
}

function method2(s_str, b_int) {
    g_str = s_str;
    method3(b_int);
    return (g_b == b_int);
}

function method3(b_int) {
    g_b = b_int;
}

var a = 1000;
var s = "Test String";
var b = 12345;
a = method1(a, s, b);
