/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
class BuiltinsMathTest : public testing::Test {
public:
    // Workaround: Avoid thread local leak [F/runtime: cannot create thread specific key for __cxa_get_globals()]
    static void SetUpTestCase()
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    static void TearDownTestCase()
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

    static PandaVM *instance_;
    static EcmaHandleScope *scope_;
    static JSThread *thread_;
};
PandaVM *BuiltinsMathTest::instance_ = nullptr;
EcmaHandleScope *BuiltinsMathTest::scope_ = nullptr;
JSThread *BuiltinsMathTest::thread_ = nullptr;

// Math.abs(-10)
TEST_F(BuiltinsMathTest, Abs)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-10)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(10);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(10)
TEST_F(BuiltinsMathTest, Abs_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(10)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(10);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(0)
TEST_F(BuiltinsMathTest, Abs_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(null)
TEST_F(BuiltinsMathTest, Abs_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs("hello")
TEST_F(BuiltinsMathTest, Abs_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("helloworld");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(Number.MAX_VALUE + 1)
TEST_F(BuiltinsMathTest, Abs_5)
{
    const double test_value = base::MAX_VALUE + 1;
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(test_value));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::MAX_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(Number.MIN_VALUE)
TEST_F(BuiltinsMathTest, Abs_6)
{
    const double test_value = base::MIN_VALUE + 1;
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(test_value));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(Number.POSITIVE_INFINITY + 1)
TEST_F(BuiltinsMathTest, Abs_7)
{
    const double test_value = base::POSITIVE_INFINITY + 1;
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(test_value));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(Number.NEGATIVE_INFINITY - 1)
TEST_F(BuiltinsMathTest, Abs_8)
{
    const double test_value = -base::POSITIVE_INFINITY - 1;
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(test_value));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(Number.NAN_VALUE)
TEST_F(BuiltinsMathTest, Abs_9)
{
    const double test_value = base::NAN_VALUE;
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(test_value));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(VALUE_UNDEFINED)
TEST_F(BuiltinsMathTest, Abs_10)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(true)
TEST_F(BuiltinsMathTest, Abs_11)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(1);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(false)
TEST_F(BuiltinsMathTest, Abs_12)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs(hole)
TEST_F(BuiltinsMathTest, Abs_13)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Hole());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.abs("100.12")
TEST_F(BuiltinsMathTest, Abs_14)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("100.12");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Abs(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(100.12);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(-1)
TEST_F(BuiltinsMathTest, Acos)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = math::GetPropPI();
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(1)
TEST_F(BuiltinsMathTest, Acos_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(-1.5)
TEST_F(BuiltinsMathTest, Acos_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-1.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(null)
TEST_F(BuiltinsMathTest, Acos_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(UNDEFINED)
TEST_F(BuiltinsMathTest, Acos_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(true)
TEST_F(BuiltinsMathTest, Acos_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(false)
TEST_F(BuiltinsMathTest, Acos_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos("0.1")
TEST_F(BuiltinsMathTest, Acos_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.4706289056333368);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos("")
TEST_F(BuiltinsMathTest, Acos_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acos(-NaN)
TEST_F(BuiltinsMathTest, Acos_9)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(1.1)
TEST_F(BuiltinsMathTest, Acosh)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(1.1));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.4435682543851154);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(0.5)
TEST_F(BuiltinsMathTest, Acosh_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(base::POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Acosh_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(null)
TEST_F(BuiltinsMathTest, Acosh_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(VALUE_UNDEFINED)
TEST_F(BuiltinsMathTest, Acosh_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(true)
TEST_F(BuiltinsMathTest, Acosh_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(false)
TEST_F(BuiltinsMathTest, Acosh_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(hole)
TEST_F(BuiltinsMathTest, Acosh_7)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Hole());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh("1")
TEST_F(BuiltinsMathTest, Acosh_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh("")
TEST_F(BuiltinsMathTest, Acosh_9)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.acosh(-NaN)
TEST_F(BuiltinsMathTest, Acosh_10)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Acosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(-1)
TEST_F(BuiltinsMathTest, Asin)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(1)
TEST_F(BuiltinsMathTest, Asin_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(-NaN)
TEST_F(BuiltinsMathTest, Asin_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(null)
TEST_F(BuiltinsMathTest, Asin_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(UNDEFINED)
TEST_F(BuiltinsMathTest, Asin_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(true)
TEST_F(BuiltinsMathTest, Asin_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(false)
TEST_F(BuiltinsMathTest, Asin_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin(""")
TEST_F(BuiltinsMathTest, Asin_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asin("1")
TEST_F(BuiltinsMathTest, Asin_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh(-1)
TEST_F(BuiltinsMathTest, Asinh)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.881373587019543);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh(1)
TEST_F(BuiltinsMathTest, Asinh_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.881373587019543);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh(null)
TEST_F(BuiltinsMathTest, Asinh_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh(-NaN)
TEST_F(BuiltinsMathTest, Asinh_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh(NEGATIVE_INFINITY)
TEST_F(BuiltinsMathTest, Asinh_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh(true)
TEST_F(BuiltinsMathTest, Asinh_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.881373587019543);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh(false)
TEST_F(BuiltinsMathTest, Asinh_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh("")
TEST_F(BuiltinsMathTest, Asinh_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.asinh("-5.7")
TEST_F(BuiltinsMathTest, Asinh_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-5.7");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Asinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-2.44122070725561);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan(-1)
TEST_F(BuiltinsMathTest, Atan)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.7853981633974483);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan(1)
TEST_F(BuiltinsMathTest, Atan_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.7853981633974483);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan(null)
TEST_F(BuiltinsMathTest, Atan_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan(-NaN)
TEST_F(BuiltinsMathTest, Atan_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Atan_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(math::GetPropPI().GetDouble() / 2);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan(true)
TEST_F(BuiltinsMathTest, Atan_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.7853981633974483);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan(false)
TEST_F(BuiltinsMathTest, Atan_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan("")
TEST_F(BuiltinsMathTest, Atan_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" ");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan("-1")
TEST_F(BuiltinsMathTest, Atan_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.7853981633974483);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh(-1)
TEST_F(BuiltinsMathTest, Atanh)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh(1)
TEST_F(BuiltinsMathTest, Atanh_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh(null)
TEST_F(BuiltinsMathTest, Atanh_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh(-NaN)
TEST_F(BuiltinsMathTest, Atanh_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh(1.5)
TEST_F(BuiltinsMathTest, Atanh_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(1.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh(true)
TEST_F(BuiltinsMathTest, Atanh_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh(false)
TEST_F(BuiltinsMathTest, Atanh_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh("")
TEST_F(BuiltinsMathTest, Atanh_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" ");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atanh("-1")
TEST_F(BuiltinsMathTest, Atanh_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(NaN, 1.5)
TEST_F(BuiltinsMathTest, Atan2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::NAN_VALUE));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(1.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(-1, 1.5)
TEST_F(BuiltinsMathTest, Atan2_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(1.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.5880026035475675);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(1, -0)
TEST_F(BuiltinsMathTest, Atan2_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(math::GetPropPI().GetDouble() / 2);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(0, 1)
TEST_F(BuiltinsMathTest, Atan2_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(0, -0)
TEST_F(BuiltinsMathTest, Atan2_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = math::GetPropPI();
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(-0, 0)
TEST_F(BuiltinsMathTest, Atan2_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(-0, -0)
TEST_F(BuiltinsMathTest, Atan2_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-math::GetPropPI().GetDouble());
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(true, false)
TEST_F(BuiltinsMathTest, Atan2_7)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(false, true)
TEST_F(BuiltinsMathTest, Atan2_8)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2("-1","")
TEST_F(BuiltinsMathTest, Atan2_9)
{
    JSHandle<EcmaString> test_1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-1");
    JSHandle<EcmaString> test_2 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test_1.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, test_2.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-1.5707963267948966);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2("0.23","0.72")
TEST_F(BuiltinsMathTest, Atan2_10)
{
    JSHandle<EcmaString> test_1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.23");
    JSHandle<EcmaString> test_2 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.72");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test_1.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, test_2.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.3091989123270746);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.atan2(-NaN, 1.5)
TEST_F(BuiltinsMathTest, Atan2_11)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-1.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Atan2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(0)
TEST_F(BuiltinsMathTest, Cbrt)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(-0)
TEST_F(BuiltinsMathTest, Cbrt_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(NEGATIVE_INFINITY)
TEST_F(BuiltinsMathTest, Cbrt_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Cbrt_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(VALUE_UNDEFINED)
TEST_F(BuiltinsMathTest, Cbrt_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(true)
TEST_F(BuiltinsMathTest, Cbrt_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(false)
TEST_F(BuiltinsMathTest, Cbrt_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt("")
TEST_F(BuiltinsMathTest, Cbrt_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" ");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt("1.23")
TEST_F(BuiltinsMathTest, Cbrt_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0714412696907731);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cbrt(-NaN)
TEST_F(BuiltinsMathTest, Cbrt_9)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cbrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(3.25)
TEST_F(BuiltinsMathTest, Ceil)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(3.25));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(4.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Ceil_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(-0.0)
TEST_F(BuiltinsMathTest, Ceil_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(null)
TEST_F(BuiltinsMathTest, Ceil_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(0)
TEST_F(BuiltinsMathTest, Ceil_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(true)
TEST_F(BuiltinsMathTest, Ceil_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(false)
TEST_F(BuiltinsMathTest, Ceil_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil("")
TEST_F(BuiltinsMathTest, Ceil_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil("3.23")
TEST_F(BuiltinsMathTest, Ceil_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(4.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.ceil(-NaN)
TEST_F(BuiltinsMathTest, Ceil_9)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Ceil(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos(0)
TEST_F(BuiltinsMathTest, Cos)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos(-NAN)
TEST_F(BuiltinsMathTest, Cos_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Cos_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos(-POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Cos_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos(true)
TEST_F(BuiltinsMathTest, Cos_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.5403023058681398);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos(false)
TEST_F(BuiltinsMathTest, Cos_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos("")
TEST_F(BuiltinsMathTest, Cos_6)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cos("3.23")
TEST_F(BuiltinsMathTest, Cos_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cos(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.9960946152060809);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh(0)
TEST_F(BuiltinsMathTest, Cosh)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh(-NAN)
TEST_F(BuiltinsMathTest, Cosh_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Cosh_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh(-POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Cosh_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh(true)
TEST_F(BuiltinsMathTest, Cosh_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5430806348152437);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh(false)
TEST_F(BuiltinsMathTest, Cosh_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh("")
TEST_F(BuiltinsMathTest, Cosh_6)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" ");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.cosh("3.23")
TEST_F(BuiltinsMathTest, Cosh_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Cosh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(12.659607234875645);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp(0)
TEST_F(BuiltinsMathTest, Exp)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp(-NAN)
TEST_F(BuiltinsMathTest, Exp_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Exp_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp(-POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Exp_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp(true)
TEST_F(BuiltinsMathTest, Exp_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(2.718281828459045);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp(false)
TEST_F(BuiltinsMathTest, Exp_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp("")
TEST_F(BuiltinsMathTest, Exp_6)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.exp("-3.23")
TEST_F(BuiltinsMathTest, Exp_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Exp(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.039557498788398725);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Expm1(0)
TEST_F(BuiltinsMathTest, Expm1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Expm1(-0.0)
TEST_F(BuiltinsMathTest, Expm1_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Expm1(-NAN)
TEST_F(BuiltinsMathTest, Expm1_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Expm1(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Expm1_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Expm1(-POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Expm1_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.expm1(true)
TEST_F(BuiltinsMathTest, Expm1_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    double expect = 1.718281828459045;
    ASSERT_TRUE(result.IsDouble());
    ASSERT_TRUE(std::abs(result.GetDouble() - expect) < 0.00000001);
}

// Math.expm1(false)
TEST_F(BuiltinsMathTest, Expm1_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.expm1("")
TEST_F(BuiltinsMathTest, Expm1_7)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" ");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.expm1("-3.23")
TEST_F(BuiltinsMathTest, Expm1_8)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.9604425012116012);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.expm1("0x12")
TEST_F(BuiltinsMathTest, Expm1_9)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0x12");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Expm1(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(65659968.13733051);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.floor(-0.0)
TEST_F(BuiltinsMathTest, Floor)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Floor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.floor(-NAN)
TEST_F(BuiltinsMathTest, Floor_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Floor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.floor(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Floor_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Floor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.floor(true)
TEST_F(BuiltinsMathTest, Floor_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Floor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.floor("-3.23")
TEST_F(BuiltinsMathTest, Floor_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Floor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-4.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log(-0.0)
TEST_F(BuiltinsMathTest, Log)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log(-NAN)
TEST_F(BuiltinsMathTest, Log_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Log_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log(true)
TEST_F(BuiltinsMathTest, Log_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log("-3.23")
TEST_F(BuiltinsMathTest, Log_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log(0.12)
TEST_F(BuiltinsMathTest, Log_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0.12));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-2.120263536200091);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log1p(-0.0)
TEST_F(BuiltinsMathTest, Log1p)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log1p(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log1p(-NAN)
TEST_F(BuiltinsMathTest, Log1p_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log1p(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log1p(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Log1p_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log1p(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log1p(true)
TEST_F(BuiltinsMathTest, Log1p_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log1p(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.6931471805599453);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log1p("-3.23")
TEST_F(BuiltinsMathTest, Log1p_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-3.23");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log1p(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log1p(0.12)
TEST_F(BuiltinsMathTest, Log1p_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0.12));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log1p(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.11332868530700317);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log10(-0.0)
TEST_F(BuiltinsMathTest, Log10)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log10(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Log10(-NAN)
TEST_F(BuiltinsMathTest, Log10_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log10(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Log10(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Log10_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log10(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Log10(true)
TEST_F(BuiltinsMathTest, Log10_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log10(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Log10("2")
TEST_F(BuiltinsMathTest, Log10_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log10(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.3010299956639812);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Log10(0.12)
TEST_F(BuiltinsMathTest, Log10_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0.12));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log10(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.9208187539523752);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log2(-0.0)
TEST_F(BuiltinsMathTest, Log2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log2(-NAN)
TEST_F(BuiltinsMathTest, Log2_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log2(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Log2_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log2(true)
TEST_F(BuiltinsMathTest, Log2_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log2("2")
TEST_F(BuiltinsMathTest, Log2_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.log2(1)
TEST_F(BuiltinsMathTest, Log2_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Log2(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Max(NaN,1,POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Max)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::NAN_VALUE));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Max(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Max()
TEST_F(BuiltinsMathTest, Max_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Max(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Max("3",100,2.5)
TEST_F(BuiltinsMathTest, Max_2)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("3");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(100)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(2.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Max(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(100);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Max(3,"100",-101.5)
TEST_F(BuiltinsMathTest, Max_3)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("100");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info->SetCallArg(1, test.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(-101.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Max(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(100.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Max(-3,"-100",true)
TEST_F(BuiltinsMathTest, Max_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-100");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-3)));
    ecma_runtime_call_info->SetCallArg(1, test.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Max(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(1);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.min(NaN,1,POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Min)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::NAN_VALUE));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Min(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.min()
TEST_F(BuiltinsMathTest, Min_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Min(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.min("3",100,2.5)
TEST_F(BuiltinsMathTest, Min_2)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("3");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(100)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(2.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Min(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(2.5);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.min(3,"100",-101.5)
TEST_F(BuiltinsMathTest, Min_3)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("100");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info->SetCallArg(1, test.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(-101.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Min(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-101.5);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.min(3,100,false)
TEST_F(BuiltinsMathTest, Min_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(100)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Min(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.pow(2,"-2")
TEST_F(BuiltinsMathTest, Pow)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-2");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(2)));
    ecma_runtime_call_info->SetCallArg(1, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Pow(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.25);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.pow(-NaN,-2)
TEST_F(BuiltinsMathTest, Pow_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(-2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Pow(ecma_runtime_call_info.get());
    ASSERT_TRUE(std::isnan(result.GetDouble()));
}

// Math.pow()
TEST_F(BuiltinsMathTest, Pow_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Pow(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.pow(false,-2)
TEST_F(BuiltinsMathTest, Pow_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::False());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(-2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Pow(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.random()
TEST_F(BuiltinsMathTest, Random)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = math::Random(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue result2 = math::Random(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_NE(result1.GetRawData(), result2.GetRawData());
}

// Math.random()
TEST_F(BuiltinsMathTest, Random_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = math::Random(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue result2 = math::Random(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    double value1 = JSTaggedValue(static_cast<JSTaggedType>(result1.GetRawData())).GetDouble();
    double value2 = JSTaggedValue(static_cast<JSTaggedType>(result2.GetRawData())).GetDouble();
    ASSERT_TRUE(value1 >= 0);
    ASSERT_TRUE(value1 < 1.0);
    ASSERT_TRUE(value2 >= 0);
    ASSERT_TRUE(value2 < 1.0);
}

// Math.round(-NaN)
TEST_F(BuiltinsMathTest, Round)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Round(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.round(1.25)
TEST_F(BuiltinsMathTest, Round_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(1.25));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Round(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.round(-0.14)
TEST_F(BuiltinsMathTest, Round_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.14));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Round(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.round(-0.7)
TEST_F(BuiltinsMathTest, Round_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.7));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Round(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.round(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Round_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Round(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.fround(POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Fround)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Fround(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.fround(-NaN)
TEST_F(BuiltinsMathTest, Fround_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Fround(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.fround(-0)
TEST_F(BuiltinsMathTest, Fround_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Fround(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.fround(1.337)
TEST_F(BuiltinsMathTest, Fround_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(1.337));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Fround(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.3370000123977661);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.fround(-668523145.253485)
TEST_F(BuiltinsMathTest, Fround_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-668523145.253485));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Fround(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-668523136.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.clz32(NaN)
TEST_F(BuiltinsMathTest, Clz32)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Clz32(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(32);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.clz32(-0)
TEST_F(BuiltinsMathTest, Clz32_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Clz32(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(32);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.clz32(1)
TEST_F(BuiltinsMathTest, Clz32_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Clz32(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(31);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.clz32(568243)
TEST_F(BuiltinsMathTest, Clz32_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(568243)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Clz32(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(12);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.clz32(4294967295)
TEST_F(BuiltinsMathTest, Clz32_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(4294967295)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Clz32(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.clz32(10000000000.123)
TEST_F(BuiltinsMathTest, Clz32_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(10000000000.123));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Clz32(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(1);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.clz32()
TEST_F(BuiltinsMathTest, Clz32_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Clz32(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(32);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.hypot()
TEST_F(BuiltinsMathTest, Hypot)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Hypot(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.hypot(-2.1)
TEST_F(BuiltinsMathTest, Hypot_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-2.1));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Hypot(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(2.1);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.hypot(-NaN, 1)
TEST_F(BuiltinsMathTest, Hypot_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Hypot(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result.IsDouble());
    ASSERT_TRUE(std::isnan(result.GetDouble()));
}

// Math.hypot(true, 5, 8, -0.2, 90000)
TEST_F(BuiltinsMathTest, Hypot_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 14);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<int32_t>(8)));
    ecma_runtime_call_info->SetCallArg(3, JSTaggedValue(-0.2));
    ecma_runtime_call_info->SetCallArg(4, JSTaggedValue(static_cast<int32_t>(90000)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Hypot(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(90000.00050022222);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Imul()
TEST_F(BuiltinsMathTest, Imul)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Imul(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Imul("-2",9.256)
TEST_F(BuiltinsMathTest, Imul_1)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-2");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(9.256));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Imul(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(-18);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Imul(5,0xffffffff)
TEST_F(BuiltinsMathTest, Imul_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0xffffffff)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Imul(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(-5);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.Imul(5,0xfffffffe)
TEST_F(BuiltinsMathTest, Imul_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0xfffffffe)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Imul(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedInt(-10);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin(-1)
TEST_F(BuiltinsMathTest, Sin)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.8414709848078965);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin(-1.5)
TEST_F(BuiltinsMathTest, Sin_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-1.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.9974949866040544);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin(null)
TEST_F(BuiltinsMathTest, Sin_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin(UNDEFINED)
TEST_F(BuiltinsMathTest, Sin_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin(true)
TEST_F(BuiltinsMathTest, Sin_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.8414709848078965);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin("0.1")
TEST_F(BuiltinsMathTest, Sin_6)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.09983341664682815);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin(Number.POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Sin_7)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sin(-NaN)
TEST_F(BuiltinsMathTest, Sin_8)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sin(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh(-1)
TEST_F(BuiltinsMathTest, Sinh)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-1.1752011936438014);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh(-1.5)
TEST_F(BuiltinsMathTest, Sinh_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-1.5));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-2.1292794550948173);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh(null)
TEST_F(BuiltinsMathTest, Sinh_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh(UNDEFINED)
TEST_F(BuiltinsMathTest, Sinh_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh(true)
TEST_F(BuiltinsMathTest, Sinh_4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.1752011936438014);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh("0.1")
TEST_F(BuiltinsMathTest, Sinh_5)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.10016675001984403);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh(-Number.POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Sinh_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sinh(-NaN)
TEST_F(BuiltinsMathTest, Sinh_7)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sinh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sqrt(-1)
TEST_F(BuiltinsMathTest, Sqrt)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sqrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sqrt(-0)
TEST_F(BuiltinsMathTest, Sqrt_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sqrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sqrt(null)
TEST_F(BuiltinsMathTest, Sqrt_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sqrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sqrt(true)
TEST_F(BuiltinsMathTest, Sqrt_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sqrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sqrt("0.1")
TEST_F(BuiltinsMathTest, Sqrt_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sqrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.31622776601683794);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sqrt(Number.POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Sqrt_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sqrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.sqrt(-NaN)
TEST_F(BuiltinsMathTest, Sqrt_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Sqrt(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tan(-1)
TEST_F(BuiltinsMathTest, Tan)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-1.5574077246549023);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tan(-0)
TEST_F(BuiltinsMathTest, Tan_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tan(null)
TEST_F(BuiltinsMathTest, Tan_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tan(true)
TEST_F(BuiltinsMathTest, Tan_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.5574077246549023);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tan("0.1")
TEST_F(BuiltinsMathTest, Tan_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.10033467208545055);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tan(Number.POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Tan_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tan(-NaN)
TEST_F(BuiltinsMathTest, Tan_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tan(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tanh(-1)
TEST_F(BuiltinsMathTest, Tanh)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.7615941559557649);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tanh(-0)
TEST_F(BuiltinsMathTest, Tanh_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tanh(null)
TEST_F(BuiltinsMathTest, Tanh_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tanh(true)
TEST_F(BuiltinsMathTest, Tanh_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.7615941559557649);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tanh("0.1")
TEST_F(BuiltinsMathTest, Tanh_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0.1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0.09966799462495582);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tanh(Number.POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Tanh_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.tanh(-NaN)
TEST_F(BuiltinsMathTest, Tanh_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Tanh(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.trunc(-1)
TEST_F(BuiltinsMathTest, Trunc)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Trunc(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.trunc(-0)
TEST_F(BuiltinsMathTest, Trunc_1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-0.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Trunc(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.trunc(null)
TEST_F(BuiltinsMathTest, Trunc_2)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Trunc(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.trunc(true)
TEST_F(BuiltinsMathTest, Trunc_3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Trunc(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(1.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.trunc("-0.1")
TEST_F(BuiltinsMathTest, Trunc_4)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-0.1");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Trunc(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(-0.0);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.trunc(Number.POSITIVE_INFINITY)
TEST_F(BuiltinsMathTest, Trunc_5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Trunc(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::POSITIVE_INFINITY);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}

// Math.trunc(-NaN)
TEST_F(BuiltinsMathTest, Trunc_6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(-base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = math::Trunc(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue expect = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), expect.GetRawData());
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
