/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"

#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_set.h"
#include "plugins/ecmascript/runtime/js_set_iterator.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {
using JSSet = ecmascript::JSSet;

class BuiltinsSetTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

    class TestClass {
    public:
        static JSTaggedValue TestFunc(EcmaRuntimeCallInfo *argv)
        {
            JSTaggedValue key = builtins_common::GetCallArg(argv, 0).GetTaggedValue();
            if (key.IsUndefined()) {
                return JSTaggedValue::Undefined();
            }
            JSArray *js_array = JSArray::Cast(builtins_common::GetThis(argv)->GetTaggedObject());
            int length = js_array->GetArrayLength() + 1;
            js_array->SetArrayLength(argv->GetThread(), length);
            return JSTaggedValue::Undefined();
        }
    };

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSSet *CreateBuiltinsSet(JSThread *thread)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> new_target(env->GetSetFunction());
    // 4 : test case
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue(*new_target), 4);
    ecma_runtime_call_info->SetFunction(new_target.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    JSTaggedValue result = set::SetConstructor(ecma_runtime_call_info.get());

    EXPECT_TRUE(result.IsECMAObject());
    return JSSet::Cast(reinterpret_cast<TaggedObject *>(result.GetRawData()));
}
// new Set("abrupt").toString()
TEST_F(BuiltinsSetTest, CreateAndGetSize)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> new_target(env->GetSetFunction());
    JSHandle<JSSet> set(thread_, CreateBuiltinsSet(thread_));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(set.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());
    {
        [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
        JSTaggedValue result = set::proto::GetSize(ecma_runtime_call_info.get());

        EXPECT_EQ(result.GetRawData(), JSTaggedValue(0).GetRawData());
    }

    JSHandle<TaggedArray> array(factory->NewTaggedArray(5));
    for (int i = 0; i < 5; i++) {
        array->Set(thread_, i, JSTaggedValue(i));
    }

    JSHandle<JSArray> values = JSArray::CreateArrayFromList(thread_, array);
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(new_target.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(set.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, values.GetTaggedValue());
    ecma_runtime_call_info1->SetNewTarget(new_target.GetTaggedValue());
    {
        [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
        JSTaggedValue result1 = set::SetConstructor(ecma_runtime_call_info1.get());

        EXPECT_EQ(JSSet::Cast(reinterpret_cast<TaggedObject *>(result1.GetRawData()))->GetSize(), 5);
    }
}

TEST_F(BuiltinsSetTest, AddAndHas)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // create jsSet
    JSHandle<JSSet> set(thread_, CreateBuiltinsSet(thread_));
    JSHandle<JSTaggedValue> key(thread_, factory->NewFromCanBeCompressString("key").GetTaggedValue());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(set.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = set::proto::Has(ecma_runtime_call_info.get());

    EXPECT_EQ(result1.GetRawData(), JSTaggedValue::False().GetRawData());

    // test Add()
    JSTaggedValue result2 = set::proto::Add(ecma_runtime_call_info.get());
    EXPECT_TRUE(result2.IsECMAObject());
    JSSet *js_set = JSSet::Cast(reinterpret_cast<TaggedObject *>(result2.GetRawData()));
    EXPECT_EQ(js_set->GetSize(), 1);

    // test Has()
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue(js_set));
    ecma_runtime_call_info1->SetCallArg(0, key.GetTaggedValue());
    {
        [[maybe_unused]] auto prev_local = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
        JSTaggedValue result3 = set::proto::Has(ecma_runtime_call_info1.get());

        EXPECT_EQ(result3.GetRawData(), JSTaggedValue::True().GetRawData());
    }

    // test -0.0
    JSHandle<JSTaggedValue> negative_zero(thread_, JSTaggedValue(-0.0));
    JSHandle<JSTaggedValue> positive_zero(thread_, JSTaggedValue(+0.0));
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(JSTaggedValue(js_set));
    ecma_runtime_call_info2->SetCallArg(0, negative_zero.GetTaggedValue());
    {
        [[maybe_unused]] auto prev_local = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
        set::proto::Add(ecma_runtime_call_info.get());
    }

    auto ecma_runtime_call_info3 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info3->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info3->SetThis(JSTaggedValue(js_set));
    ecma_runtime_call_info3->SetCallArg(0, positive_zero.GetTaggedValue());
    {
        [[maybe_unused]] auto prev_local = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
        JSTaggedValue result4 = set::proto::Has(ecma_runtime_call_info3.get());

        EXPECT_EQ(result4.GetRawData(), JSTaggedValue::False().GetRawData());
    }
}

TEST_F(BuiltinsSetTest, ForEach)
{
    // generate a set has 5 entry{key1,key2,key3,key4,key5}
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSSet> set(thread_, CreateBuiltinsSet(thread_));
    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    char key_array[] = "key0";
    for (int i = 0; i < 5; i++) {
        key_array[3] = '1' + i;
        JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(key_array));
        auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
        ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
        ecma_runtime_call_info->SetThis(set.GetTaggedValue());
        ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

        [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
        JSTaggedValue result1 = set::proto::Add(ecma_runtime_call_info.get());

        EXPECT_TRUE(result1.IsECMAObject());
        JSSet *js_set = JSSet::Cast(reinterpret_cast<TaggedObject *>(result1.GetRawData()));
        EXPECT_EQ(js_set->GetSize(), i + 1);
    }
    // test foreach;
    JSHandle<JSArray> js_array(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)));
    JSHandle<JSFunction> func =
        factory->NewJSFunction(thread_->GetEcmaVM()->GetGlobalEnv(), reinterpret_cast<void *>(TestClass::TestFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(set.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, js_array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = set::proto::ForEach(ecma_runtime_call_info1.get());

    EXPECT_EQ(result2.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);
    EXPECT_EQ(js_array->GetArrayLength(), 5);
}

TEST_F(BuiltinsSetTest, DeleteAndRemove)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // create jsSet
    JSHandle<JSSet> set(thread_, CreateBuiltinsSet(thread_));

    // add 40 keys
    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    char key_array[] = "key0";
    // NOLINTNEXTLINE(readability-magic-numbers)
    for (int i = 0; i < 40; i++) {
        key_array[3] = '1' + i;
        JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(key_array));

        auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
        ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
        ecma_runtime_call_info->SetThis(set.GetTaggedValue());
        ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

        [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
        JSTaggedValue result1 = set::proto::Add(ecma_runtime_call_info.get());

        EXPECT_TRUE(result1.IsECMAObject());
        JSSet *js_set = JSSet::Cast(reinterpret_cast<TaggedObject *>(result1.GetRawData()));
        EXPECT_EQ(js_set->GetSize(), i + 1);
    }
    // whether jsSet has delete key
    key_array[3] = '1' + 8;
    JSHandle<JSTaggedValue> delete_key(factory->NewFromCanBeCompressString(key_array));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(set.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, delete_key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = set::proto::Has(ecma_runtime_call_info1.get());

    EXPECT_EQ(result2.GetRawData(), JSTaggedValue::True().GetRawData());

    // delete
    JSTaggedValue result3 = set::proto::Delete(ecma_runtime_call_info1.get());

    EXPECT_EQ(result3.GetRawData(), JSTaggedValue::True().GetRawData());

    // check deleteKey is deleted
    JSTaggedValue result4 = set::proto::Has(ecma_runtime_call_info1.get());

    EXPECT_EQ(result4.GetRawData(), JSTaggedValue::False().GetRawData());

    JSTaggedValue result5 = set::proto::GetSize(ecma_runtime_call_info1.get());

    EXPECT_EQ(result5.GetRawData(), JSTaggedValue(39).GetRawData());

    // clear
    JSTaggedValue result6 = set::proto::Clear(ecma_runtime_call_info1.get());
    EXPECT_EQ(result6.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);
    EXPECT_EQ(set->GetSize(), 0);
}

TEST_F(BuiltinsSetTest, Species)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> set(thread_, CreateBuiltinsSet(thread_));
    JSHandle<JSTaggedValue> species_symbol = env->GetSpeciesSymbol();
    EXPECT_TRUE(!species_symbol->IsUndefined());

    JSHandle<JSFunction> new_target(env->GetSetFunction());

    JSTaggedValue value =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(new_target), species_symbol).GetValue().GetTaggedValue();
    JSHandle<JSTaggedValue> value_handle(thread_, value);
    EXPECT_EQ(value, new_target.GetTaggedValue());

    // to string tag
    JSHandle<JSTaggedValue> to_string_tag_symbol = env->GetToStringTagSymbol();
    JSHandle<EcmaString> string_tag(JSObject::GetProperty(thread_, set, to_string_tag_symbol).GetValue());
    JSHandle<EcmaString> str = factory->NewFromCanBeCompressString("Set");
    EXPECT_TRUE(!string_tag.GetTaggedValue().IsUndefined());
    EXPECT_TRUE(EcmaString::StringsAreEqual(*str, *string_tag));

    JSHandle<JSFunction> constructor = JSHandle<JSFunction>::Cast(JSTaggedValue::ToObject(thread_, value_handle));
    EXPECT_EQ(JSHandle<JSObject>(set)->GetPrototype(thread_), constructor->GetFunctionPrototype());

    JSHandle<JSTaggedValue> key1(factory->NewFromCanBeCompressString("add"));
    JSTaggedValue value1 = JSObject::GetProperty(thread_, set, key1).GetValue().GetTaggedValue();
    EXPECT_FALSE(value1.IsUndefined());

    JSHandle<JSTaggedValue> key2(factory->NewFromCanBeCompressString("has"));
    JSTaggedValue value2 = JSObject::GetProperty(thread_, set, key1).GetValue().GetTaggedValue();
    EXPECT_FALSE(value2.IsUndefined());

    JSHandle<JSTaggedValue> key3(factory->NewFromCanBeCompressString("clear"));
    JSTaggedValue value3 = JSObject::GetProperty(thread_, set, key1).GetValue().GetTaggedValue();
    EXPECT_FALSE(value3.IsUndefined());

    JSHandle<JSTaggedValue> key4(factory->NewFromCanBeCompressString("size"));
    JSTaggedValue value4 = JSObject::GetProperty(thread_, set, key1).GetValue().GetTaggedValue();
    EXPECT_FALSE(value4.IsUndefined());

    JSHandle<JSTaggedValue> key5(factory->NewFromCanBeCompressString("delete"));
    JSTaggedValue value5 = JSObject::GetProperty(thread_, set, key1).GetValue().GetTaggedValue();
    EXPECT_FALSE(value5.IsUndefined());

    JSHandle<JSTaggedValue> key6(factory->NewFromCanBeCompressString("forEach"));
    JSTaggedValue value6 = JSObject::GetProperty(thread_, set, key1).GetValue().GetTaggedValue();
    EXPECT_FALSE(value6.IsUndefined());
}

TEST_F(BuiltinsSetTest, GetIterator)
{
    JSHandle<JSTaggedValue> set(thread_, CreateBuiltinsSet(thread_));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(set.GetTaggedValue());
    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());

    // test Values()
    JSTaggedValue result = set::proto::Values(ecma_runtime_call_info.get());
    JSHandle<JSSetIterator> iter(thread_, result);
    EXPECT_TRUE(iter->IsJSSetIterator());
    EXPECT_EQ(IterationKind::VALUE, IterationKind(iter->GetIterationKind().GetInt()));
    EXPECT_EQ(JSSet::Cast(set.GetTaggedValue().GetTaggedObject())->GetLinkedSet(), iter->GetIteratedSet());

    // test entrys()
    JSTaggedValue result2 = set::proto::Entries(ecma_runtime_call_info.get());
    JSHandle<JSSetIterator> iter2(thread_, result2);
    EXPECT_TRUE(iter2->IsJSSetIterator());
    EXPECT_EQ(IterationKind::KEY_AND_VALUE, IterationKind(iter2->GetIterationKind().GetInt()));
}
}  // namespace panda::test
