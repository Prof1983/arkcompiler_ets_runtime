/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"

#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
class BuiltinsFunctionTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

// native function for test apply and call
JSTaggedValue TestFunctionApplyAndCall(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    int result = 0;
    for (uint32_t index = 0; index < argv->GetArgsNumber(); ++index) {
        result += builtins_common::GetCallArg(argv, index)->GetInt();
    }
    JSHandle<JSTaggedValue> this_value(builtins_common::GetThis(argv));

    JSTaggedValue test_a =
        JSObject::GetProperty(thread, this_value,
                              JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")))
            .GetValue()
            .GetTaggedValue();
    JSTaggedValue test_b =
        JSObject::GetProperty(thread, this_value,
                              JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")))
            .GetValue()
            .GetTaggedValue();

    result = result + test_a.GetInt() + test_b.GetInt();
    return builtins_common::GetTaggedInt(result);
}

// func.apply(thisArg)
TEST_F(BuiltinsFunctionTest, FunctionPrototypeApply)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // ecma 19.2.3.1: func
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestFunctionApplyAndCall));

    // ecma 19.2.3.1: thisArg
    JSHandle<JSObject> this_arg(thread_, env->GetGlobalObject());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(func.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, (this_arg.GetTaggedValue()));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = function::proto::Apply(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(3).GetRawData());

    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")));
    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")));
}

// func.apply(thisArg, argArray)
TEST_F(BuiltinsFunctionTest, FunctionPrototypeApply1)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // ecma 19.2.3.1: func
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestFunctionApplyAndCall));

    // ecma 19.2.3.1: thisArg
    JSHandle<JSObject> this_arg(thread_, env->GetGlobalObject());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(10)));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(20)));

    // ecma 19.2.3.1: argArray
    JSHandle<JSObject> array(JSArray::ArrayCreate(thread_, JSTaggedNumber(2)));
    PropertyDescriptor desc(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(30)));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0)), desc);

    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(40)));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), desc1);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(func.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, (this_arg.GetTaggedValue()));
    ecma_runtime_call_info->SetCallArg(1, array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = function::proto::Apply(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(100).GetRawData());

    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")));
    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")));
}

// target.bind(thisArg)
TEST_F(BuiltinsFunctionTest, FunctionPrototypeBind)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSFunction> target = factory->NewJSFunction(env);
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(target),
                                JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("target")),
                                JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Undefined()));
    JSFunction::SetFunctionLength(thread_, target, JSTaggedValue(2));

    JSHandle<JSObject> this_arg(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, (this_arg.GetTaggedValue()));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = function::proto::Bind(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());

    JSHandle<JSBoundFunction> result_func(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    // test BoundTarget
    ASSERT_EQ(result_func->GetBoundTarget(), target.GetTaggedValue());
    // test BoundThis
    ASSERT_EQ(result_func->GetBoundThis(), this_arg.GetTaggedValue());
    // test BoundArguments
    JSHandle<TaggedArray> array(thread_, result_func->GetBoundArguments());
    ASSERT_EQ(array->GetLength(), 0);
    // test name property
    auto global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> name_key = global_const->GetHandledNameString();
    JSHandle<JSTaggedValue> result_func_handle(thread_, *result_func);
    JSHandle<EcmaString> result_name(JSObject::GetProperty(thread_, result_func_handle, name_key).GetValue());
    JSHandle<EcmaString> bound_target = factory->NewFromCanBeCompressString("bound target");
    ASSERT_EQ(result_name->Compare(*bound_target), 0);
    // test length property
    JSHandle<JSTaggedValue> length_key = global_const->GetHandledLengthString();
    JSHandle<JSTaggedValue> result_length(JSObject::GetProperty(thread_, result_func_handle, length_key).GetValue());
    ASSERT_EQ(JSTaggedValue::ToNumber(thread_, result_length).GetNumber(), 2.0);
}

// target.bind(thisArg, 123, "helloworld")
TEST_F(BuiltinsFunctionTest, FunctionPrototypeBind1)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSFunction> target = factory->NewJSFunction(env);
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(target),
                                JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("target1")),
                                JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Undefined()));
    JSFunction::SetFunctionLength(thread_, target, JSTaggedValue(5));

    JSHandle<JSObject> this_arg(thread_, env->GetGlobalObject());
    JSHandle<EcmaString> str = factory->NewFromCanBeCompressString("helloworld");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, this_arg.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(123)));
    ecma_runtime_call_info->SetCallArg(2, str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = function::proto::Bind(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());

    JSHandle<JSBoundFunction> result_func(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    // test BoundTarget
    ASSERT_EQ(result_func->GetBoundTarget(), target.GetTaggedValue());
    // test BoundThis
    ASSERT_EQ(result_func->GetBoundThis(), this_arg.GetTaggedValue());
    // test BoundArguments
    JSHandle<TaggedArray> array(thread_, result_func->GetBoundArguments());
    ASSERT_EQ(array->GetLength(), 2);
    JSTaggedValue elem = array->Get(0);
    JSTaggedValue elem1 = array->Get(1);
    ASSERT_EQ(elem.GetRawData(), JSTaggedValue(123).GetRawData());

    ASSERT_EQ(elem1.GetRawData(), str.GetTaggedType());
    ASSERT_TRUE(elem1.IsString());
    // test name property
    auto global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> name_key = global_const->GetHandledNameString();
    JSHandle<JSTaggedValue> result_func_handle(thread_, *result_func);
    JSHandle<EcmaString> result_name(JSObject::GetProperty(thread_, result_func_handle, name_key).GetValue());
    JSHandle<EcmaString> ruler_name = factory->NewFromCanBeCompressString("bound target1");
    ASSERT_EQ(result_name->Compare(*ruler_name), 0);
    // test length property
    JSHandle<JSTaggedValue> length_key = global_const->GetHandledLengthString();
    JSHandle<JSTaggedValue> result_length(JSObject::GetProperty(thread_, result_func_handle, length_key).GetValue());
    // target.length is 5, (...args) length is 2
    ASSERT_EQ(JSTaggedValue::ToNumber(thread_, result_length).GetNumber(), 3.0);
}

// target.bind(thisArg, 123, "helloworld") set target_name = EmptyString()
TEST_F(BuiltinsFunctionTest, FunctionPrototypeBind2)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSFunction> target = factory->NewJSFunction(env);
    PropertyDescriptor name_desc(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(123)), false, false, true);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(target),
                                         thread_->GlobalConstants()->GetHandledNameString(), name_desc);
    JSFunction::SetFunctionLength(thread_, target, JSTaggedValue(5));

    JSHandle<JSObject> this_arg(thread_, env->GetGlobalObject());
    JSHandle<EcmaString> str = factory->NewFromCanBeCompressString("helloworld");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, (this_arg.GetTaggedValue()));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(123)));
    ecma_runtime_call_info->SetCallArg(2, str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = function::proto::Bind(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());

    JSHandle<JSBoundFunction> result_func(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    // test BoundTarget
    ASSERT_EQ(result_func->GetBoundTarget(), target.GetTaggedValue());
    // test BoundThis
    ASSERT_EQ(result_func->GetBoundThis(), this_arg.GetTaggedValue());
    // test BoundArguments
    JSHandle<TaggedArray> array(thread_, result_func->GetBoundArguments());
    ASSERT_EQ(array->GetLength(), 2);
    JSTaggedValue elem = array->Get(0);
    JSTaggedValue elem1 = array->Get(1);
    ASSERT_EQ(elem.GetRawData(), JSTaggedValue(123).GetRawData());

    ASSERT_EQ(elem1.GetRawData(), str.GetTaggedType());
    ASSERT_TRUE(elem1.IsString());
    // test name property
    auto global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> name_key = global_const->GetHandledNameString();
    JSHandle<JSTaggedValue> result_func_handle(result_func);
    JSHandle<EcmaString> result_name(JSObject::GetProperty(thread_, result_func_handle, name_key).GetValue());
    JSHandle<EcmaString> ruler_name = factory->NewFromCanBeCompressString("bound ");
    ASSERT_EQ(result_name->Compare(*ruler_name), 0);
    // test length property
    JSHandle<JSTaggedValue> length_key = global_const->GetHandledLengthString();
    JSHandle<JSTaggedValue> result_length(JSObject::GetProperty(thread_, result_func_handle, length_key).GetValue());
    // target.length is 5, (...args) length is 2
    ASSERT_EQ(JSTaggedValue::ToNumber(thread_, result_length).GetNumber(), 3.0);
}

// func.call(thisArg)
TEST_F(BuiltinsFunctionTest, FunctionPrototypeCall)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // ecma 19.2.3.3: func
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestFunctionApplyAndCall));

    // ecma 19.2.3.3: thisArg
    JSHandle<JSObject> this_arg(thread_, env->GetGlobalObject());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(func.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, (this_arg.GetTaggedValue()));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = function::proto::Call(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(3).GetRawData());

    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")));
    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")));
}

// func.call(thisArg, 123, 456, 789)
TEST_F(BuiltinsFunctionTest, FunctionPrototypeCall1)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // ecma 19.2.3.3: func
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestFunctionApplyAndCall));

    // ecma 19.2.3.3: thisArg
    JSHandle<JSObject> this_arg(thread_, env->GetGlobalObject());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_arg),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)));

    // func thisArg ...args
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 12);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(func.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, (this_arg.GetTaggedValue()));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(123)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<int32_t>(456)));
    ecma_runtime_call_info->SetCallArg(3, JSTaggedValue(static_cast<int32_t>(789)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = function::proto::Call(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(1371).GetRawData());

    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_a")));
    JSObject::DeleteProperty(thread_, (this_arg),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_builtins_function_b")));
}

TEST_F(BuiltinsFunctionTest, FunctionPrototypeHasInstance)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    JSHandle<JSFunction> boolean_ctor(env->GetBooleanFunction());

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*boolean_ctor), 6);
    ecma_runtime_call_info1->SetFunction(boolean_ctor.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(123)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = boolean::BooleanConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<JSObject> boolean_instance(thread_, result);

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(boolean_ctor.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, boolean_instance.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    EXPECT_TRUE(function::proto::HasInstance(ecma_runtime_call_info2.get()).GetRawData());
    TestHelper::TearDownFrame(thread_, prev);
}

// NOLINTEND(readability-magic-numbers)

}  // namespace panda::test
