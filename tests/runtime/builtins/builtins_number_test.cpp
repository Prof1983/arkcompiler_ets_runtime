/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cmath>
#include <iostream>
#include <limits>
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_global_object.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
class BuiltinsNumberTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

// new Number(10)
TEST_F(BuiltinsNumberTest, NumberConstructor)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    JSHandle<JSFunction> number(env->GetNumberFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*number), 6);
    ecma_runtime_call_info->SetFunction(number.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::NumberConstructor(ecma_runtime_call_info.get());
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    JSPrimitiveRef *ref = JSPrimitiveRef::Cast(value.GetTaggedObject());
    ASSERT_EQ(ref->GetValue().GetDouble(), 5.0);
}

// Number.isFinite(-10)
TEST_F(BuiltinsNumberTest, IsFinite)
{
    const double value = -10;
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(value)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsFinite(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// Number.isFinite(Number.MAX_VALUE)
TEST_F(BuiltinsNumberTest, IsFinite1)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::MAX_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsFinite(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// Number.isFinite("helloworld")
TEST_F(BuiltinsNumberTest, IsFinite2)
{
    JSHandle<EcmaString> test = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("helloworld");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, test.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsFinite(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Number.isFinite(NaN)
TEST_F(BuiltinsNumberTest, IsFinite3)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::NAN_VALUE));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsFinite(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Number.isFinite(Infinity)
TEST_F(BuiltinsNumberTest, IsFinite4)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(base::POSITIVE_INFINITY));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsFinite(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Number.isFinite(undefined)
TEST_F(BuiltinsNumberTest, IsFinite5)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsFinite(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Number.isFinite(null)
TEST_F(BuiltinsNumberTest, IsFinite6)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Null());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsFinite(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Number.isInteger(0.1)
TEST_F(BuiltinsNumberTest, IsInteger)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0.1));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsInteger(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Number.isNaN(0.1)
TEST_F(BuiltinsNumberTest, IsNaN)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0.1));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::IsNaN(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// new Number(123.456).toString(7)
TEST_F(BuiltinsNumberTest, ToString)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> number_object(env->GetNumberFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(123.456));
    JSHandle<JSPrimitiveRef> number = thread_->GetEcmaVM()->GetFactory()->NewJSPrimitiveRef(number_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(number.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(7.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::proto::ToString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSHandle<EcmaString> correct_result = factory->NewFromCanBeCompressString("234.312256641535441");
    PandaVector<uint8_t> test(res->GetLength() + 1);
    res->CopyDataUtf8(test.data(), res->GetLength());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*res, *correct_result));
}

// new Number(123.456).toExponential(5)
TEST_F(BuiltinsNumberTest, IsExponential)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> number_object(env->GetNumberFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(123.456));
    JSHandle<JSPrimitiveRef> number = factory->NewJSPrimitiveRef(number_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(number.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(5.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::proto::ToExponential(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSHandle<EcmaString> correct_result = factory->NewFromCanBeCompressString("1.23456e+2");
    PandaVector<uint8_t> test(res->GetLength() + 1);
    res->CopyDataUtf8(test.data(), res->GetLength());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*res, *correct_result));
}

// new Number(123.456).toFixed(10)
TEST_F(BuiltinsNumberTest, ToFixed)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> number_object(env->GetNumberFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(123.456));
    JSHandle<JSPrimitiveRef> number = factory->NewJSPrimitiveRef(number_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(number.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(10.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::proto::ToFixed(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSHandle<EcmaString> correct_result = factory->NewFromCanBeCompressString("123.4560000000");
    ASSERT_TRUE(EcmaString::StringsAreEqual(*res, *correct_result));
}

// new Number(123.456).toFixed(30)
TEST_F(BuiltinsNumberTest, ToFixed1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> number_object(env->GetNumberFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(123.456));
    JSHandle<JSPrimitiveRef> number = factory->NewJSPrimitiveRef(number_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(number.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(30.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::proto::ToFixed(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSHandle<EcmaString> correct_result = factory->NewFromCanBeCompressString("123.456000000000003069544618483633");
    ASSERT_TRUE(EcmaString::StringsAreEqual(*res, *correct_result));
}

// new Number(1e21).toFixed(20)
TEST_F(BuiltinsNumberTest, ToFixed2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> number_object(env->GetNumberFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1e21));
    JSHandle<JSPrimitiveRef> number = factory->NewJSPrimitiveRef(number_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(number.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(20.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::proto::ToFixed(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSHandle<EcmaString> correct_result = factory->NewFromCanBeCompressString("1e+21");
    PandaVector<uint8_t> test(res->GetLength() + 1);
    res->CopyDataUtf8(test.data(), res->GetLength());
    std::cout << test.data();
    ASSERT_TRUE(EcmaString::StringsAreEqual(*res, *correct_result));
}

// new Number(123.456).toPrecision(8)
TEST_F(BuiltinsNumberTest, ToPrecision)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> number_object(env->GetNumberFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(123.456));
    JSHandle<JSPrimitiveRef> number = factory->NewJSPrimitiveRef(number_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(number.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(8.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::proto::ToPrecision(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSHandle<EcmaString> correct_result = factory->NewFromCanBeCompressString("123.45600");
    ASSERT_TRUE(EcmaString::StringsAreEqual(*res, *correct_result));
}

// Number.parseFloat(0x123)
TEST_F(BuiltinsNumberTest, parseFloat)
{
    JSHandle<EcmaString> param = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0x123");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, param.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::ParseFloat(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(0)).GetRawData());
}

// Number.parseFloat(0x123xx)
TEST_F(BuiltinsNumberTest, parseFloat1)
{
    JSHandle<EcmaString> param = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0x123xx");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, param.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::ParseFloat(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(0)).GetRawData());
}

// Number.parseInt(0x123)
TEST_F(BuiltinsNumberTest, parseInt)
{
    const char *number = "0x123";

    JSHandle<EcmaString> param = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(number);
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, param.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(16.0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = number::ParseInt(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(291)).GetRawData());
}

// testcases of StringToDouble flags
TEST_F(BuiltinsNumberTest, StringToDoubleFlags)
{
    JSHandle<EcmaString> str;
    Span<const uint8_t> sp;

    // flags of IGNORE_TRAILING

    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0a");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 0);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0b");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 0);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0o");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 0);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" 00x");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 0);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" 000.4_");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 0.4);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" 0010.s ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 10);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" 0010e2");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 1000);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" 0010e+3_0");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::IGNORE_TRAILING), 10000);

    // flags of ALLOW_HEX
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0x");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_TRUE(std::isnan(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_HEX)));
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  0x10 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_HEX), 16);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0x1g");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_HEX + base::IGNORE_TRAILING), 1);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0xh");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_TRUE(std::isnan(
        base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_HEX + base::IGNORE_TRAILING)));

    // flags of ALLOW_OCTAL
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0O");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_TRUE(std::isnan(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_OCTAL)));
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  0o10 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_OCTAL), 8);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0o1d");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_OCTAL | base::IGNORE_TRAILING),
              1);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0o8");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_TRUE(std::isnan(
        base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_OCTAL | base::IGNORE_TRAILING)));

    // flags of ALLOW_BINARY
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0b");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_TRUE(std::isnan(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_BINARY)));
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  0b10 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_BINARY), 2);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0b1d");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_BINARY | base::IGNORE_TRAILING),
              1);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0b2");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_TRUE(std::isnan(
        base::NumberHelper::StringToDouble(sp.begin(), sp.end(), 0, base::ALLOW_BINARY | base::IGNORE_TRAILING)));
}

// testcases of StringToDouble radix
TEST_F(BuiltinsNumberTest, StringToDoubleRadix)
{
    JSHandle<EcmaString> str;
    Span<const uint8_t> sp;
    int radix;

    radix = 0;  // default 10
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" 100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 100);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(" 100.3e2 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 10030);
    radix = 1;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  0000 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 0);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  0001 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_TRUE(std::isnan(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS)));
    radix = 2;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 4);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  11 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 3);
    radix = 3;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 9);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  21 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 7);
    radix = 4;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 16);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  31 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 13);
    radix = 8;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 64);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  71 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 57);
    radix = 10;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 100);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  0020 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 20);
    radix = 16;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 256);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  1e ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 30);
    radix = 18;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 324);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  1g ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 34);
    radix = 25;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 625);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  1g ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 41);
    radix = 36;
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  100 ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 1296);
    str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("  1z ");
    sp = Span<const uint8_t>(str->GetDataUtf8(), str->GetUtf8Length() - 1);
    ASSERT_EQ(base::NumberHelper::StringToDouble(sp.begin(), sp.end(), radix, base::NO_FLAGS), 71);
}

TEST_F(BuiltinsNumberTest, NumberToString)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> res = factory->NewFromCanBeCompressString("100");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(100))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("11223344");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(11223344))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("1234567890");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(1234567890))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("100");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(100.0)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("100.5");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(100.5)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("100.25");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(100.25)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("100.125");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(100.125)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("100.6125");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(100.6125)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("0.0006125");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(0.0006125)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("-0.0006125");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(-0.0006125)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("-1234567890.0006125");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(-1234567890.0006125)))->Compare(*res),
              0);
    res = factory->NewFromCanBeCompressString("1234567890.0006125");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(1234567890.0006125)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("11234567890.000612");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(11234567890.0006125)))->Compare(*res),
              0);
    res = factory->NewFromCanBeCompressString("4.185580496821356");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(4.1855804968213567)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("3.929201589819414");
    ASSERT_EQ(
        base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(3.9292015898194142585311918)))->Compare(*res),
        0);
    res = factory->NewFromCanBeCompressString("0.9999999999999999");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(0.9999999999999999)))->Compare(*res), 0);
    res = factory->NewFromCanBeCompressString("1");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(0.99999999999999999)))->Compare(*res),
              0);
    res = factory->NewFromCanBeCompressString("0.7777777777777778");
    ASSERT_EQ(base::NumberHelper::NumberToString(thread_, JSTaggedValue(double(0.77777777777777777)))->Compare(*res),
              0);
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
