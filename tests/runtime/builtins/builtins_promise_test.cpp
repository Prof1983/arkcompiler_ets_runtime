/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/jobs/micro_job_queue.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
using JSArray = panda::ecmascript::JSArray;

class BuiltinsPromiseTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    PandaVM *instance_ {nullptr};

private:
    EcmaHandleScope *scope_ {nullptr};
};

// native function for race2 then_on_rejected()
JSTaggedValue TestPromiseRaceThenOnRejectd(EcmaRuntimeCallInfo *argv)
{
    JSHandle<JSTaggedValue> result = builtins_common::GetCallArg(argv, 0);
    // 12345 : test case
    EXPECT_EQ(JSTaggedValue::SameValue(result.GetTaggedValue(), JSTaggedValue(12345)), true);
    return JSTaggedValue::Undefined();
}

// native function for all then_on_resolved()
JSTaggedValue TestPromiseAllThenOnResolved(EcmaRuntimeCallInfo *argv)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    JSHandle<JSTaggedValue> array = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> object_array = JSHandle<JSObject>::Cast(array);
    [[maybe_unused]] PropertyDescriptor desc(argv->GetThread());
    [[maybe_unused]] bool result1 = JSObject::GetOwnProperty(
        argv->GetThread(), object_array, JSHandle<JSTaggedValue>(argv->GetThread(), JSTaggedValue(0)), desc);
    EXPECT_TRUE(result1);
    JSHandle<JSTaggedValue> value1 = desc.GetValue();
    // 111 : test case
    EXPECT_EQ(JSTaggedValue::SameValue(value1.GetTaggedValue(), JSTaggedValue(111)), true);
    [[maybe_unused]] bool result2 = JSObject::GetOwnProperty(
        argv->GetThread(), object_array, JSHandle<JSTaggedValue>(argv->GetThread(), JSTaggedValue(1)), desc);
    EXPECT_TRUE(result2);
    JSHandle<JSTaggedValue> value2 = desc.GetValue();
    // 222 : test case
    EXPECT_EQ(JSTaggedValue::SameValue(value2.GetTaggedValue(), JSTaggedValue(222)), true);
    return JSTaggedValue::Undefined();
}

// native function for catch catch_on_rejected()
JSTaggedValue TestPromiseCatch(EcmaRuntimeCallInfo *argv)
{
    JSHandle<JSTaggedValue> result = builtins_common::GetCallArg(argv, 0);
    // 3 : test case
    EXPECT_EQ(JSTaggedValue::SameValue(result.GetTaggedValue(), JSTaggedValue(3)), true);
    return JSTaggedValue::Undefined();
}

// native function for then then_on_resolved()
JSTaggedValue TestPromiseThenOnResolved(EcmaRuntimeCallInfo *argv)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    auto factory = argv->GetThread()->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> result = builtins_common::GetCallArg(argv, 0);
    auto expect = factory->NewFromCanBeCompressString("resolve");
    EXPECT_EQ(JSTaggedValue::SameValue(result.GetTaggedValue(), expect.GetTaggedValue()), true);
    return JSTaggedValue::Undefined();
}

// native function for then then_on_rejected()
JSTaggedValue TestPromiseThenOnRejected(EcmaRuntimeCallInfo *argv)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    auto factory = argv->GetThread()->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> result = builtins_common::GetCallArg(argv, 0);
    auto expect = factory->NewFromCanBeCompressString("reject");
    EXPECT_EQ(JSTaggedValue::SameValue(result.GetTaggedValue(), expect.GetTaggedValue()), true);
    return JSTaggedValue::Undefined();
}

/*
 * @tc.name: Reject1
 * @tc.desc: The reject method receives a number.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, Reject1)
{
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg(thread_, JSTaggedValue(3));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg.GetTaggedValue());

    /// @tc.steps: var p1 = Promise.reject(3).
    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = promise::Reject(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> reject_promise(thread_, result);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseResult(), JSTaggedValue(3)), true);
}

/*
 * @tc.name: Reject2
 * @tc.desc: The reject method receives a promise object.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, Reject2)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    // constructor promise1
    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg1 =
        JSHandle<JSTaggedValue>::Cast(factory->NewFromCanBeCompressString("Promise reject"));

    /// @tc.steps: step1. var p1 = Promise.reject("Promise reject")
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, promise.GetTaggedValue(), 6);
    ecma_runtime_call_info->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, param_msg1.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = promise::Reject(ecma_runtime_call_info.get());
    JSHandle<JSPromise> promise1(thread_, result);
    EXPECT_EQ(JSTaggedValue::SameValue(promise1->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(promise1->GetPromiseResult(), param_msg1.GetTaggedValue()), true);

    /// @tc.steps: step2. var p2 = Promise.reject(p1)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, promise.GetTaggedValue(), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, promise1.GetTaggedValue());
    [[maybe_unused]] auto prev1 = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());

    JSTaggedValue result1 = promise::Reject(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> promise2(thread_, result1);
    EXPECT_NE(*promise1, *promise2);
    EXPECT_EQ(JSTaggedValue::SameValue(promise2->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(
        JSTaggedValue::SameValue(promise2->GetPromiseResult(), JSTaggedValue(promise1.GetTaggedValue().GetRawData())),
        true);
}

/*
 * @tc.name: Resolve1
 * @tc.desc: The resolve method receives a number.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, Resolve1)
{
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg(thread_, JSTaggedValue(5));

    /// @tc.steps: step1. var p1 = Promise.resolve(12345)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = promise::Resolve(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> reject_promise(thread_, result);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::FULFILLED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseResult(), JSTaggedValue(5)), true);
}

/*
 * @tc.name: Resolve2
 * @tc.desc: The resolve method receives a promise object.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, Resolve2)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    // constructor promise1
    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg1 =
        JSHandle<JSTaggedValue>::Cast(factory->NewFromCanBeCompressString("Promise reject"));

    /// @tc.steps: step1. var p1 = Promise.reject("Promise reject")
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, promise.GetTaggedValue(), 6);
    ecma_runtime_call_info->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, param_msg1.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = promise::Reject(ecma_runtime_call_info.get());
    JSHandle<JSPromise> promise1(thread_, result);
    EXPECT_EQ(JSTaggedValue::SameValue(promise1->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(promise1->GetPromiseResult(), param_msg1.GetTaggedValue()), true);

    // promise1 Enter Reject() as a parameter.
    /// @tc.steps: step2. var p2 = Promise.resolve(p1)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, promise.GetTaggedValue(), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, promise1.GetTaggedValue());
    [[maybe_unused]] auto prev1 = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());

    JSTaggedValue result1 = promise::Resolve(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> promise2(thread_, result1);
    EXPECT_EQ(*promise1, *promise2);
    EXPECT_EQ(JSTaggedValue::SameValue(promise2->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(promise2->GetPromiseResult(), param_msg1.GetTaggedValue()), true);
}

/*
 * @tc.name: Race1
 * @tc.desc: The race method receives an array.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, Race1)
{
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg1(thread_, JSTaggedValue(12345));
    JSHandle<JSTaggedValue> param_msg2(thread_, JSTaggedValue(6789));

    /// @tc.steps: step1. var p1 = Promise.reject(12345)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg1.GetTaggedValue());

    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = promise::Reject(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> reject_promise(thread_, result1);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseResult(), JSTaggedValue(12345)), true);

    /// @tc.steps: step2. var p2 = Promise.resolve(6789)
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info2->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info2->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, param_msg2.GetTaggedValue());

    [[maybe_unused]] auto prev_resolve = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = promise::Resolve(ecma_runtime_call_info2.get());
    JSHandle<JSPromise> resolve_promise(thread_, result2);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::FULFILLED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise->GetPromiseResult(), JSTaggedValue(6789)), true);

    /// @tc.steps: step3. Construct an array with two elements p1 and p2. array = [p1. p2]
    JSHandle<JSObject> array(JSArray::ArrayCreate(thread_, JSTaggedNumber(2)));
    PropertyDescriptor desc(thread_, JSHandle<JSTaggedValue>::Cast(reject_promise));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0)), desc);

    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>::Cast(resolve_promise));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), desc1);

    /// @tc.steps: step4. var p3 = Promise.race([p1,p2]);
    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info4->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info4->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, array.GetTaggedValue());

    [[maybe_unused]] auto prev4 = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    JSTaggedValue result4 = promise::Race(ecma_runtime_call_info4.get());
    JSHandle<JSPromise> race_promise(thread_, result4);
    EXPECT_EQ(JSTaggedValue::SameValue(race_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))),
              true);
    EXPECT_EQ(race_promise->GetPromiseResult().IsUndefined(), true);
}

/*
 * @tc.name: Race2
 * @tc.desc: The Race method receives an array, uses the Then method to save the task in the task queue, and outputs
 * the execution result of the queue.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, Race2)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg1(thread_, JSTaggedValue(12345));
    JSHandle<JSTaggedValue> param_msg2(thread_, JSTaggedValue(6789));

    /// @tc.steps: step1. var p1 = Promise.reject(12345)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg1.GetTaggedValue());

    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = promise::Reject(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> reject_promise(thread_, result1);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseResult(), JSTaggedValue(12345)), true);

    /// @tc.steps: step2. var p2 = Promise.resolve(6789)
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info2->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info2->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, param_msg2.GetTaggedValue());

    [[maybe_unused]] auto prev_resolve = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = promise::Resolve(ecma_runtime_call_info2.get());
    JSHandle<JSPromise> resolve_promise(thread_, result2);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::FULFILLED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise->GetPromiseResult(), JSTaggedValue(6789)), true);

    /// @tc.steps: step3. Construct an array with two elements p1 and p2. array = [p1. p2]
    JSHandle<JSObject> array(JSArray::ArrayCreate(thread_, JSTaggedNumber(2)));
    PropertyDescriptor desc(thread_, JSHandle<JSTaggedValue>::Cast(reject_promise));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0)), desc);

    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>::Cast(resolve_promise));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), desc1);

    /// @tc.steps: step4. var p3 = Promise.race([p1,p2]);
    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info4->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info4->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, array.GetTaggedValue());

    [[maybe_unused]] auto prev4 = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    JSTaggedValue result4 = promise::Race(ecma_runtime_call_info4.get());
    JSHandle<JSPromise> race_promise(thread_, result4);
    EXPECT_EQ(JSTaggedValue::SameValue(race_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))),
              true);
    EXPECT_EQ(race_promise->GetPromiseResult().IsUndefined(), true);

    /// @tc.steps: step5. p3.then((resolve)=>{print(resolve)}, (reject)=>{print(reject)})
    JSHandle<JSFunction> native_func_race_then_onrejected =
        factory->NewJSFunction(env, reinterpret_cast<void *>(TestPromiseRaceThenOnRejectd));
    auto ecma_runtime_call_info5 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, race_promise.GetTaggedValue(), 8);
    ecma_runtime_call_info5->SetFunction(race_promise.GetTaggedValue());
    ecma_runtime_call_info5->SetThis(race_promise.GetTaggedValue());
    ecma_runtime_call_info5->SetCallArg(0, JSTaggedValue::Undefined());
    ecma_runtime_call_info5->SetCallArg(1, native_func_race_then_onrejected.GetTaggedValue());

    [[maybe_unused]] auto prev5 = TestHelper::SetupFrame(thread_, ecma_runtime_call_info5.get());
    JSTaggedValue then_result = promise::proto::Then(ecma_runtime_call_info5.get());
    JSHandle<JSPromise> then_promise(thread_, then_result);

    EXPECT_TRUE(JSTaggedValue::SameValue(then_promise->GetPromiseState(),
                                         JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))));
    EXPECT_TRUE(then_promise->GetPromiseResult().IsUndefined());

    /// @tc.steps: step6. execute promise queue
    auto micro_job_queue = EcmaVM::Cast(instance_)->GetMicroJobQueue();
    if (LIKELY(!thread_->HasPendingException())) {
        job::MicroJobQueue::ExecutePendingJob(thread_, micro_job_queue);
    }
}

/*
 * @tc.name: All
 * @tc.desc: The All method receives an array, uses the Then method to save the task in the task queue, and outputs the
 * execution result of the queue.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, All)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg1(thread_, JSTaggedValue(111));
    JSHandle<JSTaggedValue> param_msg2(thread_, JSTaggedValue(222));

    /// @tc.steps: step1. var p1 = Promise.resolve(111)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg1.GetTaggedValue());

    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = promise::Resolve(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> resolve_promise1(thread_, result1);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise1->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::FULFILLED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise1->GetPromiseResult(), JSTaggedValue(111)), true);

    /// @tc.steps: step2. var p2 = Promise.resolve(222)
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info2->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info2->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, param_msg2.GetTaggedValue());

    [[maybe_unused]] auto prev_resolve = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = promise::Resolve(ecma_runtime_call_info2.get());
    JSHandle<JSPromise> resolve_promise2(thread_, result2);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise2->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::FULFILLED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise2->GetPromiseResult(), JSTaggedValue(222)), true);

    /// @tc.steps: step3. Construct an array with two elements p1 and p2. array = [p1. p2]
    JSHandle<JSObject> array(JSArray::ArrayCreate(thread_, JSTaggedNumber(2)));
    PropertyDescriptor desc(thread_, JSHandle<JSTaggedValue>::Cast(resolve_promise1));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0)), desc);

    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>::Cast(resolve_promise2));
    JSArray::DefineOwnProperty(thread_, array, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), desc1);

    /// @tc.steps: step4. var p3 = Promise.all([p1,p2]);
    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info4->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info4->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, array.GetTaggedValue());

    [[maybe_unused]] auto prev4 = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    JSTaggedValue result4 = promise::All(ecma_runtime_call_info4.get());
    JSHandle<JSPromise> all_promise(thread_, result4);
    EXPECT_EQ(JSTaggedValue::SameValue(all_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))),
              true);
    EXPECT_EQ(all_promise->GetPromiseResult().IsUndefined(), true);

    /// @tc.steps: step5. p3.then((resolve)=>{print(resolve)}, (reject)=>{print(reject)});
    JSHandle<JSFunction> native_func_race_then_on_resolved =
        factory->NewJSFunction(env, reinterpret_cast<void *>(TestPromiseAllThenOnResolved));
    auto ecma_runtime_call_info5 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, all_promise.GetTaggedValue(), 8);
    ecma_runtime_call_info5->SetFunction(all_promise.GetTaggedValue());
    ecma_runtime_call_info5->SetThis(all_promise.GetTaggedValue());
    ecma_runtime_call_info5->SetCallArg(0, native_func_race_then_on_resolved.GetTaggedValue());
    ecma_runtime_call_info5->SetCallArg(1, native_func_race_then_on_resolved.GetTaggedValue());

    [[maybe_unused]] auto prev5 = TestHelper::SetupFrame(thread_, ecma_runtime_call_info5.get());
    JSTaggedValue then_result = promise::proto::Then(ecma_runtime_call_info5.get());
    JSHandle<JSPromise> then_promise(thread_, then_result);

    EXPECT_TRUE(JSTaggedValue::SameValue(then_promise->GetPromiseState(),
                                         JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))));
    EXPECT_TRUE(then_promise->GetPromiseResult().IsUndefined());

    /// @tc.steps: step6. execute promise queue
    auto micro_job_queue = EcmaVM::Cast(instance_)->GetMicroJobQueue();
    if (LIKELY(!thread_->HasPendingException())) {
        job::MicroJobQueue::ExecutePendingJob(thread_, micro_job_queue);
    }
}

/*
 * @tc.name: Catch
 * @tc.desc: test Catch() method
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, Catch)
{
    auto env = EcmaVM::Cast(instance_)->GetGlobalEnv();
    auto factory = EcmaVM::Cast(instance_)->GetFactory();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg1(thread_, JSTaggedValue(3));

    /// @tc.steps: step1. var p1 = Promise.reject(3)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg1.GetTaggedValue());

    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = promise::Reject(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> reject_promise(thread_, result);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseResult(), JSTaggedValue(3)), true);

    /// @tc.steps: step2. p1 invokes catch()
    JSHandle<JSFunction> test_promise_catch = factory->NewJSFunction(env, reinterpret_cast<void *>(TestPromiseCatch));
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, reject_promise.GetTaggedValue(), 6);
    ecma_runtime_call_info2->SetFunction(reject_promise.GetTaggedValue());
    ecma_runtime_call_info2->SetThis(reject_promise.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, test_promise_catch.GetTaggedValue());

    [[maybe_unused]] auto prev_catch = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue catch_result = promise::proto::Catch(ecma_runtime_call_info2.get());
    JSHandle<JSPromise> catch_promise(thread_, catch_result);

    EXPECT_EQ(JSTaggedValue::SameValue(catch_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))),
              true);
    EXPECT_EQ(catch_promise->GetPromiseResult().IsUndefined(), true);

    /// @tc.steps: step3. execute promise queue
    auto micro_job_queue = EcmaVM::Cast(instance_)->GetMicroJobQueue();
    if (LIKELY(!thread_->HasPendingException())) {
        job::MicroJobQueue::ExecutePendingJob(thread_, micro_job_queue);
    }
}

/*
 * @tc.name: ThenResolve
 * @tc.desc: Testing the Then() function with the Resolve() function
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, ThenResolve)
{
    auto env = EcmaVM::Cast(instance_)->GetGlobalEnv();
    auto factory = EcmaVM::Cast(instance_)->GetFactory();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg = JSHandle<JSTaggedValue>::Cast(factory->NewFromCanBeCompressString("resolve"));

    /// @tc.steps: step1. var p1 = Promise.resolve("resolve")
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = promise::Resolve(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> resolve_promise(thread_, result);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::FULFILLED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(resolve_promise->GetPromiseResult(), param_msg.GetTaggedValue()), true);

    /// @tc.steps: step2. p1 invokes then()
    JSHandle<JSFunction> test_promise_then_on_resolved =
        factory->NewJSFunction(env, reinterpret_cast<void *>(TestPromiseThenOnResolved));
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, resolve_promise.GetTaggedValue(), 8);
    ecma_runtime_call_info2->SetFunction(resolve_promise.GetTaggedValue());
    ecma_runtime_call_info2->SetThis(resolve_promise.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, test_promise_then_on_resolved.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(1, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev_then = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue then_result = promise::proto::Then(ecma_runtime_call_info2.get());
    JSHandle<JSPromise> then_promise(thread_, then_result);

    EXPECT_EQ(JSTaggedValue::SameValue(then_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))),
              true);
    EXPECT_EQ(then_promise->GetPromiseResult().IsUndefined(), true);

    /// @tc.steps: step3.  execute promise queue
    auto micro_job_queue = EcmaVM::Cast(instance_)->GetMicroJobQueue();
    if (LIKELY(!thread_->HasPendingException())) {
        job::MicroJobQueue::ExecutePendingJob(thread_, micro_job_queue);
    }
}

/*
 * @tc.name: ThenReject
 * @tc.desc: Testing the Then() function with the Reject() function
 * @tc.type: FUNC
 */
TEST_F(BuiltinsPromiseTest, ThenReject)
{
    auto env = EcmaVM::Cast(instance_)->GetGlobalEnv();
    auto factory = EcmaVM::Cast(instance_)->GetFactory();

    JSHandle<JSFunction> promise = JSHandle<JSFunction>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> param_msg = JSHandle<JSTaggedValue>::Cast(factory->NewFromCanBeCompressString("reject"));

    /// @tc.steps: step1. var p1 = Promise.Reject(5)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*promise), 6);
    ecma_runtime_call_info1->SetFunction(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(promise.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev_reject = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = promise::Reject(ecma_runtime_call_info1.get());
    JSHandle<JSPromise> reject_promise(thread_, result);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED))),
              true);
    EXPECT_EQ(JSTaggedValue::SameValue(reject_promise->GetPromiseResult(), param_msg.GetTaggedValue()), true);

    /// @tc.steps: step1. p1 invokes then()
    JSHandle<JSFunction> test_promise_then_on_rejected =
        factory->NewJSFunction(env, reinterpret_cast<void *>(TestPromiseThenOnRejected));
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, reject_promise.GetTaggedValue(), 8);
    ecma_runtime_call_info2->SetFunction(reject_promise.GetTaggedValue());
    ecma_runtime_call_info2->SetThis(reject_promise.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, test_promise_then_on_rejected.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(1, test_promise_then_on_rejected.GetTaggedValue());

    [[maybe_unused]] auto prev_then = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue then_result = promise::proto::Then(ecma_runtime_call_info2.get());
    JSHandle<JSPromise> then_promise(thread_, then_result);

    EXPECT_EQ(JSTaggedValue::SameValue(then_promise->GetPromiseState(),
                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING))),
              true);
    EXPECT_EQ(then_promise->GetPromiseResult().IsUndefined(), true);
    /// @tc.steps: step3.  execute promise queue
    auto micro_job_queue = EcmaVM::Cast(instance_)->GetMicroJobQueue();
    if (LIKELY(!thread_->HasPendingException())) {
        job::MicroJobQueue::ExecutePendingJob(thread_, micro_job_queue);
    }
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
