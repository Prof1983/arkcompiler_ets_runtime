/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/symbol_table-inl.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {

class BuiltinsSymbolTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

// new Symbol.toString()
TEST_F(BuiltinsSymbolTest, SymbolNoParameterToString)
{
    auto ecma_vm = thread_->GetEcmaVM();

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewJSSymbol();

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(symbol.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    ASSERT_TRUE(result.IsString());

    auto symbol_value = ecmascript::builtins_common::GetTaggedString(thread_, "Symbol()");
    ASSERT_EQ(reinterpret_cast<EcmaString *>(symbol_value.GetRawData())->Compare(*result_handle), 0);
}

// new Symbol("aaa").toString()
TEST_F(BuiltinsSymbolTest, SymbolWithParameterToString)
{
    auto ecma_vm = thread_->GetEcmaVM();

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewPublicSymbolWithChar("aaa");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(symbol.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    ASSERT_TRUE(result.IsString());

    auto symbol_value = ecmascript::builtins_common::GetTaggedString(thread_, "Symbol(aaa)");
    ASSERT_EQ(reinterpret_cast<EcmaString *>(symbol_value.GetRawData())->Compare(*result_handle), 0);
}

// new Symbol().valueOf()
TEST_F(BuiltinsSymbolTest, SymbolNoParameterValueOf)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewJSSymbol();

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(symbol.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::proto::ValueOf(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_TRUE(result.IsSymbol());
    ASSERT_EQ(result.GetRawData() == (JSTaggedValue(*symbol)).GetRawData(), true);

    JSHandle<JSFunction> symbol_object(env->GetSymbolFunction());
    JSHandle<JSTaggedValue> symbol_value(symbol);
    JSHandle<JSPrimitiveRef> symbol_ref = ecma_vm->GetFactory()->NewJSPrimitiveRef(symbol_object, symbol_value);

    auto other_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    other_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    other_ecma_runtime_call_info->SetThis(symbol_ref.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, other_ecma_runtime_call_info.get());
    JSTaggedValue other_result = symbol::proto::ValueOf(other_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_TRUE(other_result.IsSymbol());
    ASSERT_EQ(other_result.GetRawData() == (JSTaggedValue(*symbol)).GetRawData(), true);
}

// new Symbol("bbb").valueOf()
TEST_F(BuiltinsSymbolTest, SymbolWithParameterValueOf)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewPublicSymbolWithChar("bbb");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(symbol.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::proto::ValueOf(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_TRUE(result.IsSymbol());
    ASSERT_EQ(result.GetRawData() == (JSTaggedValue(*symbol)).GetRawData(), true);

    JSHandle<JSFunction> symbol_object(env->GetSymbolFunction());
    JSHandle<JSTaggedValue> symbol_value(symbol);
    JSHandle<JSPrimitiveRef> symbol_ref = ecma_vm->GetFactory()->NewJSPrimitiveRef(symbol_object, symbol_value);

    auto other_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    other_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    other_ecma_runtime_call_info->SetThis(symbol_ref.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, other_ecma_runtime_call_info.get());
    JSTaggedValue other_result = symbol::proto::ValueOf(other_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_TRUE(other_result.IsSymbol());
    ASSERT_EQ(other_result.GetRawData() == (JSTaggedValue(*symbol)).GetRawData(), true);
}

// new Symbol().for
TEST_F(BuiltinsSymbolTest, SymbolWithParameterFor)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<SymbolTable> table_handle(env->GetRegisterSymbols());

    JSHandle<EcmaString> string = ecma_vm->GetFactory()->NewFromCanBeCompressString("ccc");
    ASSERT_EQ(string->GetLength(), 3);
    JSHandle<JSTaggedValue> string_handle(string);
    ASSERT_EQ(table_handle->ContainsKey(thread_, string_handle.GetTaggedValue()), false);

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewSymbolWithTableWithChar("ccc");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::For(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(table_handle->ContainsKey(thread_, string_handle.GetTaggedValue()), true);

    JSTaggedValue target(*symbol);
    ASSERT_EQ(result.GetRawData() == target.GetRawData(), true);
}

// Symbol.keyFor (sym)
TEST_F(BuiltinsSymbolTest, SymbolKeyFor)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewPublicSymbolWithChar("bbb");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, symbol.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::KeyFor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    JSHandle<EcmaString> string = ecma_vm->GetFactory()->NewFromCanBeCompressString("ccc");
    ASSERT_EQ(string->GetLength(), 3);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, string.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    symbol::For(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<JSSymbol> other_symbol = ecma_vm->GetFactory()->NewPublicSymbolWithChar("ccc");
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetCallArg(0, other_symbol.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue other_result = symbol::KeyFor(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(other_result.IsString());
    JSHandle<SymbolTable> table_handle(env->GetRegisterSymbols());
    JSTaggedValue string_value(*string);
    ASSERT_EQ(table_handle->ContainsKey(thread_, string_value), true);
}

// Symbol.ToPrimitive()
TEST_F(BuiltinsSymbolTest, SymbolToPrimitive)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewJSSymbol();

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(symbol.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::proto::ToPrimitive(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_TRUE(result.IsSymbol());
    ASSERT_EQ(result.GetRawData() == (JSTaggedValue(*symbol)).GetRawData(), true);

    JSHandle<JSFunction> symbol_object(env->GetSymbolFunction());
    JSHandle<JSTaggedValue> symbol_value(symbol);
    JSHandle<JSPrimitiveRef> symbol_ref = ecma_vm->GetFactory()->NewJSPrimitiveRef(symbol_object, symbol_value);

    auto other_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    other_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    other_ecma_runtime_call_info->SetThis(symbol_ref.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, other_ecma_runtime_call_info.get());
    JSTaggedValue other_result = symbol::proto::ToPrimitive(other_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_TRUE(other_result.IsSymbol());
    ASSERT_EQ(other_result.GetRawData() == (JSTaggedValue(*symbol)).GetRawData(), true);
}

// constructor
TEST_F(BuiltinsSymbolTest, SymbolConstructor)
{
    auto ecma_vm = thread_->GetEcmaVM();

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::SymbolConstructor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_TRUE(result.IsSymbol());
    auto *sym = reinterpret_cast<JSSymbol *>(result.GetRawData());
    ASSERT_EQ(sym->GetDescription().IsUndefined(), true);

    JSHandle<EcmaString> string = ecma_vm->GetFactory()->NewFromCanBeCompressString("ddd");

    auto other_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    other_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    other_ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    other_ecma_runtime_call_info->SetCallArg(0, string.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, other_ecma_runtime_call_info.get());
    JSTaggedValue result1 = symbol::SymbolConstructor(other_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSHandle<EcmaString> result_string = JSTaggedValue::ToString(
        thread_,
        JSHandle<JSTaggedValue>(thread_, reinterpret_cast<JSSymbol *>(result1.GetRawData())->GetDescription()));
    ASSERT_EQ(result_string->Compare(*string), 0);
}

TEST_F(BuiltinsSymbolTest, SymbolGetter)
{
    auto ecma_vm = thread_->GetEcmaVM();

    JSHandle<JSSymbol> symbol = ecma_vm->GetFactory()->NewPublicSymbolWithChar("");
    JSHandle<EcmaString> string = ecma_vm->GetFactory()->NewFromCanBeCompressString("");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(symbol.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = symbol::proto::GetDescription(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result.IsString());
    auto *res_string = reinterpret_cast<EcmaString *>(result.GetRawData());
    ASSERT_EQ(res_string->GetLength(), 0);
    ASSERT_EQ(EcmaString::StringsAreEqual(res_string, *string), true);
}
}  // namespace panda::test
