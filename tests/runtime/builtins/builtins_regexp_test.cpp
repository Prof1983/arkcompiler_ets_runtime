/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_regexp.h"

#include "plugins/ecmascript/runtime/builtins/builtins_regexp.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/regexp/regexp_parser_cache.h"

#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {
class BuiltinsRegExpTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSTaggedValue CreateBuiltinsRegExpObjByPatternAndFlags(JSThread *thread, const JSHandle<EcmaString> &pattern,
                                                       const JSHandle<EcmaString> &flags)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> regexp(env->GetRegExpFunction());
    JSHandle<JSObject> global_object(thread, env->GetGlobalObject());
    // make ecma_runtime_call_info
    // 8 : test case
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue(*regexp), 8);
    ecma_runtime_call_info->SetFunction(regexp.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, pattern.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, flags.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    // invoke RegExpConstructor method
    JSTaggedValue result = reg_exp::RegExpConstructor(ecma_runtime_call_info.get());
    return result;
}

TEST_F(BuiltinsRegExpTest, RegExpConstructor1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("\\w+");
    JSHandle<EcmaString> flags = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("i");
    JSTaggedValue result = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern, flags);

    // ASSERT IsRegExp()
    JSHandle<JSTaggedValue> regexp_object(thread_, result);
    ASSERT_TRUE(JSObject::IsRegExp(thread_, regexp_object));

    JSHandle<JSRegExp> js_regexp(thread_, JSRegExp::Cast(regexp_object->GetTaggedObject()));
    JSHandle<JSTaggedValue> original_source(thread_, js_regexp->GetOriginalSource());
    uint8_t flags_bits = static_cast<uint8_t>(js_regexp->GetOriginalFlags().GetInt());
    JSHandle<JSTaggedValue> original_flags(thread_, reg_exp::FlagsBitsToString(thread_, flags_bits));
    ASSERT_EQ(static_cast<EcmaString *>(original_source->GetTaggedObject())->Compare(*pattern), 0);
    ASSERT_EQ(static_cast<EcmaString *>(original_flags->GetTaggedObject())->Compare(*flags), 0);
}

TEST_F(BuiltinsRegExpTest, RegExpConstructor2)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("\\w+");
    JSHandle<EcmaString> flags = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("i");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern, flags);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> regexp(env->GetRegExpFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*regexp), 8);
    ecma_runtime_call_info->SetFunction(regexp.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke RegExpConstructor method
    JSTaggedValue result2 = reg_exp::RegExpConstructor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    // ASSERT IsRegExp()
    JSHandle<JSTaggedValue> regexp_object(thread_, result2);
    ASSERT_TRUE(JSObject::IsRegExp(thread_, regexp_object));

    JSHandle<JSRegExp> js_regexp(thread_, JSRegExp::Cast(regexp_object->GetTaggedObject()));
    JSHandle<JSTaggedValue> original_source(thread_, js_regexp->GetOriginalSource());
    uint8_t flags_bits = static_cast<uint8_t>(js_regexp->GetOriginalFlags().GetInt());
    JSHandle<JSTaggedValue> original_flags(thread_, reg_exp::FlagsBitsToString(thread_, flags_bits));
    ASSERT_EQ(static_cast<EcmaString *>(original_source->GetTaggedObject())->Compare(*pattern), 0);
    ASSERT_EQ(static_cast<EcmaString *>(original_flags->GetTaggedObject())->Compare(*flags), 0);
}

TEST_F(BuiltinsRegExpTest, RegExpConstructor3)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("\\w+");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("i");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> regexp(env->GetRegExpFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    JSHandle<EcmaString> flags2 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("gi");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*regexp), 8);
    ecma_runtime_call_info->SetFunction(regexp.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, flags2.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke RegExpConstructor method
    JSTaggedValue result2 = reg_exp::RegExpConstructor(ecma_runtime_call_info.get());

    // ASSERT IsRegExp()
    JSHandle<JSTaggedValue> regexp_object(thread_, result2);
    ASSERT_TRUE(JSObject::IsRegExp(thread_, regexp_object));

    JSHandle<JSRegExp> js_regexp(thread_, JSRegExp::Cast(regexp_object->GetTaggedObject()));
    JSHandle<JSTaggedValue> original_source(thread_, js_regexp->GetOriginalSource());
    uint8_t flags_bits = static_cast<uint8_t>(js_regexp->GetOriginalFlags().GetInt());
    JSHandle<JSTaggedValue> original_flags(thread_, reg_exp::FlagsBitsToString(thread_, flags_bits));
    ASSERT_EQ(static_cast<EcmaString *>(original_source->GetTaggedObject())->Compare(*pattern1), 0);
    ASSERT_EQ(static_cast<EcmaString *>(original_flags->GetTaggedObject())->Compare(*flags2), 0);
}

TEST_F(BuiltinsRegExpTest, GetSource1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("i");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSTaggedValue> result1_handle(thread_, result1);

    // invoke GetSource method
    JSHandle<JSTaggedValue> source(
        thread_, thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("source").GetTaggedValue());
    JSHandle<JSTaggedValue> source_result(JSObject::GetProperty(thread_, result1_handle, source).GetValue());

    JSHandle<EcmaString> expect = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("(?:)");
    ASSERT_EQ(static_cast<EcmaString *>(source_result->GetTaggedObject())->Compare(*expect), 0);
}

TEST_F(BuiltinsRegExpTest, GetSource2)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("/w+");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("i");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSTaggedValue> result1_handle(thread_, result1);

    // invoke GetSource method
    JSHandle<JSTaggedValue> source(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("source"));
    JSHandle<JSTaggedValue> source_result(JSObject::GetProperty(thread_, result1_handle, source).GetValue());

    JSHandle<EcmaString> expect = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("\\/w+");
    ASSERT_EQ(static_cast<EcmaString *>(source_result->GetTaggedObject())->Compare(*expect), 0);
}

TEST_F(BuiltinsRegExpTest, Get)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("\\w+");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("gimuy");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSTaggedValue> result1_handle(thread_, result1);

    JSHandle<JSTaggedValue> global(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("global"));
    JSTaggedValue tagged_global_result =
        JSTaggedValue(JSObject::GetProperty(thread_, result1_handle, global).GetValue().GetTaggedValue());
    ASSERT_EQ(tagged_global_result.GetRawData(), JSTaggedValue::True().GetRawData());

    JSHandle<JSTaggedValue> ignore_case(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("ignoreCase"));
    JSTaggedValue tagged_ignore_case_result =
        JSTaggedValue(JSObject::GetProperty(thread_, result1_handle, ignore_case).GetValue().GetTaggedValue());
    ASSERT_EQ(tagged_ignore_case_result.GetRawData(), JSTaggedValue::True().GetRawData());

    JSHandle<JSTaggedValue> multiline(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("multiline"));
    JSTaggedValue tagged_multiline_result =
        JSTaggedValue(JSObject::GetProperty(thread_, result1_handle, multiline).GetValue().GetTaggedValue());
    ASSERT_EQ(tagged_multiline_result.GetRawData(), JSTaggedValue::True().GetRawData());

    JSHandle<JSTaggedValue> sticky(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("sticky"));
    JSTaggedValue tagged_sticky_result =
        JSTaggedValue(JSObject::GetProperty(thread_, result1_handle, sticky).GetValue().GetTaggedValue());
    ASSERT_EQ(tagged_sticky_result.GetRawData(), JSTaggedValue::True().GetRawData());

    JSHandle<JSTaggedValue> unicode(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("unicode"));
    JSTaggedValue tagged_unicode_result =
        JSTaggedValue(JSObject::GetProperty(thread_, result1_handle, unicode).GetValue().GetTaggedValue());
    ASSERT_EQ(tagged_unicode_result.GetRawData(), JSTaggedValue::True().GetRawData());
}

TEST_F(BuiltinsRegExpTest, GetFlags)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("\\w+");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("imuyg");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSTaggedValue> result1_handle(thread_, result1);

    // invoke GetFlags method
    JSHandle<JSTaggedValue> flags(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("flags"));
    JSHandle<JSTaggedValue> flags_result(JSObject::GetProperty(thread_, result1_handle, flags).GetValue());

    JSHandle<EcmaString> expect_result = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("gimuy");
    ASSERT_EQ(static_cast<EcmaString *>(flags_result->GetTaggedObject())->Compare(*expect_result), 0);
}

TEST_F(BuiltinsRegExpTest, toString)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("\\w+");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("imuyg");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke ToString method
    JSTaggedValue to_string_result = reg_exp::proto::ToString(ecma_runtime_call_info.get());
    ASSERT_TRUE(to_string_result.IsString());
    JSHandle<JSTaggedValue> to_string_result_handle(thread_, to_string_result);
    JSHandle<EcmaString> expect_result = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("/\\w+/gimuy");
    ASSERT_EQ(static_cast<EcmaString *>(to_string_result_handle->GetTaggedObject())->Compare(*expect_result), 0);
}

TEST_F(BuiltinsRegExpTest, Exec1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("quick\\s(brown).+?(jumps)");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("ig");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("The Quick Brown Fox Jumps Over The Lazy Dog");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke Exec method
    JSTaggedValue results = reg_exp::proto::Exec(ecma_runtime_call_info.get());

    JSHandle<JSTaggedValue> exec_result(thread_, results);
    JSHandle<EcmaString> result_zero =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Quick Brown Fox Jumps");
    JSHandle<EcmaString> result_one = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Brown");
    JSHandle<EcmaString> result_two = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Jumps");

    JSHandle<JSTaggedValue> index(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("index"));
    JSHandle<JSTaggedValue> index_handle(JSObject::GetProperty(thread_, exec_result, index).GetValue());
    uint32_t result_index = JSTaggedValue::ToUint32(thread_, index_handle);
    ASSERT_TRUE(result_index == 4U);

    JSHandle<JSTaggedValue> input(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("input"));

    JSHandle<JSTaggedValue> input_handle(JSObject::GetProperty(thread_, exec_result, input).GetValue());
    JSHandle<EcmaString> output_input = JSTaggedValue::ToString(thread_, input_handle);
    ASSERT_EQ(output_input->Compare(*input_string), 0);

    JSHandle<JSTaggedValue> zero(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0"));
    JSHandle<JSTaggedValue> zero_handle(JSObject::GetProperty(thread_, exec_result, zero).GetValue());
    JSHandle<EcmaString> output_zero = JSTaggedValue::ToString(thread_, zero_handle);
    ASSERT_EQ(output_zero->Compare(*result_zero), 0);

    JSHandle<JSTaggedValue> first(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1"));
    JSHandle<JSTaggedValue> one_handle(JSObject::GetProperty(thread_, exec_result, first).GetValue());
    JSHandle<EcmaString> output_one = JSTaggedValue::ToString(thread_, one_handle);
    ASSERT_EQ(output_one->Compare(*result_one), 0);

    JSHandle<JSTaggedValue> second(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2"));
    JSHandle<JSTaggedValue> two_handle(JSObject::GetProperty(thread_, exec_result, second).GetValue());
    JSHandle<EcmaString> output_two = JSTaggedValue::ToString(thread_, two_handle);
    ASSERT_EQ(output_two->Compare(*result_two), 0);

    JSHandle<JSTaggedValue> regexp = JSHandle<JSTaggedValue>::Cast(value);
    JSHandle<JSTaggedValue> last_index_handle(
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("lastIndex"));
    JSHandle<JSTaggedValue> last_index_obj(JSObject::GetProperty(thread_, regexp, last_index_handle).GetValue());
    int last_index = last_index_obj->GetInt();
    ASSERT_TRUE(last_index == 25);
}

TEST_F(BuiltinsRegExpTest, Exec2)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("((1)|(12))((3)|(23))");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("ig");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("123");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke Exec method
    JSTaggedValue results = reg_exp::proto::Exec(ecma_runtime_call_info.get());

    JSHandle<JSTaggedValue> exec_result(thread_, results);
    JSHandle<EcmaString> result_zero = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("123");
    JSHandle<EcmaString> result_one = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1");
    JSHandle<EcmaString> result_two = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1");
    JSHandle<EcmaString> result_four = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("23");
    JSHandle<EcmaString> result_six = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("23");

    JSHandle<JSTaggedValue> index(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("index"));
    JSHandle<JSTaggedValue> index_handle(JSObject::GetProperty(thread_, exec_result, index).GetValue());
    uint32_t result_index = JSTaggedValue::ToUint32(thread_, index_handle);
    ASSERT_TRUE(result_index == 0U);

    JSHandle<JSTaggedValue> input(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("input"));
    JSHandle<JSTaggedValue> input_handle(JSObject::GetProperty(thread_, exec_result, input).GetValue());
    JSHandle<EcmaString> output_input = JSTaggedValue::ToString(thread_, input_handle);
    ASSERT_EQ(output_input->Compare(*input_string), 0);

    JSHandle<JSTaggedValue> zero(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0"));
    JSHandle<JSTaggedValue> zero_handle(JSObject::GetProperty(thread_, exec_result, zero).GetValue());
    JSHandle<EcmaString> output_zero = JSTaggedValue::ToString(thread_, zero_handle);
    ASSERT_EQ(output_zero->Compare(*result_zero), 0);

    JSHandle<JSTaggedValue> first(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1"));
    JSHandle<JSTaggedValue> one_handle(JSObject::GetProperty(thread_, exec_result, first).GetValue());
    JSHandle<EcmaString> output_one = JSTaggedValue::ToString(thread_, one_handle);
    ASSERT_EQ(output_one->Compare(*result_one), 0);

    JSHandle<JSTaggedValue> second(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2"));
    JSHandle<JSTaggedValue> two_handle(JSObject::GetProperty(thread_, exec_result, second).GetValue());
    JSHandle<EcmaString> output_two = JSTaggedValue::ToString(thread_, two_handle);
    ASSERT_EQ(output_two->Compare(*result_two), 0);

    JSHandle<JSTaggedValue> regexp = JSHandle<JSTaggedValue>::Cast(value);
    JSHandle<JSTaggedValue> last_index_handle(
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("lastIndex"));
    JSHandle<JSTaggedValue> last_index_obj(JSObject::GetProperty(thread_, regexp, last_index_handle).GetValue());
    int last_index = last_index_obj->GetInt();
    ASSERT_TRUE(last_index == 3);

    JSHandle<JSTaggedValue> third(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("3"));
    JSHandle<JSTaggedValue> third_handle(JSObject::GetProperty(thread_, exec_result, third).GetValue());
    ASSERT_TRUE(third_handle->IsUndefined());

    JSHandle<JSTaggedValue> four(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("4"));
    JSHandle<JSTaggedValue> four_handle(JSObject::GetProperty(thread_, exec_result, four).GetValue());
    JSHandle<EcmaString> output_four = JSTaggedValue::ToString(thread_, four_handle);
    ASSERT_EQ(output_four->Compare(*result_four), 0);

    JSHandle<JSTaggedValue> five(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("5"));
    JSHandle<JSTaggedValue> five_handle(JSObject::GetProperty(thread_, exec_result, five).GetValue());
    ASSERT_TRUE(five_handle->IsUndefined());

    JSHandle<JSTaggedValue> six(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("6"));
    JSHandle<JSTaggedValue> six_handle(JSObject::GetProperty(thread_, exec_result, six).GetValue());
    JSHandle<EcmaString> output_six = JSTaggedValue::ToString(thread_, six_handle);
    ASSERT_EQ(output_six->Compare(*result_six), 0);
}

TEST_F(BuiltinsRegExpTest, Match1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("quick\\s(brown).+?(jumps)");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("The Quick Brown Fox Jumps Over The Lazy Dog");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke Match method
    JSTaggedValue match_results = reg_exp::proto::Match(ecma_runtime_call_info.get());

    JSHandle<JSTaggedValue> match_result(thread_, match_results);
    JSHandle<JSTaggedValue> zero(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0"));
    JSHandle<EcmaString> result_zero =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Quick Brown Fox Jumps");
    JSHandle<JSTaggedValue> zero_handle(JSObject::GetProperty(thread_, match_result, zero).GetValue());
    JSHandle<EcmaString> output_zero = JSTaggedValue::ToString(thread_, zero_handle);
    ASSERT_EQ(output_zero->Compare(*result_zero), 0);
}

TEST_F(BuiltinsRegExpTest, Test1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("quick\\s(brown).+?(jumps)");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("The Quick Brown Fox Jumps Over The Lazy Dog");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke Test method
    JSTaggedValue test_result = reg_exp::proto::Test(ecma_runtime_call_info.get());
    ASSERT_EQ(test_result.GetRawData(), JSTaggedValue::True().GetRawData());
}

TEST_F(BuiltinsRegExpTest, Search1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("quick\\s(brown).+?(jumps)");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("The Quick Brown Fox Jumps Over The Lazy Dog");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke Search method
    JSTaggedValue search_result = reg_exp::proto::Search(ecma_runtime_call_info.get());
    ASSERT_EQ(search_result.GetRawData(), JSTaggedValue(4).GetRawData());
}

TEST_F(BuiltinsRegExpTest, Split1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke Split method
    JSTaggedValue split_results = reg_exp::proto::Split(ecma_runtime_call_info.get());
    JSHandle<JSTaggedValue> split_result(thread_, split_results);

    JSHandle<JSTaggedValue> zero(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0"));
    JSHandle<JSTaggedValue> zero_handle(JSObject::GetProperty(thread_, split_result, zero).GetValue());
    JSHandle<EcmaString> output_zero = JSTaggedValue::ToString(thread_, zero_handle);

    ASSERT_EQ(output_zero->Compare(*input_string), 0);
}

TEST_F(BuiltinsRegExpTest, Split2)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("a-b-c");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke Split method
    JSTaggedValue split_results = reg_exp::proto::Split(ecma_runtime_call_info.get());
    JSHandle<JSTaggedValue> split_result(thread_, split_results);
    JSHandle<EcmaString> result_zero = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("a");
    JSHandle<EcmaString> result_one = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("b");
    JSHandle<EcmaString> result_two = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("c");

    JSHandle<JSTaggedValue> zero(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("0"));
    JSHandle<JSTaggedValue> zero_handle(JSObject::GetProperty(thread_, split_result, zero).GetValue());
    JSHandle<EcmaString> output_zero = JSTaggedValue::ToString(thread_, zero_handle);
    ASSERT_EQ(output_zero->Compare(*result_zero), 0);

    JSHandle<JSTaggedValue> first(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1"));
    JSHandle<JSTaggedValue> one_handle(JSObject::GetProperty(thread_, split_result, first).GetValue());
    JSHandle<EcmaString> output_one = JSTaggedValue::ToString(thread_, one_handle);
    ASSERT_EQ(output_one->Compare(*result_one), 0);

    JSHandle<JSTaggedValue> second(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2"));
    JSHandle<JSTaggedValue> two_handle(JSObject::GetProperty(thread_, split_result, second).GetValue());
    JSHandle<EcmaString> output_two = JSTaggedValue::ToString(thread_, two_handle);
    ASSERT_EQ(output_two->Compare(*result_two), 0);
}

TEST_F(BuiltinsRegExpTest, GetSpecies)
{
    // invoke RegExpConstructor method
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> species_symbol = env->GetSpeciesSymbol();
    EXPECT_TRUE(!species_symbol.GetTaggedValue().IsUndefined());

    JSHandle<JSFunction> new_target(env->GetRegExpFunction());

    JSTaggedValue value =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(new_target), species_symbol).GetValue().GetTaggedValue();
    EXPECT_EQ(value, new_target.GetTaggedValue());
}

TEST_F(BuiltinsRegExpTest, Replace1)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("quick\\s(brown).+?(jumps)");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("The Quick Brown Fox Jumps Over The Lazy Dog");
    JSHandle<EcmaString> replace_string =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("$&a $` $\' $2 $01 $$1 $21 $32 a");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke replace method
    JSTaggedValue results = reg_exp::proto::Replace(ecma_runtime_call_info.get());
    JSHandle<JSTaggedValue> replace_result(thread_, results);
    JSHandle<EcmaString> result_zero = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(
        "The Quick Brown Fox Jumpsa The   Over The Lazy Dog Jumps Brown $1 Jumps1 $32 a Over The Lazy Dog");
    ASSERT_EQ(static_cast<EcmaString *>(replace_result->GetTaggedObject())->Compare(*result_zero), 0);
}

TEST_F(BuiltinsRegExpTest, Replace2)
{
    // invoke RegExpConstructor method
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> pattern1 = factory->NewFromCanBeCompressString("b(c)(z)?(.)");
    JSHandle<EcmaString> flags1 = factory->NewFromCanBeCompressString("");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string = factory->NewFromCanBeCompressString("abcde");
    JSHandle<EcmaString> replace_string = factory->NewFromCanBeCompressString("[$01$02$03$04$00]");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke replace method
    JSTaggedValue results = reg_exp::proto::Replace(ecma_runtime_call_info.get());
    JSHandle<JSTaggedValue> replace_result(thread_, results);
    JSHandle<EcmaString> result_zero = factory->NewFromCanBeCompressString("a[cd$04$00]e");
    ASSERT_EQ(static_cast<EcmaString *>(replace_result->GetTaggedObject())->Compare(*result_zero), 0);
}

TEST_F(BuiltinsRegExpTest, Replace3)
{
    // invoke RegExpConstructor method
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> pattern1 = factory->NewFromCanBeCompressString("abc");
    JSHandle<EcmaString> flags1 = factory->NewFromCanBeCompressString("g");
    JSTaggedValue result1 = CreateBuiltinsRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> value(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));

    JSHandle<EcmaString> input_string = factory->NewFromCanBeCompressString("abcde");
    JSHandle<EcmaString> replace_string = factory->NewFromCanBeCompressString("");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(value.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, input_string.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    // invoke replace method
    JSTaggedValue results = reg_exp::proto::Replace(ecma_runtime_call_info.get());
    JSHandle<JSTaggedValue> replace_result(thread_, results);
    JSHandle<EcmaString> result_zero = factory->NewFromCanBeCompressString("de");
    ASSERT_EQ(static_cast<EcmaString *>(replace_result->GetTaggedObject())->Compare(*result_zero), 0);
}

TEST_F(BuiltinsRegExpTest, RegExpParseCache)
{
    RegExpParserCache *reg_exp_parser_cache = thread_->GetEcmaVM()->GetRegExpParserCache();
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> string1 = factory->NewFromString("abc");
    JSHandle<EcmaString> string2 = factory->NewFromString("abcd");
    PandaVector<PandaString> vec {};
    reg_exp_parser_cache->SetCache(*string1, 0, JSTaggedValue::True(), 2, std::move(vec));
    ASSERT_TRUE(reg_exp_parser_cache->GetCache(*string1, 0, vec).first == JSTaggedValue::True());
    ASSERT_TRUE(reg_exp_parser_cache->GetCache(*string1, 0, vec).second == 2U);
    ASSERT_TRUE(reg_exp_parser_cache->GetCache(*string1, RegExpParserCache::CACHE_SIZE, vec).first ==
                JSTaggedValue::Hole());
    ASSERT_TRUE(reg_exp_parser_cache->GetCache(*string2, 0, vec).first == JSTaggedValue::Hole());
}
}  // namespace panda::test
