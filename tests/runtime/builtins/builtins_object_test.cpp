/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "file_items.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
class BuiltinsObjectTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSTaggedValue CreateBuiltinJSObject(JSThread *thread, const PandaString &key_c_str)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> obj_fun = global_env->GetObjectFunction();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> obj(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun));
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(&key_c_str[0]));
    JSHandle<JSTaggedValue> value(thread, JSTaggedValue(1));
    JSObject::SetProperty(thread, obj, key, value);
    return obj.GetTaggedValue();
}

JSFunction *BuiltinsObjectTestCreate(JSThread *thread)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    return global_env->GetObjectFunction().GetObject<JSFunction>();
}

JSObject *TestNewJSObject(JSThread *thread, const JSHandle<JSHClass> &dyn_class)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSObject *obj = JSObject::Cast(factory->NewDynObject(dyn_class));

    obj->SetElements(thread, factory->EmptyArray().GetTaggedValue(), SKIP_BARRIER);
    return obj;
}

// 19.1.1.1Object ( [ value ] )
TEST_F(BuiltinsObjectTest, ObjectConstructor)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSFunction> object_func(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<GlobalEnv> global_env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> obj_fun(global_env->GetObjectFunction());

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::ObjectConstructor(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSObject> jt_handle(thread_, JSTaggedValue(reinterpret_cast<TaggedObject *>(result.GetRawData())));
    JSTaggedValue result_proto = jt_handle->GetPrototype(thread_);
    JSTaggedValue func_proto = object_func->GetFunctionPrototype();
    ASSERT_EQ(result_proto, func_proto);
    ASSERT_TRUE(jt_handle->IsExtensible());

    // num_args = 0
    JSHandle<JSObject> object =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>::Cast(function), function);
    auto tg_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*object), 4);
    tg_obj_call_info->SetFunction(JSTaggedValue(*obj_fun));
    tg_obj_call_info->SetThis(JSTaggedValue::Undefined());
    tg_obj_call_info->SetNewTarget(JSTaggedValue(*obj_fun));

    prev = TestHelper::SetupFrame(thread_, tg_obj_call_info.get());
    JSTaggedValue result_tg = object::ObjectConstructor(tg_obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result_tg.IsObject());
    JSHandle<JSObject> jt_handle_tg(thread_, JSTaggedValue(reinterpret_cast<TaggedObject *>(result_tg.GetRawData())));
    JSTaggedValue result_proto_tg = jt_handle_tg->GetPrototype(thread_);
    JSTaggedValue func_proto_tg = object_func->GetFunctionPrototype();
    ASSERT_EQ(result_proto_tg, func_proto_tg);
    ASSERT_TRUE(jt_handle_tg->IsExtensible());

    // value is null
    JSHandle<JSObject> object_vn =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>::Cast(function), function);
    auto vn_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*object_vn), 6);
    vn_obj_call_info->SetFunction(JSTaggedValue(*obj_fun));
    vn_obj_call_info->SetThis(JSTaggedValue::Undefined());
    vn_obj_call_info->SetCallArg(0, JSTaggedValue::Null());
    vn_obj_call_info->SetNewTarget(JSTaggedValue(*obj_fun));

    prev = TestHelper::SetupFrame(thread_, vn_obj_call_info.get());
    JSTaggedValue result_vn = object::ObjectConstructor(vn_obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_vn.IsObject());
    JSHandle<JSObject> jt_handle_vn(thread_, JSTaggedValue(reinterpret_cast<TaggedObject *>(result_vn.GetRawData())));
    JSTaggedValue result_proto_vn = jt_handle_vn->GetPrototype(thread_);
    JSTaggedValue func_proto_vn = object_func->GetFunctionPrototype();
    ASSERT_EQ(result_proto_vn, func_proto_vn);
    ASSERT_TRUE(jt_handle_vn->IsExtensible());
}

// 19.1.2.1Object.assign ( target, ...sources )
TEST_F(BuiltinsObjectTest, Assign)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSObject> obj_handle1 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    JSHandle<JSObject> obj_handle2 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);

    JSHandle<JSTaggedValue> key1(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("x"));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(1));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj_handle1), key1, value1);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj_handle1), key1).GetValue()->GetInt(), 1);

    JSHandle<JSTaggedValue> key2(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("y"));
    JSHandle<JSTaggedValue> value2(thread_, JSTaggedValue(2));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj_handle2), key2, value2);
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj_handle2), key2).GetValue()->GetInt(), 2);

    auto assign_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    assign_obj_call_info->SetFunction(JSTaggedValue::Undefined());
    assign_obj_call_info->SetThis(JSTaggedValue::Undefined());
    assign_obj_call_info->SetCallArg(0, obj_handle1.GetTaggedValue());
    assign_obj_call_info->SetCallArg(1, obj_handle2.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, assign_obj_call_info.get());
    JSTaggedValue result = object::Assign(assign_obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSTaggedValue> jt_handle(thread_, JSTaggedValue(reinterpret_cast<TaggedObject *>(result.GetRawData())));
    EXPECT_EQ(JSObject::GetProperty(thread_, jt_handle, key1).GetValue()->GetInt(), 1);
    EXPECT_EQ(JSObject::GetProperty(thread_, jt_handle, key2).GetValue()->GetInt(), 2);
}

// 19.1.2.2Object.create ( O [ , Properties ] )
TEST_F(BuiltinsObjectTest, Create)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSFunction> object_func(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSTaggedValue> func_proto(thread_, object_func->GetFunctionPrototype());

    // no prop
    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, func_proto.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::Create(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSObject> jt_handle(thread_, JSTaggedValue(reinterpret_cast<TaggedObject *>(result.GetRawData())));
    JSTaggedValue result_proto = jt_handle->GetPrototype(thread_);
    ASSERT_EQ(result_proto, func_proto.GetTaggedValue());

    // has prop
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("prop"));
    JSHandle<JSObject> obj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>::Cast(function), function);
    EXPECT_TRUE(*obj_handle != nullptr);

    PropertyDescriptor desc(thread_);
    desc.SetWritable(false);
    JSHandle<JSObject> desc_handle(JSObject::FromPropertyDescriptor(thread_, desc));

    PropertyDescriptor desc_nw(thread_, JSHandle<JSTaggedValue>::Cast(desc_handle), true, true, true);
    JSObject::DefineOwnProperty(thread_, obj_handle, key, desc_nw);

    auto hp_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    hp_obj_call_info->SetFunction(JSTaggedValue::Undefined());
    hp_obj_call_info->SetThis(JSTaggedValue::Undefined());
    hp_obj_call_info->SetCallArg(0, func_proto.GetTaggedValue());
    hp_obj_call_info->SetCallArg(1, obj_handle.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, hp_obj_call_info.get());
    JSTaggedValue result_hp = object::Create(hp_obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_hp.IsObject());
    PropertyDescriptor desc_res(thread_);
    bool success = JSObject::GetOwnProperty(thread_, JSHandle<JSObject>(thread_, result_hp), key, desc_res);
    EXPECT_TRUE(success);
    EXPECT_TRUE(!desc_res.IsWritable());

    // undefined
    auto un_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    un_call_info->SetFunction(JSTaggedValue::Undefined());
    un_call_info->SetThis(JSTaggedValue::Undefined());
    un_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    prev = TestHelper::SetupFrame(thread_, un_call_info.get());
    JSTaggedValue result_un = object::Create(un_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result_un.GetRawData(), JSTaggedValue::VALUE_EXCEPTION);
}

// 19.1.2.3Object.defineProperties ( O, Properties )
TEST_F(BuiltinsObjectTest, DefineProperties)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> obj_func(env->GetObjectFunction());
    JSHandle<JSObject> obj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("prop"));
    JSHandle<JSObject> jsobj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);

    PropertyDescriptor desc(thread_);
    desc.SetWritable(false);
    JSHandle<JSObject> desc_handle(JSObject::FromPropertyDescriptor(thread_, desc));

    PropertyDescriptor desc_nw(thread_, JSHandle<JSTaggedValue>::Cast(desc_handle), true, true, true);
    JSObject::DefineOwnProperty(thread_, jsobj_handle, key, desc_nw);

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, obj_handle.GetTaggedValue());
    obj_call_info->SetCallArg(1, jsobj_handle.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::DefineProperties(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
    JSTaggedValue res(reinterpret_cast<TaggedObject *>(result.GetRawData()));
    PropertyDescriptor desc_res(thread_);
    JSObject::GetOwnProperty(thread_, JSHandle<JSObject>(thread_, res), key, desc_res);
    EXPECT_TRUE(!desc_res.IsWritable());
}

// 19.1.2.4Object.defineProperty ( O, P, Attributes )
TEST_F(BuiltinsObjectTest, DefineProperty)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));

    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> obj_func(env->GetObjectFunction());
    JSHandle<JSObject> att_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    JSHandle<JSObject> obj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);

    PropertyDescriptor desc(thread_);
    desc.SetWritable(true);
    JSHandle<JSTaggedValue> writable_str = thread_->GlobalConstants()->GetHandledWritableString();
    JSHandle<JSTaggedValue> writable(thread_, JSTaggedValue(desc.IsWritable()));
    JSObject::CreateDataProperty(thread_, att_handle, writable_str, writable);

    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("x"));

    PropertyDescriptor desc_nw(thread_);
    JSObject::GetOwnProperty(thread_, obj_handle, key, desc_nw);
    EXPECT_TRUE(!desc_nw.HasWritable());

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, obj_handle.GetTaggedValue());
    obj_call_info->SetCallArg(1, key.GetTaggedValue());
    obj_call_info->SetCallArg(2, att_handle.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::DefineProperty(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
    JSTaggedValue res(reinterpret_cast<TaggedObject *>(result.GetRawData()));
    PropertyDescriptor desc_res(thread_);
    JSObject::GetOwnProperty(thread_, JSHandle<JSObject>(thread_, res), key, desc_res);
    EXPECT_TRUE(desc_res.HasWritable());
}

// B.2.2.2 Object.prototype.__defineGetter__ ( P, getter )
TEST_F(BuiltinsObjectTest, DefineGetter)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSObject> obj(thread_, CreateBuiltinJSObject(thread_, "prop"));
    JSHandle<JSTaggedValue> key(factory->NewFromString("prop"));
    JSHandle<JSTaggedValue> getter(factory->NewJSFunction(thread_->GetEcmaVM()->GetGlobalEnv()));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, getter.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::__defineGetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);
}

// B.2.2.3 Object.prototype.__defineSetter__ ( P, setter )
TEST_F(BuiltinsObjectTest, DefineSetter)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSObject> obj(thread_, CreateBuiltinJSObject(thread_, "prop"));
    JSHandle<JSTaggedValue> key(factory->NewFromString("prop"));
    JSHandle<JSTaggedValue> setter(factory->NewJSFunction(thread_->GetEcmaVM()->GetGlobalEnv()));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, setter.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::__defineSetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);
}

// B.2.2.4 Object.prototype.__lookupGetter__ ( P )
TEST_F(BuiltinsObjectTest, LookupGetter)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSObject> obj(thread_, CreateBuiltinJSObject(thread_, "prop"));
    JSHandle<JSTaggedValue> key(factory->NewFromString("prop"));
    JSHandle<JSTaggedValue> getter(factory->NewJSFunction(thread_->GetEcmaVM()->GetGlobalEnv()));

    // First - getter should not be defined.
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::__lookupGetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    // Second - set a getter function.
    ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, getter.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    result = object::proto::__defineGetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    // Third - check the getter function if it really exists.
    ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    result = object::proto::__lookupGetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result, getter.GetTaggedValue());
}

// B.2.2.5 Object.prototype.__lookupSetter__ ( P )
TEST_F(BuiltinsObjectTest, LookupSetter)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSObject> obj(thread_, CreateBuiltinJSObject(thread_, "prop"));
    JSHandle<JSTaggedValue> key(factory->NewFromString("prop"));
    JSHandle<JSTaggedValue> setter(factory->NewJSFunction(thread_->GetEcmaVM()->GetGlobalEnv()));

    // First - setter should not be defined.
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::__lookupSetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    // Second - set a setter function.
    ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, setter.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    result = object::proto::__defineSetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    // Third - check the setter function if it really exists.
    ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    result = object::proto::__lookupSetter__(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result, setter.GetTaggedValue());
}

// 19.1.2.5Object.freeze ( O )
TEST_F(BuiltinsObjectTest, Freeze)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    // An object is extensible by default, so it is also non-frozen.
    JSHandle<JSObject> empty_obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    empty_obj->GetJSHClass()->SetExtensible(true);
    auto nofreeze_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    nofreeze_obj_call_info->SetFunction(JSTaggedValue::Undefined());
    nofreeze_obj_call_info->SetThis(JSTaggedValue::Undefined());
    nofreeze_obj_call_info->SetCallArg(0, empty_obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, nofreeze_obj_call_info.get());
    JSTaggedValue result = object::IsFrozen(nofreeze_obj_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());

    object::Freeze(nofreeze_obj_call_info.get());
    JSTaggedValue result_is = object::IsFrozen(nofreeze_obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result_is.GetRawData(), JSTaggedValue::True().GetRawData());
}

// ES2021 20.1.2.7 Object.fromEntries ( iterable )
TEST_F(BuiltinsObjectTest, FromEntries)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSArray *arr1 = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetHeapObject());
    EXPECT_TRUE(arr1 != nullptr);

    JSArray *arr2 = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetHeapObject());
    EXPECT_TRUE(arr2 != nullptr);

    JSHandle<JSObject> obj(thread_, arr1);

    JSHandle<JSObject> prop(thread_, arr2);
    JSHandle<JSTaggedValue> foo_str = JSHandle<JSTaggedValue>(factory->NewFromString("foo"));
    JSHandle<JSTaggedValue> bar_str = JSHandle<JSTaggedValue>(factory->NewFromString("bar"));

    PropertyDescriptor desc0(thread_, foo_str, true, true, true);
    PropertyDescriptor desc1(thread_, bar_str, true, true, true);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));

    JSArray::DefineOwnProperty(thread_, prop, key0, desc0);
    JSArray::DefineOwnProperty(thread_, prop, key1, desc1);

    PropertyDescriptor desc_prop(thread_, JSHandle<JSTaggedValue>(prop), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc_prop);

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::FromEntries(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());

    PropertyDescriptor desc(thread_);
    JSObject::GetOwnProperty(thread_, JSHandle<JSObject>(thread_, result), foo_str, desc);

    ASSERT_TRUE(JSTaggedValue::SameValue(desc.GetValue().GetTaggedValue(), bar_str.GetTaggedValue()));
}

// 19.1.2.6 Object.getOwnPropertyDescriptor ( O, P )
TEST_F(BuiltinsObjectTest, GetOwnPropertyDescriptor)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSObject> obj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);

    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("x"));

    PropertyDescriptor desc_enum(thread_);
    desc_enum.SetWritable(true);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj_handle), key, desc_enum);

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, obj_handle.GetTaggedValue());
    obj_call_info->SetCallArg(1, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::GetOwnPropertyDescriptor(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> writable_str = thread_->GlobalConstants()->GetHandledWritableString();
    JSTaggedValue jt(reinterpret_cast<TaggedObject *>(result.GetRawData()));
    PropertyDescriptor desc(thread_);
    JSObject::GetOwnProperty(thread_, JSHandle<JSObject>(thread_, jt), writable_str, desc);
    ASSERT_TRUE(JSTaggedValue::SameValue(desc.GetValue().GetTaggedValue(), JSTaggedValue(true)));
}

// ES2021 20.1.2.9 Object.getOwnPropertyDescriptors ( O )
TEST_F(BuiltinsObjectTest, GetOwnPropertyDescriptors)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSObject> obj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);

    JSHandle<JSTaggedValue> key_x(thread_->GetEcmaVM()->GetFactory()->NewFromString("x"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(22));

    JSObject::SetProperty(thread_, obj_handle, key_x, value);

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, obj_handle.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::GetOwnPropertyDescriptors(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> handle(thread_, result);

    JSHandle<JSTaggedValue> prop = JSObject::GetProperty(thread_, handle, key_x).GetValue();

    JSHandle<JSTaggedValue> value_str = thread_->GlobalConstants()->GetHandledValueString();
    ASSERT_EQ(JSObject::GetProperty(thread_, prop, value_str).GetValue()->GetInt(), 22);

    JSHandle<JSTaggedValue> enumerable_str = thread_->GlobalConstants()->GetHandledEnumerableString();
    ASSERT_EQ(JSObject::GetProperty(thread_, prop, enumerable_str).GetValue().GetTaggedValue(), JSTaggedValue(true));

    JSHandle<JSTaggedValue> configurable_str = thread_->GlobalConstants()->GetHandledConfigurableString();
    ASSERT_EQ(JSObject::GetProperty(thread_, prop, configurable_str).GetValue().GetTaggedValue(), JSTaggedValue(true));

    JSHandle<JSTaggedValue> writable_str = thread_->GlobalConstants()->GetHandledWritableString();
    ASSERT_EQ(JSObject::GetProperty(thread_, prop, writable_str).GetValue().GetTaggedValue(), JSTaggedValue(true));
}

// 19.1.2.7 Object.getOwnPropertyNames ( O )
TEST_F(BuiltinsObjectTest, GetOwnPropertyNames)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSObject> obj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);

    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("x"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj_handle), key, value);

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, obj_handle.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::GetOwnPropertyNames(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
}

// 19.1.2.8 Object.getOwnPropertySymbols ( O )
TEST_F(BuiltinsObjectTest, GetOwnPropertySymbols)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSObject> obj_handle =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    JSHandle<JSSymbol> symbol_key = thread_->GetEcmaVM()->GetFactory()->NewJSSymbol();
    JSHandle<JSTaggedValue> key(symbol_key);
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(obj_handle), key, value);
    thread_->ClearException();

    auto obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    obj_call_info->SetFunction(JSTaggedValue::Undefined());
    obj_call_info->SetThis(JSTaggedValue::Undefined());
    obj_call_info->SetCallArg(0, obj_handle.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info.get());
    JSTaggedValue result = object::GetOwnPropertySymbols(obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
}

// 19.1.2.10 Object.is ( value1, value2 )
TEST_F(BuiltinsObjectTest, Is)
{
    // js object compare
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));

    JSHandle<JSObject> obj1 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    JSHandle<JSObject> obj2 =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);

    auto obj_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    obj_call_info1->SetFunction(JSTaggedValue::Undefined());
    obj_call_info1->SetThis(JSTaggedValue::Undefined());
    obj_call_info1->SetCallArg(0, obj1.GetTaggedValue());
    obj_call_info1->SetCallArg(1, obj2.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, obj_call_info1.get());
    JSTaggedValue obj_result1 = object::Is(obj_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(obj_result1.GetRawData(), JSTaggedValue::False().GetRawData());

    obj_call_info1->SetCallArg(1, obj1.GetTaggedValue());
    JSTaggedValue obj_result2 = object::Is(obj_call_info1.get());
    ASSERT_EQ(obj_result2.GetRawData(), JSTaggedValue::True().GetRawData());

    // string compare
    JSHandle<EcmaString> test_str_value1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("helloworld");
    JSHandle<EcmaString> test_str_value2 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("helloworld");

    auto str_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    str_call_info->SetFunction(JSTaggedValue::Undefined());
    str_call_info->SetThis(JSTaggedValue::Undefined());
    str_call_info->SetCallArg(0, test_str_value1.GetTaggedValue());
    str_call_info->SetCallArg(1, test_str_value2.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, str_call_info.get());
    JSTaggedValue str_result = object::Is(str_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(str_result.GetRawData(), JSTaggedValue::True().GetRawData());

    // bool compare
    auto bool_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    bool_call_info->SetFunction(JSTaggedValue::Undefined());
    bool_call_info->SetThis(JSTaggedValue::Undefined());
    bool_call_info->SetCallArg(0, JSTaggedValue::True());
    bool_call_info->SetCallArg(1, JSTaggedValue::False());

    prev = TestHelper::SetupFrame(thread_, bool_call_info.get());
    JSTaggedValue bool_result = object::Is(bool_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(bool_result.GetRawData(), JSTaggedValue::False().GetRawData());

    // number compare
    auto num_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    num_call_info->SetFunction(JSTaggedValue::Undefined());
    num_call_info->SetThis(JSTaggedValue::Undefined());
    num_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(0)));
    num_call_info->SetCallArg(1, JSTaggedValue(-0.0));

    prev = TestHelper::SetupFrame(thread_, num_call_info.get());
    JSTaggedValue num_result = object::Is(num_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(num_result.GetRawData(), JSTaggedValue::False().GetRawData());

    // undefined or null compare
    auto null_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    null_call_info->SetFunction(JSTaggedValue::Undefined());
    null_call_info->SetThis(JSTaggedValue::Undefined());
    null_call_info->SetCallArg(0, JSTaggedValue::Null());
    null_call_info->SetCallArg(1, JSTaggedValue::Null());

    prev = TestHelper::SetupFrame(thread_, null_call_info.get());
    JSTaggedValue null_result = object::Is(null_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(null_result.GetRawData(), JSTaggedValue::True().GetRawData());

    auto undefine_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    undefine_call_info->SetFunction(JSTaggedValue::Undefined());
    undefine_call_info->SetThis(JSTaggedValue::Undefined());
    undefine_call_info->SetCallArg(0, JSTaggedValue::Undefined());
    undefine_call_info->SetCallArg(1, JSTaggedValue::Undefined());

    prev = TestHelper::SetupFrame(thread_, undefine_call_info.get());
    JSTaggedValue undefine_result = object::Is(undefine_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(undefine_result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// 19.1.2.11 Object.isExtensible ( O )
TEST_F(BuiltinsObjectTest, IsExtensible)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));

    // New objects can be extended by default.
    JSHandle<JSObject> empty_obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    empty_obj->GetJSHClass()->SetExtensible(true);
    auto empty_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    empty_obj_call_info->SetFunction(JSTaggedValue::Undefined());
    empty_obj_call_info->SetThis(JSTaggedValue::Undefined());
    empty_obj_call_info->SetCallArg(0, empty_obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, empty_obj_call_info.get());
    JSTaggedValue result = object::IsExtensible(empty_obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());

    empty_obj->GetJSHClass()->SetExtensible(false);
    JSTaggedValue result2 = object::IsExtensible(empty_obj_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue::False().GetRawData());
}

// 19.1.2.12 Object.isFrozen ( O )
TEST_F(BuiltinsObjectTest, IsFrozen)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString("x"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(1));

    // An object is extensible by default, so it is also non-frozen.
    JSHandle<JSObject> obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>::Cast(function), function);
    obj->GetJSHClass()->SetExtensible(true);
    auto empty_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    empty_obj_call_info->SetFunction(JSTaggedValue::Undefined());
    empty_obj_call_info->SetThis(JSTaggedValue::Undefined());
    empty_obj_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, empty_obj_call_info.get());

    JSTaggedValue result = object::IsFrozen(empty_obj_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
    obj->GetJSHClass()->SetExtensible(false);
    JSTaggedValue result_nex = object::IsFrozen(empty_obj_call_info.get());
    ASSERT_EQ(result_nex.GetRawData(), JSTaggedValue::True().GetRawData());

    TestHelper::TearDownFrame(thread_, prev);

    PropertyDescriptor desc_enum(thread_);
    desc_enum.SetConfigurable(true);
    desc_enum.SetWritable(false);
    obj->GetJSHClass()->SetExtensible(true);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc_enum);
    obj->GetJSHClass()->SetExtensible(false);
    auto empty_obj_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    empty_obj_call_info2->SetFunction(JSTaggedValue::Undefined());
    empty_obj_call_info2->SetThis(JSTaggedValue::Undefined());
    empty_obj_call_info2->SetCallArg(0, obj.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, empty_obj_call_info2.get());
    JSTaggedValue result_nw = object::IsFrozen(empty_obj_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result_nw.GetRawData(), JSTaggedValue::False().GetRawData());

    desc_enum.SetConfigurable(false);
    obj->GetJSHClass()->SetExtensible(true);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), key, desc_enum);
    obj->GetJSHClass()->SetExtensible(false);
    auto empty_obj_call_info3 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    empty_obj_call_info3->SetFunction(JSTaggedValue::Undefined());
    empty_obj_call_info3->SetThis(JSTaggedValue::Undefined());
    empty_obj_call_info3->SetCallArg(0, obj.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, empty_obj_call_info3.get());
    JSTaggedValue result_nc = object::IsFrozen(empty_obj_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result_nc.GetRawData(), JSTaggedValue::True().GetRawData());
}

// 19.1.2.13 Object.isSealed ( O )
TEST_F(BuiltinsObjectTest, IsSealed)
{
    JSHandle<JSTaggedValue> function(thread_, BuiltinsObjectTestCreate(thread_));

    // An object is extensible by default, so it is also non-frozen.
    JSHandle<JSObject> empty_obj =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(function), function);
    empty_obj->GetJSHClass()->SetExtensible(true);
    auto empty_obj_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    empty_obj_call_info->SetFunction(JSTaggedValue::Undefined());
    empty_obj_call_info->SetThis(JSTaggedValue::Undefined());
    empty_obj_call_info->SetCallArg(0, empty_obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, empty_obj_call_info.get());
    JSTaggedValue result = object::IsSealed(empty_obj_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Object.keys(obj)
TEST_F(BuiltinsObjectTest, Keys)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::Keys(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
}

// Object.preventExtensions(obj)
TEST_F(BuiltinsObjectTest, PreventExtensions)
{
    JSHandle<JSObject> obj = JSHandle<JSObject>(thread_, CreateBuiltinJSObject(thread_, "x"));
    obj->GetJSHClass()->SetExtensible(true);
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::PreventExtensions(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
    JSTaggedValue jt(reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSObject> jt_handle(thread_, jt);
    ASSERT_TRUE(!jt_handle->IsExtensible());
}

// Object.seal(obj)
TEST_F(BuiltinsObjectTest, Seal)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::Seal(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());
    ASSERT_EQ(result.GetRawData(), obj->GetRawData());

    // test isSealed().
    JSTaggedValue res = object::IsSealed(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(res.GetRawData(), JSTaggedValue::True().GetRawData());
}

// Object.setPrototypeOf(obj, prototype)
TEST_F(BuiltinsObjectTest, SetPrototypeOf)
{
    JSHandle<JSObject> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    JSHandle<JSObject> obj_father(thread_, CreateBuiltinJSObject(thread_, "y"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, obj_father.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::SetPrototypeOf(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
    ASSERT_EQ(result.GetRawData(), obj.GetTaggedValue().GetRawData());

    // test obj has property "y".
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("y"));
    EXPECT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key).GetValue()->GetInt(), 1);
}

// Object.values(obj)
TEST_F(BuiltinsObjectTest, Values)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromString("y"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(22));

    JSObject::SetProperty(thread_, obj, key, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::Values(ecma_runtime_call_info.get());

    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> handle(thread_, result);
    JSHandle<JSTaggedValue> index0(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> index1(thread_, JSTaggedValue(1));

    ASSERT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(handle), index0).GetValue()->GetInt(), 1);
    ASSERT_EQ(JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(handle), index1).GetValue()->GetInt(), 22);
}

// obj.hasOwnProperty(prop)
TEST_F(BuiltinsObjectTest, HasOwnProperty)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    PandaString key_c_str = "x";
    JSHandle<EcmaString> key_string = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(&key_c_str[0]);
    JSHandle<JSTaggedValue> key(key_string);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::HasOwnProperty(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// prototypeObj.isPrototypeOf(object)
TEST_F(BuiltinsObjectTest, IsPrototypeOfFalse)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    JSHandle<JSTaggedValue> obj_father(thread_, CreateBuiltinJSObject(thread_, "y"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj_father.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = object::proto::IsPrototypeOf(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result1.GetRawData(), JSTaggedValue::False().GetRawData());
}

TEST_F(BuiltinsObjectTest, IsPrototypeOfTrue)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    JSHandle<JSTaggedValue> obj_father(thread_, CreateBuiltinJSObject(thread_, "y"));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, obj_father.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = object::SetPrototypeOf(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result1.IsObject());
    ASSERT_EQ(result1.GetRawData(), obj->GetRawData());

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(obj_father.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, obj.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = object::proto::IsPrototypeOf(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result2.GetRawData(), JSTaggedValue::True().GetRawData());
}

// obj.propertyIsEnumerable(prop)
TEST_F(BuiltinsObjectTest, PropertyIsEnumerable)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("x"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::PropertyIsEnumerable(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// obj.toLocaleString()
TEST_F(BuiltinsObjectTest, ToLocaleString)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    JSHandle<JSFunction> callee_func = thread_->GetEcmaVM()->GetFactory()->NewJSFunction(
        thread_->GetEcmaVM()->GetGlobalEnv(), reinterpret_cast<void *>(object::proto::ToString));
    JSHandle<JSTaggedValue> callee_value(callee_func);
    JSHandle<JSTaggedValue> callee_key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("toString"));
    JSObject::SetProperty(thread_, obj, callee_key, callee_value);

    JSHandle<EcmaString> result_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object Object]");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::ToLocaleString(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsString());
    ASSERT_EQ(result_value->Compare(reinterpret_cast<EcmaString *>(result.GetRawData())), 0);
}

// obj.toString()
TEST_F(BuiltinsObjectTest, ToString)
{
    JSHandle<JSTaggedValue> obj = JSHandle<JSTaggedValue>(thread_, CreateBuiltinJSObject(thread_, "x"));

    // object
    JSHandle<EcmaString> result_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object Object]");
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::ToString(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsString());
    ASSERT_EQ(result_value->Compare(reinterpret_cast<EcmaString *>(result.GetRawData())), 0);

    // array
    JSHandle<JSArray> arr = thread_->GetEcmaVM()->GetFactory()->NewJSArray();
    JSHandle<EcmaString> result_arr_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object Array]");
    auto arr_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    arr_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    arr_ecma_runtime_call_info->SetThis(arr.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, arr_ecma_runtime_call_info.get());
    JSTaggedValue result_arr = object::proto::ToString(arr_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_arr.IsString());
    ASSERT_EQ(result_arr_value->Compare(reinterpret_cast<EcmaString *>(result_arr.GetRawData())), 0);

    // string
    JSHandle<EcmaString> str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("hello");
    JSHandle<EcmaString> result_str_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object String]");
    auto str_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    str_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    str_ecma_runtime_call_info->SetThis(str.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, str_ecma_runtime_call_info.get());
    JSTaggedValue result_str = object::proto::ToString(str_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_str.IsString());
    ASSERT_EQ(result_str_value->Compare(reinterpret_cast<EcmaString *>(result_str.GetRawData())), 0);

    // function
    JSHandle<JSFunction> func = thread_->GetEcmaVM()->GetFactory()->NewJSFunction(thread_->GetEcmaVM()->GetGlobalEnv());
    JSHandle<EcmaString> result_func_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object Function]");
    auto func_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    func_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    func_ecma_runtime_call_info->SetThis(func.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, func_ecma_runtime_call_info.get());
    JSTaggedValue result_func = object::proto::ToString(func_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_func.IsString());
    ASSERT_EQ(result_func_value->Compare(reinterpret_cast<EcmaString *>(result_func.GetRawData())), 0);

    // error
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> error_object = env->GetErrorFunction();
    JSHandle<JSObject> error =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);
    JSHandle<EcmaString> error_value = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object Error]");
    auto error_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    error_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    error_ecma_runtime_call_info->SetThis(error.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, error_ecma_runtime_call_info.get());
    JSTaggedValue result_error = object::proto::ToString(error_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_error.IsString());
    ASSERT_EQ(error_value->Compare(reinterpret_cast<EcmaString *>(result_error.GetRawData())), 0);

    // boolean
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue::False());
    JSHandle<JSFunction> boolean_object(env->GetBooleanFunction());
    JSHandle<JSPrimitiveRef> boolean = thread_->GetEcmaVM()->GetFactory()->NewJSPrimitiveRef(boolean_object, value);
    JSHandle<EcmaString> result_bool_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object Boolean]");
    auto bool_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    bool_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    bool_ecma_runtime_call_info->SetThis(boolean.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, bool_ecma_runtime_call_info.get());
    JSTaggedValue result_bool = object::proto::ToString(bool_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_bool.IsString());
    ASSERT_EQ(result_bool_value->Compare(reinterpret_cast<EcmaString *>(result_bool.GetRawData())), 0);

    // number
    JSHandle<EcmaString> result_num_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("[object Number]");
    auto num_ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    num_ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    num_ecma_runtime_call_info->SetThis(JSTaggedValue(static_cast<double>(0)));

    prev = TestHelper::SetupFrame(thread_, num_ecma_runtime_call_info.get());
    JSTaggedValue result_num = object::proto::ToString(num_ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result_num.IsString());
    ASSERT_EQ(result_num_value->Compare(reinterpret_cast<EcmaString *>(result_num.GetRawData())), 0);
}

// object.valueOf()
TEST_F(BuiltinsObjectTest, ValueOf)
{
    JSHandle<JSTaggedValue> obj(thread_, CreateBuiltinJSObject(thread_, "x"));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = object::proto::ValueOf(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsECMAObject());
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
