/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"
#include "plugins/ecmascript/runtime/base/typed_array_helper.h"

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/object_operator.h"

#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {

class BuiltinsTypedArrayTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

    class TestClass {
    public:
        static JSTaggedValue TestForEachFunc(EcmaRuntimeCallInfo *argv)
        {
            JSHandle<JSTaggedValue> key = builtins_common::GetCallArg(argv, 0);
            if (key->IsUndefined()) {
                return JSTaggedValue::Undefined();
            }
            JSArray *js_array = JSArray::Cast(builtins_common::GetThis(argv)->GetTaggedObject());
            int length = js_array->GetArrayLength() + 1;
            js_array->SetArrayLength(argv->GetThread(), length);
            return JSTaggedValue::Undefined();
        }

        static JSTaggedValue TestEveryFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                [[maybe_unused]] int aaa = builtins_common::GetCallArg(argv, 0)->GetInt();
                //  10 : test case
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        static JSTaggedValue TestFilterFunc(EcmaRuntimeCallInfo *argv)
        {
            ASSERT(argv);
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                // 10 : test case
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        static JSTaggedValue TestMapFunc(EcmaRuntimeCallInfo *argv)
        {
            int accumulator = builtins_common::GetCallArg(argv, 0)->GetInt();
            accumulator = accumulator * 2;  // 2 : mapped to 2 times the original value
            return builtins_common::GetTaggedInt(accumulator);
        }

        static JSTaggedValue TestFindFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                // 10 : test case
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        static JSTaggedValue TestFindIndexFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                //  10 : test case
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        static JSTaggedValue TestReduceFunc(EcmaRuntimeCallInfo *argv)
        {
            int accumulator = builtins_common::GetCallArg(argv, 0)->GetInt();
            accumulator = accumulator + builtins_common::GetCallArg(argv, 1)->GetInt();
            return builtins_common::GetTaggedInt(accumulator);
        }

        static JSTaggedValue TestReduceRightFunc(EcmaRuntimeCallInfo *argv)
        {
            int accumulator = builtins_common::GetCallArg(argv, 0)->GetInt();
            accumulator = accumulator + builtins_common::GetCallArg(argv, 1)->GetInt();
            return builtins_common::GetTaggedInt(accumulator);
        }

        static JSTaggedValue TestSomeFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                //  10 : test case
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }
    };

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSTaggedValue CreateBuiltinsJSObject(JSThread *thread, const PandaString &key_c_str)
{
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> dynclass = env->GetObjectFunction();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> obj(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass), dynclass));
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(&key_c_str[0]));
    JSHandle<JSTaggedValue> value(thread, JSTaggedValue(1));
    JSObject::SetProperty(thread, obj, key, value);
    return obj.GetTaggedValue();
}

JSTypedArray *CreateTypedArrayFromList(JSThread *thread, const JSHandle<TaggedArray> &array)
{
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSTaggedValue> jsarray(JSArray::CreateArrayFromList(thread, array));
    JSHandle<JSFunction> int8_array(env->GetInt8ArrayFunction());
    JSHandle<JSObject> global_object(thread, env->GetGlobalObject());
    //  6 : test case
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetNewTarget(JSTaggedValue(*int8_array));
    ecma_runtime_call_info1->SetThis(JSTaggedValue(*global_object));
    ecma_runtime_call_info1->SetCallArg(0, jsarray.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info1.get());
    JSTaggedValue result = int8_array::Int8ArrayConstructor(ecma_runtime_call_info1.get());

    EXPECT_TRUE(result.IsECMAObject());
    JSTypedArray *int8arr = JSTypedArray::Cast(reinterpret_cast<TaggedObject *>(result.GetRawData()));
    return int8arr;
}

TEST_F(BuiltinsTypedArrayTest, Species)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSFunction> array(env->GetArrayFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(array.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(global_object.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = typed_array::GetSpecies(ecma_runtime_call_info1.get());
    ASSERT_TRUE(result.IsECMAObject());
}

// ES2021 23.2.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsTypedArrayTest, Includes1)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<TaggedArray> array(factory->NewTaggedArray(5));
    array->Set(thread_, 0, JSTaggedValue(1));
    array->Set(thread_, 1, JSTaggedValue(2));
    array->Set(thread_, 2, JSTaggedValue(3));
    array->Set(thread_, 3, JSTaggedValue(4));
    array->Set(thread_, 4, JSTaggedValue(3));
    JSHandle<JSTypedArray> obj(thread_, CreateTypedArrayFromList(thread_, array));

    // new Array(1,2,3,4,3).includes(1,0)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = typed_array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(true).GetRawData());
}

// ES2021 23.2.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsTypedArrayTest, Includes2)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<TaggedArray> array(factory->NewTaggedArray(5));
    array->Set(thread_, 0, JSTaggedValue(1));
    array->Set(thread_, 1, JSTaggedValue(2));
    array->Set(thread_, 2, JSTaggedValue(3));
    array->Set(thread_, 3, JSTaggedValue(4));
    array->Set(thread_, 4, JSTaggedValue(3));
    JSHandle<JSTypedArray> obj(thread_, CreateTypedArrayFromList(thread_, array));

    // new Array(1,2,3,4,3).includes(1,3)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = typed_array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(false).GetRawData());
}

// ES2021 23.2.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsTypedArrayTest, Includes3)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<TaggedArray> array(factory->NewTaggedArray(5));
    array->Set(thread_, 0, JSTaggedValue(1));
    array->Set(thread_, 1, JSTaggedValue(2));
    array->Set(thread_, 2, JSTaggedValue(3));
    array->Set(thread_, 3, JSTaggedValue(4));
    array->Set(thread_, 4, JSTaggedValue(3));
    JSHandle<JSTypedArray> obj(thread_, CreateTypedArrayFromList(thread_, array));

    // new Array(1,2,3,4,3).includes(5,0)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = typed_array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(false).GetRawData());
}

// ES2021 23.2.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsTypedArrayTest, Includes4)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<TaggedArray> array(factory->NewTaggedArray(5));
    array->Set(thread_, 0, JSTaggedValue(1));
    array->Set(thread_, 1, JSTaggedValue(2));
    array->Set(thread_, 2, JSTaggedValue(3));
    array->Set(thread_, 3, JSTaggedValue(4));
    array->Set(thread_, 4, JSTaggedValue(3));
    JSHandle<JSTypedArray> obj(thread_, CreateTypedArrayFromList(thread_, array));

    // new Array(1,2,3,4,3).includes(1)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = typed_array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(true).GetRawData());
}

// ES2021 23.2.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsTypedArrayTest, Includes5)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<TaggedArray> array(factory->NewTaggedArray(5));
    array->Set(thread_, 0, JSTaggedValue(1));
    array->Set(thread_, 1, JSTaggedValue(2));
    array->Set(thread_, 2, JSTaggedValue(3));
    array->Set(thread_, 3, JSTaggedValue(4));
    array->Set(thread_, 4, JSTaggedValue(3));
    JSHandle<JSTypedArray> obj(thread_, CreateTypedArrayFromList(thread_, array));

    // new Array(1,2,3,4,3).includes(5)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = typed_array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(false).GetRawData());
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
