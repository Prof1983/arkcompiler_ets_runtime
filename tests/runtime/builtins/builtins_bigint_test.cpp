/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_bigint.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTBEGIN(readability-magic-numbers)

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {
using BigInt = ecmascript::BigInt;
class BuiltinsBigIntTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    ecmascript::JSHandle<ecmascript::JSFunction> method_function_;
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

// new BigInt(123)
TEST_F(BuiltinsBigIntTest, BigIntConstructor1)
{
    JSHandle<JSTaggedValue> numeric_value(thread_, JSTaggedValue(123));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = big_int::BigIntConstructor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result.IsBigInt());
}

// new BigInt("456")
TEST_F(BuiltinsBigIntTest, BigIntConstructor2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("456");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = big_int::BigIntConstructor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result.IsBigInt());
}

// AsIntN(64, (2 ^ 63 - 1))
TEST_F(BuiltinsBigIntTest, AsIntN1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("9223372036854775807");
    int bit = 64;  // 64-bit

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int>(bit)));
    ecma_runtime_call_info->SetCallArg(1, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = big_int::AsIntN(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result.IsBigInt());
    JSHandle<BigInt> big_int_handle(thread_, result);
    JSHandle<EcmaString> result_str = BigInt::ToString(thread_, big_int_handle);
    JSHandle<EcmaString> str = factory->NewFromCanBeCompressString("9223372036854775807");
    EXPECT_EQ(result_str->Compare(*str), 0);
}

// AsIntN(64, (2 ^ 63))
TEST_F(BuiltinsBigIntTest, AsIntN2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("9223372036854775808");
    int bit = 64;  // 64-bit

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int>(bit)));
    ecma_runtime_call_info->SetCallArg(1, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = big_int::AsIntN(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result.IsBigInt());
    JSHandle<BigInt> big_int_handle(thread_, result);
    JSHandle<EcmaString> result_str = BigInt::ToString(thread_, big_int_handle);
    JSHandle<EcmaString> str = factory->NewFromCanBeCompressString("-9223372036854775808");
    EXPECT_EQ(result_str->Compare(*str), 0);
}

// AsUintN(64, (2 ^ 64 - 1))
TEST_F(BuiltinsBigIntTest, AsUintN1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("18446744073709551615");
    int bit = 64;  // 64-bit

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int>(bit)));
    ecma_runtime_call_info->SetCallArg(1, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = big_int::AsUintN(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result.IsBigInt());
    JSHandle<BigInt> big_int_handle(thread_, result);
    JSHandle<EcmaString> result_str = BigInt::ToString(thread_, big_int_handle);
    JSHandle<EcmaString> str = factory->NewFromCanBeCompressString("18446744073709551615");
    EXPECT_EQ(result_str->Compare(*str), 0);
}

// AsUintN(64, (2 ^ 64))
TEST_F(BuiltinsBigIntTest, AsUintN2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("18446744073709551616");
    int bit = 64;  // 64-bit

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int>(bit)));
    ecma_runtime_call_info->SetCallArg(1, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = big_int::AsUintN(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result.IsBigInt());
    JSHandle<BigInt> big_int_handle(thread_, result);
    JSHandle<EcmaString> result_str = BigInt::ToString(thread_, big_int_handle);
    JSHandle<EcmaString> str = factory->NewFromCanBeCompressString("0");
    EXPECT_EQ(result_str->Compare(*str), 0);
}

// using locale
TEST_F(BuiltinsBigIntTest, ToLocaleString1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("123456789123456789");

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    JSHandle<EcmaString> locale = factory->NewFromCanBeCompressString("de-DE");

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(big_int_handle.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, locale.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(1, JSTaggedValue::Undefined());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ToLocaleString(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result2.IsString());
    JSHandle<EcmaString> ecma_str_handle(thread_, result2);
    JSHandle<EcmaString> result_str = factory->NewFromCanBeCompressString("123.456.789.123.456.789");
    EXPECT_EQ(ecma_str_handle->Compare(*result_str), 0);
}

// using locale and options
TEST_F(BuiltinsBigIntTest, ToLocaleString2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> options_obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("123456789123456789");
    JSHandle<JSTaggedValue> format_style = thread_->GlobalConstants()->GetHandledStyleString();
    JSHandle<JSTaggedValue> style_key(factory->NewFromCanBeCompressString("currency"));
    JSHandle<JSTaggedValue> style_value(factory->NewFromCanBeCompressString("EUR"));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    JSHandle<EcmaString> locale = factory->NewFromCanBeCompressString("de-DE");
    JSObject::SetProperty(thread_, options_obj, format_style, style_key);
    JSObject::SetProperty(thread_, options_obj, style_key, style_value);

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(big_int_handle.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, locale.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(1, options_obj.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ToLocaleString(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result2.IsString());
    JSHandle<EcmaString> ecma_str_handle(thread_, result2);
    EXPECT_STREQ("123.456.789.123.456.789,00 €", PandaString(ecma_str_handle->GetCString().get()).c_str());
}

// 17.ToStirng()
TEST_F(BuiltinsBigIntTest, ToString1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("17");

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(big_int_handle.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, JSTaggedValue::Undefined());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ToString(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result2.IsString());
    JSHandle<EcmaString> ecma_str_handle(thread_, result2);
    EXPECT_STREQ("17", PandaString(ecma_str_handle->GetCString().get()).c_str());
}

// -0.ToStirng()
TEST_F(BuiltinsBigIntTest, ToString2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("-0");

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(big_int_handle.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, JSTaggedValue::Undefined());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ToString(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result2.IsString());
    JSHandle<EcmaString> ecma_str_handle(thread_, result2);
    EXPECT_STREQ("0", PandaString(ecma_str_handle->GetCString().get()).c_str());
}

// -10.ToStirng(2)
TEST_F(BuiltinsBigIntTest, ToString3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("-10");

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    JSHandle<JSTaggedValue> radix(thread_, JSTaggedValue(2));
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(big_int_handle.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, radix.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ToString(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result2.IsString());
    JSHandle<EcmaString> ecma_str_handle(thread_, result2);
    EXPECT_STREQ("-1010", PandaString(ecma_str_handle->GetCString().get()).c_str());
}

// 254.ToStirng(16)
TEST_F(BuiltinsBigIntTest, ToString4)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("254");

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    JSHandle<JSTaggedValue> radix(thread_, JSTaggedValue(16));
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(big_int_handle.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, radix.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ToString(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result2.IsString());
    JSHandle<EcmaString> ecma_str_handle(thread_, result2);
    EXPECT_STREQ("fe", PandaString(ecma_str_handle->GetCString().get()).c_str());
}

// BigInt.ValueOf
TEST_F(BuiltinsBigIntTest, ValueOf1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("-65536");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(big_int_handle.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ValueOf(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_EQ(BigInt::SameValue(thread_, result1, result2), true);
}

// Object.ValueOf
TEST_F(BuiltinsBigIntTest, ValueOf2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> numeric_value = factory->NewFromCanBeCompressString("65535");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, numeric_value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = big_int::BigIntConstructor(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<BigInt> big_int_handle(thread_, result1);
    JSHandle<JSTaggedValue> big_int_obj(big_int_handle);

    JSHandle<JSPrimitiveRef> js_primitive_ref =
        factory->NewJSPrimitiveRef(PrimitiveType::PRIMITIVE_BIGINT, big_int_obj);
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(js_primitive_ref.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = big_int::proto::ValueOf(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_EQ(BigInt::SameValue(thread_, big_int_handle.GetTaggedValue(), result2), true);
}

// testcases of NumberToBigint()
TEST_F(BuiltinsBigIntTest, NumberToBigint)
{
    JSHandle<JSTaggedValue> number(thread_, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> bigint(thread_, JSTaggedValue::Undefined());

    number = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(base::MAX_VALUE));
    bigint = JSHandle<JSTaggedValue>(thread_, BigInt::NumberToBigInt(thread_, number));
    ASSERT_TRUE(bigint->IsBigInt());
    bool compare_res = JSTaggedValue::Equal(thread_, number, bigint);
    ASSERT_TRUE(compare_res);

    number = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(-base::MAX_VALUE));
    bigint = JSHandle<JSTaggedValue>(thread_, BigInt::NumberToBigInt(thread_, number));
    ASSERT_TRUE(bigint->IsBigInt());
    compare_res = JSTaggedValue::Equal(thread_, number, bigint);
    ASSERT_TRUE(JSHandle<BigInt>::Cast(bigint)->GetSign());
    ASSERT_TRUE(compare_res);

    number = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(-0xffffffff));
    bigint = JSHandle<JSTaggedValue>(thread_, BigInt::NumberToBigInt(thread_, number));
    ASSERT_TRUE(bigint->IsBigInt());
    compare_res = JSTaggedValue::Equal(thread_, number, bigint);
    ASSERT_TRUE(compare_res);

    number = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0));
    bigint = JSHandle<JSTaggedValue>(thread_, BigInt::NumberToBigInt(thread_, number));
    ASSERT_TRUE(bigint->IsBigInt());
    compare_res = JSTaggedValue::Equal(thread_, number, bigint);
    ASSERT_TRUE(compare_res);
}

// testcases of BigintToNumber()
TEST_F(BuiltinsBigIntTest, BigintToNumber)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> bigint(thread_, JSTaggedValue::Undefined());
    JSTaggedNumber number(0);

    JSHandle<JSTaggedValue> parma(factory->NewFromCanBeCompressString("0xffff"));
    bigint = JSHandle<JSTaggedValue>(thread_, JSTaggedValue::ToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    number = BigInt::BigIntToNumber(JSHandle<BigInt>::Cast(bigint));
    ASSERT_EQ(number.GetNumber(), static_cast<double>(0xffff));

    parma = JSHandle<JSTaggedValue>(
        factory->NewFromCanBeCompressString("0xfffffffffffff8000000000000000000000000000000000000000000000000000"
                                            "0000000000000000000000000000000000000000000000000000000000000000000"
                                            "0000000000000000000000000000000000000000000000000000000000000000000"
                                            "000000000000000000000000000000000000000000000000000000000"));
    bigint = JSHandle<JSTaggedValue>(thread_, JSTaggedValue::ToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    number = BigInt::BigIntToNumber(JSHandle<BigInt>::Cast(bigint));
    ASSERT_EQ(number.GetNumber(), base::MAX_VALUE);

    parma = JSHandle<JSTaggedValue>(thread_, JSTaggedValue::False());
    bigint = JSHandle<JSTaggedValue>(thread_, JSTaggedValue::ToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    ASSERT_TRUE(JSHandle<BigInt>::Cast(bigint)->IsZero());
    number = BigInt::BigIntToNumber(JSHandle<BigInt>::Cast(bigint));
    ASSERT_EQ(number.GetNumber(), 0.0);

    parma = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(base::MAX_VALUE));
    bigint = JSHandle<JSTaggedValue>(thread_, BigInt::NumberToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    number = BigInt::BigIntToNumber(JSHandle<BigInt>::Cast(bigint));
    ASSERT_EQ(number.GetNumber(), base::MAX_VALUE);

    parma = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(-base::MAX_VALUE));
    bigint = JSHandle<JSTaggedValue>(thread_, BigInt::NumberToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    number = BigInt::BigIntToNumber(JSHandle<BigInt>::Cast(bigint));
    ASSERT_EQ(number.GetNumber(), -base::MAX_VALUE);

    /*
        // clang-14 with ASAN release - Illegal instruction (core dumped)
        parma = JSHandle<JSTaggedValue>(thread, JSTaggedValue(-0xffffffff));
        bigint = JSHandle<JSTaggedValue>(thread, BigInt::NumberToBigInt(thread, parma));
        ASSERT_TRUE(bigint->IsBigInt());
        number = BigInt::BigIntToNumber(JSHandle<BigInt>::Cast(bigint));
        ASSERT_EQ(number.GetNumber(), -0xffffffff);
    */
}

// testcases of StringToBigInt(EcmaString)
TEST_F(BuiltinsBigIntTest, StringToBigInt)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> bigint;
    JSHandle<EcmaString> str;
    JSHandle<JSTaggedValue> parma;

    // hex string
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("0xffff"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    str = BigInt::ToString(thread_, JSHandle<BigInt>::Cast(bigint), BigInt::HEXADECIMAL);
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("ffff"));
    ASSERT_EQ(str->Compare(reinterpret_cast<EcmaString *>(parma->GetRawData())), 0);

    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("0XFFFF"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    str = BigInt::ToString(thread_, JSHandle<BigInt>::Cast(bigint), BigInt::HEXADECIMAL);
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("ffff"));
    ASSERT_EQ(str->Compare(reinterpret_cast<EcmaString *>(parma->GetRawData())), 0);

    // binary string
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("0b11111111"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    str = BigInt::ToString(thread_, JSHandle<BigInt>::Cast(bigint), BigInt::BINARY);
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("11111111"));
    ASSERT_EQ(str->Compare(reinterpret_cast<EcmaString *>(parma->GetRawData())), 0);

    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("0B11111111"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    str = BigInt::ToString(thread_, JSHandle<BigInt>::Cast(bigint), BigInt::BINARY);
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("11111111"));
    ASSERT_EQ(str->Compare(reinterpret_cast<EcmaString *>(parma->GetRawData())), 0);

    // octal string
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("0o123456"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    str = BigInt::ToString(thread_, JSHandle<BigInt>::Cast(bigint), BigInt::OCTAL);
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("123456"));
    ASSERT_EQ(str->Compare(reinterpret_cast<EcmaString *>(parma->GetRawData())), 0);

    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("0O123456"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    str = BigInt::ToString(thread_, JSHandle<BigInt>::Cast(bigint), BigInt::OCTAL);
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("123456"));
    ASSERT_EQ(str->Compare(reinterpret_cast<EcmaString *>(parma->GetRawData())), 0);

    // decimal string
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("999999999"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    str = BigInt::ToString(thread_, JSHandle<BigInt>::Cast(bigint));
    ASSERT_EQ(str->Compare(reinterpret_cast<EcmaString *>(parma->GetRawData())), 0);

    // string has space
    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("  123  "));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    JSHandle<JSTaggedValue> number(thread_, JSTaggedValue(static_cast<double>(123)));
    bool compare_res = JSTaggedValue::Equal(thread_, bigint, number);
    ASSERT_TRUE(compare_res);

    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("123   "));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    number = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(static_cast<double>(123)));
    compare_res = JSTaggedValue::Equal(thread_, bigint, number);
    ASSERT_TRUE(compare_res);

    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("   123"));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    number = JSHandle<JSTaggedValue>(thread_, JSTaggedValue(static_cast<double>(123)));
    compare_res = JSTaggedValue::Equal(thread_, bigint, number);
    ASSERT_TRUE(compare_res);

    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString(""));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    ASSERT_TRUE(JSHandle<BigInt>::Cast(bigint)->IsZero());

    parma = JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("    "));
    bigint = JSHandle<JSTaggedValue>(thread_, base::NumberHelper::StringToBigInt(thread_, parma));
    ASSERT_TRUE(bigint->IsBigInt());
    ASSERT_TRUE(JSHandle<BigInt>::Cast(bigint)->IsZero());
}

// NOLINTEND(readability-magic-numbers)

}  // namespace panda::test
