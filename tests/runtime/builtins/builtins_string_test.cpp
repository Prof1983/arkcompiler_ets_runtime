/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/builtins/builtins_regexp.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"

#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_regexp.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
class BuiltinsStringTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSTaggedValue CreateRegExpObjByPatternAndFlags(JSThread *thread, const JSHandle<EcmaString> &pattern,
                                               const JSHandle<EcmaString> &flags)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> regexp(env->GetRegExpFunction());
    JSHandle<JSObject> global_object(thread, env->GetGlobalObject());

    // 8 : test case
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, regexp.GetTaggedValue(), 8);
    ecma_runtime_call_info->SetFunction(regexp.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, pattern.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, flags.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    JSTaggedValue result = reg_exp::RegExpConstructor(ecma_runtime_call_info.get());
    return result;
}

TEST_F(BuiltinsStringTest, StringConstructor1)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSFunction> string(env->GetStringFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    JSHandle<EcmaString> string2 = factory->NewFromCanBeCompressString("ABC");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, string.GetTaggedValue(), 6);
    ecma_runtime_call_info->SetFunction(string.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, string2.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::StringConstructor(ecma_runtime_call_info.get());
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    JSHandle<JSPrimitiveRef> ref(thread_, JSPrimitiveRef::Cast(value.GetTaggedObject()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("ABC").GetTaggedValue();
    ASSERT_EQ(
        EcmaString::Cast(ref->GetValue().GetTaggedObject())->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())),
        0);
}

// String.fromCharCode(65, 66, 67)
TEST_F(BuiltinsStringTest, fromCharCode1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    const double arg1 = 65;
    const double arg2 = 66;
    const double arg3 = 67;

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(arg1));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(arg2));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(arg3));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::FromCharCode(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    JSHandle<JSTaggedValue> value_handle(thread_, JSTaggedValue(value.GetTaggedObject()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("ABC").GetTaggedValue();
    ASSERT_EQ(
        EcmaString::Cast(value_handle->GetTaggedObject())->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())),
        0);
}

// String.fromCodePoint(65, 66, 67)
TEST_F(BuiltinsStringTest, fromCodePoint1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    const double arg1 = 65;
    const double arg2 = 66;
    const double arg3 = 67;

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(arg1));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(arg2));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(arg3));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::FromCodePoint(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("ABC").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "abcabcabc".charAt(5)
TEST_F(BuiltinsStringTest, charAt1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_val = factory->NewFromCanBeCompressString("abcabcabc");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::CharAt(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("c").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "一二三四".charAt(2)
TEST_F(BuiltinsStringTest, charAt2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_val = factory->NewFromString("一二三四");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::CharAt(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("三").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "abcabcabc".charAt(-1)
TEST_F(BuiltinsStringTest, charAt3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_val = factory->NewFromCanBeCompressString("abcabcabc");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::CharAt(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->GetEmptyString().GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "ABC".charCodeAt(0)
TEST_F(BuiltinsStringTest, charCodeAt1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_val = factory->NewFromCanBeCompressString("ABC");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::CharCodeAt(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(65).GetRawData());
}

// "ABC".charCodeAt(-1)
TEST_F(BuiltinsStringTest, charCodeAt2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_val = factory->NewFromCanBeCompressString("ABC");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(-1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::CharCodeAt(ecma_runtime_call_info.get());

    JSTaggedValue test = builtins_common::GetTaggedDouble(base::NAN_VALUE);
    ASSERT_EQ(result.GetRawData(), test.GetRawData());
}

// "ABC".codePointAt(1)
TEST_F(BuiltinsStringTest, codePointAt1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_val = factory->NewFromCanBeCompressString("ABC");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::CodePointAt(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(66).GetRawData());
}

// 'a'.concat('b', 'c', 'd')
TEST_F(BuiltinsStringTest, concat1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("a");
    JSHandle<EcmaString> val1 = factory->NewFromCanBeCompressString("b");
    JSHandle<EcmaString> val2 = factory->NewFromCanBeCompressString("c");
    JSHandle<EcmaString> val3 = factory->NewFromCanBeCompressString("d");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val1.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, val2.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(2, val3.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Concat(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("abcd").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "abcabcabc".indexof('b')
TEST_F(BuiltinsStringTest, indexof1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("b");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::IndexOf(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(1).GetRawData());
}

// "abcabcabc".indexof('b', 2)
TEST_F(BuiltinsStringTest, indexof2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("b");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::IndexOf(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(4).GetRawData());
}

// "abcabcabc".indexof('d')
TEST_F(BuiltinsStringTest, indexof3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("d");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::IndexOf(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(-1).GetRawData());
}

// "abcabcabc".lastIndexOf('b')
TEST_F(BuiltinsStringTest, lastIndexOf1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("b");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::LastIndexOf(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(7).GetRawData());
}
// "abcabcabc".lastIndexOf('b', 2)
TEST_F(BuiltinsStringTest, lastIndexOf2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("b");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::LastIndexOf(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(1).GetRawData());
}

// "abcabcabc".lastIndexOf('d')
TEST_F(BuiltinsStringTest, lastIndexOf3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("d");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::LastIndexOf(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(-1).GetRawData());
}

// "abcabcabc".includes('b')
TEST_F(BuiltinsStringTest, Includes2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("b");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Includes(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// "abccccccc".includes('b'，2)
TEST_F(BuiltinsStringTest, Includes3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abccccccc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("b");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Includes(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// "一二三四".includes('二')
TEST_F(BuiltinsStringTest, Includes4)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("一二三四");
    JSHandle<EcmaString> val = factory->NewFromString("二");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Includes(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// "To be, or not to be, that is the question.".startsWith('To be')
TEST_F(BuiltinsStringTest, startsWith1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("To be, or not to be, that is the question.");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("To be");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::StartsWith(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// "To be, or not to be, that is the question.".startsWith('not to be')
TEST_F(BuiltinsStringTest, startsWith2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("To be, or not to be, that is the question.");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("not to be");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::StartsWith(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// "To be, or not to be, that is the question.".startsWith('not to be', 10)
TEST_F(BuiltinsStringTest, startsWith3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("To be, or not to be, that is the question.");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("not to be");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::StartsWith(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// "To be, or not to be, that is the question.".endsWith('question.')
TEST_F(BuiltinsStringTest, endsWith1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("To be, or not to be, that is the question.");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("question.");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::EndsWith(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// "To be, or not to be, that is the question.".endsWith('to be')
TEST_F(BuiltinsStringTest, endsWith2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("To be, or not to be, that is the question.");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("to be");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::EndsWith(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// "To be, or not to be, that is the question.".endsWith('to be', 19)
TEST_F(BuiltinsStringTest, endsWith3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("To be, or not to be, that is the question.");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("to be");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(19)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::EndsWith(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// "有ABC".toLocaleLowerCase()
TEST_F(BuiltinsStringTest, toLocaleLowerCase2)
{
    ASSERT_NE(thread_, nullptr);
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("有ABC");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ToLocaleLowerCase(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("有abc").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "ABC".toLowerCase()
TEST_F(BuiltinsStringTest, toLowerCase1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("ABC");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ToLowerCase(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSHandle<EcmaString> test = factory->NewFromCanBeCompressString("abc");
    ASSERT_TRUE(JSTaggedValue::SameValue(result_handle.GetTaggedValue(), test.GetTaggedValue()));
}

// "abc".toUpperCase()
TEST_F(BuiltinsStringTest, toUpperCase1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abc");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ToUpperCase(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("ABC").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "abc".localecompare('b')
TEST_F(BuiltinsStringTest, localecompare1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("b");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::LocaleCompare(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(-1).GetRawData());
}

// "abc".localecompare('abc')
TEST_F(BuiltinsStringTest, localecompare2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("abc");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::LocaleCompare(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(0).GetRawData());
}

// "abc".localecompare('aa')
TEST_F(BuiltinsStringTest, localecompare3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abc");
    JSHandle<EcmaString> val = factory->NewFromCanBeCompressString("aa");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::LocaleCompare(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(1).GetRawData());
}

// "abc".normalize('NFC')
TEST_F(BuiltinsStringTest, normalize1)
{
    ASSERT_NE(thread_, nullptr);
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("abc");
    JSHandle<EcmaString> val = factory->NewFromString("NFC");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, val.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Normalize(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("abc").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "hello".padEnd(10)
TEST_F(BuiltinsStringTest, padEnd1)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("hello");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(10)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::PadEnd(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("hello     ").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "hello".padEnd(11, world)
TEST_F(BuiltinsStringTest, padEnd2)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("hello");
    JSHandle<EcmaString> fill_string = factory->NewFromString("world");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(11)));
    ecma_runtime_call_info->SetCallArg(1, fill_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::PadEnd(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("helloworldw").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "world".padStart(10)
TEST_F(BuiltinsStringTest, padStart1)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("world");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(10)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::PadStart(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("     world").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "world".padStart(11, "hello")
TEST_F(BuiltinsStringTest, padStart2)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("world");
    JSHandle<EcmaString> fill_string = factory->NewFromString("hello");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(11)));
    ecma_runtime_call_info->SetCallArg(1, fill_string.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::PadStart(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("hellohworld").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "abc".repeat(5)
TEST_F(BuiltinsStringTest, repeat1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abc");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Repeat(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("abcabcabcabcabc").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// 'The morning is upon us.'.slice(4, -2)
TEST_F(BuiltinsStringTest, slice1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("The morning is upon us.");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(4)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(-2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Slice(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("morning is upon u").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// 'The morning is upon us.'.slice(12)
TEST_F(BuiltinsStringTest, slice2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("The morning is upon us.");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(12)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Slice(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("is upon us.").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// 'Mozilla'.substring(3, -3)
TEST_F(BuiltinsStringTest, substring1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("Mozilla");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(3)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(-3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Substring(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("Moz").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// 'Mozilla'.substring(7, 4)
TEST_F(BuiltinsStringTest, substring2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("Mozilla");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(7)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(4)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Substring(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("lla").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "   Hello world!   ".trim()
TEST_F(BuiltinsStringTest, trim1)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("   Hello world!   ");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Trim(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("Hello world!").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

TEST_F(BuiltinsStringTest, trim2)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("   Hello world!   ");
    JSHandle<JSFunction> string_object(env->GetStringFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(this_str.GetTaggedValue().GetTaggedObject()));
    JSHandle<JSPrimitiveRef> str = factory->NewJSPrimitiveRef(string_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Trim(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromCanBeCompressString("Hello world!").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "   Hello world!   ".trimEnd()
TEST_F(BuiltinsStringTest, trimEnd1)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("   Hello world!   ");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::TrimEnd(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("   Hello world!").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// new String("   Hello world!   ").trimEnd()
TEST_F(BuiltinsStringTest, trimEnd2)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("   Hello world!   ");
    JSHandle<JSFunction> string_object(env->GetStringFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(this_str.GetTaggedValue().GetHeapObject()));
    JSHandle<JSPrimitiveRef> str = factory->NewJSPrimitiveRef(string_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::TrimEnd(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("   Hello world!").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// "   Hello world!   ".trimStart()
TEST_F(BuiltinsStringTest, trimStart1)
{
    ASSERT_NE(thread_, nullptr);

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("   Hello world!   ");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::TrimStart(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("Hello world!   ").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// new String("   Hello world!   ").trimStart()
TEST_F(BuiltinsStringTest, trimStart2)
{
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("   Hello world!   ");
    JSHandle<JSFunction> string_object(env->GetStringFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(this_str.GetTaggedValue().GetHeapObject()));
    JSHandle<JSPrimitiveRef> str = factory->NewJSPrimitiveRef(string_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::TrimStart(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    JSTaggedValue test = factory->NewFromString("Hello world!   ").GetTaggedValue();
    ASSERT_EQ(result_handle->Compare(reinterpret_cast<EcmaString *>(test.GetRawData())), 0);
}

// new String("abcabcabc").toString();
TEST_F(BuiltinsStringTest, ToString)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<JSFunction> string_object(env->GetStringFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(this_str.GetTaggedValue().GetTaggedObject()));
    JSHandle<JSPrimitiveRef> str = factory->NewJSPrimitiveRef(string_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ToString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    auto test = JSTaggedValue(*this_str);
    ASSERT_EQ(result.GetRawData(), test.GetRawData());
}

TEST_F(BuiltinsStringTest, ValueOf)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("abcabcabc");
    JSHandle<JSFunction> string_object(env->GetStringFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(this_str.GetTaggedValue().GetTaggedObject()));
    JSHandle<JSPrimitiveRef> str = factory->NewJSPrimitiveRef(string_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ValueOf(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    auto test = JSTaggedValue(*this_str);
    ASSERT_EQ(result.GetRawData(), test.GetRawData());
}

static inline JSFunction *BuiltinsStringTestCreate(JSThread *thread)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    return global_env->GetObjectFunction().GetObject<JSFunction>();
}

TEST_F(BuiltinsStringTest, Raw)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> foo(factory->NewFromCanBeCompressString("foo"));
    JSHandle<JSTaggedValue> bar(factory->NewFromCanBeCompressString("bar"));
    JSHandle<JSTaggedValue> baz(factory->NewFromCanBeCompressString("baz"));
    JSHandle<JSTaggedValue> raw_array = JSHandle<JSTaggedValue>::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)));
    JSHandle<JSObject> obj(raw_array);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, foo);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, bar);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, baz);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    JSHandle<JSTaggedValue> constructor(thread_, BuiltinsStringTestCreate(thread_));
    JSHandle<JSTaggedValue> template_string(
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor));
    JSHandle<JSTaggedValue> raw_key(factory->NewFromCanBeCompressString("raw"));
    JSObject::SetProperty(thread_, template_string, raw_key, raw_array);
    JSHandle<EcmaString> test = factory->NewFromCanBeCompressString("foo5barJavaScriptbaz");

    JSHandle<EcmaString> javascript = factory->NewFromCanBeCompressString("JavaScript");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(template_string.GetObject<EcmaString>()));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info->SetCallArg(2, javascript.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::Raw(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *test));
}

TEST_F(BuiltinsStringTest, Replace)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("Twas the night before Xmas...");
    JSHandle<EcmaString> search_str = factory->NewFromCanBeCompressString("Xmas");
    JSHandle<EcmaString> replace_str = factory->NewFromCanBeCompressString("Christmas");
    JSHandle<EcmaString> expected = factory->NewFromCanBeCompressString("Twas the night before Christmas...");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Replace(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expected));

    JSHandle<EcmaString> replace_str1 = factory->NewFromCanBeCompressString("abc$$");
    JSHandle<EcmaString> expected1 = factory->NewFromCanBeCompressString("Twas the night before abc$...");

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, replace_str1.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = string::proto::Replace(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<EcmaString> result_string1(thread_, result1);
    ASSERT_TRUE(result1.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result_string1, *expected1));

    JSHandle<EcmaString> replace_str2 = factory->NewFromCanBeCompressString("abc$$dd");
    JSHandle<EcmaString> expected2 = factory->NewFromCanBeCompressString("Twas the night before abc$dd...");

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(1, replace_str2.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = string::proto::Replace(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<EcmaString> result_string2(thread_, result2);
    ASSERT_TRUE(result2.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result_string2, *expected2));

    JSHandle<EcmaString> replace_str3 = factory->NewFromCanBeCompressString("abc$&dd");
    JSHandle<EcmaString> expected3 = factory->NewFromCanBeCompressString("Twas the night before abcXmasdd...");

    auto ecma_runtime_call_info3 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info3->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info3->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info3->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info3->SetCallArg(1, replace_str3.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
    JSTaggedValue result3 = string::proto::Replace(ecma_runtime_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<EcmaString> result_string3(thread_, result3);
    ASSERT_TRUE(result3.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result_string3, *expected3));

    JSHandle<EcmaString> replace_str4 = factory->NewFromCanBeCompressString("abc$`dd");
    JSHandle<EcmaString> expected4 =
        factory->NewFromCanBeCompressString("Twas the night before abcTwas the night before dd...");

    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info4->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info4->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(1, replace_str4.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    JSTaggedValue result4 = string::proto::Replace(ecma_runtime_call_info4.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<EcmaString> result_string4(thread_, result4);
    ASSERT_TRUE(result4.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result_string4, *expected4));
}

TEST_F(BuiltinsStringTest, Replace2)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("Twas the night before Xmas...");
    JSHandle<EcmaString> search_str = factory->NewFromCanBeCompressString("Xmas");
    JSHandle<EcmaString> replace_str = factory->NewFromCanBeCompressString("abc$\'dd");
    JSHandle<EcmaString> expected = factory->NewFromCanBeCompressString("Twas the night before abc...dd...");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Replace(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expected));

    JSHandle<EcmaString> replace_str2 = factory->NewFromCanBeCompressString("abc$`dd$\'$ff");
    JSHandle<EcmaString> expected2 =
        factory->NewFromCanBeCompressString("Twas the night before abcTwas the night before dd...$ff...");

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(1, replace_str2.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = string::proto::Replace(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<EcmaString> result_string2(thread_, result2);
    ASSERT_TRUE(result2.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result_string2, *expected2));

    JSHandle<EcmaString> replace_str3 = factory->NewFromCanBeCompressString("abc$`dd$\'$");
    JSHandle<EcmaString> expected3 =
        factory->NewFromCanBeCompressString("Twas the night before abcTwas the night before dd...$...");

    auto ecma_runtime_call_info3 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info3->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info3->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info3->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info3->SetCallArg(1, replace_str3.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
    JSTaggedValue result3 = string::proto::Replace(ecma_runtime_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSHandle<EcmaString> result_string3(thread_, result3);
    ASSERT_TRUE(result3.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result_string3, *expected3));

    JSHandle<EcmaString> replace_str4 = factory->NewFromCanBeCompressString("abc$`dd$$");
    JSHandle<EcmaString> expected4 =
        factory->NewFromCanBeCompressString("Twas the night before abcTwas the night before dd$...");

    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info4->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info4->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(1, replace_str4.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    JSTaggedValue result4 = string::proto::Replace(ecma_runtime_call_info4.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result4.IsString());
    JSHandle<EcmaString> result_string4(thread_, result4);
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result_string4, *expected4));
}

TEST_F(BuiltinsStringTest, Replace3)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("Twas the night before Xmas...");
    JSHandle<EcmaString> search_str = factory->NewFromCanBeCompressString("Xmas");
    JSHandle<EcmaString> replace_str = factory->NewFromCanBeCompressString("$&a $` $\' $2 $01 $$1 $21 $32 a");
    JSHandle<EcmaString> expected = factory->NewFromCanBeCompressString(
        "Twas the night before Xmasa Twas the night before  ... $2 $01 $1 $21 $32 a...");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Replace(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expected));
}

TEST_F(BuiltinsStringTest, Replace4)
{
    // invoke RegExpConstructor method
    JSHandle<EcmaString> pattern1 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("quick\\s(brown).+?(jumps)");
    JSHandle<EcmaString> flags1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> search_str(thread_, reinterpret_cast<JSRegExp *>(result1.GetRawData()));
    JSHandle<EcmaString> expected = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(
        "The Quick Brown Fox Jumpsa The   Over The Lazy Dog Jumps Brown $1 Jumps1 $32 a Over The Lazy Dog");

    // make dyn_runtime_call_info2
    JSHandle<EcmaString> this_str =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("The Quick Brown Fox Jumps Over The Lazy Dog");
    JSHandle<EcmaString> replace_str =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("$&a $` $\' $2 $01 $$1 $21 $32 a");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Replace(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expected));
}

TEST_F(BuiltinsStringTest, ReplaceAll1)
{
    ASSERT_NE(thread_, nullptr);
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("The Quick Brown Fox Jumps Over The Lazy Dog");
    JSHandle<EcmaString> search_str = factory->NewFromString("o");
    JSHandle<EcmaString> replace_str = factory->NewFromString("a");
    JSHandle<EcmaString> expected = factory->NewFromString("The Quick Brawn Fax Jumps Over The Lazy Dag");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ReplaceAll(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expected));
}

TEST_F(BuiltinsStringTest, ReplaceAll2)
{
    ASSERT_NE(thread_, nullptr);
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("xxx");
    JSHandle<EcmaString> search_str = factory->NewFromString("");
    JSHandle<EcmaString> replace_str = factory->NewFromString("_");
    JSHandle<EcmaString> expected = factory->NewFromString("_x_x_x_");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ReplaceAll(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expected));
}

TEST_F(BuiltinsStringTest, ReplaceAll3)
{
    ASSERT_NE(thread_, nullptr);
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromString("xxx");
    JSHandle<EcmaString> search_str = factory->NewFromString("x");
    JSHandle<EcmaString> replace_str = factory->NewFromString("");
    JSHandle<EcmaString> expected = factory->NewFromString("");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, search_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, replace_str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::ReplaceAll(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expected));
}

TEST_F(BuiltinsStringTest, Split)
{
    // invoke RegExpConstructor method
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("Hello World. How are you doing?");
    JSHandle<EcmaString> separator_str = factory->NewFromCanBeCompressString(" ");
    JSHandle<JSTaggedValue> limit(thread_, JSTaggedValue(3));
    JSHandle<EcmaString> expected1 = factory->NewFromCanBeCompressString("Hello");
    JSHandle<EcmaString> expected2 = factory->NewFromCanBeCompressString("World.");
    JSHandle<EcmaString> expected3 = factory->NewFromCanBeCompressString("How");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, separator_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Split(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSArray> result_array(thread_, reinterpret_cast<JSArray *>(result.GetRawData()));
    ASSERT_TRUE(result_array->IsJSArray());
    JSHandle<JSTaggedValue> result_obj(result_array);
    JSHandle<EcmaString> string1(
        JSObject::GetProperty(thread_, result_obj, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))).GetValue());
    JSHandle<EcmaString> string2(
        JSObject::GetProperty(thread_, result_obj, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1))).GetValue());
    JSHandle<EcmaString> string3(
        JSObject::GetProperty(thread_, result_obj, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2))).GetValue());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*string1, *expected1));
    ASSERT_TRUE(EcmaString::StringsAreEqual(*string2, *expected2));
    ASSERT_TRUE(EcmaString::StringsAreEqual(*string3, *expected3));
}

TEST_F(BuiltinsStringTest, Split2)
{
    // invoke RegExpConstructor method
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> this_str = factory->NewFromCanBeCompressString("a-b-c");
    JSHandle<EcmaString> pattern1 = factory->NewFromCanBeCompressString("-");
    JSHandle<EcmaString> flags1 = factory->NewFromCanBeCompressString("iug");
    JSTaggedValue result1 = CreateRegExpObjByPatternAndFlags(thread_, pattern1, flags1);
    JSHandle<JSRegExp> separator_obj(thread_, result1);

    JSHandle<JSTaggedValue> limit(thread_, JSTaggedValue(3));
    JSHandle<EcmaString> expected1 = factory->NewFromCanBeCompressString("a");
    JSHandle<EcmaString> expected2 = factory->NewFromCanBeCompressString("b");
    JSHandle<EcmaString> expected3 = factory->NewFromCanBeCompressString("c");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_str.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, separator_obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = string::proto::Split(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSArray> result_array(thread_, result);
    ASSERT_TRUE(result_array->IsJSArray());
    JSHandle<JSTaggedValue> result_obj(result_array);
    JSHandle<EcmaString> string1(
        JSObject::GetProperty(thread_, result_obj, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0))).GetValue());
    JSHandle<EcmaString> string2(
        JSObject::GetProperty(thread_, result_obj, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1))).GetValue());
    JSHandle<EcmaString> string3(
        JSObject::GetProperty(thread_, result_obj, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2))).GetValue());
    ASSERT_TRUE(EcmaString::StringsAreEqual(*string1, *expected1));
    ASSERT_TRUE(EcmaString::StringsAreEqual(*string2, *expected2));
    ASSERT_TRUE(EcmaString::StringsAreEqual(*string3, *expected3));
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
