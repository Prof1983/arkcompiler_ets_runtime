/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_date.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
const char NEG = '-';
const char PLUS = '+';
const int STR_LENGTH_OTHERS = 2;
const int MINUTE_PER_HOUR = 60;
const int CHINA_BEFORE_1901_MIN = 485;
const int CHINA_AFTER_1901_MIN = 480;
const int64_t CHINA_BEFORE_1900_MS = -2177481943000;
class BuiltinsDateTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSHandle<JSDate> JSDateCreateTest(JSThread *thread)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> date_function = global_env->GetDateFunction();
    JSHandle<JSDate> date_object =
        JSHandle<JSDate>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(date_function), date_function));
    return date_object;
}

static std::unique_ptr<EcmaRuntimeCallInfo> CreateAndSetRuntimeCallInfo(JSThread *thread, ArraySizeT argv_length,
                                                                        JSTaggedValue this_value)
{
    auto ecma_runtime_call_info =
        TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue::Undefined(), argv_length);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(this_value);
    return ecma_runtime_call_info;
}

TEST_F(BuiltinsDateTest, SetGetDate)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 6, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    [[maybe_unused]] JSTaggedValue result1 = date::proto::SetDate(ecma_runtime_call_info.get());
    JSTaggedValue result2 = date::proto::GetDate(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetUTCDate)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 6, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    [[maybe_unused]] JSTaggedValue result3 = date::proto::SetUTCDate(ecma_runtime_call_info.get());
    JSTaggedValue result4 = date::proto::GetUTCDate(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMinusUTCDate)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 6, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(-2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    [[maybe_unused]] JSTaggedValue result3 = date::proto::SetUTCDate(ecma_runtime_call_info.get());
    JSTaggedValue result4 = date::proto::GetUTCDate(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(29)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetFullYear)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 10, js_date.GetTaggedValue());
    // 2018 : test case
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(2018)));
    // 10 : test case
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));
    // 2, 6 : test case
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(6)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetFullYear(ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::GetFullYear(ecma_runtime_call_info.get());
    // 2018 : test case
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(2018)).GetRawData());

    JSTaggedValue result2 = date::proto::GetMonth(ecma_runtime_call_info.get());
    // 10 : test case
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(10)).GetRawData());

    JSTaggedValue result3 = date::proto::GetDate(ecma_runtime_call_info.get());
    // 6 : test case
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(6)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetUTCFullYear)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 10, js_date.GetTaggedValue());
    // 2018 : test case
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(2018)));
    // 10 : test case
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));
    // 2, 6 : test case
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(6)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetUTCFullYear(ecma_runtime_call_info.get());
    JSTaggedValue result4 = date::proto::GetUTCFullYear(ecma_runtime_call_info.get());
    // 2018 : test case
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(2018)).GetRawData());

    JSTaggedValue result5 = date::proto::GetUTCMonth(ecma_runtime_call_info.get());
    // 10 : test case
    ASSERT_EQ(result5.GetRawData(), JSTaggedValue(static_cast<double>(10)).GetRawData());

    JSTaggedValue result6 = date::proto::GetUTCDate(ecma_runtime_call_info.get());
    // 6 : test case
    ASSERT_EQ(result6.GetRawData(), JSTaggedValue(static_cast<double>(6)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMinusFullYear)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 10, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(-2018)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(-10)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(-6)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetFullYear(ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::GetFullYear(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(-2019)).GetRawData());

    JSTaggedValue result2 = date::proto::GetMonth(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(1)).GetRawData());

    JSTaggedValue result3 = date::proto::GetDate(ecma_runtime_call_info.get());
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(22)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMinusUTCFullYear)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 10, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(-2018)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(-10)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(-6)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetUTCFullYear(ecma_runtime_call_info.get());
    JSTaggedValue result4 = date::proto::GetUTCFullYear(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(-2019)).GetRawData());

    JSTaggedValue result5 = date::proto::GetUTCMonth(ecma_runtime_call_info.get());
    ASSERT_EQ(result5.GetRawData(), JSTaggedValue(static_cast<double>(1)).GetRawData());

    JSTaggedValue result6 = date::proto::GetUTCDate(ecma_runtime_call_info.get());
    ASSERT_EQ(result6.GetRawData(), JSTaggedValue(static_cast<double>(22)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetHours)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 12, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(18)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(6)));
    ecma_runtime_call_info->SetCallArg(3, JSTaggedValue(static_cast<double>(111)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetHours(ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::GetHours(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(18)).GetRawData());

    JSTaggedValue result2 = date::proto::GetMinutes(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(10)).GetRawData());

    JSTaggedValue result3 = date::proto::GetSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(6)).GetRawData());

    JSTaggedValue result4 = date::proto::GetMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(111)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetUTCHours)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 12, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(18)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(6)));
    ecma_runtime_call_info->SetCallArg(3, JSTaggedValue(static_cast<double>(111)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetUTCHours(ecma_runtime_call_info.get());
    JSTaggedValue result5 = date::proto::GetUTCHours(ecma_runtime_call_info.get());
    ASSERT_EQ(result5.GetRawData(), JSTaggedValue(static_cast<double>(18)).GetRawData());

    JSTaggedValue result6 = date::proto::GetUTCMinutes(ecma_runtime_call_info.get());
    ASSERT_EQ(result6.GetRawData(), JSTaggedValue(static_cast<double>(10)).GetRawData());

    JSTaggedValue result7 = date::proto::GetUTCSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result7.GetRawData(), JSTaggedValue(static_cast<double>(6)).GetRawData());

    JSTaggedValue result8 = date::proto::GetUTCMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result8.GetRawData(), JSTaggedValue(static_cast<double>(111)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMinusHours)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 12, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(-18)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(-10)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(-6)));
    ecma_runtime_call_info->SetCallArg(3, JSTaggedValue(static_cast<double>(-111)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetHours(ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::GetHours(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(5)).GetRawData());

    JSTaggedValue result2 = date::proto::GetMinutes(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(49)).GetRawData());

    JSTaggedValue result3 = date::proto::GetSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(53)).GetRawData());

    JSTaggedValue result4 = date::proto::GetMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(889)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMinusUTCHours)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 12, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(-18)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(-10)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(-6)));
    ecma_runtime_call_info->SetCallArg(3, JSTaggedValue(static_cast<double>(-111)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetUTCHours(ecma_runtime_call_info.get());
    JSTaggedValue result5 = date::proto::GetUTCHours(ecma_runtime_call_info.get());
    ASSERT_EQ(result5.GetRawData(), JSTaggedValue(static_cast<double>(5)).GetRawData());

    JSTaggedValue result6 = date::proto::GetUTCMinutes(ecma_runtime_call_info.get());
    ASSERT_EQ(result6.GetRawData(), JSTaggedValue(static_cast<double>(49)).GetRawData());

    JSTaggedValue result7 = date::proto::GetUTCSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result7.GetRawData(), JSTaggedValue(static_cast<double>(53)).GetRawData());

    JSTaggedValue result8 = date::proto::GetUTCMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result8.GetRawData(), JSTaggedValue(static_cast<double>(889)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMilliseconds)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 6, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(100)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::SetMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(100)).GetRawData());

    JSTaggedValue result2 = date::proto::GetMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(100)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetUTCMilliseconds)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 6, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(100)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result3 = date::proto::SetUTCMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(100)).GetRawData());

    JSTaggedValue result4 = date::proto::GetUTCMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(100)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMinutes)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 10, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(10)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(6)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(111)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetMinutes(ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::GetMinutes(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(10)).GetRawData());

    JSTaggedValue result2 = date::proto::GetSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(6)).GetRawData());

    JSTaggedValue result3 = date::proto::GetMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(111)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetUTCMinutes)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 10, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(10)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(6)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(111)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetUTCMinutes(ecma_runtime_call_info.get());
    JSTaggedValue result4 = date::proto::GetUTCMinutes(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(10)).GetRawData());

    JSTaggedValue result5 = date::proto::GetUTCSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result5.GetRawData(), JSTaggedValue(static_cast<double>(6)).GetRawData());

    JSTaggedValue result6 = date::proto::GetUTCMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result6.GetRawData(), JSTaggedValue(static_cast<double>(111)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetMonth)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 8, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(8)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetMonth(ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::GetMonth(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(8)).GetRawData());

    JSTaggedValue result2 = date::proto::GetDate(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(3)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetUTCMonth)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 8, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(8)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetUTCMonth(ecma_runtime_call_info.get());
    JSTaggedValue result3 = date::proto::GetUTCMonth(ecma_runtime_call_info.get());
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(8)).GetRawData());

    JSTaggedValue result4 = date::proto::GetUTCDate(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(3)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetSeconds)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 8, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(59)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(123)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetSeconds(ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::GetSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(59)).GetRawData());

    JSTaggedValue result2 = date::proto::GetMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(123)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetUTCSeconds)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 8, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(59)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(123)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    date::proto::SetUTCSeconds(ecma_runtime_call_info.get());
    JSTaggedValue result3 = date::proto::GetUTCSeconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result3.GetRawData(), JSTaggedValue(static_cast<double>(59)).GetRawData());

    JSTaggedValue result4 = date::proto::GetUTCMilliseconds(ecma_runtime_call_info.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(123)).GetRawData());
}

TEST_F(BuiltinsDateTest, SetGetTime)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 6, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::SetTime(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());

    JSTaggedValue result2 = date::proto::GetTime(ecma_runtime_call_info.get());
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());
}

TEST_F(BuiltinsDateTest, UTC)
{
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 12, JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(2020.982));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(10.23));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(4.32));
    ecma_runtime_call_info->SetCallArg(3, JSTaggedValue(11.32));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::UTC(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1604487600000)).GetRawData());

    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread_, 18, JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(2020.982));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(10.23));
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(4.32));
    ecma_runtime_call_info1->SetCallArg(3, JSTaggedValue(11.32));
    ecma_runtime_call_info1->SetCallArg(4, JSTaggedValue(45.1));
    ecma_runtime_call_info1->SetCallArg(5, JSTaggedValue(34.321));
    ecma_runtime_call_info1->SetCallArg(6, JSTaggedValue(static_cast<int32_t>(231)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    result1 = date::UTC(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1604490334231)).GetRawData());

    auto ecma_runtime_call_info2 = CreateAndSetRuntimeCallInfo(thread_, 10, JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetCallArg(0, JSTaggedValue(10.23));
    ecma_runtime_call_info2->SetCallArg(1, JSTaggedValue(4.32));
    ecma_runtime_call_info2->SetCallArg(2, JSTaggedValue(11.32));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    result1 = date::UTC(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(-1882224000000)).GetRawData());

    auto ecma_runtime_call_info3 = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info3->SetCallArg(0, JSTaggedValue(1994.982));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
    result1 = date::UTC(ecma_runtime_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(757382400000)).GetRawData());

    auto ecma_runtime_call_info4 = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info4->SetCallArg(0, JSTaggedValue(19999944.982));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    result1 = date::UTC(ecma_runtime_call_info4.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(base::NAN_VALUE)).GetRawData());
}

void SetAllYearAndHours(JSThread *thread, const JSHandle<JSDate> &js_date)
{
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread, 10, js_date.GetTaggedValue());
    // 2018 : test case
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(2018)));
    // 10 : test case
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));
    // 2, 6 : test case
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(6)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    date::proto::SetFullYear(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread, prev);

    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread, 12, js_date.GetTaggedValue());
    // 18 : test case
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<double>(18)));
    // 10 : test case
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));
    // 2, 6 : test case
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<double>(6)));
    // 3, 111 : test case
    ecma_runtime_call_info1->SetCallArg(3, JSTaggedValue(static_cast<double>(111)));

    prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info1.get());
    date::proto::SetHours(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread, prev);
}

void SetAll1(JSThread *thread, const JSHandle<JSDate> &js_date)
{
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread, 10, js_date.GetTaggedValue());
    // 1900 : test case
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(1900)));
    // 11 : test case
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(11)));
    // 2, 31 : test case
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(31)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    date::proto::SetFullYear(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread, prev);

    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread, 12, js_date.GetTaggedValue());
    // 23 : test case
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<double>(23)));
    // 54 : test case
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<double>(54)));
    // 2, 16 : test case
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<double>(16)));
    // 3, 888 : test case
    ecma_runtime_call_info1->SetCallArg(3, JSTaggedValue(static_cast<double>(888)));

    prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info1.get());
    date::proto::SetHours(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread, prev);
}

void SetAll2(JSThread *thread, const JSHandle<JSDate> &js_date)
{
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread, 10, js_date.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<double>(1901)));  // 1901 : test case
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<double>(0)));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(static_cast<double>(1)));  // 2 : test case

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    date::proto::SetFullYear(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread, prev);

    // 12 : test case
    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread, 12, js_date.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<double>(0)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<double>(3)));    // 3 : test case
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<double>(21)));   // 2, 21 : test case
    ecma_runtime_call_info1->SetCallArg(3, JSTaggedValue(static_cast<double>(129)));  // 3, 129 : test case

    prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info1.get());
    date::proto::SetHours(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread, prev);
}

TEST_F(BuiltinsDateTest, parse)
{
    JSHandle<EcmaString> str =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2020-11-19T12:18:18.132Z");
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::Parse(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1605788298132)).GetRawData());

    JSHandle<EcmaString> str1 = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2020-11-19Z");
    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, str1.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    result1 = date::Parse(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1605744000000)).GetRawData());

    JSHandle<EcmaString> str2 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2020-11T12:18:17.231+08:00");
    auto ecma_runtime_call_info2 = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetCallArg(0, str2.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    result1 = date::Parse(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1604204297231)).GetRawData());

    JSHandle<EcmaString> str3 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Thu Nov 19 2020 20:18:18 GMT+0800");
    auto ecma_runtime_call_info3 = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info3->SetCallArg(0, str3.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
    result1 = date::Parse(ecma_runtime_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1605788298000)).GetRawData());

    JSHandle<EcmaString> str4 =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Thu 03 Jun 2093 04:18 GMT");
    auto ecma_runtime_call_info4 = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info4->SetCallArg(0, str4.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    result1 = date::Parse(ecma_runtime_call_info4.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(3894841080000)).GetRawData());

    auto ecma_runtime_call_info5 = CreateAndSetRuntimeCallInfo(thread_, 6, JSTaggedValue::Undefined());
    ecma_runtime_call_info5->SetCallArg(0, JSTaggedValue::Null());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info5.get());
    result1 = date::Parse(ecma_runtime_call_info5.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(base::NAN_VALUE)).GetRawData());
}

TEST_F(BuiltinsDateTest, ToDateString)
{
    JSHandle<EcmaString> expect_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Tue Nov 06 2018");
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    SetAllYearAndHours(thread_, js_date);

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = date::proto::ToDateString(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_TRUE(result.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result.GetRawData()), *expect_value));
}

TEST_F(BuiltinsDateTest, ToISOString)
{
    JSHandle<EcmaString> expect_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2020-11-19T12:18:18.132Z");
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    JSDate::Cast(js_date.GetTaggedValue().GetTaggedObject())->SetTimeValue(thread_, JSTaggedValue(1605788298132.0));
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ToISOString(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result1.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result1.GetRawData()), *expect_value));
}

TEST_F(BuiltinsDateTest, ToISOStringMinus)
{
    JSHandle<EcmaString> expect_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1831-12-02T21:47:18.382Z");
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    JSDate::Cast(js_date.GetTaggedValue().GetTaggedObject())->SetTimeValue(thread_, JSTaggedValue(-4357419161618.0));

    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ToISOString(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result1.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result1.GetRawData()), *expect_value));
}

// test toJSON and toPrimitive
TEST_F(BuiltinsDateTest, ToJSON)
{
    JSHandle<EcmaString> expect_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2020-11-19T12:18:18.132Z");
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    js_date->SetTimeValue(thread_, JSTaggedValue(1605788298132.0));
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ToJSON(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result1.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result1.GetRawData()), *expect_value));
}

TEST_F(BuiltinsDateTest, ToJSONMinus)
{
    JSHandle<EcmaString> expect_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("1831-12-02T21:47:18.382Z");
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    js_date->SetTimeValue(thread_, JSTaggedValue(-4357419161618.0));
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ToJSON(ecma_runtime_call_info.get());
    ASSERT_TRUE(result1.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result1.GetRawData()), *expect_value));
}

static PandaString GetLocalTime(JSHandle<JSDate> js_date, int64_t local_min)
{
    PandaString local_time;
    local_min = JSDate::GetLocalOffsetFromOS(local_min, true);
    if (static_cast<int64_t>(JSDate::Cast(js_date.GetTaggedValue().GetTaggedObject())->GetTimeValue().GetDouble()) <
            CHINA_BEFORE_1900_MS &&
        local_min == CHINA_AFTER_1901_MIN) {
        local_min = CHINA_BEFORE_1901_MIN;
    }
    if (local_min >= 0) {
        local_time += PLUS;
    } else if (local_min < 0) {
        local_time += NEG;
        local_min = -local_min;
    }
    local_time = local_time + JSDate::StrToTargetLength(ToPandaString(local_min / MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
    return local_time + JSDate::StrToTargetLength(ToPandaString(local_min % MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
}

TEST_F(BuiltinsDateTest, ToString)
{
    int local_min = 0;
    PandaString local_time;

    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());

    SetAllYearAndHours(thread_, js_date);
    local_time = GetLocalTime(js_date, local_min);
    JSTaggedValue result1 = date::proto::ToString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result1.IsString());
    TestHelper::TearDownFrame(thread_, prev);
    JSHandle<EcmaString> result1_val(thread_, reinterpret_cast<EcmaString *>(result1.GetRawData()));
    PandaString str = "Tue Nov 06 2018 18:10:06 GMT" + local_time;
    JSHandle<EcmaString> str_handle = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str);
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result1_val, *str_handle));

    JSHandle<JSDate> js_date1 = JSDateCreateTest(thread_);
    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread_, 4, js_date1.GetTaggedValue());
    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());

    SetAll1(thread_, js_date1);
    local_time = GetLocalTime(js_date1, local_min);
    JSTaggedValue result2 = date::proto::ToString(ecma_runtime_call_info1.get());
    ASSERT_TRUE(result2.IsString());
    TestHelper::TearDownFrame(thread_, prev);
    JSHandle<EcmaString> result2_val(thread_, reinterpret_cast<EcmaString *>(result2.GetRawData()));
    str = "Mon Dec 31 1900 23:54:16 GMT" + local_time;
    str_handle = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str);
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result2_val, *str_handle));

    JSHandle<JSDate> js_date2 = JSDateCreateTest(thread_);
    auto ecma_runtime_call_info2 = CreateAndSetRuntimeCallInfo(thread_, 4, js_date2.GetTaggedValue());
    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());

    SetAll2(thread_, js_date2);
    local_time = GetLocalTime(js_date, local_min);
    JSTaggedValue result3 = date::proto::ToString(ecma_runtime_call_info2.get());
    ASSERT_TRUE(result3.IsString());
    TestHelper::TearDownFrame(thread_, prev);
    JSHandle<EcmaString> result3_val(thread_, reinterpret_cast<EcmaString *>(result3.GetRawData()));
    str = "Tue Jan 01 1901 00:03:21 GMT" + local_time;
    str_handle = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str);
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result3_val, *str_handle));
}

TEST_F(BuiltinsDateTest, ToTimeString)
{
    int local_min = 0;
    PandaString local_time;

    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());

    SetAllYearAndHours(thread_, js_date);
    local_time = GetLocalTime(js_date, local_min);
    JSTaggedValue result1 = date::proto::ToTimeString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result1.IsString());
    JSHandle<EcmaString> result1_val(thread_, reinterpret_cast<EcmaString *>(result1.GetRawData()));
    PandaString str = "18:10:06 GMT" + local_time;
    JSHandle<EcmaString> str_handle = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str);
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result1_val, *str_handle));

    JSHandle<JSDate> js_date1 = JSDateCreateTest(thread_);
    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread_, 4, js_date1.GetTaggedValue());
    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    SetAll1(thread_, js_date1);
    local_time = GetLocalTime(js_date1, local_min);
    JSTaggedValue result2 = date::proto::ToTimeString(ecma_runtime_call_info1.get());
    ASSERT_TRUE(result2.IsString());
    JSHandle<EcmaString> result2_val(thread_, reinterpret_cast<EcmaString *>(result2.GetRawData()));
    str = "23:54:16 GMT" + local_time;
    str_handle = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str);
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result2_val, *str_handle));
    JSHandle<JSDate> js_date2 = JSDateCreateTest(thread_);
    auto ecma_runtime_call_info2 = CreateAndSetRuntimeCallInfo(thread_, 4, js_date2.GetTaggedValue());
    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    SetAll2(thread_, js_date2);
    local_time = GetLocalTime(js_date, local_min);
    JSTaggedValue result3 = date::proto::ToTimeString(ecma_runtime_call_info2.get());
    ASSERT_TRUE(result3.IsString());
    JSHandle<EcmaString> result3_val(thread_, reinterpret_cast<EcmaString *>(result3.GetRawData()));
    str = "00:03:21 GMT" + local_time;
    str_handle = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str);
    ASSERT_TRUE(EcmaString::StringsAreEqual(*result3_val, *str_handle));
}

TEST_F(BuiltinsDateTest, ToUTCString)
{
    JSHandle<EcmaString> expect_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Thu, 19 Nov 2020 12:18:18 GMT");
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    JSDate::Cast(js_date.GetTaggedValue().GetTaggedObject())->SetTimeValue(thread_, JSTaggedValue(1605788298132.0));
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ToUTCString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result1.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result1.GetRawData()), *expect_value));
}

TEST_F(BuiltinsDateTest, ToUTCStringMinus)
{
    JSHandle<EcmaString> expect_value =
        thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("Fri, 02 Dec 1831 21:47:18 GMT");
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    JSDate::Cast(js_date.GetTaggedValue().GetTaggedObject())->SetTimeValue(thread_, JSTaggedValue(-4357419161618.0));
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ToUTCString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result1.IsString());
    ASSERT_TRUE(EcmaString::StringsAreEqual(reinterpret_cast<EcmaString *>(result1.GetRawData()), *expect_value));
}

TEST_F(BuiltinsDateTest, ValueOf)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    JSDate::Cast(js_date.GetTaggedValue().GetTaggedObject())->SetTimeValue(thread_, JSTaggedValue(1605788298132.0));
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ValueOf(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1605788298132)).GetRawData());
}

TEST_F(BuiltinsDateTest, ValueOfMinus)
{
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    JSDate::Cast(js_date.GetTaggedValue().GetTaggedObject())->SetTimeValue(thread_, JSTaggedValue(-4357419161618.0));
    auto ecma_runtime_call_info = CreateAndSetRuntimeCallInfo(thread_, 4, js_date.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = date::proto::ValueOf(ecma_runtime_call_info.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(-4357419161618)).GetRawData());
}

TEST_F(BuiltinsDateTest, DateConstructor)
{
    // case1: test new target is undefined.
    JSHandle<JSDate> js_date = JSDateCreateTest(thread_);
    JSHandle<GlobalEnv> global_env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> date_func(global_env->GetDateFunction());
    auto ecma_runtime_call_info1 = CreateAndSetRuntimeCallInfo(thread_, 4, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result1 = date::DateConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result1.IsString());

    // case2: length == 0
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, js_date.GetTaggedValue(), 4);
    ecma_runtime_call_info2->SetFunction(date_func.GetTaggedValue());
    ecma_runtime_call_info2->SetThis(js_date.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    JSTaggedValue result2 = date::DateConstructor(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result2.IsObject());

    // case3: length == 1
    auto ecma_runtime_call_info3 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, js_date.GetTaggedValue(), 6);
    ecma_runtime_call_info3->SetFunction(date_func.GetTaggedValue());
    ecma_runtime_call_info3->SetThis(js_date.GetTaggedValue());
    ecma_runtime_call_info3->SetCallArg(0, JSTaggedValue(static_cast<double>(2018)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
    JSTaggedValue result3 = date::DateConstructor(ecma_runtime_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result3.IsObject());

    date::proto::SetFullYear(ecma_runtime_call_info3.get());
    JSTaggedValue result4 = date::proto::GetFullYear(ecma_runtime_call_info3.get());
    ASSERT_EQ(result4.GetRawData(), JSTaggedValue(static_cast<double>(2018)).GetRawData());

    // case3: length > 1
    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, js_date.GetTaggedValue(), 8);
    ecma_runtime_call_info4->SetFunction(date_func.GetTaggedValue());
    ecma_runtime_call_info4->SetThis(js_date.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, JSTaggedValue(static_cast<double>(2018)));
    ecma_runtime_call_info4->SetCallArg(1, JSTaggedValue(static_cast<double>(10)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    JSTaggedValue result5 = date::DateConstructor(ecma_runtime_call_info4.get());
    ASSERT_TRUE(result5.IsObject());

    SetAllYearAndHours(thread_, js_date);
    date::proto::SetFullYear(ecma_runtime_call_info4.get());
    JSTaggedValue result6 = date::proto::GetFullYear(ecma_runtime_call_info4.get());
    ASSERT_EQ(result6.GetRawData(), JSTaggedValue(static_cast<double>(2018)).GetRawData());
    JSTaggedValue result7 = date::proto::GetMonth(ecma_runtime_call_info4.get());
    ASSERT_EQ(result7.GetRawData(), JSTaggedValue(static_cast<double>(10)).GetRawData());
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
