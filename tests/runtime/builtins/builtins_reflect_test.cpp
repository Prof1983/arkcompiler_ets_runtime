/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
class BuiltinsReflectTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

// use for create a JSObject of test
static JSHandle<JSFunction> TestObjectCreate(JSThread *thread)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    return JSHandle<JSFunction>::Cast(env->GetObjectFunction());
}

// native function for test Reflect.apply
JSTaggedValue TestReflectApply(EcmaRuntimeCallInfo *argv)
{
    auto thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    int result = 0;
    for (uint32_t index = 0; index < argv->GetArgsNumber(); ++index) {
        result += builtins_common::GetCallArg(argv, index).GetTaggedValue().GetInt();
    }
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    JSTaggedValue test_a =
        JSObject::GetProperty(thread, this_value,
                              JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_reflect_apply_a")))
            .GetValue()
            .GetTaggedValue();
    JSTaggedValue test_b =
        JSObject::GetProperty(thread, this_value,
                              JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_reflect_apply_b")))
            .GetValue()
            .GetTaggedValue();

    result = result + test_a.GetInt() + test_b.GetInt();
    return builtins_common::GetTaggedInt(result);
}

// Reflect.apply (target, thisArgument, argumentsList)
TEST_F(BuiltinsReflectTest, ReflectApply)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSFunction> target = factory->NewJSFunction(env, reinterpret_cast<void *>(TestReflectApply));
    // thisArgument
    JSHandle<JSObject> this_argument = factory->NewJSObjectByConstructor(
        TestObjectCreate(thread_), JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_argument),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_reflect_apply_a")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(11)));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(this_argument),
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_reflect_apply_b")),
                          JSHandle<JSTaggedValue>(thread_, JSTaggedValue(22)));
    // argumentsList
    JSHandle<JSObject> arguments_list(JSArray::ArrayCreate(thread_, JSTaggedNumber(2)));
    PropertyDescriptor desc(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(33)));
    JSArray::DefineOwnProperty(thread_, arguments_list, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0)), desc);

    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(44)));
    JSArray::DefineOwnProperty(thread_, arguments_list, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), desc1);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(*this_argument));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(*arguments_list));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::Apply(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(110).GetRawData());

    JSObject::DeleteProperty(thread_, (this_argument),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_reflect_apply_a")));
    JSObject::DeleteProperty(thread_, (this_argument),
                             JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("test_reflect_apply_b")));
}

// Reflect.construct (target, argumentsList [ , new_target])
TEST_F(BuiltinsReflectTest, ReflectConstruct)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSFunction> target = JSHandle<JSFunction>::Cast(env->GetStringFunction());
    // argumentsList
    JSHandle<JSObject> arguments_list(JSArray::ArrayCreate(thread_, JSTaggedNumber(1)));
    PropertyDescriptor desc(thread_,
                            JSHandle<JSTaggedValue>::Cast(factory->NewFromCanBeCompressString("ReflectConstruct")));
    JSArray::DefineOwnProperty(thread_, arguments_list, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0)), desc);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(*arguments_list));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::Construct(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSTaggedValue> tagged_result(thread_, result);
    JSHandle<JSPrimitiveRef> ref_result = JSHandle<JSPrimitiveRef>::Cast(tagged_result);
    JSHandle<EcmaString> ruler = factory->NewFromCanBeCompressString("ReflectConstruct");
    ASSERT_EQ(EcmaString::Cast(ref_result->GetValue().GetTaggedObject())->Compare(*ruler), 0);
}

// Reflect.defineProperty (target, propertyKey, attributes)
TEST_F(BuiltinsReflectTest, ReflectDefineProperty)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    // propertyKey
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString("test_reflect_define_property"));
    // attributes
    JSHandle<JSObject> attributes = factory->NewJSObjectByConstructor(
        TestObjectCreate(thread_), JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    // attributes value
    auto global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> value_key = global_const->GetHandledValueString();
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(100));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(attributes), value_key, value);
    // attributes writable
    JSHandle<JSTaggedValue> writable_key = global_const->GetHandledWritableString();
    JSHandle<JSTaggedValue> writable(thread_, JSTaggedValue::True());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(attributes), writable_key, writable);
    // attributes enumerable
    JSHandle<JSTaggedValue> enumerable_key = global_const->GetHandledEnumerableString();
    JSHandle<JSTaggedValue> enumerable(thread_, JSTaggedValue::False());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(attributes), enumerable_key, enumerable);
    // attributes configurable
    JSHandle<JSTaggedValue> configurable_key = global_const->GetHandledConfigurableString();
    JSHandle<JSTaggedValue> configurable(thread_, JSTaggedValue::True());
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(attributes), configurable_key, configurable);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, key.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue(*attributes));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::DefineProperty(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());

    PropertyDescriptor desc_ruler(thread_);
    JSObject::GetOwnProperty(thread_, target, key, desc_ruler);
    ASSERT_EQ(desc_ruler.GetValue()->GetInt(), 100);
    ASSERT_EQ(desc_ruler.IsWritable(), true);
    ASSERT_EQ(desc_ruler.IsEnumerable(), false);
    ASSERT_EQ(desc_ruler.IsConfigurable(), true);
}

// Reflect.deleteProperty (target, propertyKey)
TEST_F(BuiltinsReflectTest, ReflectDeleteProperty)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    // propertyKey
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString("test_reflect_delete_property"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(101));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(target), key, value);

    PropertyDescriptor desc(thread_);
    ASSERT_EQ(JSObject::GetOwnProperty(thread_, target, key, desc), true);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::DeleteProperty(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
    ASSERT_EQ(JSObject::GetOwnProperty(thread_, target, key, desc), false);
}

// Reflect.get (target, propertyKey [ , receiver])
TEST_F(BuiltinsReflectTest, ReflectGet)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    // propertyKey
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString("test_reflect_get"));
    // set property
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(101.5));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(target), key, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::Get(ecma_runtime_call_info.get());

    JSHandle<JSTaggedValue> result_value(thread_, result);
    ASSERT_EQ(result_value->GetDouble(), 101.5);
}

// Reflect.getOwnPropertyDescriptor ( target, propertyKey )
TEST_F(BuiltinsReflectTest, ReflectGetOwnPropertyDescriptor)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    // propertyKey
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString("test_reflect_get_property_descriptor"));
    PropertyDescriptor desc(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(102)), true, false, true);
    ASSERT_EQ(JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(target), key, desc), true);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::GetOwnPropertyDescriptor(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> result_obj(thread_, result);
    // test value
    auto global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> value_key = global_const->GetHandledValueString();
    JSHandle<JSTaggedValue> result_value = JSObject::GetProperty(thread_, result_obj, value_key).GetValue();
    ASSERT_EQ(result_value->GetInt(), 102);
    // test writable
    JSHandle<JSTaggedValue> writable_key = global_const->GetHandledWritableString();
    JSHandle<JSTaggedValue> result_writable = JSObject::GetProperty(thread_, result_obj, writable_key).GetValue();
    ASSERT_EQ(result_writable->ToBoolean(), true);
    // test enumerable
    JSHandle<JSTaggedValue> enumerable_key = global_const->GetHandledEnumerableString();
    JSHandle<JSTaggedValue> result_enumerable = JSObject::GetProperty(thread_, result_obj, enumerable_key).GetValue();
    ASSERT_EQ(result_enumerable->ToBoolean(), false);
    // test configurable
    JSHandle<JSTaggedValue> configurable_key = global_const->GetHandledConfigurableString();
    JSHandle<JSTaggedValue> result_configurable =
        JSObject::GetProperty(thread_, result_obj, configurable_key).GetValue();
    ASSERT_EQ(result_configurable->ToBoolean(), true);
}

// Reflect.getPrototypeOf (target)
TEST_F(BuiltinsReflectTest, ReflectGetPrototypeOf)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    JSHandle<JSObject> proto = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                 JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));

    ASSERT_EQ(JSObject::SetPrototype(thread_, target, JSHandle<JSTaggedValue>(proto)), true);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::GetPrototypeOf(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSTaggedValue> result_obj(thread_, JSTaggedValue(reinterpret_cast<TaggedObject *>(result.GetRawData())));
    ASSERT_EQ(JSTaggedValue::SameValue(result_obj.GetTaggedValue(), proto.GetTaggedValue()), true);
}

// Reflect.has (target, propertyKey)
TEST_F(BuiltinsReflectTest, ReflectHas)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    // propertyKey
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString("test_reflect_has"));
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(103));
    ASSERT_EQ(JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(target), key, value), true);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, key.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::Has(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
}

// Reflect.isExtensible (target)
TEST_F(BuiltinsReflectTest, ReflectIsExtensible)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    target->GetJSHClass()->SetExtensible(false);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::IsExtensible(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

// Reflect.ownKeys (target)
TEST_F(BuiltinsReflectTest, ReflectOwnKeys)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    JSHandle<JSTaggedValue> key0(factory->NewFromCanBeCompressString("test_reflect_own_keys1"));
    JSHandle<JSTaggedValue> value0(thread_, JSTaggedValue(104));
    ASSERT_EQ(JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(target), key0, value0), true);
    JSHandle<JSTaggedValue> key1(factory->NewFromCanBeCompressString("test_reflect_own_keys2"));
    JSHandle<JSTaggedValue> value1(thread_, JSTaggedValue(105));
    ASSERT_EQ(JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(target), key1, value1), true);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::OwnKeys(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSTaggedValue> result_tagged_value(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSArray> result_array = JSHandle<JSArray>::Cast(result_tagged_value);
    // test length
    JSHandle<JSTaggedValue> result_length_key = thread_->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> result_length =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(result_array), result_length_key).GetValue();
    ASSERT_EQ(result_length->GetInt(), 2);
    // test array[0]
    JSHandle<JSTaggedValue> result_key0(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> result_value0 =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(result_array), result_key0).GetValue();
    ASSERT_EQ(reinterpret_cast<EcmaString *>(result_value0.GetTaggedValue().GetTaggedObject())
                  ->Compare(reinterpret_cast<EcmaString *>(key0.GetTaggedValue().GetTaggedObject())),
              0);
    // test array[1]
    JSHandle<JSTaggedValue> result_key1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> result_value1 =
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(result_array), result_key1).GetValue();
    ASSERT_EQ(reinterpret_cast<EcmaString *>(result_value1.GetTaggedValue().GetTaggedObject())
                  ->Compare(reinterpret_cast<EcmaString *>(key1.GetTaggedValue().GetTaggedObject())),
              0);
}

// Reflect.preventExtensions (target)
TEST_F(BuiltinsReflectTest, ReflectPreventExtensions)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    target->GetJSHClass()->SetExtensible(true);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::PreventExtensions(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
    ASSERT_EQ(target->IsExtensible(), false);
}

// Reflect.set (target, propertyKey, V [ , receiver])
TEST_F(BuiltinsReflectTest, ReflectSet)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    // target
    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    // propertyKey
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString("test_reflect_set"));
    // value
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue(106));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, key.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(2, value.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::Set(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());

    JSHandle<JSTaggedValue> ruler = JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(target), key).GetValue();
    ASSERT_EQ(JSTaggedValue::ToInt32(thread_, ruler), 106);
}

// Reflect.setPrototypeOf (target, proto)
TEST_F(BuiltinsReflectTest, ReflectSetPrototypeOf)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();

    JSHandle<JSObject> target = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                  JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));
    JSHandle<JSObject> proto = factory->NewJSObjectByConstructor(TestObjectCreate(thread_),
                                                                 JSHandle<JSTaggedValue>(TestObjectCreate(thread_)));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, proto.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reflect::SetPrototypeOf(ecma_runtime_call_info.get());

    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());
    JSHandle<JSTaggedValue> result_obj(thread_, target->GetJSHClass()->GetPrototype());
    ASSERT_EQ(JSTaggedValue::SameValue(result_obj.GetTaggedValue(), proto.GetTaggedValue()), true);
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
