/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "file_items.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {
class BuiltinsProxyTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSHandle<JSObject> BuiltinsTestProxyCreate(JSThread *thread)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> obj_fun(global_env->GetObjectFunction());
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    return obj;
}

// 26.2.1.1 Proxy( [ value ] )
TEST_F(BuiltinsProxyTest, ProxyConstructor)
{
    JSHandle<JSObject> target = BuiltinsTestProxyCreate(thread_);
    JSHandle<JSObject> handler = BuiltinsTestProxyCreate(thread_);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Null(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, handler.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = proxy::ProxyConstructor(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSTaggedValue> result_handle(thread_, result);
    EXPECT_TRUE(result_handle->IsJSProxy());
}

// 26.2.2.1 Proxy.revocable ( target, handler )
TEST_F(BuiltinsProxyTest, Revocable)
{
    JSHandle<JSObject> target = BuiltinsTestProxyCreate(thread_);
    JSHandle<JSObject> handler = BuiltinsTestProxyCreate(thread_);

    JSHandle<GlobalEnv> global_env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> proxy_fun(global_env->GetProxyFunction());
    JSHandle<JSTaggedValue> key(thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("prop"));
    PropertyDescriptor desc(thread_);
    desc.SetWritable(false);
    JSObject::DefineOwnProperty(thread_, handler, key, desc);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetCallArg(0, target.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, handler.GetTaggedValue());
    ecma_runtime_call_info->SetNewTarget(JSTaggedValue(*proxy_fun));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = proxy::Revocable(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsECMAObject());
    JSHandle<JSObject> result_handle(thread_, result);

    auto global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> proxy_key = global_const->GetHandledProxyString();
    JSHandle<JSTaggedValue> revoke_key = global_const->GetHandledRevokeString();

    JSHandle<TaggedArray> keys = JSObject::GetOwnPropertyKeys(thread_, result_handle);
    bool pflag = false;
    bool rflag = false;
    for (uint32_t i = 0; i < keys->GetLength(); i++) {
        if (JSTaggedValue::SameValue(keys->Get(i), proxy_key.GetTaggedValue())) {
            pflag = true;
        }
        if (JSTaggedValue::SameValue(keys->Get(i), revoke_key.GetTaggedValue())) {
            rflag = true;
        }
    }
    EXPECT_TRUE(pflag);
    EXPECT_TRUE(rflag);

    PropertyDescriptor desc_res(thread_);
    JSObject::GetOwnProperty(thread_, result_handle, revoke_key, desc_res);
    EXPECT_TRUE(desc_res.GetValue()->IsProxyRevocFunction());
}
}  // namespace panda::test
