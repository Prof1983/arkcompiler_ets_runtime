/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"

#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"

#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/object_operator.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
class BuiltinsArrayTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

    class TestClass {
    public:
        static JSTaggedValue TestForEachFunc(EcmaRuntimeCallInfo *argv)
        {
            JSHandle<JSTaggedValue> key = builtins_common::GetCallArg(argv, 0);
            if (key->IsUndefined()) {
                return JSTaggedValue::Undefined();
            }
            JSArray *js_array = JSArray::Cast(builtins_common::GetThis(argv)->GetTaggedObject());
            int length = js_array->GetArrayLength() + 1;
            js_array->SetArrayLength(argv->GetThread(), length);
            return JSTaggedValue::Undefined();
        }

        static JSTaggedValue TestEveryFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {  // 10 : test case
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        static JSTaggedValue TestMapFunc(EcmaRuntimeCallInfo *argv)
        {
            int accumulator = builtins_common::GetCallArg(argv, 0)->GetInt();
            accumulator = accumulator * 2;  // 2 : mapped to 2 times the original value
            return builtins_common::GetTaggedInt(accumulator);
        }

        static JSTaggedValue TestFindFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                // 10 : test case
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        static JSTaggedValue TestFindIndexFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                // 10 : test case
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        static JSTaggedValue TestReduceFunc(EcmaRuntimeCallInfo *argv)
        {
            int accumulator = builtins_common::GetCallArg(argv, 0)->GetInt();
            accumulator = accumulator + builtins_common::GetCallArg(argv, 1)->GetInt();
            return builtins_common::GetTaggedInt(accumulator);
        }

        static JSTaggedValue TestReduceRightFunc(EcmaRuntimeCallInfo *argv)
        {
            int accumulator = builtins_common::GetCallArg(argv, 0)->GetInt();
            accumulator = accumulator + builtins_common::GetCallArg(argv, 1)->GetInt();
            return builtins_common::GetTaggedInt(accumulator);
        }

        static JSTaggedValue TestSomeFunc(EcmaRuntimeCallInfo *argv)
        {
            uint32_t argc = argv->GetArgsNumber();
            if (argc > 0) {
                if (builtins_common::GetCallArg(argv, 0)->GetInt() > 10) {  // 10 : test case
                    return builtins_common::GetTaggedBoolean(true);
                }
            }
            return builtins_common::GetTaggedBoolean(false);
        }

        // x => [x*2]
        static JSTaggedValue TestFlatMapFunc(EcmaRuntimeCallInfo *argv)  //« element, sourceIndex, source »
        {
            auto thread = argv->GetThread();

            // element = [x]
            JSTaggedValue element = builtins_common::GetCallArg(argv, 0).GetTaggedValue();
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            JSHandle<JSTaggedValue> key0(thread, JSTaggedValue(0));

            // val = x
            JSTaggedValue val = JSArray::GetProperty(thread, JSHandle<JSTaggedValue>(thread, element), key0)
                                    .GetValue()
                                    .GetTaggedValue();
            int accumulator = val.GetNumber();
            accumulator *= 2;

            return builtins_common::GetTaggedInt(accumulator);
        }
    };

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSTaggedValue CreateBuiltinsJSObject(JSThread *thread, const PandaString &key_c_str)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();

    JSHandle<JSTaggedValue> dynclass = global_env->GetObjectFunction();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> obj(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(dynclass), dynclass));
    JSHandle<JSTaggedValue> key(factory->NewFromCanBeCompressString(&key_c_str[0]));
    JSHandle<JSTaggedValue> value(thread, JSTaggedValue(1));
    JSObject::SetProperty(thread, obj, key, value);
    return obj.GetTaggedValue();
}

TEST_F(BuiltinsArrayTest, ArrayConstructor)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSFunction> array(env->GetArrayFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info1->SetFunction(array.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::ArrayConstructor(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(1));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));
}

// 22.1.2.1 Array.from ( items [ , mapfn [ , thisArg ] ] )
TEST_F(BuiltinsArrayTest, From)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0))->GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(5)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::From(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(1));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(2));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, value_handle, key3, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(4));
    JSObject::GetOwnProperty(thread_, value_handle, key4, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));
}

// 22.1.2.2 Array.isArray(arg)
TEST_F(BuiltinsArrayTest, IsArray)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetCallArg(0, obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::IsArray(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::True().GetRawData());

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    result = array::IsArray(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::False().GetRawData());
}

TEST_F(BuiltinsArrayTest, Of)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSFunction> array(env->GetArrayFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info1->SetFunction(array.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::Of(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));

    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(1));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));
}

TEST_F(BuiltinsArrayTest, Species)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSFunction> array(env->GetArrayFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(array.GetTaggedValue());
    ecma_runtime_call_info1->SetThis(global_object.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::GetSpecies(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_TRUE(result.IsECMAObject());
}

TEST_F(BuiltinsArrayTest, Concat)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    auto *arr1 = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();
    EXPECT_TRUE(arr1 != nullptr);
    JSHandle<JSObject> obj1(thread_, arr1);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(0));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj1, key4, desc4);
    JSHandle<JSTaggedValue> key5(thread_, JSTaggedValue(1));
    PropertyDescriptor desc5(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(5)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj1, key5, desc5);
    JSHandle<JSTaggedValue> key6(thread_, JSTaggedValue(2));
    PropertyDescriptor desc6(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(6)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj1, key6, desc6);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, obj1.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Concat(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());

    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    JSHandle<JSTaggedValue> key7(thread_, JSTaggedValue(5));
    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle), length_key_handle).GetValue()->GetInt(),
        6);
    JSObject::GetOwnProperty(thread_, value_handle, key7, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(6));
}

// 22.1.3.3 new Array(1,2,3,4,5).CopyWithin(0,3,5)
TEST_F(BuiltinsArrayTest, CopyWithin)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(5)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::CopyWithin(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(4));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, value_handle, key3, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(4));
    JSObject::GetOwnProperty(thread_, value_handle, key4, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));
}

TEST_F(BuiltinsArrayTest, Every)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(100)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);

    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(200)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(300)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    JSHandle<JSArray> js_array = factory->NewJSArray();
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestEveryFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, js_array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = array::proto::Every(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result2.GetRawData(), JSTaggedValue::True().GetRawData());
}

TEST_F(BuiltinsArrayTest, Map)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(50)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(200)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSArray> js_array(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)));
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestMapFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, js_array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Map(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());

    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle), length_key_handle).GetValue()->GetInt(),
        3);
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);

    ASSERT_EQ(desc_res.GetValue()->GetInt(), 100);
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue()->GetInt(), 400);
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue()->GetInt(), 6);
}

TEST_F(BuiltinsArrayTest, Reverse)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(50)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(200)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Reverse(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());

    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle), length_key_handle).GetValue()->GetInt(),
        3);
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(200));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(50));
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 3);
    JSObject::GetOwnProperty(thread_, obj, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, obj, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(200));
    JSObject::GetOwnProperty(thread_, obj, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(50));
}

TEST_F(BuiltinsArrayTest, Slice)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(4)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Slice(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());

    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle), length_key_handle).GetValue()->GetInt(),
        3);
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(2));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(4));
}

TEST_F(BuiltinsArrayTest, Splice)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(5)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(2)));
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<int32_t>(100)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Splice(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 4);

    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle), length_key_handle).GetValue()->GetInt(),
        2);
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(2));
}

// ES2019 22.1.3.10 Builtin Array.flat()
TEST_F(BuiltinsArrayTest, Flat)
{
    ASSERT_NE(thread_, nullptr);
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();

    auto *arr1 = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();
    auto *arr2 = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();
    auto *arr3 = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();

    EXPECT_TRUE(arr1 != nullptr);
    EXPECT_TRUE(arr2 != nullptr);
    EXPECT_TRUE(arr3 != nullptr);

    JSHandle<JSObject> obj1(thread_, arr1);
    JSHandle<JSObject> obj2(thread_, arr2);
    JSHandle<JSObject> obj3(thread_, arr3);

    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), length_key_handle).GetValue()->GetInt(), 0);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj2), length_key_handle).GetValue()->GetInt(), 0);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj3), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));

    // arr1 = [0, 1, arr2]
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(0)), true, true, true);
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    PropertyDescriptor desc_nested1(thread_, JSHandle<JSTaggedValue>(thread_, obj2.GetTaggedValue()), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj1, key0, desc0);
    JSArray::DefineOwnProperty(thread_, obj1, key1, desc1);
    JSArray::DefineOwnProperty(thread_, obj1, key2, desc_nested1);

    // arr2 = [2, 3, arr3]
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    PropertyDescriptor desc_nested2(thread_, JSHandle<JSTaggedValue>(thread_, obj3.GetTaggedValue()), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj2, key0, desc2);
    JSArray::DefineOwnProperty(thread_, obj2, key1, desc3);
    JSArray::DefineOwnProperty(thread_, obj2, key2, desc_nested2);

    // arr3 = [4, 5]
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    PropertyDescriptor desc5(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(5)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj3, key0, desc4);
    JSArray::DefineOwnProperty(thread_, obj3, key1, desc5);

    // [0, 1, [2, 3, [4, 5]]].flat(2) = [0, 1, 2, 3, 4, 5]
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj1.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(2)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Flat(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    PropertyDescriptor desc_res(thread_);

    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    JSHandle<JSObject> value_handle(thread_, value);

    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle), length_key_handle).GetValue()->GetInt(),
        6);

    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    JSHandle<JSTaggedValue> key5(thread_, JSTaggedValue(5));

    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(0));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(1));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(2));
    JSObject::GetOwnProperty(thread_, value_handle, key3, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));
    JSObject::GetOwnProperty(thread_, value_handle, key4, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(4));
    JSObject::GetOwnProperty(thread_, value_handle, key5, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));

    // [0, 1, [2, 3, [4, 5]]].flat() = [0, 1, 2, 3, [4, 5]]
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(obj1.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    result = array::proto::Flat(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);

    JSTaggedValue value2(static_cast<JSTaggedType>(result.GetRawData()));
    JSHandle<JSObject> value_handle2(thread_, value2);

    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle2), length_key_handle).GetValue()->GetInt(),
        5);

    JSObject::GetOwnProperty(thread_, value_handle2, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(0));
    JSObject::GetOwnProperty(thread_, value_handle2, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(1));
    JSObject::GetOwnProperty(thread_, value_handle2, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(2));
    JSObject::GetOwnProperty(thread_, value_handle2, key3, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(3));

    JSObject::GetOwnProperty(thread_, value_handle2, key4, desc_res);
    ASSERT_TRUE(desc_res.GetValue().GetTaggedValue().IsECMAObject());

    JSTaggedValue inner_arr_value(desc_res.GetValue().GetTaggedValue());
    JSHandle<JSObject> inner_arr_handle(thread_, inner_arr_value);

    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(inner_arr_handle), length_key_handle)
                  .GetValue()
                  ->GetInt(),
              2);

    JSObject::GetOwnProperty(thread_, inner_arr_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(4));
    JSObject::GetOwnProperty(thread_, inner_arr_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));
}

// ES2019 22.1.3.11 Builtin Array.flatMap()
TEST_F(BuiltinsArrayTest, FlatMap)
{
    // [[1], [2], [4]].flatMap(x => [x*2]) = [2, 4, 8]
    ASSERT_NE(thread_, nullptr);
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    auto *arr = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();
    auto *arr1 = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();
    auto *arr2 = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();
    auto *arr3 = JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetObject<JSArray>();

    EXPECT_TRUE(arr != nullptr);
    EXPECT_TRUE(arr1 != nullptr);
    EXPECT_TRUE(arr2 != nullptr);
    EXPECT_TRUE(arr3 != nullptr);

    JSHandle<JSObject> obj(thread_, arr);
    JSHandle<JSObject> obj1(thread_, arr1);
    JSHandle<JSObject> obj2(thread_, arr2);
    JSHandle<JSObject> obj3(thread_, arr3);

    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), length_key_handle).GetValue()->GetInt(), 0);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj2), length_key_handle).GetValue()->GetInt(), 0);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj3), length_key_handle).GetValue()->GetInt(), 0);

    // create [1], [2], [4] elements
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedNumber(0));
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedNumber(1));
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedNumber(2));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSObject::DefineOwnProperty(thread_, obj1, key0, desc1);
    JSObject::DefineOwnProperty(thread_, obj2, key0, desc2);
    JSObject::DefineOwnProperty(thread_, obj3, key0, desc4);

    // arr = [[1], [2], [4]]
    PropertyDescriptor obj_desc1(thread_, JSHandle<JSTaggedValue>(thread_, obj1.GetTaggedValue()), true, true, true);
    PropertyDescriptor obj_desc2(thread_, JSHandle<JSTaggedValue>(thread_, obj2.GetTaggedValue()), true, true, true);
    PropertyDescriptor obj_desc4(thread_, JSHandle<JSTaggedValue>(thread_, obj3.GetTaggedValue()), true, true, true);
    JSObject::DefineOwnProperty(thread_, obj, key0, obj_desc1);
    JSObject::DefineOwnProperty(thread_, obj, key1, obj_desc2);
    JSObject::DefineOwnProperty(thread_, obj, key2, obj_desc4);

    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj1), length_key_handle).GetValue()->GetInt(), 1);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj2), length_key_handle).GetValue()->GetInt(), 1);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj3), length_key_handle).GetValue()->GetInt(), 1);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 3);

    JSHandle<JSFunction> cb = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestFlatMapFunc));
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, cb.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::FlatMap(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    PropertyDescriptor res_desc(thread_);

    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    JSHandle<JSObject> value_handle(thread_, value);

    EXPECT_EQ(
        JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(value_handle), length_key_handle).GetValue()->GetInt(),
        3);

    JSObject::GetOwnProperty(thread_, value_handle, key0, res_desc);
    ASSERT_EQ(res_desc.GetValue().GetTaggedValue(), JSTaggedValue(2));
    JSObject::GetOwnProperty(thread_, value_handle, key1, res_desc);
    ASSERT_EQ(res_desc.GetValue().GetTaggedValue(), JSTaggedValue(4));
    JSObject::GetOwnProperty(thread_, value_handle, key2, res_desc);
    ASSERT_EQ(res_desc.GetValue().GetTaggedValue(), JSTaggedValue(8));
}

// 22.1.3.6 new Array(1,2,3,4,5).Fill(0,1,3)
TEST_F(BuiltinsArrayTest, Fill)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(5)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(0)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info1->SetCallArg(2, JSTaggedValue(static_cast<int32_t>(3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Fill(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSTaggedValue value(static_cast<JSTaggedType>(result.GetRawData()));
    ASSERT_TRUE(value.IsECMAObject());
    PropertyDescriptor desc_res(thread_);
    JSHandle<JSObject> value_handle(thread_, value);
    JSObject::GetOwnProperty(thread_, value_handle, key0, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(1));
    JSObject::GetOwnProperty(thread_, value_handle, key1, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(0));
    JSObject::GetOwnProperty(thread_, value_handle, key2, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(0));
    JSObject::GetOwnProperty(thread_, value_handle, key3, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(4));
    JSObject::GetOwnProperty(thread_, value_handle, key4, desc_res);
    ASSERT_EQ(desc_res.GetValue().GetTaggedValue(), JSTaggedValue(5));
}

TEST_F(BuiltinsArrayTest, Find)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(102)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSArray> js_array(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)));
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestFindFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, js_array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = array::proto::Find(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_EQ(result2.GetRawData(), JSTaggedValue(102).GetRawData());
}

TEST_F(BuiltinsArrayTest, FindIndex)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);

    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(30)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    JSHandle<JSArray> js_array(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)));
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestFindIndexFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, js_array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = array::proto::FindIndex(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_EQ(result2.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());
}

TEST_F(BuiltinsArrayTest, ForEach)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    JSHandle<JSArray> js_array(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)));
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestForEachFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, js_array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = array::proto::ForEach(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    EXPECT_EQ(result2.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);
    EXPECT_EQ(js_array->GetArrayLength(), 3);
}

// ES2021 23.1.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsArrayTest, Includes1)
{
    ASSERT_NE(thread_, nullptr);

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetHeapObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    // new Array(1,2,3,4,3).includes(1,0)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(true).GetRawData());
}

// ES2021 23.1.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsArrayTest, Includes2)
{
    ASSERT_NE(thread_, nullptr);

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetHeapObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    // new Array(1,2,3,4,3).includes(1,3)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(false).GetRawData());
}

// ES2021 23.1.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsArrayTest, Includes3)
{
    ASSERT_NE(thread_, nullptr);

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetHeapObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    // new Array(1,2,3,4,3).includes(5,0)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(false).GetRawData());
}

// ES2021 23.1.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsArrayTest, Includes4)
{
    ASSERT_NE(thread_, nullptr);

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetHeapObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    // new Array(1,2,3,4,3).includes(1)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(true).GetRawData());
}

// ES2021 23.1.3.13 new Array(1,2,3,4,3).includes(searchElement [ , fromIndex ])
TEST_F(BuiltinsArrayTest, Includes5)
{
    ASSERT_NE(thread_, nullptr);

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetHeapObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    // new Array(1,2,3,4,3).includes(5)
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = array::proto::Includes(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(false).GetRawData());
}

// 22.1.3.11 new Array(1,2,3,4,3).IndexOf(searchElement [ , fromIndex ])
TEST_F(BuiltinsArrayTest, IndexOf)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::IndexOf(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info2->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    result = array::proto::IndexOf(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(4)).GetRawData());

    auto ecma_runtime_call_info3 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info3->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info3->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info3->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info3->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(0)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
    result = array::proto::IndexOf(ecma_runtime_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(-1).GetRawData());

    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info4->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info4->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    result = array::proto::IndexOf(ecma_runtime_call_info4.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());
}

// 22.1.3.14 new Array(1,2,3,4,3).LastIndexOf(searchElement [ , fromIndex ])
TEST_F(BuiltinsArrayTest, LastIndexOf)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    PropertyDescriptor desc3(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key3, desc3);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    PropertyDescriptor desc4(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key4, desc4);

    // new Array(1,2,3,4,3).LastIndexOf(3,4)
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(4)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::LastIndexOf(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(4)).GetRawData());

    // new Array(1,2,3,4,3).LastIndexOf(3,3)
    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info2->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));
    ecma_runtime_call_info2->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(3)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    result = array::proto::LastIndexOf(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(2)).GetRawData());

    // new Array(1,2,3,4,3).LastIndexOf(5,4)
    auto ecma_runtime_call_info3 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info3->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info3->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info3->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(5)));
    ecma_runtime_call_info3->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(4)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info3.get());
    result = array::proto::LastIndexOf(ecma_runtime_call_info3.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(-1).GetRawData());

    // new Array(1,2,3,4,3).LastIndexOf(3)
    auto ecma_runtime_call_info4 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info4->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info4->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info4->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(3)));

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info4.get());
    result = array::proto::LastIndexOf(ecma_runtime_call_info4.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(4)).GetRawData());
}

// 22.1.3.11 new Array().Pop()
TEST_F(BuiltinsArrayTest, Pop)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Pop(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    auto ecma_runtime_call_info2 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info2->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info2->SetThis(obj.GetTaggedValue());

    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info2.get());
    result = array::proto::Pop(ecma_runtime_call_info2.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(3).GetRawData());
}

// 22.1.3.11 new Array(1,2,3).Push(...items)
TEST_F(BuiltinsArrayTest, Push)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(4)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Push(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetNumber(), 5);

    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 5);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(3));
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key3).GetValue()->GetInt(), 4);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(4));
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key4).GetValue()->GetInt(), 5);
}

TEST_F(BuiltinsArrayTest, Reduce)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestReduceFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(10)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Reduce(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(16).GetRawData());
}

TEST_F(BuiltinsArrayTest, ReduceRight)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);
    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestReduceRightFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(10)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::ReduceRight(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(16).GetRawData());
}

TEST_F(BuiltinsArrayTest, Shift)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Shift(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(1).GetRawData());
}

TEST_F(BuiltinsArrayTest, Some)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(20)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    JSHandle<JSArray> js_array(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)));
    JSHandle<JSFunction> func = factory->NewJSFunction(env, reinterpret_cast<void *>(TestClass::TestSomeFunc));

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, func.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(1, js_array.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = array::proto::Some(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    ASSERT_EQ(result2.GetRawData(), JSTaggedValue::True().GetRawData());
}

TEST_F(BuiltinsArrayTest, Sort)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = array::proto::Sort(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    EXPECT_TRUE(result2.IsECMAObject());
    JSHandle<JSTaggedValue> result_arr =
        JSHandle<JSTaggedValue>(thread_, JSTaggedValue(static_cast<JSTaggedType>(result2.GetRawData())));
    EXPECT_EQ(JSArray::GetProperty(thread_, result_arr, key0).GetValue()->GetInt(), 1);
    EXPECT_EQ(JSArray::GetProperty(thread_, result_arr, key1).GetValue()->GetInt(), 2);
    EXPECT_EQ(JSArray::GetProperty(thread_, result_arr, key2).GetValue()->GetInt(), 3);
}

TEST_F(BuiltinsArrayTest, Unshift)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSObject> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(1)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)), true, true, true);
    JSArray::DefineOwnProperty(thread_, obj, key2, desc2);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(4)));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Unshift(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);

    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(5)).GetRawData());

    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), length_key_handle).GetValue()->GetInt(), 5);
    JSHandle<JSTaggedValue> key3(thread_, JSTaggedValue(0));
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key3).GetValue()->GetInt(), 4);
    JSHandle<JSTaggedValue> key4(thread_, JSTaggedValue(1));
    EXPECT_EQ(JSArray::GetProperty(thread_, JSHandle<JSTaggedValue>(obj), key4).GetValue()->GetInt(), 5);
}

TEST_F(BuiltinsArrayTest, Join)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSTaggedValue> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, obj, length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)));
    JSArray::DefineOwnProperty(thread_, JSHandle<JSObject>(obj), key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)));
    JSArray::DefineOwnProperty(thread_, JSHandle<JSObject>(obj), key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)));
    JSArray::DefineOwnProperty(thread_, JSHandle<JSObject>(obj), key2, desc2);

    JSHandle<EcmaString> str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2,3,4");
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Join(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    [[maybe_unused]] auto *res = EcmaString::Cast(result_handle.GetTaggedValue().GetTaggedObject());

    ASSERT_EQ(res->Compare(*str), 0);
}

TEST_F(BuiltinsArrayTest, ToString)
{
    JSHandle<JSTaggedValue> length_key_handle = thread_->GlobalConstants()->GetHandledLengthString();
    JSArray *arr = JSArray::Cast(JSArray::ArrayCreate(thread_, JSTaggedNumber(0)).GetTaggedValue().GetTaggedObject());
    EXPECT_TRUE(arr != nullptr);
    JSHandle<JSTaggedValue> obj(thread_, arr);
    EXPECT_EQ(JSArray::GetProperty(thread_, obj, length_key_handle).GetValue()->GetInt(), 0);

    JSHandle<JSTaggedValue> key0(thread_, JSTaggedValue(0));
    PropertyDescriptor desc0(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(2)));
    JSArray::DefineOwnProperty(thread_, JSHandle<JSObject>(obj), key0, desc0);
    JSHandle<JSTaggedValue> key1(thread_, JSTaggedValue(1));
    PropertyDescriptor desc1(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(3)));
    JSArray::DefineOwnProperty(thread_, JSHandle<JSObject>(obj), key1, desc1);
    JSHandle<JSTaggedValue> key2(thread_, JSTaggedValue(2));
    PropertyDescriptor desc2(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(4)));
    JSArray::DefineOwnProperty(thread_, JSHandle<JSObject>(obj), key2, desc2);

    JSHandle<EcmaString> str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("2,3,4");
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(obj.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result = array::proto::Join(ecma_runtime_call_info1.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    [[maybe_unused]] auto *res = EcmaString::Cast(result_handle.GetTaggedValue().GetTaggedObject());

    ASSERT_EQ(res->Compare(*str), 0);
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
