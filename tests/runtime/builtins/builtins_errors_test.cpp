/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/base/error_helper.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"

#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {
using JSType = ecmascript::JSType;

class BuiltinsErrorsTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "BuiltinsErrorsTest SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "BuiltinsErrorsTest TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    PandaVM *instance_ {nullptr};

private:
    EcmaHandleScope *scope_ {nullptr};
};

/*
 * @tc.name: GetJSErrorObject
 * @tc.desc: get JSError Object
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, GetJSErrorObject)
{
    /// @tc.steps: step1. Create JSError object
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();

    JSHandle<JSObject> handle_obj = factory->GetJSError(ErrorType::TYPE_ERROR);
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    /// @tc.steps: step2. obtain JSError object prototype chain name property and message property
    JSHandle<JSTaggedValue> msg_value(
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(handle_obj), msg_key).GetValue());
    EXPECT_EQ(reinterpret_cast<EcmaString *>(msg_value->GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(
                      ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())),
              0);
    JSHandle<JSTaggedValue> name_value(
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(handle_obj), name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("TypeError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: GetJSErrorWithMessage
 * @tc.desc: Obtains the TypeError object.
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, GetJSErrorWithMessage)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();

    JSHandle<JSObject> handle_obj = factory->GetJSError(ErrorType::TYPE_ERROR, "I am type error");
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();
    JSHandle<JSTaggedValue> msg_value(
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(handle_obj), msg_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("I am type error")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);
    JSHandle<JSTaggedValue> name_value(
        JSObject::GetProperty(thread_, JSHandle<JSTaggedValue>(handle_obj), name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("TypeError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: ErrorNoParameterConstructor
 * @tc.desc: new Error()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ErrorNoParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetErrorFunction());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 4);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = error::ErrorConstructor(ecma_runtime_call_info.get());

    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(
        reinterpret_cast<EcmaString *>(ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())
            ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
        0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Error")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: ErrorParameterConstructor
 * @tc.desc: new Error("Hello Error!")
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ErrorParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetErrorFunction());
    JSHandle<JSTaggedValue> param_msg(factory->NewFromCanBeCompressString("Hello Error!"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 6);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));
    ecma_runtime_call_info->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = error::ErrorConstructor(ecma_runtime_call_info.get());

    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();
    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());

    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Hello Error!")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Error")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: ErrorNoParameterToString
 * @tc.desc: new Error().toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ErrorNoParameterToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = error::proto::ToString(ecma_runtime_call_info.get());

    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Error")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(*result_handle)),
              0);
}

/*
 * @tc.name: ErrorToString
 * @tc.desc: new Error("This is Error!").toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ErrorToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    JSHandle<JSTaggedValue> handle_msg_key(factory->NewFromCanBeCompressString("message"));
    JSObject::SetProperty(
        thread_, JSHandle<JSTaggedValue>(error), handle_msg_key,
        JSHandle<JSTaggedValue>(thread_, factory->NewFromCanBeCompressString("This is Error!").GetTaggedValue()));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = error::proto::ToString(ecma_runtime_call_info.get());

    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Error: This is Error!")).GetRawData())
                  ->Compare(*result_handle),
              0);
}

/*
 * @tc.name: RangeErrorNoParameterConstructor
 * @tc.desc: new RangeError()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, RangeErrorNoParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetRangeErrorFunction());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = range_error::RangeErrorConstructor(ecma_runtime_call_info.get());

    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(
        reinterpret_cast<EcmaString *>(ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())
            ->Compare(reinterpret_cast<EcmaString *>(JSTaggedValue(msg_value.GetTaggedValue()).GetRawData())),
        0);
    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("RangeError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(JSTaggedValue(name_value.GetTaggedValue()).GetRawData())),
              0);
}

/*
 * @tc.name: RangeErrorParameterConstructor
 * @tc.desc: new RangeError("Hello RangeError!")
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, RangeErrorParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetRangeErrorFunction());
    JSHandle<JSTaggedValue> param_msg(factory->NewFromCanBeCompressString("Hello RangeError!"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 6);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));
    ecma_runtime_call_info->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = range_error::RangeErrorConstructor(ecma_runtime_call_info.get());

    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Hello RangeError!")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("RangeError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: RangeErrorNoParameterToString
 * @tc.desc: new RangeError().toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, RangeErrorNoParameterToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetRangeErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = range_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<JSTaggedValue> result_handle(thread_, result);

    EXPECT_TRUE(result.IsString());

    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("RangeError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(result_handle->GetRawData())),
              0);
}

/*
 * @tc.name: RangeErrorToString
 * @tc.desc: new RangeError("This is RangeError!").toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, RangeErrorToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetRangeErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    JSHandle<JSTaggedValue> handle_msg_key(factory->NewFromCanBeCompressString("message"));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(error), handle_msg_key,
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("This is RangeError!")));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = range_error::proto::ToString(ecma_runtime_call_info.get());

    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(factory->NewFromCanBeCompressString("RangeError: This is RangeError!")->Compare(*result_handle), 0);
}

// new ReferenceError()
/*
 * @tc.name: ReferenceErrorNoParameterConstructor
 * @tc.desc: new ReferenceError()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ReferenceErrorNoParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetReferenceErrorFunction());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 4);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reference_error::ReferenceErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(
        reinterpret_cast<EcmaString *>(ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())
            ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
        0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("ReferenceError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: ReferenceErrorParameterConstructor
 * @tc.desc: new ReferenceError("Hello RangeError!")
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ReferenceErrorParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetReferenceErrorFunction());
    JSHandle<JSTaggedValue> param_msg(factory->NewFromCanBeCompressString("Hello ReferenceError!"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 6);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));
    ecma_runtime_call_info->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reference_error::ReferenceErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();
    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Hello ReferenceError!")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("ReferenceError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: ReferenceErrorNoParameterToString
 * @tc.desc: new ReferenceError().toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ReferenceErrorNoParameterToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetReferenceErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reference_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("ReferenceError")).GetRawData())
                  ->Compare(*result_handle),
              0);
}

/*
 * @tc.name: ReferenceErrorToString
 * @tc.desc: new ReferenceError("This is ReferenceError!").toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, ReferenceErrorToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetReferenceErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    JSHandle<JSTaggedValue> handle_msg_key(factory->NewFromCanBeCompressString("message"));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(error), handle_msg_key,
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("This is ReferenceError!")));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = reference_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(factory->NewFromCanBeCompressString("ReferenceError: This is ReferenceError!")->Compare(*result_handle),
              0);
}

/*
 * @tc.name: TypeErrorNoParameterConstructor
 * @tc.desc: new TypeError()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, TypeErrorNoParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetTypeErrorFunction());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 4);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = type_error::TypeErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(
        reinterpret_cast<EcmaString *>(ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())
            ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
        0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    EXPECT_EQ(reinterpret_cast<EcmaString *>(JSTaggedValue(name_value.GetTaggedValue()).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(JSTaggedValue(name_value.GetTaggedValue()).GetRawData())),
              0);
}

/*
 * @tc.name: TypeErrorParameterConstructor
 * @tc.desc: new TypeError("Hello RangeError!")
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, TypeErrorParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetTypeErrorFunction());
    JSHandle<JSTaggedValue> param_msg(factory->NewFromCanBeCompressString("Hello TypeError!"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 6);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));
    ecma_runtime_call_info->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = type_error::TypeErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Hello TypeError!")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("TypeError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: TypeErrorNoParameterToString
 * @tc.desc: new TypeError().toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, TypeErrorNoParameterToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetTypeErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = type_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("TypeError")).GetRawData())
                  ->Compare(*result_handle),
              0);
}

/*
 * @tc.name: TypeErrorToString
 * @tc.desc: new TypeError("This is TypeError!").toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, TypeErrorToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetTypeErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    JSHandle<JSTaggedValue> value(factory->NewFromCanBeCompressString("This is TypeError!"));
    JSHandle<JSTaggedValue> handle_msg_key(factory->NewFromCanBeCompressString("message"));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(error), handle_msg_key, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = type_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(factory->NewFromCanBeCompressString("TypeError: This is TypeError!")->Compare(*result_handle), 0);
}

/*
 * @tc.name: URIErrorNoParameterConstructor
 * @tc.desc: new URIError()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, URIErrorNoParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetURIErrorFunction());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 4);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = uri_error::URIErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(
        reinterpret_cast<EcmaString *>(ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())
            ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
        0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("URIError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: URIErrorParameterConstructor
 * @tc.desc: new URIError("Hello RangeError!")
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, URIErrorParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetURIErrorFunction());
    JSHandle<JSTaggedValue> param_msg(factory->NewFromCanBeCompressString("Hello URIError!"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 6);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));
    ecma_runtime_call_info->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = uri_error::URIErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Hello URIError!")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("URIError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: URIErrorNoParameterToString
 * @tc.desc: new URIError().toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, URIErrorNoParameterToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetURIErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = uri_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());

    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("URIError")).GetRawData())
                  ->Compare(*result_handle),
              0);
}

/*
 * @tc.name: URIErrorToString
 * @tc.desc: new URIError("This is URIError!").toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, URIErrorToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetURIErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    JSHandle<JSTaggedValue> handle_msg_key(factory->NewFromCanBeCompressString("message"));
    JSObject::SetProperty(
        thread_, JSHandle<JSTaggedValue>(error), handle_msg_key,
        JSHandle<JSTaggedValue>(thread_, factory->NewFromCanBeCompressString("This is URIError!").GetTaggedValue()));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = uri_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());

    EXPECT_EQ(
        reinterpret_cast<EcmaString *>(
            ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("URIError: This is URIError!")).GetRawData())
            ->Compare(*result_handle),
        0);
}

/*
 * @tc.name: SyntaxErrorNoParameterConstructor
 * @tc.desc: new SyntaxError()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, SyntaxErrorNoParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetSyntaxErrorFunction());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 4);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = syntax_error::SyntaxErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(
        reinterpret_cast<EcmaString *>(ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())
            ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
        0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("SyntaxError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: SyntaxErrorParameterConstructor
 * @tc.desc: new SyntaxError("Hello RangeError!")
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, SyntaxErrorParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetSyntaxErrorFunction());
    JSHandle<JSTaggedValue> param_msg(factory->NewFromCanBeCompressString("Hello SyntaxError!"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 6);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));
    ecma_runtime_call_info->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = syntax_error::SyntaxErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Hello SyntaxError!")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("SyntaxError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: SyntaxErrorNoParameterToString
 * @tc.desc: new SyntaxError().toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, SyntaxErrorNoParameterToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetSyntaxErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = syntax_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());

    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("SyntaxError")).GetRawData())
                  ->Compare(*result_handle),
              0);
}

/*
 * @tc.name: SyntaxErrorToString
 * @tc.desc: new SyntaxError("This is SyntaxError!").toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, SyntaxErrorToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetSyntaxErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    JSHandle<JSTaggedValue> handle_msg_key(factory->NewFromCanBeCompressString("message"));
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(error), handle_msg_key,
                          JSHandle<JSTaggedValue>(factory->NewFromCanBeCompressString("This is SyntaxError!")));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = syntax_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());

    EXPECT_EQ(factory->NewFromCanBeCompressString("SyntaxError: This is SyntaxError!")->Compare(*result_handle), 0);
}

/*
 * @tc.name: EvalErrorNoParameterConstructor
 * @tc.desc: new EvalError()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, EvalErrorNoParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetEvalErrorFunction());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 4);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = eval_error::EvalErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(
        reinterpret_cast<EcmaString *>(ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("")).GetRawData())
            ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
        0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("EvalError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: EvalErrorParameterConstructor
 * @tc.desc: new EvalError("Hello RangeError!")
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, EvalErrorParameterConstructor)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSFunction> error(env->GetEvalErrorFunction());
    JSHandle<JSTaggedValue> param_msg(factory->NewFromCanBeCompressString("Hello EvalError!"));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*error), 6);
    ecma_runtime_call_info->SetFunction(error.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));
    ecma_runtime_call_info->SetCallArg(0, param_msg.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = eval_error::EvalErrorConstructor(ecma_runtime_call_info.get());
    EXPECT_TRUE(result.IsECMAObject());

    JSHandle<JSTaggedValue> error_object(thread_, reinterpret_cast<TaggedObject *>(result.GetRawData()));
    JSHandle<JSTaggedValue> msg_key(factory->NewFromCanBeCompressString("message"));
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();

    JSHandle<JSTaggedValue> msg_value(JSObject::GetProperty(thread_, error_object, msg_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("Hello EvalError!")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(msg_value->GetRawData())),
              0);

    JSHandle<JSTaggedValue> name_value(JSObject::GetProperty(thread_, error_object, name_key).GetValue());
    ASSERT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("EvalError")).GetRawData())
                  ->Compare(reinterpret_cast<EcmaString *>(name_value->GetRawData())),
              0);
}

/*
 * @tc.name: EvalErrorNoParameterToString
 * @tc.desc: new EvalError().toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, EvalErrorNoParameterToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();
    JSHandle<JSTaggedValue> error_object = env->GetEvalErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = eval_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(reinterpret_cast<EcmaString *>(
                  ecmascript::JSTaggedValue(*factory->NewFromCanBeCompressString("EvalError")).GetRawData())
                  ->Compare(*result_handle),
              0);
}

/*
 * @tc.name: EvalErrorToString
 * @tc.desc: new EvalError("This is EvalError!").toString()
 * @tc.type: FUNC
 */
TEST_F(BuiltinsErrorsTest, EvalErrorToString)
{
    ObjectFactory *factory = EcmaVM::Cast(instance_)->GetFactory();
    JSHandle<GlobalEnv> env = EcmaVM::Cast(instance_)->GetGlobalEnv();

    JSHandle<JSTaggedValue> error_object = env->GetEvalErrorFunction();
    JSHandle<JSObject> error = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(error_object), error_object);

    JSHandle<JSTaggedValue> handle_msg_key(factory->NewFromCanBeCompressString("message"));
    JSObject::SetProperty(
        thread_, JSHandle<JSTaggedValue>(error), handle_msg_key,
        JSHandle<JSTaggedValue>(thread_, factory->NewFromCanBeCompressString("This is EvalError!").GetTaggedValue()));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue(*error));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = eval_error::proto::ToString(ecma_runtime_call_info.get());
    JSHandle<EcmaString> result_handle(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    EXPECT_TRUE(result.IsString());
    EXPECT_EQ(factory->NewFromCanBeCompressString("EvalError: This is EvalError!")->Compare(*result_handle), 0);
}
}  // namespace panda::test
