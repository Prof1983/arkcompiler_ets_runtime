/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {
class BuiltinsBooleanTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

// new Boolean(123)
TEST_F(BuiltinsBooleanTest, BooleanConstructor)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    JSHandle<JSFunction> boolean(env->GetBooleanFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*boolean), 6);
    ecma_runtime_call_info->SetFunction(boolean.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    // NOLINTNEXTLINE(readability-magic-numbers)
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(123)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = boolean::BooleanConstructor(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());
    ASSERT_EQ(JSPrimitiveRef::Cast(result.GetTaggedObject())->GetValue().IsTrue(), 1);
}

// new Boolean(undefined)
TEST_F(BuiltinsBooleanTest, BooleanConstructor1)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    JSHandle<JSFunction> boolean(env->GetBooleanFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*boolean), 6);
    ecma_runtime_call_info->SetFunction(boolean.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue::Undefined());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = boolean::BooleanConstructor(ecma_runtime_call_info.get());

    ASSERT_TRUE(result.IsECMAObject());
    ASSERT_EQ(JSPrimitiveRef::Cast(result.GetTaggedObject())->GetValue().IsFalse(), 1);
}

// Boolean("helloworld")
TEST_F(BuiltinsBooleanTest, BooleanConstructor2)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();

    JSHandle<JSFunction> boolean(env->GetBooleanFunction());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    JSHandle<EcmaString> str = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("helloworld");

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(boolean.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, str.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = boolean::BooleanConstructor(ecma_runtime_call_info.get());

    JSTaggedValue ruler = builtins_common::GetTaggedBoolean(true);
    ASSERT_EQ(result.GetRawData(), ruler.GetRawData());
}

// false.toString()
TEST_F(BuiltinsBooleanTest, BooleanPrototypeToString)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = boolean::proto::ToString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    auto ruler = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("false");
    ASSERT_EQ(res->Compare(*ruler), 0);
}

// (new Boolean(true)).toString()
TEST_F(BuiltinsBooleanTest, BooleanPrototypeToString1)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> boolean_object(env->GetBooleanFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue::True());
    JSHandle<JSPrimitiveRef> boolean = thread_->GetEcmaVM()->GetFactory()->NewJSPrimitiveRef(boolean_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(boolean.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = boolean::proto::ToString(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsString());
    JSHandle<EcmaString> res(thread_, reinterpret_cast<EcmaString *>(result.GetRawData()));
    auto ruler = thread_->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("true");
    ASSERT_EQ(res->Compare(*ruler), 0);
}

// true.valueOf()
TEST_F(BuiltinsBooleanTest, BooleanPrototypeValueOf)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = boolean::proto::ValueOf(ecma_runtime_call_info.get());

    JSTaggedValue ruler = builtins_common::GetTaggedBoolean(true);
    ASSERT_EQ(result.GetRawData(), ruler.GetRawData());
}

// (new Boolean(false)).valueOf()
TEST_F(BuiltinsBooleanTest, BooleanPrototypeValueOf1)
{
    auto ecma_vm = thread_->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    JSHandle<JSFunction> boolean_object(env->GetBooleanFunction());
    JSHandle<JSTaggedValue> value(thread_, JSTaggedValue::False());
    JSHandle<JSPrimitiveRef> boolean = thread_->GetEcmaVM()->GetFactory()->NewJSPrimitiveRef(boolean_object, value);

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(boolean.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = boolean::proto::ValueOf(ecma_runtime_call_info.get());

    JSTaggedValue ruler = builtins_common::GetTaggedBoolean(false);
    ASSERT_EQ(result.GetRawData(), ruler.GetRawData());
}
}  // namespace panda::test
