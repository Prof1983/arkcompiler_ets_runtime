/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"

#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;

namespace panda::test {
class BuiltinsArrayBufferTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSTaggedValue CreateBuiltinsArrayBuffer(JSThread *thread, int32_t length)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> array_buffer(thread, env->GetArrayBufferFunction().GetTaggedValue());
    JSHandle<JSObject> global_object(thread, env->GetGlobalObject());
    // 6 : test case
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, array_buffer.GetTaggedValue(), 6);
    ecma_runtime_call_info->SetFunction(array_buffer.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(length));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    JSTaggedValue result = builtins::array_buffer::ArrayBufferConstructor(ecma_runtime_call_info.get());
    return result;
}

// new ArrayBuffer(8)
TEST_F(BuiltinsArrayBufferTest, Constructor1)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> array_buffer(thread_, env->GetArrayBufferFunction().GetTaggedValue());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, array_buffer.GetTaggedValue(), 6);
    ecma_runtime_call_info->SetFunction(array_buffer.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(8)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = builtins::array_buffer::ArrayBufferConstructor(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsECMAObject());
}

// (new ArrayBuffer(5)).byteLength
TEST_F(BuiltinsArrayBufferTest, byteLength1)
{
    JSTaggedValue tagged = CreateBuiltinsArrayBuffer(thread_, 5);
    JSHandle<JSArrayBuffer> arr_buf(thread_,
                                    JSArrayBuffer::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(arr_buf.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = builtins::array_buffer::proto::GetByteLength(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(5).GetRawData());
}

// (new ArrayBuffer(10)).slice(1, 5).bytelength
TEST_F(BuiltinsArrayBufferTest, slice1)
{
    // NOLINTNEXTLINE(readability-magic-numbers)
    JSTaggedValue tagged = CreateBuiltinsArrayBuffer(thread_, 10);
    JSHandle<JSArrayBuffer> arr_buf(thread_,
                                    JSArrayBuffer::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(arr_buf.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(static_cast<int32_t>(1)));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(static_cast<int32_t>(5)));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result1 = builtins::array_buffer::proto::Slice(ecma_runtime_call_info.get());
    TestHelper::TearDownFrame(thread_, prev);
    JSHandle<JSArrayBuffer> arr_buf1(thread_,
                                     JSArrayBuffer::Cast(reinterpret_cast<TaggedObject *>(result1.GetRawData())));
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(arr_buf1.GetTaggedValue());
    prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = builtins::array_buffer::proto::GetByteLength(ecma_runtime_call_info1.get());

    ASSERT_EQ(result2.GetRawData(), JSTaggedValue(4).GetRawData());
}
}  // namespace panda::test
