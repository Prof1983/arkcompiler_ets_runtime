/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_set_iterator.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"

#include "plugins/ecmascript/runtime/js_weak_container.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"
#include "utils/bit_utils.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

namespace panda::test {
using JSWeakSet = ecmascript::JSWeakSet;

class BuiltinsWeakSetTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

static JSObject *JSObjectTestCreate(JSThread *thread)
{
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> global_env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> js_func = global_env->GetObjectFunction();
    JSHandle<JSObject> new_obj =
        thread->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(js_func), js_func);
    return *new_obj;
}

JSWeakSet *CreateBuiltinsWeakSet(JSThread *thread)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> new_target(env->GetWeakSetFunction());

    // 4 : test case
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue(*new_target), 4);
    ecma_runtime_call_info->SetFunction(new_target.GetTaggedValue());
    ecma_runtime_call_info->SetThis(JSTaggedValue::Undefined());
    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    JSTaggedValue result = weak_set::WeakSetConstructor(ecma_runtime_call_info.get());

    EXPECT_TRUE(result.IsECMAObject());
    JSWeakSet *js_weak_set = JSWeakSet::Cast(reinterpret_cast<TaggedObject *>(result.GetRawData()));
    return js_weak_set;
}

TEST_F(BuiltinsWeakSetTest, CreateAndGetSize)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> new_target(env->GetWeakSetFunction());
    JSHandle<JSWeakSet> weak_set(thread_, CreateBuiltinsWeakSet(thread_));

    JSHandle<TaggedArray> array(factory->NewTaggedArray(5));
    for (int i = 0; i < 5; i++) {
        JSHandle<JSTaggedValue> key(thread_, JSObjectTestCreate(thread_));
        array->Set(thread_, i, key.GetTaggedValue());
    }

    JSHandle<JSArray> values = JSArray::CreateArrayFromList(thread_, array);
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(new_target.GetTaggedValue());
    ecma_runtime_call_info->SetThis(weak_set.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, values.GetTaggedValue());
    ecma_runtime_call_info->SetNewTarget(new_target.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());

    JSTaggedValue result1 = weak_set::WeakSetConstructor(ecma_runtime_call_info.get());
    JSHandle<JSWeakSet> weak_set_result(thread_,
                                        JSWeakSet::Cast(reinterpret_cast<TaggedObject *>(result1.GetRawData())));
    EXPECT_EQ(weak_set_result->GetSize(), 5);
}

TEST_F(BuiltinsWeakSetTest, AddAndHas)
{
    // create jsWeakSet
    JSHandle<JSWeakSet> weak_set(thread_, CreateBuiltinsWeakSet(thread_));
    JSHandle<JSTaggedValue> key(thread_, JSObjectTestCreate(thread_));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(weak_set.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

    JSWeakSet *js_weak_set;
    {
        [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
        JSTaggedValue result1 = weak_set::proto::Has(ecma_runtime_call_info.get());

        EXPECT_EQ(result1.GetRawData(), JSTaggedValue::False().GetRawData());

        // test Add()
        JSTaggedValue result2 = weak_set::proto::Add(ecma_runtime_call_info.get());
        EXPECT_TRUE(result2.IsECMAObject());
        js_weak_set = JSWeakSet::Cast(reinterpret_cast<TaggedObject *>(result2.GetRawData()));
        EXPECT_EQ(js_weak_set->GetSize(), 1);
    }

    // test Has()
    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(JSTaggedValue(js_weak_set));
    ecma_runtime_call_info1->SetCallArg(0, key.GetTaggedValue());
    {
        [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
        JSTaggedValue result3 = weak_set::proto::Has(ecma_runtime_call_info1.get());

        EXPECT_EQ(result3.GetRawData(), JSTaggedValue::True().GetRawData());
    }
}

TEST_F(BuiltinsWeakSetTest, DeleteAndRemove)
{
    // create jsSet
    JSHandle<JSWeakSet> weak_set(thread_, CreateBuiltinsWeakSet(thread_));

    // add 40 keys
    JSTaggedValue last_key(JSTaggedValue::Undefined());
    // NOLINTNEXTLINE(readability-magic-numbers)
    for (int i = 0; i < 40; i++) {
        JSHandle<JSTaggedValue> key(thread_, JSObjectTestCreate(thread_));

        auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
        ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
        ecma_runtime_call_info->SetThis(weak_set.GetTaggedValue());
        ecma_runtime_call_info->SetCallArg(0, key.GetTaggedValue());

        [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
        JSTaggedValue result1 = weak_set::proto::Add(ecma_runtime_call_info.get());

        EXPECT_TRUE(result1.IsECMAObject());
        JSWeakSet *js_weak_set = JSWeakSet::Cast(reinterpret_cast<TaggedObject *>(result1.GetRawData()));
        EXPECT_EQ(js_weak_set->GetSize(), i + 1);
        last_key = key.GetTaggedValue();
    }
    // whether jsWeakSet has delete lastKey

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(weak_set.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, last_key);

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info1.get());
    JSTaggedValue result2 = weak_set::proto::Has(ecma_runtime_call_info1.get());

    EXPECT_EQ(result2.GetRawData(), JSTaggedValue::True().GetRawData());

    // delete
    JSTaggedValue result3 = weak_set::proto::Delete(ecma_runtime_call_info1.get());

    EXPECT_EQ(result3.GetRawData(), JSTaggedValue::True().GetRawData());

    // check deleteKey is deleted
    JSTaggedValue result4 = weak_set::proto::Has(ecma_runtime_call_info1.get());

    EXPECT_EQ(result4.GetRawData(), JSTaggedValue::False().GetRawData());
}
}  // namespace panda::test
