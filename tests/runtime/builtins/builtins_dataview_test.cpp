/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_dataview.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/tests/runtime/common/test_helper.h"

// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript;
// NOLINTNEXTLINE(google-build-using-namespace)
using namespace panda::ecmascript::builtins;

// NOLINTBEGIN(readability-magic-numbers)

namespace panda::test {
using DataViewType = ecmascript::DataViewType;
class BuiltinsDataViewTest : public testing::Test {
public:
    static void SetUpTestCase()
    {
        GTEST_LOG_(INFO) << "SetUpTestCase";
    }

    static void TearDownTestCase()
    {
        GTEST_LOG_(INFO) << "TearDownCase";
    }

    void SetUp() override
    {
        TestHelper::CreateEcmaVMWithScope(instance_, thread_, scope_);
    }

    void TearDown() override
    {
        TestHelper::DestroyEcmaVMWithScope(instance_, scope_);
    }

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    JSThread *thread_ {nullptr};

private:
    PandaVM *instance_ {nullptr};
    EcmaHandleScope *scope_ {nullptr};
};

JSTaggedValue CreateBuiltinsArrayBuffer(JSThread *thread, int32_t length)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> array_buffer(thread, env->GetArrayBufferFunction().GetTaggedValue());
    JSHandle<JSObject> global_object(thread, env->GetGlobalObject());
    // 6 : test case
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue(*array_buffer), 6);
    ecma_runtime_call_info->SetFunction(array_buffer.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(length));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    JSTaggedValue result = builtins::array_buffer::ArrayBufferConstructor(ecma_runtime_call_info.get());
    return result;
}

JSTaggedValue CreateBuiltinsDataView(JSThread *thread, int32_t length, int32_t byte_offset)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> data_view(thread, env->GetDataViewFunction().GetTaggedValue());
    JSHandle<JSObject> global_object(thread, env->GetGlobalObject());
    JSTaggedValue tagged = CreateBuiltinsArrayBuffer(thread, length);
    JSHandle<JSArrayBuffer> arr_buf(thread, JSArrayBuffer::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    // 8 : test case
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue(*data_view), 8);
    ecma_runtime_call_info->SetFunction(data_view.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, arr_buf.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(byte_offset));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::DataViewConstructor(ecma_runtime_call_info.get());
    return result;
}

void SetUint8(JSThread *thread, const JSHandle<JSDataView> &view, int32_t offset, JSTaggedValue value)
{
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(offset));
    ecma_runtime_call_info->SetCallArg(1, value);

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread, ecma_runtime_call_info.get());
    data_view::proto::SetUint8(ecma_runtime_call_info.get());
}

// new DataView(new ArrayBuffer(10), 1)
TEST_F(BuiltinsDataViewTest, Constructor)
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> data_view(thread_, env->GetDataViewFunction().GetTaggedValue());
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    JSTaggedValue tagged = CreateBuiltinsArrayBuffer(thread_, 10);
    JSHandle<JSArrayBuffer> arr_buf(thread_,
                                    JSArrayBuffer::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue(*data_view), 8);
    ecma_runtime_call_info->SetFunction(data_view.GetTaggedValue());
    ecma_runtime_call_info->SetThis(global_object.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, arr_buf.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(1));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::DataViewConstructor(ecma_runtime_call_info.get());
    ASSERT_TRUE(result.IsECMAObject());
}

// new DataView(new ArrayBuffer(10), 1).byteOffset
TEST_F(BuiltinsDataViewTest, byteOffset)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 10, 1);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetByteOffset(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(1).GetRawData());
}

// new DataView(new ArrayBuffer(10), 2).byteLength
TEST_F(BuiltinsDataViewTest, byteLength)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 10, 2);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetByteLength(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(8).GetRawData());
}

// new DataView(new ArrayBuffer(10), 1).buffer
TEST_F(BuiltinsDataViewTest, buffer)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 10, 1);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 4);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetBuffer(ecma_runtime_call_info.get());
    ASSERT_EQ(result.IsArrayBuffer(), true);
}

// new DataView(new ArrayBuffer(8), 0).SetUint16/GetUint16
TEST_F(BuiltinsDataViewTest, getUint16)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-1870724872));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::SetUint16(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue::True());

    JSTaggedValue result1 = data_view::proto::GetUint16(ecma_runtime_call_info1.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(63488).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetInt16/GetInt16
TEST_F(BuiltinsDataViewTest, getInt16)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-1870724872));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::SetInt16(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue::True());

    JSTaggedValue result1 = data_view::proto::GetInt16(ecma_runtime_call_info1.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(-2048).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetUint8/GetUint32
TEST_F(BuiltinsDataViewTest, GetUint32)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    SetUint8(thread_, view, 0, JSTaggedValue(127));
    SetUint8(thread_, view, 1, JSTaggedValue(255));
    SetUint8(thread_, view, 2, JSTaggedValue(255));
    SetUint8(thread_, view, 3, JSTaggedValue(255));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetUint32(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(2147483647).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetUint8/GetInt32
TEST_F(BuiltinsDataViewTest, GetInt32)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    SetUint8(thread_, view, 0, JSTaggedValue(127));
    SetUint8(thread_, view, 1, JSTaggedValue(255));
    SetUint8(thread_, view, 2, JSTaggedValue(255));
    SetUint8(thread_, view, 3, JSTaggedValue(255));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetInt32(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(2147483647).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetUint8/GetInt8
TEST_F(BuiltinsDataViewTest, GetInt8)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    SetUint8(thread_, view, 0, JSTaggedValue(255));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetInt8(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(-1).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetUint8/GetUint8
TEST_F(BuiltinsDataViewTest, GetUint8)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    SetUint8(thread_, view, 0, JSTaggedValue(127));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetUint8(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(127).GetRawData());
}

// new DataView(new ArrayBuffer(8), 4).SetUint8/GetFloat32
TEST_F(BuiltinsDataViewTest, GetFloat32)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    SetUint8(thread_, view, 4, JSTaggedValue(75));
    SetUint8(thread_, view, 5, JSTaggedValue(75));
    SetUint8(thread_, view, 6, JSTaggedValue(75));
    SetUint8(thread_, view, 7, JSTaggedValue(75));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(4));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetFloat32(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(13323083)).GetRawData());
}

// new DataView(new ArrayBuffer(12), 4).SetUint8/GetFloat64
TEST_F(BuiltinsDataViewTest, GetFloat64)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 12, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    SetUint8(thread_, view, 4, JSTaggedValue(67));
    SetUint8(thread_, view, 5, JSTaggedValue(67));
    SetUint8(thread_, view, 6, JSTaggedValue(68));
    SetUint8(thread_, view, 7, JSTaggedValue(68));
    SetUint8(thread_, view, 8, JSTaggedValue(67));
    SetUint8(thread_, view, 9, JSTaggedValue(67));
    SetUint8(thread_, view, 10, JSTaggedValue(68));
    SetUint8(thread_, view, 11, JSTaggedValue(68));

    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(4));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue::False());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::GetFloat64(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue(static_cast<double>(10846169068898440)).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetUint32/GetUint32
TEST_F(BuiltinsDataViewTest, SetUint32)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(0x907f00f8));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::SetUint32(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue::False());

    JSTaggedValue result1 = data_view::proto::GetUint32(ecma_runtime_call_info1.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(0xf8007f90)).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetInt32/GetInt32
TEST_F(BuiltinsDataViewTest, SetInt32)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-1870724872));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::SetInt32(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue::False());

    JSTaggedValue result1 = data_view::proto::GetInt32(ecma_runtime_call_info1.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(-134185072).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetInt8/GetUint8
TEST_F(BuiltinsDataViewTest, SetInt8)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(-1));
    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::SetInt8(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 6);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(0));
    JSTaggedValue result1 = data_view::proto::GetUint8(ecma_runtime_call_info1.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(255).GetRawData());
}

// new DataView(new ArrayBuffer(4), 0).SetFloat32/GetFloat32
TEST_F(BuiltinsDataViewTest, SetFloat32)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 4, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(42));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::SetFloat32(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue::False());

    JSTaggedValue result1 = data_view::proto::GetFloat32(ecma_runtime_call_info1.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(1.4441781973331565e-41)).GetRawData());
}

// new DataView(new ArrayBuffer(8), 0).SetFloat64/GetFloat64
TEST_F(BuiltinsDataViewTest, SetFloat64)
{
    JSTaggedValue tagged = CreateBuiltinsDataView(thread_, 8, 0);
    JSHandle<JSDataView> view(thread_, JSDataView::Cast(reinterpret_cast<TaggedObject *>(tagged.GetRawData())));
    auto ecma_runtime_call_info = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 10);
    ecma_runtime_call_info->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info->SetCallArg(1, JSTaggedValue(42));
    ecma_runtime_call_info->SetCallArg(2, JSTaggedValue::True());

    [[maybe_unused]] auto prev = TestHelper::SetupFrame(thread_, ecma_runtime_call_info.get());
    JSTaggedValue result = data_view::proto::SetFloat64(ecma_runtime_call_info.get());
    ASSERT_EQ(result.GetRawData(), JSTaggedValue::VALUE_UNDEFINED);

    auto ecma_runtime_call_info1 = TestHelper::CreateEcmaRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), 8);
    ecma_runtime_call_info1->SetFunction(JSTaggedValue::Undefined());
    ecma_runtime_call_info1->SetThis(view.GetTaggedValue());
    ecma_runtime_call_info1->SetCallArg(0, JSTaggedValue(0));
    ecma_runtime_call_info1->SetCallArg(1, JSTaggedValue::False());

    JSTaggedValue result1 = data_view::proto::GetFloat64(ecma_runtime_call_info1.get());
    ASSERT_EQ(result1.GetRawData(), JSTaggedValue(static_cast<double>(8.759e-320)).GetRawData());
}
}  // namespace panda::test

// NOLINTEND(readability-magic-numbers)
