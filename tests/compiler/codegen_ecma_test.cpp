/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <random>
#include "optimizer/ir/inst.h"
#include "optimizer/ir/basicblock.h"
#include "optimizer/optimizations/if_conversion.h"
#include "optimizer/optimizations/lowering.h"
#include "optimizer/optimizations/regalloc/reg_alloc.h"

#include "libpandabase/macros.h"
#include "gtest/gtest.h"
#include "unit_ecma_test.h"
#include "optimizer/code_generator/codegen.h"
#include "vixl_exec_module.h"
#include "runtime/include/coretypes/tagged_value.h"
#include "ir-dyn-base-types.h"

namespace panda::compiler {
class CodegenEcmaTest : public GraphTest {
public:
    CodegenEcmaTest() : exec_module_(GetAllocator(), GetGraph()->GetRuntime()) {}
    ~CodegenEcmaTest() override = default;

    NO_COPY_SEMANTIC(CodegenEcmaTest);
    NO_MOVE_SEMANTIC(CodegenEcmaTest);

    void TestCompareAnyType(AnyBaseType any_type, uint64_t val, bool expected_value);
    void TestAnyTypeCheck(AnyBaseType any_type, uint64_t val);
    template <typename T>
    void CastAnyTypeValue(AnyBaseType boxed_type, TaggedValue boxed_value, T expected_unboxed_value);
    template <typename T>
    void CastValueToAnyType(T unboxed_value, AnyBaseType boxed_type, uint64_t expected_boxed_value);
    template <typename T>
    void CheckReturnValue(Graph *graph, T expected_value);

private:
    VixlExecModule exec_module_;
};

static bool RunCodegen(Graph *graph)
{
    return graph->RunPass<Codegen>();
}

// NOLINTBEGIN(readability-magic-numbers)
template <typename T>
void CodegenEcmaTest::CheckReturnValue(Graph *graph, T expected_value)
{
    SetNumVirtRegs(0);
    ASSERT_TRUE(RegAlloc(graph));
    ASSERT_TRUE(RunCodegen(graph));

    auto code_entry = reinterpret_cast<char *>(graph->GetData().Data());
    auto code_exit = code_entry + graph->GetData().Size();  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)

    ASSERT(code_entry != nullptr && code_exit != nullptr);

    exec_module_.SetInstructions(code_entry, code_exit);
    exec_module_.SetDump(false);

    exec_module_.Execute();
    auto rv = exec_module_.GetRetValue<T>();
    EXPECT_EQ(rv, expected_value);
}

void CodegenEcmaTest::TestCompareAnyType(AnyBaseType any_type, uint64_t val, bool expected_value)
{
    auto graph = GetGraph();
    graph->SetDynamicMethod();
    GRAPH(graph)
    {
        CONSTANT(0, val).any();

        BASIC_BLOCK(2, -1)
        {
            INST(2, Opcode::CompareAnyType).b().AnyType(any_type).Inputs(0);
            INST(3, Opcode::Return).b().Inputs(2);
        }
    }

    CheckReturnValue(graph, expected_value);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstBoolTrue1)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::VALUE_TRUE, true);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstBoolTrue2)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::VALUE_FALSE, true);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstBoolFalse)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::VALUE_TRUE + 1, false);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstDoubleTrue)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, TaggedValue::DOUBLE_ENCODE_OFFSET, true);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstDoubleFalse1)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, TaggedValue::TAG_INT, false);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstDoubleFalse2)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, TaggedValue::TAG_OBJECT + 1, false);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstIntTrue)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_INT_TYPE, TaggedValue::TAG_INT, true);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstIntFalse)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_INT_TYPE, TaggedValue::DOUBLE_ENCODE_OFFSET, false);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstNullTrue)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_NULL_TYPE, TaggedValue::VALUE_NULL, true);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstNullFalse)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_NULL_TYPE, TaggedValue::TAG_INT, false);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstObjectTrue)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_OBJECT_TYPE, TaggedValue::TAG_OBJECT + 1, true);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstObjectFalse)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_OBJECT_TYPE, TaggedValue::TAG_INT, false);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstStringFalse)
{
    // Not implemented.
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_STRING_TYPE, TaggedValue::TAG_OBJECT + 1, false);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstUndefinedTrue)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE, TaggedValue::VALUE_UNDEFINED, true);
}

TEST_F(CodegenEcmaTest, CompareAnyTypeInstUndefinedFalse)
{
    TestCompareAnyType(AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE, TaggedValue::TAG_INT, false);
}

template <typename T>
void CodegenEcmaTest::CastAnyTypeValue(AnyBaseType boxed_type, TaggedValue boxed_value, T expected_unboxed_value)
{
    auto graph = GetGraph();
    graph->SetDynamicMethod();
    GRAPH(graph)
    {
        CONSTANT(0, boxed_value.GetRawData()).any();

        BASIC_BLOCK(2, -1)
        {
            INST(2, Opcode::CastAnyTypeValue).AnyType(boxed_type).Inputs(0);
            INS(2).SetType(AnyBaseTypeToDataType(boxed_type));
            INS(2).SetFlag(inst_flags::Flags::NO_CSE);
            INS(2).SetFlag(inst_flags::Flags::NO_HOIST);
            INST(3, Opcode::Return).Inputs(2);
            INS(3).SetType(AnyBaseTypeToDataType(boxed_type));
        }
    }

    CheckReturnValue(graph, expected_unboxed_value);
}

TEST_F(CodegenEcmaTest, DISABLED_CastAnyTypeValueInstNull)
{
    // Unclear way for using should be fixed after real example.
    CastAnyTypeValue(AnyBaseType::ECMASCRIPT_NULL_TYPE, TaggedValue::Null(), 0);
}

TEST_F(CodegenEcmaTest, DISABLED_CastAnyTypeValueInstUndefined)
{
    // Unclear way for using should be fixed after real example.
    CastAnyTypeValue(AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE, TaggedValue::Undefined(), 0);
}

TEST_F(CodegenEcmaTest, CastAnyTypeValueInstFalse)
{
    CastAnyTypeValue(AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::False(), false);
}

TEST_F(CodegenEcmaTest, CastAnyTypeValueInstTrue)
{
    CastAnyTypeValue(AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::True(), true);
}

TEST_F(CodegenEcmaTest, CastAnyTypeValueInstInt)
{
    constexpr int32_t PAYLOAD = 42;
    auto value = TaggedValue(PAYLOAD);

    ASSERT_TRUE(value.IsInt());
    ASSERT_TRUE(value.GetInt() == PAYLOAD);
    CastAnyTypeValue(AnyBaseType::ECMASCRIPT_INT_TYPE, value, PAYLOAD);
}

TEST_F(CodegenEcmaTest, CastAnyTypeValueInstDouble)
{
    constexpr double PAYLOAD = 42.0;
    auto value = TaggedValue(PAYLOAD);

    ASSERT_TRUE(value.IsDouble());
    ASSERT_TRUE(value.GetDouble() == PAYLOAD);
    CastAnyTypeValue(AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, value, PAYLOAD);
}

TEST_F(CodegenEcmaTest, CastAnyTypeValueInstObject)
{
    // As long as tagging remains the same for all heap objects (strings, array, objects, etc.),
    // this single test is enough.
    const auto *payload_ptr = reinterpret_cast<ObjectHeader *>(0xbee8);
    const auto payload_int = reinterpret_cast<int64_t>(payload_ptr);
    auto value = TaggedValue(payload_ptr);

    ASSERT_TRUE(value.IsHeapObject());
    ASSERT_TRUE(value.GetHeapObject() == payload_ptr);
    ASSERT_TRUE(reinterpret_cast<uintptr_t>(value.GetHeapObject()) == static_cast<uintptr_t>(payload_int));
    CastAnyTypeValue(AnyBaseType::ECMASCRIPT_OBJECT_TYPE, value, payload_int);
}

template <typename T>
void CodegenEcmaTest::CastValueToAnyType(T unboxed_value, AnyBaseType boxed_type, uint64_t expected_boxed_value)
{
    auto graph = GetGraph();
    graph->SetDynamicMethod();
    GRAPH(graph)
    {
        CONSTANT(0, unboxed_value);
        BASIC_BLOCK(2, -1)
        {
            INST(2, Opcode::CastValueToAnyType).any().AnyType(boxed_type).Inputs(0);
            INS(2).SetFlag(inst_flags::Flags::NO_CSE);
            INS(2).SetFlag(inst_flags::Flags::NO_HOIST);
            INST(3, Opcode::Return).any().Inputs(2);
        }
    }

    CheckReturnValue(graph, expected_boxed_value);
}

TEST_F(CodegenEcmaTest, DISABLED_CastValueToAnyTypeInstNull)
{
    // Unclear way for using should be fixed after real example.
    CastValueToAnyType(0, AnyBaseType::ECMASCRIPT_NULL_TYPE, TaggedValue::Null().GetRawData());
}

TEST_F(CodegenEcmaTest, DISABLED_CastValueToAnyTypeInstUndefined)
{
    // Unclear way for using should be fixed after real example.
    CastValueToAnyType(0, AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE, TaggedValue::Undefined().GetRawData());
}

TEST_F(CodegenEcmaTest, CastValueToAnyTypeInstFalse)
{
    CastValueToAnyType(false, AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::False().GetRawData());
}

TEST_F(CodegenEcmaTest, CastValueToAnyTypeInstTrue)
{
    CastValueToAnyType(true, AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::True().GetRawData());
}

TEST_F(CodegenEcmaTest, CastValueToAnyTypeInstInt)
{
    constexpr int32_t PAYLOAD = 42;
    auto value = TaggedValue(PAYLOAD);

    ASSERT_TRUE(value.IsInt());
    ASSERT_TRUE(value.GetInt() == PAYLOAD);
    CastValueToAnyType(PAYLOAD, AnyBaseType::ECMASCRIPT_INT_TYPE, value.GetRawData());
}

TEST_F(CodegenEcmaTest, CastValueToAnyTypeInstDouble)
{
    constexpr double PAYLOAD = 42.0;
    auto value = TaggedValue(PAYLOAD);

    ASSERT_TRUE(value.IsDouble());
    ASSERT_TRUE(value.GetDouble() == PAYLOAD);
    CastValueToAnyType(PAYLOAD, AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, value.GetRawData());
}

TEST_F(CodegenEcmaTest, CastValueToAnyTypeInstObject)
{
    // As long as tagging remains the same for all heap objects (strings, array, objects, etc.),
    // this single test is enough.
    const auto *payload_ptr = reinterpret_cast<ObjectHeader *>(0xbee8);
    const auto payload_int = reinterpret_cast<int64_t>(payload_ptr);
    auto value = TaggedValue(payload_ptr);

    ASSERT_TRUE(value.IsHeapObject());
    ASSERT_TRUE(value.GetHeapObject() == payload_ptr);
    ASSERT_TRUE(reinterpret_cast<uintptr_t>(value.GetHeapObject()) == static_cast<uintptr_t>(payload_int));
    CastValueToAnyType(payload_int, AnyBaseType::ECMASCRIPT_OBJECT_TYPE, value.GetRawData());
}

void CodegenEcmaTest::TestAnyTypeCheck(AnyBaseType any_type, uint64_t val)
{
    auto graph = CreateEmptyGraph();
    graph->SetDynamicMethod();
#ifndef NDEBUG
    graph->SetDynUnitTestFlag();
#endif
    GRAPH(graph)
    {
        CONSTANT(0, val).any();
        CONSTANT(1, 1);

        BASIC_BLOCK(2, -1)
        {
            INST(4, Opcode::SaveState).Inputs(0).SrcVregs({0});
            INST(2, Opcode::AnyTypeCheck).b().AnyType(any_type).Inputs(0, 4);
            INST(3, Opcode::Return).b().Inputs(1);
        }
    }

    CheckReturnValue(graph, 1);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstBoolTrue1)
{
    TestAnyTypeCheck(AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::VALUE_TRUE);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstBoolTrue2)
{
    TestAnyTypeCheck(AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE, TaggedValue::VALUE_FALSE);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstDoubleTrue)
{
    TestAnyTypeCheck(AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, TaggedValue::DOUBLE_ENCODE_OFFSET);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstIntTrue)
{
    TestAnyTypeCheck(AnyBaseType::ECMASCRIPT_INT_TYPE, TaggedValue::TAG_INT);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstNullTrue)
{
    TestAnyTypeCheck(AnyBaseType::ECMASCRIPT_NULL_TYPE, TaggedValue::VALUE_NULL);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstObjectTrue)
{
    TestAnyTypeCheck(AnyBaseType::ECMASCRIPT_OBJECT_TYPE, TaggedValue::TAG_OBJECT + 1);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstUndefinedTrue)
{
    TestAnyTypeCheck(AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE, TaggedValue::VALUE_UNDEFINED);
}

TEST_F(CodegenEcmaTest, AnyTypeCheckInstCommonUndefined)
{
    TestAnyTypeCheck(AnyBaseType::UNDEFINED_TYPE, TaggedValue::VALUE_FALSE);
    TestAnyTypeCheck(AnyBaseType::UNDEFINED_TYPE, TaggedValue::VALUE_TRUE);
    TestAnyTypeCheck(AnyBaseType::UNDEFINED_TYPE, TaggedValue::DOUBLE_ENCODE_OFFSET);
    TestAnyTypeCheck(AnyBaseType::UNDEFINED_TYPE, TaggedValue::TAG_INT);
    TestAnyTypeCheck(AnyBaseType::UNDEFINED_TYPE, TaggedValue::TAG_OBJECT + 1);
    TestAnyTypeCheck(AnyBaseType::UNDEFINED_TYPE, TaggedValue::VALUE_NULL);
    TestAnyTypeCheck(AnyBaseType::UNDEFINED_TYPE, TaggedValue::VALUE_UNDEFINED);
}
// NOLINTEND(readability-magic-numbers)

}  // namespace panda::compiler
