# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set(PANDA_COMPILER_ECMA_TESTS_LIBRARIES arkcompiler arkbase arkassembler arkruntime arkaotmanager aot_builder)

set(INCLUDE_ECMA_DIRS
    ${PANDA_ROOT}
    ${PANDA_ROOT}/compiler
    ${PANDA_ROOT}/compiler/tests
)

set(PANDA_COMPILER_ECMA_TESTS_SOURCES
    unit_ecma_test.cpp
    branch_elimination_ecma_test.cpp
    checks_elimination_ecma_test.cpp
    inline_call_intrinsics_ecma_test.cpp
    ir_builder_ecma_test.cpp
    lowering_ecma_test.cpp
    peepholes_ecma_test.cpp
    inline_intrinsics_ecma_test.cpp
    phi_type_resolving_ecma_test.cpp
    vn_test_ecma.cpp
)

if(NOT PANDA_MINIMAL_VIXL AND PANDA_COMPILER_ENABLE)
    panda_ecmascript_add_gtest(
        CONTAINS_MAIN
        NAME compiler_unit_tests_ecma
        SOURCES
            ${PANDA_COMPILER_ECMA_TESTS_SOURCES}
        LIBRARIES
            ${PANDA_COMPILER_ECMA_TESTS_LIBRARIES}
        SANITIZERS
            ${PANDA_SANITIZERS_LIST}
        INCLUDE_DIRS
            ${INCLUDE_ECMA_DIRS}
        DEPS_TARGETS
            arkstdlib
    )
endif()

#  AMD64 and X86 - for unit tests
if(NOT PANDA_MINIMAL_VIXL AND PANDA_TARGET_AMD64)
    set(PANDA_CODEGEN_ECMA_TESTS_SOURCES
        unit_ecma_test.cpp
    )
    if (PANDA_COMPILER_TARGET_AARCH64)
        list(APPEND PANDA_CODEGEN_ECMA_TESTS_SOURCES
            codegen_ecma_test.cpp
        )
        set_source_files_properties(codegen_ecma_test.cpp PROPERTIES COMPILE_FLAGS "-Wno-shadow -Wno-deprecated-declarations")
    endif()

    panda_ecmascript_add_gtest(
        CONTAINS_MAIN
        NAME compiler_codegen_tests_ecma
        SOURCES
            ${PANDA_CODEGEN_ECMA_TESTS_SOURCES}
        LIBRARIES
            ${PANDA_COMPILER_ECMA_TESTS_LIBRARIES}
        SANITIZERS
            ${PANDA_SANITIZERS_LIST}
        INCLUDE_DIRS
            ${INCLUDE_ECMA_DIRS}
        DEPS_TARGETS
            arkstdlib
    )

endif()
