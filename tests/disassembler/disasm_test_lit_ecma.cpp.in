/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <string>

#include <gtest/gtest.h>
#include "disassembler.h"

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#cmakedefine DISASM_BIN_DIR "@DISASM_BIN_DIR@/"

static inline std::string ExtractArrayBody(const std::string &text, const std::string &header)
{
    auto beg = text.find(header);
    auto end = text.find('}', beg);

    assert(beg != std::string::npos);
    assert(end != std::string::npos);

    return text.substr(beg + header.length(), end - (beg + header.length()));
}

TEST(InstructionsTest, TestLiteralArrayEcma)
{
    panda::disasm::Disassembler d {};

    std::stringstream ss {};
    d.Disassemble(std::string(DISASM_BIN_DIR) + "lit.bc");
    d.Serialize(ss);

    EXPECT_TRUE(ss.str().find(".language ECMAScript") != std::string::npos);

    auto array_body = ExtractArrayBody(ss.str(), ".array array_0 {\n");

    std::string line;
    std::stringstream arr {array_body};
    std::getline(arr, line);
    EXPECT_EQ("\tpanda.JSString \"1\"", line);
    std::getline(arr, line);
    EXPECT_EQ("\ti32 1", line);
    std::getline(arr, line);
    EXPECT_EQ("\tpanda.JSString \"2\"", line);
    std::getline(arr, line);
    EXPECT_EQ("\tf64 1.2", line);
    std::getline(arr, line);
    EXPECT_EQ("\tpanda.JSString \"43\"", line);
    std::getline(arr, line);
    EXPECT_EQ("\tpanda.JSString \"some val\"", line);
}

#undef DISASM_BIN_DIR
