## Requirements

- [Node.JS](https://nodejs.org/en/), **x64**, version `>=16.14.x`

- Linux
    - On Debian-based Linux: `sudo apt-get install build-essential g++ libx11-dev libxkbfile-dev libsecret-1-dev python-is-python3`

## Installing the extension globally
- Open vscode
- Press F1
- Type 'vsix' and press Enter
- Select build VSIX

## Build VSIX file
- sudo npm install -g vsce
- type 'vsce package' in ./vs_plugin

## Testing

Integration tests for highlighting the STS language.

### Running tests for the first time

If you running tests for the first time, you need to run following command in `./tests/vs_plugin` directory

    npm install
### Run tests from CLI

Run following command in `./tests/vs_plugin` directory

    npm run test

### Run tests from vscode GUI
- Open the arkcompiler_ets_runtime folder in vscode
- Press `Ctrl + Shift + B`, to compile ts test into js
- Press `Ctrl + Shift + D`, to open Run and Debug tab
- Choose `Run highlighting tests`
- Press `F5`

## Debugging highlight plugin

If you just want to make sure taht syntax highlighting works, you can follow these steps
- Open the arkcompiler_ets_runtime folder in vscode
- Press `Ctrl + Shift + D`, to open Run and Debug tab
- Choose `Run extension`
- Press `F5`
- In the window that opens, open any .sts file

## Known Issues

- No known issues
