# STS for Visual Studio Code Changelog

## Version 1.0.2: 03.07.2023
### New features
* Support `launch` keyword
* Support `final` keyword
### Improvements
* Add `CHANGELOG.md`
* Add `README.md`
* Add icon for extension and files

### Bug fixes
* Move `package.json` to root `vs_plugin` folder

## Version 1.0.1: 16.03.2023
### New features

* Support `.ets` file extension for STS highlight. [!244](https://gitee.com/openharmony-sig/arkcompiler_ets_runtime/pulls/244)

## Version 1.0.0: 30.09.2022
### New features

* Initial release of syntax highlight extension
* Added tests
