/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_eval.h"
#include "ecma_string.h"
#include "include/mem/panda_string.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter-inl.h"
#include "es2panda.h"
#include "assembler/assembly-emitter.h"

namespace panda::ecmascript {
JSTaggedValue EvalUtils::GetEvaluatedScript(JSThread *thread, const JSHandle<EcmaString> &str,
                                            const es2panda::CompilerOptions &options, uint32_t parser_status)
{
    auto buffer = str->GetCString();

    es2panda::Compiler compiler(es2panda::ScriptExtension::JS);
    es2panda::SourceFile input("eval.js", std::string_view(buffer.get()), false);
    std::unique_ptr<pandasm::Program> program(compiler.Compile(input, options, parser_status));
    ObjectFactory *object_factory = thread->GetEcmaVM()->GetFactory();

    if (!program) {
        const auto &err = compiler.GetError();
        JSHandle<JSObject> error = object_factory->GetJSError(ErrorType::SYNTAX_ERROR, err.Message().c_str());
        THROW_NEW_ERROR_AND_RETURN_VALUE(thread, error.GetTaggedValue(), JSTaggedValue::Exception());
    }

    panda_file::MemoryWriter writer;
    [[maybe_unused]] bool status = pandasm::AsmEmitter::Emit(&writer, *program);
    ASSERT(status);
    const auto &data = writer.GetData();

    auto pf = panda_file::OpenPandaFileFromMemory(data.data(), data.size());
    ASSERT(pf);

    auto *panda_file = pf.get();
    auto *ecma_vm = thread->GetEcmaVM();

    ecma_vm->AddPandaFile(panda_file, false);
    Runtime::GetCurrent()->GetClassLinker()->AddPandaFile(std::move(pf));

    auto resolve_func = ecma_vm->GetInvocableFunction(*panda_file, "func_main_0");
    ASSERT(resolve_func.HasValue());

    JSHandle<JSFunction> func(thread, resolve_func.Value());
    // For DirectEval, functions in envirement and in arguments are different.
    // Compiled code use function form the parameter for creating envirement.
    // So we temporary disable compilation for eval calls.
    auto method = func->GetCallTarget();
    ASSERT(method->GetCompilationStatus() == Method::NOT_COMPILED);
    method->AtomicSetCompilationStatus(Method::NOT_COMPILED, Method::FAILED);
    ASSERT(method->GetCompilationStatus() == Method::FAILED);

    return resolve_func.Value();
}

JSTaggedValue EvalUtils::DirectEval(JSThread *thread, uint32_t parser_status, JSTaggedValue arg0,
                                    JSTaggedValue evalbindings)
{
    [[maybe_unused]] EcmaHandleScope scope(thread);

    JSHandle<JSTaggedValue> arg0_handle(thread, arg0);
    JSHandle<JSTaggedValue> eval_bindings_handle(thread, evalbindings);
    JSHandle<EcmaString> arg_str = JSTaggedValue::ToString(thread, arg0_handle);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, arg_str.GetTaggedValue());

    es2panda::CompilerOptions options;
    options.is_eval = true;
    options.is_direct_eval = true;

    JSHandle<JSFunction> func(thread, GetEvaluatedScript(thread, arg_str, options, parser_status));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, func.GetTaggedValue());
    uint32_t index = 0;

    JSHandle<JSTaggedValue> param_func = JSArray::FastGetPropertyByValue(thread, eval_bindings_handle, index++);
    JSHandle<JSTaggedValue> new_target = JSArray::FastGetPropertyByValue(thread, eval_bindings_handle, index++);
    JSHandle<JSTaggedValue> this_value = JSArray::FastGetPropertyByValue(thread, eval_bindings_handle, index++);
    JSHandle<JSTaggedValue> lexical_context = JSArray::FastGetPropertyByValue(thread, eval_bindings_handle, index++);

    ASSERT(1 == index - js_method_args::NUM_MANDATORY_ARGS);
    auto info = NewRuntimeCallInfo(thread, param_func, this_value, new_target, 1);
    info->SetCallArgs(lexical_context);

    auto res = EcmaInterpreter::ExecuteInEnv(info.Get(), func.GetTaggedValue());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
    return res;
}

JSTaggedValue EvalUtils::Eval(JSThread *thread, const JSHandle<JSTaggedValue> &arg0)
{
    [[maybe_unused]] EcmaHandleScope scope(thread);

    JSHandle<EcmaString> arg_str = JSTaggedValue::ToString(thread, arg0);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, arg_str.GetTaggedValue());

    es2panda::CompilerOptions options;
    options.is_eval = true;
    JSHandle<JSFunction> func(thread, GetEvaluatedScript(thread, arg_str, options));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, func.GetTaggedValue());

    JSHandle<JSTaggedValue> this_value(thread, thread->GetGlobalObject());
    auto info = NewRuntimeCallInfo(thread, func, this_value, JSTaggedValue::Undefined(), 0);
    return EcmaInterpreter::Execute(info.Get());
}

JSTaggedValue EvalUtils::CreateDynamicFunction(EcmaRuntimeCallInfo *argv, DynamicFunctionKind kind)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> function_body(thread, JSTaggedValue::Undefined());
    JSHandle<EcmaString> function_param_str = factory->GetEmptyString();
    uint32_t nargs = argv->GetArgsNumber();

    if (nargs == 1) {
        function_body = builtins_common::GetCallArg(argv, 0);
    } else if (nargs > 1) {
        JSHandle<JSTaggedValue> first_param_handle = builtins_common::GetCallArg(argv, 0);
        function_param_str = JSTaggedValue::ToString(thread, first_param_handle);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

        uint32_t iter = 1;

        while (iter < nargs - 1) {
            JSHandle<EcmaString> comma_str = factory->NewFromCanBeCompressString(",");
            function_param_str = factory->ConcatFromString(function_param_str, comma_str);
            JSHandle<JSTaggedValue> param_handle = builtins_common::GetCallArg(argv, iter);
            JSHandle<EcmaString> param_string = JSTaggedValue::ToString(thread, param_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            function_param_str = factory->ConcatFromString(function_param_str, param_string);
            iter++;
        }

        function_body = builtins_common::GetCallArg(argv, iter);
    }

    JSHandle<EcmaString> result_str = factory->NewFromCanBeCompressString("(");
    JSHandle<EcmaString> function_body_str = factory->GetEmptyString();

    if (!function_body->IsUndefined()) {
        function_body_str = JSTaggedValue::ToString(thread, function_body);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    if (kind == DynamicFunctionKind::ASYNC || kind == DynamicFunctionKind::ASYNC_GENERATOR) {
        // '(async function'
        JSHandle<EcmaString> async_str = factory->NewFromCanBeCompressString("async ");
        result_str = factory->ConcatFromString(result_str, async_str);
    }

    // '(function'
    JSHandle<EcmaString> function_str = factory->NewFromCanBeCompressString("function ");
    result_str = factory->ConcatFromString(result_str, function_str);

    if (kind == DynamicFunctionKind::GENERATOR || kind == DynamicFunctionKind::ASYNC_GENERATOR) {
        // '(function*'
        JSHandle<EcmaString> asterisk_str = factory->NewFromCanBeCompressString("* ");
        result_str = factory->ConcatFromString(result_str, asterisk_str);
    }

    // '(function anonymous('
    JSHandle<EcmaString> anonymous_str = factory->NewFromCanBeCompressString("anonymous(");
    result_str = factory->ConcatFromString(result_str, anonymous_str);

    // '(function anonymous(params'
    result_str = factory->ConcatFromString(result_str, function_param_str);

    // '(function anonymous(params){'
    JSHandle<EcmaString> right_paren_left_brace = factory->NewFromCanBeCompressString("){");
    result_str = factory->ConcatFromString(result_str, right_paren_left_brace);

    // '(function anonymous(params) {body'
    result_str = factory->ConcatFromString(result_str, function_body_str);

    // '(function anonymous(params) {body})'
    JSHandle<EcmaString> right_brace_right_paren = factory->NewFromCanBeCompressString("})");
    result_str = factory->ConcatFromString(result_str, right_brace_right_paren);

    es2panda::CompilerOptions options;
    options.is_eval = true;
    options.is_function_eval = true;
    JSHandle<JSFunction> func(thread, GetEvaluatedScript(thread, result_str, options));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, func.GetTaggedValue());

    JSHandle<JSTaggedValue> this_value(thread, thread->GetGlobalObject());

    auto info = NewRuntimeCallInfo(thread, func, this_value, JSTaggedValue::Undefined(), 0);
    JSHandle<JSTaggedValue> res(thread, EcmaInterpreter::Execute(info.Get()));

    ASSERT(res->IsCallable());

    JSHandle<JSTaggedValue> new_target_handle = argv->GetNewTarget();

    if (new_target_handle->IsUndefined()) {
        return res.GetTaggedValue();
    }

    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> new_target_proto =
        JSTaggedValue::GetProperty(thread, new_target_handle, global_const->GetHandledPrototypeString()).GetValue();

    if (new_target_proto->IsECMAObject()) {
        JSObject::SetPrototype(thread, JSHandle<JSObject>::Cast(res), new_target_proto);
    }

    return res.GetTaggedValue();
}
}  // namespace panda::ecmascript
