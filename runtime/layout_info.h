/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_LAYOUT_INFO_H
#define ECMASCRIPT_LAYOUT_INFO_H

#include "plugins/ecmascript/runtime/tagged_array.h"
#include "plugins/ecmascript/runtime/property_attributes.h"
#include "plugins/ecmascript/runtime/js_object.h"

namespace panda::ecmascript {
struct Properties {
    JSTaggedValue key;
    JSTaggedValue attr;

    static constexpr uint32_t GetKeyOffset()
    {
        return MEMBER_OFFSET(Properties, key);
    }

    static constexpr uint32_t GetAttrOffset()
    {
        return MEMBER_OFFSET(Properties, attr);
    }
};

class LayoutInfo : private TaggedArray {
public:
    static constexpr int32_t MIN_PROPERTIES_LENGTH = JSObject::MIN_PROPERTIES_LENGTH;
    static constexpr int32_t MAX_PROPERTIES_LENGTH = PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES;
    static constexpr int32_t NUMBER_OF_PROPERTIES_INDEX = 0;
    static constexpr int32_t ELEMENTS_START_INDEX = 1;

    inline static LayoutInfo *Cast(ObjectHeader *obj)
    {
        ASSERT(JSTaggedValue(obj).IsTaggedArray());
        return reinterpret_cast<LayoutInfo *>(obj);
    }

    int32_t GetPropertiesCapacity() const;
    int32_t NumberOfElements() const;
    void SetNumberOfElements(const JSThread *thread, int32_t properties);
    uint32_t GetKeyIndex(int index) const;
    uint32_t GetAttrIndex(int index) const;
    void SetPropertyInit(const JSThread *thread, int32_t index, const JSTaggedValue &key,
                         const PropertyAttributes &attr);
    void SetKey(const JSThread *thread, int32_t index, const JSTaggedValue &key);
    void SetNormalAttr(const JSThread *thread, int32_t index, const PropertyAttributes &attr);
    JSTaggedValue GetKey(int32_t index) const;
    PropertyAttributes GetAttr(int32_t index) const;
    JSTaggedValue GetSortedKey(int32_t index) const;
    uint32_t GetSortedIndex(int32_t index) const;
    void SetSortedIndex(const JSThread *thread, int32_t index, int32_t sorted_index);
    void AddKey(const JSThread *thread, int32_t index, const JSTaggedValue &key, const PropertyAttributes &attr);

    inline uint32_t GetLength() const
    {
        return TaggedArray::GetLength();
    }

    inline Properties *GetProperties() const
    {
        return reinterpret_cast<Properties *>(reinterpret_cast<uintptr_t>(this) + GetPropertiesOffset());
    }

    static inline uint32_t ComputeArrayLength(uint32_t properties_number)
    {
        return (properties_number << 1U) + ELEMENTS_START_INDEX;
    }

    static inline uint32_t ComputeGrowCapacity(uint32_t old_capacity)
    {
        uint32_t new_capacity = old_capacity + MIN_PROPERTIES_LENGTH;
        return new_capacity > MAX_PROPERTIES_LENGTH ? MAX_PROPERTIES_LENGTH : new_capacity;
    }

    static inline constexpr uint32_t GetPropertiesOffset()
    {
        return TaggedArray::DATA_OFFSET + ELEMENTS_START_INDEX * JSTaggedValue::TaggedTypeSize();
    }

    int32_t FindElementWithCache(JSThread *thread, JSHClass *cls, JSTaggedValue key, int32_t properties_number);
    int32_t FindElement(JSTaggedValue key, int32_t properties_number);
    int32_t BinarySearch(JSTaggedValue key, int32_t properties_number);
    void GetAllKeys(const JSThread *thread, int32_t end, int32_t offset, TaggedArray *key_array);
    void GetAllKeys(const JSThread *thread, int32_t end, std::vector<JSTaggedValue> &key_vector);
    void GetAllEnumKeys(const JSThread *thread, int32_t end, int32_t offset, TaggedArray *key_array, uint32_t *keys);
    void GetAllNames(const JSThread *thread, int32_t end, const JSHandle<TaggedArray> &key_array, uint32_t *length);

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_LAYOUT_INFO_H
