
/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description: Header file of object wrapper.
 */
#ifndef PANDA_RUNTIME_ECMASCRIPT_WRAPPER_OBJECT_H
#define PANDA_RUNTIME_ECMASCRIPT_WRAPPER_OBJECT_H

#include "include/object_header.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_hclass.h"

namespace panda::ecmascript {
class ObjectWrapper : public TaggedObject {
public:
    static ObjectWrapper *Cast(TaggedObject *object)
    {
        return static_cast<ObjectWrapper *>(object);
    }

    ACCESSORS_START(TaggedObject::TaggedObjectSize())
    ACCESSORS(0, Value)
    ACCESSORS_FINISH(1)
};
}  // namespace panda::ecmascript
#endif  // PANDA_RUNTIME_ECMASCRIPT_WRAPPER_OBJECT_H
