/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/thread_environment_api.h"

namespace panda::ecmascript {
extern "C" uint64_t EcmaResolveSecondArgument(uint64_t function_tagged_value)
{
    JSTaggedValue js_func(function_tagged_value);
    if (js_func.IsJSFunction() && !JSFunction::Cast(js_func.GetHeapObject())->IsStrict()) {
        return JSThread::GetCurrentRaw()->GetEcmaVM()->GetGlobalEnv()->GetGlobalObject().GetRawData();
    }
    return JSTaggedValue::Undefined().GetRawData();
}
}  // namespace panda::ecmascript
