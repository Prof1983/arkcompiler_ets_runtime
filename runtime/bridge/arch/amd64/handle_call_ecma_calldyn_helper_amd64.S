/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// uint64_t EcmaResolveSecondArgument(uint64_t function_tagged_value);
.extern EcmaResolveSecondArgument

// Prepare mandatory params for ecma.call1dyn, ecma.call2dyn, ecma.call3dyn
// regs set as follow
// %rax - insn_ptr, %rbx - frame.vregs, %r12 - method, %r15 - acc, %r13, %r14 - temp

    // (%r13) <- functional object
    movzbl (%rax), %r13d
    movq (%rbx, %r13, FRAME_VREGISTER_SIZE), %r13

    movl JSFUNCTION_FUNCTION_INFO_FLAG_OFFSET(%r13), %r14d
    andl $(1 << JSFUNCTION_FUNCTION_INFO_FLAG_IS_STRICT_START_BIT), %r14d
    jz 1f
    // (r14) <- resolved this
    movq $TAGGED_VALUE_UNDEFINED, %r14
    jmp 2f

1:
    // !IsStrict slowpath
    movq %r13, %rdi
    movq %rax, %r9
    // (r14) <- resolved this
    callq EcmaResolveSecondArgument@plt
    movq %rax, %r14
    movq %r9, %rax

2:
    // ABI arg reg 0 (rdi) <- panda::Method*
    movq %r12, %rdi

    // Reserve stack args
    leal (, %rsi, 8), %r9d
    subq %r9, %rsp
    andq $-16, %rsp

    // ABI arg stack <- functional object
    movq %r13, (0 * FRAME_VREGISTER_SIZE)(%rsp)

    // ABI arg stack <- new.target
    movq $TAGGED_VALUE_UNDEFINED, (1 * FRAME_VREGISTER_SIZE)(%rsp)

    // ABI arg stack <- resolved this
    movq %r14, (2 * FRAME_VREGISTER_SIZE)(%rsp)

