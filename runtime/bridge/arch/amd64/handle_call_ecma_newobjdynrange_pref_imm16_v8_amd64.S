/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// handle ecma.newobjdynrange
// regs set as follow
// %rax - insn_ptr, %rbx - frame.vregs, %r12 - method, %r15 - acc, %r13, %r14 - temp

    // r14 <- acc
    movq %r15, %r14
    // r15d <- num_args
    movzwl (%rax), %r15d
    addl $(3 - 2), %r15d // described in js_decode_call_instr.h

    // rbx <- range in frame.vregs
    movzbl 2(%rax), %r13d
    leaq (%rbx, %r13, FRAME_VREGISTER_SIZE), %rbx

    // ABI arg reg 0 (rdi) <- panda::Method*
    // ABI arg reg 1 (rsi) <- num_args
    movq %r12, %rdi
    movq %r15, %rsi

    // Reserve stack args
    // r12 <- stack pointer
    leal (, %r15d, 8), %r12d
    subq %r12, %rsp
    andq $-16, %rsp
    movq %rsp, %r12

    // stack arg0 <- func (range0)
    // stack arg1 <- new_target (range1)
    // stack arg3 <- this (passed in acc)
    movq (0 * FRAME_VREGISTER_SIZE)(%rbx), %r13
    movq %r13, (0 * 8)(%r12)
    movq (1 * FRAME_VREGISTER_SIZE)(%rbx), %r13
    movq %r13, (1 * 8)(%r12)
    movq %r14, (2 * 8)(%r12)

    subl $3, %r15d
    jz .Linvoke

    addq $(1 * FRAME_VREGISTER_SIZE), %rbx
    addq $(2 * 8), %r12
    shll $3, %r15d
1:
    movq (%rbx, %r15), %r13
    movq %r13, (%r12, %r15)
    subl $FRAME_VREGISTER_SIZE, %r15d
    ja 1b

    jmp .Linvoke
