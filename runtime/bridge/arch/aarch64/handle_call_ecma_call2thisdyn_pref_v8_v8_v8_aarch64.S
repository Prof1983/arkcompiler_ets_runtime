/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// handle ecma.call2thisdyn
// regs set as follow
// x9 - frame.vregs, x10 - insn_ptr, x12 - method, x15 - acc, x13, x14 - temp

    // ABI arg reg 1 (x1) <- number of arguments
    mov x1, #5  // mandatory params + arg0 + arg1
#include "handle_call_ecma_callthisdyn_helper_aarch64.S"

    // ABI arg stack <- arg 1
    ldrb w13, [x10], #1
    ldr x14, [x9, x13, lsl #3] // log2(FRAME_VREGISTER_SIZE)
    str x14, [sp, 3 * FRAME_VREGISTER_SIZE]

    // ABI arg stack <- arg 2
    str x15, [sp, 4 * FRAME_VREGISTER_SIZE]
    b .Linvoke
