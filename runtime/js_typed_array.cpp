/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/accessor_data.h"
#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"

namespace panda::ecmascript {
using TypedArrayHelper = base::TypedArrayHelper;

JSHandle<JSTaggedValue> JSTypedArray::ToPropKey(JSThread *thread, const JSHandle<JSTaggedValue> &key)
{
    if (key->IsSymbol()) {
        return key;
    }
    return JSHandle<JSTaggedValue>(JSTaggedValue::ToString(thread, key));
}
// 9.4.5.1 [[GetOwnProperty]] ( P )
bool JSTypedArray::GetOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                  const JSHandle<JSTaggedValue> &key, PropertyDescriptor &desc)
{
    // 1. Assert : IsPropertyKey(P) is true.
    ASSERT(JSTaggedValue::IsPropertyKey(key));
    // 2. Assert: O is an Object that has a [[ViewedArrayBuffer]] internal slot.
    // 3. If Type(P) is String, then
    //   a. Let numericIndex be CanonicalNumericIndexString(P).
    //   b. Assert: numericIndex is not an abrupt completion.
    //   c. If numericIndex is not undefined, then
    //     i. Let value be IntegerIndexedElementGet (O, numericIndex).
    //     ii. ReturnIfAbrupt(value).
    //     iii. If value is undefined, return undefined.
    //     iv. Return a PropertyDescriptor{ [[Value]]: value, [[Enumerable]]: true, [[Writable]]: true,
    //         [[Configurable]]: false }.
    if (key->IsString()) {
        JSTaggedValue numeric_index = JSTaggedValue::CanonicalNumericIndexString(thread, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
        if (!numeric_index.IsUndefined()) {
            JSHandle<JSTaggedValue> value =
                JSTypedArray::IntegerIndexedElementGet(thread, typedarray, numeric_index).GetValue();
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            if (value->IsUndefined()) {
                return false;
            }
            desc.SetValue(value);
            desc.SetEnumerable(true);
            desc.SetWritable(true);
            desc.SetConfigurable(true);
            return true;
        }
    }
    // 4. Return OrdinaryGetOwnProperty(O, P).
    return JSObject::OrdinaryGetOwnProperty(thread, JSHandle<JSObject>(typedarray), key, desc);
}

// 9.4.5.2 [[HasProperty]] ( P )
bool JSTypedArray::HasProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                               const JSHandle<JSTaggedValue> &key)
{
    // 1. Assert: IsPropertyKey(P) is true.
    ASSERT(JSTaggedValue::IsPropertyKey(key));
    // 2. Assert: O is an Object that has a [[ViewedArrayBuffer]] internal slot.
    // 3. If Type(P) is String, then
    //   a. Let numericIndex be CanonicalNumericIndexString(P).
    //   b. Assert: numericIndex is not an abrupt completion.
    //   c. If numericIndex is not undefined, then
    //     i. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    //     ii. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    //     iii. If IsInteger(numericIndex) is false, return false
    //     iv. If numericIndex = −0, return false.
    //     v. If numericIndex < 0, return false.
    //     vi. If numericIndex ≥ the value of O’s [[ArrayLength]] internal slot, return false.
    //     vii. Return true.
    JSHandle<JSObject> typedarray_obj(typedarray);
    if (key->IsString()) {
        JSTaggedValue numeric_index = JSTaggedValue::CanonicalNumericIndexString(thread, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
        if (!numeric_index.IsUndefined()) {
            JSTaggedValue buffer = JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer();
            if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", false);
            }
            if (!numeric_index.IsInteger()) {
                return false;
            }
            JSHandle<JSTaggedValue> numeric_index_handle(thread, numeric_index);
            JSTaggedNumber numeric_index_number = JSTaggedValue::ToNumber(thread, numeric_index_handle);
            double t_neg_zero = -0.0;
            auto e_zero = JSTaggedNumber(t_neg_zero);
            if (JSTaggedNumber::SameValue(numeric_index_number, e_zero)) {
                return false;
            }

            if (JSTaggedValue::Less(thread, numeric_index_handle, thread->GlobalConstants()->GetHandledZero())) {
                return false;
            }
            int32_t arr_len = TypedArrayHelper::GetArrayLength(thread, typedarray_obj);
            JSHandle<JSTaggedValue> arr_len_handle(thread, JSTaggedValue(arr_len));
            return JSTaggedValue::Less(thread, numeric_index_handle, arr_len_handle);
        }
    }
    // 4. Return OrdinaryHasProperty(O, P).
    PropertyDescriptor desc(thread);
    if (JSObject::OrdinaryGetOwnProperty(thread, typedarray_obj, key, desc)) {
        return true;
    }
    JSTaggedValue parent = typedarray_obj->GetPrototype(thread);
    if (!parent.IsNull()) {
        return JSTaggedValue::HasProperty(thread, JSHandle<JSTaggedValue>(thread, parent), key);
    }
    return false;
}

// 9.4.5.3 [[DefineOwnProperty]] ( P, Desc )
bool JSTypedArray::DefineOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                     const JSHandle<JSTaggedValue> &key, const PropertyDescriptor &desc)
{
    // 1. Assert: IsPropertyKey(P) is true.
    ASSERT(JSTaggedValue::IsPropertyKey(key));
    // 2. Assert: O is an Object that has a [[ViewedArrayBuffer]] internal slot.
    // 3. If Type(P) is String, then
    //   a. Let numericIndex be CanonicalNumericIndexString (P).
    //   b. Assert: numericIndex is not an abrupt completion.
    //   c. If numericIndex is not undefined, then
    JSHandle<JSObject> typedarray_obj(typedarray);
    if (key->IsString()) {
        JSTaggedValue numeric_index = JSTaggedValue::CanonicalNumericIndexString(thread, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
        if (!numeric_index.IsUndefined()) {
            // i. If IsInteger(numericIndex) is false, return false
            // ii. Let intIndex be numericIndex.
            // iii. If intIndex = −0, return false.
            // iv. If intIndex < 0, return false.
            // v. Let length be the value of O’s [[ArrayLength]] internal slot.
            // vi. If intIndex ≥ length, return false.
            // vii. If IsAccessorDescriptor(Desc) is true, return false.
            // viii. If Desc has a [[Configurable]] field and if Desc.[[Configurable]] is true, return false.
            // ix. If Desc has an [[Enumerable]] field and if Desc.[[Enumerable]] is false, return false.
            // x. If Desc has a [[Writable]] field and if Desc.[[Writable]] is false, return false.
            // xi. If Desc has a [[Value]] field, then
            //   1. Let value be Desc.[[Value]].
            //   2. Return IntegerIndexedElementSet (O, intIndex, value).
            // xii. Return true.
            if (!numeric_index.IsInteger()) {
                return false;
            }
            JSHandle<JSTaggedValue> numeric_index_handle(thread, numeric_index);
            JSTaggedNumber numeric_index_number = JSTaggedValue::ToNumber(thread, numeric_index_handle);
            double t_neg_zero = -0.0;
            auto e_zero = JSTaggedNumber(t_neg_zero);
            if (JSTaggedNumber::SameValue(numeric_index_number, e_zero)) {
                return false;
            }
            if (JSTaggedValue::Less(thread, numeric_index_handle, thread->GlobalConstants()->GetHandledZero())) {
                return false;
            }
            int32_t arr_len = TypedArrayHelper::GetArrayLength(thread, typedarray_obj);
            JSHandle<JSTaggedValue> arr_len_handle(thread, JSTaggedValue(arr_len));
            if (!JSTaggedValue::Less(thread, numeric_index_handle, arr_len_handle)) {
                return false;
            }
            if (desc.IsAccessorDescriptor()) {
                return false;
            }
            if (desc.HasConfigurable() && !desc.IsConfigurable()) {
                return false;
            }
            if (desc.HasEnumerable() && !desc.IsEnumerable()) {
                return false;
            }
            if (desc.HasWritable() && !desc.IsWritable()) {
                return false;
            }
            if (desc.HasValue()) {
                JSHandle<JSTaggedValue> value = desc.GetValue();
                // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue
                // (issue #I5QOJX)
                // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
                return (JSTypedArray::IntegerIndexedElementSet(thread, typedarray, numeric_index, value));
            }
            return true;
        }
    }
    // 4. Return OrdinaryDefineOwnProperty(O, P, Desc).
    return JSObject::OrdinaryDefineOwnProperty(thread, typedarray_obj, key, desc);
}

// 9.4.5.4 [[Get]] ( P, Receiver )
OperationResult JSTypedArray::GetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                          const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &receiver)
{
    // 1. Assert : IsPropertyKey(P) is true.
    ASSERT(JSTaggedValue::IsPropertyKey(key));
    // 2. If Type(P) is String and if SameValue(O, Receiver) is true, then
    if (key->IsString() && JSTaggedValue::SameValue(typedarray, receiver)) {
        //   a. Let numericIndex be CanonicalNumericIndexString (P).
        //   b. Assert: numericIndex is not an abrupt completion.
        //   c. If numericIndex is not undefined, then
        //     i. Return IntegerIndexedElementGet (O, numericIndex).
        JSTaggedValue numeric_index = JSTaggedValue::CanonicalNumericIndexString(thread, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread,
                                          OperationResult(thread, JSTaggedValue::Exception(), PropertyMetaData(false)));
        if (!numeric_index.IsUndefined()) {
            return JSTypedArray::IntegerIndexedElementGet(thread, typedarray, numeric_index);
        }
    }

    // 3. Return the result of calling the default ordinary object [[Get]] internal method (9.1.8) on O
    //   passing P and Receiver as arguments.
    return JSObject::GetProperty(thread, typedarray, key, receiver);
}

// 9.4.5.5 [[Set]] ( P, V, Receiver )
bool JSTypedArray::SetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                               const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value,
                               const JSHandle<JSTaggedValue> &receiver, bool may_throw)
{
    // 1. Assert : IsPropertyKey(P) is true.
    ASSERT(JSTaggedValue::IsPropertyKey(key));
    // 2. If Type(P) is String and if SameValue(O, Receiver) is true, then
    if (key->IsString() && JSTaggedValue::SameValue(typedarray, receiver)) {
        //   a. Let numericIndex be CanonicalNumericIndexString (P).
        //   b. Assert: numericIndex is not an abrupt completion.
        //   c. If numericIndex is not undefined, then
        //     i. Return IntegerIndexedElementSet (O, numericIndex, V).
        JSTaggedValue numeric_index = JSTaggedValue::CanonicalNumericIndexString(thread, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
        if (!numeric_index.IsUndefined()) {
            return JSTypedArray::IntegerIndexedElementSet(thread, typedarray, numeric_index, value);
        }
    }
    // 3. Return the result of calling the default ordinary object [[Set]] internal method (9.1.8) on O passing
    // P, V, and Receiver as arguments.
    return JSObject::SetProperty(thread, typedarray, key, value, receiver, may_throw);
}

// 9.4.5.6 [[OwnPropertyKeys]] ( )
JSHandle<TaggedArray> JSTypedArray::OwnPropertyKeys(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let keys be a new empty List.
    // 2. Assert: O is an Object that has [[ViewedArrayBuffer]], [[ArrayLength]], [[ByteOffset]], and
    // [[TypedArrayName]] internal slots.
    // 3. Let len be the value of O’s [[ArrayLength]] internal slot.
    JSHandle<JSObject> array_obj(typedarray);
    JSHandle<TaggedArray> obj_keys = JSObject::GetOwnPropertyKeys(thread, array_obj);
    uint32_t obj_keys_len = obj_keys->GetLength();
    uint32_t buffer_keys_len = TypedArrayHelper::GetArrayLength(thread, array_obj);
    uint32_t length = obj_keys_len + buffer_keys_len;
    JSHandle<TaggedArray> name_list = factory->NewTaggedArray(length);

    // 4. For each integer i starting with 0 such that i < len, in ascending order,
    //   a. Add ToString(i) as the last element of keys.
    uint32_t copy_length = 0;
    JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
    for (uint32_t k = 0; k < buffer_keys_len; k++) {
        t_key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> s_key(JSTaggedValue::ToString(thread, t_key));
        name_list->Set(thread, copy_length, s_key.GetTaggedValue());
        copy_length++;
    }

    // 5. For each own property key P of O such that Type(P) is String and P is not an integer index, in
    // property creation order
    //   a. Add P as the last element of keys.
    for (uint32_t i = 0; i < obj_keys_len; i++) {
        JSTaggedValue key = obj_keys->Get(i);
        if (JSTaggedValue(key).IsString()) {
            name_list->Set(thread, copy_length, key);
            copy_length++;
        }
    }

    // 6. For each own property key P of O such that Type(P) is Symbol, in property creation order
    //   a. Add P as the last element of keys.
    for (uint32_t i = 0; i < obj_keys_len; i++) {
        JSTaggedValue key = obj_keys->Get(i);
        if (JSTaggedValue(key).IsSymbol()) {
            name_list->Set(thread, copy_length, key);
            copy_length++;
        }
    }

    // 7. Return keys.
    return factory->CopyArray(name_list, length, copy_length);
}

// 9.4.5.7 IntegerIndexedObjectCreate (prototype, internalSlotsList)

// 9.4.5.8 IntegerIndexedElementGet ( O, index )
OperationResult JSTypedArray::IntegerIndexedElementGet(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                                       JSTaggedValue index)
{
    // 1. Assert: Type(index) is Number.
    ASSERT(index.IsNumber());
    // 2. Assert: O is an Object that has [[ViewedArrayBuffer]], [[ArrayLength]], [[ByteOffset]], and
    // [[TypedArrayName]] internal slots.
    ASSERT(typedarray->IsTypedArray());
    // 3. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSHandle<JSObject> typedarray_obj(typedarray);
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSTaggedValue buffer = JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer();
        // 4. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
        if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer",
                                        OperationResult(thread, JSTaggedValue::Exception(), PropertyMetaData(false)));
        }
    }
    // 5. If IsInteger(index) is false, return undefined
    if (!index.IsInteger()) {
        return OperationResult(thread, JSTaggedValue::Undefined(), PropertyMetaData(true));
    }

    // 6. If index = −0, return undefined.
    // 7. Let length be the value of O’s [[ArrayLength]] internal slot.
    // 8. If index < 0 or index ≥ length, return undefined.
    JSHandle<JSTaggedValue> index_handle(thread, index);
    JSTaggedNumber index_number = JSTaggedValue::ToNumber(thread, index_handle);
    double t_neg_zero = -0.0;
    auto e_zero = JSTaggedNumber(t_neg_zero);
    if (JSTaggedNumber::SameValue(index_number, e_zero)) {
        return OperationResult(thread, JSTaggedValue::Undefined(), PropertyMetaData(true));
    }
    int32_t arr_len = TypedArrayHelper::GetArrayLength(thread, typedarray_obj);
    JSHandle<JSTaggedValue> arr_len_handle(thread, JSTaggedValue(arr_len));
    if (JSTaggedValue::Less(thread, index_handle, thread->GlobalConstants()->GetHandledZero()) ||
        !JSTaggedValue::Less(thread, index_handle, arr_len_handle)) {
        return OperationResult(thread, JSTaggedValue::Undefined(), PropertyMetaData(true));
    }
    // 9. Let offset be the value of O’s [[ByteOffset]] internal slot.
    int32_t offset = TypedArrayHelper::GetByteOffset(thread, typedarray_obj);
    // 10. Let arrayTypeName be the String value of O’s [[TypedArrayName]] internal slot.
    // 11. Let elementSize be the Number value of the Element Size value specified in Table 49 for
    // arrayTypeName.
    int32_t element_size = TypedArrayHelper::GetElementSize(typedarray_obj);
    // 12. Let indexedPosition = (index × elementSize) + offset.
    int32_t k = JSTaggedValue::ToInteger(thread, index_handle).ToInt32();
    int32_t byte_index = k * element_size + offset;
    // 13. Let elementType be the String value of the Element Type value in Table 49 for arrayTypeName.
    DataViewType element_type = TypedArrayHelper::GetType(typedarray_obj);
    // 14. Return GetValueFromBuffer(buffer, indexedPosition, elementType).
    JSTaggedValue result;
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSHandle<JSTaggedValue> buffer(thread, JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer());
        result = builtins::array_buffer::GetValueFromBuffer(thread, buffer, byte_index, element_type, true);
    }
    return OperationResult(thread, result, PropertyMetaData(true));
}

// static
bool JSTypedArray::FastCopyElementToArray(JSThread *thread, const JSHandle<JSTaggedValue> &typed_array,
                                          JSHandle<TaggedArray> &array)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 2. Assert: O is an Object that has [[ViewedArrayBuffer]], [[ArrayLength]], [[ByteOffset]], and
    // [[TypedArrayName]] internal slots.
    ASSERT(typed_array->IsTypedArray());
    // 3. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSHandle<JSObject> typedarray_obj(typed_array);
    JSHandle<JSTaggedValue> buffer(thread, JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer());
    // 4. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    if (builtins::array_buffer::IsDetachedBuffer(buffer.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", false);
    }

    // 7. Let length be the value of O’s [[ArrayLength]] internal slot.
    // 8. If index < 0 or index ≥ length, return undefined.
    int32_t arr_len = TypedArrayHelper::GetArrayLength(thread, typedarray_obj);
    if (arr_len < 0) {
        return false;
    }

    // 9. Let offset be the value of O’s [[ByteOffset]] internal slot.
    int32_t offset = TypedArrayHelper::GetByteOffset(thread, typedarray_obj);
    // 11. Let elementSize be the Number value of the Element Size value specified in Table 49 for arrayTypeName.
    int32_t element_size = TypedArrayHelper::GetElementSize(typedarray_obj);
    // 13. Let elementType be the String value of the Element Type value in Table 49 for arrayTypeName.
    DataViewType element_type = TypedArrayHelper::GetType(typedarray_obj);
    for (int index = 0; index < arr_len; index++) {
        // 12. Let indexedPosition = (index × elementSize) + offset.
        int32_t byte_index = index * element_size + offset;
        // 14. Return GetValueFromBuffer(buffer, indexedPosition, elementType).
        JSTaggedValue result =
            builtins::array_buffer::GetValueFromBuffer(thread, buffer, byte_index, element_type, true);
        array->Set(thread, index, result);
    }
    return true;
}

// static
OperationResult JSTypedArray::FastElementGet(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                             uint32_t index)
{
    // 2. Assert: O is an Object that has [[ViewedArrayBuffer]], [[ArrayLength]], [[ByteOffset]], and
    // [[TypedArrayName]] internal slots.
    ASSERT(typedarray->IsTypedArray());
    // 3. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSHandle<JSObject> typedarray_obj(typedarray);
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSTaggedValue buffer = JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer();
        // 4. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
        if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer",
                                        OperationResult(thread, JSTaggedValue::Exception(), PropertyMetaData(false)));
        }
    }

    // 7. Let length be the value of O’s [[ArrayLength]] internal slot.
    // 8. If index < 0 or index ≥ length, return undefined.
    int32_t arr_len = TypedArrayHelper::GetArrayLength(thread, typedarray_obj);
    if (arr_len < 0 || index >= static_cast<uint32_t>(arr_len)) {
        return OperationResult(thread, JSTaggedValue::Undefined(), PropertyMetaData(true));
    }
    // 9. Let offset be the value of O’s [[ByteOffset]] internal slot.
    int32_t offset = TypedArrayHelper::GetByteOffset(thread, typedarray_obj);
    // 11. Let elementSize be the Number value of the Element Size value specified in Table 49 for arrayTypeName.
    int32_t element_size = TypedArrayHelper::GetElementSize(typedarray_obj);
    // 12. Let indexedPosition = (index × elementSize) + offset.
    int32_t byte_index = index * element_size + offset;
    // 13. Let elementType be the String value of the Element Type value in Table 49 for arrayTypeName.
    DataViewType element_type = TypedArrayHelper::GetType(typedarray_obj);
    // 14. Return GetValueFromBuffer(buffer, indexedPosition, elementType).
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSHandle<JSTaggedValue> buffer(thread, JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer());
        JSTaggedValue result =
            builtins::array_buffer::GetValueFromBuffer(thread, buffer, byte_index, element_type, true);
        return OperationResult(thread, result, PropertyMetaData(true));
    }
}

// 9.4.5.9 IntegerIndexedElementSet ( O, index, value )
bool JSTypedArray::IntegerIndexedElementSet(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                            JSTaggedValue index, const JSHandle<JSTaggedValue> &value)
{
    JSHandle<JSTaggedValue> index_handle(thread, index);
    // 1. Assert: Type(index) is Number.
    ASSERT(index.IsNumber());
    // 2. Assert: O is an Object that has [[ViewedArrayBuffer]], [[ArrayLength]], [[ByteOffset]], and
    // [[TypedArrayName]] internal slots.
    ASSERT(typedarray->IsTypedArray());
    // 3. If O.[[ContentType]] is BigInt, let numValue be ? ToBigInt(value).
    JSHandle<JSTaggedValue> num_value_handle;
    ContentType content_type = JSHandle<JSTypedArray>::Cast(typedarray)->GetContentType();
    if (content_type == ContentType::BIG_INT) {
        num_value_handle = JSHandle<JSTaggedValue>(thread, JSTaggedValue::ToBigInt(thread, value));
    } else {
        num_value_handle = JSHandle<JSTaggedValue>(thread, JSTaggedValue::ToNumber(thread, value));
    }
    // 4. ReturnIfAbrupt(numValue).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);

    // 5. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSHandle<JSObject> typedarray_obj(typedarray);
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSTaggedValue buffer = JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer();
        // 6. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
        if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", false);
        }
    }
    // 7. If IsInteger(index) is false, return false
    if (!index.IsInteger()) {
        return false;
    }

    // 8. If index = −0, return false.
    // 9. Let length be the value of O’s [[ArrayLength]] internal slot.
    // 10. If index < 0 or index ≥ length, return false.
    JSTaggedNumber index_number = JSTaggedValue::ToNumber(thread, index_handle);
    double t_neg_zero = -0.0;
    auto e_zero = JSTaggedNumber(t_neg_zero);
    if (JSTaggedNumber::SameValue(index_number, e_zero)) {
        return false;
    }
    int32_t arr_len = TypedArrayHelper::GetArrayLength(thread, typedarray_obj);
    JSHandle<JSTaggedValue> arr_len_handle(thread, JSTaggedValue(arr_len));
    if (JSTaggedValue::Less(thread, index_handle, thread->GlobalConstants()->GetHandledZero()) ||
        !JSTaggedValue::Less(thread, index_handle, arr_len_handle)) {
        return false;
    }

    // 11. Let offset be the value of O’s [[ByteOffset]] internal slot.
    int32_t offset = TypedArrayHelper::GetByteOffset(thread, typedarray_obj);
    // 12. Let arrayTypeName be the String value of O’s [[TypedArrayName]] internal slot.
    // 13. Let elementSize be the Number value of the Element Size value specified in Table 49 for
    // arrayTypeName.
    int32_t element_size = TypedArrayHelper::GetElementSize(typedarray_obj);
    // 14. Let indexedPosition = (index × elementSize) + offset.
    int32_t k = JSTaggedValue::ToInteger(thread, index_handle).ToInt32();
    int32_t byte_index = k * element_size + offset;
    // 15. Let elementType be the String value of the Element Type value in Table 49 for arrayTypeName.
    DataViewType element_type = TypedArrayHelper::GetType(typedarray_obj);
    // 16. Perform SetValueInBuffer(buffer, indexedPosition, elementType, numValue).
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSHandle<JSTaggedValue> buffer(thread, JSTypedArray::Cast(*typedarray_obj)->GetViewedArrayBuffer());
        builtins::array_buffer::SetValueInBuffer(thread, buffer, byte_index, element_type, num_value_handle, true);
    }
    // 17. Return true.
    return true;
}
}  // namespace panda::ecmascript
