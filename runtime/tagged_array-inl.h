/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_TAGGED_ARRAY_INL_H
#define ECMASCRIPT_TAGGED_ARRAY_INL_H

#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

namespace panda::ecmascript {
inline JSTaggedValue TaggedArray::Get(uint32_t idx) const
{
    if (UNLIKELY(idx >= GetLength())) {
        return JSTaggedValue::Hole();
    }
    // Note: Here we can't statically decide the element type is a primitive or heap object, especially for
    //       dynamically-typed languages like JavaScript. So we simply skip the read-barrier.
    size_t offset = JSTaggedValue::TaggedTypeSize() * idx;
    // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
    return JSTaggedValue(Barriers::GetDynValue<JSTaggedType>(this, DATA_OFFSET + offset));
}

inline JSTaggedValue TaggedArray::Get([[maybe_unused]] const JSThread *thread, uint32_t idx) const
{
    return Get(idx);
}

inline uint32_t TaggedArray::GetIdx(const JSTaggedValue &value) const
{
    uint32_t length = GetLength();

    for (uint32_t i = 0; i < length; i++) {
        if (JSTaggedValue::SameValue(Get(i), value)) {
            return i;
        }
    }
    return TaggedArray::MAX_ARRAY_INDEX;
}

template <typename T>
inline void TaggedArray::Set(const JSThread *thread, uint32_t idx, const JSHandle<T> &value)
{
    ASSERT(idx < GetLength());
    size_t offset = JSTaggedValue::TaggedTypeSize() * idx;

    ObjectAccessor::SetDynValue(thread, this, DATA_OFFSET + offset, value.GetTaggedValue().GetRawData());
}

inline void TaggedArray::Set(const JSThread *thread, uint32_t idx, const JSTaggedValue &value)
{
    ASSERT(idx < GetLength());
    size_t offset = JSTaggedValue::TaggedTypeSize() * idx;

    ObjectAccessor::SetDynValue(thread, this, DATA_OFFSET + offset, value.GetRawData());
}

JSHandle<TaggedArray> TaggedArray::Append(const JSThread *thread, const JSHandle<TaggedArray> &first,
                                          const JSHandle<TaggedArray> &second)
{
    uint32_t first_length = first->GetLength();
    uint32_t second_length = second->GetLength();
    uint32_t length = first_length + second_length;
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> argument = factory->NewTaggedArray(length);
    uint32_t index = 0;
    for (; index < first_length; ++index) {
        argument->Set(thread, index, first->Get(index));
    }
    for (; index < length; ++index) {
        argument->Set(thread, index, second->Get(index - first_length));
    }
    return argument;
}

JSHandle<TaggedArray> TaggedArray::AppendSkipHole(const JSThread *thread, const JSHandle<TaggedArray> &first,
                                                  const JSHandle<TaggedArray> &second, uint32_t copy_length)
{
    uint32_t first_length = first->GetLength();
    uint32_t second_length = second->GetLength();
    ASSERT(first_length + second_length >= copy_length);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> argument = factory->NewTaggedArray(copy_length);
    uint32_t index = 0;
    for (; index < first_length; ++index) {
        JSTaggedValue val = first->Get(index);
        if (val.IsHole()) {
            break;
        }
        argument->Set(thread, index, val);
        ASSERT(copy_length >= index);
    }
    for (uint32_t i = 0; i < second_length; ++i) {
        JSTaggedValue val = second->Get(i);
        if (val.IsHole()) {
            break;
        }
        argument->Set(thread, index++, val);
        ASSERT(copy_length >= index);
    }
    return argument;
}

inline bool TaggedArray::HasDuplicateEntry() const
{
    uint32_t length = GetLength();
    for (uint32_t i = 0; i < length; i++) {
        for (uint32_t j = i + 1; j < length; j++) {
            if (JSTaggedValue::SameValue(Get(i), Get(j))) {
                return true;
            }
        }
    }
    return false;
}

void TaggedArray::InitializeWithSpecialValue(JSTaggedValue init_value, uint32_t length)
{
    ASSERT(init_value.IsSpecial());
    SetLength(length);
    for (uint32_t i = 0; i < length; i++) {
        size_t offset = JSTaggedValue::TaggedTypeSize() * i;
        // Without barrier because 'this' is just allocated object and it doesn't contains references.
        ObjectAccessor::SetDynValueWithoutBarrier(this, DATA_OFFSET + offset, init_value.GetRawData());
    }
}

inline JSHandle<TaggedArray> TaggedArray::SetCapacity(const JSThread *thread, const JSHandle<TaggedArray> &array,
                                                      uint32_t capa)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    uint32_t old_length = array->GetLength();
    JSHandle<TaggedArray> new_array = factory->CopyArray(array, old_length, capa);
    return new_array;
}

inline bool TaggedArray::IsDictionaryMode() const
{
    return GetClass()->IsDictionary();
}

void TaggedArray::Trim(JSThread *thread, uint32_t new_length)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    uint32_t old_length = GetLength();
    ASSERT(old_length > new_length);
    size_t trim_bytes = (old_length - new_length) * JSTaggedValue::TaggedTypeSize();
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), new_length);
    factory->FillFreeObject(ToUintPtr(this) + size, trim_bytes, RemoveSlots::YES);
    SetLength(new_length);
}
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_TAGGED_ARRAY_INL_H
