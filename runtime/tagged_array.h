/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_TAGGED_ARRAY_H
#define ECMASCRIPT_TAGGED_ARRAY_H

#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"

namespace panda::ecmascript {
class ObjectFactory;

class TaggedArray : public TaggedObject {
public:
    static constexpr uint32_t MAX_ARRAY_INDEX = std::numeric_limits<uint32_t>::max();
    static constexpr uint32_t MAX_END_UNUSED = 4;

    inline static TaggedArray *Cast(ObjectHeader *obj)
    {
        ASSERT(JSTaggedValue(obj).IsTaggedArray());
        return static_cast<TaggedArray *>(obj);
    }

    JSTaggedValue Get(uint32_t idx) const;

    uint32_t GetIdx(const JSTaggedValue &value) const;

    template <typename T>
    void Set(const JSThread *thread, uint32_t idx, const JSHandle<T> &value);

    JSTaggedValue Get(const JSThread *thread, uint32_t idx) const;

    void Set(const JSThread *thread, uint32_t idx, const JSTaggedValue &value);

    static inline JSHandle<TaggedArray> Append(const JSThread *thread, const JSHandle<TaggedArray> &first,
                                               const JSHandle<TaggedArray> &second);
    static inline JSHandle<TaggedArray> AppendSkipHole(const JSThread *thread, const JSHandle<TaggedArray> &first,
                                                       const JSHandle<TaggedArray> &second, uint32_t copy_length);

    static inline size_t ComputeSize(size_t elem_size, uint32_t length)
    {
        ASSERT(elem_size != 0);
        size_t size = DATA_OFFSET + elem_size * length;
#ifdef PANDA_TARGET_32
        size_t size_limit = (std::numeric_limits<size_t>::max() - DATA_OFFSET) / elem_size;
        if (UNLIKELY(size_limit < static_cast<size_t>(length))) {
            return 0;
        }
#endif
        return size;
    }

    inline JSTaggedType *GetData() const
    {
        return reinterpret_cast<JSTaggedType *>(ToUintPtr(this) + DATA_OFFSET);
    }

    inline bool IsDictionaryMode() const;

    bool HasDuplicateEntry() const;

    static JSHandle<TaggedArray> SetCapacity(const JSThread *thread, const JSHandle<TaggedArray> &array, uint32_t capa);

    inline void InitializeWithSpecialValue(JSTaggedValue init_value, uint32_t length);

    static inline bool ShouldTrim([[maybe_unused]] JSThread *thread, uint32_t old_length, uint32_t new_length)
    {
        return (old_length - new_length > MAX_END_UNUSED);
    }
    inline void Trim(JSThread *thread, uint32_t new_length);

    static inline constexpr size_t GetElementOffset(uint32_t idx)
    {
        return DATA_OFFSET + idx * JSTaggedValue::TaggedTypeSize();
    }

    ACCESSORS_START(TaggedObjectSize())
    ACCESSORS_PRIMITIVE_FIELD(0, Length, uint32_t);
    ACCESSORS_FINISH(1)
    static constexpr size_t DATA_OFFSET = SIZE;  // Empty Array size

    DECL_VISIT_ARRAY(DATA_OFFSET, GetLength());

private:
    friend class ObjectFactory;
};

static_assert(TaggedArray::GetLengthOffset() == coretypes::Array::GetLengthOffset());
static_assert(TaggedArray::GetElementOffset(0) == coretypes::Array::GetDataOffset());
static_assert((TaggedArray::GetElementOffset(0) % GetAlignmentInBytes(TAGGED_OBJECT_ALIGNMENT)) == 0);
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_TAGGED_ARRAY_H
