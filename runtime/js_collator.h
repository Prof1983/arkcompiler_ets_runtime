/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_COLLATOR_H
#define ECMASCRIPT_JS_COLLATOR_H

#include "js_locale.h"

#include "unicode/udata.h"

namespace panda::ecmascript {
enum class UsageOption : uint8_t { SORT = 0x01, SEARCH, EXCEPTION };
enum class CaseFirstOption : uint8_t { UPPER = 0x01, LOWER, FALSE_OPTION, UNDEFINED, EXCEPTION };
enum class SensitivityOption : uint8_t { BASE = 0x01, ACCENT, CASE, VARIANT, UNDEFINED, EXCEPTION };

class JSCollator : public JSObject {
public:
    // NOLINTNEXTLINE (readability-identifier-naming, fuchsia-statically-constructed-objects)
    static const std::string uIcuDataColl;

    static const std::map<std::string, CaseFirstOption> CASE_FIRST_MAP;

    static const std::map<CaseFirstOption, UColAttributeValue> U_COL_ATTRIBUTE_VALUE_MAP;

    CAST_CHECK(JSCollator, IsJSCollator);

    // icu field.
    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, IcuField)
    ACCESSORS(1, Locale)
    ACCESSORS(2, Usage)
    ACCESSORS(3, Sensitivity)
    ACCESSORS(4, IgnorePunctuation)
    ACCESSORS(5, Collation)
    ACCESSORS(6, Numeric)
    ACCESSORS(7, CaseFirst)
    ACCESSORS(8, BoundCompare)
    ACCESSORS_FINISH(9)

    DECL_DUMP()

    icu::Collator *GetIcuCollator() const
    {
        ASSERT(GetIcuField().IsJSNativePointer());
        auto result = JSNativePointer::Cast(GetIcuField().GetTaggedObject())->GetExternalPointer();
        return reinterpret_cast<icu::Collator *>(result);
    }

    static void FreeIcuCollator(void *pointer, [[maybe_unused]] void *hint = nullptr)
    {
        if (pointer == nullptr) {
            return;
        }
        auto icu_collator = reinterpret_cast<icu::Collator *>(pointer);
        delete icu_collator;
    }

    static void SetIcuCollator(JSThread *thread, const JSHandle<JSCollator> &collator, icu::Collator *icu_collator,
                               const DeleteEntryPoint &callback);

    // 11.1.1 InitializeCollator ( collator, locales, options )
    static JSHandle<JSCollator> InitializeCollator(JSThread *thread, const JSHandle<JSCollator> &collator,
                                                   const JSHandle<JSTaggedValue> &locales,
                                                   const JSHandle<JSTaggedValue> &options);

    // 11.3.4 Intl.Collator.prototype.resolvedOptions ()
    static JSHandle<JSObject> ResolvedOptions(JSThread *thread, const JSHandle<JSCollator> &collator);

    static JSHandle<TaggedArray> GetAvailableLocales(JSThread *thread);

    static JSTaggedValue CompareStrings(const icu::Collator *icu_collator, const JSHandle<EcmaString> &string1,
                                        const JSHandle<EcmaString> &string2);

private:
    static CaseFirstOption StringToCaseForstOption(const std::string &str);

    static UColAttributeValue OptionToUColAttribute(CaseFirstOption case_first_option);

    static std::set<std::string> BuildLocaleSet(const std::vector<std::string> &available_locales, const char *path,
                                                const char *key);

    static void SetNumericOption(icu::Collator *icu_collator, bool numeric);

    static void SetCaseFirstOption(icu::Collator *icu_collator, CaseFirstOption case_first_option);
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_JS_COLLATOR_H