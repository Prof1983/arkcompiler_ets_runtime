/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_arraylist.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_proxy.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "js_object-inl.h"
#include "object_factory.h"

namespace panda::ecmascript {
JSHandle<EcmaString> GetTypeString(JSThread *thread, PreferredPrimitiveType type)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (type == NO_PREFERENCE) {
        return factory->NewFromCanBeCompressString("default");
    }
    if (type == PREFER_NUMBER) {
        return factory->NewFromCanBeCompressString("number");
    }
    return factory->NewFromCanBeCompressString("string");
}

JSHandle<JSTaggedValue> JSTaggedValue::ToPropertyKey(JSThread *thread, const JSHandle<JSTaggedValue> &tagged)
{
    if (tagged->IsStringOrSymbol() || tagged->IsNumber()) {
        return tagged;
    }
    JSHandle<JSTaggedValue> key(thread, ToPrimitive(thread, tagged, PREFER_STRING));
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);
    if (key->IsSymbol()) {
        return key;
    }
    JSHandle<EcmaString> string = ToString(thread, key);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);
    return JSHandle<JSTaggedValue>::Cast(string);
}

bool JSTaggedValue::IsInteger() const
{
    if (!IsNumber()) {
        return false;
    }

    if (IsInt()) {
        return true;
    }

    double this_value = GetDouble();
    // If argument is NaN, +∞, or -∞, return false.
    if (!std::isfinite(this_value)) {
        return false;
    }

    // If floor(abs(argument)) ≠ abs(argument), return false.
    if (std::floor(std::abs(this_value)) != std::abs(this_value)) {
        return false;
    }

    return true;
}

bool JSTaggedValue::WithinInt32() const
{
    if (!IsNumber()) {
        return false;
    }

    double double_value = GetNumber();
    if (bit_cast<int64_t>(double_value) == bit_cast<int64_t>(-0.0)) {
        return false;
    }

    int32_t intvalue = base::NumberHelper::DoubleToInt(double_value, base::INT32_BITS);
    return double_value == static_cast<double>(intvalue);
}

bool JSTaggedValue::IsZero() const
{
    if (GetRawData() == VALUE_ZERO) {
        return true;
    }
    if (IsDouble()) {
        const double limit = 1e-8;
        return (std::abs(GetDouble() - 0.0) <= limit);
    }
    return false;
}

bool JSTaggedValue::Equal(JSThread *thread, const JSHandle<JSTaggedValue> &x, const JSHandle<JSTaggedValue> &y)
{
    if (x->IsNumber()) {
        if (y->IsNumber()) {
            return StrictNumberEquals(x->ExtractNumber(), y->ExtractNumber());
        }
        if (y->IsString()) {
            JSTaggedNumber y_number = ToNumber(thread, y);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return StrictNumberEquals(x->ExtractNumber(), y_number.GetNumber());
        }
        if (y->IsBigInt()) {
            return Equal(thread, y, x);
        }
        if (y->IsBoolean()) {
            JSTaggedNumber y_number = ToNumber(thread, y);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return StrictNumberEquals(x->ExtractNumber(), y_number.GetNumber());
        }
        if (y->IsHeapObject() && !y->IsSymbol()) {
            JSHandle<JSTaggedValue> y_primitive(thread, ToPrimitive(thread, y));
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return Equal(thread, x, y_primitive);
        }
        return false;
    }

    if (x->IsString()) {
        if (y->IsString()) {
            return EcmaString::StringsAreEqual(static_cast<EcmaString *>(x->GetTaggedObject()),
                                               static_cast<EcmaString *>(y->GetTaggedObject()));
        }
        if (y->IsNumber()) {
            JSTaggedNumber x_number = ToNumber(thread, x);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return StrictNumberEquals(x_number.GetNumber(), y->ExtractNumber());
        }
        if (y->IsBigInt()) {
            return Equal(thread, y, x);
        }
        if (y->IsBoolean()) {
            JSTaggedNumber x_number = ToNumber(thread, x);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            JSTaggedNumber y_number = ToNumber(thread, y);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return StrictNumberEquals(x_number.GetNumber(), y_number.GetNumber());
        }
        if (y->IsHeapObject() && !y->IsSymbol()) {
            JSHandle<JSTaggedValue> y_primitive(thread, ToPrimitive(thread, y));
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return Equal(thread, x, y_primitive);
        }
        return false;
    }

    if (x->IsBoolean()) {
        JSTaggedNumber x_number = ToNumber(thread, x);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
        return Equal(thread, JSHandle<JSTaggedValue>(thread, x_number), y);
    }

    if (x->IsSymbol()) {
        if (y->IsSymbol()) {
            return x.GetTaggedValue() == y.GetTaggedValue();
        }
        if (y->IsBigInt()) {
            return Equal(thread, y, x);
        }
        if (y->IsHeapObject() && y->IsECMAObject()) {
            JSHandle<JSTaggedValue> y_primitive(thread, ToPrimitive(thread, y));
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return Equal(thread, x, y_primitive);
        }
        return false;
    }

    if (x->IsBigInt()) {
        if (y->IsBigInt()) {
            return BigInt::Equal(thread, x.GetTaggedValue(), y.GetTaggedValue());
        }
        if (y->IsString()) {
            JSHandle<JSTaggedValue> y_number(thread, base::NumberHelper::StringToBigInt(thread, y));
            if (!y_number->IsBigInt()) {
                return false;
            }
            return BigInt::Equal(thread, x.GetTaggedValue(), y_number.GetTaggedValue());
        }
        if (y->IsBoolean()) {
            JSHandle<JSTaggedValue> y_number(thread, ToBigInt(thread, y));
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return BigInt::Equal(thread, x.GetTaggedValue(), y_number.GetTaggedValue());
        }
        if (y->IsNumber()) {
            JSHandle<BigInt> bigint = JSHandle<BigInt>::Cast(x);
            return BigInt::CompareWithNumber(bigint, y) == ComparisonResult::EQUAL;
        }
        if (y->IsHeapObject() && !y->IsSymbol()) {
            JSHandle<JSTaggedValue> y_primitive(thread, ToPrimitive(thread, y));
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return Equal(thread, x, y_primitive);
        }
        return false;
    }

    if (x->IsHeapObject()) {
        if (y->IsHeapObject()) {
            // if same type, must call Type::StrictEqual()
            JSType x_type = x.GetTaggedValue().GetTaggedObject()->GetClass()->GetObjectType();
            JSType y_type = y.GetTaggedValue().GetTaggedObject()->GetClass()->GetObjectType();
            if (x_type == y_type) {
                return StrictEqual(thread, x, y);
            }
        }
        if (y->IsNumber() || y->IsStringOrSymbol() || y->IsBoolean() || y->IsBigInt()) {
            JSHandle<JSTaggedValue> x_primitive(thread, ToPrimitive(thread, x));
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
            return Equal(thread, x_primitive, y);
        }
        return false;
    }

    if (x->IsNull() && y->IsNull()) {
        return true;
    }

    if (x->IsUndefined() && y->IsUndefined()) {
        return true;
    }

    if (x->IsNull() && y->IsUndefined()) {
        return true;
    }

    if (x->IsUndefined() && y->IsNull()) {
        return true;
    }

    return false;
}

ComparisonResult JSTaggedValue::Compare(JSThread *thread, const JSHandle<JSTaggedValue> &x,
                                        const JSHandle<JSTaggedValue> &y)
{
    JSHandle<JSTaggedValue> prim_x(thread, ToPrimitive(thread, x));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, ComparisonResult::UNDEFINED);
    JSHandle<JSTaggedValue> prim_y(thread, ToPrimitive(thread, y));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, ComparisonResult::UNDEFINED);

    if (prim_x->IsString() && prim_y->IsString()) {
        auto x_string = static_cast<EcmaString *>(prim_x->GetTaggedObject());
        auto y_string = static_cast<EcmaString *>(prim_y->GetTaggedObject());
        int result = x_string->Compare(y_string);
        if (result < 0) {
            return ComparisonResult::LESS;
        }
        if (result == 0) {
            return ComparisonResult::EQUAL;
        }
        return ComparisonResult::GREAT;
    }

    if (prim_x->IsBigInt()) {
        if (prim_y->IsNumber()) {
            JSHandle<BigInt> bigint = JSHandle<BigInt>::Cast(prim_x);
            return BigInt::CompareWithNumber(bigint, prim_y);
        }
        if (prim_y->IsString()) {
            JSHandle<JSTaggedValue> big_y(thread, base::NumberHelper::StringToBigInt(thread, prim_y));
            if (!big_y->IsBigInt()) {
                return ComparisonResult::UNDEFINED;
            }
            return BigInt::Compare(thread, prim_x.GetTaggedValue(), big_y.GetTaggedValue());
        }
        JSHandle<JSTaggedValue> big_y(thread, ToBigInt(thread, prim_y));
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, ComparisonResult::UNDEFINED);
        return BigInt::Compare(thread, prim_x.GetTaggedValue(), big_y.GetTaggedValue());
    }

    if (prim_y->IsBigInt()) {
        ComparisonResult res = Compare(thread, prim_y, prim_x);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, ComparisonResult::UNDEFINED);
        if (res == ComparisonResult::GREAT) {
            return ComparisonResult::LESS;
        }
        if (res == ComparisonResult::LESS) {
            return ComparisonResult::GREAT;
        }
        return res;
    }

    JSTaggedNumber x_number = ToNumber(thread, x);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, ComparisonResult::UNDEFINED);
    JSTaggedNumber y_number = ToNumber(thread, y);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, ComparisonResult::UNDEFINED);
    return StrictNumberCompare(x_number.GetNumber(), y_number.GetNumber());
}

bool JSTaggedValue::IsSameTypeOrHClass(JSTaggedValue x, JSTaggedValue y)
{
    if (x.IsNumber() && y.IsNumber()) {
        return true;
    }
    if (x.IsBoolean() && y.IsBoolean()) {
        return true;
    }
    if (x.IsString() && y.IsString()) {
        return true;
    }
    if (x.IsHeapObject() && y.IsHeapObject()) {
        return x.GetTaggedObject()->GetClass() == y.GetTaggedObject()->GetClass();
    }

    return false;
}

JSTaggedValue JSTaggedValue::ToPrimitive(JSThread *thread, const JSHandle<JSTaggedValue> &tagged,
                                         PreferredPrimitiveType type)
{
    if (tagged->IsECMAObject()) {
        EcmaVM *vm = thread->GetEcmaVM();
        JSHandle<JSTaggedValue> key_string = vm->GetGlobalEnv()->GetToPrimitiveSymbol();

        JSHandle<JSTaggedValue> exotic_toprim = JSObject::GetMethod(thread, tagged, key_string);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        if (!exotic_toprim->IsUndefined()) {
            JSTaggedValue value = GetTypeString(thread, type).GetTaggedValue();

            auto info = NewRuntimeCallInfo(thread, exotic_toprim, tagged, JSTaggedValue::Undefined(), 1);
            info->SetCallArgs(value);
            JSTaggedValue value_result = JSFunction::Call(info.Get());
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
            if (!value_result.IsECMAObject()) {
                return value_result;
            }
            THROW_TYPE_ERROR_AND_RETURN(thread, "", JSTaggedValue::Exception());
        } else {
            type = (type == NO_PREFERENCE) ? PREFER_NUMBER : type;
            return OrdinaryToPrimitive(thread, tagged, type);
        }
    }
    return tagged.GetTaggedValue();
}

JSTaggedValue JSTaggedValue::OrdinaryToPrimitive(JSThread *thread, const JSHandle<JSTaggedValue> &tagged,
                                                 PreferredPrimitiveType type)
{
    static_assert(PREFER_NUMBER == 0 && PREFER_STRING == 1);
    ASSERT(tagged->IsECMAObject());
    auto global_const = thread->GlobalConstants();
    for (uint8_t i = 0; i < 2; i++) {  // 2: 2 means value has 2 target types, string or value.
        JSHandle<JSTaggedValue> key_string;
        if ((type ^ i) != 0) {
            key_string = global_const->GetHandledToStringString();
        } else {
            key_string = global_const->GetHandledValueOfString();
        }
        JSHandle<JSTaggedValue> entryfunc = GetProperty(thread, tagged, key_string).GetValue();
        if (entryfunc->IsCallable()) {
            auto info = NewRuntimeCallInfo(thread, entryfunc, tagged, JSTaggedValue::Undefined(), 0);
            JSTaggedValue value_result = JSFunction::Call(info.Get());
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
            if (!value_result.IsECMAObject()) {
                return value_result;
            }
        }
    }
    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot convert a illegal value to a Primitive", JSTaggedValue::Undefined());
}

JSHandle<EcmaString> JSTaggedValue::ToString(JSThread *thread, const JSHandle<JSTaggedValue> &tagged)
{
    if (tagged->IsString()) {
        return JSHandle<EcmaString>(tagged);
    }
    auto global_const = thread->GlobalConstants();
    if (tagged->IsSpecial()) {
        switch (tagged->GetRawData()) {
            case VALUE_UNDEFINED: {
                return JSHandle<EcmaString>(global_const->GetHandledUndefinedString());
            }
            case VALUE_NULL: {
                return JSHandle<EcmaString>(global_const->GetHandledNullString());
            }
            case VALUE_TRUE: {
                return JSHandle<EcmaString>(global_const->GetHandledTrueString());
            }
            case VALUE_FALSE: {
                return JSHandle<EcmaString>(global_const->GetHandledFalseString());
            }
            case VALUE_HOLE: {
                return JSHandle<EcmaString>(global_const->GetHandledEmptyString());
            }
            default:
                break;
        }
    }

    if (tagged->IsNumber()) {
        return base::NumberHelper::NumberToString(thread, tagged.GetTaggedValue());
    }

    if (tagged->IsBigInt()) {
        JSHandle<BigInt> tagged_value(tagged);
        return BigInt::ToString(thread, tagged_value);
    }

    auto empty_str = global_const->GetHandledEmptyString();
    if (tagged->IsECMAObject()) {
        JSHandle<JSTaggedValue> prim_value(thread, ToPrimitive(thread, tagged, PREFER_STRING));
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<EcmaString>(empty_str));
        return ToString(thread, prim_value);
    }
    // Already Include Symbol
    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot convert a illegal value to a String", JSHandle<EcmaString>(empty_str));
}

JSTaggedValue JSTaggedValue::CanonicalNumericIndexString(JSThread *thread, const JSHandle<JSTaggedValue> &tagged)
{
    JSHandle<EcmaString> str = thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString("-0");
    if (tagged->IsString()) {
        if (EcmaString::StringsAreEqual(static_cast<EcmaString *>(tagged->GetTaggedObject()), *str)) {
            return JSTaggedValue(-0.0);
        }
        JSHandle<JSTaggedValue> tmp(thread, ToNumber(thread, tagged));
        if (SameValue(ToString(thread, tmp).GetTaggedValue(), tagged.GetTaggedValue())) {
            return tmp.GetTaggedValue();
        }
    }
    return JSTaggedValue::Undefined();
}

JSHandle<JSObject> JSTaggedValue::ToObject(JSThread *thread, const JSHandle<JSTaggedValue> &tagged)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (tagged->IsInt() || tagged->IsDouble()) {
        return JSHandle<JSObject>::Cast(factory->NewJSPrimitiveRef(PrimitiveType::PRIMITIVE_NUMBER, tagged));
    }

    switch (tagged->GetRawData()) {
        case JSTaggedValue::VALUE_UNDEFINED: {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot convert a UNDEFINED value to a JSObject",
                                        JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
        }
        case JSTaggedValue::VALUE_HOLE: {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot convert a HOLE value to a JSObject",
                                        JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
        }
        case JSTaggedValue::VALUE_NULL: {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot convert a NULL value to a JSObject",
                                        JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
        }
        case JSTaggedValue::VALUE_TRUE:
        case JSTaggedValue::VALUE_FALSE: {
            return JSHandle<JSObject>::Cast(factory->NewJSPrimitiveRef(PrimitiveType::PRIMITIVE_BOOLEAN, tagged));
        }
        default: {
            break;
        }
    }

    if (tagged->IsECMAObject()) {
        return JSHandle<JSObject>::Cast(tagged);
    }
    if (tagged->IsSymbol()) {
        return JSHandle<JSObject>::Cast(factory->NewJSPrimitiveRef(PrimitiveType::PRIMITIVE_SYMBOL, tagged));
    }
    if (tagged->IsString()) {
        return JSHandle<JSObject>::Cast(factory->NewJSPrimitiveRef(PrimitiveType::PRIMITIVE_STRING, tagged));
    }
    if (tagged->IsBigInt()) {
        return JSHandle<JSObject>::Cast(factory->NewJSPrimitiveRef(PrimitiveType::PRIMITIVE_BIGINT, tagged));
    }
    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot convert a Unknown object value to a JSObject",
                                JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
}

// 7.3.1 Get ( O, P )
OperationResult JSTaggedValue::GetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                           const JSHandle<JSTaggedValue> &key)
{
    if (obj->IsUndefined() || obj->IsNull() || obj->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Obj is not a valid object",
                                    OperationResult(thread, JSTaggedValue::Exception(), PropertyMetaData(false)));
    }
    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");

    if (obj->IsJSProxy()) {
        return JSProxy::GetProperty(thread, JSHandle<JSProxy>(obj), key);
    }
    if (obj->IsTypedArray()) {
        return JSTypedArray::GetProperty(thread, obj, JSTypedArray::ToPropKey(thread, key));
    }

    return JSObject::GetProperty(thread, obj, key);
}

OperationResult JSTaggedValue::GetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj, uint32_t key)
{
    if (obj->IsUndefined() || obj->IsNull() || obj->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Obj is not a valid object",
                                    OperationResult(thread, JSTaggedValue::Exception(), PropertyMetaData(false)));
    }

    if (obj->IsJSProxy()) {
        JSHandle<JSTaggedValue> key_handle(thread, JSTaggedValue(key));
        return JSProxy::GetProperty(thread, JSHandle<JSProxy>(obj), key_handle);
    }

    if (obj->IsTypedArray()) {
        return JSTypedArray::GetProperty(thread, obj, key);
    }

    return JSObject::GetProperty(thread, obj, key);
}

OperationResult JSTaggedValue::GetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                           const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &receiver)
{
    if (obj->IsUndefined() || obj->IsNull() || obj->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Obj is not a valid object",
                                    OperationResult(thread, JSTaggedValue::Exception(), PropertyMetaData(false)));
    }
    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");

    if (obj->IsJSProxy()) {
        return JSProxy::GetProperty(thread, JSHandle<JSProxy>(obj), key, receiver);
    }
    if (obj->IsTypedArray()) {
        return JSTypedArray::GetProperty(thread, obj, JSTypedArray::ToPropKey(thread, key), receiver);
    }

    return JSObject::GetProperty(thread, obj, key, receiver);
}

// 7.3.3 Set (O, P, V, Throw)
bool JSTaggedValue::SetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value,
                                bool may_throw)
{
    if (obj->IsUndefined() || obj->IsNull() || obj->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Obj is not a Valid object", false);
    }

    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");

    // 4. Let success be O.[[Set]](P, V, O).
    bool success = false;
    if (obj->IsJSProxy()) {
        success = JSProxy::SetProperty(thread, JSHandle<JSProxy>(obj), key, value, may_throw);
    } else if (obj->IsTypedArray()) {
        success = JSTypedArray::SetProperty(thread, obj, JSTypedArray::ToPropKey(thread, key), value, may_throw);
    } else {
        success = JSObject::SetProperty(thread, obj, key, value, may_throw);
    }
    // 5. ReturnIfAbrupt(success).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, success);
    // 6. If success is false and Throw is true, throw a TypeError exception.
    // have done in JSObject::SetPropert.
    return success;
}

bool JSTaggedValue::SetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj, uint32_t key,
                                const JSHandle<JSTaggedValue> &value, bool may_throw)
{
    if (obj->IsUndefined() || obj->IsNull() || obj->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Obj is not a Valid object", false);
    }

    // 4. Let success be O.[[Set]](P, V, O).
    bool success = false;
    if (obj->IsJSProxy()) {
        JSHandle<JSTaggedValue> key_handle(thread, JSTaggedValue(key));
        success = JSProxy::SetProperty(thread, JSHandle<JSProxy>(obj), key_handle, value, may_throw);
    } else if (obj->IsTypedArray()) {
        JSHandle<JSTaggedValue> key_handle(thread, JSTaggedValue(key));
        success = JSTypedArray::SetProperty(
            thread, obj, JSHandle<JSTaggedValue>(JSTaggedValue::ToString(thread, key_handle)), value, may_throw);
    } else {
        success = JSObject::SetProperty(thread, obj, key, value, may_throw);
    }
    // 5. ReturnIfAbrupt(success).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, success);
    // 6. If success is false and Throw is true, throw a TypeError exception.
    // have done in JSObject::SetPropert.
    return success;
}

bool JSTaggedValue::SetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value,
                                const JSHandle<JSTaggedValue> &receiver, bool may_throw)
{
    if (obj->IsUndefined() || obj->IsNull() || obj->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Obj is not a Valid object", false);
    }

    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");

    // 4. Let success be O.[[Set]](P, V, O).
    bool success = false;
    if (obj->IsJSProxy()) {
        success = JSProxy::SetProperty(thread, JSHandle<JSProxy>(obj), key, value, receiver, may_throw);
    } else if (obj->IsTypedArray()) {
        success =
            JSTypedArray::SetProperty(thread, obj, JSTypedArray::ToPropKey(thread, key), value, receiver, may_throw);
    } else {
        success = JSObject::SetProperty(thread, obj, key, value, receiver, may_throw);
    }
    // 5. ReturnIfAbrupt(success).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, success);
    // 6. If success is false and Throw is true, throw a TypeError exception.
    // have done in JSObject::SetPropert.
    return success;
}

bool JSTaggedValue::DeleteProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                   const JSHandle<JSTaggedValue> &key)
{
    if (obj->IsJSProxy()) {
        return JSProxy::DeleteProperty(thread, JSHandle<JSProxy>(obj), key);
    }

    if (obj->IsSpecialContainer()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Can not delete property in Container Object", false);
    }

    return JSObject::DeleteProperty(thread, JSHandle<JSObject>(obj), key);
}

// 7.3.8 DeletePropertyOrThrow (O, P)
bool JSTaggedValue::DeletePropertyOrThrow(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                          const JSHandle<JSTaggedValue> &key)
{
    if (!obj->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Obj is not a valid object", false);
    }
    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");

    // 3. Let success be O.[[Delete]](P).
    bool success = DeleteProperty(thread, obj, key);

    // 4. ReturnIfAbrupt(success).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, success);
    // 5. If success is false, throw a TypeError exception
    if (!success) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot delete property", false);
    }
    return success;
}

// 7.3.7 DefinePropertyOrThrow (O, P, desc)
bool JSTaggedValue::DefinePropertyOrThrow(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                          const JSHandle<JSTaggedValue> &key, const PropertyDescriptor &desc)
{
    // 1. Assert: Type(O) is Object.
    // 2. Assert: IsPropertyKey(P) is true.
    ASSERT_PRINT(obj->IsECMAObject(), "Obj is not a valid object");
    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");
    // 3. Let success be ? O.[[DefineOwnProperty]](P, desc).
    bool success = DefineOwnProperty(thread, obj, key, desc);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    // 4. If success is false, throw a TypeError exception.
    if (!success) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", false);
    }
    return success;
}

bool JSTaggedValue::DefineOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                      const JSHandle<JSTaggedValue> &key, const PropertyDescriptor &desc)
{
    if (obj->IsJSArray()) {
        return JSArray::DefineOwnProperty(thread, JSHandle<JSObject>(obj), key, desc);
    }

    if (obj->IsJSProxy()) {
        return JSProxy::DefineOwnProperty(thread, JSHandle<JSProxy>(obj), key, desc);
    }

    if (obj->IsTypedArray()) {
        return JSTypedArray::DefineOwnProperty(thread, obj, JSTypedArray::ToPropKey(thread, key), desc);
    }

    if (obj->IsSpecialContainer()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Can not defineProperty on Container Object", false);
    }

    return JSObject::DefineOwnProperty(thread, JSHandle<JSObject>(obj), key, desc);
}

bool JSTaggedValue::GetOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                   const JSHandle<JSTaggedValue> &key, PropertyDescriptor &desc)
{
    if (obj->IsJSProxy()) {
        return JSProxy::GetOwnProperty(thread, JSHandle<JSProxy>(obj), key, desc);
    }
    if (obj->IsTypedArray()) {
        return JSTypedArray::GetOwnProperty(thread, obj, JSTypedArray::ToPropKey(thread, key), desc);
    }
    if (obj->IsSpecialContainer()) {
        return GetContainerProperty(thread, obj, key, desc);
    }
    return JSObject::GetOwnProperty(thread, JSTaggedValue::ToObject(thread, obj), key, desc);
}

bool JSTaggedValue::SetPrototype(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                 const JSHandle<JSTaggedValue> &proto)
{
    if (obj->IsJSProxy()) {
        return JSProxy::SetPrototype(thread, JSHandle<JSProxy>(obj), proto);
    }
    if (obj->IsSpecialContainer()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Can not set Prototype on Container Object", false);
    }

    return JSObject::SetPrototype(thread, JSHandle<JSObject>(obj), proto);
}

bool JSTaggedValue::PreventExtensions(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    if (obj->IsJSProxy()) {
        return JSProxy::PreventExtensions(thread, JSHandle<JSProxy>(obj));
    }
    return JSObject::PreventExtensions(thread, JSHandle<JSObject>(obj));
}

JSHandle<TaggedArray> JSTaggedValue::GetOwnPropertyKeys(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    if (obj->IsJSProxy()) {
        return JSProxy::OwnPropertyKeys(thread, JSHandle<JSProxy>(obj));
    }
    if (obj->IsTypedArray()) {
        return JSTypedArray::OwnPropertyKeys(thread, obj);
    }
    if (obj->IsSpecialContainer()) {
        return GetOwnContainerPropertyKeys(thread, obj);
    }
    return JSObject::GetOwnPropertyKeys(thread, JSTaggedValue::ToObject(thread, obj));
}

// 7.3.10 HasProperty (O, P)
bool JSTaggedValue::HasProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                const JSHandle<JSTaggedValue> &key)
{
    if (obj->IsJSProxy()) {
        return JSProxy::HasProperty(thread, JSHandle<JSProxy>(obj), key);
    }
    if (obj->IsTypedArray()) {
        return JSTypedArray::HasProperty(thread, obj, JSTypedArray::ToPropKey(thread, key));
    }
    if (obj->IsSpecialContainer()) {
        return HasContainerProperty(thread, obj, key);
    }
    return JSObject::HasProperty(thread, JSHandle<JSObject>(obj), key);
}

bool JSTaggedValue::HasProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj, uint32_t key)
{
    if (obj->IsJSProxy()) {
        JSHandle<JSTaggedValue> key_handle(thread, JSTaggedValue(key));
        return JSProxy::HasProperty(thread, JSHandle<JSProxy>(obj), key_handle);
    }
    if (obj->IsTypedArray()) {
        JSHandle<JSTaggedValue> key_handle(thread, JSTaggedValue(key));
        return JSTypedArray::HasProperty(thread, obj, JSHandle<JSTaggedValue>(ToString(thread, key_handle)));
    }
    if (obj->IsSpecialContainer()) {
        return HasContainerProperty(thread, obj, JSHandle<JSTaggedValue>(thread, JSTaggedValue(key)));
    }
    return JSObject::HasProperty(thread, JSHandle<JSObject>(obj), key);
}

// 7.3.11 HasOwnProperty (O, P)
bool JSTaggedValue::HasOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                   const JSHandle<JSTaggedValue> &key)
{
    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");

    PropertyDescriptor desc(thread);
    return JSTaggedValue::GetOwnProperty(thread, obj, key, desc);
}

bool JSTaggedValue::GlobalHasOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &key)
{
    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");

    PropertyDescriptor desc(thread);
    return JSObject::GlobalGetOwnProperty(thread, key, desc);
}

JSTaggedNumber JSTaggedValue::ToIndex(JSThread *thread, const JSHandle<JSTaggedValue> &tagged)
{
    if (tagged->IsUndefined()) {
        return JSTaggedNumber(0);
    }
    JSTaggedNumber integer_index = ToInteger(thread, tagged);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedNumber::Exception());
    if (integer_index.GetNumber() < 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "integerIndex < 0", JSTaggedNumber::Exception());
    }
    JSTaggedNumber index = ToLength(thread, tagged);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedNumber::Exception());
    if (!SameValue(integer_index, index)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "integerIndex != index", JSTaggedNumber::Exception());
    }
    return index;
}

JSHandle<JSTaggedValue> JSTaggedValue::ToPrototypeOrObj(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();

    if (obj->IsNumber()) {
        return JSHandle<JSTaggedValue>(thread,
                                       env->GetNumberFunction().GetObject<JSFunction>()->GetFunctionPrototype());
    }
    if (obj->IsBoolean()) {
        return JSHandle<JSTaggedValue>(thread,
                                       env->GetBooleanFunction().GetObject<JSFunction>()->GetFunctionPrototype());
    }
    if (obj->IsString()) {
        return JSHandle<JSTaggedValue>(thread,
                                       env->GetStringFunction().GetObject<JSFunction>()->GetFunctionPrototype());
    }
    if (obj->IsSymbol()) {
        return JSHandle<JSTaggedValue>(thread,
                                       env->GetSymbolFunction().GetObject<JSFunction>()->GetFunctionPrototype());
    }
    if (obj->IsBigInt()) {
        return JSHandle<JSTaggedValue>(thread,
                                       env->GetBigIntFunction().GetObject<JSFunction>()->GetFunctionPrototype());
    }

    return obj;
}

JSTaggedValue JSTaggedValue::GetSuperBase(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    if (obj->IsUndefined()) {
        return JSTaggedValue::Undefined();
    }

    ASSERT(obj->IsECMAObject());
    return JSObject::Cast(obj.GetTaggedValue())->GetPrototype(thread);
}

bool JSTaggedValue::HasContainerProperty([[maybe_unused]] JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                         const JSHandle<JSTaggedValue> &key)
{
    auto *hclass = obj->GetTaggedObject()->GetClass();
    JSType js_type = hclass->GetObjectType();
    switch (js_type) {
        case JSType::JS_ARRAY_LIST: {
            return JSHandle<JSArrayList>::Cast(obj)->Has(key.GetTaggedValue());
        }
        case JSType::JS_QUEUE:
            break;
        default: {
            UNREACHABLE();
        }
    }
    return false;
}

JSHandle<TaggedArray> JSTaggedValue::GetOwnContainerPropertyKeys(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    auto *hclass = obj->GetTaggedObject()->GetClass();
    JSType js_type = hclass->GetObjectType();
    switch (js_type) {
        case JSType::JS_ARRAY_LIST: {
            return JSArrayList::OwnKeys(thread, JSHandle<JSArrayList>::Cast(obj));
        }
        case JSType::JS_QUEUE:
            break;
        default: {
            UNREACHABLE();
        }
    }
    return thread->GetEcmaVM()->GetFactory()->EmptyArray();
}

bool JSTaggedValue::GetContainerProperty(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                         const JSHandle<JSTaggedValue> &key, PropertyDescriptor &desc)
{
    auto *hclass = obj->GetTaggedObject()->GetClass();
    JSType js_type = hclass->GetObjectType();
    switch (js_type) {
        case JSType::JS_ARRAY_LIST: {
            return JSArrayList::GetOwnProperty(thread, JSHandle<JSArrayList>::Cast(obj), key, desc);
        }
        case JSType::JS_QUEUE:
            break;
        default: {
            UNREACHABLE();
        }
    }
    return false;
}
}  // namespace panda::ecmascript
