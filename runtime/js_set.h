/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JSSET_H
#define ECMASCRIPT_JSSET_H

#include <limits>
#include "js_object.h"
#include "js_tagged_value-inl.h"
#include "linked_hash_table.h"

namespace panda::ecmascript {
class JSSet : public JSObject {
public:
    CAST_CHECK(JSSet, IsJSSet);

    static bool Delete(const JSThread *thread, const JSHandle<JSSet> &set, const JSHandle<JSTaggedValue> &value);

    static void Add(JSThread *thread, const JSHandle<JSSet> &set, const JSHandle<JSTaggedValue> &value);

    static void Clear(const JSThread *thread, const JSHandle<JSSet> &set);

    static bool IsKey(JSTaggedValue key)
    {
        return LinkedHashMap::IsKey(key);
    }

    bool Has(JSTaggedValue value, int hash) const;

    int GetSize() const;

    JSTaggedValue GetValue(int entry) const;

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, LinkedSet)
    ACCESSORS_FINISH(1)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JSSET_H
