/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ecma_string_table.h"
#include "ecma_vm.h"
#include "include/stack_walker.h"
#include "js_method.h"
#include "plugins/ecmascript/runtime/accessor_data.h"
#include "plugins/ecmascript/runtime/base/error_helper.h"
#include "plugins/ecmascript/runtime/builtins.h"
#include "plugins/ecmascript/runtime/class_info_extractor.h"
#include "plugins/ecmascript/runtime/class_linker/program_object.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_module.h"
#include "plugins/ecmascript/runtime/free_object.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/global_env_constants-inl.h"
#include "plugins/ecmascript/runtime/global_env_constants.h"
#include "plugins/ecmascript/runtime/ic/ic_handler.h"
#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/ic/property_box.h"
#include "plugins/ecmascript/runtime/ic/proto_change_details.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/jobs/micro_job_queue.h"
#include "plugins/ecmascript/runtime/jobs/pending_job.h"
#include "plugins/ecmascript/runtime/js_arguments.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_arraylist.h"
#include "plugins/ecmascript/runtime/js_async_function.h"
#include "plugins/ecmascript/runtime/js_async_from_sync_iterator_object.h"
#include "plugins/ecmascript/runtime/js_async_generator_object.h"
#include "plugins/ecmascript/runtime/js_dataview.h"
#include "plugins/ecmascript/runtime/js_date.h"
#include "plugins/ecmascript/runtime/js_for_in_iterator.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"
#include "plugins/ecmascript/runtime/js_hclass-inl.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_map.h"
#include "plugins/ecmascript/runtime/js_map_iterator.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_proxy.h"
#include "plugins/ecmascript/runtime/js_realm.h"
#include "plugins/ecmascript/runtime/js_regexp.h"
#include "plugins/ecmascript/runtime/js_regexp_iterator.h"
#include "plugins/ecmascript/runtime/js_set.h"
#include "plugins/ecmascript/runtime/js_set_iterator.h"
#include "plugins/ecmascript/runtime/js_string_iterator.h"
#include "plugins/ecmascript/runtime/js_symbol.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/js_weak_container.h"
#include "plugins/ecmascript/runtime/js_finalization_registry.h"
#include "plugins/ecmascript/runtime/layout_info-inl.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/mem/mem_manager.h"
#include "plugins/ecmascript/runtime/record.h"
#include "plugins/ecmascript/runtime/symbol_table-inl.h"
#include "plugins/ecmascript/runtime/template_map.h"
#include "plugins/ecmascript/runtime/interpreter/slow_runtime_stub.h"

namespace panda::ecmascript {

using ErrorType = base::ErrorType;
using ErrorHelper = base::ErrorHelper;

static void PandaFreeBufferFunc(void *buffer, void *size)
{
    auto *thread = Thread::GetCurrent();
    // Need to check if thread is available because
    // we can enter here after the runtime started termination
    if (thread == nullptr) {
        Runtime::GetCurrent()->GetInternalAllocator()->Free(buffer);
        return;
    }
    reinterpret_cast<JSThread *>(thread)->GetEcmaVM()->FreeAndRegisterNative(buffer, reinterpret_cast<size_t>(size));
}

ObjectFactory::ObjectFactory(JSThread *thread)
    : thread_(thread), heap_helper_(thread->GetEcmaVM()), vm_(thread->GetEcmaVM())
{
}

JSHandle<JSHClass> ObjectFactory::NewEcmaDynClassClass(JSHClass *hclass, uint32_t size, JSType type)
{
    NewObjectHook();
    uint32_t class_size = JSHClass::SIZE;
    auto *new_class = static_cast<JSHClass *>(heap_helper_.AllocateDynClassClass(hclass, class_size));
    new_class->Initialize(thread_, size, type, 0, HClass::HCLASS);

    return JSHandle<JSHClass>(thread_, new_class);
}

JSHandle<JSHClass> ObjectFactory::NewEcmaDynClass(JSHClass *hclass, uint32_t size, JSType type, uint32_t flags,
                                                  uint32_t inlined_props)
{
    NewObjectHook();
    uint32_t class_size = JSHClass::SIZE;
    auto *new_class = static_cast<JSHClass *>(heap_helper_.AllocateNonMovableOrHugeObject(hclass, class_size));
    new_class->Initialize(thread_, size, type, inlined_props, flags);

    return JSHandle<JSHClass>(thread_, new_class);
}

JSHandle<JSHClass> ObjectFactory::NewEcmaDynClass(uint32_t size, JSType type, uint32_t inlined_props)
{
    return NewEcmaDynClass(hclass_class_, size, type, 0, inlined_props);
}

void ObjectFactory::ObtainRootClass([[maybe_unused]] const JSHandle<GlobalEnv> &global_env)
{
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    hclass_class_ = JSHClass::Cast(global_const->GetHClassClass().GetTaggedObject());
    string_class_ = JSHClass::Cast(global_const->GetStringClass().GetTaggedObject());
    array_class_ = JSHClass::Cast(global_const->GetArrayClass().GetTaggedObject());
    weak_array_class_ = JSHClass::Cast(global_const->GetWeakArrayClass().GetTaggedObject());
    big_int_class_ = JSHClass::Cast(global_const->GetBigIntClass().GetTaggedObject());
    dictionary_class_ = JSHClass::Cast(global_const->GetDictionaryClass().GetTaggedObject());
    js_native_pointer_class_ = JSHClass::Cast(global_const->GetJSNativePointerClass().GetTaggedObject());
    free_object_with_none_field_class_ =
        JSHClass::Cast(global_const->GetFreeObjectWithNoneFieldClass().GetTaggedObject());
    free_object_with_one_field_class_ =
        JSHClass::Cast(global_const->GetFreeObjectWithOneFieldClass().GetTaggedObject());
    free_object_with_two_field_class_ =
        JSHClass::Cast(global_const->GetFreeObjectWithTwoFieldClass().GetTaggedObject());
    completion_record_class_ = JSHClass::Cast(global_const->GetCompletionRecordClass().GetTaggedObject());
    generator_context_class_ = JSHClass::Cast(global_const->GetGeneratorContextClass().GetTaggedObject());
    program_class_ = JSHClass::Cast(global_const->GetProgramClass().GetTaggedObject());
    ecma_module_class_ = JSHClass::Cast(global_const->GetEcmaModuleClass().GetTaggedObject());
    env_class_ = JSHClass::Cast(global_const->GetEnvClass().GetTaggedObject());
    symbol_class_ = JSHClass::Cast(global_const->GetSymbolClass().GetTaggedObject());
    accessor_data_class_ = JSHClass::Cast(global_const->GetAccessorDataClass().GetTaggedObject());
    internal_accessor_class_ = JSHClass::Cast(global_const->GetInternalAccessorClass().GetTaggedObject());
    capability_record_class_ = JSHClass::Cast(global_const->GetCapabilityRecordClass().GetTaggedObject());
    reactions_record_class_ = JSHClass::Cast(global_const->GetReactionsRecordClass().GetTaggedObject());
    promise_iterator_record_class_ = JSHClass::Cast(global_const->GetPromiseIteratorRecordClass().GetTaggedObject());
    micro_job_queue_class_ = JSHClass::Cast(global_const->GetMicroJobQueueClass().GetTaggedObject());
    pending_job_class_ = JSHClass::Cast(global_const->GetPendingJobClass().GetTaggedObject());
    js_proxy_ordinary_class_ = JSHClass::Cast(global_const->GetJSProxyOrdinaryClass().GetTaggedObject());
    js_proxy_callable_class_ = JSHClass::Cast(global_const->GetJSProxyCallableClass().GetTaggedObject());
    js_proxy_construct_class_ = JSHClass::Cast(global_const->GetJSProxyConstructClass().GetTaggedObject());
    object_wrapper_class_ = JSHClass::Cast(global_const->GetObjectWrapperClass().GetTaggedObject());
    property_box_class_ = JSHClass::Cast(global_const->GetPropertyBoxClass().GetTaggedObject());
    proto_change_marker_class_ = JSHClass::Cast(global_const->GetProtoChangeMarkerClass().GetTaggedObject());
    proto_change_details_class_ = JSHClass::Cast(global_const->GetProtoChangeDetailsClass().GetTaggedObject());
    promise_record_class_ = JSHClass::Cast(global_const->GetPromiseRecordClass().GetTaggedObject());
    promise_resolving_functions_record_ =
        JSHClass::Cast(global_const->GetPromiseResolvingFunctionsRecordClass().GetTaggedObject());
    transition_handler_class_ = JSHClass::Cast(global_const->GetTransitionHandlerClass().GetTaggedObject());
    prototype_handler_class_ = JSHClass::Cast(global_const->GetPrototypeHandlerClass().GetTaggedObject());
    function_extra_info_ = JSHClass::Cast(global_const->GetFunctionExtraInfoClass().GetTaggedObject());
    js_realm_class_ = JSHClass::Cast(global_const->GetJSRealmClass().GetTaggedObject());
    class_info_extractor_h_class_ = JSHClass::Cast(global_const->GetClassInfoExtractorHClass().GetTaggedObject());

    linked_hash_map_class_ = JSHClass::Cast(global_const->GetLinkedHashMapClass().GetTaggedObject());
    linked_hash_set_class_ = JSHClass::Cast(global_const->GetLinkedHashSetClass().GetTaggedObject());
    weak_ref_class_ = JSHClass::Cast(global_const->GetWeakRefClass().GetTaggedObject());
    weak_linked_hash_map_class_ = JSHClass::Cast(global_const->GetWeakLinkedHashMapClass().GetTaggedObject());
    weak_linked_hash_set_class_ = JSHClass::Cast(global_const->GetWeakLinkedHashSetClass().GetTaggedObject());
}

void ObjectFactory::InitObjectFields(TaggedObject *object)
{
    auto *klass = object->GetClass();
    auto obj_body_size = klass->GetObjectSize() - TaggedObject::TaggedObjectSize();
    ASSERT(obj_body_size % JSTaggedValue::TaggedTypeSize() == 0);
    int num_of_fields = static_cast<int>(obj_body_size / JSTaggedValue::TaggedTypeSize());
    for (int i = 0; i < num_of_fields; i++) {
        size_t field_offset = TaggedObject::TaggedObjectSize() + i * JSTaggedValue::TaggedTypeSize();
        // Without barrier because 'object' is a just allocated object and it doesn't contain references.
        ObjectAccessor::SetDynValueWithoutBarrier(reinterpret_cast<void *>(object), field_offset,
                                                  JSTaggedValue::Undefined().GetRawData());
    }
}

void ObjectFactory::NewJSArrayBufferData(const JSHandle<JSArrayBuffer> &array, int32_t length)
{
    if (length == 0) {
        return;
    }
    auto size = length * sizeof(uint8_t);

    JSHandle<JSTaggedValue> data(thread_, array->GetArrayBufferData());
    if (data.GetTaggedValue() != JSTaggedValue::Undefined()) {
        JSHandle<JSNativePointer> pointer(data);
        auto *new_data = vm_->AllocAndRegisterNative(size);
        if (memset_s(new_data, length, 0, length) != EOK) {
            LOG_ECMA(FATAL) << "memset_s failed";
            UNREACHABLE();
        }
        pointer->ResetExternalPointer(new_data);
        return;
    }

    auto *new_data = vm_->AllocAndRegisterNative(size);
    if (memset_s(new_data, length, 0, length) != EOK) {
        LOG_ECMA(FATAL) << "memset_s failed";
        UNREACHABLE();
    }
    JSHandle<JSNativePointer> pointer =
        NewJSNativePointer(new_data, PandaFreeBufferFunc, reinterpret_cast<void *>(size));
    array->SetArrayBufferData(thread_, pointer.GetTaggedValue());
    vm_->PushToArrayDataList(*pointer);
}

JSHandle<JSArrayBuffer> ObjectFactory::NewJSArrayBuffer(int32_t length)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();

    JSHandle<JSFunction> constructor(env->GetArrayBufferFunction());
    JSHandle<JSTaggedValue> new_target(constructor);
    JSHandle<JSArrayBuffer> array_buffer(NewJSObjectByConstructor(constructor, new_target));
    array_buffer->SetArrayBufferByteLength(thread_, JSTaggedValue(length));
    if (length > 0) {
        auto *new_data = vm_->AllocAndRegisterNative(length);
        if (memset_s(new_data, length, 0, length) != EOK) {
            LOG_ECMA(FATAL) << "memset_s failed";
            UNREACHABLE();
        }
        JSHandle<JSNativePointer> pointer =
            NewJSNativePointer(new_data, PandaFreeBufferFunc, reinterpret_cast<void *>(length));
        array_buffer->SetArrayBufferData(thread_, pointer.GetTaggedValue());
        array_buffer->SetShared(thread_, JSTaggedValue::False());
        vm_->PushToArrayDataList(*pointer);
    }
    return array_buffer;
}

JSHandle<JSArrayBuffer> ObjectFactory::NewJSArrayBuffer(void *buffer, int32_t length, const DeleteEntryPoint &deleter,
                                                        void *data, bool share)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();

    JSHandle<JSFunction> constructor(env->GetArrayBufferFunction());
    JSHandle<JSTaggedValue> new_target(constructor);
    JSHandle<JSArrayBuffer> array_buffer(NewJSObjectByConstructor(constructor, new_target));
    length = buffer == nullptr ? 0 : length;
    array_buffer->SetArrayBufferByteLength(thread_, JSTaggedValue(length));
    if (length > 0) {
        JSHandle<JSNativePointer> pointer = NewJSNativePointer(buffer, deleter, data);
        array_buffer->SetArrayBufferData(thread_, pointer.GetTaggedValue());
        array_buffer->SetShared(thread_, JSTaggedValue(share));
        vm_->PushToArrayDataList(*pointer);
    }
    return array_buffer;
}

JSHandle<JSDataView> ObjectFactory::NewJSDataView(JSHandle<JSArrayBuffer> buffer, int32_t offset, int32_t length)
{
    JSTaggedValue array_length = buffer->GetArrayBufferByteLength();
    if (!array_length.IsNumber()) {
        THROW_TYPE_ERROR_AND_RETURN(thread_, "ArrayBuffer length error",
                                    JSHandle<JSDataView>(thread_, JSTaggedValue::Undefined()));
    }
    if (offset + length > array_length.GetNumber()) {
        THROW_TYPE_ERROR_AND_RETURN(thread_, "offset or length error",
                                    JSHandle<JSDataView>(thread_, JSTaggedValue::Undefined()));
    }
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();

    JSHandle<JSFunction> constructor(env->GetDataViewFunction());
    JSHandle<JSTaggedValue> new_target(constructor);
    JSHandle<JSDataView> array_buffer(NewJSObjectByConstructor(constructor, new_target));
    array_buffer->SetDataView(thread_, JSTaggedValue::True());
    array_buffer->SetViewedArrayBuffer(thread_, buffer.GetTaggedValue());
    array_buffer->SetByteLength(thread_, JSTaggedValue(length));
    array_buffer->SetByteOffset(thread_, JSTaggedValue(offset));
    return array_buffer;
}

void ObjectFactory::NewJSRegExpByteCodeData(const JSHandle<JSRegExp> &regexp, void *buffer, size_t size)
{
    if (buffer == nullptr) {
        return;
    }

    auto *new_buffer = vm_->AllocAndRegisterNative(size);
    if (memcpy_s(new_buffer, size, buffer, size) != EOK) {
        LOG_ECMA(FATAL) << "memcpy_s failed";
        UNREACHABLE();
    }
    JSTaggedValue data = regexp->GetByteCodeBuffer();
    if (data != JSTaggedValue::Undefined()) {
        JSNativePointer *native = JSNativePointer::Cast(data.GetTaggedObject());
        native->ResetExternalPointer(new_buffer);
        return;
    }
    JSHandle<JSNativePointer> pointer =
        NewJSNativePointer(new_buffer, PandaFreeBufferFunc, reinterpret_cast<void *>(size));
    regexp->SetByteCodeBuffer(thread_, pointer.GetTaggedValue());
    regexp->SetLength(thread_, JSTaggedValue(static_cast<uint32_t>(size)));

    // push uint8_t* to ecma array_data_list
    vm_->PushToArrayDataList(*pointer);
}

JSHandle<JSHClass> ObjectFactory::NewEcmaDynClass(uint32_t size, JSType type, const JSHandle<JSTaggedValue> &prototype,
                                                  uint32_t flags)
{
    JSHandle<JSHClass> new_class =
        NewEcmaDynClass(hclass_class_, size, type, flags, JSHClass::DEFAULT_CAPACITY_OF_IN_OBJECTS);
    new_class->SetPrototype(thread_, prototype.GetTaggedValue());
    return new_class;
}

JSHandle<JSObject> ObjectFactory::NewJSObject(const JSHandle<JSHClass> &jshclass)
{
    JSHandle<JSObject> obj(thread_, JSObject::Cast(NewDynObject(jshclass)));
    JSHandle<TaggedArray> empty_array = EmptyArray();
    obj->InitializeHash();
    obj->SetElements(thread_, empty_array, SKIP_BARRIER);
    obj->SetProperties(thread_, empty_array, SKIP_BARRIER);
    return obj;
}

JSHandle<TaggedArray> ObjectFactory::CloneProperties(const JSHandle<TaggedArray> &old)
{
    uint32_t new_length = old->GetLength();
    if (new_length == 0) {
        return EmptyArray();
    }
    NewObjectHook();
    auto klass = old->GetClass();
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), new_length);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(klass, size);
    JSHandle<TaggedArray> new_array(thread_, header);
    new_array->SetLength(new_length);

    for (uint32_t i = 0; i < new_length; i++) {
        JSTaggedValue value = old->Get(i);
        new_array->Set(thread_, i, value);
    }
    return new_array;
}

JSHandle<JSObject> ObjectFactory::CloneObjectLiteral(JSHandle<JSObject> object)
{
    NewObjectHook();
    auto klass = JSHandle<JSHClass>(thread_, object->GetClass());

    JSHandle<JSObject> clone_object = NewJSObject(klass);

    JSHandle<TaggedArray> elements(thread_, object->GetElements());
    auto new_elements = CloneProperties(elements);
    clone_object->SetElements(thread_, new_elements.GetTaggedValue());

    JSHandle<TaggedArray> properties(thread_, object->GetProperties());
    auto new_properties = CloneProperties(properties);
    clone_object->SetProperties(thread_, new_properties.GetTaggedValue());

    for (uint32_t i = 0; i < klass->GetInlinedProperties(); i++) {
        clone_object->SetPropertyInlinedProps(thread_, i, object->GetPropertyInlinedProps(i));
    }
    return clone_object;
}

JSHandle<JSArray> ObjectFactory::CloneArrayLiteral(JSHandle<JSArray> object)
{
    NewObjectHook();
    auto klass = JSHandle<JSHClass>(thread_, object->GetClass());

    JSHandle<JSArray> clone_object(NewJSObject(klass));
    clone_object->SetArrayLength(thread_, object->GetArrayLength());

    JSHandle<TaggedArray> elements(thread_, object->GetElements());
    auto new_elements = CopyArray(elements, elements->GetLength(), elements->GetLength());
    clone_object->SetElements(thread_, new_elements.GetTaggedValue());

    JSHandle<TaggedArray> properties(thread_, object->GetProperties());
    auto new_properties = CopyArray(properties, properties->GetLength(), properties->GetLength());
    clone_object->SetProperties(thread_, new_properties.GetTaggedValue());

    for (uint32_t i = 0; i < klass->GetInlinedProperties(); i++) {
        clone_object->SetPropertyInlinedProps(thread_, i, object->GetPropertyInlinedProps(i));
    }
    return clone_object;
}

JSHandle<TaggedArray> ObjectFactory::CloneProperties(const JSHandle<TaggedArray> &old,
                                                     const JSHandle<JSTaggedValue> &env, const JSHandle<JSObject> &obj,
                                                     const JSHandle<JSTaggedValue> &constpool)
{
    uint32_t new_length = old->GetLength();
    if (new_length == 0) {
        return EmptyArray();
    }
    NewObjectHook();
    auto klass = old->GetClass();
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), new_length);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(klass, size);
    JSHandle<TaggedArray> new_array(thread_, header);
    new_array->SetLength(new_length);

    for (uint32_t i = 0; i < new_length; i++) {
        JSTaggedValue value = old->Get(i);
        if (!value.IsJSFunction()) {
            new_array->Set(thread_, i, value);
        } else {
            JSHandle<JSFunction> value_handle(thread_, value);
            JSHandle<JSFunction> new_func = CloneJSFuction(value_handle, value_handle->GetFunctionKind());
            new_func->SetLexicalEnv(thread_, env);
            new_func->SetHomeObject(thread_, obj);
            new_func->SetConstantPool(thread_, constpool);
            new_array->Set(thread_, i, new_func);
        }
    }
    return new_array;
}

JSHandle<JSObject> ObjectFactory::CloneObjectLiteral(JSHandle<JSObject> object, const JSHandle<JSTaggedValue> &env,
                                                     const JSHandle<JSTaggedValue> &constpool, bool can_share_hclass)
{
    NewObjectHook();
    auto klass = JSHandle<JSHClass>(thread_, object->GetClass());

    if (!can_share_hclass) {
        klass = JSHClass::Clone(thread_, klass);
    }

    JSHandle<JSObject> clone_object = NewJSObject(klass);

    JSHandle<TaggedArray> elements(thread_, object->GetElements());
    auto new_elements = CloneProperties(elements, env, clone_object, constpool);
    clone_object->SetElements(thread_, new_elements.GetTaggedValue());

    JSHandle<TaggedArray> properties(thread_, object->GetProperties());
    auto new_properties = CloneProperties(properties, env, clone_object, constpool);
    clone_object->SetProperties(thread_, new_properties.GetTaggedValue());

    for (uint32_t i = 0; i < klass->GetInlinedProperties(); i++) {
        JSTaggedValue value = object->GetPropertyInlinedProps(i);
        if (!value.IsJSFunction()) {
            clone_object->SetPropertyInlinedProps(thread_, i, value);
        } else {
            JSHandle<JSFunction> value_handle(thread_, value);
            JSHandle<JSFunction> new_func = CloneJSFuction(value_handle, value_handle->GetFunctionKind());
            new_func->SetLexicalEnv(thread_, env);
            new_func->SetHomeObject(thread_, clone_object);
            new_func->SetConstantPool(thread_, constpool);
            clone_object->SetPropertyInlinedProps(thread_, i, new_func.GetTaggedValue());
        }
    }
    return clone_object;
}

JSHandle<JSFunction> ObjectFactory::CloneJSFuction(JSHandle<JSFunction> obj, FunctionKind kind)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> jshclass(thread_, obj->GetJSHClass());
    JSHandle<JSFunction> clone_func = NewJSFunctionByDynClass(obj->GetCallTarget(), jshclass, kind);
    if (kind == FunctionKind::GENERATOR_FUNCTION) {
        JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
        JSHandle<JSObject> initial_generator_func_prototype =
            NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
        JSObject::SetPrototype(thread_, initial_generator_func_prototype, env->GetGeneratorPrototype());
        clone_func->SetProtoOrDynClass(thread_, initial_generator_func_prototype);
    } else if (kind == FunctionKind::ASYNC_GENERATOR_FUNCTION) {
        JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
        JSHandle<JSObject> initial_async_generator_func_prototype =
            NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
        JSObject::SetPrototype(thread_, initial_async_generator_func_prototype, env->GetAsyncGeneratorPrototype());
        clone_func->SetProtoOrDynClass(thread_, initial_async_generator_func_prototype);
    }

    JSTaggedValue length = obj->GetPropertyInlinedProps(JSFunction::LENGTH_INLINE_PROPERTY_INDEX);
    clone_func->SetPropertyInlinedProps(thread_, JSFunction::LENGTH_INLINE_PROPERTY_INDEX, length);
    return clone_func;
}

JSHandle<JSFunction> ObjectFactory::CloneClassCtor(JSHandle<JSFunction> ctor, const JSHandle<JSTaggedValue> &lexenv,
                                                   bool can_share_hclass)
{
    NewObjectHook();
    JSHandle<JSTaggedValue> constpool(thread_, ctor->GetConstantPool());
    JSHandle<JSHClass> hclass(thread_, ctor->GetClass());

    if (!can_share_hclass) {
        hclass = JSHClass::Clone(thread_, hclass);
    }

    FunctionKind kind = ctor->GetFunctionKind();
    ASSERT_PRINT(kind == FunctionKind::CLASS_CONSTRUCTOR || kind == FunctionKind::DERIVED_CONSTRUCTOR,
                 "cloned function is not class");
    JSHandle<JSFunction> clone_ctor = NewJSFunctionByDynClass(ctor->GetCallTarget(), hclass, kind);

    for (uint32_t i = 0; i < hclass->GetInlinedProperties(); i++) {
        JSTaggedValue value = ctor->GetPropertyInlinedProps(i);
        if (!value.IsJSFunction()) {
            clone_ctor->SetPropertyInlinedProps(thread_, i, value);
        } else {
            JSHandle<JSFunction> value_handle(thread_, value);
            JSHandle<JSFunction> new_func = CloneJSFuction(value_handle, value_handle->GetFunctionKind());
            new_func->SetLexicalEnv(thread_, lexenv);
            new_func->SetHomeObject(thread_, clone_ctor);
            new_func->SetConstantPool(thread_, constpool);
            clone_ctor->SetPropertyInlinedProps(thread_, i, new_func.GetTaggedValue());
        }
    }

    JSHandle<TaggedArray> elements(thread_, ctor->GetElements());
    auto new_elements = CloneProperties(elements, lexenv, JSHandle<JSObject>(clone_ctor), constpool);
    clone_ctor->SetElements(thread_, new_elements.GetTaggedValue());

    JSHandle<TaggedArray> properties(thread_, ctor->GetProperties());
    auto new_properties = CloneProperties(properties, lexenv, JSHandle<JSObject>(clone_ctor), constpool);
    clone_ctor->SetProperties(thread_, new_properties.GetTaggedValue());

    clone_ctor->SetConstantPool(thread_, constpool);

    return clone_ctor;
}

JSHandle<JSObject> ObjectFactory::NewNonMovableJSObject(const JSHandle<JSHClass> &jshclass)
{
    JSHandle<JSObject> obj(thread_, JSObject::Cast(NewNonMovableDynObject(jshclass, jshclass->GetInlinedProperties())));
    obj->SetElements(thread_, EmptyArray(), SKIP_BARRIER);
    obj->SetProperties(thread_, EmptyArray(), SKIP_BARRIER);
    return obj;
}

JSHandle<JSPrimitiveRef> ObjectFactory::NewJSPrimitiveRef(const JSHandle<JSHClass> &dyn_klass,
                                                          const JSHandle<JSTaggedValue> &object)
{
    JSHandle<JSPrimitiveRef> obj = JSHandle<JSPrimitiveRef>::Cast(NewJSObject(dyn_klass));
    obj->SetValue(thread_, object);
    return obj;
}

JSHandle<JSArray> ObjectFactory::NewJSArray()
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> function = env->GetArrayFunction();

    return JSHandle<JSArray>(NewJSObjectByConstructor(JSHandle<JSFunction>(function), function));
}

JSHandle<BigInt> ObjectFactory::NewBigInt()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(big_int_class_);
    JSHandle<BigInt> obj(thread_, BigInt::Cast(header));
    obj->SetData(thread_, JSTaggedValue::Undefined());
    obj->SetSign(false);
    return obj;
}

JSHandle<JSForInIterator> ObjectFactory::NewJSForinIterator(const JSHandle<JSTaggedValue> &obj)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass(env->GetForinIteratorClass());

    JSHandle<JSForInIterator> it = JSHandle<JSForInIterator>::Cast(NewJSObject(dynclass));
    it->SetObject(thread_, obj);
    it->SetWasVisited(thread_, JSTaggedValue::False());
    it->SetVisitedKeys(thread_, env->GetEmptyTaggedQueue());
    it->SetRemainingKeys(thread_, env->GetEmptyTaggedQueue());
    return it;
}

JSHandle<JSHClass> ObjectFactory::CreateJSRegExpInstanceClass(JSHandle<JSTaggedValue> proto)
{
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSHClass> regexp_dynclass = CreateDynClass<JSRegExp>(JSType::JS_REG_EXP, proto);

    uint32_t field_order = 0;
    JSHandle<LayoutInfo> layout_info_handle = CreateLayoutInfo(1);
    {
        PropertyAttributes attributes = PropertyAttributes::Default(true, false, false);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order++);
        layout_info_handle->AddKey(thread_, 0, global_const->GetLastIndexString(), attributes);
    }

    {
        regexp_dynclass->SetLayout(thread_, layout_info_handle);
        regexp_dynclass->SetNumberOfProps(field_order);
    }

    return regexp_dynclass;
}

JSHandle<JSHClass> ObjectFactory::CreateJSArrayInstanceClass(JSHandle<JSTaggedValue> proto)
{
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSHClass> array_dynclass = CreateDynClass<JSArray>(JSType::JS_ARRAY, proto);

    uint32_t field_order = 0;
    ASSERT(JSArray::LENGTH_INLINE_PROPERTY_INDEX == field_order);
    JSHandle<LayoutInfo> layout_info_handle = CreateLayoutInfo(1);
    {
        PropertyAttributes attributes = PropertyAttributes::DefaultAccessor(true, false, false);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order++);
        layout_info_handle->AddKey(thread_, 0, global_const->GetLengthString(), attributes);
    }

    {
        array_dynclass->SetLayout(thread_, layout_info_handle);
        array_dynclass->SetNumberOfProps(field_order);
    }
    array_dynclass->SetIsStableElements(true);
    array_dynclass->SetHasConstructor(false);

    return array_dynclass;
}

JSHandle<JSHClass> ObjectFactory::CreateJSArguments()
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> proto = env->GetObjectFunctionPrototype();

    JSHandle<JSHClass> arguments_dynclass = CreateDynClass<JSArguments>(JSType::JS_ARGUMENTS, proto);

    uint32_t field_order = 0;
    ASSERT(JSArguments::LENGTH_INLINE_PROPERTY_INDEX == field_order);
    JSHandle<LayoutInfo> layout_info_handle = CreateLayoutInfo(JSArguments::LENGTH_OF_INLINE_PROPERTIES);
    {
        PropertyAttributes attributes = PropertyAttributes::Default(true, false, true);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order++);
        layout_info_handle->AddKey(thread_, JSArguments::LENGTH_INLINE_PROPERTY_INDEX, global_const->GetLengthString(),
                                   attributes);
    }

    ASSERT(JSArguments::ITERATOR_INLINE_PROPERTY_INDEX == field_order);
    {
        PropertyAttributes attributes = PropertyAttributes::Default(true, false, true);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order++);
        layout_info_handle->AddKey(thread_, JSArguments::ITERATOR_INLINE_PROPERTY_INDEX,
                                   env->GetIteratorSymbol().GetTaggedValue(), attributes);
    }

    {
        ASSERT(JSArguments::CALLEE_INLINE_PROPERTY_INDEX == field_order);
        PropertyAttributes attributes = PropertyAttributes::Default(false, false, false);
        attributes.SetIsInlinedProps(true);
        attributes.SetIsAccessor(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order++);
        layout_info_handle->AddKey(thread_, JSArguments::CALLEE_INLINE_PROPERTY_INDEX,
                                   thread_->GlobalConstants()->GetHandledCalleeString().GetTaggedValue(), attributes);
    }

    {
        arguments_dynclass->SetLayout(thread_, layout_info_handle);
        arguments_dynclass->SetNumberOfProps(field_order);
    }
    arguments_dynclass->SetIsStableElements(true);
    return arguments_dynclass;
}

JSHandle<JSArguments> ObjectFactory::NewJSArguments()
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetArgumentsClass());
    JSHandle<JSArguments> obj = JSHandle<JSArguments>::Cast(NewJSObject(dynclass));
    return obj;
}

JSHandle<JSObject> ObjectFactory::GetJSError(const ErrorType &error_type, const char *data)
{
    ASSERT_PRINT(error_type == ErrorType::ERROR || error_type == ErrorType::EVAL_ERROR ||
                     error_type == ErrorType::RANGE_ERROR || error_type == ErrorType::REFERENCE_ERROR ||
                     error_type == ErrorType::SYNTAX_ERROR || error_type == ErrorType::TYPE_ERROR ||
                     error_type == ErrorType::URI_ERROR,
                 "The error type is not in the valid range.");
    if (data != nullptr) {
        JSHandle<EcmaString> handle_msg = NewFromString(data);
        return NewJSError(error_type, handle_msg);
    }
    JSHandle<EcmaString> empty_string(thread_->GlobalConstants()->GetHandledEmptyString());
    return NewJSError(error_type, empty_string);
}

JSHandle<JSObject> ObjectFactory::NewJSError(const ErrorType &error_type, const JSHandle<EcmaString> &message)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> native_constructor;
    switch (error_type) {
        case ErrorType::RANGE_ERROR:
            native_constructor = env->GetRangeErrorFunction();
            break;
        case ErrorType::EVAL_ERROR:
            native_constructor = env->GetEvalErrorFunction();
            break;
        case ErrorType::REFERENCE_ERROR:
            native_constructor = env->GetReferenceErrorFunction();
            break;
        case ErrorType::TYPE_ERROR:
            native_constructor = env->GetTypeErrorFunction();
            break;
        case ErrorType::URI_ERROR:
            native_constructor = env->GetURIErrorFunction();
            break;
        case ErrorType::SYNTAX_ERROR:
            native_constructor = env->GetSyntaxErrorFunction();
            break;
        default:
            native_constructor = env->GetErrorFunction();
            break;
    }
    JSHandle<JSFunction> native_func = JSHandle<JSFunction>::Cast(native_constructor);
    JSHandle<JSTaggedValue> native_prototype(thread_, native_func->GetFunctionPrototype());
    JSHandle<JSTaggedValue> ctor_key = global_const->GetHandledConstructorString();

    auto info =
        NewRuntimeCallInfo(thread_, JSTaggedValue::Undefined(), native_prototype, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(message);
    JSTaggedValue obj = JSFunction::Invoke(info.Get(), ctor_key);

    JSHandle<JSObject> handle_native_instance_obj(thread_, obj);
    return handle_native_instance_obj;
}

JSHandle<JSObject> ObjectFactory::NewJSObjectByConstructor(const JSHandle<JSFunction> &constructor,
                                                           const JSHandle<JSTaggedValue> &new_target,
                                                           panda::SpaceType space_type)
{
    JSHandle<JSHClass> jshclass;
    if (!constructor->HasFunctionPrototype() ||
        (constructor->GetProtoOrDynClass().IsHeapObject() && constructor->GetFunctionPrototype().IsECMAObject())) {
        jshclass = JSFunction::GetInstanceJSHClass(thread_, constructor, new_target);
    } else {
        JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
        jshclass = JSFunction::GetInstanceJSHClass(thread_, JSHandle<JSFunction>(env->GetObjectFunction()), new_target);
    }
    // Check this exception elsewhere
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread_);

    JSHandle<JSObject> obj = space_type == panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT
                                 ? NewNonMovableJSObject(jshclass)
                                 : NewJSObject(jshclass);
    {
        JSType type = jshclass->GetObjectType();
        switch (type) {
            case JSType::JS_OBJECT:
            case JSType::JS_ERROR:
            case JSType::JS_EVAL_ERROR:
            case JSType::JS_RANGE_ERROR:
            case JSType::JS_REFERENCE_ERROR:
            case JSType::JS_TYPE_ERROR:
            case JSType::JS_URI_ERROR:
            case JSType::JS_SYNTAX_ERROR:
            case JSType::JS_ITERATOR:
            case JSType::JS_INTL:
            case JSType::JS_LOCALE:
            case JSType::JS_DATE_TIME_FORMAT:
            case JSType::JS_NUMBER_FORMAT:
            case JSType::JS_RELATIVE_TIME_FORMAT:
            case JSType::JS_COLLATOR:
            case JSType::JS_PLURAL_RULES:
                break;
            case JSType::FINALIZATION_REGISTRY:
                JSFinalizationRegistry::Cast(*obj)->SetCells(thread_, EmptyArray());
                break;
            case JSType::JS_ARRAY: {
                JSArray::Cast(*obj)->SetLength(thread_, JSTaggedValue(0));
                auto accessor = thread_->GlobalConstants()->GetArrayLengthAccessor();
                JSArray::Cast(*obj)->SetPropertyInlinedProps(thread_, JSArray::LENGTH_INLINE_PROPERTY_INDEX, accessor);
                break;
            }
            case JSType::JS_DATE:
                JSDate::Cast(*obj)->SetTimeValue(thread_, JSTaggedValue(0.0));
                JSDate::Cast(*obj)->SetLocalOffset(thread_, JSTaggedValue(JSDate::MAX_DOUBLE));
                break;
            case JSType::JS_TYPED_ARRAY:
            case JSType::JS_INT8_ARRAY:
            case JSType::JS_UINT8_ARRAY:
            case JSType::JS_UINT8_CLAMPED_ARRAY:
            case JSType::JS_INT16_ARRAY:
            case JSType::JS_UINT16_ARRAY:
            case JSType::JS_INT32_ARRAY:
            case JSType::JS_UINT32_ARRAY:
            case JSType::JS_FLOAT32_ARRAY:
            case JSType::JS_FLOAT64_ARRAY:
            case JSType::JS_BIGINT64_ARRAY:
            case JSType::JS_BIGUINT64_ARRAY:
                JSTypedArray::Cast(*obj)->SetViewedArrayBuffer(thread_, JSTaggedValue::Undefined());
                JSTypedArray::Cast(*obj)->SetTypedArrayName(thread_, JSTaggedValue::Undefined());
                JSTypedArray::Cast(*obj)->SetByteLength(thread_, JSTaggedValue(0));
                JSTypedArray::Cast(*obj)->SetByteOffset(thread_, JSTaggedValue(0));
                JSTypedArray::Cast(*obj)->SetArrayLength(thread_, JSTaggedValue(0));
                break;
            case JSType::JS_REG_EXP:
                JSRegExp::Cast(*obj)->SetByteCodeBuffer(thread_, JSTaggedValue::Undefined());
                JSRegExp::Cast(*obj)->SetOriginalSource(thread_, JSTaggedValue::Undefined());
                JSRegExp::Cast(*obj)->SetOriginalFlags(thread_, JSTaggedValue(0));
                JSRegExp::Cast(*obj)->SetGroupName(thread_, JSTaggedValue::Undefined());
                JSRegExp::Cast(*obj)->SetLength(thread_, JSTaggedValue(0));
                break;
            case JSType::JS_PRIMITIVE_REF:
                JSPrimitiveRef::Cast(*obj)->SetValue(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_SET:
                JSSet::Cast(*obj)->SetLinkedSet(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_MAP:
                JSMap::Cast(*obj)->SetLinkedMap(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_WEAK_REF:
                JSWeakRef::Cast(*obj)->SetReferent(JSTaggedValue::Undefined());
                break;
            case JSType::JS_WEAK_MAP:
                JSWeakMap::Cast(*obj)->SetLinkedMap(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_WEAK_SET:
                JSWeakSet::Cast(*obj)->SetLinkedSet(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_ASYNC_GENERATOR_OBJECT:
                JSAsyncGeneratorObject::Cast(*obj)->SetAsyncGeneratorQueue(thread_,
                                                                           GetEmptyTaggedQueue().GetTaggedValue());
                [[fallthrough]];
            case JSType::JS_ASYNC_FUNC_OBJECT:
                JSAsyncFuncObject::Cast(*obj)->SetPromise(thread_, JSTaggedValue::Undefined());
                [[fallthrough]];
            case JSType::JS_GENERATOR_OBJECT:
                JSGeneratorObject::Cast(*obj)->SetGeneratorState(thread_, JSTaggedValue::Undefined());
                JSGeneratorObject::Cast(*obj)->SetGeneratorContext(thread_, JSTaggedValue::Undefined());
                JSGeneratorObject::Cast(*obj)->SetResumeResult(thread_, JSTaggedValue::Undefined());
                JSGeneratorObject::Cast(*obj)->SetResumeMode(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_STRING_ITERATOR:
                JSStringIterator::Cast(*obj)->SetStringIteratorNextIndex(thread_, JSTaggedValue(0));
                JSStringIterator::Cast(*obj)->SetIteratedString(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_ARRAY_BUFFER:
                JSArrayBuffer::Cast(*obj)->SetArrayBufferByteLength(thread_, JSTaggedValue(0));
                JSArrayBuffer::Cast(*obj)->SetArrayBufferData(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_PROMISE:
                JSPromise::Cast(*obj)->SetPromiseState(thread_,
                                                       JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING)));
                JSPromise::Cast(*obj)->SetPromiseResult(thread_, JSTaggedValue::Undefined());
                JSPromise::Cast(*obj)->SetPromiseRejectReactions(thread_, GetEmptyTaggedQueue().GetTaggedValue());
                JSPromise::Cast(*obj)->SetPromiseFulfillReactions(thread_, GetEmptyTaggedQueue().GetTaggedValue());

                JSPromise::Cast(*obj)->SetPromiseIsHandled(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_DATA_VIEW:
                JSDataView::Cast(*obj)->SetDataView(thread_, JSTaggedValue(false));
                JSDataView::Cast(*obj)->SetViewedArrayBuffer(thread_, JSTaggedValue::Undefined());
                JSDataView::Cast(*obj)->SetByteLength(thread_, JSTaggedValue(0));
                JSDataView::Cast(*obj)->SetByteOffset(thread_, JSTaggedValue(0));
                break;
            case JSType::JS_ARRAY_LIST:
                JSArrayList::Cast(*obj)->SetLength(thread_, JSTaggedValue(0));
                break;
            case JSType::JS_FUNCTION:
            case JSType::JS_GENERATOR_FUNCTION:
                JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>(obj));
                break;
            case JSType::JS_ASYNC_GENERATOR_FUNCTION:
                JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>(obj));
                break;
            case JSType::JS_PROXY_REVOC_FUNCTION:
                JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>(obj));
                JSProxyRevocFunction::Cast(*obj)->SetRevocableProxy(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_PROMISE_REACTIONS_FUNCTION:
                JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>(obj));
                JSPromiseReactionsFunction::Cast(*obj)->SetPromise(thread_, JSTaggedValue::Undefined());
                JSPromiseReactionsFunction::Cast(*obj)->SetAlreadyResolved(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_PROMISE_EXECUTOR_FUNCTION:
                JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>(obj));
                JSPromiseExecutorFunction::Cast(*obj)->SetCapability(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_PROMISE_ALL_RESOLVE_ELEMENT_FUNCTION:
                JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>(obj));
                JSPromiseAllResolveElementFunction::Cast(*obj)->SetIndex(thread_, JSTaggedValue::Undefined());
                JSPromiseAllResolveElementFunction::Cast(*obj)->SetValues(thread_, JSTaggedValue::Undefined());
                JSPromiseAllResolveElementFunction::Cast(*obj)->SetCapabilities(thread_, JSTaggedValue::Undefined());
                JSPromiseAllResolveElementFunction::Cast(*obj)->SetRemainingElements(thread_,
                                                                                     JSTaggedValue::Undefined());
                JSPromiseAllResolveElementFunction::Cast(*obj)->SetAlreadyCalled(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_INTL_BOUND_FUNCTION:
                JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>(obj));
                JSIntlBoundFunction::Cast(*obj)->SetNumberFormat(thread_, JSTaggedValue::Undefined());
                JSIntlBoundFunction::Cast(*obj)->SetDateTimeFormat(thread_, JSTaggedValue::Undefined());
                JSIntlBoundFunction::Cast(*obj)->SetCollator(thread_, JSTaggedValue::Undefined());
                break;
            case JSType::JS_BOUND_FUNCTION: {
                JSMethod *method =
                    vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::global::CallJsBoundFunction));
                JSBoundFunction::Cast(*obj)->SetMethod(method);
                JSBoundFunction::Cast(*obj)->SetBoundTarget(thread_, JSTaggedValue::Undefined());
                JSBoundFunction::Cast(*obj)->SetBoundThis(thread_, JSTaggedValue::Undefined());
                JSBoundFunction::Cast(*obj)->SetBoundArguments(thread_, JSTaggedValue::Undefined());
                break;
            }
            case JSType::JS_FORIN_ITERATOR:
            case JSType::JS_MAP_ITERATOR:
            case JSType::JS_REG_EXP_ITERATOR:
            case JSType::JS_SET_ITERATOR:
            case JSType::JS_ARRAY_ITERATOR:
            default:
                UNREACHABLE();
        }
    }
    return obj;
}

FreeObject *ObjectFactory::FillFreeObject([[maybe_unused]] uintptr_t address, [[maybe_unused]] size_t size,
                                          [[maybe_unused]] RemoveSlots remove_slots)
{
    ASSERT(size % JSTaggedValue::TaggedTypeSize() == 0);
    for (size_t offset = 0; offset < size; offset += JSTaggedValue::TaggedTypeSize()) {
        ObjectAccessor::SetDynPrimitive(thread_, reinterpret_cast<void *>(address), offset, JSTaggedValue::VALUE_HOLE);
    }
    FreeObject *object = nullptr;
    if (size >= FreeObject::GetTaggedSizeOffset() && size < FreeObject::SIZE) {
        object = reinterpret_cast<FreeObject *>(address);
        object->SetClassWithoutBarrier(free_object_with_one_field_class_);
        object->SetNext(nullptr);
    } else if (size >= FreeObject::SIZE) {
        object = reinterpret_cast<FreeObject *>(address);
        object->SetClassWithoutBarrier(free_object_with_two_field_class_);
        object->SetAvailable(size);
        object->SetNext(nullptr);
    } else if (size == FreeObject::GetTaggedNextOffset()) {
        object = reinterpret_cast<FreeObject *>(address);
        object->SetClassWithoutBarrier(free_object_with_none_field_class_);
    } else {
        LOG_ECMA(DEBUG) << "Fill free object size is smaller";
    }
    return object;
}

TaggedObject *ObjectFactory::NewDynObject(const JSHandle<JSHClass> &dynclass)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(*dynclass);
    uint32_t inobj_prop_count = dynclass->GetInlinedProperties();
    if (inobj_prop_count > 0) {
        InitializeExtraProperties(dynclass, header, inobj_prop_count);
    }
    return header;
}

TaggedObject *ObjectFactory::NewNonMovableDynObject(const JSHandle<JSHClass> &dynclass, int inobj_prop_count)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateNonMovableOrHugeObject(*dynclass);
    if (inobj_prop_count > 0) {
        InitializeExtraProperties(dynclass, header, inobj_prop_count);
    }
    return header;
}

void ObjectFactory::InitializeExtraProperties(const JSHandle<JSHClass> &dynclass, TaggedObject *obj,
                                              int inobj_prop_count)
{
    ASSERT(inobj_prop_count * JSTaggedValue::TaggedTypeSize() < dynclass->GetObjectSize());
    auto offset = dynclass->GetObjectSize();
    JSTaggedType init_val = JSTaggedValue::Undefined().GetRawData();
    for (int i = 0; i < inobj_prop_count; ++i) {
        offset -= JSTaggedValue::TaggedTypeSize();
        // Without barrier because 'object' is a just allocated object and it doesn't contain references.
        ObjectAccessor::SetDynValueWithoutBarrier(obj, offset, init_val);
    }
}

JSHandle<JSObject> ObjectFactory::OrdinaryNewJSObjectCreate(const JSHandle<JSTaggedValue> &proto)
{
    JSHandle<JSTaggedValue> proto_value(proto);
    JSHandle<JSHClass> proto_dyn = CreateDynClass<JSObject>(JSType::JS_OBJECT, proto_value);
    JSHandle<JSObject> new_obj = NewJSObject(proto_dyn);
    new_obj->GetJSHClass()->SetExtensible(true);
    return new_obj;
}

JSHandle<JSFunction> ObjectFactory::NewJSFunction(const JSHandle<GlobalEnv> &env, const void *native_func,
                                                  FunctionKind kind)
{
    JSMethod *target = vm_->GetMethodForNativeFunction(native_func);
    return NewJSFunction(env, target, kind);
}

JSHandle<JSFunction> ObjectFactory::NewJSFunction(const JSHandle<GlobalEnv> &env, JSMethod *method, FunctionKind kind)
{
    JSHandle<JSHClass> dynclass;
    if (kind == FunctionKind::BASE_CONSTRUCTOR) {
        dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithProto());
    } else if (JSFunction::IsConstructorKind(kind)) {
        dynclass = JSHandle<JSHClass>::Cast(env->GetConstructorFunctionClass());
    } else {
        dynclass = JSHandle<JSHClass>::Cast(env->GetNormalFunctionClass());
    }

    return NewJSFunctionByDynClass(method, dynclass, kind);
}

JSHandle<JSHClass> ObjectFactory::CreateFunctionClass(FunctionKind kind, uint32_t size, JSAccessorsMaskType native_mask,
                                                      JSType type, const JSHandle<JSTaggedValue> &prototype)
{
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSHClass> function_class = NewEcmaDynClass(size, type, prototype, HClass::IS_CALLABLE);
    function_class->GetHClass()->SetNativeFieldMask(native_mask);
    {
        // FunctionKind = BASE_CONSTRUCTOR
        if (JSFunction::IsConstructorKind(kind)) {
            function_class->SetConstructor(true);
        }
        function_class->SetExtensible(true);
    }

    uint32_t field_order = 0;
    ASSERT(JSFunction::LENGTH_INLINE_PROPERTY_INDEX == field_order);
    JSHandle<LayoutInfo> layout_info_handle = CreateLayoutInfo(JSFunction::LENGTH_OF_INLINE_PROPERTIES);
    {
        PropertyAttributes attributes = PropertyAttributes::Default(false, false, true);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order);
        layout_info_handle->AddKey(thread_, field_order, global_const->GetLengthString(), attributes);
        field_order++;
    }

    ASSERT(JSFunction::NAME_INLINE_PROPERTY_INDEX == field_order);
    // not set name in-object property on class which may have a name() method
    if (!JSFunction::IsClassConstructor(kind)) {
        PropertyAttributes attributes = PropertyAttributes::DefaultAccessor(false, false, true);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order);
        layout_info_handle->AddKey(thread_, field_order,
                                   thread_->GlobalConstants()->GetHandledNameString().GetTaggedValue(), attributes);
        field_order++;
    }

    if (JSFunction::HasPrototype(kind) && !JSFunction::IsClassConstructor(kind)) {
        ASSERT(JSFunction::PROTOTYPE_INLINE_PROPERTY_INDEX == field_order);
        PropertyAttributes attributes = PropertyAttributes::DefaultAccessor(true, false, false);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order);
        layout_info_handle->AddKey(thread_, field_order, global_const->GetPrototypeString(), attributes);
        field_order++;
    } else if (JSFunction::IsClassConstructor(kind)) {
        ASSERT(JSFunction::CLASS_PROTOTYPE_INLINE_PROPERTY_INDEX == field_order);
        PropertyAttributes attributes = PropertyAttributes::DefaultAccessor(false, false, false);
        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order);
        layout_info_handle->AddKey(thread_, field_order, global_const->GetPrototypeString(), attributes);
        field_order++;
    }

    {
        function_class->SetLayout(thread_, layout_info_handle);
        function_class->SetNumberOfProps(field_order);
    }
    return function_class;
}

JSHandle<JSFunction> ObjectFactory::NewJSFunctionByDynClass(JSMethod *method, const JSHandle<JSHClass> &clazz,
                                                            FunctionKind kind, panda::SpaceType space_type)
{
    JSHandle<JSFunction> function = space_type == panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT
                                        ? JSHandle<JSFunction>::Cast(NewNonMovableJSObject(clazz))
                                        : JSHandle<JSFunction>::Cast(NewJSObject(clazz));
    ASSERT(clazz->IsCallable());
    ASSERT(clazz->IsExtensible());
    JSFunction::InitializeJSFunction(thread_, function, kind);
    function->SetCallTarget(thread_, method);
    SlowRuntimeStub::NotifyInlineCache(thread_, *function, method);
    return function;
}

JSHandle<JSFunction> ObjectFactory::NewJSNativeErrorFunction(const JSHandle<GlobalEnv> &env, const void *native_func)
{
    JSMethod *target = vm_->GetMethodForNativeFunction(native_func);
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetNativeErrorFunctionClass());
    return NewJSFunctionByDynClass(target, dynclass, FunctionKind::BUILTIN_CONSTRUCTOR);
}

JSHandle<JSFunction> ObjectFactory::NewSpecificTypedArrayFunction(const JSHandle<GlobalEnv> &env,
                                                                  const void *native_func)
{
    JSMethod *target = vm_->GetMethodForNativeFunction(native_func);
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetSpecificTypedArrayFunctionClass());
    return NewJSFunctionByDynClass(target, dynclass, FunctionKind::BUILTIN_CONSTRUCTOR);
}

JSHandle<JSBoundFunction> ObjectFactory::NewJSBoundFunction(const JSHandle<JSFunctionBase> &target,
                                                            const JSHandle<JSTaggedValue> &bound_this,
                                                            const JSHandle<TaggedArray> &args)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto = env->GetFunctionPrototype();
    JSHandle<JSHClass> dynclass =
        CreateDynClass<JSBoundFunction>(JSType::JS_BOUND_FUNCTION, proto, HClass::IS_CALLABLE);

    JSHandle<JSBoundFunction> bundle_function = JSHandle<JSBoundFunction>::Cast(NewJSObject(dynclass));
    bundle_function->SetBoundTarget(thread_, target);
    bundle_function->SetBoundThis(thread_, bound_this);
    bundle_function->SetBoundArguments(thread_, args);
    if (target.GetTaggedValue().IsConstructor()) {
        bundle_function->SetConstructor(true);
    }
    JSMethod *method = vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::global::CallJsBoundFunction));
    bundle_function->SetCallTarget(thread_, method);
    return bundle_function;
}

JSHandle<JSIntlBoundFunction> ObjectFactory::NewJSIntlBoundFunction(const void *native_func, int function_length)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetJSIntlBoundFunctionClass());

    JSHandle<JSIntlBoundFunction> intl_bound_func = JSHandle<JSIntlBoundFunction>::Cast(NewJSObject(dynclass));
    JSMethod *method = vm_->GetMethodForNativeFunction(native_func);
    intl_bound_func->SetCallTarget(thread_, method);
    JSHandle<JSFunction> function = JSHandle<JSFunction>::Cast(intl_bound_func);
    JSFunction::InitializeJSFunction(thread_, function, FunctionKind::NORMAL_FUNCTION);
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(function_length));
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> empty_string = global_const->GetHandledEmptyString();
    JSHandle<JSTaggedValue> name_key = global_const->GetHandledNameString();
    PropertyDescriptor name_desc(thread_, empty_string, false, false, true);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>::Cast(function), name_key, name_desc);
    return intl_bound_func;
}

JSHandle<JSProxyRevocFunction> ObjectFactory::NewJSProxyRevocFunction(const JSHandle<JSProxy> &proxy,
                                                                      const void *native_func)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetProxyRevocFunctionClass());

    JSHandle<JSProxyRevocFunction> revoc_function = JSHandle<JSProxyRevocFunction>::Cast(NewJSObject(dynclass));
    revoc_function->SetRevocableProxy(thread_, proxy);

    JSMethod *target = vm_->GetMethodForNativeFunction(native_func);
    revoc_function->SetCallTarget(thread_, target);
    JSHandle<JSFunction> function = JSHandle<JSFunction>::Cast(revoc_function);
    JSFunction::InitializeJSFunction(thread_, function, FunctionKind::NORMAL_FUNCTION);
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(0));
    JSHandle<JSTaggedValue> empty_string = global_const->GetHandledEmptyString();
    JSHandle<JSTaggedValue> name_key = global_const->GetHandledNameString();
    PropertyDescriptor name_desc(thread_, empty_string, false, false, true);
    JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>::Cast(function), name_key, name_desc);
    return revoc_function;
}

JSHandle<JSAsyncAwaitStatusFunction> ObjectFactory::NewJSAsyncAwaitStatusFunction(const void *native_func)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncAwaitStatusFunctionClass());

    JSHandle<JSAsyncAwaitStatusFunction> await_function =
        JSHandle<JSAsyncAwaitStatusFunction>::Cast(NewJSObject(dynclass));

    JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>::Cast(await_function));
    JSMethod *target = vm_->GetMethodForNativeFunction(native_func);
    await_function->SetCallTarget(thread_, target);
    return await_function;
}

JSHandle<JSAsyncGeneratorResolveNextFunction> ObjectFactory::NewJSAsyncGeneratorResolveNextFunction(
    const void *native_func)
{
    NewObjectHook();
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncGeneratorResolveNextFunctionClass());

    JSHandle<JSAsyncGeneratorResolveNextFunction> resolve_next_function =
        JSHandle<JSAsyncGeneratorResolveNextFunction>::Cast(NewJSObject(dynclass));

    JSFunction::InitializeJSFunction(thread_, env, JSHandle<JSFunction>::Cast(resolve_next_function));
    JSMethod *target = vm_->GetMethodForNativeFunction(native_func);
    resolve_next_function->SetCallTarget(thread_, target);
    return resolve_next_function;
}

JSHandle<JSAsyncFromSyncIteratorValueUnwrapFunction> ObjectFactory::NewJSAsyncFromSyncIteratorValueUnwrapFunction(
    const void *native_func)
{
    NewObjectHook();
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncFromSyncIteratorValueUnwrapFunctionClass());

    JSHandle<JSAsyncFromSyncIteratorValueUnwrapFunction> async_from_sync_iterator_unwrap_function =
        JSHandle<JSAsyncFromSyncIteratorValueUnwrapFunction>::Cast(NewJSObject(dynclass));

    JSFunction::InitializeJSFunction(thread_, env,
                                     JSHandle<JSFunction>::Cast(async_from_sync_iterator_unwrap_function));
    JSMethod *target = vm_->GetMethodForNativeFunction(native_func);
    async_from_sync_iterator_unwrap_function->SetCallTarget(thread_, target);
    return async_from_sync_iterator_unwrap_function;
}

JSHandle<JSFunction> ObjectFactory::NewJSGeneratorFunction(JSMethod *method)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();

    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetGeneratorFunctionClass());
    JSHandle<JSFunction> generator_func = JSHandle<JSFunction>::Cast(NewJSObject(dynclass));
    JSFunction::InitializeJSFunction(thread_, generator_func, FunctionKind::GENERATOR_FUNCTION);
    generator_func->SetCallTarget(thread_, method);
    SlowRuntimeStub::NotifyInlineCache(thread_, *generator_func, method);
    return generator_func;
}

JSHandle<JSGeneratorObject> ObjectFactory::NewJSGeneratorObject(JSHandle<JSTaggedValue> generator_function)
{
    JSHandle<JSTaggedValue> proto(thread_, JSHandle<JSFunction>::Cast(generator_function)->GetProtoOrDynClass());
    if (!proto->IsECMAObject()) {
        JSHandle<GlobalEnv> realm_handle = JSObject::GetFunctionRealm(thread_, generator_function);
        proto = realm_handle->GetGeneratorPrototype();
    }
    JSHandle<JSHClass> dynclass = CreateDynClass<JSGeneratorObject>(JSType::JS_GENERATOR_OBJECT, proto);
    JSHandle<JSGeneratorObject> generator_object = JSHandle<JSGeneratorObject>::Cast(NewJSObject(dynclass));
    return generator_object;
}

JSHandle<JSFunction> ObjectFactory::NewJSAsyncGeneratorFunction(JSMethod *method)
{
    NewObjectHook();
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();

    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncGeneratorFunctionClass());
    JSHandle<JSFunction> generator_func = JSHandle<JSFunction>::Cast(NewJSObject(dynclass));
    JSFunction::InitializeJSFunction(thread_, env, generator_func, FunctionKind::ASYNC_GENERATOR_FUNCTION);
    generator_func->SetCallTarget(thread_, method);
    return generator_func;
}

JSHandle<JSAsyncGeneratorObject> ObjectFactory::NewJSAsyncGeneratorObject(
    JSHandle<JSTaggedValue> async_generator_function)
{
    NewObjectHook();
    JSHandle<JSTaggedValue> proto(thread_, JSHandle<JSFunction>::Cast(async_generator_function)->GetProtoOrDynClass());
    if (!proto->IsECMAObject()) {
        JSHandle<GlobalEnv> realm_handle = JSObject::GetFunctionRealm(thread_, async_generator_function);
        proto = realm_handle->GetAsyncGeneratorPrototype();
    }
    JSHandle<JSHClass> dynclass = CreateDynClass<JSAsyncGeneratorObject>(JSType::JS_ASYNC_GENERATOR_OBJECT, proto);
    JSHandle<JSAsyncGeneratorObject> asyncgenerator_object =
        JSHandle<JSAsyncGeneratorObject>::Cast(NewJSObject(dynclass));
    return asyncgenerator_object;
}

JSHandle<JSAsyncFromSyncIteratorObject> ObjectFactory::NewJSAsyncFromSyncIteratorObject()
{
    NewObjectHook();
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto = env->GetAsyncFromSyncIteratorPrototype();

    JSHandle<JSHClass> dynclass =
        CreateDynClass<JSAsyncFromSyncIteratorObject>(JSType::JS_ASYNC_FROM_SYNC_ITERATOR_OBJECT, proto);
    JSHandle<JSAsyncFromSyncIteratorObject> async_from_sync_iterator_object =
        JSHandle<JSAsyncFromSyncIteratorObject>::Cast(NewJSObject(dynclass));
    return async_from_sync_iterator_object;
}

JSHandle<JSAsyncFunction> ObjectFactory::NewAsyncFunction(JSMethod *method)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncFunctionClass());
    JSHandle<JSAsyncFunction> async_function = JSHandle<JSAsyncFunction>::Cast(NewJSObject(dynclass));
    JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>::Cast(async_function));
    async_function->SetCallTarget(thread_, method);
    SlowRuntimeStub::NotifyInlineCache(thread_, *async_function, method);
    return async_function;
}

JSHandle<JSAsyncFuncObject> ObjectFactory::NewJSAsyncFuncObject()
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto = env->GetInitialGenerator();
    JSHandle<JSHClass> dynclass = CreateDynClass<JSAsyncFuncObject>(JSType::JS_ASYNC_FUNC_OBJECT, proto);
    JSHandle<JSAsyncFuncObject> async_func_object = JSHandle<JSAsyncFuncObject>::Cast(NewJSObject(dynclass));
    return async_func_object;
}

JSHandle<CompletionRecord> ObjectFactory::NewCompletionRecord(uint8_t type, const JSHandle<JSTaggedValue> &value)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(completion_record_class_);
    JSHandle<CompletionRecord> obj(thread_, header);
    obj->SetType(thread_, JSTaggedValue(static_cast<int32_t>(type)));
    obj->SetValue(thread_, value.GetTaggedValue());
    return obj;
}

JSHandle<GeneratorContext> ObjectFactory::NewGeneratorContext()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(generator_context_class_);
    JSHandle<GeneratorContext> obj(thread_, header);
    obj->SetRegsArray(thread_, JSTaggedValue::Undefined());
    obj->SetMethod(thread_, JSTaggedValue::Undefined());
    obj->SetAcc(thread_, JSTaggedValue::Undefined());
    obj->SetNRegs(thread_, JSTaggedValue::Undefined());
    obj->SetBCOffset(thread_, JSTaggedValue::Undefined());
    obj->SetLexicalEnv(thread_, JSTaggedValue::Undefined());
    obj->SetGeneratorObject(thread_, JSTaggedValue::Undefined());
    obj->SetLexicalEnv(thread_, JSTaggedValue::Undefined());
    return obj;
}

JSHandle<JSPrimitiveRef> ObjectFactory::NewJSPrimitiveRef(const JSHandle<JSFunction> &function,
                                                          const JSHandle<JSTaggedValue> &object)
{
    JSHandle<JSPrimitiveRef> obj(NewJSObjectByConstructor(function, JSHandle<JSTaggedValue>(function)));
    obj->SetValue(thread_, object);

    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    if (function.GetTaggedValue() == env->GetStringFunction().GetTaggedValue()) {
        JSHandle<JSTaggedValue> length_str = global_const->GetHandledLengthString();

        int32_t length = EcmaString::Cast(object.GetTaggedValue().GetTaggedObject())->GetLength();
        PropertyDescriptor desc(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(length)), false, false, false);
        JSTaggedValue::DefinePropertyOrThrow(thread_, JSHandle<JSTaggedValue>(obj), length_str, desc);
    }

    return obj;
}

JSHandle<JSPrimitiveRef> ObjectFactory::NewJSPrimitiveRef(PrimitiveType type, const JSHandle<JSTaggedValue> &object)
{
    ObjectFactory *factory = vm_->GetFactory();
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> function;
    switch (type) {
        case PrimitiveType::PRIMITIVE_NUMBER:
            function = env->GetNumberFunction();
            break;
        case PrimitiveType::PRIMITIVE_STRING:
            function = env->GetStringFunction();
            break;
        case PrimitiveType::PRIMITIVE_SYMBOL:
            function = env->GetSymbolFunction();
            break;
        case PrimitiveType::PRIMITIVE_BOOLEAN:
            function = env->GetBooleanFunction();
            break;
        case PrimitiveType::PRIMITIVE_BIGINT:
            function = env->GetBigIntFunction();
            break;
        default:
            break;
    }
    JSHandle<JSFunction> func_handle(function);
    return factory->NewJSPrimitiveRef(func_handle, object);
}

JSHandle<JSPrimitiveRef> ObjectFactory::NewJSString(const JSHandle<JSTaggedValue> &str)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> string_func = env->GetStringFunction();

    JSHandle<JSPrimitiveRef> obj =
        JSHandle<JSPrimitiveRef>::Cast(NewJSObjectByConstructor(JSHandle<JSFunction>(string_func), string_func));
    obj->SetValue(thread_, str);
    return obj;
}

JSHandle<GlobalEnv> ObjectFactory::NewGlobalEnv(JSHClass *global_env_class)
{
    NewObjectHook();
    // Note: Global env must be allocated in non-movable heap, since its getters will directly return
    //       the offsets of the properties as the address of Handles.
    TaggedObject *header = heap_helper_.AllocateNonMovableOrHugeObject(global_env_class);
    InitObjectFields(header);
    return JSHandle<GlobalEnv>(thread_, GlobalEnv::Cast(header));
}

JSHandle<LexicalEnv> ObjectFactory::NewLexicalEnv(int num_slots)
{
    NewObjectHook();
    size_t size = LexicalEnv::ComputeSize(num_slots);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(env_class_, size);
    JSHandle<LexicalEnv> array(thread_, header);
    array->InitializeWithSpecialValue(JSTaggedValue::Undefined(), num_slots + LexicalEnv::RESERVED_ENV_LENGTH);
    return array;
}

JSHandle<JSSymbol> ObjectFactory::NewJSSymbol()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(symbol_class_);
    JSHandle<JSSymbol> obj(thread_, JSSymbol::Cast(header));
    obj->SetDescription(thread_, JSTaggedValue::Undefined());
    obj->SetFlags(thread_, JSTaggedValue(0));
    auto result = JSTaggedValue(static_cast<int>(SymbolTable::Hash(obj.GetTaggedValue())));
    obj->SetHashField(thread_, result);
    return obj;
}

JSHandle<JSSymbol> ObjectFactory::NewPrivateSymbol()
{
    JSHandle<JSSymbol> obj = NewJSSymbol();
    obj->SetPrivate(thread_);
    return obj;
}

JSHandle<JSSymbol> ObjectFactory::NewPrivateNameSymbol(const JSHandle<JSTaggedValue> &name)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(symbol_class_);
    JSHandle<JSSymbol> obj(thread_, JSSymbol::Cast(header));
    obj->SetFlags(thread_, JSTaggedValue(0));
    obj->SetPrivateNameSymbol(thread_);
    obj->SetDescription(thread_, name);
    auto result = JSTaggedValue(static_cast<int>(SymbolTable::Hash(name.GetTaggedValue())));
    obj->SetHashField(thread_, result);
    return obj;
}

JSHandle<JSSymbol> ObjectFactory::NewWellKnownSymbol(const JSHandle<JSTaggedValue> &name)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(symbol_class_);
    JSHandle<JSSymbol> obj(thread_, JSSymbol::Cast(header));
    obj->SetFlags(thread_, JSTaggedValue(0));
    obj->SetWellKnownSymbol(thread_);
    obj->SetDescription(thread_, name);
    auto result = JSTaggedValue(static_cast<int>(SymbolTable::Hash(name.GetTaggedValue())));
    obj->SetHashField(thread_, result);
    return obj;
}

JSHandle<JSSymbol> ObjectFactory::NewPublicSymbol(const JSHandle<JSTaggedValue> &name)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(symbol_class_);
    JSHandle<JSSymbol> obj(thread_, JSSymbol::Cast(header));
    obj->SetFlags(thread_, JSTaggedValue(0));
    obj->SetDescription(thread_, name);
    auto result = JSTaggedValue(static_cast<int>(SymbolTable::Hash(name.GetTaggedValue())));
    obj->SetHashField(thread_, result);
    return obj;
}

JSHandle<JSSymbol> ObjectFactory::NewSymbolWithTable(const JSHandle<JSTaggedValue> &name)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<SymbolTable> table_handle(env->GetRegisterSymbols());
    if (table_handle->ContainsKey(thread_, name.GetTaggedValue())) {
        JSTaggedValue obj_value = table_handle->GetSymbol(name.GetTaggedValue());
        return JSHandle<JSSymbol>(thread_, obj_value);
    }

    JSHandle<JSSymbol> obj = NewPublicSymbol(name);
    JSHandle<JSTaggedValue> value_handle(obj);
    JSHandle<JSTaggedValue> key_handle(name);
    JSHandle<SymbolTable> table = SymbolTable::Insert(thread_, table_handle, key_handle, value_handle);
    env->SetRegisterSymbols(thread_, table);
    return obj;
}

JSHandle<JSSymbol> ObjectFactory::NewPrivateNameSymbolWithChar(const char *description)
{
    JSHandle<EcmaString> string = NewFromString(description);
    return NewPrivateNameSymbol(JSHandle<JSTaggedValue>(string));
}

JSHandle<JSSymbol> ObjectFactory::NewWellKnownSymbolWithChar(const char *description)
{
    JSHandle<EcmaString> string = NewFromString(description);
    return NewWellKnownSymbol(JSHandle<JSTaggedValue>(string));
}

JSHandle<JSSymbol> ObjectFactory::NewPublicSymbolWithChar(const char *description)
{
    JSHandle<EcmaString> string = NewFromString(description);
    return NewPublicSymbol(JSHandle<JSTaggedValue>(string));
}

JSHandle<JSSymbol> ObjectFactory::NewSymbolWithTableWithChar(const char *description)
{
    JSHandle<EcmaString> string = NewFromString(description);
    return NewSymbolWithTable(JSHandle<JSTaggedValue>(string));
}

JSHandle<AccessorData> ObjectFactory::NewAccessorData(panda::SpaceType space_type)
{
    NewObjectHook();
    TaggedObject *header = space_type == panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT
                               ? heap_helper_.AllocateNonMovableOrHugeObject(accessor_data_class_)
                               : heap_helper_.AllocateYoungGenerationOrHugeObject(accessor_data_class_);
    JSHandle<AccessorData> acc(thread_, AccessorData::Cast(header));
    acc->SetGetter(thread_, JSTaggedValue::Undefined());
    acc->SetSetter(thread_, JSTaggedValue::Undefined());
    return acc;
}

JSHandle<AccessorData> ObjectFactory::NewInternalAccessor(void *setter, void *getter)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateNonMovableOrHugeObject(internal_accessor_class_);
    JSHandle<AccessorData> obj(thread_, AccessorData::Cast(header));
    if (setter != nullptr) {
        JSHandle<JSNativePointer> set_func = NewJSNativePointer(setter, nullptr, nullptr, true);
        obj->SetSetter(thread_, set_func.GetTaggedValue());
    } else {
        JSTaggedValue set_func = JSTaggedValue::Undefined();
        obj->SetSetter(thread_, set_func);
        ASSERT(!obj->HasSetter());
    }
    JSHandle<JSNativePointer> get_func = NewJSNativePointer(getter, nullptr, nullptr, true);
    obj->SetGetter(thread_, get_func);
    return obj;
}

JSHandle<PromiseCapability> ObjectFactory::NewPromiseCapability()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(capability_record_class_);
    JSHandle<PromiseCapability> obj(thread_, header);
    obj->SetPromise(thread_, JSTaggedValue::Undefined());
    obj->SetResolve(thread_, JSTaggedValue::Undefined());
    obj->SetReject(thread_, JSTaggedValue::Undefined());
    return obj;
}

JSHandle<PromiseReaction> ObjectFactory::NewPromiseReaction()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(reactions_record_class_);
    JSHandle<PromiseReaction> obj(thread_, header);
    obj->SetPromiseCapability(thread_, JSTaggedValue::Undefined());
    obj->SetHandler(thread_, JSTaggedValue::Undefined());
    obj->SetType(thread_, JSTaggedValue::Undefined());
    return obj;
}

JSHandle<PromiseIteratorRecord> ObjectFactory::NewPromiseIteratorRecord(const JSHandle<JSTaggedValue> &itor,
                                                                        const JSHandle<JSTaggedValue> &done)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(promise_iterator_record_class_);
    JSHandle<PromiseIteratorRecord> obj(thread_, header);
    obj->SetIterator(thread_, itor.GetTaggedValue());
    obj->SetDone(thread_, done.GetTaggedValue());
    return obj;
}

JSHandle<job::MicroJobQueue> ObjectFactory::NewMicroJobQueue()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateNonMovableOrHugeObject(micro_job_queue_class_);
    JSHandle<job::MicroJobQueue> obj(thread_, header);
    obj->SetPromiseJobQueue(thread_, GetEmptyTaggedQueue().GetTaggedValue());
    obj->SetScriptJobQueue(thread_, GetEmptyTaggedQueue().GetTaggedValue());
    return obj;
}

JSHandle<job::PendingJob> ObjectFactory::NewPendingJob(const JSHandle<JSFunction> &func,
                                                       const JSHandle<TaggedArray> &argv)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(pending_job_class_);
    JSHandle<job::PendingJob> obj(thread_, header);
    obj->SetJob(thread_, func.GetTaggedValue());
    obj->SetArguments(thread_, argv.GetTaggedValue());
    return obj;
}

JSHandle<JSFunctionExtraInfo> ObjectFactory::NewFunctionExtraInfo(const JSHandle<JSNativePointer> &call_back,
                                                                  const JSHandle<JSNativePointer> &vm,
                                                                  const JSHandle<JSNativePointer> &data)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(function_extra_info_);
    JSHandle<JSFunctionExtraInfo> obj(thread_, header);
    obj->SetCallback(thread_, call_back.GetTaggedValue());
    obj->SetVm(thread_, vm.GetTaggedValue());
    obj->SetData(thread_, data.GetTaggedValue());
    return obj;
}

JSHandle<JSProxy> ObjectFactory::NewJSProxy(const JSHandle<JSTaggedValue> &target,
                                            const JSHandle<JSTaggedValue> &handler)
{
    NewObjectHook();
    TaggedObject *header = nullptr;
    if (target->IsCallable()) {
        header = target->IsConstructor() ? heap_helper_.AllocateYoungGenerationOrHugeObject(js_proxy_construct_class_)
                                         : heap_helper_.AllocateYoungGenerationOrHugeObject(js_proxy_callable_class_);
    } else {
        header = heap_helper_.AllocateYoungGenerationOrHugeObject(js_proxy_ordinary_class_);
    }

    JSHandle<JSProxy> proxy(thread_, header);
    JSMethod *method = nullptr;
    if (target->IsCallable()) {
        method = vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::global::CallJsProxy));
        proxy->SetCallTarget(thread_, method);
    }
    proxy->SetMethod(method);

    proxy->SetTarget(thread_, target.GetTaggedValue());
    proxy->SetHandler(thread_, handler.GetTaggedValue());
    return proxy;
}

JSHandle<JSRealm> ObjectFactory::NewJSRealm()
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> realm_env_class = CreateDynClass<GlobalEnv>(hclass_class_, JSType::GLOBAL_ENV);
    JSHandle<GlobalEnv> realm_env_handle = NewGlobalEnv(*realm_env_class);

    ObtainRootClass(env);
    realm_env_handle->SetEmptyArray(thread_, NewEmptyArray());
    auto tagged_queue_handle = NewTaggedQueue(0);
    realm_env_handle->SetEmptyTaggedQueue(thread_, tagged_queue_handle);
    auto result = TemplateMap::Create(thread_);
    realm_env_handle->SetTemplateMap(thread_, result);

    builtins::Builtins builtins;
    builtins.Initialize(realm_env_handle, thread_);
    JSHandle<JSTaggedValue> proto_value = thread_->GlobalConstants()->GetHandledJSRealmClass();
    JSHandle<JSHClass> dyn_handle = CreateDynClass<JSRealm>(JSType::JS_REALM, proto_value);
    JSHandle<JSRealm> realm(NewJSObject(dyn_handle));
    realm->SetGlobalEnv(thread_, realm_env_handle.GetTaggedValue());

    JSHandle<JSTaggedValue> realm_obj = realm_env_handle->GetJSGlobalObject();
    JSHandle<JSTaggedValue> realmkey(thread_->GlobalConstants()->GetHandledGlobalString());
    PropertyDescriptor realm_desc(thread_, JSHandle<JSTaggedValue>::Cast(realm_obj), true, false, true);
    [[maybe_unused]] bool status =
        JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>::Cast(realm), realmkey, realm_desc);
    ASSERT_PRINT(status, "Realm defineOwnProperty failed");

    return realm;
}

JSHandle<TaggedArray> ObjectFactory::NewEmptyArray(bool weak)
{
    NewObjectHook();
    auto header =
        heap_helper_.AllocateNonMovableOrHugeObject(weak ? weak_array_class_ : array_class_, TaggedArray::SIZE);
    JSHandle<TaggedArray> array(thread_, header);
    array->SetLength(0);
    return array;
}

JSHandle<TaggedArray> ObjectFactory::NewTaggedArray(uint32_t length, JSTaggedValue init_val,
                                                    panda::SpaceType space_type)
{
    NewObjectHook();
    if (length == 0) {
        return EmptyArray();
    }

    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), length);
    TaggedObject *header = nullptr;
    if (space_type == panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT) {
        header = heap_helper_.AllocateNonMovableOrHugeObject(array_class_, size);
    } else {
        header = heap_helper_.AllocateYoungGenerationOrHugeObject(array_class_, size);
    }

    JSHandle<TaggedArray> array(thread_, header);
    // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue (issue #I5QOJX)
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    array->InitializeWithSpecialValue(init_val, length);
    return array;
}

JSHandle<TaggedArray> ObjectFactory::NewTaggedArray(uint32_t length, JSTaggedValue init_val)
{
    return NewTaggedArrayImpl(length, init_val, false);
}

JSHandle<TaggedArray> ObjectFactory::NewWeakTaggedArray(uint32_t length, JSTaggedValue init_val)
{
    return NewTaggedArrayImpl(length, init_val, true);
}

JSHandle<TaggedArray> ObjectFactory::NewTaggedArrayImpl(uint32_t length, JSTaggedValue init_val, bool weak)
{
    NewObjectHook();
    if (length == 0) {
        return weak ? EmptyWeakArray() : EmptyArray();
    }

    ASSERT(!init_val.IsHeapObject());
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), length);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(weak ? weak_array_class_ : array_class_, size);
    JSHandle<TaggedArray> array(thread_, header);
    // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue (issue #I5QOJX)
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    array->InitializeWithSpecialValue(init_val, length);
    return array;
}

JSHandle<TaggedArray> ObjectFactory::NewDictionaryArray(uint32_t length)
{
    NewObjectHook();
    ASSERT(length > 0);

    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), length);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(dictionary_class_, size);
    JSHandle<TaggedArray> array(thread_, header);
    array->InitializeWithSpecialValue(JSTaggedValue::Undefined(), length);

    return array;
}

JSHandle<TaggedArray> ObjectFactory::NewLinkedHashTable(ArraySizeT length, JSType table_type, bool is_weak)
{
    NewObjectHook();
    ASSERT(length > 0);
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), length);
    JSHClass *table_class = nullptr;
    if (table_type == JSType::LINKED_HASH_MAP) {
        table_class = is_weak ? weak_linked_hash_map_class_ : linked_hash_map_class_;
    } else if (table_type == JSType::LINKED_HASH_SET) {
        table_class = is_weak ? weak_linked_hash_set_class_ : linked_hash_set_class_;
    } else {
        table_class = array_class_;
    }
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(table_class, size);
    JSHandle<TaggedArray> array(thread_, header);
    array->InitializeWithSpecialValue(JSTaggedValue::Hole(), length);
    return array;
}

JSHandle<TaggedArray> ObjectFactory::ExtendArray(const JSHandle<TaggedArray> &old, uint32_t length,
                                                 JSTaggedValue init_val)
{
    bool weak = old->GetClass()->IsWeakContainer();
    ASSERT(length > old->GetLength());
    NewObjectHook();
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), length);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(weak ? weak_array_class_ : array_class_, size);
    JSHandle<TaggedArray> new_array(thread_, header);
    new_array->SetLength(length);

    uint32_t old_length = old->GetLength();
    for (uint32_t i = 0; i < old_length; i++) {
        JSTaggedValue value = old->Get(i);
        new_array->Set(thread_, i, value);
    }

    for (uint32_t i = old_length; i < length; i++) {
        new_array->Set(thread_, i, init_val);
    }

    return new_array;
}

JSHandle<TaggedArray> ObjectFactory::CopyPartArray(const JSHandle<TaggedArray> &old, uint32_t start, uint32_t end)
{
    ASSERT(start <= end);
    ASSERT(end <= old->GetLength());

    bool weak = old->GetClass()->IsWeakContainer();
    uint32_t new_length = end - start;
    if (new_length == 0) {
        return weak ? EmptyWeakArray() : EmptyArray();
    }

    NewObjectHook();
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), new_length);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(weak ? weak_array_class_ : array_class_, size);
    JSHandle<TaggedArray> new_array(thread_, header);
    new_array->SetLength(new_length);

    for (uint32_t i = 0; i < new_length; i++) {
        JSTaggedValue value = old->Get(i + start);
        if (value.IsHole()) {
            break;
        }
        new_array->Set(thread_, i, value);
    }
    return new_array;
}

JSHandle<TaggedArray> ObjectFactory::CopyArray(const JSHandle<TaggedArray> &old, [[maybe_unused]] uint32_t old_length,
                                               uint32_t new_length, JSTaggedValue init_val)
{
    bool weak = old->GetClass()->IsWeakContainer();
    if (new_length == 0) {
        return weak ? EmptyWeakArray() : EmptyArray();
    }
    if (new_length > old_length) {
        return ExtendArray(old, new_length, init_val);
    }

    NewObjectHook();
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), new_length);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(weak ? weak_array_class_ : array_class_, size);
    JSHandle<TaggedArray> new_array(thread_, header);
    new_array->SetLength(new_length);

    for (uint32_t i = 0; i < new_length; i++) {
        JSTaggedValue value = old->Get(i);
        new_array->Set(thread_, i, value);
    }

    return new_array;
}

JSHandle<LayoutInfo> ObjectFactory::CreateLayoutInfo(int properties, JSTaggedValue init_val)
{
    int array_length = LayoutInfo::ComputeArrayLength(LayoutInfo::ComputeGrowCapacity(properties));
    JSHandle<LayoutInfo> layout_info_handle = JSHandle<LayoutInfo>::Cast(NewTaggedArray(array_length, init_val));
    layout_info_handle->SetNumberOfElements(thread_, 0);
    return layout_info_handle;
}

JSHandle<LayoutInfo> ObjectFactory::ExtendLayoutInfo(const JSHandle<LayoutInfo> &old, int properties,
                                                     JSTaggedValue init_val)
{
    ASSERT(properties > old->NumberOfElements());
    int array_length = LayoutInfo::ComputeArrayLength(LayoutInfo::ComputeGrowCapacity(properties));
    return JSHandle<LayoutInfo>(ExtendArray(JSHandle<TaggedArray>(old), array_length, init_val));
}

JSHandle<LayoutInfo> ObjectFactory::CopyLayoutInfo(const JSHandle<LayoutInfo> &old)
{
    int new_length = old->GetLength();
    return JSHandle<LayoutInfo>(CopyArray(JSHandle<TaggedArray>::Cast(old), new_length, new_length));
}

JSHandle<LayoutInfo> ObjectFactory::CopyAndReSort(const JSHandle<LayoutInfo> &old, int end, int capacity)
{
    ASSERT(capacity >= end);
    JSHandle<LayoutInfo> new_arr = CreateLayoutInfo(capacity);
    Span<struct Properties> sp(old->GetProperties(), end);
    int i = 0;
    for (; i < end; i++) {
        new_arr->AddKey(thread_, i, sp[i].key, PropertyAttributes(sp[i].attr));
    }

    return new_arr;
}

JSHandle<ConstantPool> ObjectFactory::NewConstantPool(uint32_t capacity)
{
    NewObjectHook();
    if (capacity == 0) {
        return JSHandle<ConstantPool>::Cast(EmptyArray());
    }
    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), capacity);
    auto header = heap_helper_.AllocateNonMovableOrHugeObject(array_class_, size);
    JSHandle<ConstantPool> array(thread_, header);
    array->InitializeWithSpecialValue(JSTaggedValue::Undefined(), capacity);
    return array;
}

JSHandle<Program> ObjectFactory::NewProgram()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateNonMovableOrHugeObject(program_class_);
    JSHandle<Program> p(thread_, header);
    p->SetLocation(thread_, JSTaggedValue::Undefined());
    p->SetConstantPool(thread_, JSTaggedValue::Undefined());
    p->SetMainFunction(thread_, JSTaggedValue::Undefined());
    p->SetMethodsData(nullptr);
    return p;
}

JSHandle<EcmaModule> ObjectFactory::NewEmptyEcmaModule()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(ecma_module_class_);
    JSHandle<EcmaModule> module(thread_, header);
    module->SetNameDictionary(thread_, JSTaggedValue::Undefined());
    return module;
}

JSHandle<EcmaString> ObjectFactory::GetEmptyString() const
{
    return JSHandle<EcmaString>(thread_->GlobalConstants()->GetHandledEmptyString());
}

JSHandle<TaggedArray> ObjectFactory::EmptyArray() const
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    return JSHandle<TaggedArray>(env->GetEmptyArray());
}

JSHandle<TaggedArray> ObjectFactory::EmptyWeakArray() const
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    return JSHandle<TaggedArray>(env->GetEmptyWeakArray());
}

JSHandle<ObjectWrapper> ObjectFactory::NewObjectWrapper(const JSHandle<JSTaggedValue> &value)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(object_wrapper_class_);
    ObjectWrapper *obj = ObjectWrapper::Cast(header);
    obj->SetValue(thread_, value);
    return JSHandle<ObjectWrapper>(thread_, obj);
}

JSHandle<EcmaString> ObjectFactory::GetStringFromStringTable(const uint8_t *utf8_data, uint32_t utf8_len,
                                                             bool can_be_compress, panda::SpaceType space_type) const
{
    NewObjectHook();
    if (utf8_len == 0) {
        return GetEmptyString();
    }
    auto string_table = vm_->GetEcmaStringTable();
    return JSHandle<EcmaString>(thread_,
                                string_table->GetOrInternString(utf8_data, utf8_len, can_be_compress, space_type));
}

JSHandle<EcmaString> ObjectFactory::GetStringFromStringTable(const uint16_t *utf16_data, uint32_t utf16_len,
                                                             bool can_be_compress, panda::SpaceType space_type) const
{
    NewObjectHook();
    if (utf16_len == 0) {
        return GetEmptyString();
    }
    auto string_table = vm_->GetEcmaStringTable();
    return JSHandle<EcmaString>(thread_,
                                string_table->GetOrInternString(utf16_data, utf16_len, can_be_compress, space_type));
}

JSHandle<EcmaString> ObjectFactory::GetStringFromStringTable(EcmaString *string) const
{
    ASSERT(string != nullptr);
    if (string->GetLength() == 0) {
        return GetEmptyString();
    }
    auto string_table = vm_->GetEcmaStringTable();
    return JSHandle<EcmaString>(thread_, string_table->GetOrInternString(string));
}

// NB! don't do special case for C0 80, it means '\u0000', so don't convert to UTF-8
EcmaString *ObjectFactory::GetRawStringFromStringTable(const uint8_t *mutf8_data, uint32_t utf16_len,
                                                       bool can_be_compressed, panda::SpaceType space_type) const
{
    NewObjectHook();
    if (UNLIKELY(utf16_len == 0)) {
        return *GetEmptyString();
    }

    if (can_be_compressed) {
        return EcmaString::Cast(vm_->GetEcmaStringTable()->GetOrInternString(mutf8_data, utf16_len, true, space_type));
    }

    PandaVector<uint16_t> utf16_data(utf16_len);
    auto len = utf::ConvertRegionMUtf8ToUtf16(mutf8_data, utf16_data.data(), utf::Mutf8Size(mutf8_data), utf16_len, 0);
    return EcmaString::Cast(vm_->GetEcmaStringTable()->GetOrInternString(utf16_data.data(), len, false, space_type));
}

JSHandle<PropertyBox> ObjectFactory::NewPropertyBox(const JSHandle<JSTaggedValue> &value)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateNonMovableOrHugeObject(property_box_class_);
    JSHandle<PropertyBox> box(thread_, header);
    box->SetValue(thread_, value);
    return box;
}

JSHandle<ProtoChangeMarker> ObjectFactory::NewProtoChangeMarker()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(proto_change_marker_class_);
    JSHandle<ProtoChangeMarker> marker(thread_, header);
    marker->SetHasChanged(false);
    return marker;
}

JSHandle<ProtoChangeDetails> ObjectFactory::NewProtoChangeDetails()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(proto_change_details_class_);
    JSHandle<ProtoChangeDetails> proto_info(thread_, header);
    proto_info->SetChangeListener(thread_, JSTaggedValue(0));
    proto_info->SetRegisterIndex(thread_, JSTaggedValue(ProtoChangeDetails::UNREGISTERED));
    return proto_info;
}

JSHandle<ProfileTypeInfo> ObjectFactory::NewProfileTypeInfo(uint32_t length)
{
    NewObjectHook();
    ASSERT(length > 0);

    size_t size = TaggedArray::ComputeSize(JSTaggedValue::TaggedTypeSize(), length);
    auto header = heap_helper_.AllocateNonMovableOrHugeObject(weak_array_class_, size);
    JSHandle<ProfileTypeInfo> array(thread_, header);
    array->InitializeWithSpecialValue(JSTaggedValue::Undefined(), length);

    return array;
}

// static
void ObjectFactory::NewObjectHook() const {}

JSHandle<TaggedQueue> ObjectFactory::NewTaggedQueue(uint32_t length)
{
    uint32_t queue_length = TaggedQueue::QueueToArrayIndex(length);
    auto queue = JSHandle<TaggedQueue>::Cast(NewTaggedArray(queue_length, JSTaggedValue::Hole()));
    queue->SetStart(thread_, JSTaggedValue(0));  // equal to 0 when add 1.
    queue->SetEnd(thread_, JSTaggedValue(0));
    queue->SetCapacity(thread_, JSTaggedValue(length));

    return queue;
}

JSHandle<TaggedQueue> ObjectFactory::GetEmptyTaggedQueue() const
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    return JSHandle<TaggedQueue>(env->GetEmptyTaggedQueue());
}

JSHandle<JSWeakRef> ObjectFactory::NewWeakRef(const JSHandle<JSObject> &referent)
{
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(weak_ref_class_);
    if (header == nullptr) {
        return JSHandle<JSWeakRef>();
    }
    JSHandle<JSWeakRef> ref(thread_, header);
    ref->SetReferent(referent.GetTaggedValue());
    return ref;
}

JSHandle<JSSetIterator> ObjectFactory::NewJSSetIterator(const JSHandle<JSSet> &set, IterationKind kind)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto_value = env->GetSetIteratorPrototype();
    JSHandle<JSHClass> dyn_handle = CreateDynClass<JSSetIterator>(JSType::JS_SET_ITERATOR, proto_value);
    JSHandle<JSSetIterator> iter(NewJSObject(dyn_handle));
    iter->GetJSHClass()->SetExtensible(true);
    iter->SetIteratedSet(thread_, set->GetLinkedSet());
    iter->SetNextIndex(thread_, JSTaggedValue(0));
    iter->SetIterationKind(thread_, JSTaggedValue(static_cast<int>(kind)));
    return iter;
}

JSHandle<JSRegExpIterator> ObjectFactory::NewJSRegExpIterator(const JSHandle<JSTaggedValue> &matcher,
                                                              const JSHandle<EcmaString> &input_str, bool global,
                                                              bool full_unicode)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto_value = env->GetRegExpIteratorPrototype();
    JSHandle<JSHClass> dyn_handle = CreateDynClass<JSRegExpIterator>(JSType::JS_REG_EXP_ITERATOR, proto_value);
    JSHandle<JSRegExpIterator> iter(NewJSObject(dyn_handle));
    iter->GetJSHClass()->SetExtensible(true);
    iter->SetIteratingRegExp(thread_, matcher.GetTaggedValue());
    iter->SetIteratedString(thread_, input_str.GetTaggedValue());
    iter->SetGlobal(global);
    iter->SetUnicode(full_unicode);
    iter->SetDone(false);
    return iter;
}

JSHandle<JSMapIterator> ObjectFactory::NewJSMapIterator(const JSHandle<JSMap> &map, IterationKind kind)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto_value = env->GetMapIteratorPrototype();
    JSHandle<JSHClass> dyn_handle = CreateDynClass<JSMapIterator>(JSType::JS_MAP_ITERATOR, proto_value);
    JSHandle<JSMapIterator> iter(NewJSObject(dyn_handle));
    iter->GetJSHClass()->SetExtensible(true);
    iter->SetIteratedMap(thread_, map->GetLinkedMap());
    iter->SetNextIndex(thread_, JSTaggedValue(0));
    iter->SetIterationKind(thread_, JSTaggedValue(static_cast<int>(kind)));
    return iter;
}

JSHandle<JSArrayIterator> ObjectFactory::NewJSArrayIterator(const JSHandle<JSObject> &array, IterationKind kind)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto_value = env->GetArrayIteratorPrototype();
    JSHandle<JSHClass> dyn_handle = CreateDynClass<JSArrayIterator>(JSType::JS_ARRAY_ITERATOR, proto_value);
    JSHandle<JSArrayIterator> iter(NewJSObject(dyn_handle));
    iter->GetJSHClass()->SetExtensible(true);
    iter->SetIteratedArray(thread_, array);
    iter->SetNextIndex(thread_, JSTaggedValue(0));
    iter->SetIterationKind(thread_, JSTaggedValue(static_cast<int>(kind)));
    return iter;
}

JSHandle<JSPromiseReactionsFunction> ObjectFactory::CreateJSPromiseReactionsFunction(const void *native_func)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetPromiseReactionFunctionClass());

    JSHandle<JSPromiseReactionsFunction> reactions_function =
        JSHandle<JSPromiseReactionsFunction>::Cast(NewJSObject(dynclass));
    reactions_function->SetPromise(thread_, JSTaggedValue::Hole());
    reactions_function->SetAlreadyResolved(thread_, JSTaggedValue::Hole());
    JSMethod *method = vm_->GetMethodForNativeFunction(native_func);
    reactions_function->SetCallTarget(thread_, method);
    JSHandle<JSFunction> function = JSHandle<JSFunction>::Cast(reactions_function);
    JSFunction::InitializeJSFunction(thread_, function);
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(1));
    return reactions_function;
}

JSHandle<JSPromiseExecutorFunction> ObjectFactory::CreateJSPromiseExecutorFunction(const void *native_func)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetPromiseExecutorFunctionClass());
    JSHandle<JSPromiseExecutorFunction> executor_function =
        JSHandle<JSPromiseExecutorFunction>::Cast(NewJSObject(dynclass));
    executor_function->SetCapability(thread_, JSTaggedValue::Hole());
    JSMethod *method = vm_->GetMethodForNativeFunction(native_func);
    executor_function->SetCallTarget(thread_, method);
    executor_function->SetCapability(thread_, JSTaggedValue::Undefined());
    JSHandle<JSFunction> function = JSHandle<JSFunction>::Cast(executor_function);
    JSFunction::InitializeJSFunction(thread_, function, FunctionKind::NORMAL_FUNCTION);
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(builtins::FunctionLength::TWO));
    return executor_function;
}

JSHandle<JSFunction> ObjectFactory::CreateBuiltinFunction(EcmaEntrypoint steps, uint8_t length,
                                                          JSHandle<JSTaggedValue> name, JSHandle<JSTaggedValue> proto,
                                                          JSHandle<JSTaggedValue> prefix)
{
    JSHandle<JSFunction> function = NewJSFunction(vm_->GetGlobalEnv(), reinterpret_cast<void *>(steps));
    function->SetFunctionPrototype(thread_, proto.GetTaggedValue());
    function->GetJSHClass()->SetExtensible(true);
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(length));
    JSHandle<JSFunctionBase> base_function(function);
    JSFunction::SetFunctionName(thread_, base_function, name, prefix);

    return function;
}

JSHandle<JSPromiseAllResolveElementFunction> ObjectFactory::NewJSPromiseAllResolveElementFunction(
    const void *native_func)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetPromiseAllResolveElementFunctionClass());
    JSHandle<JSPromiseAllResolveElementFunction> function =
        JSHandle<JSPromiseAllResolveElementFunction>::Cast(NewJSObject(dynclass));
    JSFunction::InitializeJSFunction(thread_, JSHandle<JSFunction>::Cast(function));
    JSMethod *method = vm_->GetMethodForNativeFunction(native_func);
    function->SetCallTarget(thread_, method);
    JSFunction::SetFunctionLength(thread_, JSHandle<JSFunction>::Cast(function), JSTaggedValue(1));
    return function;
}

EcmaString *ObjectFactory::InternString(const JSHandle<JSTaggedValue> &key)
{
    EcmaString *str = EcmaString::Cast(key->GetTaggedObject());
    if (str->IsInternString()) {
        return str;
    }

    EcmaStringTable *string_table = vm_->GetEcmaStringTable();
    return string_table->GetOrInternString(str);
}

JSHandle<TransitionHandler> ObjectFactory::NewTransitionHandler()
{
    NewObjectHook();
    TransitionHandler *handler =
        TransitionHandler::Cast(heap_helper_.AllocateYoungGenerationOrHugeObject(transition_handler_class_));
    return JSHandle<TransitionHandler>(thread_, handler);
}

JSHandle<PrototypeHandler> ObjectFactory::NewPrototypeHandler()
{
    NewObjectHook();
    PrototypeHandler *header =
        PrototypeHandler::Cast(heap_helper_.AllocateYoungGenerationOrHugeObject(prototype_handler_class_));
    JSHandle<PrototypeHandler> handler(thread_, header);
    handler->SetHandlerInfo(thread_, JSTaggedValue::Undefined());
    handler->SetProtoCell(thread_, JSTaggedValue::Undefined());
    handler->SetHolder(thread_, JSTaggedValue::Undefined());
    return handler;
}

JSHandle<PromiseRecord> ObjectFactory::NewPromiseRecord()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(promise_record_class_);
    JSHandle<PromiseRecord> obj(thread_, header);
    obj->SetValue(thread_, JSTaggedValue::Undefined());
    return obj;
}

JSHandle<ResolvingFunctionsRecord> ObjectFactory::NewResolvingFunctionsRecord()
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateYoungGenerationOrHugeObject(promise_resolving_functions_record_);
    JSHandle<ResolvingFunctionsRecord> obj(thread_, header);
    obj->SetResolveFunction(thread_, JSTaggedValue::Undefined());
    obj->SetRejectFunction(thread_, JSTaggedValue::Undefined());
    return obj;
}

JSHandle<JSHClass> ObjectFactory::CreateObjectClass(const JSHandle<TaggedArray> &properties, size_t length)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto = env->GetObjectFunctionPrototype();

    uint32_t field_order = 0;
    JSMutableHandle<JSTaggedValue> key(thread_, JSTaggedValue::Undefined());
    JSHandle<LayoutInfo> layout_info_handle = CreateLayoutInfo(length);
    while (field_order < length) {
        key.Update(properties->Get(field_order * 2));  // 2: Meaning to double
        ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");
        PropertyAttributes attributes = PropertyAttributes::Default();

        if (properties->Get(field_order * 2 + 1).IsAccessor()) {  // 2: Meaning to double
            attributes.SetIsAccessor(true);
        }

        attributes.SetIsInlinedProps(true);
        attributes.SetRepresentation(Representation::MIXED);
        attributes.SetOffset(field_order);
        layout_info_handle->AddKey(thread_, field_order, key.GetTaggedValue(), attributes);
        field_order++;
    }
    ASSERT(field_order <= PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES);
    JSHandle<JSHClass> obj_class = CreateDynClass<JSObject>(JSType::JS_OBJECT, field_order);
    obj_class->SetPrototype(thread_, proto.GetTaggedValue());
    {
        obj_class->SetExtensible(true);
        obj_class->SetIsLiteral(true);
        obj_class->SetLayout(thread_, layout_info_handle);
        obj_class->SetNumberOfProps(field_order);
    }
    return obj_class;
}

JSHandle<JSObject> ObjectFactory::NewJSObjectByClass(const JSHandle<TaggedArray> &properties, size_t length,
                                                     panda::SpaceType space_type)
{
    JSHandle<JSHClass> dynclass = CreateObjectClass(properties, length);
    JSHandle<JSObject> obj = space_type == panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT
                                 ? NewNonMovableJSObject(dynclass)
                                 : NewJSObject(dynclass);
    return obj;
}

JSHandle<JSObject> ObjectFactory::NewEmptyJSObject(panda::SpaceType space_type)
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSTaggedValue> builtin_obj = env->GetObjectFunction();
    return NewJSObjectByConstructor(JSHandle<JSFunction>(builtin_obj), builtin_obj, space_type);
}

EcmaString *ObjectFactory::ResolveString(uint32_t string_id)
{
    auto *caller = panda::StackWalker::Create(thread_).GetMethod();
    auto *pf = caller->GetPandaFile();
    auto id = panda_file::File::EntityId(string_id);
    auto found_str = pf->GetStringData(id);

    return GetRawStringFromStringTable(found_str.data, found_str.utf16_length, found_str.is_ascii);
}

JSHandle<ClassInfoExtractor> ObjectFactory::NewClassInfoExtractor(JSMethod *ctor_method)
{
    NewObjectHook();
    TaggedObject *header = heap_helper_.AllocateNonMovableOrHugeObject(class_info_extractor_h_class_);
    JSHandle<ClassInfoExtractor> obj(thread_, header);
    obj->InitializeBitField();
    obj->SetConstructorMethod(ctor_method);
    JSHandle<TaggedArray> empty_array = EmptyArray();
    obj->SetPrototypeHClass(thread_, JSTaggedValue::Undefined());
    obj->SetNonStaticKeys(thread_, empty_array, SKIP_BARRIER);
    obj->SetNonStaticProperties(thread_, empty_array, SKIP_BARRIER);
    obj->SetNonStaticElements(thread_, empty_array, SKIP_BARRIER);
    obj->SetConstructorHClass(thread_, JSTaggedValue::Undefined());
    obj->SetStaticKeys(thread_, empty_array, SKIP_BARRIER);
    obj->SetStaticProperties(thread_, empty_array, SKIP_BARRIER);
    obj->SetStaticElements(thread_, empty_array, SKIP_BARRIER);
    return obj;
}

// ----------------------------------- new string ----------------------------------------
JSHandle<EcmaString> ObjectFactory::NewFromString(const PandaString &data)
{
    auto utf8_data = reinterpret_cast<const uint8_t *>(data.c_str());
    bool can_be_compress = EcmaString::CanBeCompressed(utf8_data, data.length());
    return GetStringFromStringTable(utf8_data, data.length(), can_be_compress);
}

JSHandle<EcmaString> ObjectFactory::NewFromCanBeCompressString(const PandaString &data)
{
    auto utf8_data = reinterpret_cast<const uint8_t *>(data.c_str());
    ASSERT(EcmaString::CanBeCompressed(utf8_data, data.length()));
    return GetStringFromStringTable(utf8_data, data.length(), true);
}

JSHandle<EcmaString> ObjectFactory::NewFromStdString(const std::string &data)
{
    auto utf8_data = reinterpret_cast<const uint8_t *>(data.c_str());
    bool can_be_compress = EcmaString::CanBeCompressed(utf8_data, data.length());
    return GetStringFromStringTable(utf8_data, data.size(), can_be_compress);
}

JSHandle<EcmaString> ObjectFactory::NewFromStdStringUnCheck(const std::string &data, bool can_be_compress,
                                                            panda::SpaceType space_type)
{
    auto utf8_data = reinterpret_cast<const uint8_t *>(data.c_str());
    return GetStringFromStringTable(utf8_data, data.size(), can_be_compress, space_type);
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf8(const uint8_t *utf8_data, uint32_t utf8_len)
{
    bool can_be_compress = EcmaString::CanBeCompressed(utf8_data, utf8_len);
    return GetStringFromStringTable(utf8_data, utf8_len, can_be_compress);
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf8UnCheck(const uint8_t *utf8_data, uint32_t utf8_len,
                                                       bool can_be_compress)
{
    return GetStringFromStringTable(utf8_data, utf8_len, can_be_compress);
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf16(const uint16_t *utf16_data, uint32_t utf16_len)
{
    bool can_be_compress = EcmaString::CanBeCompressed(utf16_data, utf16_len);
    return GetStringFromStringTable(utf16_data, utf16_len, can_be_compress);
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf16UnCheck(const uint16_t *utf16_data, uint32_t utf16_len,
                                                        bool can_be_compress)
{
    return GetStringFromStringTable(utf16_data, utf16_len, can_be_compress);
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf8Literal(const uint8_t *utf8_data, uint32_t utf8_len)
{
    NewObjectHook();
    bool can_be_compress = EcmaString::CanBeCompressed(utf8_data, utf8_len);
    return JSHandle<EcmaString>(thread_, EcmaString::CreateFromUtf8(utf8_data, utf8_len, vm_, can_be_compress));
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf8LiteralUnCheck(const uint8_t *utf8_data, uint32_t utf8_len,
                                                              bool can_be_compress)
{
    NewObjectHook();
    return JSHandle<EcmaString>(thread_, EcmaString::CreateFromUtf8(utf8_data, utf8_len, vm_, can_be_compress));
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf16Literal(const uint16_t *utf16_data, uint32_t utf16_len)
{
    NewObjectHook();
    bool can_be_compress = EcmaString::CanBeCompressed(utf16_data, utf16_len);
    return JSHandle<EcmaString>(thread_, EcmaString::CreateFromUtf16(utf16_data, utf16_len, vm_, can_be_compress));
}

JSHandle<EcmaString> ObjectFactory::NewFromUtf16LiteralUnCheck(const uint16_t *utf16_data, uint32_t utf16_len,
                                                               bool can_be_compress)
{
    NewObjectHook();
    return JSHandle<EcmaString>(thread_, EcmaString::CreateFromUtf16(utf16_data, utf16_len, vm_, can_be_compress));
}

JSHandle<EcmaString> ObjectFactory::NewFromString(EcmaString *str)
{
    return GetStringFromStringTable(str);
}

JSHandle<EcmaString> ObjectFactory::ConcatFromString(const JSHandle<EcmaString> &prefix,
                                                     const JSHandle<EcmaString> &suffix)
{
    EcmaString *concat_string = EcmaString::Concat(prefix, suffix, vm_);
    return GetStringFromStringTable(concat_string);
}
}  // namespace panda::ecmascript
