/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tagged_dictionary.h"
#include "tagged_hash_table-inl.h"

namespace panda::ecmascript {
int NameDictionary::Hash(const JSTaggedValue &key)
{
    if (key.IsHeapObject()) {
        JSTaggedValue js_key(key);
        if (js_key.IsSymbol()) {
            auto symbol_string = JSSymbol::Cast(key.GetTaggedObject());
            return static_cast<JSTaggedNumber>(symbol_string->GetHashField()).GetInt();
        }
        if (js_key.IsString()) {
            auto key_string = reinterpret_cast<EcmaString *>(key.GetTaggedObject());
            return key_string->GetHashcode();
        }
    }
    // key must be object
    UNREACHABLE();
}

bool NameDictionary::IsMatch(const JSTaggedValue &key, const JSTaggedValue &other)
{
    return key == other;
}

void NameDictionary::GetAllKeys(const JSThread *thread, int offset, TaggedArray *key_array) const
{
    int array_index = 0;
    int size = Size();
    PandaVector<std::pair<JSTaggedValue, PropertyAttributes>> sort_arr;
    for (int hash_index = 0; hash_index < size; hash_index++) {
        JSTaggedValue key = GetKey(hash_index);
        if (!key.IsUndefined() && !key.IsHole()) {
            PropertyAttributes attr = GetAttributes(hash_index);
            std::pair<JSTaggedValue, PropertyAttributes> pair(key, attr);
            sort_arr.push_back(pair);
        }
    }
    std::sort(sort_arr.begin(), sort_arr.end(), CompKey);
    for (const auto &entry : sort_arr) {
        key_array->Set(thread, array_index + offset, entry.first);
        array_index++;
    }
}

void NameDictionary::GetAllEnumKeys(const JSThread *thread, int offset, TaggedArray *key_array, uint32_t *keys) const
{
    uint32_t array_index = 0;
    int size = Size();
    PandaVector<std::pair<JSTaggedValue, PropertyAttributes>> sort_arr;
    for (int hash_index = 0; hash_index < size; hash_index++) {
        JSTaggedValue key = GetKey(hash_index);
        if (key.IsString()) {
            PropertyAttributes attr = GetAttributes(hash_index);
            if (attr.IsEnumerable()) {
                std::pair<JSTaggedValue, PropertyAttributes> pair(key, attr);
                sort_arr.push_back(pair);
            }
        }
    }
    std::sort(sort_arr.begin(), sort_arr.end(), CompKey);
    for (auto entry : sort_arr) {
        key_array->Set(thread, array_index + offset, entry.first);
        array_index++;
    }
    *keys += array_index;
}

JSHandle<NameDictionary> NameDictionary::Create(const JSThread *thread, int number_of_elements)
{
    return OrderHashTableT::Create(thread, number_of_elements);
}

PropertyAttributes NameDictionary::GetAttributes(int entry) const
{
    int index = GetEntryIndex(entry) + ENTRY_DETAILS_INDEX;
    return PropertyAttributes(Get(index).GetInt());
}

void NameDictionary::SetAttributes(const JSThread *thread, int entry, const PropertyAttributes &meta_data)
{
    int index = GetEntryIndex(entry) + ENTRY_DETAILS_INDEX;
    Set(thread, index, meta_data.GetTaggedValue());
}

void NameDictionary::SetEntry(const JSThread *thread, int entry, const JSTaggedValue &key, const JSTaggedValue &value,
                              const PropertyAttributes &meta_data)
{
    SetKey(thread, entry, key);
    SetValue(thread, entry, value);
    SetAttributes(thread, entry, meta_data);
}

void NameDictionary::UpdateValueAndAttributes(const JSThread *thread, int entry, const JSTaggedValue &value,
                                              const PropertyAttributes &meta_data)
{
    SetValue(thread, entry, value);
    SetAttributes(thread, entry, meta_data);
}

void NameDictionary::UpdateValue(const JSThread *thread, int entry, const JSTaggedValue &value)
{
    SetValue(thread, entry, value);
}

void NameDictionary::ClearEntry(const JSThread *thread, int entry)
{
    JSTaggedValue hole = JSTaggedValue::Hole();
    PropertyAttributes meta_data;
    SetEntry(thread, entry, hole, hole, meta_data);
}

int NumberDictionary::Hash(const JSTaggedValue &key)
{
    if (key.IsInt()) {
        int key_value = key.GetInt();
        return GetHash32(reinterpret_cast<uint8_t *>(&key_value), sizeof(key_value) / sizeof(uint8_t));
    }
    // key must be object
    UNREACHABLE();
}

bool NumberDictionary::IsMatch(const JSTaggedValue &key, const JSTaggedValue &other)
{
    if (key.IsHole() || key.IsUndefined()) {
        return false;
    }

    if (key.IsInt()) {
        if (other.IsInt()) {
            return key.GetInt() == other.GetInt();
        }
        return false;
    }
    // key must be integer
    UNREACHABLE();
}

void NumberDictionary::GetAllKeys(const JSThread *thread, const JSHandle<NumberDictionary> &obj, int offset,
                                  const JSHandle<TaggedArray> &key_array)
{
    ASSERT_PRINT(offset + obj->EntriesCount() <= static_cast<int>(key_array->GetLength()),
                 "key_array capacity is not enough for dictionary");
    int array_index = 0;
    int size = obj->Size();
    PandaVector<JSTaggedValue> sort_arr;
    for (int hash_index = 0; hash_index < size; hash_index++) {
        JSTaggedValue key = obj->GetKey(hash_index);
        if (!key.IsUndefined() && !key.IsHole()) {
            sort_arr.push_back(JSTaggedValue(static_cast<uint32_t>(key.GetInt())));
        }
    }
    std::sort(sort_arr.begin(), sort_arr.end(), CompKey);
    for (auto entry : sort_arr) {
        JSHandle<JSTaggedValue> key_handle(thread, entry);
        JSHandle<EcmaString> str = JSTaggedValue::ToString(const_cast<JSThread *>(thread), key_handle);
        ASSERT_NO_ABRUPT_COMPLETION(thread);
        key_array->Set(thread, array_index + offset, str.GetTaggedValue());
        array_index++;
    }
}

void NumberDictionary::GetAllEnumKeys(const JSThread *thread, const JSHandle<NumberDictionary> &obj, int offset,
                                      const JSHandle<TaggedArray> &key_array, uint32_t *keys)
{
    ASSERT_PRINT(offset + obj->EntriesCount() <= static_cast<int>(key_array->GetLength()),
                 "key_array capacity is not enough for dictionary");
    uint32_t array_index = 0;
    int size = obj->Size();
    PandaVector<JSTaggedValue> sort_arr;
    for (int hash_index = 0; hash_index < size; hash_index++) {
        JSTaggedValue key = obj->GetKey(hash_index);
        if (!key.IsUndefined() && !key.IsHole()) {
            PropertyAttributes attr = obj->GetAttributes(hash_index);
            if (attr.IsEnumerable()) {
                sort_arr.push_back(JSTaggedValue(static_cast<uint32_t>(key.GetInt())));
            }
        }
    }
    std::sort(sort_arr.begin(), sort_arr.end(), CompKey);
    for (auto entry : sort_arr) {
        JSHandle<JSTaggedValue> key_handle(thread, entry);
        JSHandle<EcmaString> str = JSTaggedValue::ToString(const_cast<JSThread *>(thread), key_handle);
        ASSERT_NO_ABRUPT_COMPLETION(thread);
        key_array->Set(thread, array_index + offset, str.GetTaggedValue());
        array_index++;
    }
    *keys += array_index;
}

JSHandle<NumberDictionary> NumberDictionary::Create(const JSThread *thread, int number_of_elements)
{
    return OrderHashTableT::Create(thread, number_of_elements);
}

PropertyAttributes NumberDictionary::GetAttributes(int entry) const
{
    int index = GetEntryIndex(entry) + ENTRY_DETAILS_INDEX;
    return PropertyAttributes(Get(index).GetInt());
}

void NumberDictionary::SetAttributes(const JSThread *thread, int entry, const PropertyAttributes &meta_data)
{
    int index = GetEntryIndex(entry) + ENTRY_DETAILS_INDEX;
    Set(thread, index, meta_data.GetTaggedValue());
}

void NumberDictionary::SetEntry(const JSThread *thread, int entry, const JSTaggedValue &key, const JSTaggedValue &value,
                                const PropertyAttributes &meta_data)
{
    SetKey(thread, entry, key);
    SetValue(thread, entry, value);
    SetAttributes(thread, entry, meta_data);
}

void NumberDictionary::UpdateValueAndAttributes(const JSThread *thread, int entry, const JSTaggedValue &value,
                                                const PropertyAttributes &meta_data)
{
    SetValue(thread, entry, value);
    SetAttributes(thread, entry, meta_data);
}

void NumberDictionary::UpdateValue(const JSThread *thread, int entry, const JSTaggedValue &value)
{
    SetValue(thread, entry, value);
}

void NumberDictionary::ClearEntry(const JSThread *thread, int entry)
{
    JSTaggedValue hole = JSTaggedValue::Hole();
    PropertyAttributes meta_data;
    SetEntry(thread, entry, hole, hole, meta_data);
}
}  // namespace panda::ecmascript
