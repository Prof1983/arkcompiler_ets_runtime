/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_collator.h"

#include "unicode/udata.h"

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/mem/ecma_string.h"

namespace panda::ecmascript {
// NOLINTNEXTLINE (readability-identifier-naming, fuchsia-statically-constructed-objects)
const std::string JSCollator::uIcuDataColl = U_ICUDATA_NAME U_TREE_SEPARATOR_STRING "coll";
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::map<std::string, CaseFirstOption> JSCollator::CASE_FIRST_MAP = {
    {"upper", CaseFirstOption::UPPER}, {"lower", CaseFirstOption::LOWER}, {"false", CaseFirstOption::FALSE_OPTION}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::map<CaseFirstOption, UColAttributeValue> JSCollator::U_COL_ATTRIBUTE_VALUE_MAP = {
    {CaseFirstOption::UPPER, UCOL_UPPER_FIRST},
    {CaseFirstOption::LOWER, UCOL_LOWER_FIRST},
    {CaseFirstOption::FALSE_OPTION, UCOL_OFF},
    {CaseFirstOption::UNDEFINED, UCOL_OFF}};

JSHandle<TaggedArray> JSCollator::GetAvailableLocales(JSThread *thread)
{
    const char *key = nullptr;
    const char *path = JSCollator::uIcuDataColl.c_str();
    JSHandle<TaggedArray> available_locales = JSLocale::GetAvailableLocales(thread, key, path);
    return available_locales;
}

/* static */
void JSCollator::SetIcuCollator(JSThread *thread, const JSHandle<JSCollator> &collator, icu::Collator *icu_collator,
                                const DeleteEntryPoint &callback)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    ASSERT(icu_collator != nullptr);
    JSTaggedValue data = collator->GetIcuField();
    if (data.IsJSNativePointer()) {
        JSNativePointer *native = JSNativePointer::Cast(data.GetTaggedObject());
        native->ResetExternalPointer(icu_collator);
        return;
    }
    JSHandle<JSNativePointer> pointer = factory->NewJSNativePointer(icu_collator);
    pointer->SetDeleter(callback);
    collator->SetIcuField(thread, pointer.GetTaggedValue());
    ecma_vm->PushToArrayDataList(*pointer);
}

// NOLINTNEXTLINE(readability-function-size), CODECHECK-NOLINTNEXTLINE(C_RULE_ID_FUNCTION_SIZE)
JSHandle<JSCollator> JSCollator::InitializeCollator(JSThread *thread, const JSHandle<JSCollator> &collator,
                                                    const JSHandle<JSTaggedValue> &locales,
                                                    const JSHandle<JSTaggedValue> &options)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 1. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);

    // 2. If options is undefined, then
    //      a. Let options be ObjectCreate(null).
    // 3. Else,
    //      a. Let options be ? ToObject(options).
    JSHandle<JSObject> options_object;
    if (options->IsUndefined()) {
        JSHandle<JSTaggedValue> null_value = global_const->GetHandledNull();
        options_object = factory->OrdinaryNewJSObjectCreate(null_value);
    } else {
        options_object = JSTaggedValue::ToObject(thread, options);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);
    }
    // 4. Let usage be ? GetOption(options, "usage", "string", « "sort", "search" », "sort").
    auto usage = JSLocale::GetOptionOfString<UsageOption>(thread, options_object, global_const->GetHandledUsageString(),
                                                          {UsageOption::SORT, UsageOption::SEARCH}, {"sort", "search"},
                                                          UsageOption::SORT);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);
    JSHandle<JSTaggedValue> usage_value(thread, JSTaggedValue(static_cast<int>(usage)));
    collator->SetUsage(thread, usage_value);

    // 5. Let matcher be ? GetOption(options, "localeMatcher", "string", « "lookup", "best fit" », "best fit").
    auto matcher = JSLocale::GetOptionOfString<LocaleMatcherOption>(
        thread, options_object, global_const->GetHandledLocaleMatcherString(),
        {LocaleMatcherOption::LOOKUP, LocaleMatcherOption::BEST_FIT}, {"lookup", "best fit"},
        LocaleMatcherOption::BEST_FIT);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);

    // 6. Let collation be ? GetOption(options, "collation", "string", undefined, undefined).
    // 7. If collation is not undefined, then
    //    a. If collation does not match the Unicode Locale Identifier type nonterminal, throw a RangeError exception.
    JSHandle<JSTaggedValue> collation =
        JSLocale::GetOption(thread, options_object, global_const->GetHandledCollationString(), OptionType::STRING,
                            global_const->GetHandledUndefined(), global_const->GetHandledUndefined());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);
    collator->SetCollation(thread, collation);
    std::string collation_str;
    if (!collation->IsUndefined()) {
        JSHandle<EcmaString> collation_ecma_str = JSHandle<EcmaString>::Cast(collation);
        collation_str = JSLocale::ConvertToStdString(collation_ecma_str);
        if (!JSLocale::IsWellAlphaNumList(collation_str)) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "invalid collation", collator);
        }
    }

    // 8. Let numeric be ? GetOption(options, "numeric", "boolean", undefined, undefined).
    bool numeric = false;
    bool found_numeric =
        JSLocale::GetOptionOfBool(thread, options_object, global_const->GetHandledNumericString(), false, &numeric);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);
    JSHandle<JSTaggedValue> numeric_value(thread, JSTaggedValue(numeric));
    collator->SetNumeric(thread, numeric_value);

    // 14. Let caseFirst be ? GetOption(options, "caseFirst", "string", « "upper", "lower", "false" », undefined).
    auto case_first = JSLocale::GetOptionOfString<CaseFirstOption>(
        thread, options_object, global_const->GetHandledCaseFirstString(),
        {CaseFirstOption::UPPER, CaseFirstOption::LOWER, CaseFirstOption::FALSE_OPTION}, {"upper", "lower", "false"},
        CaseFirstOption::UNDEFINED);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);
    JSHandle<JSTaggedValue> case_first_value(thread, JSTaggedValue(static_cast<int>(case_first)));
    collator->SetCaseFirst(thread, case_first_value);

    // 16. Let relevantExtensionKeys be %Collator%.[[RelevantExtensionKeys]].
    std::set<std::string> relevant_extension_keys = {"co", "kn", "kf"};

    // 17. Let r be ResolveLocale(%Collator%.[[AvailableLocales]], requestedLocales, opt,
    //     %Collator%.[[RelevantExtensionKeys]], localeData).
    JSHandle<TaggedArray> available_locales;
    if (requested_locales->GetLength() == 0) {
        available_locales = factory->EmptyArray();
    } else {
        available_locales = GetAvailableLocales(thread);
    }
    ResolvedLocale r =
        JSLocale::ResolveLocale(thread, available_locales, requested_locales, matcher, relevant_extension_keys);
    icu::Locale icu_locale = r.locale_data;
    JSHandle<EcmaString> locale_str = JSLocale::ToLanguageTag(thread, icu_locale);
    collator->SetLocale(thread, locale_str.GetTaggedValue());
    ASSERT_PRINT(!icu_locale.isBogus(), "icuLocale is bogus");

    // If collation is undefined iterate RelevantExtensionKeys to find "co"
    //  if found, set ICU collator UnicodeKeyword to iterator->second
    UErrorCode status = U_ZERO_ERROR;
    if (!collation->IsUndefined()) {
        auto extension_iter = r.extensions.find("co");
        if (extension_iter != r.extensions.end() && extension_iter->second != collation_str) {
            icu_locale.setUnicodeKeywordValue("co", nullptr, status);
            ASSERT_PRINT(U_SUCCESS(status), "icuLocale set co failed");
        }
    }

    // If usage is serach set co-serach to icu locale key word value
    // Eles set collation string to icu locale key word value
    if (usage == UsageOption::SEARCH) {
        icu_locale.setUnicodeKeywordValue("co", "search", status);
        ASSERT(U_SUCCESS(status));
    } else {
        if (!collation_str.empty() && JSLocale::IsWellCollation(icu_locale, collation_str)) {
            icu_locale.setUnicodeKeywordValue("co", collation_str, status);
            ASSERT(U_SUCCESS(status));
        }
    }

    std::unique_ptr<icu::Collator> icu_collator(icu::Collator::createInstance(icu_locale, status));
    if (U_FAILURE(status) || icu_collator == nullptr) {  // NOLINT(readability-implicit-bool-conversion)
        status = U_ZERO_ERROR;
        icu::Locale locale_name(icu_locale.getBaseName());
        icu_collator.reset(icu::Collator::createInstance(locale_name, status));
        if (U_FAILURE(status) || icu_collator == nullptr) {  // NOLINT(readability-implicit-bool-conversion)
            THROW_RANGE_ERROR_AND_RETURN(thread, "invalid collation", collator);
        }
    }
    ASSERT(U_SUCCESS(status));
    icu::Locale collator_locale(icu_collator->getLocale(ULOC_VALID_LOCALE, status));

    icu_collator->setAttribute(UCOL_NORMALIZATION_MODE, UCOL_ON, status);
    ASSERT(U_SUCCESS(status));

    // If numeric is found set ICU collator UCOL_NUMERIC_COLLATION to numeric
    // Else iterate RelevantExtensionKeys to find "kn"
    //  if found, set ICU collator UCOL_NUMERIC_COLLATION to iterator->second
    status = U_ZERO_ERROR;
    if (found_numeric) {
        ASSERT(icu_collator.get() != nullptr);
        icu_collator->setAttribute(UCOL_NUMERIC_COLLATION, numeric ? UCOL_ON : UCOL_OFF, status);
        ASSERT(U_SUCCESS(status));
    } else {
        auto extension_iter = r.extensions.find("kn");
        if (extension_iter != r.extensions.end()) {
            ASSERT(icu_collator.get() != nullptr);
            bool found = (extension_iter->second == "true");
            JSHandle<JSTaggedValue> is_numeric(thread, JSTaggedValue(found));
            collator->SetNumeric(thread, is_numeric);
            icu_collator->setAttribute(UCOL_NUMERIC_COLLATION, found ? UCOL_ON : UCOL_OFF, status);
            ASSERT(U_SUCCESS(status));
        }
    }

    // If caseFirst is not undefined set ICU collator UColAttributeValue to caseFirst
    // Else iterate RelevantExtensionKeys to find "kf"
    //  if found, set ICU collator UColAttributeValue to iterator->second
    status = U_ZERO_ERROR;
    if (case_first != CaseFirstOption::UNDEFINED) {
        ASSERT(icu_collator.get() != nullptr);
        icu_collator->setAttribute(UCOL_CASE_FIRST, OptionToUColAttribute(case_first), status);
        ASSERT(U_SUCCESS(status));
    } else {
        auto extension_iter = r.extensions.find("kf");
        if (extension_iter != r.extensions.end()) {
            ASSERT(icu_collator.get() != nullptr);
            auto map_iter = CASE_FIRST_MAP.find(extension_iter->second);
            if (map_iter != CASE_FIRST_MAP.end()) {
                icu_collator->setAttribute(UCOL_CASE_FIRST, OptionToUColAttribute(map_iter->second), status);
                JSHandle<JSTaggedValue> value(thread, JSTaggedValue(static_cast<int>(map_iter->second)));
                collator->SetCaseFirst(thread, value);
            } else {
                icu_collator->setAttribute(UCOL_CASE_FIRST, OptionToUColAttribute(CaseFirstOption::UNDEFINED), status);
            }
            ASSERT(U_SUCCESS(status));
        }
    }

    // 24. Let sensitivity be ? GetOption(options, "sensitivity", "string", « "base", "accent", "case", "variant" »,
    //     undefined).
    auto sensitivity = JSLocale::GetOptionOfString<SensitivityOption>(
        thread, options_object, global_const->GetHandledSensitivityString(),
        {SensitivityOption::BASE, SensitivityOption::ACCENT, SensitivityOption::CASE, SensitivityOption::VARIANT},
        {"base", "accent", "case", "variant"}, SensitivityOption::UNDEFINED);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSCollator, thread);
    // 25. If sensitivity is undefined, then
    //     a. If usage is "sort", then
    //        i. Let sensitivity be "variant".
    if (sensitivity == SensitivityOption::UNDEFINED) {
        if (usage == UsageOption::SORT) {
            sensitivity = SensitivityOption::VARIANT;
        }
    }
    JSHandle<JSTaggedValue> sensitivity_value(thread, JSTaggedValue(static_cast<int>(sensitivity)));
    collator->SetSensitivity(thread, sensitivity_value);

    // Trans SensitivityOption to Icu strength option
    switch (sensitivity) {
        case SensitivityOption::BASE:
            icu_collator->setStrength(icu::Collator::PRIMARY);
            break;
        case SensitivityOption::ACCENT:
            icu_collator->setStrength(icu::Collator::SECONDARY);
            break;
        case SensitivityOption::CASE:
            icu_collator->setStrength(icu::Collator::PRIMARY);
            icu_collator->setAttribute(UCOL_CASE_LEVEL, UCOL_ON, status);
            break;
        case SensitivityOption::VARIANT:
            icu_collator->setStrength(icu::Collator::TERTIARY);
            break;
        case SensitivityOption::UNDEFINED:
            break;
        case SensitivityOption::EXCEPTION:
            UNREACHABLE();
    }

    // 27. Let ignorePunctuation be ? GetOption(options, "ignorePunctuation", "boolean", undefined, false).
    // 28. Set collator.[[IgnorePunctuation]] to ignorePunctuation.
    bool ignore_punctuation = false;
    JSLocale::GetOptionOfBool(thread, options_object, global_const->GetHandledIgnorePunctuationString(), false,
                              &ignore_punctuation);
    JSHandle<JSTaggedValue> ignore_punctuation_value(thread, JSTaggedValue(ignore_punctuation));
    collator->SetIgnorePunctuation(thread, ignore_punctuation_value);
    if (ignore_punctuation) {
        status = U_ZERO_ERROR;
        icu_collator->setAttribute(UCOL_ALTERNATE_HANDLING, UCOL_SHIFTED, status);
        ASSERT(U_SUCCESS(status));
    }

    SetIcuCollator(thread, collator, icu_collator.release(), JSCollator::FreeIcuCollator);
    collator->SetBoundCompare(thread, JSTaggedValue::Undefined());
    // 29. Return collator.
    return collator;
}

UColAttributeValue JSCollator::OptionToUColAttribute(CaseFirstOption case_first_option)
{
    auto iter = U_COL_ATTRIBUTE_VALUE_MAP.find(case_first_option);
    if (iter != U_COL_ATTRIBUTE_VALUE_MAP.end()) {
        return iter->second;
    }
    UNREACHABLE();
}

JSHandle<JSTaggedValue> OptionsToEcmaString(JSThread *thread, UsageOption usage)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (usage) {
        case UsageOption::SORT:
            result.Update(global_const->GetSortString());
            break;
        case UsageOption::SEARCH:
            result.Update(global_const->GetSearchString());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionsToEcmaString(JSThread *thread, SensitivityOption sensitivity)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (sensitivity) {
        case SensitivityOption::BASE:
            result.Update(global_const->GetBaseString());
            break;
        case SensitivityOption::ACCENT:
            result.Update(global_const->GetAccentString());
            break;
        case SensitivityOption::CASE:
            result.Update(global_const->GetCaseString());
            break;
        case SensitivityOption::VARIANT:
            result.Update(global_const->GetVariantString());
            break;
        case SensitivityOption::UNDEFINED:
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionsToEcmaString(JSThread *thread, CaseFirstOption case_first)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (case_first) {
        case CaseFirstOption::UPPER:
            result.Update(global_const->GetUpperString());
            break;
        case CaseFirstOption::LOWER:
            result.Update(global_const->GetLowerString());
            break;
        case CaseFirstOption::FALSE_OPTION:
            result.Update(global_const->GetFalseString());
            break;
        case CaseFirstOption::UNDEFINED:
            result.Update(global_const->GetUpperString());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

// 11.3.4 Intl.Collator.prototype.resolvedOptions ()
JSHandle<JSObject> JSCollator::ResolvedOptions(JSThread *thread, const JSHandle<JSCollator> &collator)
{
    auto ecma_vm = thread->GetEcmaVM();
    auto global_const = thread->GlobalConstants();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> ctor = env->GetObjectFunction();
    JSHandle<JSFunction> fun_ctor = JSHandle<JSFunction>::Cast(env->GetObjectFunction());
    JSHandle<JSObject> options(factory->NewJSObjectByConstructor(fun_ctor, ctor));

    // [[Locale]]
    JSHandle<JSTaggedValue> property = global_const->GetHandledLocaleString();
    JSHandle<JSTaggedValue> locale(thread, collator->GetLocale());
    JSObject::CreateDataPropertyOrThrow(thread, options, property, locale);

    // [[Usage]]
    UsageOption usage_option = static_cast<UsageOption>(collator->GetUsage().GetNumber());
    JSHandle<JSTaggedValue> usage_value = OptionsToEcmaString(thread, usage_option);
    JSObject::CreateDataProperty(thread, options, global_const->GetHandledUsageString(), usage_value);

    // [[Sensitivity]]
    auto sentivity_option = static_cast<SensitivityOption>(collator->GetSensitivity().GetNumber());
    JSHandle<JSTaggedValue> sensitivity_value = OptionsToEcmaString(thread, sentivity_option);
    JSObject::CreateDataProperty(thread, options, global_const->GetHandledSensitivityString(), sensitivity_value);

    // [[IgnorePunctuation]]
    JSHandle<JSTaggedValue> ignore_punctuation_value(thread, collator->GetIgnorePunctuation());
    JSObject::CreateDataProperty(thread, options, global_const->GetHandledIgnorePunctuationString(),
                                 ignore_punctuation_value);

    // [[Collation]]
    JSMutableHandle<JSTaggedValue> collation_value(thread, collator->GetCollation());
    if (collation_value->IsUndefined()) {
        collation_value.Update(global_const->GetDefaultString());
    }
    JSObject::CreateDataProperty(thread, options, global_const->GetHandledCollationString(), collation_value);

    // [[Numeric]]
    JSHandle<JSTaggedValue> numeric_value(thread, collator->GetNumeric());
    JSObject::CreateDataProperty(thread, options, global_const->GetHandledNumericString(), numeric_value);

    // [[CaseFirst]]
    CaseFirstOption case_first_option = static_cast<CaseFirstOption>(collator->GetCaseFirst().GetNumber());
    // In Ecma402 spec, caseFirst is an optional property so we set it to Upper when input is undefined
    // the requirement maybe change in the future
    JSHandle<JSTaggedValue> case_first_value = OptionsToEcmaString(thread, case_first_option);
    JSObject::CreateDataProperty(thread, options, global_const->GetHandledCaseFirstString(), case_first_value);
    return options;
}

icu::UnicodeString EcmaStringToUString(const JSHandle<EcmaString> &string)
{
    std::string std_string(ConvertToPandaString(*string, StringConvertedUsage::LOGICOPERATION));
    icu::StringPiece sp(std_string);
    icu::UnicodeString u_string = icu::UnicodeString::fromUTF8(sp);
    return u_string;
}

JSTaggedValue JSCollator::CompareStrings(const icu::Collator *icu_collator, const JSHandle<EcmaString> &string1,
                                         const JSHandle<EcmaString> &string2)
{
    icu::UnicodeString u_string1 = EcmaStringToUString(string1);
    icu::UnicodeString u_string2 = EcmaStringToUString(string2);

    UCollationResult result;
    UErrorCode status = U_ZERO_ERROR;
    result = icu_collator->compare(u_string1, u_string2, status);
    ASSERT(U_SUCCESS(status));

    return JSTaggedValue(result);
}
}  // namespace panda::ecmascript
