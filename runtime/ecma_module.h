/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_ECMA_MODULE_H
#define ECMASCRIPT_ECMA_MODULE_H

#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/mem/ecma_string.h"

namespace panda::ecmascript {
class EcmaVm;

// Forward declaration
class EcmaModule : public ECMAObject {
public:
    static EcmaModule *Cast(ObjectHeader *object)
    {
        return static_cast<EcmaModule *>(object);
    }

    JSHandle<JSTaggedValue> GetItem(const JSThread *thread, JSHandle<JSTaggedValue> item_name);

    static void AddItem(const JSThread *thread, JSHandle<EcmaModule> module, JSHandle<JSTaggedValue> item_name,
                        JSHandle<JSTaggedValue> item_value);

    static void RemoveItem(const JSThread *thread, JSHandle<EcmaModule> module, JSHandle<JSTaggedValue> item_name);

    void DebugPrint(const JSThread *thread, const PandaString &caller);

    static constexpr uint32_t DEAULT_DICTIONART_CAPACITY = 4;

    ACCESSORS_BASE(ECMAObject)
    ACCESSORS(0, NameDictionary)
    ACCESSORS_FINISH(1)

    DECL_DUMP()

protected:
    static void CopyModuleInternal(const JSThread *thread, JSHandle<EcmaModule> dst_module,
                                   JSHandle<EcmaModule> src_module);

    friend class ModuleManager;
};

class ModuleManager {
public:
    explicit ModuleManager(EcmaVM *vm);
    ~ModuleManager() = default;

    void AddModule(JSHandle<JSTaggedValue> module_name, JSHandle<JSTaggedValue> module);

    void RemoveModule(JSHandle<JSTaggedValue> module_name);

    JSHandle<JSTaggedValue> GetModule(const JSThread *thread, JSHandle<JSTaggedValue> module_name);

    PandaString GenerateModuleFullPath(const std::string &current_path_file, const PandaString &relative_file);

    const PandaString &GetCurrentExportModuleName();

    const PandaString &GetPrevExportModuleName();

    void SetCurrentExportModuleName(const std::string_view &module_file);

    void RestoreCurrentExportModuleName();

    void AddModuleItem(const JSThread *thread, JSHandle<JSTaggedValue> item_name, JSHandle<JSTaggedValue> value);

    JSHandle<JSTaggedValue> GetModuleItem(const JSThread *thread, JSHandle<JSTaggedValue> module,
                                          JSHandle<JSTaggedValue> item_name);

    void CopyModule(const JSThread *thread, JSHandle<JSTaggedValue> src);

    void DebugPrint(const JSThread *thread, const PandaString &caller);

private:
    static constexpr uint32_t DEAULT_DICTIONART_CAPACITY = 4;

    NO_COPY_SEMANTIC(ModuleManager);
    NO_MOVE_SEMANTIC(ModuleManager);

    EcmaVM *vm_ {nullptr};
    JSTaggedValue ecma_modules_ {JSTaggedValue::Hole()};
    std::vector<PandaString> module_names_ {DEAULT_DICTIONART_CAPACITY};

    friend class EcmaVM;
};
}  // namespace panda::ecmascript

#endif
