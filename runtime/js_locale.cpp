/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "base/string_helper.h"
#include "ecma_macros.h"
#include "ecma_vm.h"
#include "global_env.h"
#include "js_locale.h"
#include "object_factory.h"

#include "unicode/localebuilder.h"
#include "unicode/localematcher.h"

namespace panda::ecmascript {
// 6.2.2 IsStructurallyValidLanguageTag( locale )
bool JSLocale::IsStructurallyValidLanguageTag(const JSHandle<EcmaString> &tag)
{
    std::string tag_collection = ConvertToStdString(tag);
    std::vector<std::string> containers;
    std::string substring;
    std::set<std::string> unique_subtags;
    size_t address = 1;
    for (auto it = tag_collection.begin(); it != tag_collection.end(); it++) {
        if (*it != '-' && it != tag_collection.end() - 1) {
            substring += *it;
        } else {
            if (it == tag_collection.end() - 1) {
                substring += *it;
            }
            containers.push_back(substring);
            if (IsVariantSubtag(substring)) {
                std::transform(substring.begin(), substring.end(), substring.begin(), AsciiAlphaToLower);
                if (!unique_subtags.insert(substring).second) {
                    return false;
                }
            }
            substring.clear();
        }
    }
    bool result = DealwithLanguageTag(containers, address);
    return result;
}

std::string JSLocale::ConvertToStdString(const JSHandle<EcmaString> &ecma_str)
{
    return std::string(ConvertToPandaString(*ecma_str, StringConvertedUsage::LOGICOPERATION));
}

// 6.2.3 CanonicalizeUnicodeLocaleId( locale )
JSHandle<EcmaString> JSLocale::CanonicalizeUnicodeLocaleId(JSThread *thread, const JSHandle<EcmaString> &locale)
{
    [[maybe_unused]] ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (!IsStructurallyValidLanguageTag(locale)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "invalid locale", factory->GetEmptyString());
    }

    if (locale->GetLength() == 0 || locale->IsUtf16()) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "invalid locale", factory->GetEmptyString());
    }

    std::string locale_c_str = ConvertToStdString(locale);
    std::transform(locale_c_str.begin(), locale_c_str.end(), locale_c_str.begin(), AsciiAlphaToLower);
    UErrorCode status = U_ZERO_ERROR;
    icu::Locale formal_locale = icu::Locale::forLanguageTag(locale_c_str.c_str(), status);
    if ((U_FAILURE(status) != 0) || (formal_locale.isBogus() != 0)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "invalid locale", factory->GetEmptyString());
    }

    // Resets the LocaleBuilder to match the locale.
    // Returns an instance of Locale created from the fields set on this builder.
    formal_locale = icu::LocaleBuilder().setLocale(formal_locale).build(status);
    // Canonicalize the locale ID of this object according to CLDR.
    formal_locale.canonicalize(status);
    if ((U_FAILURE(status) != 0) || (formal_locale.isBogus() != 0)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "invalid locale", factory->GetEmptyString());
    }
    JSHandle<EcmaString> language_tag = ToLanguageTag(thread, formal_locale);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(EcmaString, thread);
    return language_tag;
}

// 6.2.4 DefaultLocale ()
JSHandle<EcmaString> JSLocale::DefaultLocale(JSThread *thread)
{
    icu::Locale default_locale;
    auto global_const = thread->GlobalConstants();
    if (strcmp(default_locale.getName(), "en_US_POSIX") == 0 || strcmp(default_locale.getName(), "c") == 0) {
        return JSHandle<EcmaString>::Cast(global_const->GetHandledEnUsString());
    }
    if (default_locale.isBogus() != 0) {
        return JSHandle<EcmaString>::Cast(global_const->GetHandledUndString());
    }
    return ToLanguageTag(thread, default_locale);
}

// 6.4.1 IsValidTimeZoneName ( timeZone )
bool JSLocale::IsValidTimeZoneName(const icu::TimeZone &tz)
{
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString id;
    tz.getID(id);
    icu::UnicodeString canonical;
    icu::TimeZone::getCanonicalID(id, canonical, status);
    UBool canonical_flag = (canonical != icu::UnicodeString("Etc/Unknown", -1, US_INV));
    return (U_SUCCESS(status) != 0) && (canonical_flag != 0);
}

void JSLocale::HandleLocaleExtension(size_t &start, size_t &extension_end, const std::string &result, size_t len)
{
    bool flag = false;
    while (start < len - INTL_INDEX_TWO) {
        if (result[start] != '-') {
            start++;
            continue;
        }
        if (result[start + INTL_INDEX_TWO] == '-') {
            extension_end = start;
            break;
        }
        if (!flag) {
            start++;
        }
        start += INTL_INDEX_TWO;
    }
}

JSLocale::ParsedLocale JSLocale::HandleLocale(const JSHandle<EcmaString> &locale_string)
{
    std::string result = ConvertToStdString(locale_string);
    size_t len = result.size();
    ParsedLocale parsed_result;

    // a. The single-character subtag ’x’ as the primary subtag indicates
    //    that the language tag consists solely of subtags whose meaning is
    //    defined by private agreement.
    // b. Extensions cannot be used in tags that are entirely private use.
    if (IsPrivateSubTag(result, len)) {
        parsed_result.base = result;
        return parsed_result;
    }
    // If cannot find "-u-", return the whole string as base.
    size_t found_extension = result.find("-u-");
    if (found_extension == std::string::npos) {
        parsed_result.base = result;
        return parsed_result;
    }
    // Let privateIndex be Call(%StringProto_indexOf%, foundLocale, « "-x-" »).
    size_t pravite_index = result.find("-x-");
    if (pravite_index != std::string::npos && pravite_index < found_extension) {
        parsed_result.base = result;
        return parsed_result;
    }
    const std::string basis = result.substr(INTL_INDEX_ZERO, found_extension);
    size_t extension_end = len;
    ASSERT(len > INTL_INDEX_TWO);
    size_t start = found_extension + INTL_INDEX_ONE;
    HandleLocaleExtension(start, extension_end, result, len);
    const std::string end = result.substr(extension_end);
    parsed_result.base = basis + end;
    parsed_result.extension = result.substr(found_extension, extension_end - found_extension);
    return parsed_result;
}

// 9.2.1 CanonicalizeLocaleList ( locales )
JSHandle<TaggedArray> JSLocale::CanonicalizeLocaleList(JSThread *thread, const JSHandle<JSTaggedValue> &locales)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. If locales is undefined, then
    //    a. Return a new empty List.
    if (locales->IsUndefined()) {
        return factory->EmptyArray();
    }
    // 2. Let seen be a new empty List.
    JSHandle<TaggedArray> locale_seen = factory->NewTaggedArray(1);
    // 3. If Type(locales) is String or Type(locales) is Object and locales has an [[InitializedLocale]] internal slot,
    //    then
    //    a. Let O be CreateArrayFromList(« locales »).
    // 4. Else,
    //    a.Let O be ? ToObject(locales).
    if (locales->IsString()) {
        JSHandle<EcmaString> tag = JSHandle<EcmaString>::Cast(locales);
        JSHandle<TaggedArray> temp = factory->NewTaggedArray(1);
        temp->Set(thread, 0, tag.GetTaggedValue());
        JSHandle<JSArray> obj = JSArray::CreateArrayFromList(thread, temp);
        JSHandle<TaggedArray> final_seen = CanonicalizeHelper<JSArray>(thread, locales, obj, locale_seen);
        return final_seen;
    }
    if (locales->IsJSLocale()) {
        JSHandle<EcmaString> tag = JSLocale::ToString(thread, JSHandle<JSLocale>::Cast(locales));
        JSHandle<TaggedArray> temp = factory->NewTaggedArray(1);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
        temp->Set(thread, 0, tag.GetTaggedValue());
        JSHandle<JSArray> obj = JSArray::CreateArrayFromList(thread, temp);
        JSHandle<TaggedArray> final_seen = CanonicalizeHelper<JSArray>(thread, locales, obj, locale_seen);
        return final_seen;
    }
    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, locales);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
    JSHandle<TaggedArray> final_seen = CanonicalizeHelper<JSObject>(thread, locales, obj, locale_seen);
    return final_seen;
}

template <typename T>
JSHandle<TaggedArray> JSLocale::CanonicalizeHelper(JSThread *thread, const JSHandle<JSTaggedValue> &locales,
                                                   JSHandle<T> &obj, JSHandle<TaggedArray> &seen)
{
    (void)locales;
    OperationResult operation_result = JSTaggedValue::GetProperty(thread, JSHandle<JSTaggedValue>::Cast(obj),
                                                                  thread->GlobalConstants()->GetHandledLengthString());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
    JSTaggedNumber len = JSTaggedValue::ToLength(thread, operation_result.GetValue());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 2. Let seen be a new empty List.
    uint32_t requested_locales_len = len.ToUint32();
    seen = factory->NewTaggedArray(requested_locales_len);
    // 6. Let k be 0.
    // 7. Repeat, while k < len
    JSMutableHandle<JSTaggedValue> pk(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> tag(thread, JSTaggedValue::Undefined());
    uint32_t index = 0;
    JSHandle<JSTaggedValue> obj_tagged = JSHandle<JSTaggedValue>::Cast(obj);
    for (uint32_t k = 0; k < requested_locales_len; k++) {
        // a. Let Pk be ToString(k).
        JSHandle<JSTaggedValue> k_handle(thread, JSTaggedValue(k));
        JSHandle<EcmaString> str = JSTaggedValue::ToString(thread, k_handle);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
        pk.Update(str.GetTaggedValue());
        // b. Let kPresent be ? HasProperty(O, Pk).
        bool k_present = JSTaggedValue::HasProperty(thread, obj_tagged, pk);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);

        // c. If kPresent is true, then
        if (k_present) {
            // i. Let kValue be ? Get(O, Pk).
            OperationResult result = JSTaggedValue::GetProperty(thread, obj_tagged, pk);
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
            JSHandle<JSTaggedValue> k_value = result.GetValue();
            // ii. If Type(kValue) is not String or Object, throw a TypeError exception.
            if (!k_value->IsString() && !k_value->IsJSObject()) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "kValue is not String or Object.", factory->EmptyArray());
            }
            // iii. If Type(kValue) is Object and kValue has an [[InitializedLocale]] internal slot, then
            //        1. Let tag be kValue.[[Locale]].
            // iv.  Else,
            //        1. Let tag be ? ToString(kValue).
            if (k_value->IsJSLocale()) {
                JSHandle<EcmaString> k_value_str = JSLocale::ToString(thread, JSHandle<JSLocale>::Cast(k_value));
                RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
                tag.Update(k_value_str.GetTaggedValue());
            } else {
                JSHandle<EcmaString> k_value_string = JSTaggedValue::ToString(thread, k_value);
                RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
                JSHandle<EcmaString> str = CanonicalizeUnicodeLocaleId(thread, k_value_string);
                RETURN_HANDLE_IF_ABRUPT_COMPLETION(TaggedArray, thread);
                tag.Update(str.GetTaggedValue());
            }
            // vii. If canonicalizedTag is not an element of seen, append canonicalizedTag as the last element of seen.
            bool is_exist = false;
            uint32_t len = seen->GetLength();
            for (uint32_t i = 0; i < len; i++) {
                if (JSTaggedValue::SameValue(seen->Get(thread, i), tag.GetTaggedValue())) {
                    is_exist = true;
                }
            }
            if (!is_exist) {
                seen->Set(thread, index++, JSHandle<JSTaggedValue>::Cast(tag));
            }
        }
        // d. Increase k by 1.
    }
    // set capacity
    seen = TaggedArray::SetCapacity(thread, seen, index);
    // 8. Return seen.
    return seen;
}

// 9.2.2 BestAvailableLocale ( availableLocales, locale )
std::string JSLocale::BestAvailableLocale(JSThread *thread, const JSHandle<TaggedArray> &available_locales,
                                          const std::string &locale)
{
    // 1. Let candidate be locale.
    std::string locale_candidate = locale;
    std::string undefined = std::string();
    // 2. Repeat,
    uint32_t length = available_locales->GetLength();
    JSMutableHandle<EcmaString> item(thread, JSTaggedValue::Undefined());
    while (true) {
        // a. If availableLocales contains an element equal to candidate, return candidate.
        for (uint32_t i = 0; i < length; ++i) {
            item.Update(available_locales->Get(thread, i));
            std::string item_str = ConvertToStdString(item);
            if (item_str == locale_candidate) {
                return locale_candidate;
            }
        }
        // b. Let pos be the character index of the last occurrence of "-" (U+002D) within candidate.
        //    If that character does not occur, return undefined.
        size_t pos = locale_candidate.rfind('-');
        if (pos == std::string::npos) {
            return undefined;
        }
        // c. If pos ≥ 2 and the character "-" occurs at index pos-2 of candidate, decrease pos by 2.
        if (pos >= INTL_INDEX_TWO && locale_candidate[pos - INTL_INDEX_TWO] == '-') {
            pos -= INTL_INDEX_TWO;
        }
        // d. Let candidate be the substring of candidate from position 0, inclusive, to position pos, exclusive.
        locale_candidate = locale_candidate.substr(0, pos);
    }
}

// 9.2.3 LookupMatcher ( availableLocales, requestedLocales )
JSHandle<EcmaString> JSLocale::LookupMatcher(JSThread *thread, const JSHandle<TaggedArray> &available_locales,
                                             const JSHandle<TaggedArray> &requested_locales)
{
    MatcherResult result = {std::string(), std::string()};
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let result be a new Record.
    // 2. For each element locale of requestedLocales in List order, do
    uint32_t length = requested_locales->GetLength();
    JSMutableHandle<EcmaString> locale(thread, JSTaggedValue::Undefined());
    for (uint32_t i = 0; i < length; ++i) {
        locale.Update(requested_locales->Get(thread, i));
        // 2. a. Let noExtensionsLocale be the String value that is locale
        //       with all Unicode locale extension sequences removed.
        ParsedLocale parsed_result = HandleLocale(locale);
        // 2. b. Let availableLocale be BestAvailableLocale(availableLocales, noExtensionsLocale).
        std::string available_locale = BestAvailableLocale(thread, available_locales, parsed_result.base);
        // 2. c. If availableLocale is not undefined, append locale to the end of subset.
        if (!available_locale.empty()) {
            result = {std::string(), std::string()};
            // 2. c. i. Set result.[[locale]] to availableLocale.
            result.locale = available_locale;
            // 2. c. ii. If locale and noExtensionsLocale are not the same String value, then
            // 2. c. ii. 1. Let extension be the String value consisting of  the first substring of locale that is a
            //              Unicode locale extension sequence.
            if (!parsed_result.extension.empty()) {
                result.extension = parsed_result.extension;
            }
            // 2. c. ii. 2. Set result.[[extension]] to extension.
            std::string res = result.locale + result.extension;
            // 2. c. iii. Return result.
            return factory->NewFromStdString(res);
        }
    }

    // 3. Let defLocale be DefaultLocale();
    // 4. Set result.[[locale]] to defLocale.
    // 5. Return result.
    std::string def_locale = ConvertToStdString(DefaultLocale(thread));
    result.locale = def_locale;
    return factory->NewFromStdString(result.locale);
}

icu::LocaleMatcher BuildLocaleMatcher(JSThread *thread, uint32_t *available_length, UErrorCode *status,
                                      const JSHandle<TaggedArray> &available_locales)
{
    std::string locale = JSLocale::ConvertToStdString(JSLocale::DefaultLocale(thread));
    icu::Locale default_locale = icu::Locale::forLanguageTag(locale, *status);
    ASSERT_PRINT(U_SUCCESS(*status), "icu::Locale::forLanguageTag failed");
    icu::LocaleMatcher::Builder builder;
    builder.setDefaultLocale(&default_locale);
    uint32_t length = available_locales->GetLength();

    JSMutableHandle<EcmaString> item(thread, JSTaggedValue::Undefined());
    for (*available_length = 0; *available_length < length; ++(*available_length)) {
        item.Update(available_locales->Get(thread, *available_length));
        std::string item_str = JSLocale::ConvertToStdString(item);
        icu::Locale locale_for_language_tag = icu::Locale::forLanguageTag(item_str, *status);
        if (U_SUCCESS(*status) != 0) {
            builder.addSupportedLocale(locale_for_language_tag);
        } else {
            break;
        }
    }
    *status = U_ZERO_ERROR;
    return builder.build(*status);
}

// 9.2.4 BestFitMatcher ( availableLocales, requestedLocales )
JSHandle<EcmaString> JSLocale::BestFitMatcher(JSThread *thread, const JSHandle<TaggedArray> &available_locales,
                                              const JSHandle<TaggedArray> &requested_locales)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    UErrorCode status = U_ZERO_ERROR;
    uint32_t available_length = available_locales->GetLength();
    icu::LocaleMatcher matcher = BuildLocaleMatcher(thread, &available_length, &status, available_locales);
    ASSERT(U_SUCCESS(status));

    uint32_t requested_locales_length = requested_locales->GetLength();
    JSIntlIterator iter(requested_locales, requested_locales_length);
    auto best_fit = matcher.getBestMatch(iter, status)->toLanguageTag<std::string>(status);

    if (U_FAILURE(status) != 0) {
        return DefaultLocale(thread);
    }

    for (uint32_t i = 0; i < requested_locales_length; ++i) {
        if (iter[i] == best_fit) {
            return JSHandle<EcmaString>(thread, requested_locales->Get(thread, i));
        }
    }
    return factory->NewFromStdString(best_fit);
}

// 9.2.8 LookupSupportedLocales ( availableLocales, requestedLocales )
JSHandle<TaggedArray> JSLocale::LookupSupportedLocales(JSThread *thread, const JSHandle<TaggedArray> &available_locales,
                                                       const JSHandle<TaggedArray> &requested_locales)
{
    uint32_t index = 0;
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    uint32_t length = requested_locales->GetLength();
    // 1. Let subset be a new empty List.
    JSHandle<TaggedArray> subset = factory->NewTaggedArray(length);
    JSMutableHandle<EcmaString> item(thread, JSTaggedValue::Undefined());
    // 2. For each element locale of requestedLocales in List order, do
    //    a. Let noExtensionsLocale be the String value that is locale with all Unicode locale extension sequences
    //       removed.
    //    b. Let availableLocale be BestAvailableLocale(availableLocales, noExtensionsLocale).
    //    c. If availableLocale is not undefined, append locale to the end of subset.
    for (uint32_t i = 0; i < length; ++i) {
        item.Update(requested_locales->Get(thread, i));
        ParsedLocale foundation_result = HandleLocale(item);
        std::string available_locale = BestAvailableLocale(thread, available_locales, foundation_result.base);
        if (!available_locale.empty()) {
            subset->Set(thread, index++, item.GetTaggedValue());
        }
    }
    // 3. Return subset.
    return TaggedArray::SetCapacity(thread, subset, index);
}

// 9.2.9 BestFitSupportedLocales ( availableLocales, requestedLocales )
JSHandle<TaggedArray> JSLocale::BestFitSupportedLocales(JSThread *thread,
                                                        const JSHandle<TaggedArray> &available_locales,
                                                        const JSHandle<TaggedArray> &requested_locales)
{
    UErrorCode status = U_ZERO_ERROR;
    uint32_t request_length = requested_locales->GetLength();
    uint32_t available_length = available_locales->GetLength();
    icu::LocaleMatcher matcher = BuildLocaleMatcher(thread, &available_length, &status, available_locales);
    ASSERT(U_SUCCESS(status));

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> default_locale = DefaultLocale(thread);
    JSHandle<TaggedArray> result = factory->NewTaggedArray(request_length);

    uint32_t index = 0;
    JSMutableHandle<EcmaString> locale(thread, JSTaggedValue::Undefined());
    for (uint32_t i = 0; i < request_length; ++i) {
        locale.Update(requested_locales->Get(thread, i));
        if (EcmaString::StringsAreEqual(*locale, *default_locale)) {
            result->Set(thread, index++, locale.GetTaggedValue());
        } else {
            status = U_ZERO_ERROR;
            std::string locale_str = ConvertToStdString(locale);
            icu::Locale desired = icu::Locale::forLanguageTag(locale_str, status);
            auto best_fit = matcher.getBestMatch(desired, status)->toLanguageTag<std::string>(status);
            if ((U_SUCCESS(status) != 0) &&
                EcmaString::StringsAreEqual(*locale, *(factory->NewFromStdString(best_fit)))) {
                result->Set(thread, index++, locale.GetTaggedValue());
            }
        }
    }
    result = TaggedArray::SetCapacity(thread, result, index);
    return result;
}

JSHandle<EcmaString> JSLocale::ToLanguageTag(JSThread *thread, const icu::Locale &locale)
{
    UErrorCode status = U_ZERO_ERROR;
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    auto result = locale.toLanguageTag<std::string>(status);
    bool flag = U_FAILURE(status) == 0;
    if (!flag) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "invalid locale", factory->GetEmptyString());
    }
    size_t find_beginning = result.find("-u-");
    std::string final_res;
    std::string temp_res;
    if (find_beginning == std::string::npos) {
        return factory->NewFromStdString(result);
    }
    size_t special_beginning = find_beginning + INTL_INDEX_THREE;
    size_t special_count = 0;
    while (special_beginning < result.size() && result[special_beginning] != '-') {
        special_count++;
        special_beginning++;
    }
    if (find_beginning != std::string::npos) {
        // It begin with "-u-xx" or with more elements.
        temp_res = result.substr(0, find_beginning + INTL_INDEX_THREE + special_count);
        if (result.size() <= find_beginning + INTL_INDEX_THREE + special_count) {
            return factory->NewFromStdString(result);
        }
        std::string left_str = result.substr(find_beginning + INTL_INDEX_THREE + special_count + INTL_INDEX_ONE);
        std::istringstream temp(left_str);
        std::string buffer;
        std::vector<std::string> res_container;
        while (getline(temp, buffer, '-')) {
            if (buffer != "true" && buffer != "yes") {
                res_container.push_back(buffer);
            }
        }
        for (auto &it : res_container) {
            std::string tag = "-";
            tag += it;
            final_res += tag;
        }
    }
    if (!final_res.empty()) {
        temp_res += final_res;
    }
    result = temp_res;
    return factory->NewFromStdString(result);
}

// 9.2.10 SupportedLocales ( availableLocales, requestedLocales, options )
JSHandle<JSArray> JSLocale::SupportedLocales(JSThread *thread, const JSHandle<TaggedArray> &available_locales,
                                             const JSHandle<TaggedArray> &requested_locales,
                                             const JSHandle<JSTaggedValue> &options)
{
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 1. If options is not undefined, then
    //    a. Let options be ? ToObject(options).
    //    b. Let matcher be ? GetOption(options, "localeMatcher", "string", « "lookup", "best fit" », "best fit").
    // 2. Else, let matcher be "best fit".
    LocaleMatcherOption matcher = LocaleMatcherOption::BEST_FIT;
    if (!options->IsUndefined()) {
        JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, options);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSArray, thread);

        matcher = GetOptionOfString<LocaleMatcherOption>(thread, obj, global_const->GetHandledLocaleMatcherString(),
                                                         {LocaleMatcherOption::LOOKUP, LocaleMatcherOption::BEST_FIT},
                                                         {"lookup", "best fit"}, LocaleMatcherOption::BEST_FIT);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSArray, thread);
    }

    // 3. If matcher is "best fit", then
    //    a. Let supportedLocales be BestFitSupportedLocales(availableLocales, requestedLocales).
    // 4. Else,
    //    a. Let supportedLocales be LookupSupportedLocales(availableLocales, requestedLocales).
    JSMutableHandle<TaggedArray> supported_locales(thread, JSTaggedValue::Undefined());
    bool is_bestfit_support = false;
    if (matcher == LocaleMatcherOption::BEST_FIT && is_bestfit_support) {
        supported_locales.Update(
            BestFitSupportedLocales(thread, available_locales, requested_locales).GetTaggedValue());
    } else {
        supported_locales.Update(LookupSupportedLocales(thread, available_locales, requested_locales).GetTaggedValue());
    }

    JSHandle<JSArray> subset = JSArray::CreateArrayFromList(thread, supported_locales);
    // 5. Return CreateArrayFromList(supportedLocales).
    return subset;
}

// 9.2.11 GetOption ( options, property, type, values, fallback )
JSHandle<JSTaggedValue> JSLocale::GetOption(JSThread *thread, const JSHandle<JSObject> &options,
                                            const JSHandle<JSTaggedValue> &property, OptionType type,
                                            const JSHandle<JSTaggedValue> &values,
                                            const JSHandle<JSTaggedValue> &fallback)
{
    // 1. Let value be ? Get(options, property).
    JSHandle<JSTaggedValue> value = JSObject::GetProperty(thread, options, property).GetValue();
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);

    // 2. If value is not undefined, then
    if (!value->IsUndefined()) {
        // a. Assert: type is "boolean" or "string".
        ASSERT_PRINT(type == OptionType::BOOLEAN || type == OptionType::STRING, "type is not boolean or string");

        // b. If type is "boolean", then
        //    i. Let value be ToBoolean(value).
        if (type == OptionType::BOOLEAN) {
            value = JSHandle<JSTaggedValue>(thread, JSTaggedValue(value->ToBoolean()));
        }
        // c. If type is "string", then
        //    i. Let value be ? ToString(value).
        if (type == OptionType::STRING) {
            JSHandle<EcmaString> str = JSTaggedValue::ToString(thread, value);
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);
            value = JSHandle<JSTaggedValue>(thread, str.GetTaggedValue());
        }

        // d. If values is not undefined, then
        //    i. If values does not contain an element equal to value, throw a RangeError exception.
        if (!values->IsUndefined()) {
            bool is_exist = false;
            JSHandle<TaggedArray> values_array = JSHandle<TaggedArray>::Cast(values);
            uint32_t length = values_array->GetLength();
            for (uint32_t i = 0; i < length; i++) {
                if (JSTaggedValue::SameValue(values_array->Get(thread, i), value.GetTaggedValue())) {
                    is_exist = true;
                }
            }
            if (!is_exist) {
                JSHandle<JSTaggedValue> exception(thread, JSTaggedValue::Exception());
                THROW_RANGE_ERROR_AND_RETURN(thread, "values does not contain an element equal to value", exception);
            }
        }
        // e. Return value.
        return value;
    }
    // 3. Else, return fallback.
    return fallback;
}

bool JSLocale::GetOptionOfString(JSThread *thread, const JSHandle<JSObject> &options,
                                 const JSHandle<JSTaggedValue> &property, const std::vector<std::string> &values,
                                 std::string *option_value)
{
    // 1. Let value be ? Get(options, property).
    OperationResult operation_result = JSObject::GetProperty(thread, options, property);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    JSHandle<JSTaggedValue> value = operation_result.GetValue();
    // 2. If value is not undefined, then
    if (value->IsUndefined()) {
        return false;
    }
    //    c. If type is "string" "string", then
    //       i. Let value be ? ToString(value).
    JSHandle<EcmaString> value_e_str = JSTaggedValue::ToString(thread, value);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    if (value_e_str->IsUtf16()) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Value out of range for locale options property", false);
    }
    *option_value = JSLocale::ConvertToStdString(value_e_str);
    if (values.empty()) {
        return true;
    }
    // d. If values is not undefined, then
    //    i. If values does not contain an element equal to value, throw a RangeError exception.
    for (const auto &item : values) {
        if (item == *option_value) {
            return true;
        }
    }
    THROW_RANGE_ERROR_AND_RETURN(thread, "Value out of range for locale options property", false);
}

// 9.2.12 DefaultNumberOption ( value, minimum, maximum, fallback )
int JSLocale::DefaultNumberOption(JSThread *thread, const JSHandle<JSTaggedValue> &value, int minimum, int maximum,
                                  int fallback)
{
    // 1. If value is not undefined, then
    if (!value->IsUndefined()) {
        // a. Let value be ? ToNumber(value).
        JSTaggedNumber number = JSTaggedValue::ToNumber(thread, value);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fallback);
        // b. If value is NaN or less than minimum or greater than maximum, throw a RangeError exception.
        double num = JSTaggedValue(number).GetNumber();
        if (std::isnan(num) || num < minimum || num > maximum) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "", fallback);
        }
        // c. Return floor(value).
        return std::floor(num);
    }
    // 2. Else, return fallback.
    return fallback;
}

// 9.2.13 GetNumberOption ( options, property, minimum, maximum, fallback )
int JSLocale::GetNumberOption(JSThread *thread, const JSHandle<JSObject> &options,
                              const JSHandle<JSTaggedValue> &property, int min, int max, int fallback)
{
    // 1. Let value be ? Get(options, property).
    JSHandle<JSTaggedValue> value = JSObject::GetProperty(thread, options, property).GetValue();
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fallback);

    // 2. Return ? DefaultNumberOption(value, minimum, maximum, fallback).
    int result = DefaultNumberOption(thread, value, min, max, fallback);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fallback);
    return result;
}

// 9.2.5 UnicodeExtensionValue ( extension, key )
std::string JSLocale::UnicodeExtensionValue(const std::string &extension, const std::string &key)
{
    // 1. Assert: The number of elements in key is 2.
    // 2. Let size be the number of elements in extension.
    ASSERT(key.size() == INTL_INDEX_TWO);
    size_t size = extension.size();
    // 3. Let searchValue be the concatenation of "-" , key, and "-".
    std::string search_value = "-" + key + "-";
    // 4. Let pos be Call(%StringProto_indexOf%, extension, « searchValue »).
    size_t pos = extension.find(search_value);
    // 5. If pos ≠ -1, then
    if (pos != std::string::npos) {
        // a. Let start be pos + 4.
        size_t start = pos + INTL_INDEX_FOUR;
        // b. Let end be start.
        size_t end = start;
        // c. Let k be start.
        size_t k = start;
        // d. Let done be false.
        bool done = false;
        // e. Repeat, while done is false
        while (!done) {
            // i. Let e be Call(%StringProto_indexOf%, extension, « "-", k »).
            size_t e = extension.find('-', k);
            size_t len;
            // ii. If e = -1, let len be size - k; else let len be e - k.
            if (e == std::string::npos) {
                len = size - k;
            } else {
                len = e - k;
            }
            // iii. If len = 2, then
            //     1. Let done be true.
            if (len == INTL_INDEX_TWO) {
                done = true;
                // iv. Else if e = -1, then
                //    1. Let end be size.
                //    2. Let done be true.
            } else if (e == std::string::npos) {
                end = size;
                done = true;
                // v. Else,
                //   1. Let end be e.
                //   2. Let k be e + 1.
            } else {
                end = e;
                k = e + INTL_INDEX_ONE;
            }
        }
        // f. Return the String value equal to the substring of extension consisting of the code units at indices.
        // start (inclusive) through end (exclusive).
        std::string result = extension.substr(start, end - start);
        return result;
    }
    // 6. Let searchValue be the concatenation of "-" and key.
    search_value = "-" + key;
    // 7. Let pos be Call(%StringProto_indexOf%, extension, « searchValue »).
    pos = extension.find(search_value);
    // 8. If pos ≠ -1 and pos + 3 = size, then
    //    a. Return the empty String.
    if (pos != std::string::npos && pos + INTL_INDEX_THREE == size) {
        return "";
    }
    // 9. Return undefined.
    return "undefined";
}

ResolvedLocale JSLocale::ResolveLocale(JSThread *thread, const JSHandle<TaggedArray> &available_locales,
                                       const JSHandle<TaggedArray> &requested_locales, LocaleMatcherOption matcher,
                                       const std::set<std::string> &relevant_extension_keys)
{
    bool is_bestfit_support = false;
    std::map<std::string, std::set<std::string>> locale_map = {{"hc", {"h11", "h12", "h23", "h24"}},
                                                               {"lb", {"strict", "normal", "loose"}},
                                                               {"kn", {"true", "false"}},
                                                               {"kf", {"upper", "lower", "false"}}};

    // 1. Let matcher be options.[[localeMatcher]].
    // 2. If matcher is "lookup" "lookup", then
    //    a. Let r be LookupMatcher(availableLocales, requestedLocales).
    // 3. Else,
    //    a. Let r be BestFitMatcher(availableLocales, requestedLocales).
    JSMutableHandle<EcmaString> locale(thread, JSTaggedValue::Undefined());
    if (available_locales->GetLength() == 0 && requested_locales->GetLength() == 0) {
        locale.Update(DefaultLocale(thread).GetTaggedValue());
    } else {
        if (matcher == LocaleMatcherOption::BEST_FIT && is_bestfit_support) {
            locale.Update(BestFitMatcher(thread, available_locales, requested_locales).GetTaggedValue());
        } else {
            locale.Update(LookupMatcher(thread, available_locales, requested_locales).GetTaggedValue());
        }
    }

    // 4. Let foundLocale be r.[[locale]].
    // 5. Let result be a new Record.
    // 6. Set result.[[dataLocale]] to foundLocale.
    // 7. Let supportedExtension be "-u".
    std::string found_locale = ConvertToStdString(locale);
    icu::Locale found_locale_data = BuildICULocale(found_locale);
    ResolvedLocale result;
    result.locale_data = found_locale_data;
    JSHandle<EcmaString> tag = ToLanguageTag(thread, found_locale_data);
    result.locale = ConvertToStdString(tag);
    std::string supported_extension = "-u";
    icu::LocaleBuilder locale_builder;
    locale_builder.setLocale(found_locale_data).clearExtensions();
    // 8. For each element key of relevantExtensionKeys in List order, do
    for (auto &key : relevant_extension_keys) {
        auto double_match = found_locale.find(key);
        if (double_match == std::string::npos) {
            continue;
        }
        UErrorCode status = U_ZERO_ERROR;
        std::set<std::string> key_locale_data;
        std::unique_ptr<icu::StringEnumeration> well_form_key(found_locale_data.createKeywords(status));
        if (U_FAILURE(status) != 0) {
            return result;
        }
        if (!well_form_key) {
            return result;
        }
        std::string value;

        // c. Let keyLocaleData be foundLocaleData.[[<key>]].
        // e. Let value be keyLocaleData[0].
        if ((key != "ca") && (key != "co") && (key != "nu")) {
            key_locale_data = locale_map[key];
            value = *key_locale_data.begin();
        }

        // g. Let supportedExtensionAddition be "".
        // h. If r has an [[extension]] field, then
        std::string supported_extension_addition;
        size_t found = found_locale.find("-u-");
        if (found != std::string::npos) {
            std::string extension = found_locale.substr(found + INTL_INDEX_ONE);

            // i. Let requestedValue be UnicodeExtensionValue(r.[[extension]], key).
            std::string requested_value = UnicodeExtensionValue(extension, key);
            if (key == "kn" && requested_value.empty()) {
                requested_value = "true";
            }

            // ii. If requestedValue is not undefined, then
            if (requested_value != "undefined") {
                // 1. If requestedValue is not the empty String, then
                if (!requested_value.empty()) {
                    // a. If keyLocaleData contains requestedValue, then
                    //    i. Let value be requestedValue.
                    //    ii. Let supportedExtensionAddition be the concatenation of "-", key, "-", and value.
                    if (key == "ca" || key == "co") {
                        if (key == "co") {
                            bool is_valid_value = IsWellCollation(found_locale_data, requested_value);
                            if (!is_valid_value) {
                                continue;
                            }
                            value = requested_value;
                            supported_extension_addition = std::string("-").append(key).append("-").append(value);
                            locale_builder.setUnicodeLocaleKeyword(key, requested_value);
                        } else {
                            bool is_valid_value = IsWellCalendar(found_locale_data, requested_value);
                            if (!is_valid_value) {
                                continue;
                            }
                            value = requested_value;
                            supported_extension_addition = std::string("-").append(key).append("-").append(value);
                            locale_builder.setUnicodeLocaleKeyword(key, requested_value);
                        }
                    } else if (key == "nu") {
                        bool is_valid_value = IsWellNumberingSystem(requested_value);
                        if (!is_valid_value) {
                            continue;
                        }
                        value = requested_value;
                        supported_extension_addition = std::string("-").append(key).append("-").append(value);
                        locale_builder.setUnicodeLocaleKeyword(key, requested_value);
                    } else if (key_locale_data.find(requested_value) != key_locale_data.end()) {
                        value = requested_value;
                        supported_extension_addition = std::string("-").append(key).append("-").append(value);
                        locale_builder.setUnicodeLocaleKeyword(key, requested_value);
                    }
                }
            }
        }
        result.extensions.insert(std::pair<std::string, std::string>(key, value));
        supported_extension += supported_extension_addition;
    }
    size_t found = found_locale.find("-u-");
    if (found != std::string::npos) {
        found_locale = found_locale.substr(0, found);
    }

    // 9. If the number of elements in supportedExtension is greater than 2, then
    if (supported_extension.size() > 2) {
        // a. Let privateIndex be Call(%StringProto_indexOf%, foundLocale, « "-x-" »).
        size_t private_index = found_locale.find("-x-");
        // b. If privateIndex = -1, then
        //    i. Let foundLocale be the concatenation of foundLocale and supportedExtension.
        if (private_index == std::string::npos) {
            found_locale = found_locale + supported_extension;
        } else {
            std::string pre_extension = found_locale.substr(0, private_index);
            std::string post_extension = found_locale.substr(private_index);
            found_locale = pre_extension + supported_extension + post_extension;
        }

        tag = ToLanguageTag(thread, found_locale_data);
        if (!IsStructurallyValidLanguageTag(tag)) {
            result.extensions.erase(result.extensions.begin(), result.extensions.end());
            result.locale = found_locale;
        }
        tag = CanonicalizeUnicodeLocaleId(thread, tag);
        found_locale = ConvertToStdString(tag);
    }

    // 10. Set result.[[locale]] to foundLocale.
    result.locale = found_locale;
    UErrorCode status = U_ZERO_ERROR;
    found_locale_data = locale_builder.build(status);
    result.locale_data = found_locale_data;

    // 11. Return result.
    return result;
}

icu::Locale JSLocale::BuildICULocale(const std::string &bcp47_locale)
{
    UErrorCode status = U_ZERO_ERROR;
    icu::Locale icu_locale = icu::Locale::forLanguageTag(bcp47_locale, status);
    ASSERT_PRINT(U_SUCCESS(status), "forLanguageTag failed");
    ASSERT_PRINT(!icu_locale.isBogus(), "icuLocale is bogus");
    return icu_locale;
}

JSHandle<TaggedArray> JSLocale::ConstructLocaleList(JSThread *thread,
                                                    const std::vector<std::string> &icu_available_locales)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<TaggedArray> locales = factory->NewTaggedArray(icu_available_locales.size());
    int32_t index = 0;
    for (const std::string &locale : icu_available_locales) {
        JSHandle<EcmaString> locale_str = factory->NewFromStdString(locale);
        locales->Set(thread, index++, locale_str);
    }
    return locales;
}

JSHandle<EcmaString> JSLocale::IcuToString(JSThread *thread, const icu::UnicodeString &string)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    return factory->NewFromUtf16(reinterpret_cast<const uint16_t *>(string.getBuffer()), string.length());
}

JSHandle<EcmaString> JSLocale::IcuToString(JSThread *thread, const icu::UnicodeString &string, int32_t begin,
                                           int32_t end)
{
    return IcuToString(thread, string.tempSubStringBetween(begin, end));
}

std::string JSLocale::GetNumberingSystem(const icu::Locale &icu_locale)
{
    UErrorCode status = U_ZERO_ERROR;
    std::unique_ptr<icu::NumberingSystem> numbering_system(icu::NumberingSystem::createInstance(icu_locale, status));
    if (U_SUCCESS(status) != 0) {
        return numbering_system->getName();
    }
    return "latn";
}

bool JSLocale::IsWellFormedCurrencyCode(const std::string &currency)
{
    if (currency.length() != INTL_INDEX_THREE) {
        return false;
    }
    return (IsAToZ(currency[INTL_INDEX_ZERO]) && IsAToZ(currency[INTL_INDEX_ONE]) && IsAToZ(currency[INTL_INDEX_TWO]));
}

JSHandle<JSObject> JSLocale::PutElement(JSThread *thread, int index, const JSHandle<JSArray> &array,
                                        const JSHandle<JSTaggedValue> &field_type_string,
                                        const JSHandle<JSTaggedValue> &value)
{
    auto ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // Let record be ! ObjectCreate(%ObjectPrototype%).
    JSHandle<JSObject> record = factory->NewEmptyJSObject();

    auto global_const = thread->GlobalConstants();
    // obj.type = field_type_string
    JSObject::CreateDataPropertyOrThrow(thread, record, global_const->GetHandledTypeString(), field_type_string);
    // obj.value = value
    JSObject::CreateDataPropertyOrThrow(thread, record, global_const->GetHandledValueString(), value);

    JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(array), index,
                               JSHandle<JSTaggedValue>::Cast(record), true);
    return record;
}

// 9.2.11 GetOption ( options, property, type, values, fallback )
bool JSLocale::GetOptionOfBool(JSThread *thread, const JSHandle<JSObject> &options,
                               const JSHandle<JSTaggedValue> &property, bool fallback, bool *res)
{
    // 1. Let value be ? Get(options, property).
    OperationResult operation_result = JSObject::GetProperty(thread, options, property);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    JSHandle<JSTaggedValue> value = operation_result.GetValue();
    *res = fallback;
    // 2. If value is not undefined, then
    if (!value->IsUndefined()) {
        // b. Let value be ToBoolean(value).
        *res = value->ToBoolean();
        return true;
    }
    // 3. not found
    return false;
}

JSHandle<JSTaggedValue> JSLocale::GetNumberFieldType(JSThread *thread, JSTaggedValue x, int32_t field_id)
{
    ASSERT(x.IsNumber());
    auto global_const = thread->GlobalConstants();
    switch (static_cast<UNumberFormatFields>(field_id)) {
        case UNUM_INTEGER_FIELD:
            if (std::isfinite(x.GetNumber())) {
                return global_const->GetHandledIntegerString();
            }
            if (std::isnan(x.GetNumber())) {
                return global_const->GetHandledNanString();
            }
            return global_const->GetHandledInfinityString();

        case UNUM_FRACTION_FIELD:
            return global_const->GetHandledFractionString();

        case UNUM_DECIMAL_SEPARATOR_FIELD:
            return global_const->GetHandledDecimalString();

        case UNUM_GROUPING_SEPARATOR_FIELD:
            return global_const->GetHandledGroupString();

        case UNUM_CURRENCY_FIELD:
            return global_const->GetHandledCurrencyString();

        case UNUM_PERCENT_FIELD:
            return global_const->GetHandledPercentSignString();

        case UNUM_SIGN_FIELD:
            return std::signbit(x.GetNumber()) ? global_const->GetHandledMinusSignString()
                                               : global_const->GetHandledPlusSignString();

        case UNUM_EXPONENT_SYMBOL_FIELD:
            return global_const->GetHandledExponentSeparatorString();

        case UNUM_EXPONENT_SIGN_FIELD:
            return global_const->GetHandledExponentMinusSignString();

        case UNUM_EXPONENT_FIELD:
            return global_const->GetHandledExponentIntegerString();

        case UNUM_COMPACT_FIELD:
            return global_const->GetHandledCompactString();

        case UNUM_MEASURE_UNIT_FIELD:
            return global_const->GetHandledUnitString();

        default:
            UNREACHABLE();
    }

    UNREACHABLE();
}

// 10.1.1 ApplyOptionsToTag( tag, options )
bool JSLocale::ApplyOptionsToTag(JSThread *thread, const JSHandle<EcmaString> &tag, const JSHandle<JSObject> &options,
                                 TagElements &tag_elements)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    ObjectFactory *factory = ecma_vm->GetFactory();
    if (*tag == *(factory->GetEmptyString())) {
        return false;
    }
    // 2. If IsStructurallyValidLanguageTag(tag) is false, throw a RangeError exception.
    if (!IsStructurallyValidLanguageTag(tag)) {
        return false;
    }

    tag_elements.language = GetOption(thread, options, global_const->GetHandledLanguageString(), OptionType::STRING,
                                      global_const->GetHandledUndefined(), global_const->GetHandledUndefined());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);

    // 4. If language is not undefined, then
    //    a. If language does not match the unicode_language_subtag production, throw a RangeError exception.
    if (!tag_elements.language->IsUndefined()) {
        std::string language_str = ConvertToStdString(JSHandle<EcmaString>::Cast(tag_elements.language));
        if (language_str[INTL_INDEX_ZERO] == '\0' || IsAlpha(language_str, INTL_INDEX_FOUR, INTL_INDEX_FOUR)) {
            return false;
        }
    }

    // 5. Let script be ? GetOption(options, "script", "string", undefined, undefined).
    tag_elements.script = GetOption(thread, options, global_const->GetHandledScriptString(), OptionType::STRING,
                                    global_const->GetHandledUndefined(), global_const->GetHandledUndefined());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);

    // 6. If script is not undefined, then
    //    a. If script does not match the unicode_script_subtag production, throw a RangeError exception.
    if (!tag_elements.script->IsUndefined()) {
        std::string script_str = JSLocale::ConvertToStdString((JSHandle<EcmaString>::Cast(tag_elements.script)));
        if (script_str[INTL_INDEX_ZERO] == '\0') {
            return false;
        }
    }

    // 7. Let region be ? GetOption(options, "region", "string", undefined, undefined).
    // 8. If region is not undefined, then
    //    a. If region does not match the unicode_region_subtag production, throw a RangeError exception.
    tag_elements.region = GetOption(thread, options, global_const->GetHandledRegionString(), OptionType::STRING,
                                    global_const->GetHandledUndefined(), global_const->GetHandledUndefined());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);

    if (!tag_elements.region->IsUndefined()) {
        std::string region_str = ConvertToStdString(JSHandle<EcmaString>::Cast(tag_elements.region));
        if (region_str[INTL_INDEX_ZERO] == '\0') {
            return false;
        }
    }
    return true;
}

bool BuildOptionsTags(const JSHandle<EcmaString> &tag, icu::LocaleBuilder *builder, JSHandle<JSTaggedValue> language,
                      JSHandle<JSTaggedValue> script, JSHandle<JSTaggedValue> region)
{
    std::string tag_str = JSLocale::ConvertToStdString(tag);
    auto len = static_cast<int32_t>(tag_str.length());
    ASSERT(len > 0);
    builder->setLanguageTag({tag_str.c_str(), len});
    UErrorCode status = U_ZERO_ERROR;
    icu::Locale locale = builder->build(status);
    locale.canonicalize(status);
    if (U_FAILURE(status) != 0) {
        return false;
    }
    builder->setLocale(locale);

    if (!language->IsUndefined()) {
        std::string language_str = JSLocale::ConvertToStdString(JSHandle<EcmaString>::Cast(language));
        builder->setLanguage(language_str);
        builder->build(status);
        if ((U_FAILURE(status) != 0)) {
            return false;
        }
    }

    if (!script->IsUndefined()) {
        std::string script_str = JSLocale::ConvertToStdString((JSHandle<EcmaString>::Cast(script)));
        builder->setScript(script_str);
        builder->build(status);
        if ((U_FAILURE(status) != 0)) {
            return false;
        }
    }

    if (!region->IsUndefined()) {
        std::string region_str = JSLocale::ConvertToStdString(JSHandle<EcmaString>::Cast(region));
        builder->setRegion(region_str);
        builder->build(status);
        if ((U_FAILURE(status) != 0)) {
            return false;
        }
    }
    return true;
}

bool InsertOptions(JSThread *thread, const JSHandle<JSObject> &options, icu::LocaleBuilder *builder)
{
    const std::vector<std::string> hour_cycle_values = {"h11", "h12", "h23", "h24"};
    const std::vector<std::string> case_first_values = {"upper", "lower", "false"};
    const std::vector<std::string> empty_values = {};
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    std::string str_result;
    bool findca = JSLocale::GetOptionOfString(thread, options, global_const->GetHandledCalendarString(), empty_values,
                                              &str_result);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    if (findca) {
        if (!uloc_toLegacyType(uloc_toLegacyKey("ca"), str_result.c_str())) {
            return false;
        }
        builder->setUnicodeLocaleKeyword("ca", str_result.c_str());
    }

    bool findco = JSLocale::GetOptionOfString(thread, options, global_const->GetHandledCollationString(), empty_values,
                                              &str_result);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    if (findco) {
        if (!uloc_toLegacyType(uloc_toLegacyKey("co"), str_result.c_str())) {
            return false;
        }
        builder->setUnicodeLocaleKeyword("co", str_result.c_str());
    }

    bool findhc = JSLocale::GetOptionOfString(thread, options, global_const->GetHandledHourCycleString(),
                                              hour_cycle_values, &str_result);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    if (findhc) {
        if (!uloc_toLegacyType(uloc_toLegacyKey("hc"), str_result.c_str())) {
            return false;
        }
        builder->setUnicodeLocaleKeyword("hc", str_result.c_str());
    }

    bool findkf = JSLocale::GetOptionOfString(thread, options, global_const->GetHandledCaseFirstString(),
                                              case_first_values, &str_result);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    if (findkf) {
        if (!uloc_toLegacyType(uloc_toLegacyKey("kf"), str_result.c_str())) {
            return false;
        }
        builder->setUnicodeLocaleKeyword("kf", str_result.c_str());
    }

    bool bool_result = false;
    bool findkn =
        JSLocale::GetOptionOfBool(thread, options, global_const->GetHandledNumericString(), false, &bool_result);
    if (findkn) {
        str_result = bool_result ? "true" : "false";
        if (!uloc_toLegacyType(uloc_toLegacyKey("kn"), str_result.c_str())) {
            return false;
        }
        builder->setUnicodeLocaleKeyword("kn", str_result.c_str());
    }

    bool findnu = JSLocale::GetOptionOfString(thread, options, global_const->GetHandledNumberingSystemString(),
                                              empty_values, &str_result);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    if (findnu) {
        if (!uloc_toLegacyType(uloc_toLegacyKey("nu"), str_result.c_str())) {
            return false;
        }
        builder->setUnicodeLocaleKeyword("nu", str_result.c_str());
    }
    return true;
}

JSHandle<JSLocale> JSLocale::InitializeLocale(JSThread *thread, const JSHandle<JSLocale> &locale,
                                              const JSHandle<EcmaString> &locale_string,
                                              const JSHandle<JSObject> &options)
{
    icu::LocaleBuilder builder;
    TagElements tag_elements;
    if (!ApplyOptionsToTag(thread, locale_string, options, tag_elements)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "apply option to tag failed", locale);
    }

    bool res =
        BuildOptionsTags(locale_string, &builder, tag_elements.language, tag_elements.script, tag_elements.region);
    if (!res) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "apply option to tag failed", locale);
    }
    bool insert_result = InsertOptions(thread, options, &builder);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, locale);
    UErrorCode status = U_ZERO_ERROR;
    icu::Locale icu_locale = builder.build(status);
    icu_locale.canonicalize(status);

    if (!insert_result || (U_FAILURE(status) != 0)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "insert or build failed", locale);
    }
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    factory->NewJSIntlIcuData(locale, icu_locale, JSLocale::FreeIcuLocale);
    return locale;
}

bool JSLocale::DealwithLanguageTag(const std::vector<std::string> &containers, size_t &address)
{
    // The abstract operation returns true if locale can be generated from the ABNF grammar in section 2.1 of the RFC,
    // starting with Language-Tag, and does not contain duplicate variant or singleton subtags
    // If language tag is empty, return false.
    if (containers.empty()) {
        return false;
    }

    // a. if the first tag is not language, return false.
    if (!IsLanguageSubtag(containers[0])) {
        return false;
    }

    // if the tag include language only, like "zh" or "de", return true;
    if (containers.size() == 1) {
        return true;
    }

    // Else, then
    // if is unique singleton subtag, script and region tag.
    if (IsExtensionSingleton(containers[1])) {
        return true;
    }

    if (IsScriptSubtag(containers[address])) {
        address++;
        if (containers.size() == address) {
            return true;
        }
    }

    if (IsRegionSubtag(containers[address])) {
        address++;
    }

    for (size_t i = address; i < containers.size(); i++) {
        if (IsExtensionSingleton(containers[i])) {
            return true;
        }
        if (!IsVariantSubtag(containers[i])) {
            return false;
        }
    }
    return true;
}

int ConvertValue(const UErrorCode &status, std::string &value, const std::string &key)
{
    if (status == U_ILLEGAL_ARGUMENT_ERROR || value.empty()) {
        return 1;
    }

    if (value == "yes") {
        value = "true";
    }

    if (key == "kf" && value == "true") {
        return 2;
    }
    return 0;
}

JSHandle<EcmaString> JSLocale::NormalizeKeywordValue(JSThread *thread, const JSHandle<JSLocale> &locale,
                                                     const std::string &key)
{
    icu::Locale *icu_locale = locale->GetIcuLocale();
    UErrorCode status = U_ZERO_ERROR;
    auto value = icu_locale->getUnicodeKeywordValue<std::string>(key, status);

    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    int result = ConvertValue(status, value, key);
    if (result == 1) {
        return JSHandle<EcmaString>::Cast(thread->GlobalConstants()->GetHandledUndefinedString());
    }
    if (result == 2) {
        return factory->GetEmptyString();
    }
    return factory->NewFromStdString(value);
}

JSHandle<EcmaString> JSLocale::ToString(JSThread *thread, const JSHandle<JSLocale> &locale)
{
    icu::Locale *icu_locale = locale->GetIcuLocale();
    if (icu_locale == nullptr) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        return factory->GetEmptyString();
    }
    JSHandle<EcmaString> result = ToLanguageTag(thread, *icu_locale);
    return result;
}

JSHandle<TaggedArray> JSLocale::GetAvailableLocales(JSThread *thread, const char *locale_key, const char *locale_path)
{
    UErrorCode status = U_ZERO_ERROR;
    auto global_const = thread->GlobalConstants();
    JSHandle<EcmaString> special_value = JSHandle<EcmaString>::Cast(global_const->GetHandledEnUsPosixString());
    std::string special_string = ConvertToStdString(special_value);
    UEnumeration *uenum = uloc_openAvailableByType(ULOC_AVAILABLE_WITH_LEGACY_ALIASES, &status);
    std::vector<std::string> all_locales;
    const char *loc = nullptr;
    for (loc = uenum_next(uenum, nullptr, &status); loc != nullptr; loc = uenum_next(uenum, nullptr, &status)) {
        ASSERT(U_SUCCESS(status));
        std::string loc_str(loc);
        std::replace(loc_str.begin(), loc_str.end(), '_', '-');
        if (loc_str == special_string) {
            loc_str = "en-US-u-va-posix";
        }

        if (locale_path != nullptr || locale_key != nullptr) {
            icu::Locale loc(loc_str.c_str());
            bool res = false;
            if (!CheckLocales(loc, locale_key, locale_path, res)) {
                continue;
            }
        }
        bool is_script = false;
        all_locales.push_back(loc_str);
        icu::Locale formal_locale = icu::Locale::createCanonical(loc_str.c_str());
        std::string script_str = formal_locale.getScript();
        is_script = !script_str.empty();
        if (is_script) {
            std::string language_str = formal_locale.getLanguage();
            std::string country_str = formal_locale.getCountry();
            std::string short_locale = icu::Locale(language_str.c_str(), country_str.c_str()).getName();
            std::replace(short_locale.begin(), short_locale.end(), '_', '-');
            all_locales.push_back(short_locale);
        }
    }
    uenum_close(uenum);
    return ConstructLocaleList(thread, all_locales);
}
}  // namespace panda::ecmascript
