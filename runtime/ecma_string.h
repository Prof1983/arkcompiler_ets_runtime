/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_STRING_H
#define ECMASCRIPT_STRING_H

#include <cstddef>
#include <cstdint>
#include <cstring>

#include "libpandabase/utils/utf.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/mem/tagged_object.h"

namespace panda::ecmascript {
template <typename T>
class JSHandle;
class EcmaVM;

template <class T>
static int32_t ComputeHashForData(const T *data, size_t size)
{
    uint32_t hash = 0;
#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
    Span<const T> sp(data, size);
#pragma GCC diagnostic pop
#endif
    for (auto c : sp) {
        constexpr size_t SHIFT = 5;
        hash = (hash << SHIFT) - hash + c;
    }
    return static_cast<int32_t>(hash);
}

class EcmaString : public TaggedObject {
public:
    static EcmaString *Cast(ObjectHeader *object);
    static const EcmaString *ConstCast(const TaggedObject *object);

    static EcmaString *CreateEmptyString(const EcmaVM *vm);
    static EcmaString *CreateFromUtf8(const uint8_t *utf8_data, uint32_t utf8_len, const EcmaVM *vm,
                                      bool can_be_compress,
                                      panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    static EcmaString *CreateFromUtf16(const uint16_t *utf16_data, uint32_t utf16_len, const EcmaVM *vm,
                                       bool can_be_compress,
                                       panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    static EcmaString *Concat(const JSHandle<EcmaString> &str1_handle, const JSHandle<EcmaString> &str2_handle,
                              const EcmaVM *vm);
    static EcmaString *FastSubString(const JSHandle<EcmaString> &src, uint32_t start, uint32_t utf16_len,
                                     const EcmaVM *vm);

    static constexpr uint32_t STRING_COMPRESSED_BIT = 0x1;
    static constexpr uint32_t STRING_INTERN_BIT = 0x2;
    enum CompressedStatus {
        STRING_COMPRESSED,
        STRING_UNCOMPRESSED,
    };

    template <bool VERIFY = true>
    uint16_t At(int32_t index) const;

    PANDA_PUBLIC_API int32_t Compare(const EcmaString *rhs) const;

    bool IsUtf16() const
    {
        return compressed_strings_enabled_ ? ((GetMixLength() & STRING_COMPRESSED_BIT) == STRING_UNCOMPRESSED) : true;
    }

    bool IsUtf8() const
    {
        return compressed_strings_enabled_ ? ((GetMixLength() & STRING_COMPRESSED_BIT) == STRING_COMPRESSED) : false;
    }

    static size_t ComputeDataSizeUtf16(uint32_t length)
    {
        return length * sizeof(uint16_t);
    }

    /// Methods for uncompressed strings (UTF16):
    static size_t ComputeSizeUtf16(uint32_t utf16_len)
    {
        return DATA_OFFSET + ComputeDataSizeUtf16(utf16_len);
    }

    inline uint16_t *GetData() const
    {
        return reinterpret_cast<uint16_t *>(ToUintPtr(this) + DATA_OFFSET);
    }

    const uint16_t *GetDataUtf16() const
    {
        LOG_IF(!IsUtf16(), FATAL, RUNTIME) << "EcmaString: Read data as utf16 for utf8 string";
        return GetData();
    }

    /// Methods for compresses strings (UTF8 or LATIN1):
    static size_t ComputeSizeUtf8(uint32_t utf8_len)
    {
        return DATA_OFFSET + utf8_len;
    }

    /// It's Utf8 format, but without 0 in the end.
    const uint8_t *GetDataUtf8() const
    {
        LOG_IF(IsUtf16(), FATAL, RUNTIME) << "EcmaString: Read data as utf8 for utf16 string";
        return reinterpret_cast<uint8_t *>(GetData());
    }

    size_t GetUtf8Length() const
    {
        if (!IsUtf16()) {
            return GetLength() + 1;  // add place for zero in the end
        }
        return utf::Utf16ToUtf8Size(GetData(), GetLength());
    }

    size_t GetUtf16Length() const
    {
        return GetLength();
    }

    inline size_t CopyDataUtf8(uint8_t *buf, size_t max_length) const
    {
        ASSERT(max_length > 0);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        buf[max_length - 1] = '\0';
        return CopyDataRegionUtf8(buf, 0, GetLength(), max_length) + 1;  // add place for zero in the end
    }

    size_t CopyDataRegionUtf8(uint8_t *buf, size_t start, size_t length, size_t max_length) const
    {
        if (length > max_length) {
            return 0;
        }
        uint32_t len = GetLength();
        if (start + length > len) {
            return 0;
        }
        if (!IsUtf16()) {
            if (length > std::numeric_limits<size_t>::max() / 2 - 1) {  // 2: half
                LOG(FATAL, RUNTIME) << " length is higher than half of size_t::max";
                UNREACHABLE();
            }
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (memcpy_s(buf, max_length, GetDataUtf8() + start, length) != EOK) {
                LOG(FATAL, RUNTIME) << "memcpy_s failed";
                UNREACHABLE();
            }
            return length;
        }
        return utf::ConvertRegionUtf16ToUtf8(GetDataUtf16(), buf, length, max_length - 1, start);
    }

    inline uint32_t CopyDataUtf16(uint16_t *buf, uint32_t max_length) const
    {
        return CopyDataRegionUtf16(buf, 0, GetLength(), max_length);
    }

    uint32_t CopyDataRegionUtf16(uint16_t *buf, uint32_t start, uint32_t length, uint32_t max_length) const
    {
        if (length > max_length) {
            return 0;
        }
        uint32_t len = GetLength();
        if (start + length > len) {
            return 0;
        }
        if (IsUtf16()) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (memcpy_s(buf, ComputeDataSizeUtf16(max_length), GetDataUtf16() + start, ComputeDataSizeUtf16(length)) !=
                EOK) {
                LOG(FATAL, RUNTIME) << "memcpy_s failed";
                UNREACHABLE();
            }
            return length;
        }
        return utf::ConvertRegionUtf8ToUtf16(GetDataUtf8(), buf, len, max_length, start);
    }

    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    inline std::unique_ptr<char[]> GetCString()
    {
        auto length = GetUtf8Length();
        char *buf = new char[length]();
        CopyDataUtf8(reinterpret_cast<uint8_t *>(buf), length);
        // NOLINTNEXTLINE(modernize-avoid-c-arrays)
        return std::unique_ptr<char[]>(buf);
    }

    inline void WriteData(EcmaString *src, uint32_t start, uint32_t dest_size, uint32_t length);
    inline void WriteData(char src, uint32_t start);
    uint32_t GetLength() const
    {
        return GetMixLength() >> 2U;
    }

    void SetIsInternString()
    {
        SetMixLength(GetMixLength() | STRING_INTERN_BIT);
    }

    bool IsInternString() const
    {
        return (GetMixLength() & STRING_INTERN_BIT) != 0;
    }

    size_t ObjectSize() const
    {
        uint32_t length = GetLength();
        return IsUtf16() ? ComputeSizeUtf16(length) : ComputeSizeUtf8(length);
    }

    uint32_t GetHashcode()
    {
        uint32_t hashcode = GetRawHashcode();
        if (hashcode == 0) {
            hashcode = ComputeHashcode();
            SetRawHashcode(hashcode);
        }
        return hashcode;
    }

    int32_t IndexOf(const EcmaString *rhs, int pos = 0) const;

    static constexpr uint32_t GetStringCompressionMask()
    {
        return STRING_COMPRESSED_BIT;
    }

    /// Compares strings by bytes, It doesn't check canonical unicode equivalence.
    static bool StringsAreEqual(EcmaString *str1, EcmaString *str2);
    /// Compares strings by bytes, It doesn't check canonical unicode equivalence.
    static bool StringsAreEqualUtf8(const EcmaString *str1, const uint8_t *utf8_data, uint32_t utf8_len,
                                    bool can_be_compress);
    /// Compares strings by bytes, It doesn't check canonical unicode equivalence.
    static bool StringsAreEqualUtf16(const EcmaString *str1, const uint16_t *utf16_data, uint32_t utf16_len);
    static uint32_t ComputeHashcodeUtf8(const uint8_t *utf8_data, size_t utf8_len, bool can_be_compress);
    static uint32_t ComputeHashcodeUtf16(const uint16_t *utf16_data, uint32_t length);

    static void SetCompressedStringsEnabled(bool val)
    {
        compressed_strings_enabled_ = val;
    }

    static bool GetCompressedStringsEnabled()
    {
        return compressed_strings_enabled_;
    }

    static EcmaString *AllocStringObject(size_t length, bool compressed, const EcmaVM *vm,
                                         panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);

    static bool CanBeCompressed(const uint8_t *utf8_data, uint32_t utf8_len);
    static bool CanBeCompressed(const uint16_t *utf16_data, uint32_t utf16_len);

    // In last bit of mix_length we store if this string is compressed or not.
    ACCESSORS_START(TaggedObjectSize())
    ACCESSORS_PRIMITIVE_FIELD(0, MixLength, uint32_t)
    ACCESSORS_PRIMITIVE_FIELD(1, RawHashcode, uint32_t)
    ACCESSORS_FINISH(2)
    // DATA_OFFSET: the string data stored after the string header.
    // Data can be stored in utf8 or utf16 form according to compressed bit.
    static constexpr size_t DATA_OFFSET = SIZE;  // Empty String size

private:
    void SetLength(uint32_t length, bool compressed = false)
    {
        ASSERT(length < 0x40000000U);
        // Use 0u for compressed/utf8 expression
        SetMixLength((length << 2U) | (compressed ? STRING_COMPRESSED : STRING_UNCOMPRESSED));
    }

    uint16_t *GetDataUtf16Writable()
    {
        LOG_IF(!IsUtf16(), FATAL, RUNTIME) << "EcmaString: Read data as utf16 for utf8 string";
        return GetData();
    }

    uint8_t *GetDataUtf8Writable()
    {
        LOG_IF(IsUtf16(), FATAL, RUNTIME) << "EcmaString: Read data as utf8 for utf16 string";
        return reinterpret_cast<uint8_t *>(GetData());
    }

    uint32_t ComputeHashcode() const;
    static void CopyUtf16AsUtf8(const uint16_t *utf16_from, uint8_t *utf8_to, uint32_t utf16_len);

    static bool compressed_strings_enabled_;

    static bool IsASCIICharacter(uint16_t data)
    {
        // \0 is not considered ASCII in Ecma-Modified-UTF8 [only modify '\u0000']
        return data - 1U < utf::UTF8_1B_MAX;
    }

    /**
     * str1 should have the same length as utf16_data.
     * Converts utf8_data to utf16 and compare it with given utf16_data.
     */
    static bool IsUtf8EqualsUtf16(const uint8_t *utf8_data, size_t utf8_len, const uint16_t *utf16_data,
                                  uint32_t utf16_len);

    template <typename T>
    /// Check that two spans are equal. Should have the same length.
    static bool StringsAreEquals(Span<const T> &str1, Span<const T> &str2);

    template <typename T>
    /// Copy String from src to dst
    static bool StringCopy(Span<T> &dst, size_t dst_max, Span<const T> &src, size_t count);

    template <typename T1, typename T2>
    static int32_t IndexOf(Span<const T1> &lhs_sp, Span<const T2> &rhs_sp, int32_t pos, int32_t max);
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_STRING_H
