/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_PLURAL_RULES_H
#define ECMASCRIPT_JS_PLURAL_RULES_H

#include "js_array.h"
#include "js_hclass.h"
#include "js_intl.h"
#include "js_locale.h"
#include "js_object.h"
#include "js_tagged_value.h"

namespace panda::ecmascript {
constexpr int MNFD_DEFAULT = 0;
constexpr int MXFD_DEFAULT = 3;

class JSPluralRules : public JSObject {
public:
    CAST_CHECK(JSPluralRules, IsJSPluralRules);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, Locale)
    ACCESSORS(1, InitializedPluralRules)
    ACCESSORS(2, Type)
    ACCESSORS(3, MinimumIntegerDigits)
    ACCESSORS(4, MinimumFractionDigits)
    ACCESSORS(5, MaximumFractionDigits)
    ACCESSORS(6, MinimumSignificantDigits)
    ACCESSORS(7, MaximumSignificantDigits)
    ACCESSORS(8, RoundingType)
    ACCESSORS(9, IcuPR)
    ACCESSORS(10, IcuNF)
    ACCESSORS_FINISH(11)

    DECL_DUMP()

    icu::number::LocalizedNumberFormatter *GetIcuNumberFormatter() const;

    static void SetIcuNumberFormatter(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                      const icu::number::LocalizedNumberFormatter &icu_nf,
                                      const DeleteEntryPoint &callback);

    static void FreeIcuNumberFormatter(void *pointer, [[maybe_unused]] void *hint = nullptr);

    icu::PluralRules *GetIcuPluralRules() const;

    static void SetIcuPluralRules(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                  const icu::PluralRules &icu_pr, const DeleteEntryPoint &callback);

    static void FreeIcuPluralRules(void *pointer, [[maybe_unused]] void *hint = nullptr);

    static JSHandle<TaggedArray> BuildLocaleSet(JSThread *thread, const std::set<std::string> &icu_available_locales);

    static JSHandle<TaggedArray> GetAvailableLocales(JSThread *thread);

    // 15.1.1 InitializePluralRules ( pluralRules, locales, options )
    static JSHandle<JSPluralRules> InitializePluralRules(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                                         const JSHandle<JSTaggedValue> &locales,
                                                         const JSHandle<JSTaggedValue> &options);

    // 15.1.4 ResolvePlural ( pluralRules, n )
    static JSHandle<EcmaString> ResolvePlural(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules, double n);

    static void ResolvedOptions(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                const JSHandle<JSObject> &options);
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_JS_PLURAL_RULES_H