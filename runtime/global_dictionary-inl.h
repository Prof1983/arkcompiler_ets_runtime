/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_GLOBAL_DICTIONARY_INL_H
#define ECMASCRIPT_GLOBAL_DICTIONARY_INL_H

#include "plugins/ecmascript/runtime/global_dictionary.h"
#include "plugins/ecmascript/runtime/ecma_global_storage-inl.h"
#include "plugins/ecmascript/runtime/ic/property_box.h"
#include "plugins/ecmascript/runtime/tagged_hash_table-inl.h"

namespace panda::ecmascript {
int GlobalDictionary::Hash(const JSTaggedValue &key)
{
    if (key.IsHeapObject()) {
        if (key.IsSymbol()) {
            auto symbol_string = JSSymbol::Cast(key.GetTaggedObject());
            return static_cast<JSTaggedNumber>(symbol_string->GetHashField()).GetInt();
        }
        if (key.IsString()) {
            auto key_string = EcmaString::Cast(key.GetTaggedObject());
            return key_string->GetHashcode();
        }
    }
    // key must be object
    UNREACHABLE();
}

bool GlobalDictionary::IsMatch(const JSTaggedValue &key, const JSTaggedValue &other)
{
    return key == other;
}

PropertyBox *GlobalDictionary::GetBox(int entry) const
{
    int index = GetEntryIndex(entry) + ENTRY_VALUE_INDEX;
    return PropertyBox::Cast(Get(index).GetTaggedObject());
}

PropertyBox *GlobalDictionary::GetSafeBox(int entry) const
{
    int index = GetEntryIndex(entry) + ENTRY_VALUE_INDEX;
    auto val = Get(index);
    if (!val.IsHeapObject()) {
        return nullptr;
    }
    return PropertyBox::Cast(val.GetTaggedObject());
}

JSTaggedValue GlobalDictionary::GetValue(int entry) const
{
    return GetBox(entry)->GetValue();
}

PropertyAttributes GlobalDictionary::GetAttributes(int entry) const
{
    int index = GetEntryIndex(entry) + ENTRY_DETAILS_INDEX;
    return PropertyAttributes(Get(index).GetInt());
}

void GlobalDictionary::SetEntry(const JSThread *thread, int entry, const JSTaggedValue &key, const JSTaggedValue &value,
                                const PropertyAttributes &attributes)
{
    SetKey(thread, entry, key);
    SetAttributes(thread, entry, attributes);
    UpdateValueAndAttributes(thread, entry, value, attributes);
}

void GlobalDictionary::ClearEntry(const JSThread *thread, int entry)
{
#if ECMASCRIPT_ENABLE_IC
    auto box = GetBox(entry);
    // The PropertyBox can be referenced from compiled code for TryLdGlobalByName
    // TODO(aefremov): use handle instead
    thread->GetEcmaGlobalStorage()->NewGlobalHandle(JSTaggedValue(box).GetRawData());
    box->Clear(thread);
#endif
    JSTaggedValue hole = JSTaggedValue::Hole();
    PropertyAttributes meta_data;
    SetEntry(thread, entry, hole, hole, meta_data);
}

void GlobalDictionary::UpdateValueAndAttributes(const JSThread *thread, int entry, const JSTaggedValue &value,
                                                const PropertyAttributes &meta_data)
{
    UpdateValue(thread, entry, value);
    SetAttributes(thread, entry, meta_data);
}

void GlobalDictionary::SetAttributes(const JSThread *thread, int entry, const PropertyAttributes &meta_data)
{
    int index = GetEntryIndex(entry) + ENTRY_DETAILS_INDEX;
    Set(thread, index, meta_data.GetTaggedValue());
}

void GlobalDictionary::UpdateValue(const JSThread *thread, int entry, const JSTaggedValue &value)
{
    SetValue(thread, entry, value);
}

void GlobalDictionary::GetAllKeys(const JSThread *thread, int offset, TaggedArray *key_array) const
{
    ASSERT_PRINT(offset + EntriesCount() <= static_cast<int>(key_array->GetLength()),
                 "key_array capacity is not enough for dictionary");
    int array_index = 0;
    int size = Size();

    PandaVector<std::pair<JSTaggedValue, uint32_t>> sort_arr;
    for (int hash_index = 0; hash_index < size; hash_index++) {
        JSTaggedValue key = GetKey(hash_index);
        if (!key.IsUndefined() && !key.IsHole()) {
            PropertyAttributes attr = GetAttributes(hash_index);
            std::pair<JSTaggedValue, uint32_t> pair(key, attr.GetOffset());
            sort_arr.push_back(pair);
        }
    }
    std::sort(sort_arr.begin(), sort_arr.end(), CompKey);
    for (auto entry : sort_arr) {
        JSTaggedValue name_key = entry.first;
        key_array->Set(thread, array_index + offset, name_key);
        array_index++;
    }
}

void GlobalDictionary::GetEnumAllKeys(const JSThread *thread, int offset, TaggedArray *key_array, uint32_t *keys) const
{
    ASSERT_PRINT(offset + EntriesCount() <= static_cast<int>(key_array->GetLength()),
                 "key_array capacity is not enough for dictionary");
    int array_index = 0;
    int size = Size();

    PandaVector<std::pair<JSTaggedValue, uint32_t>> sort_arr;
    for (int hash_index = 0; hash_index < size; hash_index++) {
        JSTaggedValue key = GetKey(hash_index);
        if (key.IsString()) {
            PropertyAttributes attr = GetAttributes(hash_index);
            if (attr.IsEnumerable()) {
                std::pair<JSTaggedValue, uint32_t> pair(key, attr.GetOffset());
                sort_arr.push_back(pair);
            }
        }
    }
    std::sort(sort_arr.begin(), sort_arr.end(), CompKey);
    for (const auto &entry : sort_arr) {
        JSTaggedValue name_key = entry.first;
        key_array->Set(thread, array_index + offset, name_key);
        array_index++;
    }
    *keys += array_index;
}

bool GlobalDictionary::CompKey(const std::pair<JSTaggedValue, uint32_t> &a, const std::pair<JSTaggedValue, uint32_t> &b)
{
    return a.second < b.second;
}

void GlobalDictionary::InvalidatePropertyBox(JSThread *thread, const JSHandle<GlobalDictionary> &dict_handle, int entry,
                                             const PropertyAttributes &meta_data)
{
    PropertyBox *box = dict_handle->GetBox(entry);
    PropertyAttributes new_attr(meta_data);

    ASSERT(!box->GetValue().IsHole());
    JSHandle<JSTaggedValue> old_value(thread, box->GetValue());
    dict_handle->SetAttributes(thread, entry, new_attr);
    GlobalDictionary::InvalidateAndReplaceEntry(thread, dict_handle, entry, old_value);
}

void GlobalDictionary::InvalidateAndReplaceEntry(JSThread *thread, const JSHandle<GlobalDictionary> &dict_handle,
                                                 int entry, const JSHandle<JSTaggedValue> &old_value)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    PropertyAttributes attr = dict_handle->GetAttributes(entry);
    ASSERT_PRINT(attr.IsConfigurable(), "property must be configurable");
    ASSERT_PRINT(!dict_handle->GetBox(entry)->GetValue().IsHole(), "value must not be hole");

    // Swap with a copy.
    JSHandle<PropertyBox> new_box = factory->NewPropertyBox(old_value);
    // Cell is officially mutable henceforth.
    attr.SetBoxType(PropertyBoxType::MUTABLE);
    dict_handle->SetAttributes(thread, entry, attr);
    dict_handle->GetBox(entry)->Clear(thread);
    dict_handle->UpdateValue(thread, entry, new_box.GetTaggedValue());
}
}  // namespace panda::ecmascript

#endif
