/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_COMMON_H
#define ECMASCRIPT_COMMON_H

#include "libpandabase/macros.h"
#include "plugins/ecmascript/runtime/ecma_call_params.h"

namespace panda::ecmascript {
enum BarrierMode { SKIP_BARRIER, WRITE_BARRIER, READ_BARRIER };

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define PUBLIC_API PANDA_PUBLIC_API

#ifdef NDEBUG
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define DUMP_API_ATTR __attribute__((unused))
#else
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define DUMP_API_ATTR __attribute__((visibility("default"), used))
#endif
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_COMMON_H
