/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_arraybuffer.h"

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "securec.h"

namespace panda::ecmascript {
void JSArrayBuffer::CopyDataBlockBytes(JSTaggedValue to_block, JSTaggedValue from_block, int32_t from_index,
                                       int32_t count)
{
    void *from_buf = JSNativePointer::Cast(from_block.GetTaggedObject())->GetExternalPointer();
    void *to_buf = JSNativePointer::Cast(to_block.GetTaggedObject())->GetExternalPointer();
    auto *from = static_cast<uint8_t *>(from_buf);
    auto *to = static_cast<uint8_t *>(to_buf);
    if (memcpy_s(to, count, from + from_index, count) != EOK) {  // NOLINT
        LOG_ECMA(FATAL) << "memcpy_s failed";
        UNREACHABLE();
    }
}

void JSArrayBuffer::Attach(JSThread *thread, JSTaggedValue array_buffer_byte_length, JSTaggedValue array_buffer_data)
{
    ASSERT(array_buffer_data.IsNativePointer());
    SetArrayBufferByteLength(thread, array_buffer_byte_length);
    SetArrayBufferData(thread, array_buffer_data);
    EcmaVM *vm = thread->GetEcmaVM();
    vm->PushToArrayDataList(JSNativePointer::Cast(array_buffer_data.GetHeapObject()));
}

void JSArrayBuffer::Detach(JSThread *thread)
{
    JSTaggedValue array_buffer_data = GetArrayBufferData();
    // already detached.
    if (array_buffer_data.IsNull()) {
        return;
    }

    EcmaVM *vm = thread->GetEcmaVM();
    // remove vm's control over arrayBufferData.
    JSNativePointer *js_native_pointer = JSNativePointer::Cast(array_buffer_data.GetHeapObject());
    vm->RemoveArrayDataList(js_native_pointer);
    js_native_pointer->Destroy();

    SetArrayBufferData(thread, JSTaggedValue::Null());
    SetArrayBufferByteLength(thread, JSTaggedValue(0));
}
}  // namespace panda::ecmascript
