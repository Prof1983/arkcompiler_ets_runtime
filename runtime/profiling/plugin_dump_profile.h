    /**
     * Copyright (c) 2022 Huawei Device Co., Ltd.
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

case panda_file::SourceLang::ECMASCRIPT: {
    auto kind = profiling::GetProfileKind(inst->GetOpcode());
    stm << profiling::GetProfilingKindName(kind) << ' ';
    auto &ecma_prof = *reinterpret_cast<ecmascript::EcmaProfileElement *>(profile);
    auto prof_id = inst->GetProfileId();
    switch (kind) {
        case profiling::ProfilingKind::UNARY_ARITH: {
            ecmascript::UnaryOperationProfile p(&ecma_prof[prof_id]);
            p.GetOperandType(0).Dump(stm);
            break;
        }
        case profiling::ProfilingKind::BINARY_ARITH: {
            ecmascript::BinaryOperationProfile p(&ecma_prof[prof_id]);
            p.GetOperandType(0).Dump(stm);
            stm << ", ";
            p.GetOperandType(1).Dump(stm);
            break;
        }
        case profiling::ProfilingKind::CALL: {
            ecmascript::CallProfile *p = ecmascript::CallProfile::FromBuffer(&ecma_prof[prof_id]);
            stm << (int)p->GetCallKind();
            break;
        }
        default:
            LOG(FATAL, DISASSEMBLER) << "Unknown profile";
            break;
    }
    break;
}