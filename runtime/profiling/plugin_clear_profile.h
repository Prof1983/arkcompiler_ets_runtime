    /**
     * Copyright (c) 2022 Huawei Device Co., Ltd.
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

case panda_file::SourceLang::ECMASCRIPT: {
    auto js_method = static_cast<ecmascript::JSMethod *>(method);
    if (js_method->GetProfileSize() == 0) {
        return;
    }
    auto prof_id = inst.GetProfileId();
    if (prof_id == -1) {
        return;
    }
    ASSERT(static_cast<uint32_t>(prof_id) < js_method->GetProfileSize());
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto prof_data = js_method->GetProfilingVector() + prof_id;
    ASSERT(profiling::GetProfileKind(inst.GetOpcode()) == profiling::ProfilingKind::CALL);
    auto profile = ecmascript::CallProfile::FromBuffer(prof_data);
    ASSERT(profile != nullptr);
    auto *profile_table = static_cast<ecmascript::EcmaVM *>(vm)->GetEcmaCallProfileTable();
    profile->Clear(profile_table);
}
