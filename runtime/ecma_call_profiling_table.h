/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_CALL_PROFILING_TABLE_H
#define ECMASCRIPT_CALL_PROFILING_TABLE_H

#include "runtime/include/mem/panda_containers.h"
#include "runtime/mem/gc/bitmap.h"

namespace panda::ecmascript {
class EcmaVM;
class ECMAObject;

class EcmaCallProfilingTable {
public:
    explicit EcmaCallProfilingTable(uint32_t size);
    ~EcmaCallProfilingTable() = default;

    std::optional<uint16_t> InsertNewObject(ECMAObject *js_func);
    void ClearObject(uint16_t idx);
    ECMAObject *GetObject(uint16_t idx) const;
    uintptr_t GetObjectPtr(uint16_t idx) const;
    void Sweep(const GCObjectVisitor &visitor);
    void UpdateMoved();
    void VisitRoots(const GCRootVisitor &visitor);

private:
    NO_COPY_SEMANTIC(EcmaCallProfilingTable);
    NO_MOVE_SEMANTIC(EcmaCallProfilingTable);

private:
    PandaVector<ECMAObject *> call_profiling_table_ GUARDED_BY(table_lock_);
    PandaVector<bool> call_profiling_bit_map_ GUARDED_BY(table_lock_);
    mutable os::memory::Mutex table_lock_;
};

}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_CALL_PROFILING_TABLE_H