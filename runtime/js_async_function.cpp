/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_async_function.h"

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/generator_helper.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript {

JSTaggedValue JSAsyncFuncObject::AsyncFunctionAwait(JSThread *thread, const JSHandle<JSAsyncFuncObject> &async_func_obj,
                                                    const JSHandle<JSTaggedValue> &value)
{
    // 1.Let asyncContext be the running execution context.
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();

    JSHandle<JSTaggedValue> async_ctxt(thread, async_func_obj->GetGeneratorContext());

    // 2. Let promise be ? PromiseResolve(%Promise%, value).
    JSTaggedValue promise_val =
        JSPromise::PromiseResolve(thread, JSHandle<JSTaggedValue>::Cast(env->GetPromiseFunction()), value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSPromise> promise(thread, JSPromise::Cast(promise_val.GetHeapObject()));

    // 4.Let onFulfilled be a new built-in function object as defined in AsyncFunction Awaited Fulfilled.
    JSHandle<JSAsyncAwaitStatusFunction> ful_func = factory->NewJSAsyncAwaitStatusFunction(
        reinterpret_cast<void *>(builtins::promise_handler::AsyncAwaitFulfilled));

    // 5.Let onRejected be a new built-in function object as defined in AsyncFunction Awaited Rejected.
    JSHandle<JSAsyncAwaitStatusFunction> rej_func =
        factory->NewJSAsyncAwaitStatusFunction(reinterpret_cast<void *>(builtins::promise_handler::AsyncAwaitRejected));

    // 6.Set onFulfilled.[[AsyncContext]] to asyncContext.
    // 7.Set onRejected.[[AsyncContext]] to asyncContext.
    ful_func->SetAsyncContext(thread, async_ctxt);
    rej_func->SetAsyncContext(thread, async_ctxt);

    // 8.Let throwawayCapability be ! NewPromiseCapability(%Promise%).
    // 9.Set throwawayCapability.[[Promise]].[[PromiseIsHandled]] to true.
    JSHandle<PromiseCapability> tcap =
        JSPromise::NewPromiseCapability(thread, JSHandle<JSTaggedValue>::Cast(env->GetPromiseFunction()));
    JSHandle<JSPromise>(thread, tcap->GetPromise())->SetPromiseIsHandled(thread, JSTaggedValue::Undefined());

    // 10.Perform ! PerformPromiseThen(promiseCapability.[[Promise]], onFulfilled, onRejected, throwawayCapability).
    [[maybe_unused]] JSTaggedValue pres = builtins::promise::PerformPromiseThen(
        thread, promise, JSHandle<JSTaggedValue>::Cast(ful_func), JSHandle<JSTaggedValue>::Cast(rej_func),
        JSHandle<JSTaggedValue>::Cast(tcap));

    // 11.Remove asyncContext from the execution context stack and restore the execution context that
    //    is at the top of the execution context stack as the running execution context.
    // 12.Set the code evaluation state of asyncContext such that when evaluation is resumed with a Completion
    //    resumptionValue the following steps will be performed:
    //   a.Return resumptionValue.
    // 13.Return.

    return JSTaggedValue::Hole();
}

JSHandle<JSTaggedValue> JSAsyncAwaitStatusFunction::AsyncFunctionAwaitFulfilled(
    JSThread *thread, const JSHandle<JSAsyncAwaitStatusFunction> &func, const JSHandle<JSTaggedValue> &value)
{
    // 1.Let asyncContext be F.[[AsyncContext]].
    JSHandle<GeneratorContext> async_ctxt(thread, func->GetAsyncContext());

    // 2.Let prevContext be the running execution context.
    // 3.Suspend prevContext.
    // 4.Push asyncContext onto the execution context stack; asyncContext is now the running execution context.
    // 5.Resume the suspended evaluation of asyncContext using NormalCompletion(value) as the result of the
    //   operation that suspended it. Let result be the value returned by the resumed computation.
    JSHandle<JSTaggedValue> result =
        GeneratorHelper::Continue(thread, async_ctxt, GeneratorResumeMode::NEXT, value.GetTaggedValue());
    // 6.Assert: When we reach this step, asyncContext has already been removed from the execution context stack
    //   and prevContext is the currently running execution context.

    // 7.Return Completion(result).
    return result;
}

JSHandle<JSTaggedValue> JSAsyncAwaitStatusFunction::AsyncFunctionAwaitRejected(
    JSThread *thread, const JSHandle<JSAsyncAwaitStatusFunction> &func, const JSHandle<JSTaggedValue> &reason)
{
    // 1.Let asyncContext be F.[[AsyncContext]].
    JSHandle<GeneratorContext> async_ctxt(thread, func->GetAsyncContext());

    // 2.Let prevContext be the running execution context.
    // 3.Suspend prevContext.
    // 4.Push asyncContext onto the execution context stack; asyncContext is now the running execution context.
    // 5.Resume the suspended evaluation of asyncContext using Completion{[[Type]]: throw,
    //   [[Value]]: reason, [[Target]]: empty} as the result of the operation that suspended it.
    //   Let result be the value returned by the resumed computation.
    JSHandle<JSTaggedValue> result =
        GeneratorHelper::Continue(thread, async_ctxt, GeneratorResumeMode::THROW, reason.GetTaggedValue());
    // 6.Assert: When we reach this step, asyncContext has already been removed from the execution context stack
    //   and prevContext is the currently running execution context.

    // 7.Return Completion(result).
    return result;
}
}  // namespace panda::ecmascript
