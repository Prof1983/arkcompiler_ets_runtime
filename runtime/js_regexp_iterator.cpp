/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_regexp_iterator.h"

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/builtins/builtins_regexp.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript {
JSTaggedValue JSRegExpIterator::Next(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> input(builtins_common::GetThis(argv));
    // 2. If Type(O) is not Object, throw a TypeError exception.
    // 3. If O does not have all of the internal slots of a RegExp String Iterator Object Instance
    // (see 21.2.7.2), throw a TypeError exception.
    if (!input->IsJSRegExpIterator()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this value is not a regExp iterator", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> undefined_handle(thread->GlobalConstants()->GetHandledUndefined());
    JSHandle<JSRegExpIterator> js_iterator = JSHandle<JSRegExpIterator>::Cast(input);
    // 4. If O.[[Done]] is true, then
    //     a. Return ! CreateIterResultObject(undefined, true).
    if (js_iterator->GetDone()) {
        return JSIterator::CreateIterResultObject(thread, undefined_handle, true).GetTaggedValue();
    }

    // 5. Let R be O.[[IteratingRegExp]].
    // 6. Let S be O.[[IteratedString]].
    // 7. Let global be O.[[Global]].
    // 8. Let fullUnicode be O.[[Unicode]].
    JSHandle<JSTaggedValue> regex_handle(thread, js_iterator->GetIteratingRegExp());
    JSHandle<JSTaggedValue> input_str(thread, js_iterator->GetIteratedString());
    bool global = js_iterator->GetGlobal();
    bool full_unicode = js_iterator->GetUnicode();

    // 9. Let match be ? RegExpExec(R, S).
    JSTaggedValue match = builtins::reg_exp::RegExpExec(thread, regex_handle, input_str, false);
    JSHandle<JSTaggedValue> match_handle(thread, match);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 10. If match is null, then
    //     a. Set O.[[Done]] to true.
    //     b. Return ! CreateIterResultObject(undefined, true).
    // Else,
    if (match_handle->IsNull()) {
        js_iterator->SetDone(true);
        return JSIterator::CreateIterResultObject(thread, undefined_handle, true).GetTaggedValue();
    }
    // a. If global is true, then
    //    i. Let matchStr be ? ToString(? Get(match, "0")).
    if (global) {
        const GlobalEnvConstants *global_constants = thread->GlobalConstants();
        JSHandle<JSTaggedValue> zero_string(global_constants->GetHandledZeroString());
        JSHandle<JSTaggedValue> get_zero(JSObject::GetProperty(thread, match_handle, zero_string).GetValue());
        JSHandle<EcmaString> match_str = JSTaggedValue::ToString(thread, get_zero);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // ii. If matchStr is the empty String, then
        //     1. Let thisIndex be ? ToLength(? Get(R, "lastIndex")).
        //     2. Let nextIndex be ! AdvanceStringIndex(S, thisIndex, fullUnicode).
        //     3. Perform ? Set(R, "lastIndex", 𝔽(nextIndex), true).
        if (match_str->GetLength() == 0) {
            JSHandle<JSTaggedValue> last_index_string(global_constants->GetHandledLastIndexString());
            JSHandle<JSTaggedValue> get_last_index(
                JSObject::GetProperty(thread, regex_handle, last_index_string).GetValue());
            JSTaggedNumber this_index = JSTaggedValue::ToLength(thread, get_last_index);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSTaggedValue next_index(
                builtins::reg_exp::AdvanceStringIndex(input_str, this_index.ToUint32(), full_unicode));
            FastRuntimeStub::FastSetPropertyByValue(thread, regex_handle, last_index_string,
                                                    HandleFromLocal(&next_index));
        }
        // iii. Return ! CreateIterResultObject(match, false).
        return JSIterator::CreateIterResultObject(thread, match_handle, false).GetTaggedValue();
    }
    // b. Else,
    //    i. Set O.[[Done]] to true.
    //   ii. Return ! CreateIterResultObject(match, false).
    js_iterator->SetDone(true);
    return JSIterator::CreateIterResultObject(thread, match_handle, false).GetTaggedValue();
}

JSHandle<JSTaggedValue> JSRegExpIterator::CreateRegExpStringIterator(JSThread *thread,
                                                                     const JSHandle<JSTaggedValue> &matcher,
                                                                     const JSHandle<EcmaString> &input_str, bool global,
                                                                     bool full_unicode)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (!matcher->IsJSRegExp()) {
        JSHandle<JSTaggedValue> undefined_handle(thread->GlobalConstants()->GetHandledUndefined());
        THROW_TYPE_ERROR_AND_RETURN(thread, "matcher is not JSRegExp", undefined_handle);
    }
    JSHandle<JSTaggedValue> iter(factory->NewJSRegExpIterator(matcher, input_str, global, full_unicode));
    return iter;
}
}  // namespace panda::ecmascript
