/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_string_table.h"

#include "plugins/ecmascript/runtime/ecma_string-inl.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/mem/ecma_string.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript {
EcmaStringTable::EcmaStringTable(const EcmaVM *vm) : vm_(vm) {}

EcmaString *EcmaStringTable::GetString(const uint8_t *utf8_data, uint32_t utf8_len, bool can_be_compress) const
{
    uint32_t hash_code = EcmaString::ComputeHashcodeUtf8(utf8_data, utf8_len, can_be_compress);
    os::memory::ReadLockHolder holder(table_lock_);
    for (auto it = table_.find(hash_code); it != table_.end(); it++) {
        auto founded_string = it->second;
        if (EcmaString::StringsAreEqualUtf8(founded_string, utf8_data, utf8_len, can_be_compress)) {
            return founded_string;
        }
    }
    return nullptr;
}

EcmaString *EcmaStringTable::GetString(const uint16_t *utf16_data, uint32_t utf16_len) const
{
    uint32_t hash_code = EcmaString::ComputeHashcodeUtf16(const_cast<uint16_t *>(utf16_data), utf16_len);
    os::memory::ReadLockHolder holder(table_lock_);
    for (auto it = table_.find(hash_code); it != table_.end(); it++) {
        auto founded_string = it->second;
        if (EcmaString::StringsAreEqualUtf16(founded_string, utf16_data, utf16_len)) {
            return founded_string;
        }
    }
    return nullptr;
}

EcmaString *EcmaStringTable::GetString(EcmaString *string) const
{
    auto hash = string->GetHashcode();
    os::memory::ReadLockHolder holder(table_lock_);
    for (auto it = table_.find(hash); it != table_.end(); it++) {
        auto founded_string = it->second;
        if (EcmaString::StringsAreEqual(founded_string, string)) {
            return founded_string;
        }
    }
    return nullptr;
}

ObjectHeader *EcmaStringTable::ResolveString(const panda_file::File &pf, panda_file::File::EntityId id)
{
    auto found_str = pf.GetStringData(id);
    const auto factory = vm_->GetFactory();

    if (UNLIKELY(found_str.utf16_length == 0)) {
        return *(factory->GetEmptyString());
    }

    return EcmaString::Cast(this->GetOrInternString(found_str.data, found_str.utf16_length, found_str.is_ascii));
}

void EcmaStringTable::InternString(EcmaString *string)
{
    if (string->IsInternString()) {
        return;
    }
    os::memory::WriteLockHolder holder(table_lock_);
    table_.insert(std::pair<uint32_t, EcmaString *>(string->GetHashcode(), string));
    string->SetIsInternString();
}

void EcmaStringTable::InternEmptyString(EcmaString *empty_str)
{
    InternString(empty_str);
}

EcmaString *EcmaStringTable::GetOrInternString(const uint8_t *utf8_data, uint32_t utf8_len, bool can_be_compress,
                                               panda::SpaceType space_type)
{
    EcmaString *result = GetString(utf8_data, utf8_len, can_be_compress);
    if (result != nullptr) {
        return result;
    }

    result = EcmaString::CreateFromUtf8(utf8_data, utf8_len, vm_, can_be_compress, space_type);
    InternString(result);
    return result;
}

EcmaString *EcmaStringTable::GetOrInternString(const uint16_t *utf16_data, uint32_t utf16_len, bool can_be_compress,
                                               panda::SpaceType space_type)
{
    EcmaString *result = GetString(utf16_data, utf16_len);
    if (result != nullptr) {
        return result;
    }

    result = EcmaString::CreateFromUtf16(utf16_data, utf16_len, vm_, can_be_compress, space_type);
    InternString(result);
    return result;
}

EcmaString *EcmaStringTable::GetOrInternString(EcmaString *string)
{
    if (string->IsInternString()) {
        return string;
    }

    EcmaString *result = GetString(string);
    if (result != nullptr) {
        return result;
    }
    InternString(string);
    return string;
}

void EcmaStringTable::VisitRoots(const StringTable::StringVisitor &visitor,
                                 [[maybe_unused]] mem::VisitGCRootFlags flags)
{
    os::memory::ReadLockHolder holder(table_lock_);
    for (const auto &v : table_) {
        visitor(v.second);
    }
}

void EcmaStringTable::Sweep(const GCObjectVisitor &visitor)
{
    os::memory::WriteLockHolder holder(table_lock_);
    for (auto it = table_.begin(); it != table_.end();) {
        auto *object = it->second;
        if (object->IsForwarded()) {
            ASSERT(visitor(object) != ObjectStatus::DEAD_OBJECT);
            ObjectHeader *fwd_string = panda::mem::GetForwardAddress(object);
            it->second = static_cast<EcmaString *>(fwd_string);
            ++it;
            LOG(DEBUG, GC) << "StringTable: forward " << std::hex << object << " -> " << fwd_string;
        } else if (visitor(object) == ObjectStatus::DEAD_OBJECT) {
            LOG(DEBUG, GC) << "StringTable: delete string " << std::hex << object
                           << ", val = " << ConvertToPandaString(object);
            table_.erase(it++);
        } else {
            ++it;
        }
    }
}

bool EcmaStringTable::UpdateMoved()
{
    bool updated = false;
    os::memory::WriteLockHolder holder(table_lock_);
    for (auto it = table_.begin(); it != table_.end();) {
        ObjectHeader *object = it->second;
        if (object->IsForwarded()) {
            ObjectHeader *fwd_string = panda::mem::GetForwardAddress(object);
            it->second = static_cast<EcmaString *>(fwd_string);
            LOG(DEBUG, GC) << "StringTable: forward " << std::hex << object << " -> " << fwd_string;
            updated = true;
        }
        ++it;
    }
    return updated;
}

void EcmaStringTable::SweepWeakReference(const WeakRootVisitor &visitor)
{
    os::memory::WriteLockHolder holder(table_lock_);
    for (auto it = table_.begin(); it != table_.end();) {
        auto *object = it->second;
        auto fwd = visitor(object);
        if (fwd == nullptr) {
            LOG(DEBUG, GC) << "StringTable: delete string " << std::hex << object
                           << ", val = " << ConvertToPandaString(object);
            table_.erase(it++);
        } else if (fwd != object) {
            it->second = static_cast<EcmaString *>(fwd);
            ++it;
            LOG(DEBUG, GC) << "StringTable: forward " << std::hex << object << " -> " << fwd;
        } else {
            ++it;
        }
    }
}
}  // namespace panda::ecmascript
