/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_weak_container.h"

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "libpandabase/utils/bit_utils.h"

namespace panda::ecmascript {
void JSWeakMap::Set(JSThread *thread, const JSHandle<JSWeakMap> &map, const JSHandle<JSTaggedValue> &key,
                    const JSHandle<JSTaggedValue> &value)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    if (!LinkedHashMap::IsKey(JSTaggedValue(key.GetTaggedValue()))) {
        THROW_TYPE_ERROR(thread, "the value must be Key of JSMap");
    }
    JSHandle<LinkedHashMap> map_handle(thread, LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject()));

    auto result = LinkedHashMap::Set(thread, map_handle, key, value);
    map->SetLinkedMap(thread, result);
}

bool JSWeakMap::Delete(JSThread *thread, const JSHandle<JSWeakMap> &map, const JSHandle<JSTaggedValue> &key)
{
    JSHandle<LinkedHashMap> map_handle(thread, LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject()));
    if (!LinkedHashSet::IsKey(key.GetTaggedValue())) {
        return false;
    }
    int hash = LinkedHash::Hash(key.GetTaggedValue());
    int entry = map_handle->FindElement(key.GetTaggedValue(), hash);
    if (entry == -1) {
        return false;
    }
    map_handle->RemoveEntry(thread, entry);

    JSHandle<LinkedHashMap> new_map = LinkedHashMap::Shrink(thread, map_handle);
    map->SetLinkedMap(thread, new_map);
    return true;
}

bool JSWeakMap::Has(JSTaggedValue key, int hash) const
{
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->Has(key, hash);
}

JSTaggedValue JSWeakMap::Get(JSTaggedValue key, int hash) const
{
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->Get(key, hash);
}

int JSWeakMap::GetSize() const
{
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->NumberOfElements();
}

void JSWeakSet::Add(JSThread *thread, const JSHandle<JSWeakSet> &weak_set, const JSHandle<JSTaggedValue> &value)
{
    if (!LinkedHashSet::IsKey(value.GetTaggedValue())) {
        THROW_TYPE_ERROR(thread, "the value must be Key of JSWeakSet");
    }
    JSHandle<LinkedHashSet> weak_set_handle(thread, LinkedHashSet::Cast(weak_set->GetLinkedSet().GetTaggedObject()));

    auto result = LinkedHashSet::Add(thread, weak_set_handle, value);
    weak_set->SetLinkedSet(thread, result);
}

bool JSWeakSet::Delete(JSThread *thread, const JSHandle<JSWeakSet> &weak_set, const JSHandle<JSTaggedValue> &value)
{
    JSHandle<LinkedHashSet> weak_set_handle(thread, LinkedHashSet::Cast(weak_set->GetLinkedSet().GetTaggedObject()));
    int hash = LinkedHash::Hash(value.GetTaggedValue());
    int entry = weak_set_handle->FindElement(value.GetTaggedValue(), hash);
    if (entry == -1) {
        return false;
    }
    weak_set_handle->RemoveEntry(thread, entry);
    JSHandle<LinkedHashSet> new_set = LinkedHashSet::Shrink(thread, weak_set_handle);
    weak_set->SetLinkedSet(thread, new_set);
    return true;
}

bool JSWeakSet::Has(JSTaggedValue value, int hash) const
{
    return LinkedHashSet::Cast(GetLinkedSet().GetTaggedObject())->Has(value, hash);
}

int JSWeakSet::GetSize() const
{
    return LinkedHashSet::Cast(GetLinkedSet().GetTaggedObject())->NumberOfElements();
}
}  // namespace panda::ecmascript
