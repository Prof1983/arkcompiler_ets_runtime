/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_for_in_iterator.h"

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/global_dictionary-inl.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/runtime/tagged_queue-inl.h"
#include "plugins/ecmascript/runtime/tagged_queue.h"

namespace panda::ecmascript {

bool JSForInIterator::CheckObjProto(const JSThread *thread, const JSHandle<JSForInIterator> &it)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> object(thread, it->GetObject());
    if (!object->IsJSObject()) {
        return false;
    }
    auto *hclass = object->GetTaggedObject()->GetClass();
    JSType js_type = hclass->GetObjectType();
    if (js_type != JSType::JS_OBJECT) {
        return false;
    }
    JSTaggedValue proto = hclass->GetPrototype();
    if (!proto.IsJSObject()) {
        return false;
    }
    return hclass->GetPrototype().GetTaggedObject()->GetClass() ==
           env->GetObjectFunctionPrototypeClass().GetTaggedValue().GetTaggedObject()->GetClass();
}

void JSForInIterator::FastGetAllEnumKeys(const JSThread *thread, const JSHandle<JSForInIterator> &it,
                                         const JSHandle<JSTaggedValue> &object)
{
    JSMutableHandle<JSTaggedValue> value(thread, JSTaggedValue::Undefined());
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj(object);
    uint32_t num_of_elements = obj->GetNumberOfElements();
    uint32_t num_of_keys = obj->GetNumberOfKeys();
    JSHandle<TaggedQueue> remaining = factory->NewTaggedQueue(num_of_elements + num_of_keys + 1);
    if (num_of_elements > 0) {
        uint32_t element_index = 0;
        if (obj->IsJSPrimitiveRef() && JSPrimitiveRef::Cast(*obj)->IsString()) {
            element_index = JSPrimitiveRef::Cast(*obj)->GetStringLength();
            for (uint32_t i = 0; i < element_index; i++) {
                value.Update(factory->NewFromCanBeCompressString(ToPandaString(i)).GetTaggedValue());
                TaggedQueue::PushFixedQueue(thread, remaining, value);
            }
        } else {
            JSHandle<TaggedArray> elements(thread, obj->GetElements());
            if (!elements->IsDictionaryMode()) {
                uint32_t elements_len = elements->GetLength();
                for (uint32_t i = 0; i < elements_len; ++i) {
                    if (!elements->Get(i).IsHole()) {
                        value.Update(factory->NewFromCanBeCompressString(ToPandaString(i)).GetTaggedValue());
                        TaggedQueue::PushFixedQueue(thread, remaining, value);
                    }
                }
            } else {
                JSHandle<NumberDictionary> number_dic(elements);
                int size = number_dic->Size();
                PandaVector<JSTaggedValue> sort_arr;
                for (int hash_index = 0; hash_index < size; hash_index++) {
                    JSTaggedValue key = number_dic->GetKey(hash_index);
                    if (!key.IsUndefined() && !key.IsHole()) {
                        PropertyAttributes attr = number_dic->GetAttributes(hash_index);
                        if (attr.IsEnumerable()) {
                            sort_arr.push_back(JSTaggedValue(static_cast<uint32_t>(key.GetInt())));
                        }
                    }
                }
                std::sort(sort_arr.begin(), sort_arr.end(), NumberDictionary::CompKey);
                for (const auto &entry : sort_arr) {
                    value.Update(factory->NewFromCanBeCompressString(ToPandaString(entry.GetInt())).GetTaggedValue());
                    TaggedQueue::PushFixedQueue(thread, remaining, value);
                }
            }
        }
    }
    if (num_of_keys > 0) {
        if (obj->IsJSGlobalObject()) {
            GlobalDictionary *dict = GlobalDictionary::Cast(obj->GetProperties().GetTaggedObject());
            int size = dict->Size();
            PandaVector<std::pair<JSTaggedValue, uint32_t>> sort_arr;
            for (int hash_index = 0; hash_index < size; hash_index++) {
                JSTaggedValue key = dict->GetKey(hash_index);
                if (!key.IsUndefined() && !key.IsHole()) {
                    PropertyAttributes attr = dict->GetAttributes(hash_index);
                    if (attr.IsEnumerable()) {
                        std::pair<JSTaggedValue, uint32_t> pair(key, attr.GetOffset());
                        sort_arr.emplace_back(pair);
                    }
                }
            }
            std::sort(sort_arr.begin(), sort_arr.end(), GlobalDictionary::CompKey);
            for (const auto &entry : sort_arr) {
                JSTaggedValue name_key = entry.first;
                if (name_key.IsString()) {
                    value.Update(name_key);
                    TaggedQueue::PushFixedQueue(thread, remaining, value);
                }
            }
        } else {
            JSHandle<TaggedArray> properties_arr(thread, obj->GetProperties());
            if (!properties_arr->IsDictionaryMode()) {
                JSHClass *js_hclass = obj->GetJSHClass();
                JSTaggedValue enum_cache = js_hclass->GetEnumCache();
                if (!enum_cache.IsNull()) {
                    JSHandle<TaggedArray> cache(thread, enum_cache);
                    uint32_t length = cache->GetLength();
                    if (length != num_of_keys) {
                        JSHandle<LayoutInfo> layout_info_handle(thread, js_hclass->GetLayout());
                        for (uint32_t i = 0; i < num_of_keys; i++) {
                            JSTaggedValue key = layout_info_handle->GetKey(i);
                            if (key.IsString()) {
                                value.Update(key);
                                if (layout_info_handle->GetAttr(i).IsEnumerable()) {
                                    TaggedQueue::PushFixedQueue(thread, remaining, value);
                                }
                            }
                        }
                    } else {
                        for (uint32_t i = 0; i < length; i++) {
                            JSTaggedValue key = cache->Get(i);
                            if (key.IsString()) {
                                value.Update(key);
                                TaggedQueue::PushFixedQueue(thread, remaining, value);
                            }
                        }
                    }
                } else {
                    JSHandle<LayoutInfo> layout_info_handle(thread, js_hclass->GetLayout());
                    for (uint32_t i = 0; i < num_of_keys; i++) {
                        JSTaggedValue key = layout_info_handle->GetKey(i);
                        if (key.IsString()) {
                            value.Update(key);
                            if (layout_info_handle->GetAttr(i).IsEnumerable()) {
                                TaggedQueue::PushFixedQueue(thread, remaining, value);
                            }
                        }
                    }
                }
            } else {
                JSHandle<NameDictionary> name_dic(properties_arr);
                int size = name_dic->Size();
                PandaVector<std::pair<JSTaggedValue, PropertyAttributes>> sort_arr;
                for (int hash_index = 0; hash_index < size; hash_index++) {
                    JSTaggedValue key = name_dic->GetKey(hash_index);
                    if (key.IsString()) {
                        PropertyAttributes attr = name_dic->GetAttributes(hash_index);
                        if (attr.IsEnumerable()) {
                            std::pair<JSTaggedValue, PropertyAttributes> pair(key, attr);
                            sort_arr.emplace_back(pair);
                        }
                    }
                }
                std::sort(sort_arr.begin(), sort_arr.end(), NameDictionary::CompKey);
                for (const auto &entry : sort_arr) {
                    value.Update(entry.first);
                    TaggedQueue::PushFixedQueue(thread, remaining, value);
                }
            }
        }
    }
    it->SetRemainingKeys(thread, remaining);
    it->SetWasVisited(thread, JSTaggedValue::True());
}

static bool IsInVisitedKeys(const JSHandle<JSTaggedValue> &key, const JSMutableHandle<TaggedQueue> &visited)
{
    uint32_t len = visited->Size();
    for (uint32_t i = 0; i < len; i++) {
        if (JSTaggedValue::SameValue(key.GetTaggedValue(), visited->Get(i))) {
            return true;
        }
    }

    return false;
}

void JSForInIterator::SlowGetAllEnumKeys(JSThread *thread, const JSHandle<JSForInIterator> &it,
                                         const JSHandle<JSTaggedValue> &object)
{
    JSMutableHandle<TaggedQueue> visited(thread, it->GetVisitedKeys());
    JSMutableHandle<JSTaggedValue> value(thread, JSTaggedValue::Undefined());
    JSMutableHandle<TaggedQueue> remaining(thread, it->GetRemainingKeys());
    JSHandle<TaggedArray> arr = JSTaggedValue::GetOwnPropertyKeys(thread, object);
    uint32_t len = arr->GetLength();
    for (uint32_t i = 0; i < len; i++) {
        value.Update(arr->Get(i));
        PropertyDescriptor desc(thread);
        if (value->IsString() && JSTaggedValue::GetOwnProperty(thread, object, value, desc) &&
            !IsInVisitedKeys(JSHandle<JSTaggedValue>(thread, value.GetTaggedValue()), visited)) {
            if (!desc.IsEnumerable()) {
                TaggedQueue *new_visited_queue = TaggedQueue::Push(thread, visited, value);
                visited.Update(JSTaggedValue(new_visited_queue));
                continue;
            }

            TaggedQueue *new_queue = TaggedQueue::Push(thread, remaining, value);
            remaining.Update(JSTaggedValue(new_queue));
        }
    }
    it->SetRemainingKeys(thread, remaining);
    it->SetVisitedKeys(thread, visited);
    it->SetWasVisited(thread, JSTaggedValue::True());
}

static JSTaggedNumber GetTaggedQueueLengthWithStartOffset(const JSHandle<TaggedQueue> &tq)
{
    if (tq->Empty()) {
        return JSTaggedNumber(0);
    }

    return JSTaggedNumber(tq->Size());
}

std::pair<JSTaggedValue, bool> JSForInIterator::NextInternal(JSThread *thread, const JSHandle<JSForInIterator> &it)
{
    bool not_modi_obj_proto = true;
    not_modi_obj_proto = CheckObjProto(thread, it);
    while (true) {
        JSHandle<JSTaggedValue> object(thread, it->GetObject());
        if (object->IsNull() || object->IsUndefined()) {
            return std::make_pair(JSTaggedValue::Undefined(), true);
        }

        JSMutableHandle<TaggedQueue> remaining(thread, it->GetRemainingKeys());
        if (it->GetWasVisited().IsFalse()) {
            if (object->IsJSObject() && not_modi_obj_proto) {
                FastGetAllEnumKeys(thread, it, object);
            } else {
                SlowGetAllEnumKeys(thread, it, object);
            }
            remaining.Update(it->GetRemainingKeys());
            it->SetFastRemainingIndex(GetTaggedQueueLengthWithStartOffset(remaining));
        }

        JSMutableHandle<TaggedQueue> visited(thread, it->GetVisitedKeys());

        uint32_t fast_index = it->GetFastRemainingIndex().GetInt();
        uint32_t remain_length = GetTaggedQueueLengthWithStartOffset(remaining).GetInt();

        if (fast_index != remain_length) {
            // Restore correct state of FastRemainingIndex.
            uint32_t element_count = remain_length - fast_index;
            for (uint32_t i = 0; i < element_count; ++i) {
                JSHandle<JSTaggedValue> key(thread, remaining->Pop(thread));
                auto new_queue = JSTaggedValue(TaggedQueue::Push(thread, visited, key));
                visited.Update(new_queue);
                it->SetVisitedKeys(thread, new_queue);
            }
            it->SetFastRemainingIndex(GetTaggedQueueLengthWithStartOffset(remaining));
        }

        if (!remaining->Empty()) {
            JSTaggedValue r = remaining->Pop(thread);
            if (object->IsJSObject() && not_modi_obj_proto) {
                it->SetFastRemainingIndex(GetTaggedQueueLengthWithStartOffset(remaining));
                return std::make_pair(r, false);
            }
            JSHandle<JSTaggedValue> key(thread, r);

            auto new_queue = JSTaggedValue(TaggedQueue::Push(thread, visited, key));
            it->SetFastRemainingIndex(GetTaggedQueueLengthWithStartOffset(remaining));
            visited.Update(new_queue);
            it->SetVisitedKeys(thread, new_queue);
            return std::make_pair(key.GetTaggedValue(), false);
        }

        if (not_modi_obj_proto) {
            return std::make_pair(JSTaggedValue::Undefined(), true);
        }

        JSTaggedValue proto = JSHandle<JSObject>::Cast(object)->GetPrototype(thread);
        it->SetObject(thread, proto);
        it->SetWasVisited(thread, JSTaggedValue::False());
    }
}

// 13.7.5.16.2.1 %ForInIteratorPrototype%.next ( )
JSTaggedValue JSForInIterator::Next(EcmaRuntimeCallInfo *msg)
{
    ASSERT(msg);
    JSThread *thread = msg->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSForInIterator> it(builtins_common::GetThis(msg));
    ASSERT(it->IsForinIterator());
    std::pair<JSTaggedValue, bool> res = NextInternal(thread, it);
    return res.first;
}
}  // namespace panda::ecmascript
