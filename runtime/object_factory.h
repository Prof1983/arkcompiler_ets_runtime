/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_OBJECT_FACTORY_H
#define ECMASCRIPT_OBJECT_FACTORY_H

#include "plugins/ecmascript/runtime/base/error_type.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/js_function_kind.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_native_pointer.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/mem/mem_manager.h"
#include "plugins/ecmascript/runtime/object_wrapper.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

namespace panda::ecmascript {
class JSObject;
class JSArray;
class BigInt;
class JSSymbol;
class JSFunctionBase;
class JSFunction;
class JSBoundFunction;
class JSProxyRevocFunction;
class JSAsyncAwaitStatusFunction;
class JSAsyncGeneratorResolveNextFunction;
class JSAsyncFromSyncIteratorValueUnwrapFunction;
class JSPrimitiveRef;
class GlobalEnv;
class GlobalEnvConstants;
class AccessorData;
class JSGlobalObject;
class LexicalEnv;
class JSDate;
class JSProxy;
class JSRealm;
class JSArguments;
class TaggedQueue;
class JSForInIterator;
class JSSet;
class JSMap;
class JSWeakRef;
class JSRegExp;
class JSSetIterator;
class JSRegExpIterator;
class JSMapIterator;
class JSArrayIterator;
class JSStringIterator;
class JSGeneratorObject;
class CompletionRecord;
class GeneratorContext;
class JSArrayBuffer;
class JSDataView;
class JSPromise;
class JSPromiseReactionsFunction;
class JSPromiseExecutorFunction;
class JSPromiseAllResolveElementFunction;
class PromiseReaction;
class PromiseCapability;
class PromiseIteratorRecord;
class JSAsyncFuncObject;
class JSAsyncFunction;
class JSAsyncGeneratorObject;
class JSAsyncFromSyncIteratorObject;
class PromiseRecord;
class JSLocale;
class ResolvingFunctionsRecord;
class JSFunctionExtraInfo;
class EcmaVM;
class ConstantPool;
class Program;
class EcmaModule;
class LayoutInfo;
class JSIntlBoundFunction;
class FreeObject;
class JSNativePointer;
class DynamicObjectHelpersTest;

namespace job {
class MicroJobQueue;
class PendingJob;
}  // namespace job
class TransitionHandler;
class PrototypeHandler;
class PropertyBox;
class ProtoChangeMarker;
class ProtoChangeDetails;
class ProfileTypeInfo;
class ClassInfoExtractor;

namespace builtins {
class Builtins;
}  // namespace builtins

enum class PrimitiveType : uint8_t;
enum class IterationKind;

using ErrorType = base::ErrorType;
using DeleteEntryPoint = void (*)(void *, void *);

enum class RemoveSlots { YES, NO };
class ObjectFactory {
public:
    explicit ObjectFactory(JSThread *thread);

    JSHandle<ProfileTypeInfo> NewProfileTypeInfo(uint32_t length);
    JSHandle<ConstantPool> NewConstantPool(uint32_t capacity);
    JSHandle<Program> NewProgram();
    JSHandle<EcmaModule> NewEmptyEcmaModule();

    JSHandle<JSObject> GetJSError(const ErrorType &error_type, const char *data = nullptr);

    JSHandle<JSObject> NewJSError(const ErrorType &error_type, const JSHandle<EcmaString> &message);

    JSHandle<TransitionHandler> NewTransitionHandler();

    JSHandle<PrototypeHandler> NewPrototypeHandler();

    JSHandle<JSObject> NewEmptyJSObject(panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);

    // use for others create, prototype is Function.prototype
    // use for native function
    PANDA_PUBLIC_API JSHandle<JSFunction> NewJSFunction(const JSHandle<GlobalEnv> &env,
                                                        const void *native_func = nullptr,
                                                        FunctionKind kind = FunctionKind::NORMAL_FUNCTION);
    // use for method
    JSHandle<JSFunction> NewJSFunction(const JSHandle<GlobalEnv> &env, JSMethod *method,
                                       FunctionKind kind = FunctionKind::NORMAL_FUNCTION);

    JSHandle<JSFunction> NewJSNativeErrorFunction(const JSHandle<GlobalEnv> &env, const void *native_func = nullptr);

    JSHandle<JSFunction> NewSpecificTypedArrayFunction(const JSHandle<GlobalEnv> &env,
                                                       const void *native_func = nullptr);

    JSHandle<JSObject> OrdinaryNewJSObjectCreate(const JSHandle<JSTaggedValue> &proto);

    JSHandle<JSBoundFunction> NewJSBoundFunction(const JSHandle<JSFunctionBase> &target,
                                                 const JSHandle<JSTaggedValue> &bound_this,
                                                 const JSHandle<TaggedArray> &args);

    JSHandle<JSIntlBoundFunction> NewJSIntlBoundFunction(const void *native_func = nullptr, int function_length = 1);

    JSHandle<JSProxyRevocFunction> NewJSProxyRevocFunction(const JSHandle<JSProxy> &proxy,
                                                           const void *native_func = nullptr);

    JSHandle<JSAsyncAwaitStatusFunction> NewJSAsyncAwaitStatusFunction(const void *native_func);
    JSHandle<JSAsyncGeneratorResolveNextFunction> NewJSAsyncGeneratorResolveNextFunction(const void *native_func);
    JSHandle<JSAsyncFromSyncIteratorValueUnwrapFunction> NewJSAsyncFromSyncIteratorValueUnwrapFunction(
        const void *native_func);

    JSHandle<JSFunction> NewJSGeneratorFunction(JSMethod *method);
    JSHandle<JSFunction> NewJSAsyncGeneratorFunction(JSMethod *method);

    JSHandle<JSAsyncFunction> NewAsyncFunction(JSMethod *method);

    JSHandle<JSGeneratorObject> NewJSGeneratorObject(JSHandle<JSTaggedValue> generator_function);
    JSHandle<JSAsyncGeneratorObject> NewJSAsyncGeneratorObject(JSHandle<JSTaggedValue> async_generator_function);
    JSHandle<JSAsyncFromSyncIteratorObject> NewJSAsyncFromSyncIteratorObject();

    JSHandle<JSAsyncFuncObject> NewJSAsyncFuncObject();

    JSHandle<JSPrimitiveRef> NewJSPrimitiveRef(const JSHandle<JSFunction> &function,
                                               const JSHandle<JSTaggedValue> &object);
    JSHandle<JSPrimitiveRef> NewJSPrimitiveRef(PrimitiveType type, const JSHandle<JSTaggedValue> &object);

    // get JSHClass for Ecma ClassLinker
    JSHandle<GlobalEnv> NewGlobalEnv(JSHClass *global_env_class);

    // get JSHClass for Ecma ClassLinker
    JSHandle<LexicalEnv> NewLexicalEnv(int num_slots);

    inline LexicalEnv *InlineNewLexicalEnv(int num_slots);

    JSHandle<JSSymbol> NewJSSymbol();

    JSHandle<JSSymbol> NewPrivateSymbol();

    JSHandle<JSSymbol> NewPrivateNameSymbol(const JSHandle<JSTaggedValue> &name);

    JSHandle<JSSymbol> NewWellKnownSymbol(const JSHandle<JSTaggedValue> &name);

    JSHandle<JSSymbol> NewPublicSymbol(const JSHandle<JSTaggedValue> &name);

    JSHandle<JSSymbol> NewSymbolWithTable(const JSHandle<JSTaggedValue> &name);

    JSHandle<JSSymbol> NewPrivateNameSymbolWithChar(const char *description);

    JSHandle<JSSymbol> NewWellKnownSymbolWithChar(const char *description);

    JSHandle<JSSymbol> NewPublicSymbolWithChar(const char *description);

    JSHandle<JSSymbol> NewSymbolWithTableWithChar(const char *description);

    JSHandle<AccessorData> NewAccessorData(panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    JSHandle<AccessorData> NewInternalAccessor(void *setter, void *getter);

    JSHandle<PromiseCapability> NewPromiseCapability();

    JSHandle<PromiseReaction> NewPromiseReaction();

    JSHandle<PromiseRecord> NewPromiseRecord();

    JSHandle<ResolvingFunctionsRecord> NewResolvingFunctionsRecord();

    JSHandle<PromiseIteratorRecord> NewPromiseIteratorRecord(const JSHandle<JSTaggedValue> &itor,
                                                             const JSHandle<JSTaggedValue> &done);

    JSHandle<job::MicroJobQueue> NewMicroJobQueue();

    JSHandle<job::PendingJob> NewPendingJob(const JSHandle<JSFunction> &func, const JSHandle<TaggedArray> &argv);

    JSHandle<JSFunctionExtraInfo> NewFunctionExtraInfo(const JSHandle<JSNativePointer> &call_back,
                                                       const JSHandle<JSNativePointer> &vm,
                                                       const JSHandle<JSNativePointer> &data);

    JSHandle<JSArray> NewJSArray();
    JSHandle<BigInt> NewBigInt();

    JSHandle<JSProxy> NewJSProxy(const JSHandle<JSTaggedValue> &target, const JSHandle<JSTaggedValue> &handler);
    JSHandle<JSRealm> NewJSRealm();

    JSHandle<JSArguments> NewJSArguments();

    JSHandle<JSPrimitiveRef> NewJSString(const JSHandle<JSTaggedValue> &str);

    PANDA_PUBLIC_API JSHandle<TaggedArray> NewTaggedArray(uint32_t length,
                                                          JSTaggedValue init_val = JSTaggedValue::Hole());
    JSHandle<TaggedArray> NewTaggedArray(uint32_t length, JSTaggedValue init_val, panda::SpaceType space_type);
    JSHandle<TaggedArray> NewWeakTaggedArray(uint32_t length, JSTaggedValue init_val = JSTaggedValue::Hole());

    JSHandle<TaggedArray> NewDictionaryArray(uint32_t length);
    JSHandle<JSForInIterator> NewJSForinIterator(const JSHandle<JSTaggedValue> &obj);

    JSHandle<TaggedArray> NewLinkedHashTable(ArraySizeT length, JSType table_type, bool is_weak);
    JSHandle<PropertyBox> NewPropertyBox(const JSHandle<JSTaggedValue> &value);

    JSHandle<ProtoChangeMarker> NewProtoChangeMarker();

    JSHandle<ProtoChangeDetails> NewProtoChangeDetails();

    // use for copy properties keys's array to another array
    JSHandle<TaggedArray> ExtendArray(const JSHandle<TaggedArray> &old, uint32_t length,
                                      JSTaggedValue init_val = JSTaggedValue::Hole());
    JSHandle<TaggedArray> CopyPartArray(const JSHandle<TaggedArray> &old, uint32_t start, uint32_t end);
    JSHandle<TaggedArray> CopyArray(const JSHandle<TaggedArray> &old, uint32_t old_length, uint32_t new_length,
                                    JSTaggedValue init_val = JSTaggedValue::Hole());
    JSHandle<TaggedArray> CloneProperties(const JSHandle<TaggedArray> &old);
    JSHandle<TaggedArray> CloneProperties(const JSHandle<TaggedArray> &old, const JSHandle<JSTaggedValue> &env,
                                          const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &constpool);

    JSHandle<LayoutInfo> CreateLayoutInfo(int properties, JSTaggedValue init_val = JSTaggedValue::Hole());

    JSHandle<LayoutInfo> ExtendLayoutInfo(const JSHandle<LayoutInfo> &old, int properties,
                                          JSTaggedValue init_val = JSTaggedValue::Hole());

    JSHandle<LayoutInfo> CopyLayoutInfo(const JSHandle<LayoutInfo> &old);

    JSHandle<LayoutInfo> CopyAndReSort(const JSHandle<LayoutInfo> &old, int end, int capacity);

    JSHandle<EcmaString> GetEmptyString() const;

    JSHandle<TaggedArray> EmptyArray() const;
    JSHandle<TaggedArray> EmptyWeakArray() const;

    JSHandle<ObjectWrapper> NewObjectWrapper(const JSHandle<JSTaggedValue> &value);

    FreeObject *FillFreeObject(uintptr_t address, size_t size, RemoveSlots remove_slots = RemoveSlots::NO);

    TaggedObject *NewDynObject(const JSHandle<JSHClass> &dynclass);

    TaggedObject *NewNonMovableDynObject(const JSHandle<JSHClass> &dynclass, int inobj_prop_count = 0);

    void InitializeExtraProperties(const JSHandle<JSHClass> &dynclass, TaggedObject *obj, int inobj_prop_count);

    JSHandle<TaggedQueue> NewTaggedQueue(uint32_t length);

    JSHandle<TaggedQueue> GetEmptyTaggedQueue() const;

    JSHandle<JSWeakRef> NewWeakRef(const JSHandle<JSObject> &referent);

    JSHandle<JSSetIterator> NewJSSetIterator(const JSHandle<JSSet> &set, IterationKind kind);

    JSHandle<JSRegExpIterator> NewJSRegExpIterator(const JSHandle<JSTaggedValue> &matcher,
                                                   const JSHandle<EcmaString> &input_str, bool global,
                                                   bool full_unicode);

    JSHandle<JSMapIterator> NewJSMapIterator(const JSHandle<JSMap> &map, IterationKind kind);

    JSHandle<JSArrayIterator> NewJSArrayIterator(const JSHandle<JSObject> &array, IterationKind kind);

    JSHandle<CompletionRecord> NewCompletionRecord(uint8_t type, const JSHandle<JSTaggedValue> &value);

    JSHandle<GeneratorContext> NewGeneratorContext();

    JSHandle<JSPromiseReactionsFunction> CreateJSPromiseReactionsFunction(const void *native_func);

    JSHandle<JSPromiseExecutorFunction> CreateJSPromiseExecutorFunction(const void *native_func);

    JSHandle<JSFunction> CreateBuiltinFunction(EcmaEntrypoint steps, uint8_t length, JSHandle<JSTaggedValue> name,
                                               JSHandle<JSTaggedValue> proto, JSHandle<JSTaggedValue> prefix);

    JSHandle<JSPromiseAllResolveElementFunction> NewJSPromiseAllResolveElementFunction(const void *native_func);

    JSHandle<JSObject> CloneObjectLiteral(JSHandle<JSObject> object, const JSHandle<JSTaggedValue> &env,
                                          const JSHandle<JSTaggedValue> &constpool, bool can_share_hclass = true);
    JSHandle<JSObject> CloneObjectLiteral(JSHandle<JSObject> object);
    JSHandle<JSArray> CloneArrayLiteral(JSHandle<JSArray> object);
    JSHandle<JSFunction> CloneJSFuction(JSHandle<JSFunction> obj, FunctionKind kind);
    JSHandle<JSFunction> CloneClassCtor(JSHandle<JSFunction> ctor, const JSHandle<JSTaggedValue> &lexenv,
                                        bool can_share_hclass);

    void NewJSArrayBufferData(const JSHandle<JSArrayBuffer> &array, int32_t length);

    JSHandle<JSArrayBuffer> NewJSArrayBuffer(int32_t length);

    JSHandle<JSArrayBuffer> NewJSArrayBuffer(void *buffer, int32_t length, const DeleteEntryPoint &deleter, void *data,
                                             bool share = false);

    JSHandle<JSDataView> NewJSDataView(JSHandle<JSArrayBuffer> buffer, int32_t offset, int32_t length);

    void NewJSRegExpByteCodeData(const JSHandle<JSRegExp> &regexp, void *buffer, size_t size);

    template <typename T, typename S>
    inline void NewJSIntlIcuData(const JSHandle<T> &obj, const S &icu, const DeleteEntryPoint &callback);

    EcmaString *InternString(const JSHandle<JSTaggedValue> &key);

    inline JSHandle<JSNativePointer> NewJSNativePointer(void *external_pointer,
                                                        const DeleteEntryPoint &call_back = nullptr,
                                                        void *data = nullptr, bool non_movable = false);

    JSHandle<JSObject> NewJSObjectByClass(const JSHandle<TaggedArray> &properties, size_t length,
                                          panda::SpaceType space_type);

    // only use for creating Function.prototype and Function
    JSHandle<JSFunction> NewJSFunctionByDynClass(JSMethod *method, const JSHandle<JSHClass> &clazz,
                                                 FunctionKind kind = FunctionKind::NORMAL_FUNCTION,
                                                 panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    EcmaString *ResolveString(uint32_t string_id);

    void ObtainRootClass(const JSHandle<GlobalEnv> &global_env);

    const MemManager &GetHeapManager() const
    {
        return heap_helper_;
    }

    // used for creating jsobject by constructor
    PANDA_PUBLIC_API JSHandle<JSObject> NewJSObjectByConstructor(
        const JSHandle<JSFunction> &constructor, const JSHandle<JSTaggedValue> &new_target,
        panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);

    JSHandle<ClassInfoExtractor> NewClassInfoExtractor(JSMethod *ctor_method);

    ~ObjectFactory() = default;

    // ----------------------------------- new string ----------------------------------------
    PANDA_PUBLIC_API JSHandle<EcmaString> NewFromString(const PandaString &data);
    PANDA_PUBLIC_API JSHandle<EcmaString> NewFromCanBeCompressString(const PandaString &data);

    PANDA_PUBLIC_API JSHandle<EcmaString> NewFromStdString(const std::string &data);
    JSHandle<EcmaString> NewFromStdStringUnCheck(const std::string &data, bool can_be_compress,
                                                 panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);

    PANDA_PUBLIC_API JSHandle<EcmaString> NewFromUtf8(const uint8_t *utf8_data, uint32_t utf8_len);
    JSHandle<EcmaString> NewFromUtf8UnCheck(const uint8_t *utf8_data, uint32_t utf8_len, bool can_be_compress);

    PANDA_PUBLIC_API JSHandle<EcmaString> NewFromUtf16(const uint16_t *utf16_data, uint32_t utf16_len);
    JSHandle<EcmaString> NewFromUtf16UnCheck(const uint16_t *utf16_data, uint32_t utf16_len, bool can_be_compress);

    PANDA_PUBLIC_API JSHandle<EcmaString> NewFromUtf8Literal(const uint8_t *utf8_data, uint32_t utf8_len);
    JSHandle<EcmaString> NewFromUtf8LiteralUnCheck(const uint8_t *utf8_data, uint32_t utf8_len, bool can_be_compress);

    PANDA_PUBLIC_API JSHandle<EcmaString> NewFromUtf16Literal(const uint16_t *utf16_data, uint32_t utf16_len);
    JSHandle<EcmaString> NewFromUtf16LiteralUnCheck(const uint16_t *utf16_data, uint32_t utf16_len,
                                                    bool can_be_compress);

    JSHandle<EcmaString> NewFromString(EcmaString *str);
    JSHandle<EcmaString> ConcatFromString(const JSHandle<EcmaString> &prefix, const JSHandle<EcmaString> &suffix);

    // used for creating Function
    JSHandle<JSObject> NewJSObject(const JSHandle<JSHClass> &jshclass);

    // used to create nonmovable js_object
    JSHandle<JSObject> NewNonMovableJSObject(const JSHandle<JSHClass> &jshclass);

    // used for creating jshclass in GlobalEnv, EcmaVM
    template <typename T>
    inline JSHandle<JSHClass> CreateDynClass(JSHClass *hclass, JSType type, uint32_t flags = 0,
                                             uint32_t inlined_props = JSHClass::DEFAULT_CAPACITY_OF_IN_OBJECTS)
    {
        JSHandle<JSHClass> cls = NewEcmaDynClass(hclass, T::SIZE, type, flags, inlined_props);
        if constexpr (T::NATIVE_FIELDS_MASK != 0) {
            cls->GetHClass()->SetNativeFieldMask(T::NATIVE_FIELDS_MASK);
        }
        return cls;
    }
    // used for creating jshclass in Builtins, Function, Class_Linker
    template <typename T>
    inline JSHandle<JSHClass> CreateDynClass(JSType type, const JSHandle<JSTaggedValue> &prototype, uint32_t flags = 0)
    {
        JSHandle<JSHClass> cls = NewEcmaDynClass(T::SIZE, type, prototype, flags);
        if constexpr (T::NATIVE_FIELDS_MASK != 0) {
            cls->GetHClass()->SetNativeFieldMask(T::NATIVE_FIELDS_MASK);
        }
        return cls;
    }
    // used for creating jshclass in GlobalEnv, EcmaVM
    JSHandle<JSHClass> NewEcmaDynClass(JSHClass *hclass, uint32_t size, JSType type, uint32_t flags = 0,
                                       uint32_t inlined_props = JSHClass::DEFAULT_CAPACITY_OF_IN_OBJECTS);
    // used for creating jshclass in Builtins, Function, Class_Linker
    JSHandle<JSHClass> NewEcmaDynClass(uint32_t size, JSType type, const JSHandle<JSTaggedValue> &prototype,
                                       uint32_t flags = 0);

    void SetTriggerGc(bool status)
    {
        is_trigger_gc_ = status;
    }

private:
    JSHandle<TaggedArray> NewTaggedArrayImpl(uint32_t length, JSTaggedValue init_val, bool weak);

    friend class GlobalEnv;
    friend class GlobalEnvConstants;
    friend class EcmaString;
    JSHandle<JSFunction> NewJSFunctionImpl(JSMethod *method);

    void InitObjectFields(TaggedObject *object);

    JSThread *thread_ {nullptr};
    bool is_trigger_gc_ {false};
    bool trigger_semi_gc_ {false};
    MemManager heap_helper_;

    JSHClass *hclass_class_ {nullptr};
    JSHClass *string_class_ {nullptr};
    JSHClass *array_class_ {nullptr};
    JSHClass *weak_array_class_ {nullptr};
    JSHClass *big_int_class_ {nullptr};
    JSHClass *dictionary_class_ {nullptr};
    JSHClass *free_object_with_none_field_class_ {nullptr};
    JSHClass *free_object_with_one_field_class_ {nullptr};
    JSHClass *free_object_with_two_field_class_ {nullptr};
    JSHClass *completion_record_class_ {nullptr};
    JSHClass *generator_context_class_ {nullptr};
    JSHClass *env_class_ {nullptr};
    JSHClass *symbol_class_ {nullptr};
    JSHClass *accessor_data_class_ {nullptr};
    JSHClass *internal_accessor_class_ {nullptr};
    JSHClass *capability_record_class_ {nullptr};
    JSHClass *reactions_record_class_ {nullptr};
    JSHClass *promise_iterator_record_class_ {nullptr};
    JSHClass *micro_job_queue_class_ {nullptr};
    JSHClass *pending_job_class_ {nullptr};
    JSHClass *js_proxy_ordinary_class_ {nullptr};
    JSHClass *js_proxy_callable_class_ {nullptr};
    JSHClass *js_proxy_construct_class_ {nullptr};
    JSHClass *object_wrapper_class_ {nullptr};
    JSHClass *property_box_class_ {nullptr};
    JSHClass *proto_change_details_class_ {nullptr};
    JSHClass *proto_change_marker_class_ {nullptr};
    JSHClass *promise_record_class_ {nullptr};
    JSHClass *promise_resolving_functions_record_ {nullptr};
    JSHClass *js_native_pointer_class_ {nullptr};
    JSHClass *transition_handler_class_ {nullptr};
    JSHClass *prototype_handler_class_ {nullptr};
    JSHClass *function_extra_info_ {nullptr};
    JSHClass *js_realm_class_ {nullptr};
    JSHClass *program_class_ {nullptr};
    JSHClass *ecma_module_class_ {nullptr};
    JSHClass *class_info_extractor_h_class_ {nullptr};
    JSHClass *linked_hash_map_class_ {nullptr};
    JSHClass *linked_hash_set_class_ {nullptr};
    JSHClass *weak_ref_class_ {nullptr};
    JSHClass *weak_linked_hash_map_class_ {nullptr};
    JSHClass *weak_linked_hash_set_class_ {nullptr};

    EcmaVM *vm_ {nullptr};

    NO_COPY_SEMANTIC(ObjectFactory);
    NO_MOVE_SEMANTIC(ObjectFactory);

    void NewObjectHook() const;

    // used for creating jshclass in Builtins, Function, Class_Linker
    template <typename T>
    inline JSHandle<JSHClass> CreateDynClass(JSType type,
                                             uint32_t inlined_props = JSHClass::DEFAULT_CAPACITY_OF_IN_OBJECTS)
    {
        JSHandle<JSHClass> cls = NewEcmaDynClass(T::SIZE, type, inlined_props);
        if constexpr (T::NATIVE_FIELDS_MASK != 0) {
            cls->GetHClass()->SetNativeFieldMask(T::NATIVE_FIELDS_MASK);
        }
        return cls;
    }
    // used for creating jshclass in Builtins, Function, Class_Linker
    JSHandle<JSHClass> NewEcmaDynClass(uint32_t size, JSType type,
                                       uint32_t inlined_props = JSHClass::DEFAULT_CAPACITY_OF_IN_OBJECTS);
    // used for creating jshclass in GlobalEnv, EcmaVM
    JSHandle<JSHClass> NewEcmaDynClassClass(JSHClass *hclass, uint32_t size, JSType type);

    // used for creating Function
    JSHandle<JSFunction> NewJSFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &dyn_klass);
    JSHandle<JSHClass> CreateObjectClass(const JSHandle<TaggedArray> &properties, size_t length);

    template <typename T>
    inline JSHandle<JSHClass> CreateFunctionClass(FunctionKind kind, JSType type,
                                                  const JSHandle<JSTaggedValue> &prototype)
    {
        return CreateFunctionClass(kind, T::SIZE, T::NATIVE_FIELDS_MASK, type, prototype);
    }
    JSHandle<JSHClass> CreateFunctionClass(FunctionKind kind, uint32_t size, JSAccessorsMaskType native_mask,
                                           JSType type, const JSHandle<JSTaggedValue> &prototype);

    // used for creating ref.prototype in buildins, such as Number.prototype
    JSHandle<JSPrimitiveRef> NewJSPrimitiveRef(const JSHandle<JSHClass> &dyn_klass,
                                               const JSHandle<JSTaggedValue> &object);

    JSHandle<EcmaString> GetStringFromStringTable(
        const uint8_t *utf8_data, uint32_t utf8_len, bool can_be_compress,
        panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT) const;
    // For MUtf-8 string data
    EcmaString *GetRawStringFromStringTable(const uint8_t *mutf8_data, uint32_t utf16_len, bool can_be_compressed,
                                            panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT) const;

    JSHandle<EcmaString> GetStringFromStringTable(
        const uint16_t *utf16_data, uint32_t utf16_len, bool can_be_compress,
        panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT) const;

    JSHandle<EcmaString> GetStringFromStringTable(EcmaString *string) const;

    inline EcmaString *AllocStringObject(size_t size);
    inline EcmaString *AllocNonMovableStringObject(size_t size);
    JSHandle<TaggedArray> NewEmptyArray(bool weak = false);  // only used for EcmaVM.

    JSHandle<JSHClass> CreateJSArguments();
    JSHandle<JSHClass> CreateJSArrayInstanceClass(JSHandle<JSTaggedValue> proto);
    JSHandle<JSHClass> CreateJSRegExpInstanceClass(JSHandle<JSTaggedValue> proto);

    friend class builtins::Builtins;  // create builtins object need dynclass
    friend class JSFunction;          // create prototype_or_dynclass need dynclass
    friend class JSHClass;            // HC transition need dynclass
    friend class EcmaVM;              // hold the factory instance
    friend class JsVerificationTest;
    friend class PandaFileTranslator;
    friend class LiteralDataExtractor;
    friend class RuntimeTrampolines;
    friend class ClassInfoExtractor;
    friend class DynamicObjectHelpersTest;
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_OBJECT_FACTORY_H
