/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/thread_environment_api.h"
#include "plugins/ecmascript/runtime/global_env_constants-inl.h"
#include "plugins/ecmascript/runtime/ic/properties_cache-inl.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter-inl.h"
#include "runtime/include/panda_vm.h"
#include "runtime/include/stack_walker-inl.h"

namespace panda::ecmascript {
// static
JSThread *JSThread::Create(Runtime *runtime, PandaVM *vm)
{
    auto js_thread = new JSThread(runtime, vm);
    js_thread->InitBuffers();
    JSThread::SetCurrent(js_thread);
    js_thread->NativeCodeBegin();
    js_thread->InitForStackOverflowCheck(STACK_OVERFLOW_RESERVED_SIZE, STACK_OVERFLOW_PROTECTED_SIZE);
    return js_thread;
}

JSThread::JSThread(Runtime *runtime, PandaVM *vm)
    : ManagedThread(GetCurrentThreadId(), runtime->GetInternalAllocator(), vm, Thread::ThreadType::THREAD_TYPE_MANAGED,
                    panda_file::SourceLang::ECMASCRIPT)
{
    SetLanguageContext(runtime->GetLanguageContext(panda_file::SourceLang::ECMASCRIPT));
    global_storage_ = runtime->GetInternalAllocator()->New<EcmaGlobalStorage>();
    internal_call_params_ = new InternalCallParams();
    properties_cache_ = new PropertiesCache();
}

JSThread::~JSThread()
{
    for (auto n : handle_storage_nodes_) {
        delete n;
    }
    handle_storage_nodes_.clear();
    current_handle_storage_index_ = -1;
    handle_scope_count_ = 0;
    handle_scope_storage_next_ = handle_scope_storage_end_ = nullptr;
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(global_storage_);

    if (internal_call_params_ != nullptr) {
        delete internal_call_params_;
        internal_call_params_ = nullptr;
    }
    if (properties_cache_ != nullptr) {
        delete properties_cache_;
        properties_cache_ = nullptr;
    }
}

EcmaVM *JSThread::GetEcmaVM() const
{
    return EcmaVM::Cast(GetVM());
}

void JSThread::SetException(JSTaggedValue exception)
{
    ManagedThread::SetException(exception.GetHeapObject());
}

void JSThread::ClearException()
{
    ManagedThread::ClearException();
}

void JSThread::Iterate(const RootVisitor &v0, const RootRangeVisitor &v1)
{
    auto *exc = ManagedThread::GetException();
    if (!JSTaggedValue(exc).IsHole()) {
        v0(Root::ROOT_VM, ObjectSlot(ToUintPtr(exc)));
        v0(Root::ROOT_VM,
           ObjectSlot(ToUintPtr(static_cast<ManagedThread *>(this)) + ManagedThread::GetExceptionOffset()));
    }

    for (auto stack = StackWalker::Create(this); stack.HasFrame(); stack.NextFrame()) {
        stack.IterateObjects([&v0](auto &vreg) {
            v0(Root::ROOT_FRAME, ObjectSlot(ToUintPtr(vreg.GetReference())));
            return true;
        });
    }
    IterateEcmascriptEnvironment(v0, v1);
    v0(Root::ROOT_VM, ObjectSlot(ToUintPtr(&invocation_lexical_env_)));

    // visit internal call params；
    internal_call_params_->Iterate(v1);
    // visit tagged handle storage roots
    if (current_handle_storage_index_ != -1) {
        int32_t nid = current_handle_storage_index_;
        for (int32_t i = 0; i <= nid; ++i) {
            auto node = handle_storage_nodes_.at(i);
            auto start = node->data();
            auto end = (i != nid) ? &(node->data()[NODE_BLOCK_SIZE]) : handle_scope_storage_next_;
            v1(ecmascript::Root::ROOT_HANDLE, ObjectSlot(ToUintPtr(start)), ObjectSlot(ToUintPtr(end)));
        }
    }
    global_storage_->IterateUsageGlobal([v0](EcmaGlobalStorage::Node *node) {
        JSTaggedValue value(node->GetObject());
        if (value.IsHeapObject()) {
            v0(ecmascript::Root::ROOT_HANDLE, ecmascript::ObjectSlot(node->GetObjectAddress()));
        }
    });

    JSSpanHandle::IterateChain(this, v1);
    ScopedCallInfo::IterateChain(this, v1);
}

void JSThread::IterateEcmascriptEnvironment(const RootVisitor &v0, const RootRangeVisitor &v1)
{
    auto adapter = [&v1]([[maybe_unused]] ObjectHeader *obj, ObjectSlot start, ObjectSlot end) {
        v1(Root::ROOT_VM, start, end);
    };
    auto visit_env = [&v0, &adapter](EcmascriptEnvironment *env) {
        v0(Root::ROOT_VM, ObjectSlot(ToUintPtr(env) + EcmascriptEnvironment::GetConstantPoolOffset()));
        v0(Root::ROOT_VM, ObjectSlot(ToUintPtr(env) + EcmascriptEnvironment::GetLexicalEnvOffset()));
        v0(Root::ROOT_VM, ObjectSlot(ToUintPtr(env) + EcmascriptEnvironment::GetThisFuncOffset()));

        JSTaggedValue lex_env(env->GetLexicalEnv());
        while (!lex_env.IsJSGlobalEnv()) {
            LexicalEnv::Cast(lex_env.GetHeapObject())->VisitRangeSlot(adapter);
            lex_env = LexicalEnv::Cast(lex_env.GetHeapObject())->GetParentEnv();
        }
    };

    for (auto stack = StackWalker::Create(this); stack.HasFrame(); stack.NextFrame()) {
        if (!stack.IsCFrame()) {
            auto iframe = stack.GetIFrame();
            if (iframe->GetExt() != reinterpret_cast<void *>(iframe)) {
                visit_env(JSFrame::GetJSEnv(iframe));
            }
        }
    }

    if (properties_cache_ != nullptr) {
        properties_cache_->Clear();
    }

    // visit global Constant
    global_const_.VisitRangeSlot(v1);
}

uintptr_t *JSThread::ExpandHandleStorage()
{
    uintptr_t *result = nullptr;
    int32_t last_index = handle_storage_nodes_.size() - 1;
    if (current_handle_storage_index_ == last_index) {
        auto n = new std::array<JSTaggedType, NODE_BLOCK_SIZE>();
        handle_storage_nodes_.push_back(n);
        current_handle_storage_index_++;
        result = reinterpret_cast<uintptr_t *>(&n->data()[0]);
        handle_scope_storage_end_ = &n->data()[NODE_BLOCK_SIZE];
    } else {
        current_handle_storage_index_++;
        auto last_node = handle_storage_nodes_[current_handle_storage_index_];
        result = reinterpret_cast<uintptr_t *>(&last_node->data()[0]);
        handle_scope_storage_end_ = &last_node->data()[NODE_BLOCK_SIZE];
    }

    return result;
}

void JSThread::ShrinkHandleStorage(int prev_index)
{
    current_handle_storage_index_ = prev_index;
    int32_t last_index = handle_storage_nodes_.size() - 1;
#if ECMASCRIPT_ENABLE_ZAP_MEM
    uintptr_t size = ToUintPtr(handle_scope_storage_end_) - ToUintPtr(handle_scope_storage_next_);
    memset_s(handle_scope_storage_next_, size, 0, size);
    for (int32_t i = current_handle_storage_index_ + 1; i < last_index; i++) {
        memset_s(handle_storage_nodes_[i], NODE_BLOCK_SIZE * sizeof(JSTaggedType), 0,
                 NODE_BLOCK_SIZE * sizeof(JSTaggedType));
    }
#endif

    if (last_index > MIN_HANDLE_STORAGE_SIZE && current_handle_storage_index_ < MIN_HANDLE_STORAGE_SIZE) {
        for (int i = MIN_HANDLE_STORAGE_SIZE; i < last_index; i++) {
            auto node = handle_storage_nodes_.back();
            delete node;
            handle_storage_nodes_.pop_back();
        }
    }
}

void JSThread::NotifyStableArrayElementsGuardians(JSHandle<JSObject> receiver)
{
    if (!receiver->GetJSHClass()->IsPrototype()) {
        return;
    }
    if (!stable_array_elements_guardians_) {
        return;
    }
    auto env = GetEcmaVM()->GetGlobalEnv();
    if (receiver.GetTaggedValue() == env->GetObjectFunctionPrototype().GetTaggedValue() ||
        receiver.GetTaggedValue() == env->GetArrayPrototype().GetTaggedValue()) {
        stable_array_elements_guardians_ = false;
    }
}

void JSThread::ResetGuardians()
{
    stable_array_elements_guardians_ = true;
}

void JSThread::DisableStackOverflowCheck()
{
    GetStackFrameAllocator()->UseWholeMemory();
    ManagedThread::DisableStackOverflowCheck();
}

void JSThread::EnableStackOverflowCheck()
{
    GetStackFrameAllocator()->ReserveMemory();
    ManagedThread::EnableStackOverflowCheck();
}

}  // namespace panda::ecmascript
