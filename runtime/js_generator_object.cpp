/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_generator_object.h"
#include "generator_helper.h"
#include "js_iterator.h"
#include "js_tagged_value-inl.h"

namespace panda::ecmascript {
JSTaggedValue JSGeneratorObject::GeneratorValidate(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    // 1.Perform ? RequireInternalSlot(generator, [[GeneratorState]]).
    // 2.Assert: generator also has a [[GeneratorContext]] internal slot.
    if (!obj->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", JSTaggedValue::Undefined());
    }
    JSHandle<JSObject> to_obj = JSTaggedValue::ToObject(thread, obj);
    if (!to_obj->IsGeneratorObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", JSTaggedValue::Undefined());
    }

    // 3.Let state be generator.[[GeneratorState]].
    JSHandle<JSGeneratorObject> generator(thread, JSGeneratorObject::Cast(*(to_obj)));
    JSTaggedValue state = generator->GetGeneratorState();
    // 4.If state is executing, throw a TypeError exception.
    if (state == JSTaggedValue(static_cast<int32_t>(JSGeneratorState::EXECUTING))) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", JSTaggedValue::Undefined());
    }
    // 5.Return state.
    return state;
}

JSHandle<JSTaggedValue> JSGeneratorObject::GeneratorResume(JSThread *thread,
                                                           const JSHandle<JSGeneratorObject> &generator,
                                                           const JSHandle<JSTaggedValue> &value)
{
    // 1.Let state be ? GeneratorValidate(generator).
    JSHandle<JSTaggedValue> gen(thread, generator.GetTaggedValue());
    JSTaggedValue state = GeneratorValidate(thread, gen);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);

    // 2.If state is completed, return CreateIterResultObject(undefined, true).
    if (JSGeneratorObject::IsState(state, JSGeneratorState::COMPLETED)) {
        JSHandle<JSTaggedValue> value_handle(thread, JSTaggedValue::Undefined());
        return JSHandle<JSTaggedValue>::Cast(JSIterator::CreateIterResultObject(thread, value_handle, true));
    }

    // 3.Assert: state is either suspendedStart or suspendedYield.
    ASSERT_PRINT(JSGeneratorObject::IsState(state, JSGeneratorState::SUSPENDED_START) ||
                     JSGeneratorObject::IsState(state, JSGeneratorState::SUSPENDED_YIELD),
                 "state is neither suspendedStart nor suspendedYield");

    // 4.Let genContext be generator.[[GeneratorContext]].
    JSHandle<GeneratorContext> gen_context(thread, generator->GetGeneratorContext());

    // 5.Let methodContext be the running execution context.
    // 6.Suspend methodContext.

    // 7.Set generator.[[GeneratorState]] to executing.
    generator->SetState(thread, JSGeneratorState::EXECUTING);

    // 8.Push genContext onto the execution context stack; genContext is now the running execution context.
    // 9.Resume the suspended evaluation of genContext using NormalCompletion(value) as the result of the operation
    //   that suspended it. Let result be the value returned by the resumed computation.
    // 10.Assert: When we return here, genContext has already been removed from the execution context stack and
    //    methodContext is the currently running execution context.
    // 11.Return Completion(result).
    JSHandle<JSTaggedValue> result =
        GeneratorHelper::Continue(thread, gen_context, GeneratorResumeMode::NEXT, value.GetTaggedValue());
    return result;
}

JSHandle<JSTaggedValue> JSGeneratorObject::GeneratorResumeAbrupt(JSThread *thread,
                                                                 const JSHandle<JSGeneratorObject> &generator,
                                                                 const JSHandle<CompletionRecord> &abrupt_completion)
{
    // 1.Let state be ? GeneratorValidate(generator).
    JSHandle<JSTaggedValue> gen(thread, generator.GetTaggedValue());
    JSTaggedValue state = GeneratorValidate(thread, gen);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);

    // 2.If state is suspendedStart, then
    //     a.Set generator.[[GeneratorState]] to completed.
    //     b.Once a generator enters the completed state it never leaves it and its associated execution context is
    //       never resumed. Any execution state associated with generator can be discarded at this point.
    //     c.Set state to completed.
    if (JSGeneratorObject::IsState(state, JSGeneratorState::SUSPENDED_START)) {
        generator->SetState(thread, JSGeneratorState::COMPLETED);
        state = JSTaggedValue(static_cast<int32_t>(JSGeneratorState::COMPLETED));
    }

    // 3.If state is completed, then
    //     a.If abrupt_completion.[[Type]] is return, then
    //         i.Return CreateIterResultObject(abrupt_completion.[[Value]], true).
    //     b.Return Completion(abrupt_completion).
    if (JSGeneratorObject::IsState(state, JSGeneratorState::COMPLETED)) {
        JSHandle<JSTaggedValue> value_handle(thread, abrupt_completion->GetValue());
        JSHandle<JSTaggedValue> result =
            JSHandle<JSTaggedValue>::Cast(JSIterator::CreateIterResultObject(thread, value_handle, true));

        if (abrupt_completion->IsReturn()) {
            return result;
        }
        THROW_NEW_ERROR_AND_RETURN_VALUE(thread, value_handle.GetTaggedValue(), result);
    }

    // 4.Assert: state is suspendedYield.
    ASSERT_PRINT(JSGeneratorObject::IsState(state, JSGeneratorState::SUSPENDED_YIELD), "state is not suspendedYield");

    // 5.Let genContext be generator.[[GeneratorContext]].
    JSHandle<GeneratorContext> gen_context(thread, generator->GetGeneratorContext());

    // 6.Let methodContext be the running execution context.
    // 7.Suspend methodContext.

    // 8.Set generator.[[GeneratorState]] to executing.
    generator->SetState(thread, JSGeneratorState::EXECUTING);

    // 9.Push genContext onto the execution context stack; genContext is now the running execution context.
    // 10.Resume the suspended evaluation of genContext using abrupt_completion as the result of the operation that
    //    suspended it. Let result be the completion record returned by the resumed computation.
    // 11.Assert: When we return here, genContext has already been removed from the execution context stack and
    //    methodContext is the currently running execution context.
    // 12.Return Completion(result).
    JSHandle<JSTaggedValue> result;
    if (abrupt_completion->IsReturn()) {
        result =
            GeneratorHelper::Continue(thread, gen_context, GeneratorResumeMode::RETURN, abrupt_completion->GetValue());
    } else {
        result =
            GeneratorHelper::Continue(thread, gen_context, GeneratorResumeMode::THROW, abrupt_completion->GetValue());
    }
    return result;
}
}  // namespace panda::ecmascript
