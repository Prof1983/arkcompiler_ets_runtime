/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_map_iterator.h"
#include "js_array.h"
#include "js_map.h"
#include "linked_hash_table-inl.h"
#include "object_factory.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"

namespace panda::ecmascript {
JSTaggedValue JSMapIterator::Next(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1.Let O be the this value
    JSHandle<JSTaggedValue> input(builtins_common::GetThis(argv));

    // 3.If O does not have all of the internal slots of a Map Iterator Instance (23.1.5.3), throw a TypeError
    // exception.
    if (!input->IsJSMapIterator()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this value is not a map iterator", JSTaggedValue::Exception());
    }
    JSHandle<JSMapIterator> iter(input);
    iter->Update(thread);
    JSHandle<JSTaggedValue> undefined_handle(thread, JSTaggedValue::Undefined());
    // 4.Let m be O.[[IteratedMap]].
    JSHandle<JSTaggedValue> iterated_map(thread, iter->GetIteratedMap());

    // 5.Let index be O.[[MapNextIndex]].
    int index = iter->GetNextIndex().GetInt();
    IterationKind item_kind = IterationKind(iter->GetIterationKind().GetInt());
    // 7.If m is undefined, return CreateIterResultObject(undefined, true).
    if (iterated_map->IsUndefined()) {
        return JSIterator::CreateIterResultObject(thread, undefined_handle, true).GetTaggedValue();
    };
    JSHandle<LinkedHashMap> map(iterated_map);
    int total_elements = map->NumberOfElements() + map->NumberOfDeletedElements();

    JSMutableHandle<JSTaggedValue> key_handle(thread, JSTaggedValue::Undefined());
    while (index < total_elements) {
        JSTaggedValue key = map->GetKey(index);
        if (!key.IsHole()) {
            iter->SetNextIndex(thread, JSTaggedValue(index + 1));
            key_handle.Update(key);
            // If itemKind is key, let result be e.[[Key]]
            if (item_kind == IterationKind::KEY) {
                return JSIterator::CreateIterResultObject(thread, key_handle, false).GetTaggedValue();
            }
            JSHandle<JSTaggedValue> value(thread, map->GetValue(index));
            // Else if itemKind is value, let result be e.[[Value]].
            if (item_kind == IterationKind::VALUE) {
                return JSIterator::CreateIterResultObject(thread, value, false).GetTaggedValue();
            }
            // Else
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            JSHandle<TaggedArray> array(factory->NewTaggedArray(2));  // 2 means the length of array
            array->Set(thread, 0, key_handle);
            array->Set(thread, 1, value);
            JSHandle<JSTaggedValue> key_and_value(JSArray::CreateArrayFromList(thread, array));
            return JSIterator::CreateIterResultObject(thread, key_and_value, false).GetTaggedValue();
        }
        index++;
    }
    // 13.Set O.[[IteratedMap]] to undefined.
    iter->SetIteratedMap(thread, JSTaggedValue::Undefined());
    return JSIterator::CreateIterResultObject(thread, undefined_handle, true).GetTaggedValue();
}

void JSMapIterator::Update(const JSThread *thread)
{
    JSTaggedValue iterated_map = GetIteratedMap();
    if (iterated_map.IsUndefined()) {
        return;
    }
    LinkedHashMap *map = LinkedHashMap::Cast(iterated_map.GetTaggedObject());
    if (map->GetNextTable().IsHole()) {
        return;
    }
    int index = GetNextIndex().GetInt();
    JSTaggedValue next_table = map->GetNextTable();
    while (!next_table.IsHole()) {
        index -= map->GetDeletedElementsAt(index);
        map = LinkedHashMap::Cast(next_table.GetTaggedObject());
        next_table = map->GetNextTable();
    }
    SetIteratedMap(thread, JSTaggedValue(map));
    SetNextIndex(thread, JSTaggedValue(index));
}

JSHandle<JSTaggedValue> JSMapIterator::CreateMapIterator(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                         IterationKind kind)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (!obj->IsJSMap()) {
        JSHandle<JSTaggedValue> undefined_handle(thread, JSTaggedValue::Undefined());
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSMap", undefined_handle);
    }
    JSHandle<JSTaggedValue> iter(factory->NewJSMapIterator(JSHandle<JSMap>(obj), kind));
    return iter;
}
}  // namespace panda::ecmascript
