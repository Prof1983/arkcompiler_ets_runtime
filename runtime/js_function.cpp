/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_function.h"

#include "plugins/ecmascript/runtime/base/error_type.h"
#include "plugins/ecmascript/runtime/class_info_extractor.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter-inl.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_proxy.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

namespace panda::ecmascript {
void JSFunction::InitializeJSFunction(JSThread *thread, const JSHandle<JSFunction> &func, FunctionKind kind,
                                      bool strict)
{
    FunctionMode this_mode;
    if (IsArrowFunction(kind)) {
        this_mode = FunctionMode::LEXICAL;
    } else if (strict) {
        this_mode = FunctionMode::STRICT;
    } else {
        this_mode = FunctionMode::GLOBAL;
    }

    int32_t flag = FunctionKindBit::Encode(kind) | StrictBit::Encode(strict) | ThisModeBit::Encode(this_mode);
    func->SetProtoOrDynClass(thread, JSTaggedValue::Hole(), SKIP_BARRIER);
    func->SetHomeObject(thread, JSTaggedValue::Undefined(), SKIP_BARRIER);
    func->SetLexicalEnv(thread, JSTaggedValue::Undefined(), SKIP_BARRIER);
    func->SetConstantPool(thread, JSTaggedValue::Undefined(), SKIP_BARRIER);
    func->SetProfileTypeInfo(thread, JSTaggedValue::Undefined(), SKIP_BARRIER);
    func->SetFunctionExtraInfo(thread, JSTaggedValue::Undefined());
    func->SetFunctionInfoFlag(thread, JSTaggedValue(flag));

    // Set the [[Realm]] internal slot of F to the running execution context's Realm
    auto *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<LexicalEnv> lexical_env = factory->NewLexicalEnv(0);
    lexical_env->SetParentEnv(thread, ecma_vm->GetGlobalEnv().GetTaggedValue());
    func->SetLexicalEnv(thread, lexical_env.GetTaggedValue());

    ASSERT(!func->IsPropertiesDict());
    auto global_const = thread->GlobalConstants();
    if (HasPrototype(kind)) {
        JSHandle<JSTaggedValue> accessor = global_const->GetHandledFunctionPrototypeAccessor();
        if (kind == FunctionKind::BASE_CONSTRUCTOR || kind == FunctionKind::GENERATOR_FUNCTION ||
            kind == FunctionKind::ASYNC_GENERATOR_FUNCTION) {
            func->SetPropertyInlinedProps(thread, PROTOTYPE_INLINE_PROPERTY_INDEX, accessor.GetTaggedValue());
            accessor = global_const->GetHandledFunctionNameAccessor();
            func->SetPropertyInlinedProps(thread, NAME_INLINE_PROPERTY_INDEX, accessor.GetTaggedValue());
        } else if (!JSFunction::IsClassConstructor(kind)) {  // class ctor do nothing
            PropertyDescriptor desc(thread, accessor, kind != FunctionKind::BUILTIN_CONSTRUCTOR, false, false);
            [[maybe_unused]] bool success = JSObject::DefineOwnProperty(
                thread, JSHandle<JSObject>(func), global_const->GetHandledPrototypeString(), desc);
            ASSERT(success);
        }
    } else if (HasAccessor(kind)) {
        JSHandle<JSTaggedValue> accessor = global_const->GetHandledFunctionNameAccessor();
        func->SetPropertyInlinedProps(thread, NAME_INLINE_PROPERTY_INDEX, accessor.GetTaggedValue());
    }
}

JSHandle<JSObject> JSFunction::NewJSFunctionPrototype(JSThread *thread, ObjectFactory *factory,
                                                      const JSHandle<JSFunction> &func)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> fun_pro = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    func->SetFunctionPrototype(thread, fun_pro.GetTaggedValue());

    // set "constructor" in prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    PropertyDescriptor descriptor(thread, JSHandle<JSTaggedValue>::Cast(func), true, false, true);
    JSObject::DefineOwnProperty(thread, fun_pro, constructor_key, descriptor);

    return fun_pro;
}

JSHClass *JSFunction::GetOrCreateInitialJSHClass(JSThread *thread, const JSHandle<JSFunction> &fun)
{
    JSTaggedValue proto_or_dyn(fun->GetProtoOrDynClass());
    if (proto_or_dyn.IsJSHClass()) {
        return reinterpret_cast<JSHClass *>(proto_or_dyn.GetTaggedObject());
    }

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> proto;
    if (!fun->HasFunctionPrototype()) {
        proto = JSHandle<JSTaggedValue>::Cast(NewJSFunctionPrototype(thread, factory, fun));
    } else {
        proto = JSHandle<JSTaggedValue>(thread, fun->GetProtoOrDynClass());
    }

    JSHandle<JSHClass> dynclass = factory->CreateDynClass<JSObject>(JSType::JS_OBJECT, proto);
    fun->SetProtoOrDynClass(thread, dynclass);
    return *dynclass;
}

JSTaggedValue JSFunction::PrototypeGetter(JSThread *thread, const JSHandle<JSObject> &self)
{
    JSHandle<JSFunction> func = JSHandle<JSFunction>::Cast(self);
    if (!func->HasFunctionPrototype()) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        NewJSFunctionPrototype(thread, factory, func);
    }
    return JSFunction::Cast(*self)->GetFunctionPrototype();
}

bool JSFunction::PrototypeSetter(JSThread *thread, const JSHandle<JSObject> &self, const JSHandle<JSTaggedValue> &value,
                                 [[maybe_unused]] bool may_throw)
{
    JSHandle<JSFunction> func(self);
    JSTaggedValue proto_or_dyn = func->GetProtoOrDynClass();
    if (proto_or_dyn.IsJSHClass()) {
        // need transtion
        JSHandle<JSHClass> dynclass(thread, proto_or_dyn);
        JSHandle<JSHClass> new_dynclass = JSHClass::TransitionProto(thread, dynclass, value);
        if (value->IsECMAObject()) {
            JSObject::Cast(value->GetTaggedObject())->GetJSHClass()->SetIsPrototype(true);
        }
        func->SetProtoOrDynClass(thread, new_dynclass);
    } else {
        func->SetFunctionPrototype(thread, value.GetTaggedValue());
    }
    return true;
}

JSTaggedValue JSFunction::NameGetter(JSThread *thread, const JSHandle<JSObject> &self)
{
    JSMethod *target = JSHandle<JSFunction>::Cast(self)->GetCallTarget();
    if (target->GetPandaFile() == nullptr) {
        return JSTaggedValue::Undefined();
    }
    PandaString func_name = target->ParseFunctionName();
    if (func_name.empty()) {
        return thread->GlobalConstants()->GetEmptyString();
    }
    if (JSHandle<JSFunction>::Cast(self)->GetFunctionKind() == FunctionKind::GETTER_FUNCTION) {
        func_name.insert(0, "get ");
    }
    if (JSHandle<JSFunction>::Cast(self)->GetFunctionKind() == FunctionKind::SETTER_FUNCTION) {
        func_name.insert(0, "set ");
    }

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    return factory->NewFromString(func_name).GetTaggedValue();
}

bool JSFunction::OrdinaryHasInstance(JSThread *thread, const JSHandle<JSTaggedValue> &constructor,
                                     const JSHandle<JSTaggedValue> &obj)
{
    // 1. If IsCallable(C) is false, return false.
    if (!constructor->IsCallable()) {
        return false;
    }

    // 2. If C has a [[BoundTargetFunction]] internal slot, then
    //    a. Let BC be the value of C's [[BoundTargetFunction]] internal slot.
    //    b. Return InstanceofOperator(O,BC)  (see 12.9.4).
    if (constructor->IsBoundFunction()) {
        JSHandle<JSBoundFunction> bound_function(thread, JSBoundFunction::Cast(constructor->GetTaggedObject()));
        JSTaggedValue bound_target = bound_function->GetBoundTarget();
        return JSObject::InstanceOf(thread, obj, JSHandle<JSTaggedValue>(thread, bound_target));
    }
    // 3. If Type(O) is not Object, return false
    if (!obj->IsECMAObject()) {
        return false;
    }

    // 4. Let P be Get(C, "prototype").
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> constructor_prototype =
        JSTaggedValue::GetProperty(thread, constructor, global_const->GetHandledPrototypeString()).GetValue();

    // 5. ReturnIfAbrupt(P).
    // no throw exception, so needn't return
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);

    // 6. If Type(P) is not Object, throw a TypeError exception.
    if (!constructor_prototype->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "HasInstance: is not Object", false);
    }

    // 7. Repeat
    //    a.Let O be O.[[GetPrototypeOf]]().
    //    b.ReturnIfAbrupt(O).
    //    c.If O is null, return false.
    //    d.If SameValue(P, O) is true, return true.
    JSTaggedValue obj_prototype = obj.GetTaggedValue();
    while (!obj_prototype.IsNull()) {
        if (JSTaggedValue::SameValue(obj_prototype, constructor_prototype.GetTaggedValue())) {
            return true;
        }
        obj_prototype = JSObject::Cast(obj_prototype)->GetPrototype(thread);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
    }
    return false;
}

bool JSFunction::MakeConstructor(JSThread *thread, const JSHandle<JSFunction> &func,
                                 const JSHandle<JSTaggedValue> &proto, bool writable)
{
    ASSERT_PRINT(proto->IsHeapObject() || proto->IsUndefined(), "proto must be JSObject or Undefined");
    ASSERT_PRINT(func->IsConstructor(), "func must be Constructor type");
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();

    ASSERT_PRINT(func->GetProtoOrDynClass().IsHole() && func->IsExtensible(),
                 "function doesn't has proto_type property and is extensible object");
    ASSERT_PRINT(JSObject::HasProperty(thread, JSHandle<JSObject>(func), constructor_key),
                 "function must have constructor");

    // proto.constructor = func
    bool status = false;
    if (proto->IsUndefined()) {
        // Let prototype be ObjectCreate(%ObjectPrototype%).
        JSHandle<JSTaggedValue> obj_prototype = env->GetObjectFunctionPrototype();
        PropertyDescriptor constructor_desc(thread, JSHandle<JSTaggedValue>::Cast(func), writable, false, true);
        status = JSTaggedValue::DefinePropertyOrThrow(thread, obj_prototype, constructor_key, constructor_desc);
    } else {
        PropertyDescriptor constructor_desc(thread, JSHandle<JSTaggedValue>::Cast(func), writable, false, true);
        status = JSTaggedValue::DefinePropertyOrThrow(thread, proto, constructor_key, constructor_desc);
    }

    ASSERT_PRINT(status, "DefineProperty construct failed");
    // func.prototype = proto
    // Let status be DefinePropertyOrThrow(F, "prototype", PropertyDescriptor{[[Value]]:
    // prototype, [[Writable]]: writablePrototype, [[Enumerable]]: false, [[Configurable]]: false}).
    func->SetFunctionPrototype(thread, proto.GetTaggedValue());

    ASSERT_PRINT(status, "DefineProperty proto_type failed");
    return status;
}

JSTaggedValue JSFunction::Call(EcmaRuntimeCallInfo *info)
{
    JSThread *thread = info->GetThread();
    // 1. ReturnIfAbrupt(F).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> func = info->GetFunction();
    // 2. If argumentsList was not passed, let argumentsList be a new empty List.
    // 3. If IsCallable(F) is false, throw a TypeError exception.
    if (!func->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Callable is false", JSTaggedValue::Exception());
    }

    auto *jshclass = func->GetTaggedObject()->GetClass();
    if (jshclass->IsClassConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "class constructor cannot call", JSTaggedValue::Exception());
    }
    return EcmaInterpreter::Execute(info);
}

JSTaggedValue JSFunction::Construct(EcmaRuntimeCallInfo *info)
{
    JSThread *thread = info->GetThread();
    JSHandle<JSTaggedValue> func(info->GetFunction());
    JSMutableHandle<JSTaggedValue> target(info->GetNewTarget());

    if (target->IsUndefined()) {
        target.Update(func.GetTaggedValue());
        info->SetNewTarget(target.GetTaggedValue());
    }
    if (!(func->IsConstructor() && target->IsConstructor())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Constructor is false", JSTaggedValue::Exception());
    }
    if (func->IsJSFunction()) {
        return JSFunction::ConstructInternal(info);
    }
    if (func->IsJSProxy()) {
        return JSProxy::ConstructInternal(info);
    }
    ASSERT(func->IsBoundFunction());
    return JSBoundFunction::ConstructInternal(info);
}

JSTaggedValue JSFunction::Invoke(EcmaRuntimeCallInfo *info, const JSHandle<JSTaggedValue> &key)
{
    ASSERT(JSTaggedValue::IsPropertyKey(key));
    JSThread *thread = info->GetThread();
    JSHandle<JSTaggedValue> this_arg = info->GetThis();
    JSHandle<JSTaggedValue> func(JSTaggedValue::GetProperty(thread, this_arg, key).GetValue());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    info->SetFunction(func.GetTaggedValue());
    return JSFunction::Call(info);
}

// [[Construct]]
JSTaggedValue JSFunction::ConstructInternal(EcmaRuntimeCallInfo *info)
{
    if (info == nullptr) {
        return JSTaggedValue::Exception();
    }

    JSThread *thread = info->GetThread();
    JSHandle<JSFunction> func(info->GetFunction());
    JSHandle<JSTaggedValue> new_target(info->GetNewTarget());
    ASSERT(new_target->IsECMAObject());
    if (!func->IsConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Constructor is false", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> obj(thread, JSTaggedValue::Undefined());
    if (func->IsBase()) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        obj = JSHandle<JSTaggedValue>(factory->NewJSObjectByConstructor(func, new_target));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    info->SetThis(obj.GetTaggedValue());
    JSTaggedValue result_value = EcmaInterpreter::Execute(info);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 9.3.2 [[Construct]] (argumentsList, newTarget)
    if (result_value.IsECMAObject()) {
        return result_value;
    }

    if (func->IsBase()) {
        return obj.GetTaggedValue();
    }

    // derived ctor(sub class) return the obj which created by base ctor(parent class)
    if (func->IsDerivedConstructor()) {
        return result_value;
    }

    if (!result_value.IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "function is non-constructor", JSTaggedValue::Exception());
    }
    return obj.GetTaggedValue();
}

JSHandle<JSTaggedValue> JSFunctionBase::GetFunctionName(JSThread *thread, const JSHandle<JSFunctionBase> &func)
{
    JSHandle<JSTaggedValue> name_key = thread->GlobalConstants()->GetHandledNameString();

    return JSObject::GetProperty(thread, JSHandle<JSTaggedValue>(func), name_key).GetValue();
}

bool JSFunctionBase::SetFunctionName(JSThread *thread, const JSHandle<JSFunctionBase> &func,
                                     const JSHandle<JSTaggedValue> &name, const JSHandle<JSTaggedValue> &prefix)
{
    ASSERT_PRINT(func->IsExtensible(), "Function must be extensible");
    ASSERT_PRINT(name->IsStringOrSymbol(), "name must be string or symbol");
    bool need_prefix = false;
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (!prefix->IsUndefined()) {
        ASSERT_PRINT(prefix->IsString(), "prefix must be string");
        need_prefix = true;
    }
    // If Type(name) is Symbol, then
    // Let description be name’s [[Description]] value.
    // If description is undefined, let name be the empty String.
    // Else, let name be the concatenation of "[", description, and "]".
    JSHandle<EcmaString> function_name;
    if (name->IsSymbol()) {
        JSTaggedValue description = JSHandle<JSSymbol>::Cast(name)->GetDescription();
        JSHandle<EcmaString> description_handle(thread, description);
        if (description.IsUndefined()) {
            function_name = factory->GetEmptyString();
        } else {
            JSHandle<EcmaString> left_brackets = factory->NewFromCanBeCompressString("[");
            JSHandle<EcmaString> right_brackets = factory->NewFromCanBeCompressString("]");
            function_name = factory->ConcatFromString(left_brackets, description_handle);
            function_name = factory->ConcatFromString(function_name, right_brackets);
        }
    } else {
        function_name = JSHandle<EcmaString>::Cast(name);
    }
    EcmaString *new_string;
    if (need_prefix) {
        JSHandle<EcmaString> handle_prefix_string = JSTaggedValue::ToString(thread, prefix);
        JSHandle<EcmaString> space_string(factory->NewFromCanBeCompressString(" "));
        JSHandle<EcmaString> concat_string = factory->ConcatFromString(handle_prefix_string, space_string);
        new_string = *factory->ConcatFromString(concat_string, function_name);
    } else {
        new_string = *function_name;
    }
    JSHandle<JSTaggedValue> name_handle(thread, new_string);
    JSHandle<JSTaggedValue> name_key = thread->GlobalConstants()->GetHandledNameString();
    PropertyDescriptor name_desc(thread, name_handle, false, false, true);
    JSHandle<JSTaggedValue> func_handle(func);
    return JSTaggedValue::DefinePropertyOrThrow(thread, func_handle, name_key, name_desc);
}

bool JSFunction::SetFunctionLength(JSThread *thread, const JSHandle<JSFunction> &func, JSTaggedValue length, bool cfg)
{
    ASSERT_PRINT(func->IsExtensible(), "Function must be extensible");
    ASSERT_PRINT(length.IsInteger(), "length must be integer");
    JSHandle<JSTaggedValue> length_key_handle = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> length_handle(thread, length);
    ASSERT_PRINT(!JSTaggedValue::Less(thread, length_handle, JSHandle<JSTaggedValue>(thread, JSTaggedValue(0))),
                 "length must be non negtive integer");
    PropertyDescriptor length_desc(thread, length_handle, false, false, cfg);
    JSHandle<JSTaggedValue> func_handle(func);
    return JSTaggedValue::DefinePropertyOrThrow(thread, func_handle, length_key_handle, length_desc);
}

// 9.4.1.2[[Construct]](argumentsList, new_target)
JSTaggedValue JSBoundFunction::ConstructInternal(EcmaRuntimeCallInfo *info)
{
    JSThread *thread = info->GetThread();
    JSHandle<JSBoundFunction> func(info->GetFunction());
    JSHandle<JSTaggedValue> target(thread, func->GetBoundTarget());
    ASSERT(target->IsConstructor());
    JSHandle<JSTaggedValue> new_target = info->GetNewTarget();
    JSMutableHandle<JSTaggedValue> new_target_mutable(thread, new_target.GetTaggedValue());
    if (JSTaggedValue::SameValue(func.GetTaggedValue(), new_target.GetTaggedValue())) {
        new_target_mutable.Update(target.GetTaggedValue());
    }

    JSHandle<TaggedArray> bound_args(thread, func->GetBoundArguments());
    const uint32_t bound_length = bound_args->GetLength();
    const uint32_t args_length = info->GetArgsNumber();
    JSHandle<JSTaggedValue> undefined = thread->GlobalConstants()->GetHandledUndefined();
    auto runtime_info = NewRuntimeCallInfo(thread, target, undefined, new_target_mutable, bound_length + args_length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (bound_length != 0) {
        runtime_info->SetCallArg(bound_length, bound_args->GetData());
    }
    if (args_length != 0) {
        runtime_info->SetCallArg(
            args_length, reinterpret_cast<JSTaggedType *>(info->GetArgAddress(js_method_args::NUM_MANDATORY_ARGS)),
            bound_length);
    }
    return JSFunction::Construct(runtime_info.Get());
}

void JSProxyRevocFunction::ProxyRevocFunctions(const JSThread *thread, const JSHandle<JSProxyRevocFunction> &revoker)
{
    // 1.Let p be the value of F’s [[RevocableProxy]] internal slot.
    JSTaggedValue proxy = revoker->GetRevocableProxy();
    // 2.If p is null, return undefined.
    if (proxy.IsNull()) {
        return;
    }

    // 3.Set the value of F’s [[RevocableProxy]] internal slot to null.
    revoker->SetRevocableProxy(thread, JSTaggedValue::Null());

    // 4.Assert: p is a Proxy object.
    ASSERT(proxy.IsJSProxy());
    JSHandle<JSProxy> proxy_handle(thread, proxy);

    // 5 ~ 6 Set internal slot of p to null.
    proxy_handle->SetTarget(thread, JSTaggedValue::Null());
    proxy_handle->SetHandler(thread, JSTaggedValue::Null());
}

JSTaggedValue JSFunction::AccessCallerArgumentsThrowTypeError([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(),
                                "Under strict mode, 'caller' and 'arguments' properties must not be accessed.",
                                JSTaggedValue::Exception());
}

JSTaggedValue JSIntlBoundFunction::IntlNameGetter(JSThread *thread, [[maybe_unused]] const JSHandle<JSObject> &self)
{
    return thread->GlobalConstants()->GetEmptyString();
}

void JSFunction::SetFunctionNameNoPrefix(JSThread *thread, JSFunction *func, JSTaggedValue name)
{
    ASSERT_PRINT(func->IsExtensible(), "Function must be extensible");
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> func_handle(thread, func);
    {
        JSMutableHandle<JSTaggedValue> name_handle(thread, JSTaggedValue::Undefined());
        if (!name.IsSymbol()) {
            name_handle.Update(name);
        } else {
            JSHandle<JSTaggedValue> name_begin(thread, name);
            JSTaggedValue description = JSSymbol::Cast(name.GetTaggedObject())->GetDescription();
            if (description.IsUndefined()) {
                name_handle.Update(thread->GlobalConstants()->GetEmptyString());
            } else {
                JSHandle<EcmaString> concat_name;
                JSHandle<EcmaString> left_brackets = factory->NewFromCanBeCompressString("[");
                JSHandle<EcmaString> right_brackets = factory->NewFromCanBeCompressString("]");
                concat_name = factory->ConcatFromString(
                    left_brackets,
                    JSHandle<EcmaString>(thread, JSSymbol::Cast(name_begin->GetHeapObject())->GetDescription()));
                concat_name = factory->ConcatFromString(concat_name, right_brackets);
                name_handle.Update(concat_name.GetTaggedValue());
            }
        }
        PropertyDescriptor name_desc(thread, name_handle, false, false, true);
        JSTaggedValue::DefinePropertyOrThrow(thread, JSHandle<JSTaggedValue>(thread, func_handle.GetTaggedValue()),
                                             thread->GlobalConstants()->GetHandledNameString(), name_desc);
    }
}

JSHandle<JSHClass> JSFunction::GetInstanceJSHClass(JSThread *thread, JSHandle<JSFunction> constructor,
                                                   JSHandle<JSTaggedValue> new_target)
{
    JSHandle<JSHClass> ctor_initial_js_hclass(thread, JSFunction::GetOrCreateInitialJSHClass(thread, constructor));
    // new_target is construct itself
    if (new_target.GetTaggedValue() == constructor.GetTaggedValue()) {
        return ctor_initial_js_hclass;
    }

    // new_target is derived-class of constructor
    if (new_target->IsJSFunction()) {
        JSHandle<JSFunction> new_target_func = JSHandle<JSFunction>::Cast(new_target);
        if (new_target_func->IsDerivedConstructor()) {
            JSTaggedValue new_target_proto = JSHandle<JSObject>::Cast(new_target)->GetPrototype(thread);
            if (new_target_proto == constructor.GetTaggedValue()) {
                return GetOrCreateDerivedJSHClass(thread, new_target_func, constructor, ctor_initial_js_hclass);
            }
        }
    }

    // ECMA2015 9.1.15 3.Let proto be Get(constructor, "prototype").
    JSMutableHandle<JSTaggedValue> prototype(thread, JSTaggedValue::Undefined());
    if (new_target->IsJSFunction()) {
        JSHandle<JSFunction> new_target_func = JSHandle<JSFunction>::Cast(new_target);
        FunctionKind kind = new_target_func->GetFunctionKind();
        if (HasPrototype(kind)) {
            prototype.Update(PrototypeGetter(thread, JSHandle<JSObject>::Cast(new_target_func)));
        }
    } else {
        // Such case: bound function and define a "prototype" property.
        JSHandle<JSTaggedValue> customize_prototype =
            JSTaggedValue::GetProperty(thread, new_target, thread->GlobalConstants()->GetHandledPrototypeString())
                .GetValue();
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSHClass, thread);
        prototype.Update(customize_prototype.GetTaggedValue());
        // Reload JSHClass of constructor, where the lookup of 'prototype' property may change it.
        ctor_initial_js_hclass =
            JSHandle<JSHClass>(thread, JSFunction::GetOrCreateInitialJSHClass(thread, constructor));
    }

    if (!prototype->IsECMAObject()) {
        prototype.Update(constructor->GetFunctionPrototype());
    }

    JSHandle<JSHClass> new_jsh_class = JSHClass::Clone(thread, ctor_initial_js_hclass);
    new_jsh_class->SetPrototype(thread, prototype);

    return new_jsh_class;
}

JSHandle<JSHClass> JSFunction::GetOrCreateDerivedJSHClass(JSThread *thread, JSHandle<JSFunction> derived,
                                                          [[maybe_unused]] JSHandle<JSFunction> constructor,
                                                          JSHandle<JSHClass> ctor_initial_js_hclass)
{
    JSTaggedValue proto_or_dyn(derived->GetProtoOrDynClass());
    // has cached JSHClass, return directly
    if (proto_or_dyn.IsJSHClass()) {
        return JSHandle<JSHClass>(thread, proto_or_dyn);
    }

    JSHandle<JSHClass> new_jsh_class = JSHClass::Clone(thread, ctor_initial_js_hclass);
    // guarante derived has function prototype
    JSHandle<JSTaggedValue> prototype(thread, derived->GetProtoOrDynClass());
    ASSERT(!prototype->IsHole());
    new_jsh_class->SetPrototype(thread, prototype);
    derived->SetProtoOrDynClass(thread, new_jsh_class);
    return new_jsh_class;
}

// Those interface below is discarded
void JSFunction::InitializeJSFunction(JSThread *thread, [[maybe_unused]] const JSHandle<GlobalEnv> &env,
                                      const JSHandle<JSFunction> &func, FunctionKind kind, bool strict)
{
    InitializeJSFunction(thread, func, kind, strict);
}

bool JSFunction::IsDynClass(JSTaggedValue object)
{
    return object.IsJSHClass();
}

DynClass *JSFunction::GetOrCreateInitialDynClass(JSThread *thread, const JSHandle<JSFunction> &fun)
{
    return reinterpret_cast<DynClass *>(JSFunction::GetOrCreateInitialJSHClass(thread, fun));
}

JSHandle<DynClass> JSFunction::GetInstanceDynClass(JSThread *thread, JSHandle<JSFunction> constructor,
                                                   JSHandle<JSTaggedValue> new_target)
{
    return JSHandle<DynClass>(JSFunction::GetInstanceJSHClass(thread, constructor, new_target));
}

bool JSFunction::NameSetter(JSThread *thread, const JSHandle<JSObject> &self, const JSHandle<JSTaggedValue> &value,
                            [[maybe_unused]] bool may_throw)
{
    if (self->IsPropertiesDict()) {
        // replace setter with value
        JSHandle<JSTaggedValue> name_string = thread->GlobalConstants()->GetHandledNameString();
        return self->UpdatePropertyInDictionary(thread, name_string.GetTaggedValue(), value.GetTaggedValue());
    }
    self->SetPropertyInlinedProps(thread, NAME_INLINE_PROPERTY_INDEX, value.GetTaggedValue());
    return true;
}

}  // namespace panda::ecmascript
