/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/class_info_extractor.h"

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"

namespace panda::ecmascript {
void ClassInfoExtractor::BuildClassInfoExtractorFromLiteral(JSThread *thread, JSHandle<ClassInfoExtractor> &extractor,
                                                            const JSHandle<TaggedArray> &literal)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    uint32_t literal_buffer_length = literal->GetLength();
    // non static properties number is hidden in the last index of Literal buffer
    uint32_t non_static_num = literal->Get(thread, literal_buffer_length - 1).GetInt();

    // Reserve sufficient length to prevent frequent creation.
    JSHandle<TaggedArray> non_static_keys = factory->NewTaggedArray(non_static_num + NON_STATIC_RESERVED_LENGTH);
    JSHandle<TaggedArray> non_static_properties = factory->NewTaggedArray(non_static_num + NON_STATIC_RESERVED_LENGTH);

    non_static_keys->Set(thread, CONSTRUCTOR_INDEX, global_const->GetConstructorString());

    JSHandle<TaggedArray> non_static_elements = factory->EmptyArray();

    if (non_static_num != 0U) {
        ExtractContentsDetail non_static_detail {0, non_static_num * 2, NON_STATIC_RESERVED_LENGTH, nullptr};

        if (UNLIKELY(ExtractAndReturnWhetherWithElements(thread, literal, non_static_detail, non_static_keys,
                                                         non_static_properties, non_static_elements))) {
            extractor->SetNonStaticWithElements();
            extractor->SetNonStaticElements(thread, non_static_elements);
        }
    }

    extractor->SetNonStaticKeys(thread, non_static_keys);
    extractor->SetNonStaticProperties(thread, non_static_properties);

    JSHandle<JSHClass> prototype_h_class = CreatePrototypeHClass(thread, non_static_keys, non_static_properties);
    extractor->SetPrototypeHClass(thread, prototype_h_class);

    uint32_t static_num = (literal_buffer_length - 1) / 2 - non_static_num;

    // Reserve sufficient length to prevent frequent creation.
    JSHandle<TaggedArray> static_keys = factory->NewTaggedArray(static_num + STATIC_RESERVED_LENGTH);
    JSHandle<TaggedArray> static_properties = factory->NewTaggedArray(static_num + STATIC_RESERVED_LENGTH);

    static_keys->Set(thread, LENGTH_INDEX, global_const->GetLengthString());
    static_keys->Set(thread, NAME_INDEX, global_const->GetNameString());
    static_keys->Set(thread, PROTOTYPE_INDEX, global_const->GetPrototypeString());

    JSHandle<TaggedArray> static_elements = factory->EmptyArray();

    if (static_num != 0U) {
        ExtractContentsDetail static_detail {non_static_num * 2, literal_buffer_length - 1, STATIC_RESERVED_LENGTH,
                                             extractor->GetConstructorMethod()};

        if (UNLIKELY(ExtractAndReturnWhetherWithElements(thread, literal, static_detail, static_keys, static_properties,
                                                         static_elements))) {
            extractor->SetStaticWithElements();
            extractor->SetStaticElements(thread, static_elements);
        }
    } else {
        // without static properties, set class name
        PandaString cls_name = extractor->GetConstructorMethod()->ParseFunctionName();
        JSHandle<EcmaString> cls_name_handle = factory->NewFromString(cls_name);
        static_properties->Set(thread, NAME_INDEX, cls_name_handle);
    }

    // set prototype internal accessor
    JSHandle<JSTaggedValue> prototype_accessor = global_const->GetHandledFunctionPrototypeAccessor();
    static_properties->Set(thread, PROTOTYPE_INDEX, prototype_accessor);

    extractor->SetStaticKeys(thread, static_keys);
    extractor->SetStaticProperties(thread, static_properties);

    JSHandle<JSHClass> ctor_h_class = CreateConstructorHClass(thread, static_keys, static_properties);
    extractor->SetConstructorHClass(thread, ctor_h_class);
}

bool ClassInfoExtractor::ExtractAndReturnWhetherWithElements(JSThread *thread, const JSHandle<TaggedArray> &literal,
                                                             const ExtractContentsDetail &detail,
                                                             JSHandle<TaggedArray> &keys,
                                                             JSHandle<TaggedArray> &properties,
                                                             JSHandle<TaggedArray> &elements)
{
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    ASSERT(keys->GetLength() == properties->GetLength() && elements->GetLength() == 0);

    uint32_t pos = detail.fill_start_loc;
    bool with_elemenst_flag = false;
    bool is_static_flag = detail.ctor_method != nullptr;
    bool keys_has_name_flag = false;

    JSHandle<JSTaggedValue> name_string = global_const->GetHandledNameString();
    JSMutableHandle<JSTaggedValue> first_value(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> second_value(thread, JSTaggedValue::Undefined());
    for (uint32_t index = detail.extract_begin; index < detail.extract_end; index += 2) {  // 2: key-value pair
        first_value.Update(literal->Get(index));
        second_value.Update(literal->Get(index + 1));
        ASSERT_PRINT(JSTaggedValue::IsPropertyKey(first_value), "Key is not a property key");

        if (LIKELY(first_value->IsString())) {
            if (is_static_flag && !keys_has_name_flag && JSTaggedValue::SameValue(first_value, name_string)) {
                properties->Set(thread, NAME_INDEX, second_value);
                keys_has_name_flag = true;
                continue;
            }

            // front-end can do better: write index in class literal directly.
            uint32_t element_index = 0;
            if (JSTaggedValue::StringToElementIndex(first_value.GetTaggedValue(), &element_index)) {
                ASSERT(element_index < JSObject::MAX_ELEMENT_INDEX);
                uint32_t elements_length = elements->GetLength();
                elements = TaggedArray::SetCapacity(thread, elements, elements_length + 2);  // 2: key-value pair
                elements->Set(thread, elements_length, first_value);
                elements->Set(thread, elements_length + 1, second_value);
                with_elemenst_flag = true;
                continue;
            }
        }

        keys->Set(thread, pos, first_value);
        properties->Set(thread, pos, second_value);
        pos++;
    }

    if (is_static_flag) {
        if (LIKELY(!keys_has_name_flag)) {
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            PandaString cls_name = detail.ctor_method->ParseFunctionName();
            JSHandle<EcmaString> cls_name_handle = factory->NewFromString(cls_name);
            properties->Set(thread, NAME_INDEX, cls_name_handle);
        } else {
            // class has static name property, reserved length bigger 1 than actual, need trim
            uint32_t trim_one_length = keys->GetLength() - 1;
            keys->Trim(thread, trim_one_length);
            properties->Trim(thread, trim_one_length);
        }
    }

    if (UNLIKELY(with_elemenst_flag)) {
        ASSERT(pos + elements->GetLength() / 2 == properties->GetLength());  // 2: half
        keys->Trim(thread, pos);
        properties->Trim(thread, pos);
    }

    return with_elemenst_flag;
}

JSHandle<JSHClass> ClassInfoExtractor::CreatePrototypeHClass(JSThread *thread, JSHandle<TaggedArray> &keys,
                                                             JSHandle<TaggedArray> &properties)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    uint32_t length = keys->GetLength();
    JSHandle<JSHClass> hclass;
    if (LIKELY(length <= PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES)) {
        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        JSHandle<LayoutInfo> layout = factory->CreateLayoutInfo(length);
        for (uint32_t index = 0; index < length; ++index) {
            key.Update(keys->Get(index));
            ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");
            PropertyAttributes attributes = PropertyAttributes::Default(true, false, true);  // non-enumerable

            if (UNLIKELY(properties->Get(index).IsAccessor())) {
                attributes.SetIsAccessor(true);
            }

            attributes.SetIsInlinedProps(true);
            attributes.SetRepresentation(Representation::MIXED);
            attributes.SetOffset(index);
            layout->AddKey(thread, index, key.GetTaggedValue(), attributes);
        }

        hclass = factory->CreateDynClass<JSObject>(JSType::JS_OBJECT, length);
        // Not need set proto here
        hclass->SetLayout(thread, layout);
        hclass->SetNumberOfProps(length);
    } else {
        // dictionary mode
        hclass = factory->CreateDynClass<JSObject>(JSType::JS_OBJECT, 0);  // without in-obj
        hclass->SetIsDictionaryMode(true);
        hclass->SetNumberOfProps(0);
    }

    hclass->SetClassPrototype(true);
    hclass->SetIsPrototype(true);
    return hclass;
}

JSHandle<JSHClass> ClassInfoExtractor::CreateConstructorHClass(JSThread *thread, JSHandle<TaggedArray> &keys,
                                                               JSHandle<TaggedArray> &properties)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    uint32_t length = keys->GetLength();
    JSHandle<JSHClass> hclass;
    if (LIKELY(length <= PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES)) {
        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        JSHandle<LayoutInfo> layout = factory->CreateLayoutInfo(length);
        for (uint32_t index = 0; index < length; ++index) {
            key.Update(keys->Get(index));
            ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key");
            PropertyAttributes attributes;
            switch (index) {
                case LENGTH_INDEX:
                    attributes = PropertyAttributes::Default(false, false, true);
                    break;
                case NAME_INDEX:
                    if (LIKELY(properties->Get(NAME_INDEX).IsString())) {
                        attributes = PropertyAttributes::Default(false, false, true);
                    } else {
                        ASSERT(properties->Get(NAME_INDEX).IsJSFunction() || properties->Get(NAME_INDEX).IsNull());
                        attributes = PropertyAttributes::Default(true, false, true);
                    }
                    break;
                case PROTOTYPE_INDEX:
                    attributes = PropertyAttributes::DefaultAccessor(false, false, false);
                    break;
                default:
                    attributes = PropertyAttributes::Default(true, false, true);
                    break;
            }

            if (UNLIKELY(properties->Get(index).IsAccessor())) {
                attributes.SetIsAccessor(true);
            }

            attributes.SetIsInlinedProps(true);
            attributes.SetRepresentation(Representation::MIXED);
            attributes.SetOffset(index);
            layout->AddKey(thread, index, key.GetTaggedValue(), attributes);
        }

        hclass = factory->CreateDynClass<JSConstructorFunction>(factory->hclass_class_, JSType::JS_FUNCTION,
                                                                HClass::IS_CALLABLE, length);
        // Not need set proto here
        hclass->SetLayout(thread, layout);
        hclass->SetNumberOfProps(length);
    } else {
        // dictionary mode
        hclass = factory->CreateDynClass<JSConstructorFunction>(JSType::JS_FUNCTION, 0);  // without in-obj
        hclass->SetIsDictionaryMode(true);
        hclass->SetNumberOfProps(0);
    }

    hclass->SetClassConstructor(true);
    hclass->SetConstructor(true);

    return hclass;
}

JSHandle<JSFunction> ClassHelper::DefineClassTemplate(JSThread *thread, JSHandle<ClassInfoExtractor> &extractor,
                                                      const JSHandle<ConstantPool> &constantpool)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSHClass> prototype_h_class(thread, extractor->GetPrototypeHClass());
    JSHandle<JSObject> prototype = factory->NewJSObject(prototype_h_class);

    JSHandle<JSHClass> constructor_h_class(thread, extractor->GetConstructorHClass());
    JSHandle<JSFunction> constructor = factory->NewJSFunctionByDynClass(
        extractor->GetConstructorMethod(), constructor_h_class, FunctionKind::CLASS_CONSTRUCTOR);

    // non-static
    JSHandle<TaggedArray> non_static_properties(thread, extractor->GetNonStaticProperties());
    non_static_properties->Set(thread, 0, constructor);

    uint32_t non_static_length = non_static_properties->GetLength();
    JSMutableHandle<JSTaggedValue> prop_value(thread, JSTaggedValue::Undefined());

    if (LIKELY(!prototype_h_class->IsDictionaryMode())) {
        for (uint32_t index = 0; index < non_static_length; ++index) {
            prop_value.Update(non_static_properties->Get(index));
            if (prop_value->IsJSFunction()) {
                JSHandle<JSFunction> prop_func = JSHandle<JSFunction>::Cast(prop_value);
                prop_func->SetHomeObject(thread, prototype);
                prop_func->SetConstantPool(thread, constantpool);
            }
            prototype->SetPropertyInlinedProps(thread, index, prop_value.GetTaggedValue());
        }
    } else {
        JSHandle<TaggedArray> non_static_keys(thread, extractor->GetNonStaticKeys());
        JSHandle<NameDictionary> dict = BuildDictionaryPropeties(
            thread, prototype, non_static_keys, non_static_properties, ClassPropertyType::NON_STATIC, constantpool);
        prototype->SetProperties(thread, dict);
    }

    // non-static elements
    if (UNLIKELY(extractor->IsNonStaticWithElements())) {
        JSHandle<TaggedArray> non_static_elements(thread, extractor->GetNonStaticElements());
        ClassHelper::HandleElementsProperties(thread, prototype, non_static_elements, constantpool);
    }

    // static
    JSHandle<TaggedArray> static_properties(thread, extractor->GetStaticProperties());
    uint32_t static_length = static_properties->GetLength();

    if (LIKELY(!constructor_h_class->IsDictionaryMode())) {
        for (uint32_t index = 0; index < static_length; ++index) {
            prop_value.Update(static_properties->Get(index));
            if (prop_value->IsJSFunction()) {
                JSHandle<JSFunction> prop_func = JSHandle<JSFunction>::Cast(prop_value);
                prop_func->SetHomeObject(thread, constructor);
                prop_func->SetConstantPool(thread, constantpool);
            }
            JSHandle<JSObject>::Cast(constructor)->SetPropertyInlinedProps(thread, index, prop_value.GetTaggedValue());
        }
    } else {
        JSHandle<TaggedArray> static_keys(thread, extractor->GetStaticKeys());
        JSHandle<NameDictionary> dict =
            BuildDictionaryPropeties(thread, JSHandle<JSObject>(constructor), static_keys, static_properties,
                                     ClassPropertyType::STATIC, constantpool);
        constructor->SetProperties(thread, dict);
    }

    // static elements
    if (UNLIKELY(extractor->IsStaticWithElements())) {
        JSHandle<TaggedArray> static_elements(thread, extractor->GetStaticElements());
        ClassHelper::HandleElementsProperties(thread, JSHandle<JSObject>(constructor), static_elements, constantpool);
    }

    constructor->SetProtoOrDynClass(thread, prototype);

    return constructor;
}

JSHandle<NameDictionary> ClassHelper::BuildDictionaryPropeties(JSThread *thread, const JSHandle<JSObject> &object,
                                                               JSHandle<TaggedArray> &keys,
                                                               JSHandle<TaggedArray> &properties,
                                                               ClassPropertyType type,
                                                               const JSHandle<ConstantPool> &constantpool)
{
    uint32_t length = keys->GetLength();
    ASSERT(length > PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES);
    ASSERT(keys->GetLength() == properties->GetLength());

    JSMutableHandle<NameDictionary> dict(thread,
                                         NameDictionary::Create(thread, NameDictionary::ComputeHashTableSize(length)));
    JSMutableHandle<JSTaggedValue> prop_key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> prop_value(thread, JSTaggedValue::Undefined());
    for (uint32_t index = 0; index < length; index++) {
        PropertyAttributes attributes;
        if (type == ClassPropertyType::STATIC) {
            switch (index) {
                case ClassInfoExtractor::LENGTH_INDEX:
                    attributes = PropertyAttributes::Default(false, false, true);
                    break;
                case ClassInfoExtractor::NAME_INDEX:
                    if (LIKELY(properties->Get(ClassInfoExtractor::NAME_INDEX).IsString())) {
                        attributes = PropertyAttributes::Default(false, false, true);
                    } else {
                        ASSERT(properties->Get(ClassInfoExtractor::NAME_INDEX).IsJSFunction());
                        attributes = PropertyAttributes::Default(true, false, true);
                    }
                    break;
                case ClassInfoExtractor::PROTOTYPE_INDEX:
                    attributes = PropertyAttributes::DefaultAccessor(false, false, false);
                    break;
                default:
                    attributes = PropertyAttributes::Default(true, false, true);
                    break;
            }
        } else {
            attributes = PropertyAttributes::Default(true, false, true);  // non-enumerable
        }
        prop_key.Update(keys->Get(index));
        prop_value.Update(properties->Get(index));
        if (prop_value->IsJSFunction()) {
            JSHandle<JSFunction> prop_func = JSHandle<JSFunction>::Cast(prop_value);
            prop_func->SetHomeObject(thread, object);
            prop_func->SetConstantPool(thread, constantpool);
        }
        JSHandle<NameDictionary> new_dict = NameDictionary::PutIfAbsent(thread, dict, prop_key, prop_value, attributes);
        dict.Update(new_dict);
    }

    return dict;
}

void ClassHelper::HandleElementsProperties(JSThread *thread, const JSHandle<JSObject> &object,
                                           JSHandle<TaggedArray> &elements, const JSHandle<ConstantPool> &constantpool)
{
    JSMutableHandle<JSTaggedValue> elements_key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> elements_value(thread, JSTaggedValue::Undefined());
    for (uint32_t index = 0; index < elements->GetLength(); index += 2) {  // 2: key-value pair
        elements_key.Update(elements->Get(index));
        elements_value.Update(elements->Get(index + 1));
        // class property attribute is not default, will transition to dictionary directly.
        JSObject::DefinePropertyByLiteral(thread, object, elements_key, elements_value, true);

        if (elements_value->IsJSFunction()) {
            JSHandle<JSFunction> elements_func = JSHandle<JSFunction>::Cast(elements_value);
            elements_func->SetHomeObject(thread, object);
            elements_func->SetConstantPool(thread, constantpool);
        }
    }
}
}  // namespace panda::ecmascript
