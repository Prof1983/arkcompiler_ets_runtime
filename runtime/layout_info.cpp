/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/layout_info-inl.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/js_symbol.h"

namespace panda::ecmascript {
void LayoutInfo::AddKey(const JSThread *thread, [[maybe_unused]] int index, const JSTaggedValue &key,
                        const PropertyAttributes &attr)
{
    int number = NumberOfElements();
    ASSERT(attr.GetOffset() == static_cast<uint32_t>(number));
    ASSERT(number + 1 <= GetPropertiesCapacity());
    ASSERT(number == index);
    SetNumberOfElements(thread, number + 1);
    SetPropertyInit(thread, number, key, attr);

    uint32_t key_hash = key.GetKeyHashCode();
    int insert_index = number;
    for (; insert_index > 0; --insert_index) {
        JSTaggedValue prev_key = GetSortedKey(insert_index - 1);
        if (prev_key.GetKeyHashCode() <= key_hash) {
            break;
        }
        SetSortedIndex(thread, insert_index, GetSortedIndex(insert_index - 1));
    }
    SetSortedIndex(thread, insert_index, number);
}

void LayoutInfo::GetAllKeys(const JSThread *thread, int end, int offset, TaggedArray *key_array)
{
    ASSERT(end <= NumberOfElements());
    ASSERT_PRINT(offset + end <= static_cast<int>(key_array->GetLength()),
                 "key_array capacity is not enough for dictionary");

    int enum_keys = 0;
    for (int i = 0; i < end; i++) {
        JSTaggedValue key = GetKey(i);
        if (key.IsString()) {
            key_array->Set(thread, enum_keys + offset, key);
            enum_keys++;
        }
    }

    if (enum_keys < end) {
        for (int i = 0; i < end; i++) {
            JSTaggedValue key = GetKey(i);
            if (key.IsSymbol()) {
                key_array->Set(thread, enum_keys + offset, key);
                enum_keys++;
            }
        }
    }
}

void LayoutInfo::GetAllKeys([[maybe_unused]] const JSThread *thread, int end, std::vector<JSTaggedValue> &key_vector)
{
    ASSERT(end <= NumberOfElements());
    for (int i = 0; i < end; i++) {
        JSTaggedValue key = GetKey(i);
        if (key.IsString()) {
            key_vector.emplace_back(key);
        }
    }
}

void LayoutInfo::GetAllEnumKeys(const JSThread *thread, int end, int offset, TaggedArray *key_array, uint32_t *keys)
{
    ASSERT(end <= NumberOfElements());
    ASSERT_PRINT(offset + end <= static_cast<int>(key_array->GetLength()),
                 "key_array capacity is not enough for dictionary");

    int enum_keys = 0;
    for (int i = 0; i < end; i++) {
        JSTaggedValue key = GetKey(i);
        if (key.IsString() && GetAttr(i).IsEnumerable()) {
            key_array->Set(thread, enum_keys + offset, key);
            enum_keys++;
        }
    }
    *keys += enum_keys;
}

void LayoutInfo::GetAllNames(const JSThread *thread, int end, const JSHandle<TaggedArray> &key_array, uint32_t *length)
{
    int array_index = 0;
    for (int i = 0; i < end; i++) {
        JSTaggedValue key = GetKey(i);
        if (key.IsString()) {
            PropertyAttributes attr = GetAttr(i);
            if (attr.IsEnumerable()) {
                key_array->Set(thread, array_index++, key);
            }
        }
    }
    *length += array_index;
}
}  // namespace panda::ecmascript
