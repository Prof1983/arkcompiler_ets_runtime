/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/builtins.h"

#include "ecma_vm.h"
#include "init_icu.h"
#include "plugins/ecmascript/runtime/base/error_type.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/containers/containers_private.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_arraylist.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_async_from_sync_iterator_object.h"
#include "plugins/ecmascript/runtime/js_async_function.h"
#include "plugins/ecmascript/runtime/js_async_generator_object.h"
#include "plugins/ecmascript/runtime/js_collator.h"
#include "plugins/ecmascript/runtime/js_dataview.h"
#include "plugins/ecmascript/runtime/js_date_time_format.h"
#include "plugins/ecmascript/runtime/js_for_in_iterator.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_locale.h"
#include "plugins/ecmascript/runtime/js_map.h"
#include "plugins/ecmascript/runtime/js_map_iterator.h"
#include "plugins/ecmascript/runtime/js_number_format.h"
#include "plugins/ecmascript/runtime/js_plural_rules.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_regexp.h"
#include "plugins/ecmascript/runtime/js_regexp_iterator.h"
#include "plugins/ecmascript/runtime/js_relative_time_format.h"
#include "plugins/ecmascript/runtime/js_runtime_options.h"
#include "plugins/ecmascript/runtime/js_set.h"
#include "plugins/ecmascript/runtime/js_set_iterator.h"
#include "plugins/ecmascript/runtime/js_string_iterator.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/js_weak_container.h"
#include "plugins/ecmascript/runtime/js_finalization_registry.h"
#include "plugins/ecmascript/runtime/mem/mem.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {

using ErrorType = base::ErrorType;
using ContainersPrivate = containers::ContainersPrivate;

void Builtins::Initialize(const JSHandle<GlobalEnv> &env, JSThread *thread)
{
    thread_ = thread;
    vm_ = thread->GetEcmaVM();
    factory_ = vm_->GetFactory();
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<JSTaggedValue> null_handle(thread, JSTaggedValue::Null());

    // Object.prototype[dynclass]
    JSHandle<JSHClass> obj_prototype_dynclass = factory_->CreateDynClass<JSObject>(JSType::JS_OBJECT, null_handle);

    // Object.prototype
    JSHandle<JSObject> obj_func_prototype = factory_->NewJSObject(obj_prototype_dynclass);
    JSHandle<JSTaggedValue> obj_func_prototype_val(obj_func_prototype);

    // Object.prototype_or_dynclass
    JSHandle<JSHClass> obj_func_dynclass =
        factory_->CreateDynClass<JSObject>(JSType::JS_OBJECT, obj_func_prototype_val);

    // GLobalObject.prototype_or_dynclass
    JSHandle<JSHClass> global_obj_func_dynclass = factory_->CreateDynClass<JSObject>(JSType::JS_GLOBAL_OBJECT, 0);
    global_obj_func_dynclass->SetPrototype(thread_, obj_func_prototype_val.GetTaggedValue());
    global_obj_func_dynclass->SetIsDictionaryMode(true);
    // Function.prototype_or_dynclass
    JSHandle<JSHClass> empty_func_dynclass(
        factory_->CreateDynClass<JSFunction>(JSType::JS_FUNCTION, obj_func_prototype_val, HClass::IS_CALLABLE));
    empty_func_dynclass->SetExtensible(true);

    // PrimitiveRef.prototype_or_dynclass
    JSHandle<JSHClass> prim_ref_obj_dynclass =
        factory_->CreateDynClass<JSPrimitiveRef>(JSType::JS_PRIMITIVE_REF, obj_func_prototype_val);

    // init global object
    JSHandle<JSObject> global_object = factory_->NewNonMovableJSObject(global_obj_func_dynclass);
    JSHandle<JSHClass> new_global_dynclass = JSHClass::Clone(thread_, global_obj_func_dynclass);
    global_object->SetClass(new_global_dynclass);
    env->SetJSGlobalObject(thread_, global_object);

    // initialize Function, forbidden change order
    InitializeFunction(env, empty_func_dynclass);

    JSHandle<JSObject> obj_func_instance_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> obj_func_instance_prototype_value(obj_func_instance_prototype);
    JSHandle<JSHClass> async_func_class = factory_->CreateFunctionClass<JSAsyncFunction>(
        FunctionKind::ASYNC_FUNCTION, JSType::JS_ASYNC_FUNCTION, obj_func_instance_prototype_value);
    async_func_class->SetExtensible(true);
    env->SetAsyncFunctionClass(thread_, async_func_class);

    JSHandle<JSHClass> async_await_status_func_class = factory_->CreateFunctionClass<JSAsyncAwaitStatusFunction>(
        FunctionKind::NORMAL_FUNCTION, JSType::JS_ASYNC_AWAIT_STATUS_FUNCTION, env->GetFunctionPrototype());
    env->SetAsyncAwaitStatusFunctionClass(thread_, async_await_status_func_class);

    JSHandle<JSHClass> async_generator_resolve_next_func_class =
        factory_->CreateDynClass<JSAsyncGeneratorResolveNextFunction>(JSType::JS_ASYNC_GENERATOR_RESOLVE_NEXT_FUNCTION,
                                                                      env->GetFunctionPrototype(), HClass::IS_CALLABLE);
    async_generator_resolve_next_func_class->SetExtensible(true);
    env->SetAsyncGeneratorResolveNextFunctionClass(thread_, async_generator_resolve_next_func_class);

    JSHandle<JSHClass> async_from_sync_iterator_value_unwrap_func_class =
        factory_->CreateDynClass<JSAsyncFromSyncIteratorValueUnwrapFunction>(
            JSType::JS_ASYNC_FROM_SYNC_ITERATOR_VALUE_UNWRAP_FUNCTION, env->GetFunctionPrototype(),
            HClass::IS_CALLABLE);
    async_from_sync_iterator_value_unwrap_func_class->SetExtensible(true);
    env->SetAsyncFromSyncIteratorValueUnwrapFunctionClass(thread_, async_from_sync_iterator_value_unwrap_func_class);

    JSHandle<JSHClass> promise_reaction_func_class = factory_->CreateDynClass<JSPromiseReactionsFunction>(
        JSType::JS_PROMISE_REACTIONS_FUNCTION, env->GetFunctionPrototype(), HClass::IS_CALLABLE);
    promise_reaction_func_class->SetExtensible(true);
    env->SetPromiseReactionFunctionClass(thread_, promise_reaction_func_class);

    JSHandle<JSHClass> promise_executor_func_class = factory_->CreateDynClass<JSPromiseExecutorFunction>(
        JSType::JS_PROMISE_EXECUTOR_FUNCTION, env->GetFunctionPrototype(), HClass::IS_CALLABLE);
    promise_executor_func_class->SetExtensible(true);
    env->SetPromiseExecutorFunctionClass(thread_, promise_executor_func_class);

    JSHandle<JSHClass> promise_all_resolve_element_function_class =
        factory_->CreateDynClass<JSPromiseAllResolveElementFunction>(JSType::JS_PROMISE_ALL_RESOLVE_ELEMENT_FUNCTION,
                                                                     env->GetFunctionPrototype(), HClass::IS_CALLABLE);
    promise_all_resolve_element_function_class->SetExtensible(true);
    env->SetPromiseAllResolveElementFunctionClass(thread_, promise_all_resolve_element_function_class);

    JSHandle<JSHClass> proxy_revoc_func_class = factory_->CreateDynClass<JSProxyRevocFunction>(
        JSType::JS_PROXY_REVOC_FUNCTION, env->GetFunctionPrototype(), HClass::IS_CALLABLE);
    proxy_revoc_func_class->SetExtensible(true);
    env->SetProxyRevocFunctionClass(thread_, proxy_revoc_func_class);

    // Object = new Function()
    JSHandle<JSObject> object_function(
        NewBuiltinConstructor(env, obj_func_prototype, object::ObjectConstructor, "Object", FunctionLength::ONE));
    object_function.GetObject<JSFunction>()->SetFunctionPrototype(thread_, obj_func_dynclass.GetTaggedValue());
    // initialize object method.
    env->SetObjectFunction(thread_, object_function);
    env->SetObjectFunctionPrototype(thread_, obj_func_prototype);

    JSHandle<JSHClass> function_class = factory_->CreateFunctionClass<JSFunction>(
        FunctionKind::BASE_CONSTRUCTOR, JSType::JS_FUNCTION, env->GetFunctionPrototype());
    env->SetFunctionClassWithProto(thread_, function_class);
    function_class = factory_->CreateFunctionClass<JSFunction>(FunctionKind::NORMAL_FUNCTION, JSType::JS_FUNCTION,
                                                               env->GetFunctionPrototype());
    env->SetFunctionClassWithoutProto(thread_, function_class);
    function_class = factory_->CreateFunctionClass<JSFunction>(FunctionKind::CLASS_CONSTRUCTOR, JSType::JS_FUNCTION,
                                                               env->GetFunctionPrototype());
    env->SetFunctionClassWithoutName(thread_, function_class);

    if (env == vm_->GetGlobalEnv()) {
        InitializeAllTypeError(env, obj_func_dynclass);
        InitializeSymbol(env, prim_ref_obj_dynclass);
    } else {
        // error and symbol need to be shared when initialize realm
        InitializeAllTypeErrorWithRealm(env);
        InitializeSymbolWithRealm(env, prim_ref_obj_dynclass);
    }

    InitializeNumber(env, prim_ref_obj_dynclass);
    InitializeBigInt(env, obj_func_dynclass);
    InitializeDate(env, obj_func_dynclass);
    InitializeObject(env, obj_func_prototype, object_function);
    InitializeBoolean(env, prim_ref_obj_dynclass);

    InitializeRegExp(env);
    InitializeSet(env, obj_func_dynclass);
    InitializeMap(env, obj_func_dynclass);
    InitializeWeakRef(env, obj_func_dynclass);
    InitializeWeakMap(env, obj_func_dynclass);
    InitializeWeakSet(env, obj_func_dynclass);
    InitializeArray(env, obj_func_prototype_val);
    InitializeTypedArray(env, obj_func_dynclass);
    InitializeString(env, prim_ref_obj_dynclass);
    InitializeArrayBuffer(env, obj_func_dynclass);
    InitializeDataView(env, obj_func_dynclass);

    JSHandle<JSHClass> arguments_dynclass = factory_->CreateJSArguments();
    env->SetArgumentsClass(thread_, arguments_dynclass);
    SetArgumentsSharedAccessor(env);

    InitializeGlobalObject(env, global_object);
    InitializeMath(env, obj_func_prototype_val);
    InitializeJson(env, obj_func_prototype_val);
    InitializeIterator(env, obj_func_dynclass);
    InitializeAsyncIterator(env, obj_func_dynclass);
    InitializeProxy(env);
    InitializeReflect(env, obj_func_prototype_val);
    InitializeAsyncFunction(env, obj_func_dynclass);
    InitializeGenerator(env, obj_func_dynclass);
    InitializeGeneratorFunction(env, obj_func_dynclass);
    InitializeAsyncGenerator(env, obj_func_dynclass);
    InitializeAsyncGeneratorFunction(env, obj_func_dynclass);
    InitializeAsyncFromSyncIteratorPrototypeObject(env, obj_func_dynclass);
    InitializePromise(env, obj_func_dynclass);
    InitializePromiseJob(env);
    InitializeFinalizationRegistry(env, obj_func_dynclass);

    std::string icu_path = vm_->GetJSOptions().GetIcuDataPath();
    if (icu_path == "default") {
        SetIcuDirectory();
    } else {
        u_setDataDirectory(icu_path.c_str());
    }
    InitializeIntl(env, obj_func_prototype_val);
    InitializeLocale(env);
    InitializeDateTimeFormat(env);
    InitializeNumberFormat(env);
    InitializeRelativeTimeFormat(env);
    InitializeCollator(env);
    InitializePluralRules(env);

    JSHandle<JSHClass> generator_func_class = factory_->CreateFunctionClass<JSGeneratorFunction>(
        FunctionKind::GENERATOR_FUNCTION, JSType::JS_GENERATOR_FUNCTION, env->GetGeneratorFunctionPrototype());
    env->SetGeneratorFunctionClass(thread_, generator_func_class);

    JSHandle<JSHClass> async_generator_func_class = factory_->CreateFunctionClass<JSAsyncGeneratorFunction>(
        FunctionKind::ASYNC_GENERATOR_FUNCTION, JSType::JS_ASYNC_GENERATOR_FUNCTION,
        env->GetAsyncGeneratorFunctionPrototype());
    env->SetAsyncGeneratorFunctionClass(thread_, async_generator_func_class);

    env->SetObjectFunctionPrototypeClass(thread_, JSTaggedValue(obj_func_prototype->GetClass()));
    thread_->ResetGuardians();

#ifdef FUZZING_FUZZILLI_BUILTIN
    InitializeFuzzilli(env, obj_func_dynclass);
#endif
}

void Builtins::InitializeGlobalObject(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &global_object)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);

    InitializeGcMarker(env);

    // TODO(ivagin): Remove ArkTools?
    if (vm_->GetJSOptions().IsEnableArkTools()) {
        JSHandle<JSTaggedValue> ark_tools(InitializeArkTools(env));
        SetConstantObject(global_object, "ArkTools", ark_tools);
    }

#ifndef PANDA_PRODUCT_BUILD
    JSHandle<JSTaggedValue> runtime_testing(InitializeRuntimeTesting(env));
    SetConstantObject(global_object, "RuntimeTesting", runtime_testing);
#endif  // PANDA_PRODUCT_BUILD

#if ECMASCRIPT_ENABLE_ARK_CONTAINER
    // Set ArkPrivate
    JSHandle<JSTaggedValue> ark_private(InitializeArkPrivate(env));
    SetConstantObject(global_object, "ArkPrivate", ark_private);
#endif

    // Global object property
    SetGlobalThis(global_object, "globalThis", JSHandle<JSTaggedValue>::Cast(global_object));
    SetConstant(global_object, "Infinity", JSTaggedValue(base::POSITIVE_INFINITY));
    SetConstant(global_object, "NaN", JSTaggedValue(base::NAN_VALUE));
    SetConstant(global_object, "undefined", JSTaggedValue::Undefined());

    SetFunctionsGenGlobal(env, global_object);
}

void Builtins::InitializeFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &empty_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Initialize Function.prototype
    JSMethod *invoke_self =
        vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::function::FunctionPrototypeInvokeSelf));
    JSHandle<JSFunction> func_func_prototype = factory_->NewJSFunctionByDynClass(invoke_self, empty_func_dynclass);
    // ecma 19.2.3 The value of the name property of the Function prototype object is the empty String.
    JSHandle<JSTaggedValue> empty_string(thread_->GlobalConstants()->GetHandledEmptyString());
    JSHandle<JSTaggedValue> undefined_string(thread_, JSTaggedValue::Undefined());
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(func_func_prototype), empty_string, undefined_string);
    // ecma 19.2.3 The value of the length property of the Function prototype object is 0.
    JSFunction::SetFunctionLength(thread_, func_func_prototype, JSTaggedValue(FunctionLength::ZERO));

    JSHandle<JSTaggedValue> func_func_prototype_value(func_func_prototype);
    // Function.prototype_or_dynclass
    JSHandle<JSHClass> func_func_intance_dynclass = factory_->CreateDynClass<JSFunction>(
        JSType::JS_FUNCTION, func_func_prototype_value, HClass::IS_CALLABLE | HClass::IS_BUILTINS_CTOR);
    func_func_intance_dynclass->SetConstructor(true);

    // Function = new Function() (forbidden use NewBuiltinConstructor)
    JSMethod *ctor = vm_->GetMethodForNativeFunction(reinterpret_cast<void *>(builtins::function::FunctionConstructor));
    JSHandle<JSFunction> func_func =
        factory_->NewJSFunctionByDynClass(ctor, func_func_intance_dynclass, FunctionKind::BUILTIN_CONSTRUCTOR);

    auto func_func_prototype_obj = JSHandle<JSObject>(func_func_prototype);
    InitializeCtor(env, func_func_prototype_obj, func_func, "Function", FunctionLength::ONE);

    func_func->SetFunctionPrototype(thread_, func_func_intance_dynclass.GetTaggedValue());
    env->SetFunctionFunction(thread_, func_func);
    env->SetFunctionPrototype(thread_, func_func_prototype);

    JSHandle<JSHClass> normal_func_class =
        factory_->CreateDynClass<JSFunction>(JSType::JS_FUNCTION, env->GetFunctionPrototype(), HClass::IS_CALLABLE);
    env->SetNormalFunctionClass(thread_, normal_func_class);

    JSHandle<JSHClass> j_s_intl_bound_function_class = factory_->CreateFunctionClass<JSIntlBoundFunction>(
        FunctionKind::NORMAL_FUNCTION, JSType::JS_INTL_BOUND_FUNCTION, env->GetFunctionPrototype());
    env->SetJSIntlBoundFunctionClass(thread_, j_s_intl_bound_function_class);

    JSHandle<JSHClass> constructor_function_class = factory_->CreateDynClass<JSFunction>(
        JSType::JS_FUNCTION, env->GetFunctionPrototype(), HClass::IS_CALLABLE | HClass::IS_BUILTINS_CTOR);
    constructor_function_class->SetConstructor(true);
    env->SetConstructorFunctionClass(thread_, constructor_function_class);

    StrictModeForbiddenAccessCallerArguments(env, func_func_prototype_obj);

    SetFunctionsGenFunctionProto(env, func_func_prototype_obj);
}

void Builtins::InitializeObject(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj_func_prototype,
                                const JSHandle<JSObject> &obj_func)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    SetFunctionsGenObject(env, obj_func);

    SetFunctionsGenObjectProto(env, obj_func_prototype);
}

void Builtins::InitializeSymbol(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // Symbol.prototype
    JSHandle<JSObject> symbol_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> symbol_func_prototype_value(symbol_func_prototype);

    // Symbol.prototype_or_dynclass
    JSHandle<JSHClass> symbol_func_instance_dynclass =
        factory_->CreateDynClass<JSPrimitiveRef>(JSType::JS_PRIMITIVE_REF, symbol_func_prototype_value);

    // Symbol = new Function()
    JSHandle<JSObject> symbol_function(NewBuiltinConstructor(
        env, symbol_func_prototype, builtins::symbol::SymbolConstructor, "Symbol", FunctionLength::ZERO));
    JSHandle<JSFunction>(symbol_function)
        ->SetFunctionPrototype(thread_, symbol_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    PropertyDescriptor descriptor(thread_, JSHandle<JSTaggedValue>::Cast(symbol_function), true, false, true);
    JSObject::DefineOwnProperty(thread_, symbol_func_prototype, constructor_key, descriptor);

    // Symbol attribute
    JSHandle<JSTaggedValue> has_instance_symbol(factory_->NewWellKnownSymbolWithChar("Symbol.hasInstance"));
    SetNoneAttributeProperty(symbol_function, "hasInstance", has_instance_symbol);
    JSHandle<JSTaggedValue> is_concat_spreadable_symbol(
        factory_->NewWellKnownSymbolWithChar("Symbol.isConcatSpreadable"));
    SetNoneAttributeProperty(symbol_function, "isConcatSpreadable", is_concat_spreadable_symbol);
    JSHandle<JSTaggedValue> to_string_tag_symbol(factory_->NewWellKnownSymbolWithChar("Symbol.toStringTag"));
    SetNoneAttributeProperty(symbol_function, "toStringTag", to_string_tag_symbol);
    JSHandle<JSTaggedValue> iterator_symbol(factory_->NewPublicSymbolWithChar("Symbol.iterator"));
    SetNoneAttributeProperty(symbol_function, "iterator", iterator_symbol);
    JSHandle<JSTaggedValue> async_iterator_symbol(factory_->NewPublicSymbolWithChar("Symbol.asyncIterator"));
    SetNoneAttributeProperty(symbol_function, "asyncIterator", async_iterator_symbol);
    JSHandle<JSTaggedValue> match_symbol(factory_->NewPublicSymbolWithChar("Symbol.match"));
    SetNoneAttributeProperty(symbol_function, "match", match_symbol);
    JSHandle<JSTaggedValue> match_all_symbol(factory_->NewPublicSymbolWithChar("Symbol.matchAll"));
    SetNoneAttributeProperty(symbol_function, "matchAll", match_all_symbol);
    JSHandle<JSTaggedValue> replace_symbol(factory_->NewPublicSymbolWithChar("Symbol.replace"));
    SetNoneAttributeProperty(symbol_function, "replace", replace_symbol);
    JSHandle<JSTaggedValue> search_symbol(factory_->NewPublicSymbolWithChar("Symbol.search"));
    SetNoneAttributeProperty(symbol_function, "search", search_symbol);
    JSHandle<JSTaggedValue> species_symbol(factory_->NewPublicSymbolWithChar("Symbol.species"));
    SetNoneAttributeProperty(symbol_function, "species", species_symbol);
    JSHandle<JSTaggedValue> split_symbol(factory_->NewPublicSymbolWithChar("Symbol.split"));
    SetNoneAttributeProperty(symbol_function, "split", split_symbol);
    JSHandle<JSTaggedValue> to_primitive_symbol(factory_->NewPublicSymbolWithChar("Symbol.toPrimitive"));
    SetNoneAttributeProperty(symbol_function, "toPrimitive", to_primitive_symbol);
    JSHandle<JSTaggedValue> unscopables_symbol(factory_->NewPublicSymbolWithChar("Symbol.unscopables"));
    SetNoneAttributeProperty(symbol_function, "unscopables", unscopables_symbol);

    env->SetSymbolFunction(thread_, symbol_function);
    env->SetHasInstanceSymbol(thread_, has_instance_symbol);
    env->SetIsConcatSpreadableSymbol(thread_, is_concat_spreadable_symbol);
    env->SetToStringTagSymbol(thread_, to_string_tag_symbol);
    env->SetIteratorSymbol(thread_, iterator_symbol);
    env->SetAsyncIteratorSymbol(thread_, async_iterator_symbol);
    env->SetMatchSymbol(thread_, match_symbol);
    env->SetMatchAllSymbol(thread_, match_all_symbol);
    env->SetReplaceSymbol(thread_, replace_symbol);
    env->SetSearchSymbol(thread_, search_symbol);
    env->SetSpeciesSymbol(thread_, species_symbol);
    env->SetSplitSymbol(thread_, split_symbol);
    env->SetToPrimitiveSymbol(thread_, to_primitive_symbol);
    env->SetUnscopablesSymbol(thread_, unscopables_symbol);

    // Setup %SymbolPrototype%
    SetStringTagSymbol(env, symbol_func_prototype, "Symbol");

    JSHandle<JSTaggedValue> holey_symbol(factory_->NewPrivateNameSymbolWithChar("holey"));
    env->SetHoleySymbol(thread_, holey_symbol.GetTaggedValue());
    JSHandle<JSTaggedValue> element_ic_symbol(factory_->NewPrivateNameSymbolWithChar("element-ic"));
    env->SetElementICSymbol(thread_, element_ic_symbol.GetTaggedValue());

    // ecma 19.2.3.6 Function.prototype[@@hasInstance] ( V )
    JSHandle<JSObject> func_func_prototype_obj = JSHandle<JSObject>(env->GetFunctionPrototype());
    SetFunctionAtSymbol<JSSymbol::SymbolType::HAS_INSTANCE>(
        env, func_func_prototype_obj, env->GetHasInstanceSymbol(), "[Symbol.hasInstance]",
        builtins::function::proto::HasInstance, FunctionLength::ONE);

    SetFunctionsGenSymbol(env, symbol_function);
    SetFunctionsGenSymbolProto(env, symbol_func_prototype);
}

void Builtins::InitializeSymbolWithRealm(const JSHandle<GlobalEnv> &realm,
                                         const JSHandle<JSHClass> &obj_func_instance_dynclass)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    // Symbol.prototype
    JSHandle<JSObject> symbol_func_prototype = factory_->NewJSObject(obj_func_instance_dynclass);
    JSHandle<JSTaggedValue> symbol_func_prototype_value(symbol_func_prototype);

    // Symbol.prototype_or_dynclass
    JSHandle<JSHClass> symbol_func_instance_dynclass =
        factory_->CreateDynClass<JSPrimitiveRef>(JSType::JS_PRIMITIVE_REF, symbol_func_prototype_value);

    // Symbol = new Function()
    JSHandle<JSObject> symbol_function(NewBuiltinConstructor(
        realm, symbol_func_prototype, builtins::symbol::SymbolConstructor, "Symbol", FunctionLength::ZERO));
    JSHandle<JSFunction>(symbol_function)
        ->SetFunctionPrototype(thread_, symbol_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = thread_->GlobalConstants()->GetHandledConstructorString();
    PropertyDescriptor descriptor(thread_, JSHandle<JSTaggedValue>::Cast(symbol_function), true, false, true);
    JSObject::DefineOwnProperty(thread_, symbol_func_prototype, constructor_key, descriptor);

    // Symbol attribute
    SetNoneAttributeProperty(symbol_function, "hasInstance", env->GetHasInstanceSymbol());
    SetNoneAttributeProperty(symbol_function, "isConcatSpreadable", env->GetIsConcatSpreadableSymbol());
    SetNoneAttributeProperty(symbol_function, "toStringTag", env->GetToStringTagSymbol());
    SetNoneAttributeProperty(symbol_function, "iterator", env->GetIteratorSymbol());
    SetNoneAttributeProperty(symbol_function, "asyncIterator", env->GetAsyncIteratorSymbol());
    SetNoneAttributeProperty(symbol_function, "match", env->GetMatchSymbol());
    SetNoneAttributeProperty(symbol_function, "matchAll", env->GetMatchAllSymbol());
    SetNoneAttributeProperty(symbol_function, "replace", env->GetReplaceSymbol());
    SetNoneAttributeProperty(symbol_function, "search", env->GetSearchSymbol());
    SetNoneAttributeProperty(symbol_function, "species", env->GetSpeciesSymbol());
    SetNoneAttributeProperty(symbol_function, "split", env->GetSplitSymbol());
    SetNoneAttributeProperty(symbol_function, "toPrimitive", env->GetToPrimitiveSymbol());
    SetNoneAttributeProperty(symbol_function, "unscopables", env->GetUnscopablesSymbol());

    realm->SetSymbolFunction(thread_, symbol_function);
    realm->SetHasInstanceSymbol(thread_, env->GetHasInstanceSymbol());
    realm->SetIsConcatSpreadableSymbol(thread_, env->GetIsConcatSpreadableSymbol());
    realm->SetToStringTagSymbol(thread_, env->GetToStringTagSymbol());
    realm->SetIteratorSymbol(thread_, env->GetIteratorSymbol());
    realm->SetAsyncIteratorSymbol(thread_, env->GetAsyncIteratorSymbol());
    realm->SetMatchSymbol(thread_, env->GetMatchSymbol());
    realm->SetMatchAllSymbol(thread_, env->GetMatchAllSymbol());
    realm->SetReplaceSymbol(thread_, env->GetReplaceSymbol());
    realm->SetSearchSymbol(thread_, env->GetSearchSymbol());
    realm->SetSpeciesSymbol(thread_, env->GetSpeciesSymbol());
    realm->SetSplitSymbol(thread_, env->GetSplitSymbol());
    realm->SetToPrimitiveSymbol(thread_, env->GetToPrimitiveSymbol());
    realm->SetUnscopablesSymbol(thread_, env->GetUnscopablesSymbol());

    // Setup %SymbolPrototype%
    SetStringTagSymbol(realm, symbol_func_prototype, "Symbol");

    JSHandle<JSTaggedValue> holey_symbol(factory_->NewPrivateNameSymbolWithChar("holey"));
    realm->SetHoleySymbol(thread_, holey_symbol.GetTaggedValue());
    JSHandle<JSTaggedValue> element_ic_symbol(factory_->NewPrivateNameSymbolWithChar("element-ic"));
    realm->SetElementICSymbol(thread_, element_ic_symbol.GetTaggedValue());

    // ecma 19.2.3.6 Function.prototype[@@hasInstance] ( V )
    JSHandle<JSObject> func_func_prototype_obj = JSHandle<JSObject>(realm->GetFunctionPrototype());
    SetFunctionAtSymbol<JSSymbol::SymbolType::HAS_INSTANCE>(
        realm, func_func_prototype_obj, realm->GetHasInstanceSymbol(), "[Symbol.hasInstance]",
        builtins::function::proto::HasInstance, FunctionLength::ONE);

    SetFunctionsGenSymbol(realm, symbol_function);
    SetFunctionsGenSymbolProto(realm, symbol_func_prototype);
}

void Builtins::InitializeNumber(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &prim_ref_obj_dynclass)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Number.prototype
    JSHandle<JSTaggedValue> to_object(thread_, JSTaggedValue(FunctionLength::ZERO));
    JSHandle<JSObject> num_func_prototype =
        JSHandle<JSObject>::Cast(factory_->NewJSPrimitiveRef(prim_ref_obj_dynclass, to_object));
    JSHandle<JSTaggedValue> num_func_prototype_value(num_func_prototype);

    // Number.prototype_or_dynclass
    JSHandle<JSHClass> num_func_instance_class =
        factory_->CreateDynClass<JSPrimitiveRef>(JSType::JS_PRIMITIVE_REF, num_func_prototype_value);

    // Number = new Function()
    JSHandle<JSObject> num_function(NewBuiltinConstructor(env, num_func_prototype, builtins::number::NumberConstructor,
                                                          "Number", FunctionLength::ONE));
    num_function.GetObject<JSFunction>()->SetFunctionPrototype(thread_, num_func_instance_class.GetTaggedValue());

    env->SetNumberFunction(thread_, num_function);

    // Number method

    // Number constant
    const double epsilon = 2.220446049250313e-16;
    const double max_safe_integer = 9007199254740991;
    const double max_value = 1.7976931348623157e+308;
    const double min_value = 5e-324;
    const double positive_infinity = std::numeric_limits<double>::infinity();
    SetConstant(num_function, "MAX_VALUE", JSTaggedValue(max_value));
    SetConstant(num_function, "MIN_VALUE", JSTaggedValue(min_value));
    SetConstant(num_function, "NaN", JSTaggedValue(NAN));
    SetConstant(num_function, "NEGATIVE_INFINITY", JSTaggedValue(-positive_infinity));
    SetConstant(num_function, "POSITIVE_INFINITY", JSTaggedValue(positive_infinity));
    SetConstant(num_function, "MAX_SAFE_INTEGER", JSTaggedValue(max_safe_integer));
    SetConstant(num_function, "MIN_SAFE_INTEGER", JSTaggedValue(-max_safe_integer));
    SetConstant(num_function, "EPSILON", JSTaggedValue(epsilon));

    SetFunctionsGenNumberProto(env, num_func_prototype);

    SetFunctionsGenNumber(env, num_function);
}

void Builtins::InitializeBigInt(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // BigInt.prototype
    JSHandle<JSObject> big_int_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> big_int_func_prototype_value(big_int_func_prototype);

    // BigInt.prototype_or_dynclass
    JSHandle<JSHClass> big_int_func_instance_dynclass =
        factory_->CreateDynClass<JSPrimitiveRef>(JSType::JS_PRIMITIVE_REF, big_int_func_prototype_value);
    // BigInt = new Function()
    JSHandle<JSObject> big_int_function(NewBuiltinConstructor(
        env, big_int_func_prototype, builtins::big_int::BigIntConstructor, "BigInt", FunctionLength::ONE));
    JSHandle<JSFunction>(big_int_function)
        ->SetFunctionPrototype(thread_, big_int_func_instance_dynclass.GetTaggedValue());

    env->SetBigIntFunction(thread_, big_int_function);

    // @@ToStringTag
    SetStringTagSymbol(env, big_int_func_prototype, "BigInt");
    SetFunctionsGenBigIntProto(env, big_int_func_prototype);
    SetFunctionsGenBigInt(env, big_int_function);
}

void Builtins::InitializeDate(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const int utc_length = 7;
    // Date.prototype
    JSHandle<JSObject> date_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> date_func_prototype_value(date_func_prototype);

    // Date.prototype_or_dynclass
    JSHandle<JSHClass> date_func_instance_dynclass =
        factory_->CreateDynClass<JSDate>(JSType::JS_DATE, date_func_prototype_value);

    // Date = new Function()
    JSHandle<JSObject> date_function(
        NewBuiltinConstructor(env, date_func_prototype, builtins::date::DateConstructor, "Date", FunctionLength::ONE));
    JSHandle<JSFunction>(date_function)->SetFunctionPrototype(thread_, date_func_instance_dynclass.GetTaggedValue());

    env->SetDateFunction(thread_, date_function);

    // Date.length
    SetConstant(date_function, "length", JSTaggedValue(utc_length));
    SetFunctionsGenDateProto(env, date_func_prototype);

    SetFunctionsGenDate(env, date_function);
}

void Builtins::InitializeBoolean(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &prim_ref_obj_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Boolean.prototype
    JSHandle<JSTaggedValue> to_object(thread_, JSTaggedValue::False());
    JSHandle<JSObject> boolean_func_prototype =
        JSHandle<JSObject>::Cast(factory_->NewJSPrimitiveRef(prim_ref_obj_dynclass, to_object));
    JSHandle<JSTaggedValue> boolean_func_prototype_value(boolean_func_prototype);

    // Boolean.prototype_or_dynclass
    JSHandle<JSHClass> boolean_func_instance_dynclass =
        factory_->CreateDynClass<JSPrimitiveRef>(JSType::JS_PRIMITIVE_REF, boolean_func_prototype_value);

    // new Boolean Function()
    JSHandle<JSFunction> boolean_function(NewBuiltinConstructor(
        env, boolean_func_prototype, builtins::boolean::BooleanConstructor, "Boolean", FunctionLength::ONE));
    boolean_function->SetFunctionPrototype(thread_, boolean_func_instance_dynclass.GetTaggedValue());

    env->SetBooleanFunction(thread_, boolean_function);
    SetFunctionsGenBooleanProto(env, boolean_func_prototype);
}

void Builtins::InitializeProxy(const JSHandle<GlobalEnv> &env)
{
    JSHandle<JSObject> proxy_function(InitializeExoticConstructor(env, builtins::proxy::ProxyConstructor, "Proxy", 2));
    env->SetProxyFunction(thread_, proxy_function);
    SetFunctionsGenProxy(env, proxy_function);
}

JSHandle<JSFunction> Builtins::InitializeExoticConstructor(const JSHandle<GlobalEnv> &env, EcmaEntrypoint ctor_func,
                                                           const char *name, int length)
{
    JSHandle<JSFunction> ctor =
        factory_->NewJSFunction(env, reinterpret_cast<void *>(ctor_func), FunctionKind::BUILTIN_PROXY_CONSTRUCTOR);

    JSFunction::SetFunctionLength(thread_, ctor, JSTaggedValue(length));
    JSHandle<JSTaggedValue> name_string(factory_->NewFromString(name));
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(ctor), name_string,
                                JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Undefined()));

    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    PropertyDescriptor descriptor(thread_, JSHandle<JSTaggedValue>(ctor), true, false, true);
    JSObject::DefineOwnProperty(thread_, global_object, name_string, descriptor);
    return ctor;
}

void Builtins::InitializeAsyncFunction(const JSHandle<GlobalEnv> &env,
                                       const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // AsyncFunction.prototype
    JSHandle<JSObject> async_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSObject::SetPrototype(thread_, async_func_prototype, env->GetFunctionPrototype());
    JSHandle<JSTaggedValue> async_func_prototype_value(async_func_prototype);

    // AsyncFunction.prototype_or_dynclass
    JSHandle<JSHClass> async_func_instance_dynclass =
        factory_->CreateDynClass<JSAsyncFunction>(JSType::JS_ASYNC_FUNCTION, async_func_prototype_value);

    // AsyncFunction = new Function()
    JSHandle<JSFunction> async_function =
        NewBuiltinConstructor(env, async_func_prototype, builtins::async_function::AsyncFunctionConstructor,
                              "AsyncFunction", FunctionLength::ONE);
    JSObject::SetPrototype(thread_, JSHandle<JSObject>::Cast(async_function), env->GetFunctionFunction());
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    PropertyDescriptor async_desc(thread_, JSHandle<JSTaggedValue>::Cast(async_function), false, false, true);
    JSObject::DefineOwnProperty(thread_, async_func_prototype, constructor_key, async_desc);
    async_function->SetProtoOrDynClass(thread_, async_func_instance_dynclass.GetTaggedValue());

    // AsyncFunction.prototype property
    SetStringTagSymbol(env, async_func_prototype, "AsyncFunction");
    env->SetAsyncFunction(thread_, async_function);
    env->SetAsyncFunctionPrototype(thread_, async_func_prototype);
}

void Builtins::InitializeAllTypeError(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    // Error.prototype
    JSHandle<JSObject> error_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> error_func_prototype_value(error_func_prototype);
    // Error.prototype_or_dynclass
    JSHandle<JSHClass> error_func_instance_dynclass =
        factory_->CreateDynClass<JSObject>(JSType::JS_ERROR, error_func_prototype_value);
    // Error() = new Function()
    JSHandle<JSFunction> error_function(NewBuiltinConstructor(
        env, error_func_prototype, builtins::error::ErrorConstructor, "Error", FunctionLength::ONE));
    error_function->SetFunctionPrototype(thread_, error_func_instance_dynclass.GetTaggedValue());
    env->SetErrorFunction(thread_, error_function);

    // Error.prototype Attribute
    SetAttribute(error_func_prototype, "name", "Error");
    SetAttribute(error_func_prototype, "message", "");

    SetFunctionsGenErrorProto(env, error_func_prototype);

    JSHandle<JSHClass> native_error_func_class = factory_->CreateDynClass<JSFunction>(
        JSType::JS_FUNCTION, env->GetErrorFunction(), HClass::IS_CALLABLE | HClass::IS_BUILTINS_CTOR);
    native_error_func_class->SetConstructor(true);
    env->SetNativeErrorFunctionClass(thread_, native_error_func_class);

    JSHandle<JSHClass> error_native_func_instance_dynclass =
        factory_->CreateDynClass<JSObject>(JSType::JS_OBJECT, error_func_prototype_value);
    InitializeError(env, error_native_func_instance_dynclass, JSType::JS_RANGE_ERROR);
    InitializeError(env, error_native_func_instance_dynclass, JSType::JS_REFERENCE_ERROR);
    InitializeError(env, error_native_func_instance_dynclass, JSType::JS_TYPE_ERROR);
    InitializeError(env, error_native_func_instance_dynclass, JSType::JS_URI_ERROR);
    InitializeError(env, error_native_func_instance_dynclass, JSType::JS_SYNTAX_ERROR);
    InitializeError(env, error_native_func_instance_dynclass, JSType::JS_EVAL_ERROR);
}

void Builtins::InitializeAllTypeErrorWithRealm(const JSHandle<GlobalEnv> &realm) const
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();

    realm->SetErrorFunction(thread_, env->GetErrorFunction());
    realm->SetNativeErrorFunctionClass(thread_, env->GetNativeErrorFunctionClass());

    SetErrorWithRealm(realm, JSType::JS_RANGE_ERROR);
    SetErrorWithRealm(realm, JSType::JS_REFERENCE_ERROR);
    SetErrorWithRealm(realm, JSType::JS_TYPE_ERROR);
    SetErrorWithRealm(realm, JSType::JS_URI_ERROR);
    SetErrorWithRealm(realm, JSType::JS_SYNTAX_ERROR);
    SetErrorWithRealm(realm, JSType::JS_EVAL_ERROR);
}

void Builtins::SetErrorWithRealm(const JSHandle<GlobalEnv> &realm, const JSType &error_tag) const
{
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    JSHandle<JSObject> global_object(thread_, realm->GetGlobalObject());
    JSHandle<JSTaggedValue> name_string;
    JSHandle<JSTaggedValue> native_error_function;
    switch (error_tag) {
        case JSType::JS_RANGE_ERROR:
            native_error_function = env->GetRangeErrorFunction();
            name_string = JSHandle<JSTaggedValue>(thread_->GlobalConstants()->GetHandledRangeErrorString());
            realm->SetRangeErrorFunction(thread_, native_error_function);
            break;
        case JSType::JS_EVAL_ERROR:
            native_error_function = env->GetEvalErrorFunction();
            name_string = JSHandle<JSTaggedValue>(thread_->GlobalConstants()->GetHandledEvalErrorString());
            realm->SetEvalErrorFunction(thread_, native_error_function);
            break;
        case JSType::JS_REFERENCE_ERROR:
            native_error_function = env->GetReferenceErrorFunction();
            name_string = JSHandle<JSTaggedValue>(thread_->GlobalConstants()->GetHandledReferenceErrorString());
            realm->SetReferenceErrorFunction(thread_, native_error_function);
            break;
        case JSType::JS_TYPE_ERROR:
            native_error_function = env->GetTypeErrorFunction();
            name_string = JSHandle<JSTaggedValue>(thread_->GlobalConstants()->GetHandledTypeErrorString());
            realm->SetTypeErrorFunction(thread_, native_error_function);
            realm->SetThrowTypeError(thread_, env->GetThrowTypeError());
            break;
        case JSType::JS_URI_ERROR:
            native_error_function = env->GetURIErrorFunction();
            name_string = JSHandle<JSTaggedValue>(thread_->GlobalConstants()->GetHandledURIErrorString());
            realm->SetURIErrorFunction(thread_, native_error_function);
            break;
        case JSType::JS_SYNTAX_ERROR:
            native_error_function = env->GetSyntaxErrorFunction();
            name_string = JSHandle<JSTaggedValue>(thread_->GlobalConstants()->GetHandledSyntaxErrorString());
            realm->SetSyntaxErrorFunction(thread_, native_error_function);
            break;
        default:
            break;
    }
    PropertyDescriptor descriptor(thread_, native_error_function, true, false, true);
    JSObject::DefineOwnProperty(thread_, global_object, name_string, descriptor);
}

void Builtins::GeneralUpdateError(ErrorParameter *error, EcmaEntrypoint constructor, EcmaEntrypoint method,
                                  const char *name, JSType type) const
{
    error->native_constructor = constructor;
    error->native_method = method;
    error->native_property_name = name;
    error->native_jstype = type;
}

// TODO(dkofanov): Split errors initialization, use `SetFunctionsGenRangeErrorProto` from `builtins_initializers_gen.h`
// and etc.
void Builtins::InitializeError(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass,
                               const JSType &error_tag) const
{
    // NativeError.prototype
    JSHandle<JSObject> native_error_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> native_error_func_prototype_value(native_error_func_prototype);

    ErrorParameter error_parameter {builtins::range_error::RangeErrorConstructor,
                                    builtins::range_error::proto::ToString, "RangeError", JSType::JS_RANGE_ERROR};
    switch (error_tag) {
        case JSType::JS_RANGE_ERROR:
            GeneralUpdateError(&error_parameter, builtins::range_error::RangeErrorConstructor,
                               builtins::range_error::proto::ToString, "RangeError", JSType::JS_RANGE_ERROR);
            break;
        case JSType::JS_EVAL_ERROR:
            GeneralUpdateError(&error_parameter, builtins::eval_error::EvalErrorConstructor,
                               builtins::eval_error::proto::ToString, "EvalError", JSType::JS_EVAL_ERROR);
            break;
        case JSType::JS_REFERENCE_ERROR:
            GeneralUpdateError(&error_parameter, builtins::reference_error::ReferenceErrorConstructor,
                               builtins::reference_error::proto::ToString, "ReferenceError",
                               JSType::JS_REFERENCE_ERROR);
            break;
        case JSType::JS_TYPE_ERROR:
            GeneralUpdateError(&error_parameter, builtins::type_error::TypeErrorConstructor,
                               builtins::type_error::proto::ToString, "TypeError", JSType::JS_TYPE_ERROR);
            break;
        case JSType::JS_URI_ERROR:
            GeneralUpdateError(&error_parameter, builtins::uri_error::URIErrorConstructor,
                               builtins::uri_error::proto::ToString, "URIError", JSType::JS_URI_ERROR);
            break;
        case JSType::JS_SYNTAX_ERROR:
            GeneralUpdateError(&error_parameter, builtins::syntax_error::SyntaxErrorConstructor,
                               builtins::syntax_error::proto::ToString, "SyntaxError", JSType::JS_SYNTAX_ERROR);
            break;
        default:
            break;
    }

    // NativeError.prototype_or_dynclass
    JSHandle<JSHClass> native_error_func_instance_dynclass =
        factory_->CreateDynClass<JSObject>(error_parameter.native_jstype, native_error_func_prototype_value);

    // NativeError() = new Error()
    JSHandle<JSFunction> native_error_function =
        factory_->NewJSNativeErrorFunction(env, reinterpret_cast<void *>(error_parameter.native_constructor));
    InitializeCtor(env, native_error_func_prototype, native_error_function, error_parameter.native_property_name,
                   FunctionLength::ONE);

    native_error_function->SetFunctionPrototype(thread_, native_error_func_instance_dynclass.GetTaggedValue());

    // NativeError.prototype method
    SetFunction(env, native_error_func_prototype, thread_->GlobalConstants()->GetHandledToStringString(),
                error_parameter.native_method, FunctionLength::ZERO);

    // Error.prototype Attribute
    SetAttribute(native_error_func_prototype, "name", error_parameter.native_property_name);
    SetAttribute(native_error_func_prototype, "message", "");

    if (error_tag == JSType::JS_RANGE_ERROR) {
        env->SetRangeErrorFunction(thread_, native_error_function);
    } else if (error_tag == JSType::JS_REFERENCE_ERROR) {
        env->SetReferenceErrorFunction(thread_, native_error_function);
    } else if (error_tag == JSType::JS_TYPE_ERROR) {
        env->SetTypeErrorFunction(thread_, native_error_function);
        JSHandle<JSFunction> throw_type_error_function =
            factory_->NewJSFunction(env, reinterpret_cast<void *>(type_error::ThrowTypeError));
        JSFunction::SetFunctionLength(thread_, throw_type_error_function, JSTaggedValue(1), false);
        JSObject::PreventExtensions(thread_, JSHandle<JSObject>::Cast(throw_type_error_function));
        env->SetThrowTypeError(thread_, throw_type_error_function);
    } else if (error_tag == JSType::JS_URI_ERROR) {
        env->SetURIErrorFunction(thread_, native_error_function);
    } else if (error_tag == JSType::JS_SYNTAX_ERROR) {
        env->SetSyntaxErrorFunction(thread_, native_error_function);
    } else {
        env->SetEvalErrorFunction(thread_, native_error_function);
    }
}  // namespace panda::ecmascript

void Builtins::InitializeCtor(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &prototype,
                              const JSHandle<JSFunction> &ctor, const char *name, int length) const
{
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSFunction::SetFunctionLength(thread_, ctor, JSTaggedValue(length));
    JSHandle<JSTaggedValue> name_string(factory_->NewFromString(name));
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(ctor), name_string,
                                JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Undefined()));
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    PropertyDescriptor descriptor1(thread_, JSHandle<JSTaggedValue>::Cast(ctor), true, false, true);
    JSObject::DefineOwnProperty(thread_, prototype, constructor_key, descriptor1);

    /* set "prototype" in constructor */
    ctor->SetFunctionPrototype(thread_, prototype.GetTaggedValue());

    if (!JSTaggedValue::SameValue(name_string, thread_->GlobalConstants()->GetHandledAsyncFunctionString())) {
        JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
        PropertyDescriptor descriptor2(thread_, JSHandle<JSTaggedValue>::Cast(ctor), true, false, true);
        JSObject::DefineOwnProperty(thread_, global_object, name_string, descriptor2);
    }
}

void Builtins::InitializeSet(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // Set.prototype
    JSHandle<JSObject> set_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> set_func_prototype_value(set_func_prototype);
    // Set.prototype_or_dynclass
    JSHandle<JSHClass> set_func_instance_dynclass =
        factory_->CreateDynClass<JSSet>(JSType::JS_SET, set_func_prototype_value);
    // Set() = new Function()
    JSHandle<JSTaggedValue> set_function(
        NewBuiltinConstructor(env, set_func_prototype, builtins::set::SetConstructor, "Set", FunctionLength::ZERO));
    JSHandle<JSFunction>(set_function)->SetFunctionPrototype(thread_, set_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(set_func_prototype), constructor_key, set_function);

    env->SetSetFunction(thread_, set_function);
    env->SetSetPrototype(thread_, set_func_prototype);

    // @@ToStringTag
    SetStringTagSymbol(env, set_func_prototype, "Set");

    SetFunctionsGenSetProto(env, set_func_prototype);

    SetFunctionsGenSet(env, JSHandle<JSObject>(set_function));
}

void Builtins::InitializeMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // Map.prototype
    JSHandle<JSObject> map_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> map_func_prototype_value(map_func_prototype);

    // Map.prototype_or_dynclass
    JSHandle<JSHClass> map_func_instance_dynclass =
        factory_->CreateDynClass<JSMap>(JSType::JS_MAP, map_func_prototype_value);
    // Map() = new Function()
    JSHandle<JSTaggedValue> map_function(
        NewBuiltinConstructor(env, map_func_prototype, map::MapConstructor, "Map", FunctionLength::ZERO));
    // Map().prototype = Map.Prototype & Map.prototype.constructor = Map()
    JSFunction::Cast(map_function->GetTaggedObject())
        ->SetFunctionPrototype(thread_, map_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(map_func_prototype), constructor_key, map_function);

    env->SetMapFunction(thread_, map_function);
    env->SetMapPrototype(thread_, map_func_prototype);
    SetFunctionsGenMapProto(env, map_func_prototype);
    SetFunctionsGenMap(env, JSHandle<JSObject>(map_function));

    // @@ToStringTag
    SetStringTagSymbol(env, map_func_prototype, "Map");
}

void Builtins::InitializeWeakRef(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // WeakRef.prototype
    JSHandle<JSObject> weak_ref_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> weak_ref_func_prototype_value(weak_ref_func_prototype);
    // WeakRef.prototype_or_dynclass
    JSHandle<JSHClass> weak_ref_func_instance_dynclass =
        factory_->CreateDynClass<JSWeakRef>(JSType::JS_WEAK_REF, weak_ref_func_prototype_value);
    weak_ref_func_instance_dynclass->SetWeakContainer(true);
    // WeakRef() = new Function()
    JSHandle<JSTaggedValue> weak_ref_function(
        NewBuiltinConstructor(env, weak_ref_func_prototype, weak_ref::Constructor, "WeakRef", FunctionLength::ONE));
    // WeakRef().prototype = WeakRef.Prototype & WeakRef.prototype.constructor = WeakRef()
    JSFunction::Cast(weak_ref_function->GetTaggedObject())
        ->SetProtoOrDynClass(thread_, weak_ref_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(weak_ref_func_prototype), constructor_key,
                          weak_ref_function);
    SetFunctionsGenWeakRefProto(env, weak_ref_func_prototype);
    // @@ToStringTag
    SetStringTagSymbol(env, weak_ref_func_prototype, "WeakRef");
}

void Builtins::InitializeWeakMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // WeakMap.prototype
    JSHandle<JSObject> weak_map_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> weak_map_func_prototype_value(weak_map_func_prototype);
    // WeakMap.prototype_or_dynclass
    JSHandle<JSHClass> weak_map_func_instance_dynclass =
        factory_->CreateDynClass<JSWeakMap>(JSType::JS_WEAK_MAP, weak_map_func_prototype_value);
    // WeakMap() = new Function()
    JSHandle<JSTaggedValue> weak_map_function(NewBuiltinConstructor(
        env, weak_map_func_prototype, weak_map::WeakMapConstructor, "WeakMap", FunctionLength::ZERO));
    // WeakMap().prototype = WeakMap.Prototype & WeakMap.prototype.constructor = WeakMap()
    JSFunction::Cast(weak_map_function->GetTaggedObject())
        ->SetProtoOrDynClass(thread_, weak_map_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(weak_map_func_prototype), constructor_key,
                          weak_map_function);
    env->SetWeakMapFunction(thread_, weak_map_function);
    SetFunctionsGenWeakMapProto(env, weak_map_func_prototype);
    // @@ToStringTag
    SetStringTagSymbol(env, weak_map_func_prototype, "WeakMap");
}

void Builtins::InitializeWeakSet(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // Set.prototype
    JSHandle<JSObject> weak_set_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> weak_set_func_prototype_value(weak_set_func_prototype);
    // Set.prototype_or_dynclass
    JSHandle<JSHClass> weak_set_func_instance_dynclass =
        factory_->CreateDynClass<JSWeakSet>(JSType::JS_WEAK_SET, weak_set_func_prototype_value);
    // Set() = new Function()
    JSHandle<JSTaggedValue> weak_set_function(NewBuiltinConstructor(
        env, weak_set_func_prototype, weak_set::WeakSetConstructor, "WeakSet", FunctionLength::ZERO));
    JSHandle<JSFunction>(weak_set_function)
        ->SetProtoOrDynClass(thread_, weak_set_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(weak_set_func_prototype), constructor_key,
                          weak_set_function);

    env->SetWeakSetFunction(thread_, weak_set_function);
    SetFunctionsGenWeakSetProto(env, weak_set_func_prototype);
    // @@ToStringTag
    SetStringTagSymbol(env, weak_set_func_prototype, "WeakSet");
}

void Builtins::InitializeMath(const JSHandle<GlobalEnv> &env,
                              const JSHandle<JSTaggedValue> &obj_func_prototype_val) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<JSHClass> math_dynclass = factory_->CreateDynClass<JSObject>(JSType::JS_OBJECT, obj_func_prototype_val);
    JSHandle<JSObject> math_object = factory_->NewJSObject(math_dynclass);

    env->SetMathFunction(thread_, math_object);
    SetFunctionsGenMath(env, math_object);

    SetConstant(math_object, "E", math::GetPropE());
    SetConstant(math_object, "LN10", math::GetPropLN10());
    SetConstant(math_object, "LN2", math::GetPropLN2());
    SetConstant(math_object, "LOG10E", math::GetPropLOG10E());
    SetConstant(math_object, "LOG2E", math::GetPropLOG2E());
    SetConstant(math_object, "PI", math::GetPropPI());
    SetConstant(math_object, "SQRT1_2", math::GetPropSQRT1_2());
    SetConstant(math_object, "SQRT2", math::GetPropSQRT2());

    JSHandle<JSTaggedValue> math_string(factory_->NewFromCanBeCompressString("Math"));
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    PropertyDescriptor math_desc(thread_, JSHandle<JSTaggedValue>::Cast(math_object), true, false, true);
    JSObject::DefineOwnProperty(thread_, global_object, math_string, math_desc);
    // @@ToStringTag
    SetStringTagSymbol(env, math_object, "Math");
}

void Builtins::InitializeJson(const JSHandle<GlobalEnv> &env,
                              const JSHandle<JSTaggedValue> &obj_func_prototype_val) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<JSHClass> json_dynclass = factory_->CreateDynClass<JSObject>(JSType::JS_OBJECT, obj_func_prototype_val);
    JSHandle<JSObject> json_object = factory_->NewJSObject(json_dynclass);

    env->SetJsonFunction(thread_, json_object);
    SetFunctionsGenJson(env, json_object);

    PropertyDescriptor json_desc(thread_, JSHandle<JSTaggedValue>::Cast(json_object), true, false, true);
    JSHandle<JSTaggedValue> json_string(factory_->NewFromCanBeCompressString("JSON"));
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    JSObject::DefineOwnProperty(thread_, global_object, json_string, json_desc);
    // @@ToStringTag
    SetStringTagSymbol(env, json_object, "JSON");
}

void Builtins::InitializeString(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &prim_ref_obj_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // String.prototype
    JSHandle<JSTaggedValue> to_object(factory_->GetEmptyString());
    JSHandle<JSObject> string_func_prototype =
        JSHandle<JSObject>::Cast(factory_->NewJSPrimitiveRef(prim_ref_obj_dynclass, to_object));
    JSHandle<JSTaggedValue> string_func_prototype_value(string_func_prototype);

    // String.prototype_or_dynclass
    JSHandle<JSHClass> string_func_instance_dynclass =
        factory_->CreateDynClass<JSPrimitiveRef>(JSType::JS_PRIMITIVE_REF, string_func_prototype_value);

    // String = new Function()
    JSHandle<JSObject> string_function(
        NewBuiltinConstructor(env, string_func_prototype, string::StringConstructor, "String", FunctionLength::ONE));
    string_function.GetObject<JSFunction>()->SetFunctionPrototype(thread_,
                                                                  string_func_instance_dynclass.GetTaggedValue());

    env->SetStringFunction(thread_, string_function);
    env->SetStringPrototype(thread_, string_func_prototype);

    SetFunctionsGenString(env, string_function);
    SetFunctionsGenStringProto(env, string_func_prototype);
}

void Builtins::InitializeStringIterator(const JSHandle<GlobalEnv> &env,
                                        const JSHandle<JSHClass> &iterator_func_dynclass) const
{
    // StringIterator.prototype
    JSHandle<JSObject> str_iter_prototype(factory_->NewJSObject(iterator_func_dynclass));

    // StringIterator.prototype_or_dynclass
    JSHandle<JSHClass> str_iter_func_instance_dynclass = factory_->CreateDynClass<JSStringIterator>(
        JSType::JS_STRING_ITERATOR, JSHandle<JSTaggedValue>(str_iter_prototype));

    JSHandle<JSFunction> str_iter_function(
        factory_->NewJSFunction(env, static_cast<void *>(nullptr), FunctionKind::BASE_CONSTRUCTOR));
    str_iter_function->SetFunctionPrototype(thread_, str_iter_func_instance_dynclass.GetTaggedValue());

    env->SetStringIteratorFunction(thread_, str_iter_function);
    env->SetStringIteratorPrototype(thread_, str_iter_prototype);
    SetFunctionsGenStringIteratorProto(env, str_iter_prototype);
    SetStringTagSymbol(env, str_iter_prototype, "String Iterator");
}

void Builtins::InitializeIterator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Iterator.prototype
    JSHandle<JSObject> iterator_prototype = factory_->NewJSObject(obj_func_dynclass);

    env->SetIteratorPrototype(thread_, iterator_prototype);
    SetFunctionsGenIteratorProto(env, iterator_prototype);

    // Iterator.dynclass
    JSHandle<JSHClass> iterator_func_dynclass =
        factory_->CreateDynClass<JSObject>(JSType::JS_ITERATOR, JSHandle<JSTaggedValue>(iterator_prototype));

    InitializeForinIterator(env, iterator_func_dynclass);
    InitializeSetIterator(env, iterator_func_dynclass);
    InitializeMapIterator(env, iterator_func_dynclass);
    InitializeArrayIterator(env, iterator_func_dynclass);
    InitializeStringIterator(env, iterator_func_dynclass);
    InitializeRegexpIterator(env, iterator_func_dynclass);
}

void Builtins::InitializeAsyncIterator(const JSHandle<GlobalEnv> &env,
                                       const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // AsyncIterator.prototype
    JSHandle<JSObject> async_iterator_prototype = factory_->NewJSObject(obj_func_dynclass);

    SetFunctionsGenAsyncIteratorProto(env, async_iterator_prototype);

    env->SetAsyncIteratorPrototype(thread_, async_iterator_prototype);
}

void Builtins::InitializeForinIterator(const JSHandle<GlobalEnv> &env,
                                       const JSHandle<JSHClass> &iterator_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Iterator.prototype
    JSHandle<JSObject> forin_iterator_prototype = factory_->NewJSObject(iterator_func_dynclass);
    JSHandle<JSHClass> dynclass = factory_->CreateDynClass<JSForInIterator>(
        JSType::JS_FORIN_ITERATOR, JSHandle<JSTaggedValue>(forin_iterator_prototype));

    env->SetForinIteratorPrototype(thread_, forin_iterator_prototype);
    env->SetForinIteratorClass(thread_, dynclass);
    SetFunctionsGenForInIteratorProto(env, forin_iterator_prototype);
}

void Builtins::InitializeSetIterator(const JSHandle<GlobalEnv> &env,
                                     const JSHandle<JSHClass> &iterator_func_dynclass) const
{
    // SetIterator.prototype
    JSHandle<JSObject> set_iterator_prototype(factory_->NewJSObject(iterator_func_dynclass));

    SetFunctionsGenSetIteratorProto(env, set_iterator_prototype);
    SetStringTagSymbol(env, set_iterator_prototype, "Set Iterator");
    env->SetSetIteratorPrototype(thread_, set_iterator_prototype);
}

void Builtins::InitializeMapIterator(const JSHandle<GlobalEnv> &env,
                                     const JSHandle<JSHClass> &iterator_func_dynclass) const
{
    // MapIterator.prototype
    JSHandle<JSObject> map_iterator_prototype(factory_->NewJSObject(iterator_func_dynclass));
    SetFunctionsGenMapIteratorProto(env, map_iterator_prototype);

    SetStringTagSymbol(env, map_iterator_prototype, "Map Iterator");
    env->SetMapIteratorPrototype(thread_, map_iterator_prototype);
}

void Builtins::InitializeArrayIterator(const JSHandle<GlobalEnv> &env,
                                       const JSHandle<JSHClass> &iterator_func_dynclass) const
{
    // ArrayIterator.prototype
    JSHandle<JSObject> array_iterator_prototype(factory_->NewJSObject(iterator_func_dynclass));

    SetFunctionsGenArrayIteratorProto(env, array_iterator_prototype);
    SetStringTagSymbol(env, array_iterator_prototype, "Array Iterator");
    env->SetArrayIteratorPrototype(thread_, array_iterator_prototype);
}

void Builtins::InitializeRegexpIterator(const JSHandle<GlobalEnv> &env,
                                        const JSHandle<JSHClass> &iterator_func_class) const
{
    // RegExpIterator.prototype
    JSHandle<JSObject> reg_exp_iterator_prototype(factory_->NewJSObject(iterator_func_class));

    SetFunctionsGenRegExpIteratorProto(env, reg_exp_iterator_prototype);
    SetStringTagSymbol(env, reg_exp_iterator_prototype, "RegExp String Iterator");
    env->SetRegExpIteratorPrototype(thread_, reg_exp_iterator_prototype);
}

void Builtins::InitializeRegExp(const JSHandle<GlobalEnv> &env)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // RegExp.prototype
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> reg_prototype = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> reg_prototype_value(reg_prototype);

    // RegExp.prototype_or_dynclass
    JSHandle<JSHClass> regexp_func_instance_dynclass = factory_->CreateJSRegExpInstanceClass(reg_prototype_value);

    // RegExp = new Function()
    JSHandle<JSObject> regexp_function(
        NewBuiltinConstructor(env, reg_prototype, reg_exp::RegExpConstructor, "RegExp", FunctionLength::TWO));

    JSHandle<JSFunction>(regexp_function)
        ->SetFunctionPrototype(thread_, regexp_func_instance_dynclass.GetTaggedValue());

    SetFunctionsGenRegExpProto(env, reg_prototype);
    SetFunctionsGenRegExp(env, regexp_function);

    env->SetRegExpFunction(thread_, regexp_function);
    auto global_const = const_cast<GlobalEnvConstants *>(thread_->GlobalConstants());
    global_const->SetConstant(ConstantIndex::JS_REGEXP_CLASS_INDEX, regexp_func_instance_dynclass.GetTaggedValue());
}

void Builtins::InitializeArray(const JSHandle<GlobalEnv> &env,
                               const JSHandle<JSTaggedValue> &obj_func_prototype_val) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Arraybase.prototype
    JSHandle<JSHClass> arr_base_func_instance_dynclass = factory_->CreateJSArrayInstanceClass(obj_func_prototype_val);

    // Array.prototype
    JSHandle<JSObject> arr_func_prototype = factory_->NewJSObject(arr_base_func_instance_dynclass);
    JSHandle<JSArray>::Cast(arr_func_prototype)->SetLength(thread_, JSTaggedValue(FunctionLength::ZERO));
    auto accessor = thread_->GlobalConstants()->GetArrayLengthAccessor();
    JSArray::Cast(*arr_func_prototype)
        ->SetPropertyInlinedProps(thread_, JSArray::LENGTH_INLINE_PROPERTY_INDEX, accessor);
    JSHandle<JSTaggedValue> arr_func_prototype_value(arr_func_prototype);

    //  Array.prototype_or_dynclass
    JSHandle<JSHClass> arr_func_instance_dynclass = factory_->CreateJSArrayInstanceClass(arr_func_prototype_value);

    // Array = new Function()
    JSHandle<JSObject> array_function(NewBuiltinConstructor(env, arr_func_prototype, builtins::array::ArrayConstructor,
                                                            "Array", FunctionLength::ONE));
    JSHandle<JSFunction> array_func_function(array_function);

    // Set the [[Realm]] internal slot of F to the running execution context's Realm
    JSHandle<LexicalEnv> lexical_env = factory_->NewLexicalEnv(0);
    lexical_env->SetParentEnv(thread_, env.GetTaggedValue());
    array_func_function->SetLexicalEnv(thread_, lexical_env.GetTaggedValue());

    array_func_function->SetFunctionPrototype(thread_, arr_func_instance_dynclass.GetTaggedValue());

    env->SetArrayFunction(thread_, array_function);
    env->SetArrayPrototype(thread_, arr_func_prototype);

    SetFunctionsGenArrayProto(env, arr_func_prototype);

    SetFunctionsGenArray(env, array_function);

    const int arr_proto_len = 0;
    JSHandle<JSTaggedValue> key_string = thread_->GlobalConstants()->GetHandledLengthString();
    PropertyDescriptor descriptor(thread_, JSHandle<JSTaggedValue>(thread_, JSTaggedValue(arr_proto_len)), true, false,
                                  false);
    JSObject::DefineOwnProperty(thread_, arr_func_prototype, key_string, descriptor);

    JSHandle<JSTaggedValue> values_key(factory_->NewFromCanBeCompressString("values"));
    PropertyDescriptor desc(thread_);
    JSObject::GetOwnProperty(thread_, arr_func_prototype, values_key, desc);

    env->SetArrayProtoValuesFunction(thread_, desc.GetValue());
}

void Builtins::InitializeTypedArray(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // TypedArray.prototype
    JSHandle<JSObject> typed_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> typed_arr_func_prototype_value(typed_arr_func_prototype);

    // TypedArray.prototype_or_dynclass
    JSHandle<JSHClass> typed_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_TYPED_ARRAY, typed_arr_func_prototype_value);

    // TypedArray = new Function()
    JSHandle<JSObject> typed_array_function(NewBuiltinConstructor(
        env, typed_arr_func_prototype, typed_array::TypedArrayBaseConstructor, "TypedArray", FunctionLength::ZERO));

    JSHandle<JSFunction>(typed_array_function)
        ->SetProtoOrDynClass(thread_, typed_arr_func_instance_dynclass.GetTaggedValue());

    env->SetTypedArrayFunction(thread_, typed_array_function.GetTaggedValue());
    env->SetTypedArrayPrototype(thread_, typed_arr_func_prototype);

    JSHandle<JSHClass> specific_typed_array_func_class = factory_->CreateDynClass<JSFunction>(
        JSType::JS_FUNCTION, env->GetTypedArrayFunction(), HClass::IS_CALLABLE | HClass::IS_BUILTINS_CTOR);
    specific_typed_array_func_class->SetConstructor(true);
    env->SetSpecificTypedArrayFunctionClass(thread_, specific_typed_array_func_class);

    SetFunctionsGenTypedArrayProto(env, typed_arr_func_prototype);

    SetFunctionsGenTypedArray(env, typed_array_function);

    InitializeInt8Array(env, typed_arr_func_instance_dynclass);
    InitializeUint8Array(env, typed_arr_func_instance_dynclass);
    InitializeUint8ClampedArray(env, typed_arr_func_instance_dynclass);
    InitializeInt16Array(env, typed_arr_func_instance_dynclass);
    InitializeUint16Array(env, typed_arr_func_instance_dynclass);
    InitializeInt32Array(env, typed_arr_func_instance_dynclass);
    InitializeUint32Array(env, typed_arr_func_instance_dynclass);
    InitializeFloat32Array(env, typed_arr_func_instance_dynclass);
    InitializeFloat64Array(env, typed_arr_func_instance_dynclass);
    InitializeBigInt64Array(env, typed_arr_func_instance_dynclass);
    InitializeBigUint64Array(env, typed_arr_func_instance_dynclass);
}

void Builtins::InitializeInt8Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Int8Array.prototype
    JSHandle<JSObject> int8_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> int8_arr_func_prototype_value(int8_arr_func_prototype);

    // Int8Array.prototype_or_dynclass
    JSHandle<JSHClass> int8_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_INT8_ARRAY, int8_arr_func_prototype_value);

    // Int8Array = new Function()
    JSHandle<JSFunction> int8_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(int8_array::Int8ArrayConstructor));
    InitializeCtor(env, int8_arr_func_prototype, int8_array_function, "Int8Array", FunctionLength::THREE);

    int8_array_function->SetProtoOrDynClass(thread_, int8_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 1;
    SetConstant(int8_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(int8_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetInt8ArrayFunction(thread_, int8_array_function);
}

void Builtins::InitializeUint8Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Uint8Array.prototype
    JSHandle<JSObject> uint8_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> uint8_arr_func_prototype_value(uint8_arr_func_prototype);

    // Uint8Array.prototype_or_dynclass
    JSHandle<JSHClass> uint8_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_UINT8_ARRAY, uint8_arr_func_prototype_value);

    // Uint8Array = new Function()
    JSHandle<JSFunction> uint8_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(uint8_array::Uint8ArrayConstructor));
    InitializeCtor(env, uint8_arr_func_prototype, uint8_array_function, "Uint8Array", FunctionLength::THREE);

    uint8_array_function->SetProtoOrDynClass(thread_, uint8_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 1;
    SetConstant(uint8_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(uint8_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetUint8ArrayFunction(thread_, uint8_array_function);
}

void Builtins::InitializeUint8ClampedArray(const JSHandle<GlobalEnv> &env,
                                           const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Uint8ClampedArray.prototype
    JSHandle<JSObject> uint8_clamped_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> uint8_clamped_arr_func_prototype_value(uint8_clamped_arr_func_prototype);

    // Uint8ClampedArray.prototype_or_dynclass
    JSHandle<JSHClass> uint8_clamped_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_UINT8_CLAMPED_ARRAY, uint8_clamped_arr_func_prototype_value);

    // Uint8ClampedArray = new Function()
    JSHandle<JSFunction> uint8_clamped_array_function = factory_->NewSpecificTypedArrayFunction(
        env, reinterpret_cast<void *>(uint8_clamped_array::Uint8ClampedArrayConstructor));
    InitializeCtor(env, uint8_clamped_arr_func_prototype, uint8_clamped_array_function, "Uint8ClampedArray",
                   FunctionLength::THREE);

    uint8_clamped_array_function->SetProtoOrDynClass(thread_,
                                                     uint8_clamped_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 1;
    SetConstant(uint8_clamped_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(uint8_clamped_array_function), "BYTES_PER_ELEMENT",
                JSTaggedValue(bytes_per_element));
    env->SetUint8ClampedArrayFunction(thread_, uint8_clamped_array_function);
}

void Builtins::InitializeInt16Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Int16Array.prototype
    JSHandle<JSObject> int16_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> int16_arr_func_prototype_value(int16_arr_func_prototype);

    // Int16Array.prototype_or_dynclass
    JSHandle<JSHClass> int16_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_INT16_ARRAY, int16_arr_func_prototype_value);

    // Int16Array = new Function()
    JSHandle<JSFunction> int16_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(int16_array::Int16ArrayConstructor));
    InitializeCtor(env, int16_arr_func_prototype, int16_array_function, "Int16Array", FunctionLength::THREE);

    int16_array_function->SetProtoOrDynClass(thread_, int16_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 2;
    SetConstant(int16_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(int16_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetInt16ArrayFunction(thread_, int16_array_function);
}

void Builtins::InitializeUint16Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Uint16Array.prototype
    JSHandle<JSObject> uint16_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> uint16_arr_func_prototype_value(uint16_arr_func_prototype);

    // Uint16Array.prototype_or_dynclass
    JSHandle<JSHClass> uint16_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_UINT16_ARRAY, uint16_arr_func_prototype_value);

    // Uint16Array = new Function()
    JSHandle<JSFunction> uint16_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(uint16_array::Uint16ArrayConstructor));
    InitializeCtor(env, uint16_arr_func_prototype, uint16_array_function, "Uint16Array", FunctionLength::THREE);

    uint16_array_function->SetProtoOrDynClass(thread_, uint16_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 2;
    SetConstant(uint16_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(uint16_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetUint16ArrayFunction(thread_, uint16_array_function);
}

void Builtins::InitializeInt32Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Int32Array.prototype
    JSHandle<JSObject> int32_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> int32_arr_func_prototype_value(int32_arr_func_prototype);

    // Int32Array.prototype_or_dynclass
    JSHandle<JSHClass> int32_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_INT32_ARRAY, int32_arr_func_prototype_value);

    // Int32Array = new Function()
    JSHandle<JSFunction> int32_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(int32_array::Int32ArrayConstructor));
    InitializeCtor(env, int32_arr_func_prototype, int32_array_function, "Int32Array", FunctionLength::THREE);

    int32_array_function->SetProtoOrDynClass(thread_, int32_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 4;
    SetConstant(int32_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(int32_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetInt32ArrayFunction(thread_, int32_array_function);
}

void Builtins::InitializeUint32Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Uint32Array.prototype
    JSHandle<JSObject> uint32_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> uint32_arr_func_prototype_value(uint32_arr_func_prototype);

    // Uint32Array.prototype_or_dynclass
    JSHandle<JSHClass> uint32_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_UINT32_ARRAY, uint32_arr_func_prototype_value);

    // Uint32Array = new Function()
    JSHandle<JSFunction> uint32_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(uint32_array::Uint32ArrayConstructor));
    InitializeCtor(env, uint32_arr_func_prototype, uint32_array_function, "Uint32Array", FunctionLength::THREE);

    uint32_array_function->SetProtoOrDynClass(thread_, uint32_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 4;
    SetConstant(uint32_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(uint32_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetUint32ArrayFunction(thread_, uint32_array_function);
}

void Builtins::InitializeFloat32Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Float32Array.prototype
    JSHandle<JSObject> float32_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> float32_arr_func_prototype_value(float32_arr_func_prototype);

    // Float32Array.prototype_or_dynclass
    JSHandle<JSHClass> float32_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_FLOAT32_ARRAY, float32_arr_func_prototype_value);

    // Float32Array = new Function()
    JSHandle<JSFunction> float32_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(float32_array::Float32ArrayConstructor));
    InitializeCtor(env, float32_arr_func_prototype, float32_array_function, "Float32Array", FunctionLength::THREE);

    float32_array_function->SetProtoOrDynClass(thread_, float32_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 4;
    SetConstant(float32_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(float32_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetFloat32ArrayFunction(thread_, float32_array_function);
}

void Builtins::InitializeFloat64Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Float64Array.prototype
    JSHandle<JSObject> float64_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> float64_arr_func_prototype_value(float64_arr_func_prototype);

    // Float64Array.prototype_or_dynclass
    JSHandle<JSHClass> float64_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_FLOAT64_ARRAY, float64_arr_func_prototype_value);

    // Float64Array = new Function()
    JSHandle<JSFunction> float64_array_function =
        factory_->NewSpecificTypedArrayFunction(env, reinterpret_cast<void *>(float64_array::Float64ArrayConstructor));
    InitializeCtor(env, float64_arr_func_prototype, float64_array_function, "Float64Array", FunctionLength::THREE);

    float64_array_function->SetProtoOrDynClass(thread_, float64_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 8;
    SetConstant(float64_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(float64_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetFloat64ArrayFunction(thread_, float64_array_function);
}

void Builtins::InitializeBigInt64Array(const JSHandle<GlobalEnv> &env,
                                       const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // BigInt64Array.prototype
    JSHandle<JSObject> big_int64_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> big_int64_arr_func_prototype_value(big_int64_arr_func_prototype);

    // BigInt64Array.prototype_or_dynclass
    JSHandle<JSHClass> big_int64_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_BIGINT64_ARRAY, big_int64_arr_func_prototype_value);

    // BigInt64Array = new Function()
    JSHandle<JSFunction> big_int64_array_function = factory_->NewSpecificTypedArrayFunction(
        env, reinterpret_cast<void *>(big_int64_array::BigInt64ArrayConstructor));
    InitializeCtor(env, big_int64_arr_func_prototype, big_int64_array_function, "BigInt64Array", FunctionLength::THREE);

    big_int64_array_function->SetProtoOrDynClass(thread_, big_int64_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 8;
    SetConstant(big_int64_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(big_int64_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetBigInt64ArrayFunction(thread_, big_int64_array_function);
}

void Builtins::InitializeBigUint64Array(const JSHandle<GlobalEnv> &env,
                                        const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // BigUint64Array.prototype
    JSHandle<JSObject> big_uint64_arr_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> big_uint64_arr_func_prototype_value(big_uint64_arr_func_prototype);

    // BigUint64Array.prototype_or_dynclass
    JSHandle<JSHClass> big_uint64_arr_func_instance_dynclass =
        factory_->CreateDynClass<JSTypedArray>(JSType::JS_BIGUINT64_ARRAY, big_uint64_arr_func_prototype_value);

    // BigUint64Array = new Function()
    JSHandle<JSFunction> big_uint64_array_function = factory_->NewSpecificTypedArrayFunction(
        env, reinterpret_cast<void *>(big_uint64_array::BigUint64ArrayConstructor));
    InitializeCtor(env, big_uint64_arr_func_prototype, big_uint64_array_function, "BigUint64Array",
                   FunctionLength::THREE);

    big_uint64_array_function->SetProtoOrDynClass(thread_, big_uint64_arr_func_instance_dynclass.GetTaggedValue());

    const int bytes_per_element = 8;
    SetConstant(big_uint64_arr_func_prototype, "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    SetConstant(JSHandle<JSObject>(big_uint64_array_function), "BYTES_PER_ELEMENT", JSTaggedValue(bytes_per_element));
    env->SetBigUint64ArrayFunction(thread_, big_uint64_array_function);
}

void Builtins::InitializeArrayBuffer(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // ArrayBuffer.prototype
    JSHandle<JSObject> array_buffer_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> array_buffer_func_prototype_value(array_buffer_func_prototype);

    //  ArrayBuffer.prototype_or_dynclass
    JSHandle<JSHClass> array_buffer_func_instance_dynclass =
        factory_->CreateDynClass<JSArrayBuffer>(JSType::JS_ARRAY_BUFFER, array_buffer_func_prototype_value);

    // ArrayBuffer = new Function()
    JSHandle<JSObject> array_buffer_function(NewBuiltinConstructor(env, array_buffer_func_prototype,
                                                                   builtins::array_buffer::ArrayBufferConstructor,
                                                                   "ArrayBuffer", FunctionLength::ONE));

    JSHandle<JSFunction>(array_buffer_function)
        ->SetFunctionPrototype(thread_, array_buffer_func_instance_dynclass.GetTaggedValue());

    SetFunctionsGenArrayBufferProto(env, array_buffer_func_prototype);
    SetFunctionsGenArrayBuffer(env, array_buffer_function);

    // 24.1.4.4 ArrayBuffer.prototype[@@toStringTag]
    SetStringTagSymbol(env, array_buffer_func_prototype, "ArrayBuffer");

    env->SetArrayBufferFunction(thread_, array_buffer_function.GetTaggedValue());
}

void Builtins::InitializeReflect(const JSHandle<GlobalEnv> &env,
                                 const JSHandle<JSTaggedValue> &obj_func_prototype_val) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<JSHClass> reflect_dynclass = factory_->CreateDynClass<JSObject>(JSType::JS_OBJECT, obj_func_prototype_val);
    JSHandle<JSObject> reflect_object = factory_->NewJSObject(reflect_dynclass);

    SetFunctionsGenReflect(env, reflect_object);

    JSHandle<JSTaggedValue> reflect_string(factory_->NewFromCanBeCompressString("Reflect"));
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    PropertyDescriptor reflect_desc(thread_, JSHandle<JSTaggedValue>::Cast(reflect_object), true, false, true);
    JSObject::DefineOwnProperty(thread_, global_object, reflect_string, reflect_desc);

    // @@ToStringTag
    SetStringTagSymbol(env, reflect_object, "Reflect");

    env->SetReflectFunction(thread_, reflect_object.GetTaggedValue());
}

void Builtins::InitializePromise(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &promise_func_dynclass)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Promise.prototype
    JSHandle<JSObject> promise_func_prototype = factory_->NewJSObject(promise_func_dynclass);
    JSHandle<JSTaggedValue> promise_func_prototype_value(promise_func_prototype);
    // Promise.prototype_or_dynclass
    JSHandle<JSHClass> promise_func_instance_dynclass =
        factory_->CreateDynClass<JSPromise>(JSType::JS_PROMISE, promise_func_prototype_value);
    // Promise() = new Function()
    JSHandle<JSObject> promise_function(NewBuiltinConstructor(env, promise_func_prototype, promise::PromiseConstructor,
                                                              "Promise", FunctionLength::ONE));
    JSHandle<JSFunction>(promise_function)
        ->SetFunctionPrototype(thread_, promise_func_instance_dynclass.GetTaggedValue());
    SetFunctionsGenPromise(env, promise_function);

    SetFunctionsGenPromiseProto(env, promise_func_prototype);

    // Promise.prototype [ @@toStringTag ]
    SetStringTagSymbol(env, promise_func_prototype, "Promise");

    env->SetPromiseFunction(thread_, promise_function);
}

void Builtins::InitializePromiseJob(const JSHandle<GlobalEnv> &env)
{
    JSHandle<JSTaggedValue> key_string(thread_->GlobalConstants()->GetHandledEmptyString());
    auto func = NewFunction(env, key_string, promise_job::PromiseReactionJob, FunctionLength::TWO);
    env->SetPromiseReactionJob(thread_, func);
    func = NewFunction(env, key_string, promise_job::PromiseResolveThenableJob, FunctionLength::THREE);
    env->SetPromiseResolveThenableJob(thread_, func);
}

void Builtins::InitializeFinalizationRegistry(const JSHandle<GlobalEnv> &env,
                                              const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    // FinalizationRegistry.prototype
    JSHandle<JSObject> registry_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> registry_func_prototype_value(registry_func_prototype);
    // FinalizationRegistry.prototype_or_dynclass
    JSHandle<JSHClass> registry_func_instance_dynclass =
        factory_->CreateDynClass<JSFinalizationRegistry>(JSType::FINALIZATION_REGISTRY, registry_func_prototype_value);
    // FinalizationRegistry() = new Function()
    JSHandle<JSTaggedValue> registry_function(NewBuiltinConstructor(
        env, registry_func_prototype, finalization_registry::Constructor, "FinalizationRegistry", FunctionLength::ONE));
    // FinalizationRegistry().prototype = FinalizationRegistry.Prototype &
    // FinalizationRegistry.prototype.constructor = Finalizationregistry()
    JSFunction::Cast(registry_function->GetTaggedObject())
        ->SetProtoOrDynClass(thread_, registry_func_instance_dynclass.GetTaggedValue());

    // "constructor" property on the prototype
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(registry_func_prototype), constructor_key,
                          registry_function);

    SetFunctionsGenFinalizationRegistryProto(env, registry_func_prototype);
    // @@ToStringTag
    SetStringTagSymbol(env, registry_func_prototype, "FinalizationRegistry");

    const_cast<GlobalEnvConstants *>(global_const)
        ->SetConstant(ConstantIndex::FINALIZATION_REGISTRY_CLASS_INDEX,
                      registry_func_instance_dynclass.GetTaggedValue());
}

void Builtins::InitializeDataView(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // ArrayBuffer.prototype
    JSHandle<JSObject> data_view_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> data_view_func_prototype_value(data_view_func_prototype);

    //  ArrayBuffer.prototype_or_dynclass
    JSHandle<JSHClass> data_view_func_instance_dynclass =
        factory_->CreateDynClass<JSDataView>(JSType::JS_DATA_VIEW, data_view_func_prototype_value);

    // ArrayBuffer = new Function()
    JSHandle<JSObject> data_view_function(NewBuiltinConstructor(
        env, data_view_func_prototype, data_view::DataViewConstructor, "DataView", FunctionLength::ONE));

    JSHandle<JSFunction>(data_view_function)
        ->SetProtoOrDynClass(thread_, data_view_func_instance_dynclass.GetTaggedValue());

    SetFunctionsGenDataViewProto(env, data_view_func_prototype);

    // 24.2.4.21 DataView.prototype[ @@toStringTag ]
    SetStringTagSymbol(env, data_view_func_prototype, "DataView");
    env->SetDataViewFunction(thread_, data_view_function.GetTaggedValue());
}

JSHandle<JSFunction> Builtins::NewBuiltinConstructor(const JSHandle<GlobalEnv> &env,
                                                     const JSHandle<JSObject> &prototype, EcmaEntrypoint ctor_func,
                                                     const char *name, int length) const
{
    JSHandle<JSFunction> ctor =
        factory_->NewJSFunction(env, reinterpret_cast<void *>(ctor_func), FunctionKind::BUILTIN_CONSTRUCTOR);
    InitializeCtor(env, prototype, ctor, name, length);
    return ctor;
}

JSHandle<JSFunction> Builtins::NewFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &key,
                                           EcmaEntrypoint func, int length) const
{
    JSHandle<JSFunction> function = factory_->NewJSFunction(env, reinterpret_cast<void *>(func));
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(length));
    JSHandle<JSFunctionBase> base_function(function);
    JSHandle<JSTaggedValue> handle_undefine(thread_, JSTaggedValue::Undefined());
    JSFunction::SetFunctionName(thread_, base_function, key, handle_undefine);
    return function;
}

void Builtins::SetFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj, const char *key,
                           EcmaEntrypoint func, int length) const
{
    JSHandle<JSTaggedValue> key_string(factory_->NewFromString(key));
    SetFunction(env, obj, key_string, func, length);
}

void Builtins::SetFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj,
                           const JSHandle<JSTaggedValue> &key, EcmaEntrypoint func, int length) const
{
    JSHandle<JSFunction> function(NewFunction(env, key, func, length));
    SetFunction(obj, key, JSHandle<JSTaggedValue>(function));
}

void Builtins::SetFunction(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                           const JSHandle<JSTaggedValue> &func) const
{
    PropertyDescriptor descriptor(thread_, func, true, false, true);
    JSObject::DefineOwnProperty(thread_, obj, key, descriptor);
}

void Builtins::SetFrozenFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj, const char *key,
                                 EcmaEntrypoint func, int length) const
{
    JSHandle<JSTaggedValue> key_string(factory_->NewFromString(key));
    JSHandle<JSFunction> function = NewFunction(env, key_string, func, length);
    PropertyDescriptor descriptor(thread_, JSHandle<JSTaggedValue>(function), false, false, false);
    JSObject::DefineOwnProperty(thread_, obj, key_string, descriptor);
}

template <JSSymbol::SymbolType FLAG>
void Builtins::SetFunctionAtSymbol(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj,
                                   const JSHandle<JSTaggedValue> &symbol, const char *name, EcmaEntrypoint func,
                                   int length) const
{
    JSHandle<JSFunction> function = factory_->NewJSFunction(env, reinterpret_cast<void *>(func));
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(length));
    JSHandle<JSTaggedValue> name_string(factory_->NewFromString(name));
    JSHandle<JSFunctionBase> base_function(function);
    JSHandle<JSTaggedValue> handle_undefine(thread_, JSTaggedValue::Undefined());
    JSFunction::SetFunctionName(thread_, base_function, name_string, handle_undefine);
    SetFunctionAtSymbol<FLAG>(obj, symbol, JSHandle<JSTaggedValue>::Cast(function));
}

template <JSSymbol::SymbolType FLAG>
void Builtins::SetFunctionAtSymbol(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &symbol,
                                   const JSHandle<JSTaggedValue> &function) const
{
    // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
    if constexpr (FLAG == JSSymbol::SymbolType::TO_PRIMITIVE) {
        PropertyDescriptor descriptor(thread_, function, false, false, true);
        JSObject::DefineOwnProperty(thread_, obj, symbol, descriptor);
        return;
    }
    if constexpr (FLAG == JSSymbol::SymbolType::HAS_INSTANCE) {  // NOLINTE(readability-braces-around-statements)
        // ecma 19.2.3.6 Function.prototype[@@hasInstance] has the attributes
        // { [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: false }.
        PropertyDescriptor descriptor(thread_, function, false, false, false);
        JSObject::DefineOwnProperty(thread_, obj, symbol, descriptor);
        return;
    }
    PropertyDescriptor descriptor(thread_, function, true, false, true);
    JSObject::DefineOwnProperty(thread_, obj, symbol, descriptor);
}

void Builtins::SetStringTagSymbol(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj, const char *key) const
{
    JSHandle<JSTaggedValue> tag(factory_->NewFromString(key));
    JSHandle<JSTaggedValue> symbol = env->GetToStringTagSymbol();
    PropertyDescriptor desc(thread_, tag, false, false, true);
    JSObject::DefineOwnProperty(thread_, obj, symbol, desc);
}

JSHandle<JSTaggedValue> Builtins::CreateGetter(const JSHandle<GlobalEnv> &env, EcmaEntrypoint func, const char *name,
                                               int length) const
{
    ASSERT(length == 0);
    JSHandle<JSFunction> function = factory_->NewJSFunction(env, reinterpret_cast<void *>(func));
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(length));
    JSHandle<JSTaggedValue> func_name(factory_->NewFromString(name));
    JSHandle<JSTaggedValue> prefix = thread_->GlobalConstants()->GetHandledGetString();
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(function), func_name, prefix);
    return JSHandle<JSTaggedValue>(function);
}

JSHandle<JSTaggedValue> Builtins::CreateSetter(const JSHandle<GlobalEnv> &env, EcmaEntrypoint func, const char *name,
                                               int length) const
{
    ASSERT(length == 1);
    JSHandle<JSFunction> function = factory_->NewJSFunction(env, reinterpret_cast<void *>(func));
    JSFunction::SetFunctionLength(thread_, function, JSTaggedValue(length));
    JSHandle<JSTaggedValue> func_name(factory_->NewFromString(name));
    JSHandle<JSTaggedValue> prefix = thread_->GlobalConstants()->GetHandledSetString();
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(function), func_name, prefix);
    return JSHandle<JSTaggedValue>(function);
}

void Builtins::SetConstant(const JSHandle<JSObject> &obj, const char *key, JSTaggedValue value) const
{
    JSHandle<JSTaggedValue> value_handle(thread_, value);
    JSHandle<JSTaggedValue> key_string(factory_->NewFromString(key));
    PropertyDescriptor descriptor(thread_, value_handle, false, false, false);
    JSObject::DefineOwnProperty(thread_, obj, key_string, descriptor);
}

void Builtins::SetConstantObject(const JSHandle<JSObject> &obj, const char *key, JSHandle<JSTaggedValue> &value) const
{
    JSHandle<JSTaggedValue> key_string(factory_->NewFromString(key));
    PropertyDescriptor descriptor(thread_, value, false, false, false);
    JSObject::DefineOwnProperty(thread_, obj, key_string, descriptor);
}

void Builtins::SetGlobalThis(const JSHandle<JSObject> &obj, const char *key,
                             const JSHandle<JSTaggedValue> &global_value)
{
    JSHandle<JSTaggedValue> key_string(factory_->NewFromString(key));
    PropertyDescriptor descriptor(thread_, global_value, true, false, true);
    JSObject::DefineOwnProperty(thread_, obj, key_string, descriptor);
}

void Builtins::SetAttribute(const JSHandle<JSObject> &obj, const char *key, const char *value) const
{
    JSHandle<JSTaggedValue> key_string(factory_->NewFromString(key));
    PropertyDescriptor descriptor(thread_, JSHandle<JSTaggedValue>(factory_->NewFromString(value)), true, false, true);
    JSObject::DefineOwnProperty(thread_, obj, key_string, descriptor);
}

void Builtins::SetNoneAttributeProperty(const JSHandle<JSObject> &obj, const char *key,
                                        const JSHandle<JSTaggedValue> &value) const
{
    JSHandle<JSTaggedValue> key_string(factory_->NewFromString(key));
    PropertyDescriptor des(thread_, value, false, false, false);
    JSObject::DefineOwnProperty(thread_, obj, key_string, des);
}

void Builtins::StrictModeForbiddenAccessCallerArguments(const JSHandle<GlobalEnv> &env,
                                                        const JSHandle<JSObject> &prototype) const
{
    JSHandle<JSFunction> function =
        factory_->NewJSFunction(env, reinterpret_cast<void *>(JSFunction::AccessCallerArgumentsThrowTypeError));

    JSHandle<JSTaggedValue> caller(factory_->NewFromCanBeCompressString("caller"));
    SetAccessor(prototype, caller, JSHandle<JSTaggedValue>::Cast(function), JSHandle<JSTaggedValue>::Cast(function));

    JSHandle<JSTaggedValue> arguments(factory_->NewFromCanBeCompressString("arguments"));
    SetAccessor(prototype, arguments, JSHandle<JSTaggedValue>::Cast(function), JSHandle<JSTaggedValue>::Cast(function));
}

void Builtins::InitializeGeneratorFunction(const JSHandle<GlobalEnv> &env,
                                           const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSObject> generator_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> generator_func_prototype_value(generator_func_prototype);

    // 26.3.3.1 GeneratorFunction.prototype.constructor
    // GeneratorFunction.prototype_or_dynclass
    JSHandle<JSHClass> generator_func_instance_dynclass = factory_->CreateDynClass<JSFunction>(
        JSType::JS_GENERATOR_FUNCTION, generator_func_prototype_value, HClass::IS_CALLABLE);
    generator_func_instance_dynclass->SetExtensible(true);
    // GeneratorFunction = new GeneratorFunction()
    JSHandle<JSFunction> generator_function =
        NewBuiltinConstructor(env, generator_func_prototype, builtins::generator_function::GeneratorFunctionConstructor,
                              "GeneratorFunction", FunctionLength::ONE);
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    PropertyDescriptor generator_desc(thread_, JSHandle<JSTaggedValue>::Cast(generator_function), false, false, true);
    JSObject::DefineOwnProperty(thread_, generator_func_prototype, constructor_key, generator_desc);
    generator_function->SetProtoOrDynClass(thread_, generator_func_instance_dynclass.GetTaggedValue());
    env->SetGeneratorFunctionFunction(thread_, generator_function);

    // 26.3.3.2 GeneratorFunction.prototype.prototype -> Generator prototype object.
    PropertyDescriptor descriptor(thread_, env->GetGeneratorPrototype(), false, false, true);
    JSObject::DefineOwnProperty(thread_, generator_func_prototype, global_const->GetHandledPrototypeString(),
                                descriptor);

    // 26.3.3.3 GeneratorFunction.prototype[@@toStringTag]
    SetStringTagSymbol(env, generator_func_prototype, "GeneratorFunction");

    // GeneratorFunction prototype __proto__ -> Function.
    JSObject::SetPrototype(thread_, generator_func_prototype, env->GetFunctionPrototype());

    // 26.5.1.1 Generator.prototype.constructor -> %GeneratorFunction.prototype%.
    PropertyDescriptor generator_obj_desc(thread_, generator_func_prototype_value, false, false, true);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>(env->GetInitialGenerator()),
                                global_const->GetHandledConstructorString(), generator_obj_desc);

    // Generator instances prototype -> GeneratorFunction.prototype.prototype
    PropertyDescriptor generator_obj_proto_desc(thread_, generator_func_prototype_value, true, false, false);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>(env->GetInitialGenerator()),
                                global_const->GetHandledPrototypeString(), generator_obj_proto_desc);

    env->SetGeneratorFunctionPrototype(thread_, generator_func_prototype);
}

void Builtins::InitializeGenerator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSObject> generator_func_prototype = factory_->NewJSObject(obj_func_dynclass);

    SetFunctionsGenGeneratorProto(env, generator_func_prototype);

    // 26.5.1.5 Generator.prototype[@@toStringTag]
    SetStringTagSymbol(env, generator_func_prototype, "Generator");

    // Generator with constructor, symbolTag, next/return/throw etc.
    PropertyDescriptor descriptor(thread_, env->GetIteratorPrototype(), true, false, false);
    JSObject::DefineOwnProperty(thread_, generator_func_prototype, global_const->GetHandledPrototypeString(),
                                descriptor);
    env->SetGeneratorPrototype(thread_, generator_func_prototype);
    JSObject::SetPrototype(thread_, generator_func_prototype, env->GetIteratorPrototype());

    // Generator {}
    JSHandle<JSObject> initial_generator_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSObject::SetPrototype(thread_, initial_generator_func_prototype,
                           JSHandle<JSTaggedValue>(generator_func_prototype));
    env->SetInitialGenerator(thread_, initial_generator_func_prototype);
}

void Builtins::InitializeAsyncGeneratorFunction(const JSHandle<GlobalEnv> &env,
                                                const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSObject> async_generator_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSHandle<JSTaggedValue> async_generator_func_prototype_value(async_generator_func_prototype);

    // 26.3.3.1 AsyncGeneratorFunction.prototype.constructor
    // AsyncGeneratorFunction.prototype_or_dynclass
    JSHandle<JSHClass> async_generator_func_instance_dynclass = factory_->CreateDynClass<JSFunction>(
        JSType::JS_ASYNC_GENERATOR_FUNCTION, async_generator_func_prototype_value, HClass::IS_CALLABLE);
    async_generator_func_instance_dynclass->SetExtensible(true);

    // AsyncGeneratorFunction = new AsyncGeneratorFunction()
    JSHandle<JSFunction> async_generator_function = NewBuiltinConstructor(
        env, async_generator_func_prototype, builtins::async_generator_function::AsyncGeneratorFunctionConstructor,
        "AsyncGeneratorFunction", FunctionLength::ONE);
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    PropertyDescriptor async_generator_desc(thread_, JSHandle<JSTaggedValue>::Cast(async_generator_function), false,
                                            false, true);
    JSObject::DefineOwnProperty(thread_, async_generator_func_prototype, constructor_key, async_generator_desc);
    async_generator_function->SetProtoOrDynClass(thread_, async_generator_func_instance_dynclass.GetTaggedValue());
    env->SetAsyncGeneratorFunctionFunction(thread_, async_generator_function);

    // 26.3.3.2 AsyncGeneratorFunction.prototype.prototype -> AsyncGenerator prototype object.
    PropertyDescriptor descriptor(thread_, env->GetAsyncGeneratorPrototype(), false, false, true);
    JSObject::DefineOwnProperty(thread_, async_generator_func_prototype, global_const->GetHandledPrototypeString(),
                                descriptor);

    // 26.3.3.3 AsyncGeneratorFunction.prototype[@@toStringTag]
    SetStringTagSymbol(env, async_generator_func_prototype, "AsyncGeneratorFunction");

    // AsyncGeneratorFunction prototype __proto__ -> Function.
    JSObject::SetPrototype(thread_, async_generator_func_prototype, env->GetFunctionPrototype());

    // 26.5.1.1 AsyncGenerator.prototype.constructor -> %AsyncGeneratorFunction.prototype%.
    PropertyDescriptor async_generator_obj_desc(thread_, async_generator_func_prototype_value, false, false, true);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>(env->GetInitialAsyncGenerator()),
                                global_const->GetHandledConstructorString(), async_generator_obj_desc);

    // AsyncGenerator instances prototype -> AsyncGeneratorFunction.prototype.prototype
    PropertyDescriptor async_generator_obj_proto_desc(thread_, async_generator_func_prototype_value, true, false,
                                                      false);
    JSObject::DefineOwnProperty(thread_, JSHandle<JSObject>(env->GetInitialAsyncGenerator()),
                                global_const->GetHandledPrototypeString(), async_generator_obj_proto_desc);

    env->SetAsyncGeneratorFunctionPrototype(thread_, async_generator_func_prototype);
}

void Builtins::InitializeAsyncGenerator(const JSHandle<GlobalEnv> &env,
                                        const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSHandle<JSObject> async_generator_func_prototype = factory_->NewJSObject(obj_func_dynclass);

    SetFunctionsGenAsyncGeneratorProto(env, async_generator_func_prototype);

    // 27.6.1.5 AsyncGenerator.prototype[@@toStringTag]
    SetStringTagSymbol(env, async_generator_func_prototype, "AsyncGenerator");

    PropertyDescriptor descriptor(thread_, env->GetAsyncIteratorPrototype(), true, false, false);
    JSObject::DefineOwnProperty(thread_, async_generator_func_prototype, global_const->GetHandledPrototypeString(),
                                descriptor);
    env->SetAsyncGeneratorPrototype(thread_, async_generator_func_prototype);
    JSObject::SetPrototype(thread_, async_generator_func_prototype, env->GetAsyncIteratorPrototype());

    // AsyncGenerator {}
    JSHandle<JSObject> initial_async_generator_func_prototype = factory_->NewJSObject(obj_func_dynclass);
    JSObject::SetPrototype(thread_, initial_async_generator_func_prototype,
                           JSHandle<JSTaggedValue>(async_generator_func_prototype));
    env->SetInitialAsyncGenerator(thread_, initial_async_generator_func_prototype);
}

void Builtins::InitializeAsyncFromSyncIteratorPrototypeObject(const JSHandle<GlobalEnv> &env,
                                                              const JSHandle<JSHClass> &obj_func_dynclass) const
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<JSObject> async_from_sync_iterator_prototype = factory_->NewJSObject(obj_func_dynclass);

    SetFunctionsGenAsyncFromSyncIteratorProto(env, async_from_sync_iterator_prototype);

    JSObject::SetPrototype(thread_, async_from_sync_iterator_prototype, env->GetAsyncIteratorPrototype());
    env->SetAsyncFromSyncIteratorPrototype(thread_, async_from_sync_iterator_prototype);
}

void Builtins::SetArgumentsSharedAccessor(const JSHandle<GlobalEnv> &env)
{
    JSHandle<JSTaggedValue> throw_function = env->GetThrowTypeError();

    JSHandle<AccessorData> accessor = factory_->NewAccessorData();
    accessor->SetGetter(thread_, throw_function);
    accessor->SetSetter(thread_, throw_function);
    env->SetArgumentsCallerAccessor(thread_, accessor);

    accessor = factory_->NewAccessorData();
    accessor->SetGetter(thread_, throw_function);
    accessor->SetSetter(thread_, throw_function);
    env->SetArgumentsCalleeAccessor(thread_, accessor);
}

void Builtins::SetAccessor(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                           const JSHandle<JSTaggedValue> &getter, const JSHandle<JSTaggedValue> &setter) const
{
    JSHandle<AccessorData> accessor = factory_->NewAccessorData();
    accessor->SetGetter(thread_, getter);
    accessor->SetSetter(thread_, setter);
    PropertyAttributes attr = PropertyAttributes::DefaultAccessor(false, false, true);
    JSObject::AddAccessor(thread_, JSHandle<JSTaggedValue>::Cast(obj), key, accessor, attr);
}

void Builtins::SetGetter(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                         const JSHandle<JSTaggedValue> &getter) const
{
    JSHandle<AccessorData> accessor = factory_->NewAccessorData();
    accessor->SetGetter(thread_, getter);
    PropertyAttributes attr = PropertyAttributes::DefaultAccessor(false, false, true);
    JSObject::AddAccessor(thread_, JSHandle<JSTaggedValue>::Cast(obj), key, accessor, attr);
}

JSHandle<JSFunction> Builtins::NewIntlConstructor(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &prototype,
                                                  EcmaEntrypoint ctor_func, const char *name, int length)
{
    JSHandle<JSFunction> ctor =
        factory_->NewJSFunction(env, reinterpret_cast<void *>(ctor_func), FunctionKind::BUILTIN_CONSTRUCTOR);
    InitializeIntlCtor(env, prototype, ctor, name, length);
    return ctor;
}

void Builtins::InitializeIntlCtor(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &prototype,
                                  const JSHandle<JSFunction> &ctor, const char *name, int length)
{
    const GlobalEnvConstants *global_const = thread_->GlobalConstants();
    JSFunction::SetFunctionLength(thread_, ctor, JSTaggedValue(length));
    JSHandle<JSTaggedValue> name_string(factory_->NewFromString(name));
    JSFunction::SetFunctionName(thread_, JSHandle<JSFunctionBase>(ctor), name_string,
                                JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Undefined()));
    JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
    PropertyDescriptor descriptor1(thread_, JSHandle<JSTaggedValue>::Cast(ctor), true, false, true);
    JSObject::DefineOwnProperty(thread_, prototype, constructor_key, descriptor1);

    // set "prototype" in constructor.
    ctor->SetFunctionPrototype(thread_, prototype.GetTaggedValue());

    if (!JSTaggedValue::SameValue(name_string, thread_->GlobalConstants()->GetHandledAsyncFunctionString())) {
        JSHandle<JSObject> intl_object(thread_, env->GetIntlFunction().GetTaggedValue());
        PropertyDescriptor descriptor2(thread_, JSHandle<JSTaggedValue>::Cast(ctor), true, false, true);
        JSObject::DefineOwnProperty(thread_, intl_object, name_string, descriptor2);
    }
}

void Builtins::InitializeIntl(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &obj_func_prototype_value)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<JSHClass> intl_dynclass = factory_->CreateDynClass<JSObject>(JSType::JS_INTL, obj_func_prototype_value);
    JSHandle<JSObject> intl_object = factory_->NewJSObject(intl_dynclass);

    JSHandle<JSTaggedValue> init_intl_symbol(factory_->NewPublicSymbolWithChar("Symbol.IntlLegacyConstructedSymbol"));
    SetNoneAttributeProperty(intl_object, "fallbackSymbol", init_intl_symbol);

    SetFunctionsGenIntl(env, intl_object);

    // initial value of the "Intl" property of the global object.
    JSHandle<JSTaggedValue> intl_string(factory_->NewFromString("Intl"));
    JSHandle<JSObject> global_object(thread_, env->GetGlobalObject());
    PropertyDescriptor intl_desc(thread_, JSHandle<JSTaggedValue>::Cast(intl_object), true, false, true);
    JSObject::DefineOwnProperty(thread_, global_object, intl_string, intl_desc);

    SetStringTagSymbol(env, intl_object, "Intl");

    env->SetIntlFunction(thread_, intl_object);
}

void Builtins::InitializeDateTimeFormat(const JSHandle<GlobalEnv> &env)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // DateTimeFormat.prototype
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> dtf_prototype = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> dtf_prototype_value(dtf_prototype);

    // DateTimeFormat.prototype_or_dynclass
    JSHandle<JSHClass> dtf_func_instance_dynclass =
        factory_->CreateDynClass<JSDateTimeFormat>(JSType::JS_DATE_TIME_FORMAT, dtf_prototype_value);

    // DateTimeFormat = new Function()
    // 13.4.1 Intl.DateTimeFormat.prototype.constructor
    JSHandle<JSObject> dtf_function(NewIntlConstructor(env, dtf_prototype, date_time_format::DateTimeFormatConstructor,
                                                       "DateTimeFormat", FunctionLength::ZERO));
    JSHandle<JSFunction>(dtf_function)->SetFunctionPrototype(thread_, JSTaggedValue(*dtf_func_instance_dynclass));

    SetFunctionsGenDateTimeFormat(env, dtf_function);

    // DateTimeFormat.prototype method
    // 13.4.2 Intl.DateTimeFormat.prototype [ @@toStringTag ]
    SetStringTagSymbol(env, dtf_prototype, "Intl.DateTimeFormat");
    env->SetDateTimeFormatFunction(thread_, dtf_function);

    SetFunctionsGenDateTimeFormatProto(env, dtf_prototype);
}

void Builtins::InitializeRelativeTimeFormat(const JSHandle<GlobalEnv> &env)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // RelativeTimeFormat.prototype
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> rtf_prototype = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> rtf_prototype_value(rtf_prototype);

    // RelativeTimeFormat.prototype_or_dynclass
    JSHandle<JSHClass> rtf_func_instance_dynclass =
        factory_->CreateDynClass<JSRelativeTimeFormat>(JSType::JS_RELATIVE_TIME_FORMAT, rtf_prototype_value);

    // RelativeTimeFormat = new Function()
    // 14.2.1 Intl.RelativeTimeFormat.prototype.constructor
    JSHandle<JSObject> rtf_function(NewIntlConstructor(env, rtf_prototype,
                                                       relative_time_format::RelativeTimeFormatConstructor,
                                                       "RelativeTimeFormat", FunctionLength::ZERO));
    JSHandle<JSFunction>(rtf_function)->SetFunctionPrototype(thread_, JSTaggedValue(*rtf_func_instance_dynclass));
    SetFunctionsGenRelativeTimeFormat(env, rtf_function);

    // RelativeTimeFormat.prototype method
    // 14.4.2 Intl.RelativeTimeFormat.prototype [ @@toStringTag ]
    SetStringTagSymbol(env, rtf_prototype, "Intl.RelativeTimeFormat");
    env->SetRelativeTimeFormatFunction(thread_, rtf_function);

    SetFunctionsGenRelativeTimeFormatProto(env, rtf_prototype);
}

void Builtins::InitializeNumberFormat(const JSHandle<GlobalEnv> &env)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // NumberFormat.prototype
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> nf_prototype = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> nf_prototype_value(nf_prototype);

    // NumberFormat.prototype_or_dynclass
    JSHandle<JSHClass> nf_func_instance_dynclass =
        factory_->CreateDynClass<JSNumberFormat>(JSType::JS_NUMBER_FORMAT, nf_prototype_value);

    // NumberFormat = new Function()
    // 12.4.1 Intl.NumberFormat.prototype.constructor
    JSHandle<JSObject> nf_function(NewIntlConstructor(env, nf_prototype, number_format::NumberFormatConstructor,
                                                      "NumberFormat", FunctionLength::ZERO));
    JSHandle<JSFunction>(nf_function)->SetFunctionPrototype(thread_, JSTaggedValue(*nf_func_instance_dynclass));

    SetFunctionsGenNumberFormat(env, nf_function);
    // NumberFormat.prototype method
    // 12.4.2 Intl.NumberFormat.prototype [ @@toStringTag ]
    SetStringTagSymbol(env, nf_prototype, "Intl.NumberFormat");
    env->SetNumberFormatFunction(thread_, nf_function);

    SetFunctionsGenNumberFormatProto(env, nf_prototype);
}

void Builtins::InitializeLocale(const JSHandle<GlobalEnv> &env)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Locale.prototype
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> locale_prototype = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> locale_prototype_value(locale_prototype);

    // Locale.prototype_or_dynclass
    JSHandle<JSHClass> locale_func_instance_dynclass =
        factory_->CreateDynClass<JSLocale>(JSType::JS_LOCALE, locale_prototype_value);

    // Locale = new Function()
    JSHandle<JSObject> locale_function(
        NewIntlConstructor(env, locale_prototype, locale::LocaleConstructor, "Locale", FunctionLength::ONE));
    JSHandle<JSFunction>(locale_function)->SetFunctionPrototype(thread_, JSTaggedValue(*locale_func_instance_dynclass));
    SetFunctionsGenLocaleProto(env, locale_prototype);

    // 10.3.2 Intl.Locale.prototype[ @@toStringTag ]
    SetStringTagSymbol(env, locale_prototype, "Intl.Locale");
    env->SetLocaleFunction(thread_, locale_function);
}

void Builtins::InitializeCollator(const JSHandle<GlobalEnv> &env)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // Collator.prototype
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> collator_prototype = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> collator_prototype_value(collator_prototype);

    // Collator.prototype_or_dynclass
    JSHandle<JSHClass> collator_func_instance_dynclass =
        factory_->CreateDynClass<JSCollator>(JSType::JS_COLLATOR, collator_prototype_value);

    // Collator = new Function()
    // 11.1.2 Intl.Collator.prototype.constructor
    JSHandle<JSObject> collator_function(
        NewIntlConstructor(env, collator_prototype, collator::CollatorConstructor, "Collator", FunctionLength::ZERO));
    JSHandle<JSFunction>(collator_function)
        ->SetFunctionPrototype(thread_, JSTaggedValue(*collator_func_instance_dynclass));

    SetFunctionsGenCollator(env, collator_function);

    // Collator.prototype method
    // 11.3.2 Intl.Collator.prototype [ @@toStringTag ]
    SetStringTagSymbol(env, collator_prototype, "Intl.Collator");
    env->SetCollatorFunction(thread_, collator_function);

    SetFunctionsGenCollatorProto(env, collator_prototype);
}

void Builtins::InitializePluralRules(const JSHandle<GlobalEnv> &env)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    // PluralRules.prototype
    JSHandle<JSTaggedValue> obj_fun(env->GetObjectFunction());
    JSHandle<JSObject> pr_prototype = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSHandle<JSTaggedValue> pr_prototype_value(pr_prototype);

    // PluralRules.prototype_or_dynclass
    JSHandle<JSHClass> pr_func_instance_dynclass =
        factory_->CreateDynClass<JSPluralRules>(JSType::JS_PLURAL_RULES, pr_prototype_value);

    // PluralRules = new Function()
    // 15.2.1 Intl.PluralRules.prototype.constructor
    JSHandle<JSObject> pr_function(NewIntlConstructor(env, pr_prototype, plural_rules::PluralRulesConstructor,
                                                      "PluralRules", FunctionLength::ZERO));
    JSHandle<JSFunction>(pr_function)->SetFunctionPrototype(thread_, JSTaggedValue(*pr_func_instance_dynclass));

    env->SetPluralRulesFunction(thread_, pr_function);
    SetFunctionsGenPluralRules(env, pr_function);

    // PluralRules.prototype method
    // 15.4.2 Intl.PluralRules.prototype [ @@toStringTag ]
    SetStringTagSymbol(env, pr_prototype, "Intl.PluralRules");

    SetFunctionsGenPluralRulesProto(env, pr_prototype);
}

void Builtins::InitializeGcMarker(const JSHandle<GlobalEnv> &env) const
{
    JSHandle<JSObject> marker = factory_->NewEmptyJSObject();
    env->SetGcMarker(thread_, marker.GetTaggedValue());
    SetFunctionsGenGCMarker(env, marker);
}

JSHandle<JSObject> Builtins::InitializeArkTools(const JSHandle<GlobalEnv> &env) const
{
    JSHandle<JSObject> tools = factory_->NewEmptyJSObject();
    SetFunctionsGenArkTools(env, tools);
    return tools;
}

JSHandle<JSObject> Builtins::InitializeArkPrivate(const JSHandle<GlobalEnv> &env) const
{
    JSHandle<JSObject> ark_private = factory_->NewEmptyJSObject();
    SetFrozenFunction(env, ark_private, "Load", ContainersPrivate::Load, FunctionLength::ZERO);
    SetConstant(ark_private, "ArrayList", JSTaggedValue(static_cast<int>(containers::ContainerTag::ARRAY_LIST)));
    SetConstant(ark_private, "Queue", JSTaggedValue(static_cast<int>(containers::ContainerTag::QUEUE)));
    SetConstant(ark_private, "Deque", JSTaggedValue(static_cast<int>(containers::ContainerTag::DEQUE)));
    SetConstant(ark_private, "Stack", JSTaggedValue(static_cast<int>(containers::ContainerTag::STACK)));
    SetConstant(ark_private, "Vector", JSTaggedValue(static_cast<int>(containers::ContainerTag::VECTOR)));
    SetConstant(ark_private, "List", JSTaggedValue(static_cast<int>(containers::ContainerTag::LIST)));
    SetConstant(ark_private, "LinkedList", JSTaggedValue(static_cast<int>(containers::ContainerTag::LINKED_LIST)));
    SetConstant(ark_private, "TreeMap", JSTaggedValue(static_cast<int>(containers::ContainerTag::TREE_MAP)));
    SetConstant(ark_private, "TreeSet", JSTaggedValue(static_cast<int>(containers::ContainerTag::TREE_SET)));
    SetConstant(ark_private, "HashMap", JSTaggedValue(static_cast<int>(containers::ContainerTag::HASH_MAP)));
    SetConstant(ark_private, "HashSet", JSTaggedValue(static_cast<int>(containers::ContainerTag::HASH_SET)));
    SetConstant(ark_private, "LightWightMap",
                JSTaggedValue(static_cast<int>(containers::ContainerTag::LIGHT_WIGHT_MAP)));
    SetConstant(ark_private, "LightWightSet",
                JSTaggedValue(static_cast<int>(containers::ContainerTag::LIGHT_WIGHT_SET)));
    SetConstant(ark_private, "PlainArray", JSTaggedValue(static_cast<int>(containers::ContainerTag::PLAIN_ARRAY)));
    return ark_private;
}

#ifndef PANDA_PRODUCT_BUILD
JSHandle<JSObject> Builtins::InitializeRuntimeTesting(const JSHandle<GlobalEnv> &env) const
{
    JSHandle<JSObject> runtime_testing = factory_->NewEmptyJSObject();
    SetFunctionsGenRuntimeTesting(env, runtime_testing);
    return runtime_testing;
}
#endif  // PANDA_PRODUCT_BUILD

#ifdef FUZZING_FUZZILLI_BUILTIN
void Builtins::InitializeFuzzilli(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass)
{
    class BuiltinsFuzzilli {
    public:
        static JSTaggedValue FuzzilliConstructor(EcmaRuntimeCallInfo *argv)
        {
            ASSERT(argv);
            BUILTINS_API_TRACE(argv->GetThread(), Object, ObjectConstructor);
            JSThread *thread = argv->GetThread();
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);

            auto value = builtins_common::GetCallArg(argv, 0);
            JSHandle<EcmaString> command = JSTaggedValue::ToString(thread, value);
            auto commandVal = base::StringHelper::ToStdString(*command);
            if (commandVal == "FUZZILLI_CRASH") {
                JSTaggedNumber num = JSTaggedValue::ToNumber(thread, builtins_common::GetCallArg(argv, 1));
                auto numVal = base::NumberHelper::DoubleInRangeInt32(num.GetNumber());
                switch (numVal) {
                    case 0:
                        *((int *)0x41414141) = 0x1337;
                        break;
                    case 1:
                        ASSERT(false);
                        break;
                    default:
                        ASSERT(false);
                        break;
                }
            } else if (commandVal == "FUZZILLI_PRINT") {
                JSHandle<EcmaString> arg = JSTaggedValue::ToString(thread, builtins_common::GetCallArg(argv, 1));
                auto stringVal = base::StringHelper::ToStdString(*arg);
                // REPRL_DWFD for fuzzilli
                FILE *fzliout = fdopen(103, "w");
                if (!fzliout) {
                    fzliout = stderr;
                    fprintf(fzliout, "Fuzzer output channel not available, printing to stderr instead\n");
                }

                fprintf(fzliout, "%s\n", stringVal.c_str());

                fflush(fzliout);
            }

            return JSTaggedValue::ToObject(thread, value).GetTaggedValue();
        }
    };

    JSHandle<JSObject> fuzzilliFuncPrototype = factory_->NewJSObject(objFuncDynclass);
    JSHandle<JSTaggedValue> fuzzilliFuncPrototypeValue(fuzzilliFuncPrototype);

    JSHandle<JSHClass> fuzzilliFuncInstanceDynclass =
        factory_->CreateDynClass<JSDate>(JSType::JS_FUNCTION, fuzzilliFuncPrototypeValue);

    JSHandle<JSObject> fuzzilliFunction(NewBuiltinConstructor(
        env, fuzzilliFuncPrototype, BuiltinsFuzzilli::FuzzilliConstructor, "fuzzilli", FunctionLength::THREE));
    JSHandle<JSFunction>(fuzzilliFunction)
        ->SetFunctionPrototype(thread_, fuzzilliFuncInstanceDynclass.GetTaggedValue());
}
#endif
}  // namespace panda::ecmascript::builtins
