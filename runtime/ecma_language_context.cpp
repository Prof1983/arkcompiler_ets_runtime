/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_language_context-inl.h"
#include "plugins/ecmascript/runtime/ecma_language_context.h"

#include "plugins/ecmascript/compiler/ecmascript_extensions/ecmascript_environment.h"
#include "plugins/ecmascript/runtime/ecma_class_linker_extension.h"
#include "plugins/ecmascript/runtime/class_linker/program_object.h"
#include "plugins/ecmascript/runtime/ecma_exceptions.h"
#include "plugins/ecmascript/runtime/base/error_type.h"
#include "plugins/ecmascript/runtime/js_method.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/lexical_env.h"
#include "plugins/ecmascript/runtime/interpreter/js_frame.h"
#include "include/method.h"
#include "runtime/core/core_itable_builder.h"
#include "runtime/core/core_vtable_builder.h"

namespace panda {
using ecmascript::EcmaVM;
using ecmascript::JSThread;

std::pair<Method *, uint32_t> EcmaLanguageContext::GetCatchMethodAndOffset(Method *method, ManagedThread *thread) const
{
    Method *catch_method = method;
    uint32_t catch_offset = 0;
    auto js_thread = static_cast<JSThread *>(thread);
    for (auto stack = StackWalker::Create(thread); stack.HasFrame(); stack.NextFrame()) {
        catch_method = stack.GetMethod();
        if (catch_method->IsNative()) {
            continue;
        }
        catch_offset = catch_method->FindCatchBlock(js_thread->GetException().GetTaggedObject()->ClassAddr<Class>(),
                                                    stack.GetBytecodePc());
        if (catch_offset != panda_file::INVALID_OFFSET) {
            break;
        }
    }

    return std::make_pair(catch_method, catch_offset);
}

PandaVM *EcmaLanguageContext::CreateVM(Runtime *runtime, const RuntimeOptions &options) const
{
    auto ret = EcmaVM::Create(runtime, ecmascript::JSRuntimeOptions(options));
    if (ret) {
        return ret.Value();
    }
    return nullptr;
}

std::unique_ptr<ClassLinkerExtension> EcmaLanguageContext::CreateClassLinkerExtension() const
{
    return std::make_unique<ecmascript::EcmaClassLinkerExtension>();
}

PandaUniquePtr<tooling::PtLangExt> EcmaLanguageContext::CreatePtLangExt() const
{
    return PandaUniquePtr<tooling::PtLangExt>();
}

void EcmaLanguageContext::ThrowException(ManagedThread *thread, const uint8_t *mutf8_name,
                                         const uint8_t *mutf8_msg) const
{
    ecmascript::ThrowException(JSThread::Cast(thread), reinterpret_cast<const char *>(mutf8_name),
                               reinterpret_cast<const char *>(mutf8_msg));
}

void EcmaLanguageContext::ThrowStackOverflowException(ManagedThread *thread) const
{
    ecmascript::ThrowException(JSThread::Cast(thread), ecmascript::RANGE_ERROR_STRING,
                               "Maximum call stack size exceeded");
}

PandaUniquePtr<ITableBuilder> EcmaLanguageContext::CreateITableBuilder() const
{
    return MakePandaUnique<CoreITableBuilder>();
}

PandaUniquePtr<VTableBuilder> EcmaLanguageContext::CreateVTableBuilder() const
{
    return MakePandaUnique<CoreVTableBuilder>();
}

size_t EcmaLanguageContext::GetStringSize(const ObjectHeader *string_object) const
{
    auto string = static_cast<const ecmascript::EcmaString *>(string_object);
    return string->ObjectSize();
}

using VRegType = panda::compiler::VRegInfo::VRegType;
void EcmaLanguageContext::RestoreEnv(Frame *current_iframe, const StackWalker::EnvData &env_data) const
{
    auto *iframe_env = ecmascript::JSExtFrame::FromFrame(current_iframe)->GetExtData();
    iframe_env->SetThisFunc(
        ecmascript::JSFunction::Cast(ecmascript::JSTaggedValue(env_data[VRegType::THIS_FUNC]).GetHeapObject()));
    iframe_env->SetConstantPool(ecmascript::ConstantPool::Cast(env_data[VRegType::CONST_POOL]));
    iframe_env->SetLexicalEnv(ecmascript::LexicalEnv::Cast(env_data[VRegType::LEX_ENV]));
    // restore func parameter
    auto num_vregs = current_iframe->GetMethod()->GetNumVregs();
    DynamicFrameHandler frame_handler(current_iframe);
    frame_handler.GetVReg(num_vregs + ecmascript::js_method_args::FUNC_IDX).SetValue(env_data[VRegType::THIS_FUNC]);
}

void EcmaLanguageContext::InitializeOsrCframeSlots(Span<uintptr_t> param_slots) const
{
    std::fill(param_slots.begin(), param_slots.end(), TaggedValue::VALUE_UNDEFINED);
}

uint64_t EcmaLanguageContext::GetOsrEnv(const Frame *iframe, compiler::VRegInfo vreg_info) const
{
    auto env = ecmascript::JSFrame::GetJSEnv(iframe);
    switch (vreg_info.GetVRegType()) {
        case compiler::VRegInfo::VRegType::THIS_FUNC:
            return TaggedValue(env->GetThisFunc()).GetRawData();
        case compiler::VRegInfo::VRegType::CONST_POOL:
            return TaggedValue(env->GetConstantPool()).GetRawData();
        case compiler::VRegInfo::VRegType::LEX_ENV:
            return TaggedValue(env->GetLexicalEnv()).GetRawData();
        default:
            UNREACHABLE();
            return 0;
    }
}

}  // namespace panda
