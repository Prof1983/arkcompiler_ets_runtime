/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JSARRAYBUFFER_H
#define ECMASCRIPT_JSARRAYBUFFER_H

#include "plugins/ecmascript/runtime/js_object.h"

namespace panda::ecmascript {
class JSArrayBuffer final : public JSObject {
public:
    CAST_NO_CHECK(JSArrayBuffer);

    // 6.2.6.2
    static void CopyDataBlockBytes(JSTaggedValue to_block, JSTaggedValue from_block, int32_t from_index, int32_t count);

    void Attach(JSThread *thread, JSTaggedValue array_buffer_byte_length, JSTaggedValue array_buffer_data);
    void Detach(JSThread *thread);

    bool IsDetach()
    {
        JSTaggedValue array_buffer_data = GetArrayBufferData();
        return array_buffer_data == JSTaggedValue::Null();
    }

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, ArrayBufferByteLength)
    ACCESSORS(1, ArrayBufferData)
    ACCESSORS(2, Shared)
    ACCESSORS_FINISH(3)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JSARRAYBUFFER_H
