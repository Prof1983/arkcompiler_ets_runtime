/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JSDATAVIEW_H
#define ECMASCRIPT_JSDATAVIEW_H

#include "plugins/ecmascript/runtime/js_object.h"

namespace panda::ecmascript {
enum class DataViewType : uint8_t {
    BIGINT64 = 0,
    BIGUINT64,
    FLOAT32,
    FLOAT64,
    INT8,
    INT16,
    INT32,
    UINT8,
    UINT16,
    UINT32,
    UINT8_CLAMPED
};
class JSDataView : public JSObject {
public:
    CAST_CHECK(JSDataView, IsDataView);

    static int32_t GetElementSize(DataViewType type);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, DataView)
    ACCESSORS(1, ViewedArrayBuffer)
    ACCESSORS(2, ByteLength)
    ACCESSORS(3, ByteOffset)
    ACCESSORS_FINISH(4)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JSDATAVIEW_H
