/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_RELATIVE_TIME_FORMAT_H
#define ECMASCRIPT_JS_RELATIVE_TIME_FORMAT_H

#include "unicode/reldatefmt.h"

#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/common.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_date.h"
#include "plugins/ecmascript/runtime/js_intl.h"
#include "plugins/ecmascript/runtime/js_locale.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript {
class JSRelativeTimeFormat : public JSObject {
public:
    CAST_CHECK(JSRelativeTimeFormat, IsJSRelativeTimeFormat);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, Locale)
    ACCESSORS(1, InitializedRelativeTimeFormat)
    ACCESSORS(2, NumberingSystem)
    ACCESSORS(3, Style)
    ACCESSORS(4, Numeric)
    ACCESSORS(5, AvailableLocales)
    ACCESSORS(6, IcuField)  // icu field
    ACCESSORS_FINISH(7)

    DECL_DUMP()

    // 14.1.1 InitializeRelativeTimeFormat ( relativeTimeFormat, locales, options )
    static JSHandle<JSRelativeTimeFormat> InitializeRelativeTimeFormat(
        JSThread *thread, const JSHandle<JSRelativeTimeFormat> &relative_time_format,
        const JSHandle<JSTaggedValue> &locales, const JSHandle<JSTaggedValue> &options);

    // UnwrapRelativeTimeFormat
    static JSHandle<JSTaggedValue> UnwrapRelativeTimeFormat(JSThread *thread, const JSHandle<JSTaggedValue> &rtf);

    // Get icu formatter from icu field
    icu::RelativeDateTimeFormatter *GetIcuRTFFormatter() const
    {
        ASSERT(GetIcuField().IsJSNativePointer());
        auto result = JSNativePointer::Cast(GetIcuField().GetTaggedObject())->GetExternalPointer();
        return reinterpret_cast<icu::RelativeDateTimeFormatter *>(result);
    }

    static void FreeIcuRTFFormatter(void *pointer, [[maybe_unused]] void *data)
    {
        if (pointer == nullptr) {
            return;
        }
        auto icu_formatter = reinterpret_cast<icu::RelativeDateTimeFormatter *>(pointer);
        if (data != nullptr) {
            Runtime::GetCurrent()->GetInternalAllocator()->Delete(icu_formatter);
        }
    }

    static void ResolvedOptions(JSThread *thread, const JSHandle<JSRelativeTimeFormat> &relative_time_format,
                                const JSHandle<JSObject> &options);

    static JSHandle<EcmaString> Format(JSThread *thread, double value, const JSHandle<EcmaString> &unit,
                                       const JSHandle<JSRelativeTimeFormat> &relative_time_format);

    static JSHandle<JSArray> FormatToParts(JSThread *thread, double value, const JSHandle<EcmaString> &unit,
                                           const JSHandle<JSRelativeTimeFormat> &relative_time_format);
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_RELATIVE_TIME_FORMAT_H
