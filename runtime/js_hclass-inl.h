/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_HCLASS_INL_H
#define ECMASCRIPT_JS_HCLASS_INL_H

#include "plugins/ecmascript/runtime/js_hclass.h"

#include "plugins/ecmascript/runtime/layout_info-inl.h"
#include "plugins/ecmascript/runtime/transitions_dictionary.h"

namespace panda::ecmascript {
inline JSHClass *JSHClass::Cast(const TaggedObject *object)
{
    ASSERT(JSTaggedValue(object).IsJSHClass());
    return static_cast<JSHClass *>(const_cast<TaggedObject *>(object));
}

void JSHClass::AddTransitions(const JSThread *thread, const JSHandle<JSHClass> &parent, const JSHandle<JSHClass> &child,
                              const JSHandle<JSTaggedValue> &key, PropertyAttributes attributes)
{
    JSTaggedValue transitions = parent->GetTransitions();
    if (transitions.IsNull()) {
        parent->SetTransitions(thread, child.GetTaggedValue());
        child->SetParent(thread, parent.GetTaggedValue());
        return;
    }
    JSMutableHandle<TransitionsDictionary> dict(thread, JSTaggedValue::Undefined());
    if (transitions.IsJSHClass()) {
        auto cached_h_class = JSHClass::Cast(transitions.GetTaggedObject());
        int32_t last = cached_h_class->NumberOfProps() - 1;
        LayoutInfo *layout_info = LayoutInfo::Cast(cached_h_class->GetLayout().GetTaggedObject());
        auto attr = JSHandle<JSTaggedValue>(thread, JSTaggedValue(layout_info->GetAttr(last).GetPropertyMetaData()));
        auto last_key = JSHandle<JSTaggedValue>(thread, layout_info->GetKey(last));
        auto last_h_class = JSHandle<JSTaggedValue>(thread, cached_h_class);
        dict.Update(TransitionsDictionary::Create(thread));
        transitions = TransitionsDictionary::PutIfAbsent(thread, dict, last_key, last_h_class, attr).GetTaggedValue();
    }
    auto attr = JSHandle<JSTaggedValue>(thread, JSTaggedValue(attributes.GetPropertyMetaData()));
    dict.Update(transitions);
    transitions =
        TransitionsDictionary::PutIfAbsent(thread, dict, key, JSHandle<JSTaggedValue>(child), attr).GetTaggedValue();
    parent->SetTransitions(thread, transitions);
    child->SetParent(thread, parent.GetTaggedValue());
}

void JSHClass::AddExtensionTransitions(const JSThread *thread, const JSHandle<JSHClass> &parent,
                                       const JSHandle<JSHClass> &child, const JSHandle<JSTaggedValue> &key)
{
    auto attr = JSHandle<JSTaggedValue>(thread, PropertyAttributes(0).GetTaggedValue());
    AddProtoTransitions(thread, parent, child, key, attr);
}

void JSHClass::AddProtoTransitions(const JSThread *thread, const JSHandle<JSHClass> &parent,
                                   const JSHandle<JSHClass> &child, const JSHandle<JSTaggedValue> &key,
                                   const JSHandle<JSTaggedValue> &proto)
{
    JSTaggedValue transitions = parent->GetTransitions();
    JSMutableHandle<TransitionsDictionary> dict(thread, JSTaggedValue::Undefined());
    if (transitions.IsNull()) {
        transitions = TransitionsDictionary::Create(thread).GetTaggedValue();
    } else if (transitions.IsJSHClass()) {
        auto cached_h_class = JSHClass::Cast(transitions.GetTaggedObject());
        int32_t last = cached_h_class->NumberOfProps() - 1;
        LayoutInfo *layout_info = LayoutInfo::Cast(cached_h_class->GetLayout().GetTaggedObject());
        auto attr = JSHandle<JSTaggedValue>(thread, JSTaggedValue(layout_info->GetAttr(last).GetPropertyMetaData()));
        auto last_key = JSHandle<JSTaggedValue>(thread, layout_info->GetKey(last));
        auto last_h_class = JSHandle<JSTaggedValue>(thread, cached_h_class);
        dict.Update(TransitionsDictionary::Create(thread));
        transitions = TransitionsDictionary::PutIfAbsent(thread, dict, last_key, last_h_class, attr).GetTaggedValue();
    }

    dict.Update(transitions);
    transitions =
        TransitionsDictionary::PutIfAbsent(thread, dict, key, JSHandle<JSTaggedValue>(child), proto).GetTaggedValue();
    parent->SetTransitions(thread, transitions);
    child->SetParent(thread, parent.GetTaggedValue());
}

inline JSHClass *JSHClass::FindTransitions(const JSTaggedValue &key, const JSTaggedValue &attributes)
{
    JSTaggedValue transitions = GetTransitions();
    if (transitions.IsNull()) {
        return nullptr;
    }
    if (transitions.IsJSHClass()) {
        auto cached_h_class = JSHClass::Cast(transitions.GetTaggedObject());
        int32_t last = cached_h_class->NumberOfProps() - 1;
        LayoutInfo *layout_info = LayoutInfo::Cast(cached_h_class->GetLayout().GetTaggedObject());
        auto attr = layout_info->GetAttr(last).GetPropertyMetaData();
        auto cached_key = layout_info->GetKey(last);
        if (attr == attributes.GetInt() && key == cached_key) {
            return cached_h_class;
        }
        return nullptr;
    }

    ASSERT(transitions.IsTaggedArray());
    TransitionsDictionary *dict = TransitionsDictionary::Cast(transitions.GetTaggedObject());
    auto entry = dict->FindEntry(key, attributes);
    if (entry == -1) {
        return nullptr;
    }
    return JSHClass::Cast(dict->GetValue(entry).GetTaggedObject());
}

inline JSHClass *JSHClass::FindProtoTransitions(const JSTaggedValue &key, const JSTaggedValue &proto)
{
    JSTaggedValue transitions = GetTransitions();
    if (!transitions.IsTaggedArray()) {
        ASSERT(transitions.IsNull() || transitions.IsJSHClass());
        return nullptr;
    }
    ASSERT(transitions.IsTaggedArray());
    TransitionsDictionary *dict = TransitionsDictionary::Cast(transitions.GetTaggedObject());
    auto entry = dict->FindEntry(key, proto);
    if (entry == -1) {
        return nullptr;
    }
    return JSHClass::Cast(dict->GetValue(entry).GetTaggedObject());
}

inline void JSHClass::UpdatePropertyMetaData(const JSThread *thread, [[maybe_unused]] const JSTaggedValue &key,
                                             const PropertyAttributes &meta_data)
{
    ASSERT(!GetLayout().IsNull());
    LayoutInfo *layout_info = LayoutInfo::Cast(GetLayout().GetTaggedObject());
    ASSERT(layout_info->GetLength() != 0);
    int32_t entry = meta_data.GetOffset();

    layout_info->SetNormalAttr(thread, entry, meta_data);
}

inline bool JSHClass::HasReferenceField()
{
    auto type = GetObjectType();
    return type != JSType::STRING && type != JSType::JS_NATIVE_POINTER;
}
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_HCLASS_INL_H
