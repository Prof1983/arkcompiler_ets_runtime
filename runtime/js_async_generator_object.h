/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description:
 */
#ifndef ECMASCRIPT_JS_ASYNC_GENERATOR_OBJECT_H
#define ECMASCRIPT_JS_ASYNC_GENERATOR_OBJECT_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "include/object_header.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"
#include "plugins/ecmascript/runtime/js_async_function.h"

namespace panda::ecmascript {
class JSAsyncGeneratorResolveNextFunction : public JSFunction {
public:
    CAST_CHECK(JSAsyncGeneratorResolveNextFunction, IsJSAsyncGeneratorResolveNextFunction);

    // 27.6.3.6.1 AsyncGeneratorResumeNext Return Processor Fulfilled Functions
    static JSTaggedValue AsyncGeneratorResolveNextFulfilled(EcmaRuntimeCallInfo *argv);

    // 27.6.3.6.2 AsyncGeneratorResumeNext Return Processor Rejected Functions
    static JSTaggedValue AsyncGeneratorResolveNextRejected(EcmaRuntimeCallInfo *argv);

    ACCESSORS_BASE(JSFunction)
    ACCESSORS(0, AsyncGenerator)
    ACCESSORS_FINISH(1)

    DECL_DUMP()
};

class JSAsyncGeneratorObject : public JSAsyncFuncObject {
public:
    CAST_CHECK(JSAsyncGeneratorObject, IsAsyncGeneratorObject);

    // 27.6.3.3 AsyncGeneratorValidate(generator)
    static JSTaggedValue AsyncGeneratorValidate(JSThread *thread, const JSHandle<JSTaggedValue> &generator);

    // 27.6.3.4 AsyncGeneratorResolve (generator, value, done)
    static JSHandle<JSTaggedValue> AsyncGeneratorResolve(JSThread *thread, const JSHandle<JSTaggedValue> &generator,
                                                         const JSHandle<JSTaggedValue> &value, bool done);

    // 27.6.3.5 AsyncGeneratorReject (generator, exception)
    static JSHandle<JSTaggedValue> AsyncGeneratorReject(JSThread *thread, const JSHandle<JSTaggedValue> &generator,
                                                        const JSHandle<JSTaggedValue> &exception);

    // 27.6.3.6 AsyncGeneratorResumeNext (generator)
    static JSHandle<JSTaggedValue> AsyncGeneratorResumeNext(JSThread *thread, const JSHandle<JSTaggedValue> &generator);

    // 27.6.3.7 AsyncGeneratorEnqueue(generator, value)
    static JSHandle<JSTaggedValue> AsyncGeneratorEnqueue(JSThread *thread, const JSHandle<JSTaggedValue> &generator,
                                                         const JSHandle<CompletionRecord> &completion);

    ACCESSORS_BASE(JSAsyncFuncObject)
    ACCESSORS(0, AsyncGeneratorQueue)
    ACCESSORS_FINISH(1)
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_ASYNC_GENERATOR_OBJECT_H
