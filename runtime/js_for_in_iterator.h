/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_FORIN_ITERATOR_H
#define ECMASCRIPT_JS_FORIN_ITERATOR_H

#include <utility>
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "js_object.h"

namespace panda::ecmascript {
class JSForInIterator : public JSObject {
public:
    CAST_CHECK(JSForInIterator, IsForinIterator);

    static std::pair<JSTaggedValue, bool> NextInternal(JSThread *thread, const JSHandle<JSForInIterator> &it);

    static JSTaggedValue Next(EcmaRuntimeCallInfo *msg);

    static bool CheckObjProto(const JSThread *thread, const JSHandle<JSForInIterator> &it);

    static void FastGetAllEnumKeys(const JSThread *thread, const JSHandle<JSForInIterator> &it,
                                   const JSHandle<JSTaggedValue> &object);

    static void SlowGetAllEnumKeys(JSThread *thread, const JSHandle<JSForInIterator> &it,
                                   const JSHandle<JSTaggedValue> &object);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, Object)
    ACCESSORS(1, WasVisited)
    ACCESSORS(2, VisitedKeys)
    ACCESSORS(3, FastRemainingIndex)
    ACCESSORS(4, RemainingKeys)
    ACCESSORS_FINISH(5)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_FORIN_ITERATOR_H
