/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/json_parser.h"
#include "plugins/ecmascript/runtime/base/json_stringifier.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {
// 24.5.1
JSTaggedValue json::Parse(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), Json, Parse);
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    uint32_t argc = argv->GetArgsNumber();
    if (argc == 0) {
        JSHandle<JSObject> syntax_error =
            factory->GetJSError(ecmascript::base::ErrorType::SYNTAX_ERROR, "arg is empty");
        THROW_NEW_ERROR_AND_RETURN_VALUE(thread, syntax_error.GetTaggedValue(), JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSHandle<EcmaString> parse_string = JSTaggedValue::ToString(thread, msg);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> result;
    if (parse_string->IsUtf8()) {
        panda::ecmascript::base::JsonParser<uint8_t> parser(thread);
        result = parser.ParseUtf8(*parse_string);
    } else {
        panda::ecmascript::base::JsonParser<uint16_t> parser(thread);
        result = parser.ParseUtf16(*parse_string);
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSTaggedValue reviver = JSTaggedValue::Undefined();
    if (argc == 2) {  // 2: 2 args
        reviver = builtins_common::GetCallArg(argv, 1).GetTaggedValue();
        if (reviver.IsCallable()) {
            JSHandle<JSTaggedValue> callbackfn_handle(thread, reviver);
            // Let root be ! OrdinaryObjectCreate(%Object.prototype%).
            JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
            JSHandle<JSTaggedValue> constructor = env->GetObjectFunction();
            JSHandle<JSObject> root = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor);
            // Let rootName be the empty String.
            JSHandle<JSTaggedValue> root_name(factory->GetEmptyString());
            // Perform ! CreateDataPropertyOrThrow(root, rootName, unfiltered).
            bool success = JSObject::CreateDataProperty(thread, root, root_name, result);
            if (success) {
                result = base::Internalize::InternalizeJsonProperty(thread, root, root_name, callbackfn_handle);
            }
        }
    }
    return result.GetTaggedValue();
}

// 24.5.2
JSTaggedValue json::Stringify(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), Json, Parse);
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    uint32_t argc = argv->GetArgsNumber();
    JSTaggedValue value = builtins_common::GetCallArg(argv, 0).GetTaggedValue();
    JSTaggedValue replacer = JSTaggedValue::Undefined();
    JSTaggedValue gap = JSTaggedValue::Undefined();

    if (argc == 2) {  // 2: 2 args
        replacer = builtins_common::GetCallArg(argv, 1).GetTaggedValue();
    } else if (argc == 3) {  // 3: 3 args
        replacer = builtins_common::GetCallArg(argv, 1).GetTaggedValue();
        gap = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD).GetTaggedValue();
    }

    JSHandle<JSTaggedValue> handle_value(thread, value);
    JSHandle<JSTaggedValue> handle_replacer(thread, replacer);
    JSHandle<JSTaggedValue> handle_gap(thread, gap);
    panda::ecmascript::base::JsonStringifier stringifier(thread);
    JSHandle<JSTaggedValue> result = stringifier.Stringify(handle_value, handle_replacer, handle_gap);

    return result.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
