/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "libpandabase/macros.h"

namespace panda::ecmascript::builtins {
JSTaggedValue promise_job::PromiseReactionJob(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseJob, PromiseReactionJob);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Assert: reaction is a PromiseReaction Record.
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    ASSERT(value->IsPromiseReaction());
    JSHandle<PromiseReaction> reaction = JSHandle<PromiseReaction>::Cast(value);
    JSHandle<JSTaggedValue> argument = builtins_common::GetCallArg(argv, 1);

    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 2. Let promiseCapability be reaction.[[Capabilities]].
    JSTaggedValue capabilities_value = reaction->GetPromiseCapability();
    // 3. Let handler be reaction.[[Handler]].
    JSHandle<JSTaggedValue> handler(thread, reaction->GetHandler());
    JSHandle<JSTaggedValue> this_value = global_const->GetHandledUndefined();

    auto info = NewRuntimeCallInfo(thread, handler, this_value, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(argument);
    if (capabilities_value.IsUndefined()) {
        [[maybe_unused]] JSTaggedValue val = JSFunction::Call(info.Get());
        return JSTaggedValue::Undefined();
    }

    JSHandle<PromiseCapability> capability(thread, capabilities_value);
    JSMutableHandle<JSTaggedValue> call(thread, capability->GetResolve());

    if (handler->IsString()) {
        // 4. If handler is "Identity", let handlerResult be NormalCompletion(argument).
        // 5. Else if handler is "Thrower", let handlerResult be Completion{[[type]]: throw, [[value]]: argument,
        // [[target]]: empty}.

        if (EcmaString::StringsAreEqual(handler.GetObject<EcmaString>(),
                                        global_const->GetHandledThrowerString().GetObject<EcmaString>())) {
            call.Update(capability->GetReject());
        }
    } else {
        // 6. Else, let handlerResult be Call(handler, undefined, «argument»).
        JSTaggedValue tagged_value = JSFunction::Call(info.Get());
        info->SetCallArgs(tagged_value);
        // 7. If handlerResult is an abrupt completion, then
        // a. Let status be Call(promiseCapability.[[Reject]], undefined, «handlerResult.[[value]]»).
        // b. NextJob Completion(status).
        if (UNLIKELY(thread->HasPendingException())) {
            JSHandle<JSTaggedValue> throw_value = JSPromise::IfThrowGetThrowValue(thread);
            thread->ClearException();
            info->SetCallArgs(throw_value);
            call.Update(capability->GetReject());
        }
    }
    // 8. Let status be Call(promiseCapability.[[Resolve]], undefined, «handlerResult.[[value]]»).
    info->SetFunction(call.GetTaggedValue());
    return JSFunction::Call(info.Get());
}

JSTaggedValue promise_job::PromiseResolveThenableJob(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseJob, PromiseResolveThenableJob);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> promise = builtins_common::GetCallArg(argv, 0);
    ASSERT(promise->IsJSPromise());
    // 1. Let resolvingFunctions be CreateResolvingFunctions(promiseToResolve).
    JSHandle<ResolvingFunctionsRecord> resolving_functions =
        JSPromise::CreateResolvingFunctions(thread, JSHandle<JSPromise>::Cast(promise));
    JSHandle<JSTaggedValue> thenable = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSTaggedValue> then = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);

    // 2. Let thenCallResult be Call(then, thenable, «resolvingFunctions.[[Resolve]], resolvingFunctions.[[Reject]]»).

    JSTaggedValue result;
    {
        auto info = NewRuntimeCallInfo(thread, then, thenable, JSTaggedValue::Undefined(), 2);
        info->SetCallArgs(resolving_functions->GetResolveFunction(), resolving_functions->GetRejectFunction());
        result = JSFunction::Call(info.Get());  // 2: two args
    }
    JSHandle<JSTaggedValue> then_result(thread, result);
    // 3. If thenCallResult is an abrupt completion,
    // a. Let status be Call(resolvingFunctions.[[Reject]], undefined, «thenCallResult.[[value]]»).
    // b. NextJob Completion(status).
    if (UNLIKELY(thread->HasPendingException())) {
        then_result = JSPromise::IfThrowGetThrowValue(thread);
        thread->ClearException();
        JSHandle<JSTaggedValue> reject(thread, resolving_functions->GetRejectFunction());
        JSHandle<JSTaggedValue> undefined = global_const->GetHandledUndefined();
        auto info = NewRuntimeCallInfo(thread, reject, undefined, JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(then_result);
        return JSFunction::Call(info.Get());
    }
    // 4. NextJob Completion(thenCallResult).
    return result;
}
}  // namespace panda::ecmascript::builtins
