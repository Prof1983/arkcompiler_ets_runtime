/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/object_helper.h"
#include "plugins/ecmascript/runtime/builtins.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_realm.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {

enum class KeyType : uint8_t {
    STRING_TYPE = 0,
    SYMBOL_TYPE,
};

JSTaggedValue ObjectDefineProperties(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                     const JSHandle<JSTaggedValue> &prop);
JSTaggedValue GetOwnPropertyKeys(JSThread *thread, const JSHandle<JSTaggedValue> &obj, const KeyType &type);
JSTaggedValue GetBuiltinTag(JSThread *thread, const JSHandle<JSObject> &object);
// 19.1.1.1 Object ( [ value ] )
JSTaggedValue object::ObjectConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, ObjectConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    // 1.If NewTarget is neither undefined nor the active function, then
    //    a.Return OrdinaryCreateFromConstructor(NewTarget, "%ObjectPrototype%").
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (!new_target->IsUndefined() && !(new_target.GetTaggedValue() == constructor.GetTaggedValue())) {
        JSHandle<JSObject> obj =
            ecma_vm->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target);
        return obj.GetTaggedValue();
    }

    // 2.If value is null, undefined or not supplied, return ObjectCreate(%ObjectPrototype%).
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    if (value->IsNull() || value->IsUndefined()) {
        JSHandle<JSObject> obj = ecma_vm->GetFactory()->OrdinaryNewJSObjectCreate(env->GetObjectFunctionPrototype());
        return obj.GetTaggedValue();
    }

    // 3.Return ToObject(value).
    return JSTaggedValue::ToObject(thread, value).GetTaggedValue();
}

// 19.1.2.1 Object.assign ( target, ...sources )
JSTaggedValue object::Assign(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Assign);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    uint32_t num_args = argv->GetArgsNumber();
    // 1.Let to be ToObject(target).
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> to_assign = JSTaggedValue::ToObject(thread, target);
    // 2.ReturnIfAbrupt(to).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3.If only one argument was passed, return to.
    // 4.Let sources be the List of argument values starting with the second argument.
    // 5.For each element nextSource of sources, in ascending index order
    //   a.If nextSource is undefined or null, let keys be an empty List.
    //   b.Else,
    //     i.Let from be ToObject(nextSource).
    //     ii.Let keys be from.[[OwnPropertyKeys]]().
    //     iii.ReturnIfAbrupt(keys).
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> value(thread, JSTaggedValue::Undefined());
    for (uint32_t i = 1; i < num_args; i++) {
        JSHandle<JSTaggedValue> source = builtins_common::GetCallArg(argv, i);
        if (!source->IsNull() && !source->IsUndefined()) {
            JSHandle<JSObject> from = JSTaggedValue::ToObject(thread, source);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            JSHandle<TaggedArray> keys = JSTaggedValue::GetOwnPropertyKeys(thread, JSHandle<JSTaggedValue>::Cast(from));
            // ReturnIfAbrupt(keys)
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            // c.Repeat for each element nextKey of keys in List order,
            //    i.Let desc be from.[[GetOwnProperty]](nextKey).
            //    ii.ReturnIfAbrupt(desc).
            //    iii.if desc is not undefined and desc.[[Enumerable]] is true, then
            //      1.Let propValue be Get(from, nextKey).
            //      2.ReturnIfAbrupt(propValue).
            //      3.Let status be Set(to, nextKey, propValue, true).
            //      4.ReturnIfAbrupt(status).
            uint32_t keys_len = keys->GetLength();
            for (uint32_t j = 0; j < keys_len; j++) {
                PropertyDescriptor desc(thread);
                key.Update(keys->Get(j));
                bool success = JSTaggedValue::GetOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(from), key, desc);
                // ReturnIfAbrupt(desc)
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

                if (success && desc.IsEnumerable()) {
                    value.Update(FastRuntimeStub::FastGetPropertyByValue(thread, JSHandle<JSTaggedValue>(from), key));
                    // ReturnIfAbrupt(prop_value)
                    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

                    FastRuntimeStub::FastSetPropertyByValue(thread, JSHandle<JSTaggedValue>(to_assign), key, value);
                    //  ReturnIfAbrupt(status)
                    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                }
            }
        }
    }

    // 6.Return to.
    return to_assign.GetTaggedValue();
}

// Runtime Semantics
JSTaggedValue ObjectDefineProperties(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                     const JSHandle<JSTaggedValue> &prop)
{
    BUILTINS_API_TRACE(thread, Object, DefineProperties);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1.If Type(O) is not Object, throw a TypeError exception.
    if (!obj->IsECMAObject()) {
        // throw a TypeError exception
        THROW_TYPE_ERROR_AND_RETURN(thread, "is not an object", JSTaggedValue::Exception());
    }

    // 2.Let props be ToObject(Properties).
    JSHandle<JSObject> props = JSTaggedValue::ToObject(thread, prop);

    // 3.ReturnIfAbrupt(props).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4.Let keys be props.[[OwnPropertyKeys]]().
    JSHandle<TaggedArray> handle_keys = JSTaggedValue::GetOwnPropertyKeys(thread, JSHandle<JSTaggedValue>::Cast(props));

    // 5.ReturnIfAbrupt(keys).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6.Let descriptors be an empty List.
    // new an empty array and append
    uint32_t length = handle_keys->GetLength();
    [[maybe_unused]] JSHandle<TaggedArray> descriptors =
        factory->NewTaggedArray(2 * length);  // 2: 2 means two element list

    // 7.Repeat for each element nextKey of keys in List order,
    //   a.Let propDesc be props.[[GetOwnProperty]](nextKey).
    //   b.ReturnIfAbrupt(propDesc).
    //   c.If propDesc is not undefined and propDesc.[[Enumerable]] is true, then
    //     i.Let descObj be Get( props, nextKey).
    //     ii.ReturnIfAbrupt(descObj).
    //     iii.Let desc be ToPropertyDescriptor(descObj).
    //     iv.ReturnIfAbrupt(desc).
    //     v.Append the pair (a two element List) consisting of nextKey and desc to the end of descriptors.
    JSMutableHandle<JSTaggedValue> handle_key(thread, JSTaggedValue::Undefined());
    for (uint32_t i = 0; i < length; i++) {
        PropertyDescriptor prop_desc(thread);
        handle_key.Update(handle_keys->Get(i));

        bool success =
            JSTaggedValue::GetOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(props), handle_key, prop_desc);
        // ReturnIfAbrupt(propDesc)
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

        if (success && prop_desc.IsEnumerable()) {
            JSHandle<JSTaggedValue> desc_obj =
                JSTaggedValue::GetProperty(thread, JSHandle<JSTaggedValue>::Cast(props), handle_key).GetValue();
            // ReturnIfAbrupt(descObj)
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            PropertyDescriptor desc(thread);
            JSObject::ToPropertyDescriptor(thread, desc_obj, desc);

            // ReturnIfAbrupt(desc)
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            // 8.For each pair from descriptors in list order,
            //   a.Let P be the first element of pair.
            //   b.Let desc be the second element of pair.
            //   c.Let status be DefinePropertyOrThrow(O,P, desc).
            //   d.ReturnIfAbrupt(status).
            [[maybe_unused]] bool set_success = JSTaggedValue::DefinePropertyOrThrow(thread, obj, handle_key, desc);

            // ReturnIfAbrupt(status)
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
    }

    // 9.Return O.
    return obj.GetTaggedValue();
}

// 19.1.2.2 Object.create ( O [ , Properties ] )
JSTaggedValue object::Create(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Create);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1.If Type(O) is neither Object nor Null, throw a TypeError exception.
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    if (!obj->IsECMAObject() && !obj->IsNull()) {
        // throw a TypeError exception
        THROW_TYPE_ERROR_AND_RETURN(thread, "Create: O is neither Object nor Null", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> properties = builtins_common::GetCallArg(argv, 1);

    // 2.Let obj be ObjectCreate(O).
    JSHandle<JSObject> obj_create = thread->GetEcmaVM()->GetFactory()->OrdinaryNewJSObjectCreate(obj);

    // 3.If the argument Properties is present and not undefined, then
    //   a.Return ObjectDefineProperties(obj, Properties).
    if (!properties->IsUndefined()) {
        return ObjectDefineProperties(thread, JSHandle<JSTaggedValue>::Cast(obj_create), properties);
    }

    // 4.Return obj.
    return obj_create.GetTaggedValue();
}

// 19.1.2.3 Object.defineProperties ( O, Properties )
JSTaggedValue object::DefineProperties(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, DefineProperties);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1.Return ObjectDefineProperties(O, Properties).
    return ObjectDefineProperties(thread, builtins_common::GetCallArg(argv, 0), builtins_common::GetCallArg(argv, 1));
}

// 19.1.2.4 Object.defineProperty ( O, P, Attributes )
JSTaggedValue object::DefineProperty(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, DefineProperty);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1.If Type(O) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    if (!obj->IsECMAObject()) {
        // throw a TypeError
        THROW_TYPE_ERROR_AND_RETURN(thread, "DefineProperty: O is not Object", JSTaggedValue::Exception());
    }

    // 2.Let key be ToPropertyKey(P).
    JSHandle<JSTaggedValue> prop = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, prop);

    // 3.ReturnIfAbrupt(key).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 4.Let desc be ToPropertyDescriptor(Attributes).
    PropertyDescriptor desc(thread);
    JSObject::ToPropertyDescriptor(thread, builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD),
                                   desc);

    // 5.ReturnIfAbrupt(desc).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6.Let success be DefinePropertyOrThrow(O,key, desc).
    [[maybe_unused]] bool success = JSTaggedValue::DefinePropertyOrThrow(thread, obj, key, desc);

    // 7.ReturnIfAbrupt(success).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 8.Return O.
    return obj.GetTaggedValue();
}

// 19.1.2.5 Object.freeze ( O )
JSTaggedValue object::Freeze(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Freeze);

    // 1.If Type(O) is not Object, return O.
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    if (!obj->IsECMAObject()) {
        return obj.GetTaggedValue();
    }

    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 2.Let status be SetIntegrityLevel( O, "frozen").
    bool status = JSObject::SetIntegrityLevel(thread, JSHandle<JSObject>(obj), IntegrityLevel::FROZEN);

    // 3.ReturnIfAbrupt(status).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4.If status is false, throw a TypeError exception.
    if (!status) {
        // throw a TypeError exception
        THROW_TYPE_ERROR_AND_RETURN(thread, "Freeze: freeze failed", JSTaggedValue::Exception());
    }

    // 5.Return O.
    return obj.GetTaggedValue();
}

// ES2021 20.1.2.7 Object.fromEntries ( iterable )
JSTaggedValue object::FromEntries(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, SetPrototypeOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Perform ? RequireObjectCoercible(iterable).
    JSHandle<JSTaggedValue> iterable = builtins_common::GetCallArg(argv, 0);
    JSTaggedValue::RequireObjectCoercible(thread, iterable);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Let obj be ! OrdinaryObjectCreate(%Object.prototype%).
    JSHandle<JSTaggedValue> constructor(thread->GetEcmaVM()->GetGlobalEnv()->GetObjectFunction());
    JSHandle<JSObject> obj(
        thread->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor));

    // 3. Assert: obj is an extensible ordinary object with no own properties.
    ASSERT(obj->IsExtensible());

    // 4. Let stepsDefine be the algorithm steps defined in CreateDataPropertyOnObject Functions.
    EcmaEntrypoint steps_define = base::ObjectHelper::CreateDataPropertyOnObject;

    // 5. Let lengthDefine be the number of non-optional parameters of the function definition in
    size_t length_define = 2;
    // CreateDataPropertyOnObject Functions.
    // 6. Let adder be ! CreateBuiltinFunction(stepsDefine, lengthDefine, "", « »).

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSFunction> adder = factory->CreateBuiltinFunction(
        steps_define, length_define, JSHandle<JSTaggedValue>(factory->NewFromString("")),
        JSHandle<JSTaggedValue>(thread, JSTaggedValue::Undefined()), thread->GlobalConstants()->GetHandledUndefined());

    // 7. Return ? AddEntriesFromIterable(obj, iterable, adder).
    JSHandle<JSTaggedValue> adder_handler(thread, adder.GetTaggedValue());
    JSTaggedValue result =
        base::ObjectHelper::AddEntriesFromIterable(thread, JSHandle<JSTaggedValue>(obj), iterable, adder_handler);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, result);
    return result;
}

// 19.1.2.6 Object.getOwnPropertyDescriptor ( O, P )
JSTaggedValue object::GetOwnPropertyDescriptor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, GetOwnPropertyDescriptor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1.Let obj be ToObject(O).
    JSHandle<JSTaggedValue> func = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> handle = JSTaggedValue::ToObject(thread, func);

    // 2.ReturnIfAbrupt(obj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3.Let key be ToPropertyKey(P).
    JSHandle<JSTaggedValue> prop = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, prop);

    // 4.ReturnIfAbrupt(key).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5.Let desc be obj.[[GetOwnProperty]](key).
    PropertyDescriptor desc(thread);
    JSTaggedValue::GetOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(handle), key, desc);

    // 6.ReturnIfAbrupt(desc).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 7.Return FromPropertyDescriptor(desc).
    JSHandle<JSTaggedValue> res = JSObject::FromPropertyDescriptor(thread, desc);
    return res.GetTaggedValue();
}

// ES2021 20.1.2.9 Object.getOwnPropertyDescriptors ( O )
JSTaggedValue object::GetOwnPropertyDescriptors(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, GetOwnPropertyDescriptor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let obj be ? ToObject(O).
    JSHandle<JSTaggedValue> func = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, func);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Let ownKeys  be ? obj.[[OwnPropertyKeys]]().
    JSHandle<TaggedArray> own_keys = JSTaggedValue::GetOwnPropertyKeys(thread, JSHandle<JSTaggedValue>::Cast(obj));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Let obj be ! OrdinaryObjectCreate(%Object.prototype%).
    JSHandle<JSTaggedValue> constructor(thread->GetEcmaVM()->GetGlobalEnv()->GetObjectFunction());
    JSHandle<JSObject> descriptors(
        thread->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor));

    // 4. For each element key of ownKeys, do
    for (ArraySizeT i = 0; i < own_keys->GetLength(); i++) {
        // a. Let desc be ? obj.[[GetOwnProperty]](key).
        JSHandle<JSTaggedValue> key(thread, own_keys->Get(i));
        PropertyDescriptor desc(thread);
        JSTaggedValue::GetOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(obj), key, desc);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

        // b. Let descriptor be ! FromPropertyDescriptor(desc).
        JSHandle<JSTaggedValue> descriptor = JSObject::FromPropertyDescriptor(thread, desc);

        // c. If descriptor is not undefined, perform ! CreateDataPropertyOrThrow(descriptors, key, descriptor).
        if (!descriptor->IsUndefined()) {
            JSObject::CreateDataPropertyOrThrow(thread, descriptors, key, descriptor);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
    }

    // 5. return descriptors
    return descriptors.GetTaggedValue();
}

// Runtime Semantics
JSTaggedValue GetOwnPropertyKeys(JSThread *thread, const JSHandle<JSTaggedValue> &object, const KeyType &type)
{
    BUILTINS_API_TRACE(thread, Object, GetOwnPropertyKeys);
    // 1.Let obj be ToObject(O).
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, object);

    // 2.ReturnIfAbrupt(obj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3.Let keys be obj.[[OwnPropertyKeys]]().
    JSHandle<TaggedArray> handle_keys = JSTaggedValue::GetOwnPropertyKeys(thread, JSHandle<JSTaggedValue>::Cast(obj));

    // 4.ReturnIfAbrupt(keys).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5.Let nameList be a new empty List.
    // new an empty array and append
    uint32_t length = handle_keys->GetLength();
    JSHandle<TaggedArray> name_list = factory->NewTaggedArray(length);

    // 6.Repeat for each element nextKey of keys in List order,
    uint32_t copy_length = 0;
    switch (type) {
        case KeyType::STRING_TYPE: {
            for (uint32_t i = 0; i < length; i++) {
                JSTaggedValue key = handle_keys->Get(i);
                if (key.IsString()) {
                    name_list->Set(thread, copy_length, key);
                    copy_length++;
                }
            }
            break;
        }
        case KeyType::SYMBOL_TYPE: {
            for (uint32_t i = 0; i < length; i++) {
                JSTaggedValue key = handle_keys->Get(i);
                if (key.IsSymbol()) {
                    name_list->Set(thread, copy_length, key);
                    copy_length++;
                }
            }
            break;
        }
        default:
            break;
    }

    // 7.Return CreateArrayFromList(nameList).
    JSHandle<TaggedArray> result_list = factory->CopyArray(name_list, length, copy_length);
    JSHandle<JSArray> result_array = JSArray::CreateArrayFromList(thread, result_list);
    return result_array.GetTaggedValue();
}

// 19.1.2.7 Object.getOwnPropertyNames ( O )
JSTaggedValue object::GetOwnPropertyNames(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, GetOwnPropertyNames);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    KeyType type = KeyType::STRING_TYPE;

    // 1.Return GetOwnPropertyKeys(O, String).
    return GetOwnPropertyKeys(argv->GetThread(), obj, type);
}

// 19.1.2.8 Object.getOwnPropertySymbols ( O )
JSTaggedValue object::GetOwnPropertySymbols(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, GetOwnPropertySymbols);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    KeyType type = KeyType::SYMBOL_TYPE;

    // 1.Return GetOwnPropertyKeys(O, Symbol).
    return GetOwnPropertyKeys(argv->GetThread(), obj, type);
}

// 19.1.2.9 Object.getPrototypeOf ( O )
JSTaggedValue object::GetPrototypeOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, GetPrototypeOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1.Let obj be ToObject(O).
    JSHandle<JSTaggedValue> func = builtins_common::GetCallArg(argv, 0);

    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, func);

    // 2.ReturnIfAbrupt(obj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3.Return obj.[[GetPrototypeOf]]().
    return obj->GetPrototype(thread);
}

// 19.1.2.10 Object.is ( value1, value2 )
JSTaggedValue object::Is(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Is);

    // 1.Return SameValue(value1, value2).
    bool result = JSTaggedValue::SameValue(builtins_common::GetCallArg(argv, 0), builtins_common::GetCallArg(argv, 1));
    return builtins_common::GetTaggedBoolean(result);
}

// 19.1.2.11 Object.isExtensible ( O )
JSTaggedValue object::IsExtensible(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    // 1.If Type(O) is not Object, return false.
    JSTaggedValue obj = builtins_common::GetCallArg(argv, 0).GetTaggedValue();
    if (!obj.IsObject()) {
        return builtins_common::GetTaggedBoolean(false);
    }
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 2.Return IsExtensible(O).
    return builtins_common::GetTaggedBoolean(obj.IsExtensible(thread));
}

// 19.1.2.12 Object.isFrozen ( O )
JSTaggedValue object::IsFrozen(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    // 1.If Type(O) is not Object, return true.
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    if (!obj->IsECMAObject()) {
        return builtins_common::GetTaggedBoolean(true);
    }

    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 2.Return TestIntegrityLevel(O, "frozen").
    bool status = JSObject::TestIntegrityLevel(thread, JSHandle<JSObject>(obj), IntegrityLevel::FROZEN);
    return builtins_common::GetTaggedBoolean(status);
}

// 19.1.2.13 Object.isSealed ( O )
JSTaggedValue object::IsSealed(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);

    // 1.If Type(O) is not Object, return true.
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    if (!obj->IsECMAObject()) {
        return builtins_common::GetTaggedBoolean(true);
    }

    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 2.Return TestIntegrityLevel(O, "sealed").
    bool status = JSObject::TestIntegrityLevel(thread, JSHandle<JSObject>(obj), IntegrityLevel::SEALED);
    return builtins_common::GetTaggedBoolean(status);
}

// 19.1.2.14 Object.keys(O)
JSTaggedValue object::Keys(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Keys);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let obj be ? ToObject(O).
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);

    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, msg);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Let nameList be ? EnumerableOwnPropertyNames(obj, key)
    ArraySizeT count = 0;
    JSHandle<TaggedArray> name_list = JSObject::EnumerableOwnPropertyNames(thread, obj, PropertyKind::KEY, &count);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Return CreateArrayFromList(nameList).
    JSHandle<JSArray> result = JSArray::CreateArrayFromList(thread, name_list, count);
    return result.GetTaggedValue();
}

// 19.1.2.15 Object.preventExtensions(O)
JSTaggedValue object::PreventExtensions(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, PreventExtensions);
    // 1. If Type(O) is not Object, return O.
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    if (!obj->IsECMAObject()) {
        return obj.GetTaggedValue();
    }
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    // 2. Let status be O.[[PreventExtensions]]().
    bool status = JSTaggedValue::PreventExtensions(argv->GetThread(), obj);

    // 3. ReturnIfAbrupt(status).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(argv->GetThread());

    // 4. If status is false, throw a TypeError exception.
    if (!status) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "PreventExtensions: preventExtensions failed",
                                    JSTaggedValue::Exception());
    }

    // 5. Return O.
    return obj.GetTaggedValue();
}
// 19.1.2.16 Object.prototype

// 19.1.2.17 Object.seal(O)
JSTaggedValue object::Seal(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Seal);

    // 1. If Type(O) is not Object, return O.
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    if (!msg->IsECMAObject()) {
        return msg.GetTaggedValue();
    }

    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 2. Let status be SetIntegrityLevel(O, "sealed").
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, msg);
    bool status = JSObject::SetIntegrityLevel(thread, object, IntegrityLevel::SEALED);

    // 3. ReturnIfAbrupt(status).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4. If status is false, throw a TypeError exception.
    if (!status) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "Seal: seal failed", JSTaggedValue::Exception());
    }

    // 5. Return O.
    return object.GetTaggedValue();
}

// 19.1.2.18 Object.setPrototypeOf(O, proto)
JSTaggedValue object::SetPrototypeOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, SetPrototypeOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be RequireObjectCoercible(O).
    JSHandle<JSTaggedValue> object =
        JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetCallArg(argv, 0));

    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. If Type(proto) is neither Object nor Null, throw a TypeError exception.
    JSHandle<JSTaggedValue> proto = builtins_common::GetCallArg(argv, 1);
    if (!proto->IsNull() && !proto->IsECMAObject()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "SetPrototypeOf: proto is neither Object nor Null",
                                    JSTaggedValue::Exception());
    }

    // 4. If Type(O) is not Object, return O.
    if (!object->IsECMAObject()) {
        return object.GetTaggedValue();
    }

    // 5. Let status be O.[[SetPrototypeOf]](proto).
    bool status = JSTaggedValue::SetPrototype(thread, object, proto);

    // 6. ReturnIfAbrupt(status).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 7. If status is false, throw a TypeError exception.
    if (!status) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "SetPrototypeOf: prototype set failed", JSTaggedValue::Exception());
    }

    // 8. Return O.
    return object.GetTaggedValue();
}

// 19.1.3.1 Object.prototype.constructor

// 19.1.3.2 Object.prototype.hasOwnProperty(V)
JSTaggedValue object::proto::HasOwnProperty(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, HasOwnProperty);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let P be ToPropertyKey(V).
    JSHandle<JSTaggedValue> prop = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> property = JSTaggedValue::ToPropertyKey(thread, prop);

    // 2. ReturnIfAbrupt(P).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Let O be ToObject(this value).
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));

    // 4. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Return HasOwnProperty(O, P).
    bool res = JSTaggedValue::HasOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(object), property);
    return builtins_common::GetTaggedBoolean(res);
}

// 19.1.3.3 Object.prototype.isPrototypeOf(V)
JSTaggedValue object::proto::IsPrototypeOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, IsPrototypeOf);
    JSThread *thread = argv->GetThread();
    // 1. If Type(V) is not Object, return false.
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    if (!msg->IsECMAObject()) {
        return builtins_common::GetTaggedBoolean(false);
    }
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 2. Let O be ToObject(this value).
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    // 3. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4. Repeat
    //    a. Let V be V.[[GetPrototypeOf]]().
    //    b. If V is null, return false
    //    c. If SameValue(O, V) is true, return true.
    JSTaggedValue msg_value = msg.GetTaggedValue();
    while (!msg_value.IsNull()) {
        if (JSTaggedValue::SameValue(object.GetTaggedValue(), msg_value)) {
            return builtins_common::GetTaggedBoolean(true);
        }
        msg_value = JSObject::Cast(msg_value)->GetPrototype(thread);
    }
    return builtins_common::GetTaggedBoolean(false);
}

// 19.1.3.4 Object.prototype.propertyIsEnumerable(V)
JSTaggedValue object::proto::PropertyIsEnumerable(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    // 1. Let P be ToPropertyKey(V).
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> property = JSTaggedValue::ToPropertyKey(thread, msg);

    // 2. ReturnIfAbrupt(P).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Let O be ToObject(this value).
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    // 4. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Let desc be O.[[GetOwnProperty]](P).
    PropertyDescriptor desc(thread);
    JSTaggedValue::GetOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(object), property, desc);

    // 6. ReturnIfAbrupt(desc).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 7. If desc is undefined, return false.
    if (desc.IsEmpty()) {
        return builtins_common::GetTaggedBoolean(false);
    }

    // 8. Return the value of desc.[[Enumerable]].
    return builtins_common::GetTaggedBoolean(desc.IsEnumerable());
}

// 19.1.3.5 Object.prototype.toLocaleString([reserved1[, reserved2]])
JSTaggedValue object::proto::ToLocaleString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, ToLocaleString);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> object = builtins_common::GetThis(argv);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(argv->GetThread());

    // 2. Return Invoke(O, "toString").
    JSHandle<JSTaggedValue> callee_key = thread->GlobalConstants()->GetHandledToStringString();

    auto info = NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), object, JSTaggedValue::Undefined(),
                                   argv->GetArgsNumber());
    if (argv->GetArgsNumber() > 0) {
        info->SetCallArg(argv->GetArgsNumber(),
                         reinterpret_cast<JSTaggedType *>(argv->GetArgAddress(js_method_args::NUM_MANDATORY_ARGS)));
    }
    return JSFunction::Invoke(info.Get(), callee_key);
}

JSTaggedValue GetBuiltinTag(JSThread *thread, const JSHandle<JSObject> &object)
{
    BUILTINS_API_TRACE(thread, Object, GetBuiltinTag);
    // 4. Let isArray be IsArray(O).
    bool is_array = object->IsJSArray();
    // 5. ReturnIfAbrupt(isArray).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> builtin_tag = factory->NewFromCanBeCompressString("Object");
    // 6. If isArray is true, let builtinTag be "Array".
    if (is_array) {
        builtin_tag = factory->NewFromCanBeCompressString("Array");
    } else if (object->IsJSPrimitiveRef()) {
        // 7. Else, if O is an exotic String object, let builtinTag be "String".
        JSPrimitiveRef *primitive_ref = JSPrimitiveRef::Cast(*object);
        if (primitive_ref->IsString()) {
            builtin_tag = factory->NewFromCanBeCompressString("String");
        } else if (primitive_ref->IsBoolean()) {
            // 11. Else, if O has a [[BooleanData]] internal slot, let builtinTag be "Boolean".
            builtin_tag = factory->NewFromCanBeCompressString("Boolean");
        } else if (primitive_ref->IsNumber()) {
            // 12. Else, if O has a [[NumberData]] internal slot, let builtinTag be "Number".
            builtin_tag = factory->NewFromCanBeCompressString("Number");
        }
    } else if (object->IsArguments()) {
        builtin_tag = factory->NewFromCanBeCompressString("Arguments");
    } else if (object->IsCallable()) {
        builtin_tag = factory->NewFromCanBeCompressString("Function");
    } else if (object->IsJSError()) {
        builtin_tag = factory->NewFromCanBeCompressString("Error");
    } else if (object->IsDate()) {
        builtin_tag = factory->NewFromCanBeCompressString("Date");
    } else if (object->IsJSRegExp()) {
        builtin_tag = factory->NewFromCanBeCompressString("RegExp");
    }
    // 15. Else, let builtinTag be "Object".
    return builtin_tag.GetTaggedValue();
}

// 19.1.3.6 Object.prototype.toString()
JSTaggedValue object::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, ToString);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If the this value is undefined, return "[object Undefined]".

    JSHandle<JSTaggedValue> msg = builtins_common::GetThis(argv);
    if (msg->IsUndefined()) {
        return builtins_common::GetTaggedString(thread, "[object Undefined]");
    }
    // 2. If the this value is null, return "[object Null]".
    if (msg->IsNull()) {
        return builtins_common::GetTaggedString(thread, "[object Null]");
    }

    // 3. Let O be ToObject(this value).
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> builtin_tag(thread, GetBuiltinTag(thread, object));

    // 16. Let tag be Get (O, @@toStringTag).
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    auto factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> tag = JSTaggedValue::GetProperty(thread, msg, env->GetToStringTagSymbol()).GetValue();

    // 17. ReturnIfAbrupt(tag).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 18. If Type(tag) is not String, let tag be builtinTag.
    if (!tag->IsString()) {
        tag = builtin_tag;
    }

    // 19. Return the String that is the result of concatenating "[object ", tag, and "]".
    JSHandle<EcmaString> left_string(factory->NewFromCanBeCompressString("[object "));
    JSHandle<EcmaString> right_string(factory->NewFromCanBeCompressString("]"));

    JSHandle<EcmaString> new_left_string_handle =
        factory->ConcatFromString(left_string, JSTaggedValue::ToString(thread, tag));
    auto result = factory->ConcatFromString(new_left_string_handle, right_string);
    return result.GetTaggedValue();
}

// 19.1.3.7 Object.prototype.valueOf()
JSTaggedValue object::proto::ValueOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, ValueOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Return ToObject(this value).
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return object.GetTaggedValue();
}
// B.2.2.1 Object.prototype.__proto__
// NOLINTNEXLINE(readability-identifier-naming)
JSTaggedValue object::proto::Get__proto__(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, Get__proto__);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1.Let obj be ToObject(this value).
    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));

    // 2.ReturnIfAbrupt(obj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3.Return obj.[[GetPrototypeOf]]().
    return obj->GetPrototype(thread);
}

// NOLINTNEXLINE(readability-identifier-naming)
JSTaggedValue object::proto::Set__proto__(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, Set__proto__);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be RequireObjectCoercible(this value).
    JSHandle<JSTaggedValue> obj = JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv));

    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. If Type(proto) is neither Object nor Null, return undefined..
    JSHandle<JSTaggedValue> proto = builtins_common::GetCallArg(argv, 0);
    if (!proto->IsNull() && !proto->IsECMAObject()) {
        return JSTaggedValue::Undefined();
    }

    // 4. If Type(O) is not Object, return undefined.
    if (!obj->IsECMAObject()) {
        return JSTaggedValue::Undefined();
    }

    // 5. Let status be O.[[SetPrototypeOf]](proto).
    bool status = JSTaggedValue::SetPrototype(thread, obj, proto);

    // 6. ReturnIfAbrupt(status).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 7. If status is false, throw a TypeError exception.
    if (!status) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "ProtoSetter: proto set failed", JSTaggedValue::Exception());
    }

    // 8. Return O.
    return JSTaggedValue::Undefined();
}

// B.2.2.2 Object.prototype.__defineGetter__ ( P, getter )
// NOLINTNEXLINE(readability-identifier-naming)
JSTaggedValue object::proto::__defineGetter__(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, __defineGetter__);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ? ToObject(this value).
    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. If IsCallable(getter) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> getter = builtins_common::GetCallArg(argv, 1);
    if (!getter->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "invalid getter usage", JSTaggedValue::Exception());
    }

    // 3. Let desc be PropertyDescriptor { [[Get]]: getter, [[Enumerable]]: true, [[Configurable]]: true }.
    PropertyDescriptor descriptor(thread);
    descriptor.SetGetter(getter);
    descriptor.SetEnumerable(true);
    descriptor.SetConfigurable(true);

    // 4. Let key be ? ToPropertyKey(P).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 0));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Perform ? DefinePropertyOrThrow(O, key, desc).
    JSTaggedValue::DefinePropertyOrThrow(thread, JSHandle<JSTaggedValue>::Cast(obj), key, descriptor);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. Return undefined.
    return JSTaggedValue::Undefined();
}

// B.2.2.3 Object.prototype.__defineSetter__ ( P, setter )
// NOLINTNEXLINE(readability-identifier-naming)
JSTaggedValue object::proto::__defineSetter__(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ObjectPrototype, __defineSetter__);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ? ToObject(this value).
    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. If IsCallable(setter) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> setter = builtins_common::GetCallArg(argv, 1);
    if (!setter->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "invalid setter usage", JSTaggedValue::Exception());
    }

    // 3. Let desc be PropertyDescriptor { [[Set]]: setter, [[Enumerable]]: true, [[Configurable]]: true }.
    PropertyDescriptor descriptor(thread);
    descriptor.SetSetter(setter);
    descriptor.SetEnumerable(true);
    descriptor.SetConfigurable(true);

    // 4. Let key be ? ToPropertyKey(P).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 0));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Perform ? DefinePropertyOrThrow(O, key, desc).
    JSTaggedValue::DefinePropertyOrThrow(thread, JSHandle<JSTaggedValue>::Cast(obj), key, descriptor);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. Return undefined.
    return JSTaggedValue::Undefined();
}

static JSTaggedValue LookupDesc(EcmaRuntimeCallInfo *argv,
                                const std::function<JSTaggedValue(PropertyDescriptor)> &get_desc)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, LookupDesc);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ? ToObject(this value).
    JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, ecmascript::builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Let key be ? ToPropertyKey(P).
    JSHandle<JSTaggedValue> key =
        JSTaggedValue::ToPropertyKey(thread, ecmascript::builtins_common::GetCallArg(argv, 0));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Repeat,
    //    a. Let desc be ? O.[[GetOwnProperty]](key).
    //    b. If desc is not undefined, then
    //        i.  If IsAccessorDescriptor(desc) is true, return desc.[[Get]].
    //        ii. Return undefined.
    //    c. Set O to ? O.[[GetPrototypeOf]]().
    //    d. If O is null, return undefined.
    JSMutableHandle<JSTaggedValue> objval(thread, obj.GetTaggedValue());
    while (!objval->IsNull()) {
        PropertyDescriptor desc(thread);
        JSTaggedValue::GetOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(obj), key, desc);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

        if (!desc.IsEmpty()) {
            return get_desc(desc);
        }

        objval.Update(JSObject::Cast(objval.GetTaggedValue())->GetPrototype(thread));
    }

    return JSTaggedValue::Undefined();
}

// B.2.2.4 Object.prototype.__lookupGetter__ ( P )
// NOLINTNEXLINE(readability-identifier-naming)
JSTaggedValue object::proto::__lookupGetter__(EcmaRuntimeCallInfo *argv)
{
    return LookupDesc(argv, [](PropertyDescriptor desc) {
        if (desc.IsAccessorDescriptor()) {
            return desc.GetGetter().GetTaggedValue();
        }

        return JSTaggedValue::Undefined();
    });
}

// B.2.2.5 Object.prototype.__lookupSetter__ ( P )
// NOLINTNEXLINE(readability-identifier-naming)
JSTaggedValue object::proto::__lookupSetter__(EcmaRuntimeCallInfo *argv)
{
    return LookupDesc(argv, [](PropertyDescriptor desc) {
        if (desc.IsAccessorDescriptor()) {
            return desc.GetSetter().GetTaggedValue();
        }

        return JSTaggedValue::Undefined();
    });
}

JSTaggedValue object::proto::CreateRealm(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSRealm> realm = factory->NewJSRealm();
    return realm.GetTaggedValue();
}

// 20.1.2.5 Object.entries ( O )
JSTaggedValue object::Entries(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Entries);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let obj be ? ToObject(O).
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, obj);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 2. Let nameList be ? EnumerableOwnPropertyNames(obj, key+value).
    ArraySizeT length = 0;
    JSHandle<TaggedArray> name_list =
        JSObject::EnumerableOwnPropertyNames(thread, object, PropertyKind::KEY_VALUE, &length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return CreateArrayFromList(nameList).
    return JSArray::CreateArrayFromList(thread, name_list, length).GetTaggedValue();
}

// ES2021 20.1.2.22
JSTaggedValue object::Values(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Object, Values);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let obj be ? ToObject(O).
    JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> object = JSTaggedValue::ToObject(thread, obj);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 2. Let nameList be ? EnumerableOwnPropertyNames(obj, value).
    ArraySizeT length = 0;
    JSHandle<TaggedArray> name_list =
        JSObject::EnumerableOwnPropertyNames(thread, object, PropertyKind::VALUE, &length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return CreateArrayFromList(nameList).
    return JSArray::CreateArrayFromList(thread, name_list, length).GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
