/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_date.h"
#include "plugins/ecmascript/runtime/js_date_time_format.h"
#include "plugins/ecmascript/runtime/js_intl.h"

namespace panda::ecmascript::builtins {

// 13.1.5 DateTime Format Functions
JSTaggedValue AnonymousDateTimeFormat(EcmaRuntimeCallInfo *argv);

// 13.2.1 Intl.DateTimeFormat ( [ locales [ , options ] ] )
JSTaggedValue date_time_format::DateTimeFormatConstructor(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. If NewTarget is undefined, let new_target be the active function object, else let new_target be NewTarget.
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        new_target = constructor;
    }

    // 2. Let dateTimeFormat be ? OrdinaryCreateFromConstructor(new_target, "%DateTimeFormatPrototype%", «
    //    [[InitializedDateTimeFormat]], [[Locale]], [[Calendar]], [[NumberingSystem]], [[TimeZone]], [[Weekday]],
    //    [[Era]], [[Year]], [[Month]], [[Day]], [[Hour]], [[Minute]], [[Second]], [[TimeZoneName]], [[HourCycle]],
    //    [[Pattern]], [[BoundFormat]] »).
    JSHandle<JSDateTimeFormat> date_time_format = JSHandle<JSDateTimeFormat>::Cast(
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Perform ? InitializeDateTimeFormat(dateTimeFormat, locales, options).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSDateTimeFormat> dtf =
        JSDateTimeFormat::InitializeDateTimeFormat(thread, date_time_format, locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4. Let this be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 5. If newTarget is undefined and ? OrdinaryHasInstance(%DateTimeFormat%, this) is true, then
    //    a. Perform ? DefinePropertyOrThrow(this, %Intl%.[[FallbackSymbol]], PropertyDescriptor{ [[Value]]:
    //       dateTimeFormat, [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: false }).
    //    b. Return this.
    bool is_instance_of = JSFunction::OrdinaryHasInstance(thread, env->GetDateTimeFormatFunction(), this_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (new_target->IsUndefined() && this_value->IsJSObject() && is_instance_of) {
        PropertyDescriptor descriptor(thread, JSHandle<JSTaggedValue>::Cast(dtf), false, false, false);
        JSHandle<JSTaggedValue> key(thread, JSHandle<JSIntl>::Cast(env->GetIntlFunction())->GetFallbackSymbol());
        JSTaggedValue::DefinePropertyOrThrow(thread, this_value, key, descriptor);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        return this_value.GetTaggedValue();
    }

    // 6. Return dateTimeFormat.
    return dtf.GetTaggedValue();
}

// 13.3.2 Intl.DateTimeFormat.supportedLocalesOf ( locales [ , options ] )
JSTaggedValue date_time_format::SupportedLocalesOf(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let availableLocales be %DateTimeFormat%.[[AvailableLocales]].
    JSHandle<TaggedArray> available_locales = JSDateTimeFormat::GainAvailableLocales(thread);

    // 2. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Return ? SupportedLocales(availableLocales, requestedLocales, options).
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSArray> result = JSLocale::SupportedLocales(thread, available_locales, requested_locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 13.4.3 get Intl.DateTimeFormat.prototype.format
JSTaggedValue date_time_format::proto::GetFormat(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let dtf be this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 2. If Type(dtf) is not Object, throw a TypeError exception.
    if (!this_value->IsJSObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "dtf is not object", JSTaggedValue::Exception());
    }

    // 3. Let dtf be ? UnwrapDateTimeFormat(dtf).
    JSHandle<JSTaggedValue> dtf_value = JSDateTimeFormat::UnwrapDateTimeFormat(thread, this_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (dtf_value->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "dtfValue is not object", JSTaggedValue::Exception());
    }

    // 4. If dtf.[[BoundFormat]] is undefined, then
    //    a. Let F be a new built-in function object as defined in DateTime Format Functions (13.1.5).
    //    b. Set F.[[DateTimeFormat]] to dtf.
    //    c. Set dtf.[[BoundFormat]] to F.
    // 5. Return dtf.[[BoundFormat]].
    JSHandle<JSDateTimeFormat> dtf = JSHandle<JSDateTimeFormat>::Cast(dtf_value);
    JSHandle<JSTaggedValue> bound_format(thread, dtf->GetBoundFormat());
    if (bound_format->IsUndefined()) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        JSHandle<JSIntlBoundFunction> intl_bound_func =
            factory->NewJSIntlBoundFunction(reinterpret_cast<void *>(AnonymousDateTimeFormat));
        intl_bound_func->SetDateTimeFormat(thread, dtf);
        dtf->SetBoundFormat(thread, intl_bound_func);
    }
    return dtf->GetBoundFormat();
}

// 13.1.5 DateTime Format Functions
JSTaggedValue AnonymousDateTimeFormat(EcmaRuntimeCallInfo *argv)
{
    // A DateTime format function is an anonymous built-in function that has a [[DateTimeFormat]] internal slot.
    // When a DateTime format function F is called with optional argument date, the following steps are taken:
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSIntlBoundFunction> intl_bound_func =
        JSHandle<JSIntlBoundFunction>::Cast(builtins_common::GetConstructor(argv));

    // 1. Let dtf be F.[[DateTimeFormat]].
    JSHandle<JSTaggedValue> dtf(thread, intl_bound_func->GetDateTimeFormat());

    // 2. Assert: Type(dtf) is Object and dtf has an [[InitializedDateTimeFormat]] internal slot.
    ASSERT_PRINT(dtf->IsJSObject() && dtf->IsJSDateTimeFormat(), "dtf is not object or JSDateTimeFormat");

    // 3. If date is not provided or is undefined, then
    //    a. Let x be Call(%Date_now%, undefined).
    // 4. Else,
    //    a. Let x be ? ToNumber(date).
    JSHandle<JSTaggedValue> date = builtins_common::GetCallArg(argv, 0);
    double x = 0.0;
    if (date->IsUndefined()) {
        x = JSDate::Now().GetNumber();
    } else {
        JSTaggedNumber x_number = JSTaggedValue::ToNumber(thread, date);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        x = x_number.GetNumber();
    }

    // 5. Return ? FormatDateTime(dtf, x).
    JSHandle<EcmaString> result = JSDateTimeFormat::FormatDateTime(thread, JSHandle<JSDateTimeFormat>::Cast(dtf), x);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 13.4.4 Intl.DateTimeFormat.prototype.formatToParts ( date )
JSTaggedValue date_time_format::proto::FormatToParts(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let dtf be this value.
    JSHandle<JSTaggedValue> dtf = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(dtf, [[InitializedDateTimeFormat]]).
    if (!dtf->IsJSDateTimeFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "is not JSDateTimeFormat", JSTaggedValue::Exception());
    }

    // 3. If date is undefined, then
    //    a. Let x be Call(%Date_now%, undefined).
    // 4. Else,
    //    a. Let x be ? ToNumber(date).
    JSHandle<JSTaggedValue> date = builtins_common::GetCallArg(argv, 0);
    double x = 0.0;
    if (date->IsUndefined()) {
        x = JSDate::Now().GetNumber();
    } else {
        JSTaggedNumber x_number = JSTaggedValue::ToNumber(thread, date);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        x = x_number.GetNumber();
    }

    double x_value = JSDate::TimeClip(x);
    if (std::isnan(x_value)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Invalid time value", JSTaggedValue::Exception());
    }

    // 5. Return ? FormatDateTimeToParts(dtf, x).
    JSHandle<JSArray> result =
        JSDateTimeFormat::FormatDateTimeToParts(thread, JSHandle<JSDateTimeFormat>::Cast(dtf), x_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 13.4.5 Intl.DateTimeFormat.prototype.resolvedOptions ()
JSTaggedValue date_time_format::proto::ResolvedOptions(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let dtf be this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If Type(dtf) is not Object, throw a TypeError exception.
    if (!this_value->IsJSObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not object", JSTaggedValue::Exception());
    }
    // 3. Let dtf be ? UnwrapDateTimeFormat(dtf).
    this_value = JSDateTimeFormat::UnwrapDateTimeFormat(thread, this_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<JSTaggedValue> ctor = env->GetObjectFunction();
    JSHandle<JSObject> options(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), ctor));
    JSDateTimeFormat::ResolvedOptions(thread, JSHandle<JSDateTimeFormat>::Cast(this_value), options);
    // 6. Return options.
    return options.GetTaggedValue();
}

// Intl.DateTimeFormat.prototype.formatRange
JSTaggedValue date_time_format::proto::FormatRange(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let dtf be this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If Type(dtf) is not Object, throw a TypeError exception.
    if (!this_value->IsJSObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not object", JSTaggedValue::Exception());
    }

    // 3. If dtf does not have an [[InitializedDateTimeFormat]] internal slot, throw a TypeError exception.
    if (!this_value->IsJSDateTimeFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not JSDateTimeFormat", JSTaggedValue::Exception());
    }

    // 4. If startDate is undefined or endDate is undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> start_date = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> end_date = builtins_common::GetCallArg(argv, 1);
    if (start_date->IsUndefined() || end_date->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "startDate or endDate is undefined", JSTaggedValue::Exception());
    }

    // 5. Let x be ? ToNumber(startDate).
    JSTaggedNumber value_x = JSTaggedValue::ToNumber(thread, start_date);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double x = value_x.GetNumber();

    // 6. Let y be ? ToNumber(endDate).
    JSTaggedNumber value_y = JSTaggedValue::ToNumber(thread, end_date);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double y = value_y.GetNumber();
    // 7. If x is greater than y, throw a RangeError exception.
    if (x > y) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "x is greater than y", JSTaggedValue::Exception());
    }

    // 8. Return ? FormatDateTimeRange(dtf, x, y)
    JSHandle<JSDateTimeFormat> dtf = JSHandle<JSDateTimeFormat>::Cast(this_value);
    JSHandle<EcmaString> result = JSDateTimeFormat::NormDateTimeRange(thread, dtf, x, y);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// Intl.DateTimeFormat.prototype.formatRangeToParts
JSTaggedValue date_time_format::proto::FormatRangeToParts(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let dtf be this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If Type(dtf) is not Object, throw a TypeError exception.
    if (!this_value->IsJSObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not object", JSTaggedValue::Exception());
    }

    // 3. If dtf does not have an [[InitializedDateTimeFormat]] internal slot,
    //    throw a TypeError exception.
    if (!this_value->IsJSDateTimeFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not JSDateTimeFormat", JSTaggedValue::Exception());
    }

    // 4. If startDate is undefined or endDate is undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> start_date = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> end_date = builtins_common::GetCallArg(argv, 1);
    if (start_date->IsUndefined() || end_date->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "startDate or endDate is undefined", JSTaggedValue::Exception());
    }

    // 5. Let x be ? ToNumber(startDate).
    JSTaggedNumber value_x = JSTaggedValue::ToNumber(thread, start_date);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double x = value_x.GetNumber();

    // 6. Let y be ? ToNumber(endDate).
    JSTaggedNumber value_y = JSTaggedValue::ToNumber(thread, end_date);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double y = value_y.GetNumber();
    // 7. If x is greater than y, throw a RangeError exception.
    if (x > y) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "x is greater than y", JSTaggedValue::Exception());
    }

    // 8. Return ? FormatDateTimeRangeToParts(dtf, x, y)
    JSHandle<JSDateTimeFormat> dtf = JSHandle<JSDateTimeFormat>::Cast(this_value);
    JSHandle<JSArray> result = JSDateTimeFormat::NormDateTimeRangeToParts(thread, dtf, x, y);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
