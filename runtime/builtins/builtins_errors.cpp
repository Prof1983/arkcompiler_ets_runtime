/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/error_helper.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript::builtins {
using ErrorHelper = ecmascript::base::ErrorHelper;
using ErrorType = ecmascript::base::ErrorType;

// Error
// 19.5.1.1
JSTaggedValue error::ErrorConstructor(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonConstructor(argv, ErrorType::ERROR);
}

// 19.5.2.4
JSTaggedValue error::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonToString(argv, ErrorType::ERROR);
}

// 19.5.5.2
// RangeError
JSTaggedValue range_error::RangeErrorConstructor(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonConstructor(argv, ErrorType::RANGE_ERROR);
}

JSTaggedValue range_error::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonToString(argv, ErrorType::RANGE_ERROR);
}

// 19.5.5.3
// ReferenceError
JSTaggedValue reference_error::ReferenceErrorConstructor(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonConstructor(argv, ErrorType::REFERENCE_ERROR);
}

JSTaggedValue reference_error::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonToString(argv, ErrorType::REFERENCE_ERROR);
}

// 19.5.5.5
// TypeError
JSTaggedValue type_error::TypeErrorConstructor(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonConstructor(argv, ErrorType::TYPE_ERROR);
}

JSTaggedValue type_error::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonToString(argv, ErrorType::TYPE_ERROR);
}

JSTaggedValue type_error::ThrowTypeError(EcmaRuntimeCallInfo *argv)
{
    THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "type error", JSTaggedValue::Exception());
}

// 19.5.5.6
// URIError
JSTaggedValue uri_error::URIErrorConstructor(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonConstructor(argv, ErrorType::URI_ERROR);
}

JSTaggedValue uri_error::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonToString(argv, ErrorType::URI_ERROR);
}

// 19.5.5.4
// SyntaxError
JSTaggedValue syntax_error::SyntaxErrorConstructor(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonConstructor(argv, ErrorType::SYNTAX_ERROR);
}

JSTaggedValue syntax_error::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonToString(argv, ErrorType::SYNTAX_ERROR);
}

// 19.5.5.1
// EvalError
JSTaggedValue eval_error::EvalErrorConstructor(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonConstructor(argv, ErrorType::EVAL_ERROR);
}

JSTaggedValue eval_error::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    return ErrorHelper::ErrorCommonToString(argv, ErrorType::EVAL_ERROR);
}
}  // namespace panda::ecmascript::builtins
