/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/js_number_format.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_bigint.h"

namespace panda::ecmascript::builtins {

static JSTaggedValue ThisBigIntValue(EcmaRuntimeCallInfo *argv);

JSTaggedValue ThisBigIntValue(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    JSHandle<JSTaggedValue> value = builtins_common::GetThis(argv);
    // 1. If Type(value) is BigInt, return value.
    if (value->IsBigInt()) {
        return value.GetTaggedValue();
    }
    // 2. If Type(value) is Object and value has a [[BigIntData]] internal slot, then
    if (value->IsJSPrimitiveRef()) {
        JSTaggedValue primitive = JSPrimitiveRef::Cast(value->GetTaggedObject())->GetValue();
        // a. Assert: Type(value.[[BigIntData]]) is BigInt.
        if (primitive.IsBigInt()) {
            // b. Return value.[[BigIntData]].
            return primitive;
        }
    }
    // 3. Throw a TypeError exception.
    THROW_TYPE_ERROR_AND_RETURN(thread, "not BigInt type", JSTaggedValue::Exception());
}

JSTaggedValue big_int::BigIntConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, BigInt, BigIntConstructor);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    // 1. If NewTarget is not undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    if (!new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "BigInt is not a constructor", JSTaggedValue::Exception());
    }
    // 2. Let prim be ? ToPrimitive(value).
    JSHandle<JSTaggedValue> primitive(thread, JSTaggedValue::ToPrimitive(thread, value));
    // 3. If Type(prim) is Number, return ? NumberToBigInt(prim).
    if (primitive->IsNumber()) {
        return BigInt::NumberToBigInt(thread, primitive);
    }
    // 4. Otherwise, return ? ToBigInt(value).
    return JSTaggedValue::ToBigInt(thread, value);
}

JSTaggedValue big_int::AsUintN(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, BigInt, AsUintN);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> bits = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> bigint = builtins_common::GetCallArg(argv, 1);
    // 1. Let bits be ? ToIndex(bits).
    JSTaggedNumber index = JSTaggedValue::ToIndex(thread, bits);
    // 2. Let bigint be ? ToBigInt(bigint).
    JSTaggedValue js_bigint = JSTaggedValue::ToBigInt(thread, bigint);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<BigInt> js_bigint_val(thread, js_bigint);
    // 3. Return a BigInt representing bigint modulo 2bits.
    return BigInt::AsUintN(thread, index, js_bigint_val);
}

JSTaggedValue big_int::AsIntN(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, BigInt, AsIntN);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> bits = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> bigint = builtins_common::GetCallArg(argv, 1);
    // 1. Let bits be ? ToIndex(bits).
    JSTaggedNumber index = JSTaggedValue::ToIndex(thread, bits);
    // 2. Let bigint be ? ToBigInt(bigint).
    JSTaggedValue js_bigint = JSTaggedValue::ToBigInt(thread, bigint);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<BigInt> js_bigint_val(thread, js_bigint);
    // 3. Let mod be ℝ(bigint) modulo 2bits.
    // 4. If mod ≥ 2bits - 1, return ℤ(mod - 2bits); otherwise, return ℤ(mod).
    return BigInt::AsintN(thread, index, js_bigint_val);
}

JSTaggedValue big_int::proto::ToLocaleString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, BigIntPrototype, ToLocaleString);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let x be ? ThisBigIntValue(this value).
    JSTaggedValue value = ThisBigIntValue(argv);
    JSHandle<JSTaggedValue> this_val(thread, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Let numberFormat be ? Construct(%NumberFormat%, « locales, options »).
    JSHandle<JSTaggedValue> ctor = thread->GetEcmaVM()->GetGlobalEnv()->GetNumberFormatFunction();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), ctor);
    JSHandle<JSNumberFormat> number_format = JSHandle<JSNumberFormat>::Cast(obj);
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSNumberFormat::InitializeNumberFormat(thread, number_format, locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Return ? FormatNumeric(numberFormat, x).
    JSHandle<JSTaggedValue> result = JSNumberFormat::FormatNumeric(thread, number_format, this_val.GetTaggedValue());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

JSTaggedValue big_int::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, BigIntPrototype, ToString);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let x be ? thisBigIntValue(this value).
    JSTaggedValue value = ThisBigIntValue(argv);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<BigInt> this_bigint(thread, value);
    // 2. If radix is not present, let radixNumber be 10
    double radix = base::DECIMAL;
    JSHandle<JSTaggedValue> radix_value = builtins_common::GetCallArg(argv, 0);
    // 3. Else, let radixNumber be ? ToIntegerOrInfinity(radix).
    if (!radix_value->IsUndefined()) {
        JSTaggedNumber radix_number = JSTaggedValue::ToInteger(thread, radix_value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        radix = radix_number.GetNumber();
    }
    // 4. If radixNumber < 2 or radixNumber > 36, throw a RangeError exception.
    if (radix < base::MIN_RADIX || radix > base::MAX_RADIX) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "toString() radix argument must be between 2 and 36",
                                     JSTaggedValue::Exception());
    }
    // 5. If radixNumber = 10, return ToString(x).
    if (radix == base::DECIMAL) {
        return BigInt::ToString(thread, this_bigint).GetTaggedValue();
    }
    // 6. Return the String representation of this BigInt value using the radix specified by radixNumber
    return BigInt::ToString(thread, this_bigint, static_cast<int>(radix)).GetTaggedValue();
}

JSTaggedValue big_int::proto::ValueOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, BigIntPrototype, ValueOf);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let x be ? thisBigIntValue(this value).
    return ThisBigIntValue(argv);
}

}  // namespace panda::ecmascript::builtins
