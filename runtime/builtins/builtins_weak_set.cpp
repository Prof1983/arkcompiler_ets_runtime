/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_set_iterator.h"
#include "plugins/ecmascript/runtime/js_weak_container.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {
JSTaggedValue weak_set::WeakSetConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakSet, WeakSetConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1.If NewTarget is undefined, throw a TypeError exception
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        // throw type error
        THROW_TYPE_ERROR_AND_RETURN(thread, "new target can't be undefined", JSTaggedValue::Exception());
    }
    // 2.Let weakset be OrdinaryCreateFromConstructor(NewTarget, "%WeakSetPrototype%", «‍[[WeakSetData]]» ).
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target);
    // 3.returnIfAbrupt()
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSWeakSet> weak_set = JSHandle<JSWeakSet>::Cast(obj);
    // 3.ReturnIfAbrupt(weakSet).
    // 4.WeakSet set’s [[WeakSetData]] internal slot to a new empty List.
    JSHandle<LinkedHashSet> linked_set = LinkedHashSet::Create(thread, true);
    weak_set->SetLinkedSet(thread, linked_set);

    // add data into weakset from iterable
    // 5.If iterable is not present, let iterable be undefined.
    // 6.If iterable is either undefined or null, let iter be undefined.
    JSHandle<JSTaggedValue> iterable(builtins_common::GetCallArg(argv, 0));
    // 8.If iter is undefined, return weakset
    if (iterable->IsUndefined() || iterable->IsNull()) {
        return weak_set.GetTaggedValue();
    }
    // Let adder be Get(weakset, "add").
    JSHandle<JSTaggedValue> adder_key(factory->NewFromCanBeCompressString("add"));
    JSHandle<JSTaggedValue> weak_set_handle(weak_set);
    JSHandle<JSTaggedValue> adder = JSObject::GetProperty(thread, weak_set_handle, adder_key).GetValue();
    // ReturnIfAbrupt(adder).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, adder.GetTaggedValue());
    // If IsCallable(adder) is false, throw a TypeError exception
    if (!adder->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "adder is not callable", adder.GetTaggedValue());
    }
    // Let iter be GetIterator(iterable).
    JSHandle<JSTaggedValue> iter(JSIterator::GetIterator(thread, iterable));

    // ReturnIfAbrupt(iter).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, iter.GetTaggedValue());
    // values in iterator_result may be a JSArray, values[0] = key values[1]=value, used valueIndex to get value from
    // jsarray
    JSHandle<JSTaggedValue> value_index(thread, JSTaggedValue(1));
    JSHandle<JSTaggedValue> next = JSIterator::IteratorStep(thread, iter);
    JSMutableHandle<JSTaggedValue> status(thread, JSTaggedValue::Undefined());
    while (!next->IsFalse()) {
        // ReturnIfAbrupt(next).
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, next.GetTaggedValue());
        // Let nextValue be IteratorValue(next).
        JSHandle<JSTaggedValue> next_value(JSIterator::IteratorValue(thread, next));
        // ReturnIfAbrupt(nextValue).
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, next_value.GetTaggedValue());

        auto info = NewRuntimeCallInfo(thread, adder, weak_set, JSTaggedValue::Undefined(), 1);
        if (next_value->IsArray(thread)) {
            auto prop = JSObject::GetProperty(thread, next_value, value_index).GetValue();
            info->SetCallArgs(prop);
        } else {
            info->SetCallArgs(next_value);
        }
        JSTaggedValue ret = JSFunction::Call(info.Get());

        // Let status be Call(adder, weakset, «nextValue.[[value]]»).
        status.Update(ret);
        // If status is an abrupt completion, return IteratorClose(iter, status).
        if (UNLIKELY(thread->HasPendingException())) {
            return JSIterator::IteratorCloseAndReturn(thread, iter, status);
        }
        // Let next be IteratorStep(iter).
        next = JSIterator::IteratorStep(thread, iter);
    }
    return weak_set.GetTaggedValue();
}

JSTaggedValue weak_set::proto::Add(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakSetPrototype, Add);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self = builtins_common::GetThis(argv);

    // 2.If Type(S) is not Object, throw a TypeError exception.
    // 3.If S does not have a [[WeakSetData]] internal slot, throw a TypeError exception.
    if (!self->IsJSWeakSet()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakSet", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> value(builtins_common::GetCallArg(argv, 0));
    if (!value->IsHeapObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "value is not an object", JSTaggedValue::Exception());
    }
    if (value->IsSymbol() || value->IsString()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "value is Symblol or String", JSTaggedValue::Exception());
    }

    JSHandle<JSWeakSet> weak_set(thread, JSWeakSet::Cast(*JSTaggedValue::ToObject(thread, self)));

    JSWeakSet::Add(thread, weak_set, value);
    return weak_set.GetTaggedValue();
}

JSTaggedValue weak_set::proto::Delete(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakSetPrototype, Delete);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self = builtins_common::GetThis(argv);
    // 2.If Type(S) is not Object, throw a TypeError exception.
    // 3.If S does not have a [[WeakSetData]] internal slot, throw a TypeError exception.
    if (!self->IsJSWeakSet()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakSet", JSTaggedValue::Exception());
    }

    JSHandle<JSWeakSet> weak_set(thread, JSWeakSet::Cast(*JSTaggedValue::ToObject(thread, self)));
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    if (!value->IsHeapObject()) {
        builtins_common::GetTaggedBoolean(false);
    }
    return builtins_common::GetTaggedBoolean(JSWeakSet::Delete(thread, weak_set, value));
}

JSTaggedValue weak_set::proto::Has(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakSetPrototype, Has);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self = builtins_common::GetThis(argv);
    // 2.If Type(S) is not Object, throw a TypeError exception.
    // 3.If S does not have a [[SetData]] internal slot, throw a TypeError exception.
    if (!self->IsJSWeakSet()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakSet", JSTaggedValue::Exception());
    }
    JSHandle<JSWeakSet> js_weak_set(thread, JSWeakSet::Cast(*JSTaggedValue::ToObject(thread, self)));
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    if (!value->IsHeapObject()) {
        builtins_common::GetTaggedBoolean(false);
    }
    int hash = LinkedHash::Hash(value.GetTaggedValue());
    return builtins_common::GetTaggedBoolean(js_weak_set->Has(value.GetTaggedValue(), hash));
}
}  // namespace panda::ecmascript::builtins
