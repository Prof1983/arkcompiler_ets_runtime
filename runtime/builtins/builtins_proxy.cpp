/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript::builtins {

static JSTaggedValue InvalidateProxyFunction(EcmaRuntimeCallInfo *argv);

// 26.2.1.1 Proxy( [ value ] )
JSTaggedValue proxy::ProxyConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Proxy, ProxyConstructor);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());

    // 1.If NewTarget is undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "ProxyConstructor: NewTarget is undefined",
                                    JSTaggedValue::Exception());
    }

    // 2.Return ProxyCreate(target, handler).
    JSHandle<JSProxy> proxy = JSProxy::ProxyCreate(argv->GetThread(), builtins_common::GetCallArg(argv, 0),
                                                   builtins_common::GetCallArg(argv, 1));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(argv->GetThread());
    return proxy.GetTaggedValue();
}

// 26.2.2.1 Proxy.revocable ( target, handler )
JSTaggedValue proxy::Revocable([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Proxy, Revocable);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1.Let p be ProxyCreate(target, handler).
    JSHandle<JSProxy> proxy =
        JSProxy::ProxyCreate(thread, builtins_common::GetCallArg(argv, 0), builtins_common::GetCallArg(argv, 1));

    // 2.ReturnIfAbrupt(p).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3 ~ 4 new revoker function and set the [[RevocableProxy]] internal slot
    JSHandle<JSProxyRevocFunction> revoker = thread->GetEcmaVM()->GetFactory()->NewJSProxyRevocFunction(
        proxy, reinterpret_cast<void *>(InvalidateProxyFunction));

    // 5.Let result be ObjectCreate(%ObjectPrototype%).
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> proto = env->GetObjectFunctionPrototype();
    JSHandle<JSObject> result = thread->GetEcmaVM()->GetFactory()->OrdinaryNewJSObjectCreate(proto);

    // 6.Perform CreateDataProperty(result, "proxy", p).
    auto global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> proxy_key = global_const->GetHandledProxyString();
    JSObject::CreateDataProperty(thread, result, proxy_key, JSHandle<JSTaggedValue>(proxy));

    // 7.Perform CreateDataProperty(result, "revoke", revoker).
    JSHandle<JSTaggedValue> revoke_key = global_const->GetHandledRevokeString();
    JSObject::CreateDataProperty(thread, result, revoke_key, JSHandle<JSTaggedValue>(revoker));

    // 8.Return result.
    return result.GetTaggedValue();
}

// A Proxy revocation function to invalidate a specific Proxy object
JSTaggedValue InvalidateProxyFunction(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Proxy, InvalidateProxyFunction);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSObject> revoke_obj(builtins_common::GetThis(argv));
    JSHandle<JSTaggedValue> revoke_key = thread->GlobalConstants()->GetHandledRevokeString();

    PropertyDescriptor desc(thread);
    JSObject::GetOwnProperty(thread, revoke_obj, revoke_key, desc);
    JSProxyRevocFunction::ProxyRevocFunctions(thread, JSHandle<JSProxyRevocFunction>(desc.GetValue()));
    return JSTaggedValue::Undefined();
}
}  // namespace panda::ecmascript::builtins
