/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/string_helper.h"

namespace panda::ecmascript::builtins {
using StringHelper = base::StringHelper;

// Make sure the ECMASCRIPT_OBJECT_DUMP in config.h has been opened before use it
// Use through ArkTools.print(msg, [obj1, obj2, ... objn]) in js
JSTaggedValue ark_tools::Print(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<EcmaString> str = JSTaggedValue::ToString(thread, builtins_common::GetCallArg(argv, 0));
    // The default log level of ace_engine and js_runtime is error
    LOG(ERROR, RUNTIME) << ": " << base::StringHelper::ToStdString(*str);

    uint32_t num_args = argv->GetArgsNumber();
    for (uint32_t i = 1; i < num_args; i++) {
        JSHandle<JSTaggedValue> obj = builtins_common::GetCallArg(argv, i);
        std::ostringstream oss;
        obj->Dump(thread, oss);

        // The default log level of ace_engine and js_runtime is error
        LOG(ERROR, RUNTIME) << ": " << oss.str();
    }

    return JSTaggedValue::Undefined();
}
}  // namespace panda::ecmascript::builtins
