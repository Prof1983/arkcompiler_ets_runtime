/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript::builtins {
// ecma 26.1.1 Reflect.apply (target, thisArgument, argumentsList)
JSTaggedValue reflect::Apply(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, Apply);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If IsCallable(target) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.apply target is not callable", JSTaggedValue::Exception());
    }
    // 2. Let args be ? CreateListFromArrayLike(argumentsList).
    JSHandle<JSTaggedValue> this_argument = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSTaggedValue> arguments_list = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    JSHandle<JSTaggedValue> arg_or_abrupt = JSObject::CreateListFromArrayLike(thread, arguments_list);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<TaggedArray> args = JSHandle<TaggedArray>::Cast(arg_or_abrupt);

    // 3. Perform PrepareForTailCall().
    // 4. Return ? Call(target, thisArgument, args).
    const auto args_length = args->GetLength();
    JSHandle<JSTaggedValue> undefined = thread->GlobalConstants()->GetHandledUndefined();
    auto info = NewRuntimeCallInfo(thread, target, this_argument, undefined, args_length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    info->SetCallArg(args_length, args->GetData());
    return JSFunction::Call(info.Get());
}

// ecma 26.1.2 Reflect.construct (target, argumentsList [ , new_target])
JSTaggedValue reflect::Construct(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, Construct);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If IsConstructor(target) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.construct target is not constructor", JSTaggedValue::Exception());
    }
    // 2. If new_target is not present, set new_target to target.
    JSHandle<JSTaggedValue> new_target = argv->GetArgsNumber() > 2
                                             ? builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD)
                                             : target;  // 2: num args
    // 3. Else if IsConstructor(new_target) is false, throw a TypeError exception.
    if (!new_target->IsConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.construct new_target is present, but not constructor",
                                    JSTaggedValue::Exception());
    }
    // 4. Let args be ? CreateListFromArrayLike(argumentsList).
    JSHandle<JSTaggedValue> arguments_list = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSTaggedValue> arg_or_abrupt = JSObject::CreateListFromArrayLike(thread, arguments_list);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<TaggedArray> args = JSHandle<TaggedArray>::Cast(arg_or_abrupt);
    // 5. Return ? Construct(target, args, new_target).
    const auto args_length = args->GetLength();
    auto info = NewRuntimeCallInfo(thread, target, JSTaggedValue::Undefined(), new_target, args_length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    info->SetCallArg(args_length, args->GetData());
    return JSFunction::Construct(info.Get());
}

// ecma 26.1.3 Reflect.defineProperty (target, propertyKey, attributes)
JSTaggedValue reflect::DefineProperty(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, DefineProperty);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.defineProperty target is not object", JSTaggedValue::Exception());
    }
    // 2. Let key be ? ToPropertyKey(propertyKey).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 1));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Let desc be ? ToPropertyDescriptor(attributes).
    JSHandle<JSTaggedValue> attributes = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    PropertyDescriptor desc(thread);
    JSObject::ToPropertyDescriptor(thread, attributes, desc);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 4. Return ? target.[[DefineOwnProperty]](key, desc).
    return builtins_common::GetTaggedBoolean(JSTaggedValue::DefineOwnProperty(thread, target, key, desc));
}

// ecma 21.1.4 Reflect.deleteProperty (target, propertyKey)
JSTaggedValue reflect::DeleteProperty(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, DeleteProperty);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.deleteProperty target is not object", JSTaggedValue::Exception());
    }
    // 2. Let key be ? ToPropertyKey(propertyKey).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 1));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return ? target.[[Delete]](key).
    return builtins_common::GetTaggedBoolean(JSTaggedValue::DeleteProperty(thread, target, key));
}

// ecma 26.1.5 Reflect.get (target, propertyKey [ , receiver])
JSTaggedValue reflect::Get(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, Get);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> val = builtins_common::GetCallArg(argv, 0);
    if (!val->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.get target is not object", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> target = JSHandle<JSObject>::Cast(val);
    // 2. Let key be ? ToPropertyKey(propertyKey).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 1));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. If receiver is not present, then
    //     a. Set receiver to target.
    // 4. Return ? target.[[Get]](key, receiver).
    if (argv->GetArgsNumber() == 2) {  // 2: 2 means that there are 2 args in total
        return JSObject::GetProperty(thread, target, key).GetValue().GetTaggedValue();
    }
    JSHandle<JSTaggedValue> receiver = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    return JSObject::GetProperty(thread, val, key, receiver).GetValue().GetTaggedValue();
}

// ecma 26.1.6 Reflect.getOwnPropertyDescriptor ( target, propertyKey )
JSTaggedValue reflect::GetOwnPropertyDescriptor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, GetOwnPropertyDescriptor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.getOwnPropertyDescriptor target is not object",
                                    JSTaggedValue::Exception());
    }
    // 2. Let key be ? ToPropertyKey(propertyKey).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 1));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Let desc be ? target.[[GetOwnProperty]](key).
    PropertyDescriptor desc(thread);
    if (!JSTaggedValue::GetOwnProperty(thread, target, key, desc)) {
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // 4. Return FromPropertyDescriptor(desc).
    JSHandle<JSTaggedValue> res = JSObject::FromPropertyDescriptor(thread, desc);
    return res.GetTaggedValue();
}

// ecma 21.1.7 Reflect.getPrototypeOf (target)
JSTaggedValue reflect::GetPrototypeOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, GetPrototypeOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> val = builtins_common::GetCallArg(argv, 0);
    if (!val->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.getPrototypeOf target is not object", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> target = JSHandle<JSObject>::Cast(val);
    // 2. Return ? target.[[GetPrototypeOf]]().
    return target->GetPrototype(thread);
}

// ecma 26.1.8 Reflect.has (target, propertyKey)
JSTaggedValue reflect::Has(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, Has);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.has target is not object", JSTaggedValue::Exception());
    }
    // 2. Let key be ? ToPropertyKey(propertyKey).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 1));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return ? target.[[HasProperty]](key).
    return builtins_common::GetTaggedBoolean(JSTaggedValue::HasProperty(thread, target, key));
}

// ecma 26.1.9  Reflect.isExtensible (target)
JSTaggedValue reflect::IsExtensible(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.isExtensible target is not object", JSTaggedValue::Exception());
    }
    // 2. Return ? target.[[IsExtensible]]().
    return builtins_common::GetTaggedBoolean(target->IsExtensible(thread));
}

// ecma 26.1.10 Reflect.ownKeys (target)
JSTaggedValue reflect::OwnKeys(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, OwnKeys);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.ownKeys target is not object", JSTaggedValue::Exception());
    }
    // 2. Let keys be ? target.[[OwnPropertyKeys]]().
    JSHandle<TaggedArray> keys = JSTaggedValue::GetOwnPropertyKeys(thread, target);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return CreateArrayFromList(keys).
    JSHandle<JSArray> result = JSArray::CreateArrayFromList(thread, keys);
    return result.GetTaggedValue();
}

// ecma 26.1.11 Reflect.preventExtensions (target)
JSTaggedValue reflect::PreventExtensions(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, PreventExtensions);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.preventExtensions target is not object",
                                    JSTaggedValue::Exception());
    }
    // 2. Return ? target.[[PreventExtensions]]().
    return builtins_common::GetTaggedBoolean(JSTaggedValue::PreventExtensions(thread, target));
}

// ecma 26.1.12 Reflect.set (target, propertyKey, V [ , receiver])
JSTaggedValue reflect::Set(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, Set);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target_val = builtins_common::GetCallArg(argv, 0);
    if (!target_val->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.get target is not object", JSTaggedValue::Exception());
    }
    // 2. Let key be ? ToPropertyKey(propertyKey).
    JSHandle<JSTaggedValue> key = JSTaggedValue::ToPropertyKey(thread, builtins_common::GetCallArg(argv, 1));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    // 3. If receiver is not present, then
    //     a. Set receiver to target.
    // 4. Return ? target.[[Set]](key, receiver).
    if (argv->GetArgsNumber() == 3) {  // 3: 3 means that there are three args in total
        return builtins_common::GetTaggedBoolean(JSTaggedValue::SetProperty(thread, target_val, key, value));
    }
    JSHandle<JSTaggedValue> receiver = builtins_common::GetCallArg(argv, 3);  // 3: 3 means the third arg
    return builtins_common::GetTaggedBoolean(JSTaggedValue::SetProperty(thread, target_val, key, value, receiver));
}

// ecma 26.1.13  Reflect.setPrototypeOf (target, proto)
JSTaggedValue reflect::SetPrototypeOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Reflect, SetPrototypeOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. If Type(target) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> target = builtins_common::GetCallArg(argv, 0);
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reflect.setPrototypeOf target is not object", JSTaggedValue::Exception());
    }
    // 2. If Type(proto) is not Object and proto is not null, throw a TypeError exception.
    JSHandle<JSTaggedValue> proto = builtins_common::GetCallArg(argv, 1);
    if (!proto->IsECMAObject() && !proto->IsNull()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "SetPrototypeOf: proto is neither Object nor Null",
                                    JSTaggedValue::Exception());
    }
    // 3. Return ? target.[[SetPrototypeOf]](proto).
    return builtins_common::GetTaggedBoolean(JSTaggedValue::SetPrototype(thread, target, proto));
}
}  // namespace panda::ecmascript::builtins
