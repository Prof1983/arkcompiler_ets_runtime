/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description:
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_async_from_sync_iterator_object.h"

namespace panda::ecmascript::builtins {
// 27.1.4.2.2 %AsyncFromSyncIteratorPrototype%.next(value)
JSTaggedValue builtins::async_from_sync_iterator::proto::Next(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), AsyncFromSyncIteratorPrototype, Next);

    JSThread *thread = argv->GetThread();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_object = builtins_common::GetThis(argv);
    // 2. Assert: Type(O) is Object and O has a [[SyncIteratorRecord]] internal slot.
    JSHandle<JSAsyncFromSyncIteratorObject> async_from_sync_iterator_object(
        thread, JSAsyncFromSyncIteratorObject::Cast(this_object->GetHeapObject()));

    // 4. Let syncIteratorRecord be O.[[SyncIteratorRecord]].
    JSHandle<JSTaggedValue> iterator(thread, async_from_sync_iterator_object->GetIterator());
    JSHandle<JSTaggedValue> next_method(thread, async_from_sync_iterator_object->GetNextMethod());

    // 3. Let promiseCapability be ! NewPromiseCapability(%Promise%).
    JSHandle<PromiseCapability> promise_capability =
        JSPromise::NewPromiseCapability(thread, JSHandle<JSTaggedValue>::Cast(env->GetPromiseFunction()));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSMutableHandle<JSTaggedValue> result(thread, thread->GlobalConstants()->GetHandledUndefined());

    // 5. If value is present, then
    if (argv->GetArgsNumber() > 0) {
        // a. Let result be IteratorNext(syncIteratorRecord, value).
        result.Update(JSIterator::IteratorNext(thread, iterator, next_method, builtins_common::GetCallArg(argv, 0)));
    } else {
        // 6. Else,
        // a. Let result be IteratorNext(syncIteratorRecord).
        result.Update(JSIterator::IteratorNext(thread, iterator, next_method));
    }

    // 7. IfAbruptRejectPromise(result, promiseCapability).
    if (UNLIKELY(thread->HasPendingException())) {
        result.Update(JSPromise::IfThrowGetThrowValue(thread));
    }
    RETURN_REJECT_PROMISE_IF_ABRUPT(thread, result, promise_capability);

    // 8. Return ! AsyncFromSyncIteratorContinuation(result, promiseCapability).
    return JSAsyncFromSyncIteratorObject::AsyncFromSyncIteratorContinuation(thread, result, promise_capability);
}

// 27.1.4.2.3 %AsyncFromSyncIteratorPrototype%.return(value)
JSTaggedValue builtins::async_from_sync_iterator::proto::Return(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), AsyncFromSyncIteratorPrototype, Return);

    JSThread *thread = argv->GetThread();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_constants = thread->GlobalConstants();

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_object = builtins_common::GetThis(argv);
    // 2. Assert: Type(O) is Object and O has a [[SyncIteratorRecord]] internal slot.
    JSHandle<JSAsyncFromSyncIteratorObject> async_from_sync_iterator_object(
        thread, JSAsyncFromSyncIteratorObject::Cast(this_object->GetHeapObject()));

    // 3. Let promiseCapability be ! NewPromiseCapability(%Promise%).
    JSHandle<PromiseCapability> promise_capability =
        JSPromise::NewPromiseCapability(thread, JSHandle<JSTaggedValue>::Cast(env->GetPromiseFunction()));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4. Let syncIteratorRecord be O.[[SyncIteratorRecord]].
    JSHandle<JSTaggedValue> iterator(thread, async_from_sync_iterator_object->GetIterator());

    // 5. Let return be GetMethod(syncIterator, "return").
    JSHandle<JSTaggedValue> return_key = global_constants->GetHandledReturnString();
    JSHandle<JSTaggedValue> return_method = JSObject::GetMethod(thread, iterator, return_key);

    // 6. IfAbruptRejectPromise(return, promiseCapability).
    RETURN_REJECT_PROMISE_IF_ABRUPT_THROWN_VALUE(thread, return_method, promise_capability);

    // 7. If return is undefined, then
    if (return_method->IsUndefined()) {
        // a. Let iterResult be ! CreateIterResultObject(value, true).
        JSHandle<JSObject> iter_result =
            JSIterator::CreateIterResultObject(thread, builtins_common::GetCallArg(argv, 0), true);

        // b. Perform ! Call(promiseCapability.[[Resolve]], undefined, « iterResult »).
        JSHandle<JSTaggedValue> resolve(thread, promise_capability->GetResolve());
        auto info = NewRuntimeCallInfo(thread, resolve, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(iter_result);
        [[maybe_unused]] JSTaggedValue res = JSFunction::Call(info.Get());

        // c. Return promiseCapability.[[Promise]].
        return promise_capability->GetPromise();
    }

    JSMutableHandle<JSTaggedValue> result(thread, global_constants->GetHandledUndefined());

    {
        ScopedCallInfo info;
        // 8. If value is present, then
        if (argv->GetArgsNumber() > 0) {
            // a. Let result be Call(return, syncIterator, « value »).
            info = NewRuntimeCallInfo(thread, return_method, iterator, JSTaggedValue::Undefined(), 1);
            info->SetCallArgs(builtins_common::GetCallArg(argv, 0));
        } else {
            // 9. Else,
            // a. Let result be Call(return, syncIterator).
            info = NewRuntimeCallInfo(thread, return_method, iterator, JSTaggedValue::Undefined(), 0);
        }
        result.Update(JSFunction::Call(info.Get()));
    }

    // 10. IfAbruptRejectPromise(result, promiseCapability).
    if (UNLIKELY(thread->HasPendingException())) {
        result.Update(JSPromise::IfThrowGetThrowValue(thread));
    }
    RETURN_REJECT_PROMISE_IF_ABRUPT(thread, result, promise_capability);

    // 11. If Type(result) is not Object, then
    if (!result->IsECMAObject()) {
        // a. Perform ! Call(promiseCapability.[[Reject]], undefined, « a newly created TypeError object »)..
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

        JSHandle<JSObject> type_error = factory->GetJSError(ErrorType::TYPE_ERROR, "Return is not an object");
        JSHandle<JSTaggedValue> reject(thread, promise_capability->GetReject());
        auto info = NewRuntimeCallInfo(thread, reject, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(type_error);
        [[maybe_unused]] JSTaggedValue res = JSFunction::Call(info.Get());

        // b. Return promiseCapability.[[Promise]].
        return promise_capability->GetPromise();
    }

    // 12. Return ! AsyncFromSyncIteratorContinuation(result, promiseCapability).
    return JSAsyncFromSyncIteratorObject::AsyncFromSyncIteratorContinuation(thread, result, promise_capability);
}

// 27.1.4.2.4 %AsyncFromSyncIteratorPrototype%.throw(exception)
JSTaggedValue builtins::async_from_sync_iterator::proto::Throw(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), AsyncFromSyncIteratorPrototype, Throw);

    JSThread *thread = argv->GetThread();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_constants = thread->GlobalConstants();

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_object = builtins_common::GetThis(argv);
    // 2. Assert: Type(O) is Object and O has a [[SyncIteratorRecord]] internal slot.
    JSHandle<JSAsyncFromSyncIteratorObject> async_from_sync_iterator_object(
        thread, JSAsyncFromSyncIteratorObject::Cast(this_object->GetHeapObject()));

    // 3. Let promiseCapability be ! NewPromiseCapability(%Promise%).
    JSHandle<PromiseCapability> promise_capability =
        JSPromise::NewPromiseCapability(thread, JSHandle<JSTaggedValue>::Cast(env->GetPromiseFunction()));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4. Let syncIteratorRecord be O.[[SyncIteratorRecord]].
    JSHandle<JSTaggedValue> iterator(thread, async_from_sync_iterator_object->GetIterator());

    // 5. Let return be GetMethod(syncIterator, "throw").
    JSHandle<JSTaggedValue> throw_key = global_constants->GetHandledThrowString();
    JSHandle<JSTaggedValue> throw_method = JSObject::GetMethod(thread, iterator, throw_key);

    // 6. IfAbruptRejectPromise(return, promiseCapability).
    RETURN_REJECT_PROMISE_IF_ABRUPT_THROWN_VALUE(thread, throw_method, promise_capability);

    // 7. If throw is undefined, then
    if (throw_method->IsUndefined()) {
        // a. Perform ! Call(promiseCapability.[[Reject]], undefined, « value »).
        JSHandle<JSTaggedValue> reject(thread, promise_capability->GetReject());
        ScopedCallInfo info;
        if (argv->GetArgsNumber() > 0) {
            info = NewRuntimeCallInfo(thread, reject, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 1);
            info->SetCallArgs(builtins_common::GetCallArg(argv, 0));
        } else {
            info = NewRuntimeCallInfo(thread, reject, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 0);
        }
        [[maybe_unused]] JSTaggedValue call_result = JSFunction::Call(info.Get());

        // b. Return promiseCapability.[[Promise]].
        return promise_capability->GetPromise();
    }

    JSMutableHandle<JSTaggedValue> result(thread, global_constants->GetHandledUndefined());

    {
        ScopedCallInfo info;
        // 8. If value is present, then
        if (argv->GetArgsNumber() > 0) {
            // a. Let result be Call(throw, syncIterator, « value »).
            info = NewRuntimeCallInfo(thread, throw_method, iterator, JSTaggedValue::Undefined(), 1);
            info->SetCallArgs(builtins_common::GetCallArg(argv, 0));
        } else {
            // 9. Else,
            // a. Let result be Call(throw, syncIterator).
            info = NewRuntimeCallInfo(thread, throw_method, iterator, JSTaggedValue::Undefined(), 0);
        }
        result.Update(JSFunction::Call(info.Get()));
    }

    // 10. IfAbruptRejectPromise(result, promiseCapability).
    if (UNLIKELY(thread->HasPendingException())) {
        result.Update(JSPromise::IfThrowGetThrowValue(thread));
    }
    RETURN_REJECT_PROMISE_IF_ABRUPT(thread, result, promise_capability);

    // 11. If Type(result) is not Object, then
    if (!result->IsECMAObject()) {
        // a. Perform ! Call(promiseCapability.[[Reject]], undefined, « a newly created TypeError object »)..
        JSHandle<JSObject> type_error = factory->GetJSError(ErrorType::TYPE_ERROR, "Throw is not an object");
        JSHandle<JSTaggedValue> reject(thread, promise_capability->GetReject());
        auto info = NewRuntimeCallInfo(thread, reject, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(type_error);
        [[maybe_unused]] JSTaggedValue call_result = JSFunction::Call(info.Get());
        // b. Return promiseCapability.[[Promise]].
        return promise_capability->GetPromise();
    }

    // 12. Return ! AsyncFromSyncIteratorContinuation(result, promiseCapability).
    return JSAsyncFromSyncIteratorObject::AsyncFromSyncIteratorContinuation(thread, result, promise_capability);
}
}  // namespace panda::ecmascript::builtins
