/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"

namespace panda::ecmascript::builtins {
// 24.2.2.1
JSTaggedValue data_view::DataViewConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), DataView, DataViewConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> ctor = builtins_common::GetConstructor(argv);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    // 1. If NewTarget is undefined, throw a TypeError exception.
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "newtarget is undefined", JSTaggedValue::Exception());
    }
    JSHandle<JSTaggedValue> buffer_handle = builtins_common::GetCallArg(argv, 0);
    // 2. If Type(buffer) is not Object, throw a TypeError exception.
    if (!buffer_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "buffer is not Object", JSTaggedValue::Exception());
    }
    // 3. If buffer does not have an [[ArrayBufferData]] internal slot, throw a TypeError exception.
    if (!buffer_handle->IsArrayBuffer()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "buffer is not ArrayBuffer", JSTaggedValue::Exception());
    }
    JSHandle<JSTaggedValue> offset_handle = builtins_common::GetCallArg(argv, 1);
    // 4. Let numberOffset be ToNumber(byteOffset).
    JSTaggedNumber offset_number = JSTaggedValue::ToNumber(thread, offset_handle);
    // 6. ReturnIfAbrupt(offset).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t offset = ecmascript::base::NumberHelper::DoubleInRangeInt32(offset_number.GetNumber());
    // 7. If numberOffset ≠ offset or offset < 0, throw a RangeError exception.
    if (offset < 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Offset out of range", JSTaggedValue::Exception());
    }
    // 8. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    if (builtins::array_buffer::IsDetachedBuffer(buffer_handle.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "buffer is Detached Buffer", JSTaggedValue::Exception());
    }
    // 9. Let bufferByteLength be the value of buffer’s [[ArrayBufferByteLength]] internal slot.
    JSHandle<JSArrayBuffer> arr_buf_handle(buffer_handle);
    JSTaggedNumber buf_len_num = JSTaggedNumber::FromIntOrDouble(thread, arr_buf_handle->GetArrayBufferByteLength());
    int32_t buf_byte_len = buf_len_num.ToInt32();
    // 10. If offset > bufferByteLength, throw a RangeError exception.
    if (offset > buf_byte_len) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "offset > bufferByteLength", JSTaggedValue::Exception());
    }
    int32_t view_byte_len;
    JSHandle<JSTaggedValue> byte_len_handle = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    // 11. If byteLength is undefined, then Let viewByteLength be bufferByteLength – offset.
    if (byte_len_handle->IsUndefined()) {
        view_byte_len = buf_byte_len - offset;
    } else {
        // Let viewByteLength be ToIndex(byteLength).
        JSTaggedNumber byte_len = JSTaggedValue::ToIndex(thread, byte_len_handle);
        // ReturnIfAbrupt(viewByteLength).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        view_byte_len = byte_len.ToInt32();
        // If offset+viewByteLength > bufferByteLength, throw a RangeError exception.
        if (offset + view_byte_len > buf_byte_len) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "offset + viewByteLen > bufByteLen", JSTaggedValue::Exception());
        }
    }
    // 13. Let O be OrdinaryCreateFromConstructor OrdinaryCreateFromConstructor(NewTarget, "%DataViewPrototype%",
    // «[[DataView]],[[ViewedArrayBuffer]], [[ByteLength]], [[ByteOffset]]» ).
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), new_target);
    // 14. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSDataView> data_view(obj);
    // 15. Set O’s [[DataView]] internal slot to true.
    data_view->SetDataView(thread, JSTaggedValue::True());
    // 16. Set O’s [[ViewedArrayBuffer]] internal slot to buffer.
    data_view->SetViewedArrayBuffer(thread, buffer_handle.GetTaggedValue());
    // 17. Set O’s [[ByteLength]] internal slot to viewByteLength.
    data_view->SetByteLength(thread, JSTaggedValue(view_byte_len));
    // 18. Set O’s [[ByteOffset]] internal slot to offset.
    data_view->SetByteOffset(thread, JSTaggedValue(offset));
    // 19. Return O.
    return JSTaggedValue(data_view.GetTaggedValue());
}

// 24.2.4.1
JSTaggedValue data_view::proto::GetBuffer(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), DataViewPrototype, GetBuffer);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. f Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Type(O) is not Object", JSTaggedValue::Exception());
    }
    // 3. If O does not have a [[ViewedArrayBuffer]] internal slot, throw a TypeError exception.
    if (!this_handle->IsDataView()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "O does not have a [[ViewedArrayBuffer]]", JSTaggedValue::Exception());
    }
    JSHandle<JSDataView> data_view(this_handle);
    // 4. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = data_view->GetViewedArrayBuffer();
    // 5. Return buffer.
    return JSTaggedValue(buffer);
}

// 24.2.4.2
JSTaggedValue data_view::proto::GetByteLength(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), DataViewPrototype, GetByteLength);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Type(O) is not Object", JSTaggedValue::Exception());
    }
    // 3. If O does not have a [[ViewedArrayBuffer]] internal slot, throw a TypeError exception.
    if (!this_handle->IsDataView()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "O does not have a [[ViewedArrayBuffer]]", JSTaggedValue::Exception());
    }
    JSHandle<JSDataView> data_view(this_handle);
    // 4. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = data_view->GetViewedArrayBuffer();
    // 5. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", JSTaggedValue::Exception());
    }
    // 6. Let size be the value of O’s [[ByteLength]] internal slot.
    JSTaggedValue size = data_view->GetByteLength();
    // 7. Return size.
    return JSTaggedValue(size);
}

// 24.2.4.3
JSTaggedValue data_view::proto::GetByteOffset(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), DataViewPrototype, GetByteOffset);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Type(O) is not Object", JSTaggedValue::Exception());
    }
    // 3. If O does not have a [[ViewedArrayBuffer]] internal slot, throw a TypeError exception.
    if (!this_handle->IsDataView()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "O does not have a [[ViewedArrayBuffer]]", JSTaggedValue::Exception());
    }
    JSHandle<JSDataView> data_view(this_handle);
    // 4. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = data_view->GetViewedArrayBuffer();
    // 5. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", JSTaggedValue::Exception());
    }
    // 6. Let offset be the value of O’s [[ByteOffset]] internal slot.
    JSTaggedValue offset = data_view->GetByteOffset();
    // 7. Return offset.
    return JSTaggedValue(offset);
}

// 24.2.1.1
static JSTaggedValue GetViewValue(JSThread *thread, const JSHandle<JSTaggedValue> &view,
                                  const JSHandle<JSTaggedValue> &request_index, JSTaggedValue little_endian,
                                  DataViewType type)
{
    BUILTINS_API_TRACE(thread, DataView, GetViewValue);
    // 1. If Type(view) is not Object, throw a TypeError exception.
    if (!view->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Type(O) is not Object", JSTaggedValue::Exception());
    }
    // 2. If view does not have a [[DataView]] internal slot, throw a TypeError exception.
    if (!view->IsDataView()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "view is not dataview", JSTaggedValue::Exception());
    }
    // 3. Let numberIndex be ToNumber(requestIndex).
    JSTaggedNumber number_index = JSTaggedValue::ToNumber(thread, request_index);
    // 5. ReturnIfAbrupt(getIndex).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int64_t index = ecmascript::base::NumberHelper::DoubleInRangeInt32(number_index.GetNumber());
    // 6. If numberIndex ≠ getIndex or getIndex < 0, throw a RangeError exception.
    if (index < 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "getIndex < 0", JSTaggedValue::Exception());
    }
    // 7. Let isLittleEndian be ToBoolean(isLittleEndian).
    bool is_little_endian;
    if (little_endian.IsUndefined()) {
        is_little_endian = false;
    } else {
        is_little_endian = little_endian.ToBoolean();
    }
    // 8. Let buffer be the value of view’s [[ViewedArrayBuffer]] internal slot.
    JSHandle<JSDataView> data_view(view);
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSTaggedValue buffer = data_view->GetViewedArrayBuffer();
        // 9. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
        if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", JSTaggedValue::Exception());
        }
    }
    // 10. Let viewOffset be the value of view’s [[ByteOffset]] internal slot.
    JSTaggedNumber offset_num = JSTaggedNumber::FromIntOrDouble(thread, data_view->GetByteOffset());
    int32_t offset = offset_num.ToInt32();
    // 11. Let viewSize be the value of view’s [[ByteLength]] internal slot.
    JSTaggedNumber view_num = JSTaggedNumber::FromIntOrDouble(thread, data_view->GetByteLength());
    int32_t size = view_num.ToInt32();
    // 12. Let elementSize be the Number value of the Element Size value specified in Table 49 for Element Type type.
    int32_t element_size = JSDataView::GetElementSize(type);
    // 13. If getIndex +elementSize > viewSize, throw a RangeError exception.
    if (index + element_size > size) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "getIndex +elementSize > viewSize", JSTaggedValue::Exception());
    }
    // 14. Let bufferIndex be getIndex + viewOffset.
    int32_t buffer_index = index + offset;
    // 15. Return GetValueFromBuffer(buffer, bufferIndex, type, isLittleEndian).
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSHandle<JSTaggedValue> buffer(thread, data_view->GetViewedArrayBuffer());
        return builtins::array_buffer::GetValueFromBuffer(thread, buffer, buffer_index, type, is_little_endian);
    }
}

// 24.2.1.2
JSTaggedValue SetViewValue(JSThread *thread, const JSHandle<JSTaggedValue> &view,
                           const JSHandle<JSTaggedValue> &request_index, JSTaggedValue little_endian, DataViewType type,
                           const JSHandle<JSTaggedValue> &value)
{
    // 1. If Type(view) is not Object, throw a TypeError exception.
    BUILTINS_API_TRACE(thread, DataView, SetViewValue);
    if (!view->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Type(O) is not Object", JSTaggedValue::Exception());
    }
    // 2. If view does not have a [[DataView]] internal slot, throw a TypeError exception.
    if (!view->IsDataView()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "view is not dataview", JSTaggedValue::Exception());
    }
    // 3. Let numberIndex be ToNumber(requestIndex).
    JSTaggedNumber number_index = JSTaggedValue::ToIndex(thread, request_index);
    // 5. ReturnIfAbrupt(getIndex).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int64_t index = ecmascript::base::NumberHelper::DoubleInRangeInt32(number_index.GetNumber());
    // 6. If numberIndex ≠ getIndex or getIndex < 0, throw a RangeError exception.
    if (index < 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "getIndex < 0", JSTaggedValue::Exception());
    }
    JSHandle<JSTaggedValue> num_val = JSTaggedValue::ToNumeric(thread, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 7. Let isLittleEndian be ToBoolean(isLittleEndian).
    bool is_little_endian;
    if (little_endian.IsUndefined()) {
        is_little_endian = false;
    } else {
        is_little_endian = little_endian.ToBoolean();
    }
    // 8. Let buffer be the value of view’s [[ViewedArrayBuffer]] internal slot.
    JSHandle<JSDataView> data_view(view);
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSTaggedValue buffer = data_view->GetViewedArrayBuffer();
        // 9. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
        if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", JSTaggedValue::Exception());
        }
    }
    // 10. Let viewOffset be the value of view’s [[ByteOffset]] internal slot.
    JSTaggedNumber offset_num = JSTaggedNumber::FromIntOrDouble(thread, data_view->GetByteOffset());
    int32_t offset = offset_num.ToInt32();
    // 11. Let viewSize be the value of view’s [[ByteLength]] internal slot.
    JSTaggedNumber view_num = JSTaggedNumber::FromIntOrDouble(thread, data_view->GetByteLength());
    int32_t size = view_num.ToInt32();
    // 12. Let elementSize be the Number value of the Element Size value specified in Table 49 for Element Type type.
    int32_t element_size = JSDataView::GetElementSize(type);
    // 13. If getIndex +elementSize > viewSize, throw a RangeError exception.
    if (index + element_size > size) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "getIndex +elementSize > viewSize", JSTaggedValue::Exception());
    }
    // 14. Let bufferIndex be getIndex + viewOffset.
    int32_t buffer_index = index + offset;
    {
        // We use buffer without handle and GC can move it. So we have to get buffer each time.
        // To make developer get buffer each time use scope and don't populate buffer into function's scope.
        JSHandle<JSTaggedValue> buffer(thread, data_view->GetViewedArrayBuffer());
        // 15. Return SetValueFromBuffer(buffer, bufferIndex, type, value, isLittleEndian).
        return builtins::array_buffer::SetValueInBuffer(thread, buffer, buffer_index, type, num_val, is_little_endian);
    }
}

JSTaggedValue GetTypedValue(EcmaRuntimeCallInfo *argv, DataViewType type)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSTaggedValue> offset_handle = builtins_common::GetCallArg(argv, 0);
    if (type == DataViewType::UINT8 || type == DataViewType::INT8) {
        return GetViewValue(thread, this_handle, offset_handle, JSTaggedValue::True(), type);
    }
    JSHandle<JSTaggedValue> little_endian_handle = builtins_common::GetCallArg(argv, 1);
    return GetViewValue(thread, this_handle, offset_handle, little_endian_handle.GetTaggedValue(), type);
}

JSTaggedValue SetTypedValue(EcmaRuntimeCallInfo *argv, DataViewType type)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSTaggedValue> offset_handle = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 1);
    if (type == DataViewType::UINT8 || type == DataViewType::INT8) {
        return SetViewValue(thread, this_handle, offset_handle, JSTaggedValue::True(), type, value);
    }
    JSHandle<JSTaggedValue> little_endian_handle =
        builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    return SetViewValue(thread, this_handle, offset_handle, little_endian_handle.GetTaggedValue(), type, value);
}

// 24.2.4.5
JSTaggedValue data_view::proto::GetFloat32(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::FLOAT32);
}

// 24.2.4.6
JSTaggedValue data_view::proto::GetFloat64(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::FLOAT64);
}

// 24.2.4.7
JSTaggedValue data_view::proto::GetInt8(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::INT8);
}

// 24.2.4.8
JSTaggedValue data_view::proto::GetInt16(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::INT16);
}

// 24.2.4.9
JSTaggedValue data_view::proto::GetInt32(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::INT32);
}

// 24.2.4.10
JSTaggedValue data_view::proto::GetUint8(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::UINT8);
}

// 24.2.4.11
JSTaggedValue data_view::proto::GetUint16(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::UINT16);
}

// 24.2.4.12
JSTaggedValue data_view::proto::GetUint32(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::UINT32);
}

// 25.3.4.5
JSTaggedValue data_view::proto::GetBigInt64(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::BIGINT64);
}

// 25.3.4.6
JSTaggedValue data_view::proto::GetBigUint64(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return GetTypedValue(argv, DataViewType::BIGUINT64);
}

// 24.2.4.13
JSTaggedValue data_view::proto::SetFloat32(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::FLOAT32);
}

// 24.2.4.14
JSTaggedValue data_view::proto::SetFloat64(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::FLOAT64);
}

// 24.2.4.15
JSTaggedValue data_view::proto::SetInt8(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::INT8);
}

// 24.2.4.16
JSTaggedValue data_view::proto::SetInt16(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::INT16);
}

// 24.2.4.17
JSTaggedValue data_view::proto::SetInt32(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::INT32);
}

// 24.2.4.18
JSTaggedValue data_view::proto::SetUint8(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::UINT8);
}

// 24.2.4.19
JSTaggedValue data_view::proto::SetUint16(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::UINT16);
}

// 25.3.4.15
JSTaggedValue data_view::proto::SetBigInt64(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::BIGINT64);
}

// 25.3.4.16
JSTaggedValue data_view::proto::SetBigUint64(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::BIGUINT64);
}

// 24.2.4.20
JSTaggedValue data_view::proto::SetUint32(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return SetTypedValue(argv, DataViewType::UINT32);
}

}  // namespace panda::ecmascript::builtins
