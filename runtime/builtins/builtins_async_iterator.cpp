/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description:
 */

#include "plugins/ecmascript/runtime/builtins.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_iterator.h"

namespace panda::ecmascript::builtins {
// 27.1.3.1 %AsyncIteratorPrototype% [ @@asyncIterator ] ( )
JSTaggedValue async_iterator::proto::AsyncIterator(EcmaRuntimeCallInfo *argv)
{
    return ecmascript::builtins_common::GetThis(argv).GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
