/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <typeinfo>

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "securec.h"

namespace panda::ecmascript::builtins {
enum NumberSize : uint8_t {
    UINT16 = 2,
    INT16 = 2,
    UINT32 = 4,
    INT32 = 4,
    FLOAT32 = 4,
    FLOAT64 = 8,
    BIGINT64 = 8,
    BIGUINT64 = 8
};
union UnionType32 {
    uint32_t u_value;
    float value;
} __attribute__((packed, may_alias));
union UnionType64 {
    uint64_t u_value;
    double value;
} __attribute__((packed, may_alias));

// 24.1.2.1 ArrayBuffer(length)
JSTaggedValue builtins::array_buffer::ArrayBufferConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayBuffer, ArrayBufferConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    // 1. If NewTarget is undefined, throw a TypeError exception.
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "newtarget is undefined", JSTaggedValue::Exception());
    }
    JSHandle<JSTaggedValue> length_handle = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber len_num = JSTaggedValue::ToIndex(thread, length_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double length = len_num.GetNumber();
    return AllocateArrayBuffer(thread, new_target, length);
}

// 24.1.3.1 ArrayBuffer.isView(arg)
JSTaggedValue builtins::array_buffer::IsView(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    JSHandle<JSTaggedValue> arg = builtins_common::GetCallArg(argv, 0);
    // 1. If Type(arg) is not Object, return false.
    if (!arg->IsECMAObject()) {
        return builtins_common::GetTaggedBoolean(false);
    }
    // 2. If arg has a [[ViewedArrayBuffer]] internal slot, return true.
    if (arg->IsDataView() || arg->IsTypedArray()) {
        return builtins_common::GetTaggedBoolean(true);
    }
    // 3. Return false.
    return builtins_common::GetTaggedBoolean(false);
}

// 24.1.3.3 get ArrayBuffer [ @@species ]
JSTaggedValue builtins::array_buffer::GetSpecies(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return builtins_common::GetThis(argv).GetTaggedValue();
}

// 24.1.4.1 get ArrayBuffer.prototype.byteLength
JSTaggedValue builtins::array_buffer::proto::GetByteLength(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    JSThread *thread = argv->GetThread();
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this value is not an object", JSTaggedValue::Exception());
    }
    // 3. If O does not have an [[ArrayBufferData]] internal slot, throw a TypeError exception.
    if (!this_handle->IsArrayBuffer()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "don't have internal slot", JSTaggedValue::Exception());
    }
    // 4. If IsDetachedBuffer(O) is true, throw a TypeError exception.
    if (IsDetachedBuffer(this_handle.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "IsDetachedBuffer", JSTaggedValue::Exception());
    }
    JSHandle<JSArrayBuffer> arr_buf(this_handle);
    // 5. Let length be the value of O’s [[ArrayBufferByteLength]] internal slot.
    JSTaggedValue length = arr_buf->GetArrayBufferByteLength();
    // 6. Return length.
    return JSTaggedValue(length);
}

// 24.1.4.3 ArrayBuffer.prototype.slice(start, end)
JSTaggedValue builtins::array_buffer::proto::Slice(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayBufferPrototype, Slice);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this value is not an object", JSTaggedValue::Exception());
    }
    JSHandle<JSArrayBuffer> arr_buf(this_handle);
    // 3. If O does not have an [[ArrayBufferData]] internal slot, throw a TypeError exception.
    if (!this_handle->IsArrayBuffer()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "don't have internal slot", JSTaggedValue::Exception());
    }
    // 4. If IsDetachedBuffer(O) is true, throw a TypeError exception.
    if (IsDetachedBuffer(this_handle.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this value IsDetachedBuffer", JSTaggedValue::Exception());
    }
    // 5. Let len be the value of O’s [[ArrayBufferByteLength]] internal slot.
    JSTaggedNumber length_num = JSTaggedNumber::FromIntOrDouble(thread, arr_buf->GetArrayBufferByteLength());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> start_handle = builtins_common::GetCallArg(argv, 0);
    // 6. Let relativeStart be ToInteger(start).
    JSTaggedNumber relative_start = JSTaggedValue::ToInteger(thread, start_handle);
    // 7. ReturnIfAbrupt(relativeStart).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t len = length_num.ToInt32();
    int32_t start = ecmascript::base::NumberHelper::DoubleInRangeInt32(relative_start.GetNumber());
    int32_t end;
    int32_t first;
    int32_t last;
    // 8. If relativeStart < 0, let first be max((len + relativeStart),0); else let first be min(relativeStart, len).
    if (start < 0) {
        first = std::max((len + start), 0);
    } else {
        first = std::min(start, len);
    }
    // 9. If end is undefined, let relativeEnd be len; else let relativeEnd be ToInteger(end).
    JSHandle<JSTaggedValue> end_handle = builtins_common::GetCallArg(argv, 1);
    if (end_handle->IsUndefined()) {
        end = len;
    } else {
        JSTaggedNumber relative_end = JSTaggedValue::ToInteger(thread, end_handle);
        // 10. ReturnIfAbrupt(relativeEnd).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        end = ecmascript::base::NumberHelper::DoubleInRangeInt32(relative_end.GetNumber());
    }
    // 11. If relativeEnd < 0, let final be max((len + relativeEnd),0); else let final be min(relativeEnd, len).
    if (end < 0) {
        last = std::max((len + end), 0);
    } else {
        last = std::min(end, len);
    }
    // 12. Let newLen be max(final-first,0).
    int32_t new_len = std::max((last - first), 0);
    // 13. Let ctor be SpeciesConstructor(O, %ArrayBuffer%).
    JSHandle<JSTaggedValue> default_constructor = env->GetArrayBufferFunction();
    JSHandle<JSObject> obj_handle(this_handle);
    JSHandle<JSTaggedValue> constructor = JSObject::SpeciesConstructor(thread, obj_handle, default_constructor);
    // 14. ReturnIfAbrupt(ctor).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 15. Let new be Construct(ctor, «newLen»).
    JSHandle<JSTaggedValue> undefined = global_const->GetHandledUndefined();

    auto info = NewRuntimeCallInfo(thread, constructor, JSTaggedValue::Undefined(), undefined, 1);
    info->SetCallArgs(JSTaggedValue(new_len));
    JSTaggedValue tagged_new_arr_buf = JSFunction::Construct(info.Get());
    JSHandle<JSTaggedValue> new_arr_buf(thread, tagged_new_arr_buf);
    // 16. ReturnIfAbrupt(new).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 17. If new does not have an [[ArrayBufferData]] internal slot, throw a TypeError exception.
    if (!new_arr_buf->IsArrayBuffer()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "don't have bufferdata internal slot", JSTaggedValue::Exception());
    }
    // 18. If IsDetachedBuffer(new) is true, throw a TypeError exception.
    if (IsDetachedBuffer(new_arr_buf.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "new arrayBuffer IsDetachedBuffer", JSTaggedValue::Exception());
    }
    // 19. If SameValue(new, O) is true, throw a TypeError exception.
    if (JSTaggedValue::SameValue(new_arr_buf.GetTaggedValue(), this_handle.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "value of new arraybuffer and this is same", JSTaggedValue::Exception());
    }
    JSHandle<JSArrayBuffer> new_js_arr_buf(new_arr_buf);
    // 20. If the value of new’s [[ArrayBufferByteLength]] internal slot < newLen, throw a TypeError exception.
    JSTaggedNumber new_length_num = JSTaggedNumber::FromIntOrDouble(thread, new_js_arr_buf->GetArrayBufferByteLength());
    int32_t new_arr_buf_len = new_length_num.ToInt32();
    if (new_arr_buf_len < new_len) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "new array buffer length smaller than newlen", JSTaggedValue::Exception());
    }
    // 21. NOTE: Side-effects of the above steps may have detached O.
    // 22. If IsDetachedBuffer(O) is true, throw a TypeError exception.
    if (IsDetachedBuffer(this_handle.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this value IsDetachedBuffer", JSTaggedValue::Exception());
    }
    if (new_len > 0) {
        // 23. Let fromBuf be the value of O’s [[ArrayBufferData]] internal slot.
        JSTaggedValue from = arr_buf->GetArrayBufferData();
        // 24. Let toBuf be the value of new’s [[ArrayBufferData]] internal slot.
        JSTaggedValue to = new_js_arr_buf->GetArrayBufferData();
        // 25. Perform CopyDataBlockBytes(toBuf, fromBuf, first, newLen).
        JSArrayBuffer::CopyDataBlockBytes(to, from, first, new_len);
    }
    // Return new.
    return new_arr_buf.GetTaggedValue();
}

// 24.1.1.1 AllocateArrayBuffer(constructor, byteLength)
JSTaggedValue builtins::array_buffer::AllocateArrayBuffer(JSThread *thread, const JSHandle<JSTaggedValue> &new_target,
                                                          double byte_length)
{
    BUILTINS_API_TRACE(thread, ArrayBuffer, AllocateArrayBuffer);
    /**
     * 1. Let obj be OrdinaryCreateFromConstructor(constructor, "%ArrayBufferPrototype%",
     * «[[ArrayBufferData]], [[ArrayBufferByteLength]]» ).
     */
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> arr_buf_func = env->GetArrayBufferFunction();
    JSHandle<JSObject> obj;
    if (!new_target->IsBoundFunction()) {
        obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(arr_buf_func), new_target);
        // 2. ReturnIfAbrupt
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    } else {
        JSHandle<JSTaggedValue> prototype_key = thread->GlobalConstants()->GetHandledPrototypeString();
        JSHandle<JSTaggedValue> construct_tag(new_target);
        JSHandle<JSTaggedValue> construct_proto =
            JSTaggedValue::GetProperty(thread, construct_tag, prototype_key).GetValue();
        obj = JSObject::ObjectCreate(thread, JSHandle<JSObject>(construct_proto));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // 3. Assert: byteLength is a positive integer.
    ASSERT(JSTaggedValue(byte_length).IsInteger());
    ASSERT(byte_length >= 0);
    // 4. Let block be CreateByteDataBlock(byteLength).
    if (byte_length > INT_MAX) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Out of range", JSTaggedValue::Exception());
    }
    JSHandle<JSArrayBuffer> array_buffer(obj);
    // 6. Set obj’s [[ArrayBufferData]] internal slot to block.
    factory->NewJSArrayBufferData(array_buffer, byte_length);
    // 7. Set obj’s [[ArrayBufferByteLength]] internal slot to byteLength.
    array_buffer->SetArrayBufferByteLength(thread, JSTaggedValue(static_cast<int32_t>(byte_length)));
    // 8. Return obj.
    return array_buffer.GetTaggedValue();
}

// 24.1.1.2 IsDetachedBuffer()
bool builtins::array_buffer::IsDetachedBuffer(JSTaggedValue array_buffer)
{
    // 1. Assert: Type(arrayBuffer) is Object and it has an [[ArrayBufferData]] internal slot.
    ASSERT(array_buffer.IsArrayBuffer());
    JSArrayBuffer *buffer = JSArrayBuffer::Cast(array_buffer.GetTaggedObject());
    JSTaggedValue data_slot = buffer->GetArrayBufferData();
    // 2. If arrayBuffer’s [[ArrayBufferData]] internal slot is null, return true.
    // 3. Return false.
    return data_slot == JSTaggedValue::Null();
}

// 24.1.1.4
JSTaggedValue builtins::array_buffer::CloneArrayBuffer(JSThread *thread, const JSHandle<JSTaggedValue> &src_buffer,
                                                       int32_t src_byte_offset, JSHandle<JSTaggedValue> constructor)
{
    BUILTINS_API_TRACE(thread, ArrayBuffer, CloneArrayBuffer);
    // 1. Assert: Type(srcBuffer) is Object and it has an [[ArrayBufferData]] internal slot.
    ASSERT(src_buffer->IsArrayBuffer());
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    // 2. If cloneConstructor is not present
    if (constructor->IsUndefined()) {
        // a. Let cloneConstructor be SpeciesConstructor(srcBuffer, %ArrayBuffer%).
        JSHandle<JSTaggedValue> default_constructor = env->GetArrayBufferFunction();
        JSHandle<JSObject> obj_handle(src_buffer);
        constructor = JSObject::SpeciesConstructor(thread, obj_handle, default_constructor);
        // b. ReturnIfAbrupt(cloneConstructor).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // c. If IsDetachedBuffer(srcBuffer) is true, throw a TypeError exception.
        if (IsDetachedBuffer(src_buffer.GetTaggedValue())) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", JSTaggedValue::Exception());
        } else {
            ASSERT(constructor->IsConstructor());
        }
    }
    // 4. Let srcLength be the value of srcBuffer’s [[ArrayBufferByteLength]] internal slot.
    JSHandle<JSArrayBuffer> arr_buf(src_buffer);
    JSTaggedNumber length_number = JSTaggedNumber::FromIntOrDouble(thread, arr_buf->GetArrayBufferByteLength());
    int32_t src_len = length_number.ToInt32();
    // 5. Assert: srcByteOffset ≤ srcLength.
    ASSERT(src_byte_offset <= src_len);
    // 6. Let cloneLength be srcLength – srcByteOffset.
    int32_t clone_len = src_len - src_byte_offset;
    // 8. Let targetBuffer be AllocateArrayBuffer(cloneConstructor, cloneLength).
    JSTaggedValue tagged_buf = AllocateArrayBuffer(thread, constructor, clone_len);
    // 9. ReturnIfAbrupt(targetBuffer).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 10. If IsDetachedBuffer(srcBuffer) is true, throw a TypeError exception.
    if (IsDetachedBuffer(src_buffer.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Is Detached Buffer", JSTaggedValue::Exception());
    }
    // 11. Let targetBlock be the value of targetBuffer’s [[ArrayBufferData]] internal slot.
    JSHandle<JSArrayBuffer> new_arr_buf(thread, tagged_buf);
    // Perform CopyDataBlockBytes(targetBlock, 0, srcBlock, srcByteOffset, cloneLength).
    // 7. Let srcBlock be the value of srcBuffer’s [[ArrayBufferData]] internal slot.
    JSTaggedValue src_block = arr_buf->GetArrayBufferData();
    JSTaggedValue target_block = new_arr_buf->GetArrayBufferData();
    if (clone_len > 0) {
        JSArrayBuffer::CopyDataBlockBytes(target_block, src_block, src_byte_offset, clone_len);
    }
    return tagged_buf;
}

template <typename T, NumberSize SIZE>
// NOLINTNEXTLINE(readability-non-const-parameter)
JSTaggedValue GetValueFromBufferForInteger(uint8_t *block, int32_t byte_index, bool little_endian)
{
    static_assert(std::is_integral_v<T>, "T must be integral");
    static_assert(sizeof(T) == SIZE, "Invalid number size");
    static_assert(sizeof(T) >= sizeof(uint16_t), "T must have a size more than uint8");

    // NOLINTNEXTLINE(misc-redundant-expression)
    static_assert(SIZE >= NumberSize::UINT16 || SIZE <= NumberSize::FLOAT64);

    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    T res = UnalignedLoad(reinterpret_cast<T *>(block + byte_index));
    if (!little_endian) {
        res = BSWAP(res);
    }

    // uint32_t maybe overflow with TaggedInt
    // NOLINTNEXTLINE(readability-braces-around-statements,bugprone-suspicious-semicolon)
    if constexpr (std::is_same_v<T, uint32_t>) {
        // NOLINTNEXTLINE(clang-diagnostic-sign-compare)
        if (res > static_cast<uint32_t>(std::numeric_limits<int32_t>::max())) {
            return builtins_common::GetTaggedDouble(static_cast<double>(res));
        }
    }
    return builtins_common::GetTaggedInt(res);
}

template <typename T, NumberSize SIZE>
JSTaggedValue GetValueFromBufferForBigInt(JSThread *thread, const uint8_t *block, uint32_t byte_index,
                                          bool little_endian)
{
    static_assert(std::is_same_v<T, uint64_t> || std::is_same_v<T, int64_t>, "T must be uint64_t/int64_t");
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto p_tmp = UnalignedLoad(reinterpret_cast<const uint64_t *>(block + byte_index));
    if (!little_endian) {
        p_tmp = BSWAP(p_tmp);
    }
    // NOLINTNEXTLINE(readability-braces-around-statements,bugprone-suspicious-semicolon)
    if constexpr (std::is_same_v<T, uint64_t>) {
        return BigInt::Uint64ToBigInt(thread, p_tmp).GetTaggedValue();
    }
    return BigInt::Int64ToBigInt(thread, p_tmp).GetTaggedValue();
}

template <typename T, typename UnionType, NumberSize SIZE>
// NOLINTNEXTLINE(readability-non-const-parameter)
JSTaggedValue GetValueFromBufferForFloat(uint8_t *block, int32_t byte_index, bool little_endian)
{
    static_assert(std::is_same_v<T, float> || std::is_same_v<T, double>, "T must be float type");
    static_assert(sizeof(T) == SIZE, "Invalid number size");

    UnionType union_value = {0};
    // NOLINTNEXTLINE(readability-braces-around-statements)
    if constexpr (std::is_same_v<T, float>) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access,cppcoreguidelines-pro-bounds-pointer-arithmetic)
        union_value.u_value = UnalignedLoad(reinterpret_cast<uint32_t *>(block + byte_index));
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
        if (std::isnan(union_value.value)) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
            return builtins_common::GetTaggedDouble(union_value.value);
        }
        if (!little_endian) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
            return builtins_common::GetTaggedDouble(bit_cast<T>(BSWAP(union_value.u_value)));
        }
        // NOLINTNEXTLINE(readability-misleading-indentation)
    } else if constexpr (std::is_same_v<T, double>) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access,cppcoreguidelines-pro-bounds-pointer-arithmetic)
        union_value.u_value = UnalignedLoad(reinterpret_cast<uint64_t *>(block + byte_index));
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
        if (std::isnan(union_value.value) && !JSTaggedValue::IsImpureNaN(union_value.value)) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
            return builtins_common::GetTaggedDouble(union_value.value);
        }
        if (!little_endian) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
            return builtins_common::GetTaggedDouble(bit_cast<T>(BSWAP(union_value.u_value)));
        }
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
    return builtins_common::GetTaggedDouble(union_value.value);
}

// 24.1.1.5
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue builtins::array_buffer::GetValueFromBuffer(JSThread *thread, JSHandle<JSTaggedValue> arr_buf,
                                                         int32_t byte_index, DataViewType type, bool little_endian)
{
    JSArrayBuffer *js_array_buffer = JSArrayBuffer::Cast(arr_buf->GetTaggedObject());
    JSTaggedValue data = js_array_buffer->GetArrayBufferData();
    void *pointer = JSNativePointer::Cast(data.GetTaggedObject())->GetExternalPointer();
    auto *block = reinterpret_cast<uint8_t *>(pointer);
    switch (type) {
        case DataViewType::UINT8:
        case DataViewType::UINT8_CLAMPED: {
            uint8_t res = block[byte_index];  // NOLINT
            return builtins_common::GetTaggedInt(res);
        }
        case DataViewType::INT8: {
            uint8_t res = block[byte_index];  // NOLINT
            auto int8_res = static_cast<int8_t>(res);
            return builtins_common::GetTaggedInt(int8_res);
        }
        case DataViewType::UINT16:
            return GetValueFromBufferForInteger<uint16_t, NumberSize::UINT16>(block, byte_index, little_endian);
        case DataViewType::INT16:
            return GetValueFromBufferForInteger<int16_t, NumberSize::INT16>(block, byte_index, little_endian);
        case DataViewType::UINT32:
            return GetValueFromBufferForInteger<uint32_t, NumberSize::UINT32>(block, byte_index, little_endian);
        case DataViewType::INT32:
            return GetValueFromBufferForInteger<int32_t, NumberSize::INT32>(block, byte_index, little_endian);
        case DataViewType::FLOAT32:
            return GetValueFromBufferForFloat<float, UnionType32, NumberSize::FLOAT32>(block, byte_index,
                                                                                       little_endian);
        case DataViewType::FLOAT64:
            return GetValueFromBufferForFloat<double, UnionType64, NumberSize::FLOAT64>(block, byte_index,
                                                                                        little_endian);
        case DataViewType::BIGINT64:
            return GetValueFromBufferForBigInt<int64_t, NumberSize::BIGINT64>(thread, block, byte_index, little_endian);
        case DataViewType::BIGUINT64:
            return GetValueFromBufferForBigInt<uint64_t, NumberSize::BIGUINT64>(thread, block, byte_index,
                                                                                little_endian);
        default:
            break;
    }

    UNREACHABLE();
}

template <typename T>
void SetTypeData(uint8_t *block, T value, int32_t index)
{
    int32_t size_count = sizeof(T);
    auto *res = reinterpret_cast<uint8_t *>(&value);
    for (int i = 0; i < size_count; i++) {
        *(block + index + i) = *(res + i);  // NOLINT
    }
}

template <typename T>
void SetValueInBufferForByte(double val, uint8_t *block, int32_t byte_index)
{
    static_assert(std::is_same_v<T, uint8_t> || std::is_same_v<T, int8_t>, "T must be int8/uint8");
    T res;
    if (std::isnan(val) || std::isinf(val)) {
        res = 0;
        SetTypeData(block, res, byte_index);
        return;
    }
    auto int64_val = static_cast<int64_t>(val);
    res = UnalignedLoad(reinterpret_cast<T *>(&int64_val));
    SetTypeData(block, res, byte_index);
}

void SetValueInBufferForUint8Clamped(double val, uint8_t *block, int32_t byte_index)
{
    uint8_t res;
    if (std::isnan(val) || val <= 0) {
        res = 0;
        SetTypeData(block, res, byte_index);
        return;
    }
    val = val >= UINT8_MAX ? UINT8_MAX : val;
    constexpr double HALF = 0.5;
    val = val == HALF ? 0 : std::round(val);
    res = static_cast<int64_t>(val);
    SetTypeData(block, res, byte_index);
}

template <typename T>
void SetValueInBufferForInteger(double val, uint8_t *block, int32_t byte_index, bool little_endian)
{
    static_assert(std::is_integral_v<T>, "T must be integral");
    static_assert(sizeof(T) >= sizeof(uint16_t), "T must have a size more than uint8");
    T res;
    if (std::isnan(val) || std::isinf(val)) {
        res = 0;
        SetTypeData(block, res, byte_index);
        return;
    }
    auto int64_val = static_cast<int64_t>(val);
    // NOLINTNEXTLINE(readability-braces-around-statements)
    if constexpr (std::is_same_v<T, uint16_t>) {
        auto tmp = UnalignedLoad(reinterpret_cast<int16_t *>(&int64_val));
        res = static_cast<T>(tmp);
    } else {  // NOLINTNEXTLINE(readability-braces-around-statements)
        res = UnalignedLoad(reinterpret_cast<T *>(&int64_val));
    }

    if (!little_endian) {
        res = BSWAP<T>(res);
    }
    SetTypeData(block, res, byte_index);
}

template <typename T>
void SetValueInBufferForFloat(double val, uint8_t *block, int32_t byte_index, bool little_endian)
{
    static_assert(std::is_same_v<T, float> || std::is_same_v<T, double>, "T must be float type");
    auto data = static_cast<T>(val);
    if (std::isnan(val)) {
        SetTypeData(block, data, byte_index);
        return;
    }
    if (!little_endian) {
        // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
        if constexpr (std::is_same_v<T, float>) {
            data = bit_cast<T>(BSWAP(bit_cast<uint32_t>(data)));
        }
        // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
        if constexpr (std::is_same_v<T, double>) {
            data = bit_cast<T>(BSWAP(bit_cast<uint64_t>(data)));
        }
    }
    SetTypeData(block, data, byte_index);
}

template <typename T>
void SetValueInBufferForBigInt(JSThread *thread, const JSHandle<JSTaggedValue> &val, uint8_t *block,
                               uint32_t byte_index, bool little_endian)
{
    static_assert(std::is_same_v<T, int64_t> || std::is_same_v<T, uint64_t>, "T must be int64_t/uint64_t");
    T value = 0;
    bool lossless = true;
    // NOLINTNEXTLINE(readability-braces-around-statements)
    if constexpr (std::is_same_v<T, uint64_t>) {
        BigInt::BigIntToUint64(thread, val, reinterpret_cast<uint64_t *>(&value), &lossless);
        // NOLINTNEXTLINE(readability-misleading-indentation)
    } else {
        BigInt::BigIntToInt64(thread, val, reinterpret_cast<int64_t *>(&value), &lossless);
    }
    RETURN_IF_ABRUPT_COMPLETION(thread);
    if (!little_endian) {
        value = BSWAP<T>(value);
    }
    SetTypeData(block, value, byte_index);
}

// es12 25.1.2.7 IsBigIntElementType ( type )
bool IsBigIntElementType(DataViewType type)
{
    return type == DataViewType::BIGINT64 || type == DataViewType::BIGUINT64;
}

// 24.1.1.6
JSTaggedValue builtins::array_buffer::SetValueInBuffer(JSThread *thread, JSHandle<JSTaggedValue> arr_buf,
                                                       int32_t byte_index, DataViewType type,
                                                       const JSHandle<JSTaggedValue> &value, bool little_endian)
{
    JSArrayBuffer *js_array_buffer = JSArrayBuffer::Cast(arr_buf->GetTaggedObject());
    JSTaggedValue data = js_array_buffer->GetArrayBufferData();
    void *pointer = JSNativePointer::Cast(data.GetTaggedObject())->GetExternalPointer();
    auto *block = reinterpret_cast<uint8_t *>(pointer);

    if (IsBigIntElementType(type)) {
        switch (type) {
            case DataViewType::BIGINT64:
                SetValueInBufferForBigInt<int64_t>(thread, value, block, byte_index, little_endian);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                break;
            case DataViewType::BIGUINT64:
                SetValueInBufferForBigInt<uint64_t>(thread, value, block, byte_index, little_endian);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                break;
            default:
                UNREACHABLE();
        }
        return JSTaggedValue::Undefined();
    }

    double val = value->GetNumber();

    switch (type) {
        case DataViewType::UINT8:
            SetValueInBufferForByte<uint8_t>(val, block, byte_index);
            break;
        case DataViewType::UINT8_CLAMPED:
            SetValueInBufferForUint8Clamped(val, block, byte_index);
            break;
        case DataViewType::INT8:
            SetValueInBufferForByte<int8_t>(val, block, byte_index);
            break;
        case DataViewType::UINT16:
            SetValueInBufferForInteger<uint16_t>(val, block, byte_index, little_endian);
            break;
        case DataViewType::INT16:
            SetValueInBufferForInteger<int16_t>(val, block, byte_index, little_endian);
            break;
        case DataViewType::UINT32:
            SetValueInBufferForInteger<uint32_t>(val, block, byte_index, little_endian);
            break;
        case DataViewType::INT32:
            SetValueInBufferForInteger<int32_t>(val, block, byte_index, little_endian);
            break;
        case DataViewType::FLOAT32:
            SetValueInBufferForFloat<float>(val, block, byte_index, little_endian);
            break;
        case DataViewType::FLOAT64:
            SetValueInBufferForFloat<double>(val, block, byte_index, little_endian);
            break;
        default:
            UNREACHABLE();
    }
    return JSTaggedValue::Undefined();
}

}  // namespace panda::ecmascript::builtins
