/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/builtins/builtins_regexp.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string-inl.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_locale.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_regexp.h"
#include "plugins/ecmascript/runtime/js_string_iterator.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "unicode/normalizer2.h"
#include "unicode/normlzr.h"
#include "unicode/unistr.h"

namespace panda::ecmascript::builtins {
using ObjectFactory = ecmascript::ObjectFactory;
using JSArray = ecmascript::JSArray;

constexpr int32_t ENCODE_MAX_UTF16 = 0X10FFFF;
constexpr uint16_t ENCODE_LEAD_LOW = 0xD800;
constexpr uint16_t ENCODE_TRAIL_LOW = 0xDC00;
constexpr uint32_t ENCODE_FIRST_FACTOR = 0x400;
constexpr uint32_t ENCODE_SECOND_FACTOR = 0x10000;

static int32_t ConvertDoubleToInt(double d);
// 21.1.3
static JSTaggedValue ThisStringValue(JSThread *thread, JSTaggedValue value);

// 21.1.1.1 String(value)
JSTaggedValue string::StringConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), String, StringConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (argv->GetArgsNumber() > 0) {
        JSHandle<JSTaggedValue> val_tag_new = builtins_common::GetCallArg(argv, 0);
        if (new_target->IsUndefined() && val_tag_new->IsSymbol()) {
            return symbol::SymbolDescriptiveString(thread, val_tag_new.GetTaggedValue());
        }
        JSHandle<EcmaString> str = JSTaggedValue::ToString(thread, val_tag_new);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (new_target->IsUndefined()) {
            return str.GetTaggedValue();
        }
        JSHandle<JSTaggedValue> str_tag(str);
        return JSPrimitiveRef::StringCreate(thread, str_tag).GetTaggedValue();
    }
    JSHandle<EcmaString> val = factory->GetEmptyString();
    JSHandle<JSTaggedValue> val_tag(val);
    if (new_target->IsUndefined()) {
        return factory->GetEmptyString().GetTaggedValue();
    }
    return JSPrimitiveRef::StringCreate(thread, val_tag).GetTaggedValue();
}

// 21.1.2.1
JSTaggedValue string::FromCharCode(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), String, FromCharCode);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    int32_t arg_length = argv->GetArgsNumber();
    if (arg_length == 0) {
        return factory->GetEmptyString().GetTaggedValue();
    }
    JSHandle<JSTaggedValue> code_point_tag = builtins_common::GetCallArg(argv, 0);
    uint16_t code_point_value = JSTaggedValue::ToUint16(thread, code_point_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<EcmaString> str_handle = factory->NewFromUtf16Literal(&code_point_value, 1);
    if (arg_length == 1) {
        return str_handle.GetTaggedValue();
    }
    std::u16string u16str = ecmascript::base::StringHelper::Utf16ToU16String(&code_point_value, 1);
    PandaVector<uint16_t> value_table;
    value_table.reserve(arg_length - 1);
    for (int32_t i = 1; i < arg_length; i++) {
        JSHandle<JSTaggedValue> next_cp = builtins_common::GetCallArg(argv, i);
        uint16_t next_cv = JSTaggedValue::ToUint16(thread, next_cp);
        value_table.emplace_back(next_cv);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    std::u16string next_u16str = base::StringHelper::Utf16ToU16String(value_table.data(), arg_length - 1);
    u16str = base::StringHelper::Append(u16str, next_u16str);
    const char16_t *const_char16t_data = u16str.data();
    auto *char16t_data = const_cast<char16_t *>(const_char16t_data);
    auto *uint16t_data = reinterpret_cast<uint16_t *>(char16t_data);
    int32_t u16str_size = u16str.size();
    return factory->NewFromUtf16Literal(uint16t_data, u16str_size).GetTaggedValue();
}

// 21.1.2.2
JSTaggedValue string::FromCodePoint(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), String, FromCodePoint);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    int32_t arg_length = argv->GetArgsNumber();
    if (arg_length == 0) {
        return factory->GetEmptyString().GetTaggedValue();
    }
    std::u16string u16str;
    int32_t u16str_size = arg_length;
    for (int i = 0; i < arg_length; i++) {
        JSHandle<JSTaggedValue> next_cp_tag = builtins_common::GetCallArg(argv, i);
        JSTaggedNumber next_cp_val = JSTaggedValue::ToNumber(thread, next_cp_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (!next_cp_val.IsInteger()) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "is not integer", JSTaggedValue::Exception());
        }
        int32_t cp = next_cp_val.ToInt32();
        if (cp < 0 || cp > ENCODE_MAX_UTF16) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "CodePoint < 0 or CodePoint > 0x10FFFF", JSTaggedValue::Exception());
        }
        if (cp == 0) {
            PandaVector<uint16_t> data {0x00};
            return factory->NewFromUtf16Literal(data.data(), 1).GetTaggedValue();
        }
        if (cp > UINT16_MAX) {
            uint16_t cu1 = std::floor((cp - ENCODE_SECOND_FACTOR) / ENCODE_FIRST_FACTOR) + ENCODE_LEAD_LOW;
            uint16_t cu2 = ((cp - ENCODE_SECOND_FACTOR) % ENCODE_FIRST_FACTOR) + ENCODE_TRAIL_LOW;
            std::u16string next_u16str1 = ecmascript::base::StringHelper::Utf16ToU16String(&cu1, 1);
            std::u16string next_u16str2 = ecmascript::base::StringHelper::Utf16ToU16String(&cu2, 1);
            u16str = ecmascript::base::StringHelper::Append(u16str, next_u16str1);
            u16str = ecmascript::base::StringHelper::Append(u16str, next_u16str2);
            u16str_size++;
        } else {
            auto u16t_cp = static_cast<uint16_t>(cp);
            std::u16string next_u16str = ecmascript::base::StringHelper::Utf16ToU16String(&u16t_cp, 1);
            u16str = ecmascript::base::StringHelper::Append(u16str, next_u16str);
        }
    }
    const char16_t *const_char16t_data = u16str.data();
    auto *char16t_data = const_cast<char16_t *>(const_char16t_data);
    auto *uint16t_data = reinterpret_cast<uint16_t *>(char16t_data);
    return factory->NewFromUtf16Literal(uint16t_data, u16str_size).GetTaggedValue();
}

// 21.1.2.4
JSTaggedValue string::Raw(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), String, Raw);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // Let cooked be ToObject(template).
    JSHandle<JSObject> cooked = JSTaggedValue::ToObject(thread, builtins_common::GetCallArg(argv, 0));
    // ReturnIfAbrupt(cooked).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // Let raw be ToObject(Get(cooked, "raw")).
    JSHandle<JSTaggedValue> raw_key(factory->NewFromCanBeCompressString("raw"));
    JSHandle<JSTaggedValue> raw_tag =
        JSObject::GetProperty(thread, JSHandle<JSTaggedValue>::Cast(cooked), raw_key).GetValue();
    JSHandle<JSObject> raw_obj = JSTaggedValue::ToObject(thread, raw_tag);
    // ReturnIfAbrupt(rawObj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> raw_len =
        JSObject::GetProperty(thread, JSHandle<JSTaggedValue>::Cast(raw_obj), length_key).GetValue();
    // ReturnIfAbrupt(rawLen).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSTaggedNumber length_number = JSTaggedValue::ToLength(thread, raw_len);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int length = length_number.ToUint32();
    if (length <= 0) {
        return factory->GetEmptyString().GetTaggedValue();
    }

    std::u16string u16str;
    int argc = static_cast<int>(argv->GetArgsNumber()) - 1;
    bool can_be_compress = true;
    for (int i = 0, args_i = 1; i < length; ++i, ++args_i) {
        // Let nextSeg be ToString(Get(raw, nextKey)).
        JSHandle<JSTaggedValue> element_string =
            JSObject::GetProperty(thread, JSHandle<JSTaggedValue>::Cast(raw_obj), i).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        EcmaString *next_seg = *JSTaggedValue::ToString(thread, element_string);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (next_seg->IsUtf16()) {
            u16str += ecmascript::base::StringHelper::Utf16ToU16String(next_seg->GetDataUtf16(), next_seg->GetLength());
            can_be_compress = false;
        } else {
            u16str += ecmascript::base::StringHelper::Utf8ToU16String(next_seg->GetDataUtf8(), next_seg->GetLength());
        }
        if (i + 1 == length) {
            break;
        }
        if (args_i <= argc) {
            EcmaString *next_sub = *JSTaggedValue::ToString(thread, builtins_common::GetCallArg(argv, args_i));
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (next_sub->IsUtf16()) {
                u16str +=
                    ecmascript::base::StringHelper::Utf16ToU16String(next_sub->GetDataUtf16(), next_sub->GetLength());
                can_be_compress = false;
            } else {
                u16str +=
                    ecmascript::base::StringHelper::Utf8ToU16String(next_sub->GetDataUtf8(), next_sub->GetLength());
            }
        }
    }
    // return the result string
    auto *uint16t_data = reinterpret_cast<uint16_t *>(const_cast<char16_t *>(u16str.data()));
    return factory->NewFromUtf16LiteralUnCheck(uint16t_data, u16str.size(), can_be_compress).GetTaggedValue();
}

// 21.1.3.1
JSTaggedValue string::proto::CharAt(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, CharAt);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 0);
    int32_t pos;
    if (pos_tag->IsUndefined()) {
        pos = 0;
    } else {
        JSTaggedNumber pos_val = JSTaggedValue::ToInteger(thread, pos_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        pos = pos_val.ToInt32();
    }
    if (pos < 0 || pos >= this_len) {
        return factory->GetEmptyString().GetTaggedValue();
    }
    uint16_t res = this_handle->At<false>(pos);
    return factory->NewFromUtf16Literal(&res, 1).GetTaggedValue();
}

// 21.1.3.2
JSTaggedValue string::proto::CharCodeAt(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, CharCodeAt);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 0);
    int32_t pos;
    if (pos_tag->IsUndefined()) {
        pos = 0;
    } else {
        JSTaggedNumber pos_val = JSTaggedValue::ToInteger(thread, pos_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        pos = pos_val.ToInt32();
    }
    if (pos < 0 || pos >= this_len) {
        return builtins_common::GetTaggedDouble(ecmascript::base::NAN_VALUE);
    }
    uint16_t ret = this_handle->At<false>(pos);
    return builtins_common::GetTaggedInt(ret);
}

// 21.1.3.3
JSTaggedValue string::proto::CodePointAt(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, CodePointAt);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 0);

    JSTaggedNumber pos_val = JSTaggedValue::ToNumber(thread, pos_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t pos = ecmascript::base::NumberHelper::DoubleInRangeInt32(pos_val.GetNumber());
    int32_t this_len = this_handle->GetLength();
    if (pos < 0 || pos >= this_len) {
        return JSTaggedValue::Undefined();
    }
    uint16_t first = this_handle->At<false>(pos);
    if (first < utf::DECODE_LEAD_LOW || first > utf::DECODE_LEAD_HIGH || pos + 1 == this_len) {
        return builtins_common::GetTaggedInt(first);
    }
    uint16_t second = this_handle->At<false>(pos + 1);
    if (second < utf::DECODE_TRAIL_LOW || second > utf::DECODE_TRAIL_HIGH) {
        return builtins_common::GetTaggedInt(first);
    }
    uint32_t res = utf::UTF16Decode(first, second);
    return builtins_common::GetTaggedInt(res);
}

// 21.1.3.4
JSTaggedValue string::proto::Concat(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Concat);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    int32_t arg_length = argv->GetArgsNumber();
    if (arg_length == 0) {
        return this_handle.GetTaggedValue();
    }
    std::u16string u16str_this;
    std::u16string u16str_next;
    bool can_be_compress = true;
    if (this_handle->IsUtf16()) {
        u16str_this = ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_len);
        can_be_compress = false;
    } else {
        u16str_this = ecmascript::base::StringHelper::Utf8ToU16String(this_handle->GetDataUtf8(), this_len);
    }
    for (int i = 0; i < arg_length; i++) {
        JSHandle<JSTaggedValue> next_tag = builtins_common::GetCallArg(argv, i);
        JSHandle<EcmaString> next_handle = JSTaggedValue::ToString(thread, next_tag);
        int32_t next_len = next_handle->GetLength();
        if (next_handle->IsUtf16()) {
            u16str_next = ecmascript::base::StringHelper::Utf16ToU16String(next_handle->GetDataUtf16(), next_len);
            can_be_compress = false;
        } else {
            u16str_next = ecmascript::base::StringHelper::Utf8ToU16String(next_handle->GetDataUtf8(), next_len);
        }
        u16str_this = ecmascript::base::StringHelper::Append(u16str_this, u16str_next);
    }
    const char16_t *const_char16t_data = u16str_this.data();
    auto *char16t_data = const_cast<char16_t *>(const_char16t_data);
    auto *uint16t_data = reinterpret_cast<uint16_t *>(char16t_data);
    int32_t u16str_size = u16str_this.size();
    return factory->NewFromUtf16LiteralUnCheck(uint16t_data, u16str_size, can_be_compress).GetTaggedValue();
}

// 21.1.3.5 String.prototype.constructor
// 21.1.3.6
JSTaggedValue string::proto::EndsWith(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, EndsWith);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<JSTaggedValue> search_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    bool is_regexp = JSObject::IsRegExp(thread, search_tag);
    if (is_regexp) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "is regexp", JSTaggedValue::Exception());
    }
    JSHandle<EcmaString> search_handle = JSTaggedValue::ToString(thread, search_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    int32_t search_len = search_handle->GetLength();
    int32_t pos;
    JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 1);
    if (pos_tag->IsUndefined()) {
        pos = this_len;
    } else {
        JSTaggedNumber pos_val = JSTaggedValue::ToInteger(thread, pos_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        pos = pos_val.ToInt32();
    }
    int32_t end = std::min(std::max(pos, 0), this_len);
    int32_t start = end - search_len;
    if (start < 0) {
        return builtins_common::GetTaggedBoolean(false);
    }
    std::u16string u16str_this;
    std::u16string u16str_search;
    if (this_handle->IsUtf16()) {
        u16str_this = ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_len);
    } else {
        const uint8_t *uint8_this = this_handle->GetDataUtf8();
        u16str_this = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, this_len);
    }
    if (search_handle->IsUtf16()) {
        u16str_search = ecmascript::base::StringHelper::Utf16ToU16String(search_handle->GetDataUtf16(), search_len);
    } else {
        const uint8_t *uint8_search = search_handle->GetDataUtf8();
        u16str_search = ecmascript::base::StringHelper::Utf8ToU16String(uint8_search, search_len);
    }
    int32_t idx = ecmascript::base::StringHelper::Find(u16str_this, u16str_search, start);
    if (idx == start) {
        return builtins_common::GetTaggedBoolean(true);
    }
    return builtins_common::GetTaggedBoolean(false);
}

// 21.1.3.7
JSTaggedValue string::proto::Includes(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Includes);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> search_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    bool is_regexp = JSObject::IsRegExp(thread, search_tag);
    if (is_regexp) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "is regexp", JSTaggedValue::Exception());
    }
    JSHandle<EcmaString> search_handle = JSTaggedValue::ToString(thread, search_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    int32_t search_len = search_handle->GetLength();
    int32_t pos = 0;
    JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 1);
    if (argv->GetArgsNumber() == 1) {
        pos = 0;
    } else {
        JSTaggedNumber pos_val = JSTaggedValue::ToNumber(thread, pos_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        pos = ecmascript::base::NumberHelper::DoubleInRangeInt32(pos_val.GetNumber());
    }
    int32_t start = std::min(std::max(pos, 0), this_len);
    std::u16string u16str_this;
    std::u16string u16str_search;
    if (this_handle->IsUtf16()) {
        u16str_this = ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_len);
    } else {
        const uint8_t *uint8_this = this_handle->GetDataUtf8();
        u16str_this = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, this_len);
    }
    if (search_handle->IsUtf16()) {
        u16str_search = ecmascript::base::StringHelper::Utf16ToU16String(search_handle->GetDataUtf16(), search_len);
    } else {
        const uint8_t *uint8_search = search_handle->GetDataUtf8();
        u16str_search = ecmascript::base::StringHelper::Utf8ToU16String(uint8_search, search_len);
    }
    int32_t idx = ecmascript::base::StringHelper::Find(u16str_this, u16str_search, start);
    if (idx < 0 || idx > this_len) {
        return builtins_common::GetTaggedBoolean(false);
    }
    return builtins_common::GetTaggedBoolean(true);
}

// 21.1.3.8
JSTaggedValue string::proto::IndexOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, IndexOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> search_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    uint32_t this_len = this_handle->GetLength();
    JSHandle<EcmaString> search_handle = JSTaggedValue::ToString(thread, search_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 1);
    int32_t pos;
    if (pos_tag->IsInt()) {
        pos = pos_tag->GetInt();
    } else if (pos_tag->IsUndefined()) {
        pos = 0;
    } else {
        JSTaggedNumber pos_val = JSTaggedValue::ToInteger(thread, pos_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        pos = pos_val.ToInt32();
    }
    pos = std::min(std::max(pos, 0), static_cast<int32_t>(this_len));
    int32_t res = this_handle->IndexOf(*search_handle, pos);
    if (res >= 0 && res < static_cast<int32_t>(this_len)) {
        return builtins_common::GetTaggedInt(res);
    }
    return builtins_common::GetTaggedInt(-1);
}

// 21.1.3.9
JSTaggedValue string::proto::LastIndexOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, LastIndexOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> search_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    JSHandle<EcmaString> search_handle = JSTaggedValue::ToString(thread, search_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t search_len = search_handle->GetLength();
    int32_t pos;
    if (argv->GetArgsNumber() == 1) {
        pos = this_len;
    } else {
        JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 1);
        JSTaggedNumber pos_val = JSTaggedValue::ToInteger(thread, pos_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (std::isnan(JSTaggedValue::ToNumber(thread, pos_tag).GetNumber())) {
            pos = this_len;
        } else {
            pos = pos_val.ToInt32();
        }
    }
    pos = std::min(std::max(pos, 0), this_len);
    std::u16string u16str_this;
    std::u16string u16str_search;
    if (this_handle->IsUtf16()) {
        u16str_this = ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_len);
    } else {
        const uint8_t *uint8_this = this_handle->GetDataUtf8();
        u16str_this = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, this_len);
    }
    if (search_handle->IsUtf16()) {
        u16str_search = ecmascript::base::StringHelper::Utf16ToU16String(search_handle->GetDataUtf16(), search_len);
    } else {
        const uint8_t *uint8_search = search_handle->GetDataUtf8();
        u16str_search = ecmascript::base::StringHelper::Utf8ToU16String(uint8_search, search_len);
    }
    int32_t res = ecmascript::base::StringHelper::RFind(u16str_this, u16str_search, pos);
    if (res >= 0 && res < this_len) {
        return builtins_common::GetTaggedInt(res);
    }
    res = -1;
    return builtins_common::GetTaggedInt(res);
}

// 21.1.3.10
JSTaggedValue string::proto::LocaleCompare(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, LocaleCompare);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> that_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<EcmaString> that_handle = JSTaggedValue::ToString(thread, that_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t res = this_handle->Compare(*that_handle);
    return builtins_common::GetTaggedInt(res);
}

// 21.1.3.11
JSTaggedValue string::proto::Match(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Match);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<JSTaggedValue> regexp = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> match_tag = thread->GetEcmaVM()->GetGlobalEnv()->GetMatchSymbol();
    if (regexp->IsJSRegExp()) {
        JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
        JSHandle<JSRegExp> re(regexp);
        JSHandle<JSTaggedValue> pattern(thread, re->GetOriginalSource());
        JSHandle<JSTaggedValue> flags(thread, re->GetOriginalFlags());
        JSTaggedValue cache_result =
            cache_table->FindCachedResult(thread, pattern, flags, this_tag, RegExpExecResultCache::MATCH_TYPE, regexp);
        if (cache_result != JSTaggedValue::Undefined()) {
            return cache_result;
        }
    }
    if (!regexp->IsUndefined() && !regexp->IsNull()) {
        if (regexp->IsECMAObject()) {
            JSHandle<JSTaggedValue> matcher = JSObject::GetMethod(thread, regexp, match_tag);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (!matcher->IsUndefined()) {
                ASSERT(matcher->IsJSFunction());

                auto info = NewRuntimeCallInfo(thread, matcher, regexp, JSTaggedValue::Undefined(), 1);
                info->SetCallArgs(this_tag);
                return JSFunction::Call(info.Get());
            }
        }
    }
    JSHandle<EcmaString> this_val = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> undifined_handle = global_const->GetHandledUndefined();
    JSHandle<JSTaggedValue> rx(thread, reg_exp::RegExpCreate(thread, regexp, undifined_handle));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    auto info = NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), rx, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(this_val.GetTaggedValue());
    return JSFunction::Invoke(info.Get(), match_tag);
}

JSTaggedValue string::proto::MatchAll(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, MatchAll);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 1. Let O be ? RequireObjectCoercible(this value).
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<JSTaggedValue> regexp = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> match_all_tag = thread->GetEcmaVM()->GetGlobalEnv()->GetMatchAllSymbol();
    JSHandle<JSTaggedValue> gvalue(global_const->GetHandledGString());

    // 2. If regexp is neither undefined nor null, then
    if (!regexp->IsUndefined() && !regexp->IsNull()) {
        // a. Let isRegExp be ? IsRegExp(searchValue).
        bool is_js_reg_exp = JSObject::IsRegExp(thread, regexp);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // b. If isRegExp is true, then
        if (is_js_reg_exp) {
            // i. Let flags be ? Get(searchValue, "flags").
            JSHandle<JSTaggedValue> flags_string(global_const->GetHandledFlagsString());
            JSHandle<JSTaggedValue> flags = JSObject::GetProperty(thread, regexp, flags_string).GetValue();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // ii. Perform ? RequireObjectCoercible(flags).
            JSTaggedValue::RequireObjectCoercible(thread, flags);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // iii. If ? ToString(flags) does not contain "g", throw a TypeError exception.
            JSHandle<EcmaString> flag_string = JSTaggedValue::ToString(thread, flags);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            int32_t pos = flag_string->IndexOf(static_cast<EcmaString *>(gvalue->GetTaggedObject()));
            if (pos == -1) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "matchAll called with a non-global RegExp argument",
                                            JSTaggedValue::Exception());
            }
        }

        if (regexp->IsECMAObject()) {
            // c. c. Let matcher be ? GetMethod(regexp, @@matchAll).
            // d. d. If matcher is not undefined, then
            JSHandle<JSTaggedValue> matcher = JSObject::GetMethod(thread, regexp, match_all_tag);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (!matcher->IsUndefined()) {
                ASSERT(matcher->IsJSFunction());
                // i. i. Return ? Call(matcher, regexp, « O »).

                auto info = NewRuntimeCallInfo(thread, matcher, regexp, JSTaggedValue::Undefined(), 1);
                info->SetCallArgs(this_tag.GetTaggedValue());
                return JSFunction::Call(info.Get());
            }
        }
    }
    // 3. Let S be ? ToString(O).
    JSHandle<EcmaString> this_val = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 4. Let rx be ? RegExpCreate(regexp, "g").
    JSHandle<JSTaggedValue> rx(thread, reg_exp::RegExpCreate(thread, regexp, gvalue));

    auto info = NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), rx, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(this_val.GetTaggedValue());
    return JSFunction::Invoke(info.Get(), match_all_tag);
}

// 21.1.3.12
JSTaggedValue string::proto::Normalize(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Normalize);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
    JSHandle<EcmaString> form_value;
    if (argv->GetArgsNumber() == 0) {
        form_value = factory->NewFromString("NFC");
    } else {
        JSHandle<JSTaggedValue> form_tag = builtins_common::GetCallArg(argv, 0);
        if (form_tag->IsUndefined()) {
            form_value = factory->NewFromString("NFC");
        } else {
            form_value = JSTaggedValue::ToString(thread, form_tag);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        }
    }
    JSHandle<EcmaString> nfc = factory->NewFromString("NFC");
    JSHandle<EcmaString> nfd = factory->NewFromString("NFD");
    JSHandle<EcmaString> nfkc = factory->NewFromString("NFKC");
    JSHandle<EcmaString> nfkd = factory->NewFromString("NFKD");
    if (form_value->Compare(*nfc) != 0 && form_value->Compare(*nfd) != 0 && form_value->Compare(*nfkc) != 0 &&
        form_value->Compare(*nfkd) != 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "compare not equal", JSTaggedValue::Exception());
    }
    std::u16string u16str_this;
    if (this_handle->IsUtf16()) {
        u16str_this =
            ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_handle->GetLength());
    } else {
        const uint8_t *uint8_this = this_handle->GetDataUtf8();
        u16str_this = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, this_handle->GetLength());
    }
    const char16_t *const_char16t_data = u16str_this.data();
    icu::UnicodeString src(const_char16t_data);
    icu::UnicodeString res;
    UErrorCode error_code = U_ZERO_ERROR;
    UNormalizationMode u_form;
    int32_t option = 0;
    if (form_value->Compare(*nfc) == 0) {
        u_form = UNORM_NFC;
    } else if (form_value->Compare(*nfd) == 0) {
        u_form = UNORM_NFD;
    } else if (form_value->Compare(*nfkc) == 0) {
        u_form = UNORM_NFKC;
    } else if (form_value->Compare(*nfkd) == 0) {
        u_form = UNORM_NFKD;
    } else {
        UNREACHABLE();
    }

    icu::Normalizer::normalize(src, u_form, option, res, error_code);
    JSHandle<EcmaString> str = JSLocale::IcuToString(thread, res);
    return JSTaggedValue(*str);
}

// ES2021 22.1.3.14
JSTaggedValue string::proto::PadEnd(EcmaRuntimeCallInfo *argv)
{
    // 1. Let O be ? RequireObjectCoercible(this value).
    JSThread *thread = argv->GetThread();
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Return ? StringPad(O, maxLength, fillString, start).
    return base::StringHelper::StringPad(thread, this_tag, builtins_common::GetCallArg(argv, 0),
                                         builtins_common::GetCallArg(argv, 1), base::PadPlacement::END);
}

// ES2021 22.1.3.15
JSTaggedValue string::proto::PadStart(EcmaRuntimeCallInfo *argv)
{
    // 1. Let O be ? RequireObjectCoercible(this value).
    JSThread *thread = argv->GetThread();
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Return ? StringPad(O, maxLength, fillString, start).
    return base::StringHelper::StringPad(thread, this_tag, builtins_common::GetCallArg(argv, 0),
                                         builtins_common::GetCallArg(argv, 1), base::PadPlacement::START);
}

// 21.1.3.13
JSTaggedValue string::proto::Repeat(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Repeat);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    JSHandle<JSTaggedValue> count_tag = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber num = JSTaggedValue::ToInteger(thread, count_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double d = num.GetNumber();
    if (d < 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "less than 0", JSTaggedValue::Exception());
    }
    if (d == ecmascript::base::POSITIVE_INFINITY) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "is infinity", JSTaggedValue::Exception());
    }
    int32_t count = ecmascript::base::NumberHelper::DoubleInRangeInt32(d);
    std::u16string u16str_this;
    bool can_be_compress = true;
    if (this_handle->IsUtf16()) {
        u16str_this = ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_len);
        can_be_compress = false;
    } else {
        const uint8_t *uint8_this = this_handle->GetDataUtf8();
        u16str_this = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, this_len);
    }
    if (this_len == 0) {
        return this_handle.GetTaggedValue();
    }

    EcmaString *res = ecmascript::base::StringHelper::Repeat(thread, u16str_this, count, can_be_compress);
    return JSTaggedValue(res);
}

// 21.1.3.14
JSTaggedValue string::proto::Replace(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Replace);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_tag = JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> search_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> replace_tag = builtins_common::GetCallArg(argv, 1);

    ObjectFactory *factory = ecma_vm->GetFactory();

    if (search_tag->IsJSRegExp() && replace_tag->IsString()) {
        JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
        JSHandle<JSRegExp> re(search_tag);
        JSHandle<JSTaggedValue> pattern(thread, re->GetOriginalSource());
        JSHandle<JSTaggedValue> flags(thread, re->GetOriginalFlags());
        JSTaggedValue cache_result =
            cache_table->FindCachedResult(thread, pattern, flags, this_tag, RegExpExecResultCache::REPLACE_TYPE,
                                          search_tag, replace_tag.GetTaggedValue());
        if (cache_result != JSTaggedValue::Undefined()) {
            return cache_result;
        }
    }

    // If searchValue is neither undefined nor null, then
    if (search_tag->IsECMAObject()) {
        JSHandle<JSTaggedValue> replace_key = env->GetReplaceSymbol();
        // Let replacer be GetMethod(searchValue, @@replace).
        JSHandle<JSTaggedValue> replace_method = JSObject::GetMethod(thread, search_tag, replace_key);
        // ReturnIfAbrupt(replacer).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // If replacer is not undefined, then
        if (!replace_method->IsUndefined()) {
            // Return Call(replacer, searchValue, «O, replaceValue»).

            auto info = NewRuntimeCallInfo(thread, replace_method, search_tag, JSTaggedValue::Undefined(), 2);
            info->SetCallArgs(this_tag, replace_tag);
            return JSFunction::Call(info.Get());  // 2: two args
        }
    }

    // Let string be ToString(O).
    JSHandle<EcmaString> this_string = JSTaggedValue::ToString(thread, this_tag);
    // ReturnIfAbrupt(string).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // Let searchString be ToString(searchValue).
    JSHandle<EcmaString> search_string = JSTaggedValue::ToString(thread, search_tag);
    // ReturnIfAbrupt(searchString).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // Let functionalReplace be IsCallable(replaceValue).
    if (!replace_tag->IsCallable()) {
        // If functionalReplace is false, then
        // Let replaceValue be ToString(replaceValue).
        // ReturnIfAbrupt(replaceValue)
        replace_tag = JSHandle<JSTaggedValue>(JSTaggedValue::ToString(thread, replace_tag));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // Search string for the first occurrence of searchString and let pos be the index within string of the first code
    // unit of the matched substring and let matched be searchString. If no occurrences of searchString were found,
    // return string.
    int32_t pos = this_string->IndexOf(*search_string);
    if (pos == -1) {
        return this_string.GetTaggedValue();
    }

    JSMutableHandle<JSTaggedValue> repl_handle(thread, factory->GetEmptyString().GetTaggedValue());
    // If functionalReplace is true, then
    if (replace_tag->IsCallable()) {
        // Let replValue be Call(replaceValue, undefined,«matched, pos, and string»).
        auto info = NewRuntimeCallInfo(thread, replace_tag, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 3);
        info->SetCallArgs(search_string, JSTaggedValue(pos), this_string);
        JSTaggedValue repl_str_deocode_value = JSFunction::Call(info.Get());  // 3: «matched, pos, and string»
        repl_handle.Update(repl_str_deocode_value);
    } else {
        JSHandle<JSTaggedValue> undefined = global_const->GetHandledUndefined();
        // Let captures be an empty List.
        JSHandle<TaggedArray> captures_list = factory->EmptyArray();
        ASSERT_PRINT(replace_tag->IsString(), "replace must be string");
        JSHandle<EcmaString> replacement(thread, replace_tag->GetTaggedObject());
        // Let replStr be GetSubstitution(matched, string, pos, captures, replaceValue)
        repl_handle.Update(
            string::GetSubstitution(thread, search_string, this_string, pos, captures_list, undefined, replacement));
    }
    JSHandle<EcmaString> real_replace_str = JSTaggedValue::ToString(thread, repl_handle);
    // Let tailPos be pos + the number of code units in matched.
    int32_t tail_pos = pos + search_string->GetLength();
    // Let newString be the String formed by concatenating the first pos code units of string, replStr, and the trailing
    // substring of string starting at index tailPos. If pos is 0, the first element of the concatenation will be the
    // empty String.
    // Return newString.
    JSHandle<EcmaString> prefix_string(thread, EcmaString::FastSubString(this_string, 0, pos, ecma_vm));
    JSHandle<EcmaString> suffix_string(
        thread, EcmaString::FastSubString(this_string, tail_pos, this_string->GetLength() - tail_pos, ecma_vm));
    JSHandle<EcmaString> temp_string(thread, EcmaString::Concat(prefix_string, real_replace_str, ecma_vm));
    return JSTaggedValue(EcmaString::Concat(temp_string, suffix_string, ecma_vm));
}

// ES2021 22.1.3.18
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue string::proto::ReplaceAll(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, StringPrototype, ReplaceAll);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_tag = JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> search_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> replace_tag = builtins_common::GetCallArg(argv, 1);

    ObjectFactory *factory = ecma_vm->GetFactory();

    if (!search_tag->IsUndefined() && !search_tag->IsNull()) {
        // a. Let isRegExp be ? IsRegExp(searchValue).
        bool is_js_reg_exp = JSObject::IsRegExp(thread, search_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // b. If isRegExp is true, then
        if (is_js_reg_exp) {
            // i. Let flags be ? Get(searchValue, "flags").
            JSHandle<JSTaggedValue> flags_string(global_const->GetHandledFlagsString());
            JSHandle<JSTaggedValue> flags = JSObject::GetProperty(thread, search_tag, flags_string).GetValue();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // ii. Perform ? RequireObjectCoercible(flags).
            JSTaggedValue::RequireObjectCoercible(thread, flags);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // iii. If ? ToString(flags) does not contain "g", throw a TypeError exception.
            JSHandle<EcmaString> flag_string = JSTaggedValue::ToString(thread, flags);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSHandle<EcmaString> g_string(global_const->GetHandledGString());
            int32_t pos = flag_string->IndexOf(*g_string);
            if (pos == -1) {
                THROW_TYPE_ERROR_AND_RETURN(thread,
                                            "string.prototype.replaceAll called with a non-global RegExp argument",
                                            JSTaggedValue::Exception());
            }
        }
        // c. Let replacer be ? GetMethod(searchValue, @@replace).
        JSHandle<JSTaggedValue> replace_key = env->GetReplaceSymbol();
        JSHandle<JSTaggedValue> replace_method = JSObject::GetMethod(thread, search_tag, replace_key);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // d. If replacer is not undefined, then
        if (!replace_method->IsUndefined()) {
            // i. Return ? Call(replacer, searchValue, «O, replaceValue»).

            auto info = NewRuntimeCallInfo(thread, replace_method, search_tag, JSTaggedValue::Undefined(), 2);
            info->SetCallArgs(this_tag.GetTaggedValue(), replace_tag.GetTaggedValue());
            return JSFunction::Call(info.Get());
        }
    }

    // 3. Let string be ? ToString(O).
    JSHandle<EcmaString> this_string = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 4. Let searchString be ? ToString(searchValue).
    JSHandle<EcmaString> search_string = JSTaggedValue::ToString(thread, search_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. Let functionalReplace be IsCallable(replaceValue).
    // 6. If functionalReplace is false, then
    if (!replace_tag->IsCallable()) {
        // a. Set replaceValue to ? ToString(replaceValue).
        replace_tag = JSHandle<JSTaggedValue>(JSTaggedValue::ToString(thread, replace_tag));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    // 7. Let searchLength be the length of searchString.
    // 8. Let advanceBy be max(1, searchLength).
    int32_t search_length = search_string->GetLength();
    int32_t advance_by = std::max(1, search_length);
    // 9. Let matchPositions be a new empty List.
    std::u16string string_builder;
    std::u16string string_prefix_string;
    std::u16string string_real_replace_str;
    std::u16string string_suffix_string;
    // 10. Let position be ! StringIndexOf(string, searchString, 0).
    int32_t pos = this_string->IndexOf(*search_string);
    int32_t end_of_last_match = 0;
    bool can_be_compress = true;
    JSHandle<JSTaggedValue> undefined = global_const->GetHandledUndefined();
    JSMutableHandle<JSTaggedValue> repl_handle(thread, factory->GetEmptyString().GetTaggedValue());
    while (pos != -1) {
        // If functionalReplace is true, then
        if (replace_tag->IsCallable()) {
            // Let replValue be Call(replaceValue, undefined, «matched, pos, and string»).

            auto info = NewRuntimeCallInfo(thread, replace_tag, undefined, JSTaggedValue::Undefined(), 3U);
            info->SetCallArgs(search_string.GetTaggedValue(), JSTaggedValue(pos), this_string.GetTaggedValue());
            JSTaggedValue repl_str_deocode_value = JSFunction::Call(info.Get());
            repl_handle.Update(repl_str_deocode_value);
        } else {
            // Let captures be an empty List.
            JSHandle<TaggedArray> captures_list = factory->NewTaggedArray(0);
            ASSERT_PRINT(replace_tag->IsString(), "replace must be string");
            JSHandle<EcmaString> replacement(thread, replace_tag->GetTaggedObject());
            // Let replStr be GetSubstitution(matched, string, pos, captures, replaceValue)
            repl_handle.Update(string::GetSubstitution(thread, search_string, this_string, pos, captures_list,
                                                       undefined, replacement));
        }
        JSHandle<EcmaString> real_replace_str = JSTaggedValue::ToString(thread, repl_handle);
        // Let tailPos be pos + the number of code units in matched.
        // Let newString be the String formed by concatenating the first pos code units of string,
        // replStr, and the trailing substring of string starting at index tailPos.
        // If pos is 0, the first element of the concatenation will be the
        // empty String.
        // Return newString.
        JSHandle<EcmaString> prefix_string(
            thread, EcmaString::FastSubString(this_string, end_of_last_match, pos - end_of_last_match, ecma_vm));
        if (prefix_string->IsUtf16()) {
            const uint16_t *data = prefix_string->GetDataUtf16();
            string_prefix_string = base::StringHelper::Utf16ToU16String(data, prefix_string->GetLength());
            can_be_compress = false;
        } else {
            const uint8_t *data = prefix_string->GetDataUtf8();
            string_prefix_string = base::StringHelper::Utf8ToU16String(data, prefix_string->GetLength());
        }
        if (real_replace_str->IsUtf16()) {
            const uint16_t *data = real_replace_str->GetDataUtf16();
            string_real_replace_str = base::StringHelper::Utf16ToU16String(data, real_replace_str->GetLength());
            can_be_compress = false;
        } else {
            const uint8_t *data = real_replace_str->GetDataUtf8();
            string_real_replace_str = base::StringHelper::Utf8ToU16String(data, real_replace_str->GetLength());
        }
        string_builder.append(string_prefix_string);
        string_builder.append(string_real_replace_str);
        end_of_last_match = pos + search_length;
        pos = this_string->IndexOf(*search_string, pos + advance_by);
    }

    if (end_of_last_match < static_cast<int32_t>(this_string->GetLength())) {
        JSHandle<EcmaString> suffix_string(
            thread, EcmaString::FastSubString(this_string, end_of_last_match,
                                              this_string->GetLength() - end_of_last_match, ecma_vm));
        if (suffix_string->IsUtf16()) {
            const uint16_t *data = suffix_string->GetDataUtf16();
            string_suffix_string = base::StringHelper::Utf16ToU16String(data, suffix_string->GetLength());
            can_be_compress = false;
        } else {
            const uint8_t *data = suffix_string->GetDataUtf8();
            string_suffix_string = base::StringHelper::Utf8ToU16String(data, suffix_string->GetLength());
        }
        string_builder = string_builder + string_suffix_string;
    }

    auto *char16t_data = const_cast<char16_t *>(string_builder.c_str());
    auto *uint16t_data = reinterpret_cast<uint16_t *>(char16t_data);
    return factory->NewFromUtf16LiteralUnCheck(uint16t_data, string_builder.length(), can_be_compress).GetTaggedValue();
}

// 21.1.3.14.1 Runtime Semantics: GetSubstitution()
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue string::GetSubstitution(JSThread *thread, const JSHandle<EcmaString> &matched,
                                      const JSHandle<EcmaString> &src_string, int position,
                                      const JSHandle<TaggedArray> &capture_list,
                                      const JSHandle<JSTaggedValue> &named_captures,
                                      const JSHandle<EcmaString> &replacement)
{
    BUILTINS_API_TRACE(thread, String, GetSubstitution);
    auto ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<EcmaString> dollar_string =
        JSHandle<EcmaString>::Cast(thread->GlobalConstants()->GetHandledDollarString());
    int32_t replace_length = replacement->GetLength();
    int32_t tail_pos = position + static_cast<int32_t>(matched->GetLength());

    int32_t next_dollar_index = replacement->IndexOf(*dollar_string, 0);
    if (next_dollar_index < 0) {
        return replacement.GetTaggedValue();
    }
    std::u16string string_builder;
    bool can_be_compress = true;
    if (next_dollar_index > 0) {
        if (replacement->IsUtf16()) {
            const uint16_t *data = replacement->GetDataUtf16();
            string_builder += base::StringHelper::Utf16ToU16String(data, next_dollar_index);
            can_be_compress = false;
        } else {
            const uint8_t *data = replacement->GetDataUtf8();
            string_builder += base::StringHelper::Utf8ToU16String(data, next_dollar_index);
        }
    }

    while (true) {
        int peek_index = next_dollar_index + 1;
        if (peek_index >= replace_length) {
            string_builder += '$';
            auto *char16t_data = const_cast<char16_t *>(string_builder.c_str());
            auto *uint16t_data = reinterpret_cast<uint16_t *>(char16t_data);
            return factory->NewFromUtf16LiteralUnCheck(uint16t_data, string_builder.length(), can_be_compress)
                .GetTaggedValue();
        }
        int continue_from_index = -1;
        uint16_t peek = replacement->At(peek_index);
        switch (peek) {
            case '$':  // $$
                string_builder += '$';
                continue_from_index = peek_index + 1;
                break;
            case '&':  // $& - match
                if (matched->IsUtf16()) {
                    const uint16_t *data = matched->GetDataUtf16();
                    string_builder += ecmascript::base::StringHelper::Utf16ToU16String(data, matched->GetLength());
                    can_be_compress = false;
                } else {
                    const uint8_t *data = matched->GetDataUtf8();
                    string_builder += ecmascript::base::StringHelper::Utf8ToU16String(data, matched->GetLength());
                }
                continue_from_index = peek_index + 1;
                break;
            case '`':  // $` - prefix
                if (position > 0) {
                    EcmaString *prefix = EcmaString::FastSubString(src_string, 0, position, ecma_vm);
                    if (prefix->IsUtf16()) {
                        const uint16_t *data = prefix->GetDataUtf16();
                        string_builder += ecmascript::base::StringHelper::Utf16ToU16String(data, prefix->GetLength());
                        can_be_compress = false;
                    } else {
                        const uint8_t *data = prefix->GetDataUtf8();
                        string_builder += ecmascript::base::StringHelper::Utf8ToU16String(data, prefix->GetLength());
                    }
                }
                continue_from_index = peek_index + 1;
                break;
            case '\'': {
                // $' - suffix
                int32_t src_length = src_string->GetLength();
                if (tail_pos < src_length) {
                    EcmaString *sufffix =
                        EcmaString::FastSubString(src_string, tail_pos, src_length - tail_pos, ecma_vm);
                    if (sufffix->IsUtf16()) {
                        const uint16_t *data = sufffix->GetDataUtf16();
                        string_builder += ecmascript::base::StringHelper::Utf16ToU16String(data, sufffix->GetLength());
                        can_be_compress = false;
                    } else {
                        const uint8_t *data = sufffix->GetDataUtf8();
                        string_builder += ecmascript::base::StringHelper::Utf8ToU16String(data, sufffix->GetLength());
                    }
                }
                continue_from_index = peek_index + 1;
                break;
            }
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9': {
                int captures_length = capture_list->GetLength();
                // Valid indices are $1 .. $9, $01 .. $09 and $10 .. $99
                int32_t scaled_index = (peek - '0');
                int32_t advance = 1;
                if (peek_index + 1 < replace_length) {
                    uint16_t next_peek = replacement->At(peek_index + 1);
                    if (next_peek >= '0' && next_peek <= '9') {
                        constexpr int32_t TEN_BASE = 10;
                        int32_t new_scaled_index = scaled_index * TEN_BASE + (next_peek - '0');
                        if (new_scaled_index <= captures_length) {
                            scaled_index = new_scaled_index;
                            advance = 2;  // 2: 2 means from index needs to add two.
                        }
                    }
                }

                if (scaled_index == 0 || scaled_index > captures_length) {
                    string_builder += '$';
                    continue_from_index = peek_index;
                    break;
                }

                JSTaggedValue captures_val(capture_list->Get(scaled_index - 1));
                if (!captures_val.IsUndefined()) {
                    EcmaString *capture_string = EcmaString::Cast(captures_val.GetTaggedObject());
                    if (capture_string->IsUtf16()) {
                        const uint16_t *data = capture_string->GetDataUtf16();
                        string_builder +=
                            ecmascript::base::StringHelper::Utf16ToU16String(data, capture_string->GetLength());
                        can_be_compress = false;
                    } else {
                        const uint8_t *data = capture_string->GetDataUtf8();
                        string_builder +=
                            ecmascript::base::StringHelper::Utf8ToU16String(data, capture_string->GetLength());
                    }
                }
                continue_from_index = peek_index + advance;
                break;
            }
            case '<': {
                if (named_captures->IsUndefined()) {
                    string_builder += '$';
                    continue_from_index = peek_index;
                    break;
                }
                JSHandle<EcmaString> greater_sym_string = factory->NewFromStdString(">");
                int pos = replacement->IndexOf(*greater_sym_string, peek_index);
                if (pos == -1) {
                    string_builder += '$';
                    continue_from_index = peek_index;
                    break;
                }
                JSHandle<EcmaString> group_name(
                    thread, EcmaString::FastSubString(replacement, peek_index + 1, pos - peek_index - 1, ecma_vm));
                JSHandle<JSTaggedValue> names(group_name);
                JSHandle<JSTaggedValue> capture = JSObject::GetProperty(thread, named_captures, names).GetValue();
                if (capture->IsUndefined()) {
                    continue_from_index = pos + 1;
                    break;
                }
                JSHandle<EcmaString> capture_name(capture);
                if (capture_name->IsUtf16()) {
                    const uint16_t *data = capture_name->GetDataUtf16();
                    string_builder += base::StringHelper::Utf16ToU16String(data, capture_name->GetLength());
                    can_be_compress = false;
                } else {
                    const uint8_t *data = capture_name->GetDataUtf8();
                    string_builder += base::StringHelper::Utf8ToU16String(data, capture_name->GetLength());
                }
                continue_from_index = pos + 1;
                break;
            }
            default:
                string_builder += '$';
                continue_from_index = peek_index;
                break;
        }
        // Go the the next $ in the replacement.
        next_dollar_index = replacement->IndexOf(*dollar_string, continue_from_index);
        if (next_dollar_index < 0) {
            if (continue_from_index < replace_length) {
                EcmaString *next_append = EcmaString::FastSubString(replacement, continue_from_index,
                                                                    replace_length - continue_from_index, ecma_vm);
                if (next_append->IsUtf16()) {
                    const uint16_t *data = next_append->GetDataUtf16();
                    string_builder += ecmascript::base::StringHelper::Utf16ToU16String(data, next_append->GetLength());
                    can_be_compress = false;
                } else {
                    const uint8_t *data = next_append->GetDataUtf8();
                    string_builder += ecmascript::base::StringHelper::Utf8ToU16String(data, next_append->GetLength());
                }
            }
            auto *char16t_data = const_cast<char16_t *>(string_builder.c_str());
            auto *uint16t_data = reinterpret_cast<uint16_t *>(char16t_data);
            return factory->NewFromUtf16LiteralUnCheck(uint16t_data, string_builder.length(), can_be_compress)
                .GetTaggedValue();
        }
        // Append substring between the previous and the next $ character.
        if (next_dollar_index > continue_from_index) {
            EcmaString *next_append = EcmaString::FastSubString(replacement, continue_from_index,
                                                                next_dollar_index - continue_from_index, ecma_vm);
            if (next_append->IsUtf16()) {
                const uint16_t *data = next_append->GetDataUtf16();
                string_builder += ecmascript::base::StringHelper::Utf16ToU16String(data, next_append->GetLength());
                can_be_compress = false;
            } else {
                const uint8_t *data = next_append->GetDataUtf8();
                string_builder += ecmascript::base::StringHelper::Utf8ToU16String(data, next_append->GetLength());
            }
        }
    }
    UNREACHABLE();
}

// 21.1.3.15
JSTaggedValue string::proto::Search(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Search);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<JSTaggedValue> regexp = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> search_tag = thread->GetEcmaVM()->GetGlobalEnv()->GetSearchSymbol();
    if (!regexp->IsUndefined() && !regexp->IsNull()) {
        if (regexp->IsECMAObject()) {
            JSHandle<JSTaggedValue> searcher = JSObject::GetMethod(thread, regexp, search_tag);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (!searcher->IsUndefined()) {
                ASSERT(searcher->IsJSFunction());

                auto info = NewRuntimeCallInfo(thread, searcher, regexp, JSTaggedValue::Undefined(), 1);
                info->SetCallArgs(this_tag);
                return JSFunction::Call(info.Get());
            }
        }
    }
    JSHandle<EcmaString> this_val = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> undifined_handle = global_const->GetHandledUndefined();
    JSHandle<JSTaggedValue> rx(thread, reg_exp::RegExpCreate(thread, regexp, undifined_handle));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    auto info = NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), rx, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(this_val.GetTaggedValue());
    return JSFunction::Invoke(info.Get(), search_tag);
}

// 21.1.3.16
JSTaggedValue string::proto::Slice(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Slice);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    JSHandle<JSTaggedValue> start_tag = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber start_val = JSTaggedValue::ToInteger(thread, start_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t start = ConvertDoubleToInt(start_val.GetNumber());
    int32_t end;
    JSHandle<JSTaggedValue> end_tag = builtins_common::GetCallArg(argv, 1);
    if (end_tag->IsUndefined()) {
        end = this_len;
    } else {
        JSTaggedNumber end_val = JSTaggedValue::ToInteger(thread, end_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        end = ConvertDoubleToInt(end_val.GetNumber());
    }
    int32_t from;
    int32_t to;
    if (start < 0) {
        from = std::max(start + this_len, 0);
    } else {
        from = std::min(start, this_len);
    }
    if (end < 0) {
        to = std::max(end + this_len, 0);
    } else {
        to = std::min(end, this_len);
    }
    int32_t len = std::max(to - from, 0);
    return JSTaggedValue(EcmaString::FastSubString(this_handle, from, len, thread->GetEcmaVM()));
}

// 21.1.3.17
JSTaggedValue string::proto::Split(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Split);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    // Let O be RequireObjectCoercible(this value).
    JSHandle<JSTaggedValue> this_tag = JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv));
    JSHandle<JSObject> this_obj(this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> seperator_tag = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> limit_tag = builtins_common::GetCallArg(argv, 1);
    // If separator is neither undefined nor null, then
    if (seperator_tag->IsECMAObject()) {
        JSHandle<JSTaggedValue> split_key = env->GetSplitSymbol();
        // Let splitter be GetMethod(separator, @@split).
        JSHandle<JSTaggedValue> splitter = JSObject::GetMethod(thread, seperator_tag, split_key);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (!splitter->IsUndefined()) {
            // Return Call(splitter, separator, «‍O, limit»).

            auto info = NewRuntimeCallInfo(thread, splitter, seperator_tag, JSTaggedValue::Undefined(), 2);
            info->SetCallArgs(this_tag, limit_tag);
            return JSFunction::Call(info.Get());  // 2: two args
        }
    }
    // Let S be ToString(O).
    JSHandle<EcmaString> this_string = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // Let A be ArrayCreate(0).
    JSHandle<JSObject> result_array(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    uint32_t array_length = 0;
    // If limit is undefined, let lim = 2^53–1; else let lim = ToLength(limit).
    uint32_t lim;
    if (limit_tag->IsUndefined()) {
        lim = UINT32_MAX - 1;
    } else {
        JSTaggedNumber lim_val = JSTaggedValue::ToInteger(thread, limit_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        lim = lim_val.ToUint32();
    }
    // ReturnIfAbrupt(lim).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // If lim = 0, return A.
    if (lim == 0) {
        return result_array.GetTaggedValue();
    }
    // Let s be the number of elements in S.
    int32_t this_length = this_string->GetLength();
    JSHandle<EcmaString> seperator_string = JSTaggedValue::ToString(thread, seperator_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (seperator_tag->IsUndefined()) {
        // Perform CreateDataProperty(A, "0", S).
        JSObject::CreateDataProperty(thread, result_array, 0, JSHandle<JSTaggedValue>(this_string));
        ASSERT_PRINT(!thread->HasPendingException(), "CreateDataProperty(A, \"0\", S) can't throw exception");
        return result_array.GetTaggedValue();
    }
    // If S.length = 0, then
    if (this_length == 0) {
        if (this_string->IndexOf(*seperator_string, 0) != -1) {
            return result_array.GetTaggedValue();
        }
        JSObject::CreateDataProperty(thread, result_array, 0, JSHandle<JSTaggedValue>(this_string));
        ASSERT_PRINT(!thread->HasPendingException(), "CreateDataProperty(A, \"0\", S) can't throw exception");
        return result_array.GetTaggedValue();
    }

    int32_t seperator_length = seperator_string->GetLength();
    if (seperator_length == 0) {
        for (int32_t i = 0; i < this_length; ++i) {
            EcmaString *element_string = EcmaString::FastSubString(this_string, i, 1, ecma_vm);
            JSHandle<JSTaggedValue> element_tag(thread, element_string);
            JSObject::CreateDataProperty(thread, result_array, array_length, element_tag);
            ASSERT_PRINT(!thread->HasPendingException(), "CreateDataProperty can't throw exception");
            ++array_length;
            if (array_length == lim) {
                return result_array.GetTaggedValue();
            }
        }
        return result_array.GetTaggedValue();
    }
    int32_t index = 0;
    int32_t pos = this_string->IndexOf(*seperator_string);
    while (pos != -1) {
        EcmaString *element_string = EcmaString::FastSubString(this_string, index, pos - index, ecma_vm);
        JSHandle<JSTaggedValue> element_tag(thread, element_string);
        JSObject::CreateDataProperty(thread, result_array, array_length, element_tag);
        ASSERT_PRINT(!thread->HasPendingException(), "CreateDataProperty can't throw exception");
        ++array_length;
        if (array_length == lim) {
            return result_array.GetTaggedValue();
        }
        index = pos + seperator_length;
        pos = this_string->IndexOf(*seperator_string, index);
    }
    EcmaString *element_string = EcmaString::FastSubString(this_string, index, this_length - index, ecma_vm);
    JSHandle<JSTaggedValue> element_tag(thread, element_string);
    JSObject::CreateDataProperty(thread, result_array, array_length, element_tag);
    ASSERT_PRINT(!thread->HasPendingException(), "CreateDataProperty can't throw exception");
    return result_array.GetTaggedValue();
}

// 21.1.3.18
JSTaggedValue string::proto::StartsWith(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, StartsWith);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> search_tag = builtins_common::GetCallArg(argv, 0);

    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    bool is_regexp = JSObject::IsRegExp(thread, search_tag);
    if (is_regexp) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "is regexp", JSTaggedValue::Exception());
    }

    JSHandle<EcmaString> search_handle = JSTaggedValue::ToString(thread, search_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    int32_t search_len = search_handle->GetLength();
    int32_t pos;
    JSHandle<JSTaggedValue> pos_tag = builtins_common::GetCallArg(argv, 1);
    if (pos_tag->IsUndefined()) {
        pos = 0;
    } else {
        JSTaggedNumber pos_val = JSTaggedValue::ToInteger(thread, pos_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        pos = pos_val.ToInt32();
    }
    pos = std::min(std::max(pos, 0), this_len);
    if (pos + search_len > this_len) {
        return builtins_common::GetTaggedBoolean(false);
    }
    std::u16string u16str_this;
    std::u16string u16str_search;
    if (this_handle->IsUtf16()) {
        u16str_this = ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_len);
    } else {
        const uint8_t *uint8_this = this_handle->GetDataUtf8();
        u16str_this = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, this_len);
    }
    if (search_handle->IsUtf16()) {
        u16str_search = ecmascript::base::StringHelper::Utf16ToU16String(search_handle->GetDataUtf16(), search_len);
    } else {
        const uint8_t *uint8_search = search_handle->GetDataUtf8();
        u16str_search = ecmascript::base::StringHelper::Utf8ToU16String(uint8_search, search_len);
    }
    int32_t idx = ecmascript::base::StringHelper::Find(u16str_this, u16str_search, pos);
    if (idx == pos) {
        return builtins_common::GetTaggedBoolean(true);
    }
    return builtins_common::GetTaggedBoolean(false);
}

// 21.1.3.19
JSTaggedValue string::proto::Substring(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Substring);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t this_len = this_handle->GetLength();
    JSHandle<JSTaggedValue> start_tag = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber start_val = JSTaggedValue::ToInteger(thread, start_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t start = ConvertDoubleToInt(start_val.GetNumber());
    int32_t end;
    JSHandle<JSTaggedValue> end_tag = builtins_common::GetCallArg(argv, 1);
    if (end_tag->IsUndefined()) {
        end = this_len;
    } else {
        JSTaggedNumber end_val = JSTaggedValue::ToInteger(thread, end_tag);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        end = ConvertDoubleToInt(end_val.GetNumber());
    }
    start = std::min(std::max(start, 0), this_len);
    end = std::min(std::max(end, 0), this_len);
    int32_t from = std::min(start, end);
    int32_t to = std::max(start, end);
    int32_t len = to - from;
    return JSTaggedValue(EcmaString::FastSubString(this_handle, from, len, thread->GetEcmaVM()));
}

// 21.1.3.20
JSTaggedValue string::proto::ToLocaleLowerCase(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, ToLocaleLowerCase);
    JSThread *thread = argv->GetThread();
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // Let O be RequireObjectCoercible(this value).
    JSHandle<JSTaggedValue> obj(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));

    // Let S be ? ToString(O).
    JSHandle<EcmaString> string = JSTaggedValue::ToString(thread, obj);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // If requestedLocales is not an empty List, then Let requestedLocale be requestedLocales[0].
    // Else, Let requestedLocale be DefaultLocale().
    JSHandle<EcmaString> requested_locale = JSLocale::DefaultLocale(thread);
    if (requested_locales->GetLength() != 0) {
        requested_locale = JSHandle<EcmaString>(thread, requested_locales->Get(0));
    }

    // Let noExtensionsLocale be the String value that is requestedLocale with all Unicode locale extension sequences
    // removed.
    JSLocale::ParsedLocale no_extensions_locale = JSLocale::HandleLocale(requested_locale);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Let availableLocales be a List with language tags that includes the languages for which the Unicode Character
    // Database contains language sensitive case mappings. Implementations may add additional language tags
    // if they support case mapping for additional locales.
    JSHandle<TaggedArray> available_locales = JSLocale::GetAvailableLocales(thread, nullptr, nullptr);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Let locale be BestAvailableLocale(availableLocales, noExtensionsLocale).
    std::string locale = JSLocale::BestAvailableLocale(thread, available_locales, no_extensions_locale.base);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // If locale is undefined, let locale be "und".
    if (locale.empty()) {
        locale = "und";
    }

    // Let uString be a List containing in order the code points of S as defined in ES2020, 6.1.4,
    // starting at the first element of S.
    // Transform those elements in uString to the to the Unicode Default Case Conversion algorithm
    icu::Locale icu_locale = icu::Locale::createFromName(locale.c_str());
    std::u16string utf16_string;
    if (string->IsUtf16()) {
        utf16_string =
            ecmascript::base::StringHelper::Utf16ToU16String(string->GetDataUtf16(), string->GetUtf16Length());
    } else {
        const uint8_t *uint8_this = string->GetDataUtf8();
        utf16_string = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, string->GetLength());
    }
    icu::UnicodeString u_string(utf16_string.data());
    icu::UnicodeString res = u_string.toLower(icu_locale);
    std::string cs_lower;
    res.toUTF8String(cs_lower);
    JSHandle<EcmaString> result = factory->NewFromStdString(cs_lower);
    return result.GetTaggedValue();
}

// 21.1.3.21
JSTaggedValue string::proto::ToLocaleUpperCase(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, ToLocaleUpperCase);
    JSThread *thread = argv->GetThread();
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // Let O be RequireObjectCoercible(this value).
    JSHandle<JSTaggedValue> obj(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));

    // Let S be ? ToString(O).
    JSHandle<EcmaString> string = JSTaggedValue::ToString(thread, obj);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // If requestedLocales is not an empty List, then Let requestedLocale be requestedLocales[0].
    // Else, Let requestedLocale be DefaultLocale().
    JSHandle<EcmaString> requested_locale = JSLocale::DefaultLocale(thread);
    if (requested_locales->GetLength() != 0) {
        requested_locale = JSHandle<EcmaString>(thread, requested_locales->Get(0));
    }

    // Let noExtensionsLocale be the String value that is requestedLocale with all Unicode locale extension sequences
    // removed.
    JSLocale::ParsedLocale no_extensions_locale = JSLocale::HandleLocale(requested_locale);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Let availableLocales be a List with language tags that includes the languages for which the Unicode Character
    // Database contains language sensitive case mappings. Implementations may add additional language tags
    // if they support case mapping for additional locales.
    JSHandle<TaggedArray> available_locales = JSLocale::GetAvailableLocales(thread, nullptr, nullptr);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // Let locale be BestAvailableLocale(availableLocales, noExtensionsLocale).
    std::string locale = JSLocale::BestAvailableLocale(thread, available_locales, no_extensions_locale.base);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // If locale is undefined, let locale be "und".
    if (locale.empty()) {
        locale = "und";
    }

    // Let uString be a List containing in order the code points of S as defined in ES2020, 6.1.4,
    // starting at the first element of S.
    // Transform those elements in uString to the to the Unicode Default Case Conversion algorithm
    icu::Locale icu_locale = icu::Locale::createFromName(locale.c_str());
    std::u16string utf16_string;
    if (string->IsUtf16()) {
        utf16_string =
            ecmascript::base::StringHelper::Utf16ToU16String(string->GetDataUtf16(), string->GetUtf16Length());
    } else {
        const uint8_t *uint8_this = string->GetDataUtf8();
        utf16_string = ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, string->GetLength());
    }
    icu::UnicodeString u_string(utf16_string.data());
    icu::UnicodeString res = u_string.toUpper(icu_locale);
    std::string cs_upper;
    res.toUTF8String(cs_upper);
    JSHandle<EcmaString> result = factory->NewFromStdString(cs_upper);
    return result.GetTaggedValue();
}

static std::u16string ToU16String(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_tag(
        JSTaggedValue::RequireObjectCoercible(thread, ecmascript::builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_handle = JSTaggedValue::ToString(thread, this_tag);
    if (UNLIKELY(thread->HasPendingException())) {
        return std::u16string();
    }

    int32_t this_len = this_handle->GetLength();
    if (this_handle->IsUtf16()) {
        return ecmascript::base::StringHelper::Utf16ToU16String(this_handle->GetDataUtf16(), this_len);
    }

    const uint8_t *uint8_this = this_handle->GetDataUtf8();
    return ecmascript::base::StringHelper::Utf8ToU16String(uint8_this, this_len);
}

// 21.1.3.22
JSTaggedValue string::proto::ToLowerCase(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, ToLowerCase);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    std::u16string u16str_this = ToU16String(argv);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue(ecmascript::base::StringHelper::ToLower(thread, u16str_this));
}

// 21.1.3.23
JSTaggedValue string::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return ThisStringValue(argv->GetThread(), builtins_common::GetThis(argv).GetTaggedValue());
}

// 21.1.3.24
JSTaggedValue string::proto::ToUpperCase(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, ToUpperCase);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    std::u16string u16str_this = ToU16String(argv);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue(ecmascript::base::StringHelper::ToUpper(thread, u16str_this));
}

// 21.1.3.25
JSTaggedValue string::proto::Trim(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Trim);
    JSThread *thread = argv->GetThread();
    std::u16string u16str_this = ToU16String(argv);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    EcmaString *str = ecmascript::base::StringHelper::Trim(thread, u16str_this, base::TrimKind::TRIM_START_END);
    return JSTaggedValue(str);
}

// ES2021 22.1.3.30
JSTaggedValue string::proto::TrimEnd(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, TrimEnd);
    JSThread *thread = argv->GetThread();
    std::u16string u16str_this = ToU16String(argv);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    EcmaString *str = ecmascript::base::StringHelper::Trim(thread, u16str_this, base::TrimKind::TRIM_END);
    return JSTaggedValue(str);
}

// ES2021 22.1.3.31
JSTaggedValue string::proto::TrimStart(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, TrimStart);
    JSThread *thread = argv->GetThread();
    std::u16string u16str_this = ToU16String(argv);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    EcmaString *str = ecmascript::base::StringHelper::Trim(thread, u16str_this, base::TrimKind::TRIM_START);
    return JSTaggedValue(str);
}

// 21.1.3.26
JSTaggedValue string::proto::ValueOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return ThisStringValue(argv->GetThread(), builtins_common::GetThis(argv).GetTaggedValue());
}

// 21.1.3.27
JSTaggedValue string::proto::Iterator(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Iterator);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be RequireObjectCoercible(this value).
    JSHandle<JSTaggedValue> current(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    // Let S be ToString(O).

    JSHandle<EcmaString> string = JSTaggedValue::ToString(thread, current);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(argv->GetThread());
    // Return CreateStringIterator(S).
    return JSStringIterator::CreateStringIterator(thread, string).GetTaggedValue();
}

//  B.2.3.1
JSTaggedValue string::proto::Substr(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringPrototype, Substr);
    JSThread *thread = argv->GetThread();

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be RequireObjectCoercible(this value).
    // 2. Let S be ToString(O).

    JSHandle<JSTaggedValue> this_tag(JSTaggedValue::RequireObjectCoercible(thread, builtins_common::GetThis(argv)));
    JSHandle<EcmaString> this_string = JSTaggedValue::ToString(thread, this_tag);

    // 3. ReturnIfAbrupt(S).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> int_start = builtins_common::GetCallArg(argv, 0);
    // 4. Let intStart be ToInteger(start).
    JSTaggedNumber num_start = JSTaggedValue::ToInteger(thread, int_start);
    // 5. ReturnIfAbrupt(intStart).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t start = num_start.ToInt32();
    JSHandle<JSTaggedValue> length_tag = builtins_common::GetCallArg(argv, 1);
    // 6. If length is undefined, let end be +; otherwise let end be ToInteger(length).
    int32_t end;
    if (length_tag->IsUndefined()) {
        end = INT_MAX;
    } else {
        JSTaggedNumber length_number = JSTaggedValue::ToInteger(thread, length_tag);
        // 7. ReturnIfAbrupt(end).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        end = length_number.ToInt32();
    }
    // 8. Let size be the number of code units in S.
    int32_t size = this_string->GetLength();
    // 9. If intStart < 0, let intStart be max(size + intStart,0).
    if (start < 0) {
        start = std::max(size + start, 0);
    }
    // 10. Let resultLength be min(max(end,0), size – intStart).
    int32_t result_length = std::min(std::max(end, 0), size - start);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 11. If resultLength  0, return the empty String "".
    if (result_length <= 0) {
        return factory->GetEmptyString().GetTaggedValue();
    }
    return JSTaggedValue(EcmaString::FastSubString(this_string, start, result_length, thread->GetEcmaVM()));
}

JSTaggedValue string::proto::GetLength(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);

    JSHandle<EcmaString> this_string = JSTaggedValue::ToString(thread, this_handle);
    return builtins_common::GetTaggedInt(this_string->GetLength());
}

// 21.1.3
JSTaggedValue ThisStringValue(JSThread *thread, JSTaggedValue value)
{
    if (value.IsString()) {
        return value;
    }
    if (value.IsECMAObject()) {
        auto jsh_class = value.GetTaggedObject()->GetClass();
        if (jsh_class->GetObjectType() == JSType::JS_PRIMITIVE_REF) {
            JSTaggedValue primitive = JSPrimitiveRef::Cast(value.GetTaggedObject())->GetValue();
            if (primitive.IsString()) {
                return primitive;
            }
        }
    }
    THROW_TYPE_ERROR_AND_RETURN(thread, "can not convert to String", JSTaggedValue::Exception());
}

int32_t ConvertDoubleToInt(double d)
{
    if (std::isnan(d) || d == -ecmascript::base::POSITIVE_INFINITY) {
        return 0;
    }
    if (d >= static_cast<double>(INT_MAX)) {
        return INT_MAX;
    }
    if (d <= static_cast<double>(INT_MIN)) {
        return INT_MIN;
    }
    return ecmascript::base::NumberHelper::DoubleToInt(d, ecmascript::base::INT32_BITS);
}
}  // namespace panda::ecmascript::builtins
