/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"

namespace panda::ecmascript::builtins {
// ecma 19.3.1.1 Boolean(value)
JSTaggedValue boolean::BooleanConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Boolean, BooleanConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let b be ToBoolean(value).
    bool bool_value = builtins_common::GetCallArg(argv, 0)->ToBoolean();
    // 2. If NewTarget is undefined, return b.
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        return builtins_common::GetTaggedBoolean(bool_value);
    }
    // 3. Let O be OrdinaryCreateFromConstructor(NewTarget, "%BooleanPrototype%", [[BooleanData]] ).
    // 5. Set the value of O's [[BooleanData]] internal slot to b.
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSFunction> ctor = JSHandle<JSFunction>(builtins_common::GetConstructor(argv));
    JSHandle<JSObject> result = factory->NewJSObjectByConstructor(ctor, new_target);
    JSTaggedValue obj_value = bool_value ? JSTaggedValue::True() : JSTaggedValue::False();
    JSPrimitiveRef::Cast(*result)->SetValue(thread, obj_value);
    // 4. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 6. Return O.
    return result.GetTaggedValue();
}

// ecma 19.3.3 abstract operation thisBooleanValue(value)
static JSTaggedValue ThisBooleanValue(JSThread *thread, JSTaggedValue value)
{
    BUILTINS_API_TRACE(thread, Boolean, ThisBooleanValue);
    // 1. If Type(value) is Boolean, return value
    if (value.IsBoolean()) {
        return value == JSTaggedValue::True() ? builtins_common::GetTaggedBoolean(true)
                                              : builtins_common::GetTaggedBoolean(false);
    }
    // 2. If Type(value) is Object and value has a [[BooleanData]] internal slot, then
    if (value.IsJSPrimitiveRef()) {
        JSTaggedValue primitive = JSPrimitiveRef::Cast(value.GetTaggedObject())->GetValue();
        // a. Assert: value's [[BooleanData]] internal slot is a Boolean value.
        if (primitive.IsBoolean()) {
            // b. Return the value of value's [[BooleanData]] internal slot.
            return primitive == JSTaggedValue::True() ? builtins_common::GetTaggedBoolean(true)
                                                      : builtins_common::GetTaggedBoolean(false);
        }
    }
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 3. Throw a TypeError exception.
    THROW_TYPE_ERROR_AND_RETURN(thread, "the type can not convert to BooleanValue", JSTaggedValue::Exception());
}

// ecma 19.3.3.2 Boolean.prototype.toString ()
JSTaggedValue boolean::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let b be thisBooleanValue(this value).
    JSTaggedValue this_value_to_boolean = ThisBooleanValue(thread, builtins_common::GetThis(argv).GetTaggedValue());
    // 2. ReturnIfAbrupt(b)
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. If b is true, return "true"; else return "false".
    return this_value_to_boolean.IsTrue() ? builtins_common::GetTaggedString(thread, "true")
                                          : builtins_common::GetTaggedString(thread, "false");
}

// ecma 19.3.3.3 Boolean.prototype.valueOf ()
JSTaggedValue boolean::proto::ValueOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    // 1. Return thisBooleanValue(this value).
    return ThisBooleanValue(argv->GetThread(), builtins_common::GetThis(argv).GetTaggedValue());
}
}  // namespace panda::ecmascript::builtins
