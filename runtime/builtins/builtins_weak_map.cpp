/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_map_iterator.h"
#include "plugins/ecmascript/runtime/js_weak_container.h"
#include "plugins/ecmascript/runtime/linked_hash_table.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {
JSTaggedValue weak_map::WeakMapConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakMap, WeakMapConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1.If NewTarget is undefined, throw a TypeError exception
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        // throw type error
        THROW_TYPE_ERROR_AND_RETURN(thread, "new target can't be undefined", JSTaggedValue::Exception());
    }
    // 2.Let WeakMap be OrdinaryCreateFromConstructor(NewTarget, "%WeakMapPrototype%", «‍[[WeakMapData]]» ).
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target);
    // 3.returnIfAbrupt()
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSWeakMap> weak_map = JSHandle<JSWeakMap>::Cast(obj);

    // 4.Set weakmap’s [[WeakMapData]] internal slot to a new empty List.
    JSHandle<LinkedHashMap> linked_map = LinkedHashMap::Create(thread, true);
    weak_map->SetLinkedMap(thread, linked_map);
    // add data into set from iterable
    // 5.If iterable is not present, let iterable be undefined.
    // 6.If iterable is either undefined or null, let iter be undefined.
    JSHandle<JSTaggedValue> iterable = builtins_common::GetCallArg(argv, 0);
    // 8.If iter is undefined, return set
    if (iterable->IsUndefined() || iterable->IsNull()) {
        return weak_map.GetTaggedValue();
    }
    if (!iterable->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "iterable is not object", JSTaggedValue::Exception());
    }
    // Let adder be Get(weakMap, "set").
    JSHandle<JSTaggedValue> adder_key(factory->NewFromCanBeCompressString("set"));
    JSHandle<JSTaggedValue> adder =
        JSObject::GetProperty(thread, JSHandle<JSTaggedValue>(weak_map), adder_key).GetValue();
    // ReturnIfAbrupt(adder).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, adder.GetTaggedValue());
    // If IsCallable(adder) is false, throw a TypeError exception
    if (!adder->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "adder is not callable", adder.GetTaggedValue());
    }
    // Let iter be GetIterator(iterable).
    JSHandle<JSTaggedValue> iter(JSIterator::GetIterator(thread, iterable));
    // ReturnIfAbrupt(iter).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, iter.GetTaggedValue());
    JSHandle<JSTaggedValue> key_index(thread, JSTaggedValue(0));
    JSHandle<JSTaggedValue> value_index(thread, JSTaggedValue(1));
    JSHandle<JSTaggedValue> next = JSIterator::IteratorStep(thread, iter);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, next.GetTaggedValue());
    JSMutableHandle<JSTaggedValue> status(thread, JSTaggedValue::Undefined());
    while (!next->IsFalse()) {
        // ReturnIfAbrupt(next).
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, next.GetTaggedValue());
        // Let nextValue be IteratorValue(next).
        JSHandle<JSTaggedValue> next_value(JSIterator::IteratorValue(thread, next));
        // ReturnIfAbrupt(nextValue).
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, next.GetTaggedValue());
        // If Type(nextItem) is not Object
        if (!next_value->IsECMAObject()) {
            JSHandle<JSObject> type_error = factory->GetJSError(ErrorType::TYPE_ERROR, "nextItem is not Object");
            JSHandle<JSTaggedValue> record(
                factory->NewCompletionRecord(CompletionRecord::THROW, JSHandle<JSTaggedValue>(type_error)));
            JSTaggedValue ret = JSIterator::IteratorClose(thread, iter, record).GetTaggedValue();
            if (LIKELY(!thread->HasPendingException())) {
                THROW_NEW_ERROR_AND_RETURN_VALUE(thread, type_error.GetTaggedValue(), ret);
            };
            return ret;
        }
        // Let k be Get(nextItem, "0").
        JSHandle<JSTaggedValue> key = JSObject::GetProperty(thread, next_value, key_index).GetValue();
        // If k is an abrupt completion, return IteratorClose(iter, k).
        if (UNLIKELY(thread->HasPendingException())) {
            return JSIterator::IteratorCloseAndReturn(thread, iter, key);
        }

        // Let v be Get(nextItem, "1").
        JSHandle<JSTaggedValue> value = JSObject::GetProperty(thread, next_value, value_index).GetValue();
        // If v is an abrupt completion, return IteratorClose(iter, v).
        if (UNLIKELY(thread->HasPendingException())) {
            return JSIterator::IteratorCloseAndReturn(thread, iter, value);
        }

        // Let status be Call(adder, weakMap, «nextValue.[[value]]»).

        auto info = NewRuntimeCallInfo(thread, adder, JSHandle<JSTaggedValue>(weak_map), JSTaggedValue::Undefined(), 2);
        info->SetCallArgs(key, value);
        JSTaggedValue ret = JSFunction::Call(info.Get());  // 2: key and value pair

        status.Update(ret);
        // If status is an abrupt completion, return IteratorClose(iter, status).
        if (UNLIKELY(thread->HasPendingException())) {
            return JSIterator::IteratorCloseAndReturn(thread, iter, status);
        }
        // Let next be IteratorStep(iter).
        next = JSIterator::IteratorStep(thread, iter);
    }
    return weak_map.GetTaggedValue();
}

JSTaggedValue weak_map::proto::Delete(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakMapPrototype, Delete);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self = builtins_common::GetThis(argv);
    // 2.If Type(S) is not Object, throw a TypeError exception.
    // 3.If S does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
    if (!self->IsJSWeakMap()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakMap.", JSTaggedValue::Exception());
    }

    JSHandle<JSWeakMap> weak_map(self);
    JSHandle<JSTaggedValue> key = builtins_common::GetCallArg(argv, 0);
    // 5.if Type(key) is not Object, return false.
    if (!key->IsHeapObject()) {
        return builtins_common::GetTaggedBoolean(false);
    }
    return builtins_common::GetTaggedBoolean(JSWeakMap::Delete(thread, weak_map, key));
}

JSTaggedValue weak_map::proto::Has(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakMapPrototype, Has);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self(builtins_common::GetThis(argv));
    // 2.If Type(S) is not Object, throw a TypeError exception.
    // 3.If S does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
    if (!self->IsJSWeakMap()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakMap.", JSTaggedValue::Exception());
    }
    JSHandle<JSWeakMap> js_weak_map(thread, JSWeakMap::Cast(*JSTaggedValue::ToObject(thread, self)));
    JSHandle<JSTaggedValue> key = builtins_common::GetCallArg(argv, 0);
    // 5.if Type(key) is not Object, return false.
    if (!key->IsHeapObject()) {
        return builtins_common::GetTaggedBoolean(false);
    }
    int hash = LinkedHash::Hash(key.GetTaggedValue());
    return builtins_common::GetTaggedBoolean(js_weak_map->Has(key.GetTaggedValue(), hash));
}

JSTaggedValue weak_map::proto::Get(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakMapPrototype, Get);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self(builtins_common::GetThis(argv));
    // 2.If Type(S) is not Object, throw a TypeError exception.
    // 3.If S does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
    if (!self->IsJSWeakMap()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakMap.", JSTaggedValue::Exception());
    }
    JSHandle<JSWeakMap> js_weak_map(thread, JSWeakMap::Cast(*JSTaggedValue::ToObject(thread, self)));
    JSHandle<JSTaggedValue> key = builtins_common::GetCallArg(argv, 0);
    if (!key->IsHeapObject()) {
        return JSTaggedValue::Undefined();
    }
    int hash = LinkedHash::Hash(key.GetTaggedValue());
    return js_weak_map->Get(key.GetTaggedValue(), hash);
}

JSTaggedValue weak_map::proto::Set(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), WeakMapPrototype, Set);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self = builtins_common::GetThis(argv);

    // 2.If Type(S) is not Object, throw a TypeError exception.
    // 3.If S does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
    if (!self->IsJSWeakMap()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakMap.", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> key = builtins_common::GetCallArg(argv, 0);
    if (!key->IsHeapObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not an object.", JSTaggedValue::Exception());
    }
    if (key->IsSymbol() || key->IsString()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "key is Symblol or String", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 1);

    JSHandle<JSWeakMap> map(self);
    JSWeakMap::Set(thread, map, key, value);
    return map.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
