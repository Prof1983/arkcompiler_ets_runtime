/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_locale.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_plural_rules.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {
// 15.2.1 Intl.PluralRules ( [ locales [ , options ] ] )
JSTaggedValue plural_rules::PluralRulesConstructor(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. If NewTarget is undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "new_target is undefined", JSTaggedValue::Exception());
    }

    // 2. Let pluralRules be ? OrdinaryCreateFromConstructor(NewTarget, "%PluralRulesPrototype%",
    // « [[InitializedPluralRules]], [[Locale]], [[Type]], [[MinimumIntegerDigits]], [[MinimumFractionDigits]],
    // [[MaximumFractionDigits]], [[MinimumSignificantDigits]], [[MaximumSignificantDigits]], [[RoundingType]] »).
    JSHandle<JSPluralRules> plural_rules =
        JSHandle<JSPluralRules>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Return ? InitializePluralRules(pluralRules, locales, options).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSPluralRules::InitializePluralRules(thread, plural_rules, locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return plural_rules.GetTaggedValue();
}

// 15.3.2 Intl.PluralRules.supportedLocalesOf ( locales [, options ] )
JSTaggedValue plural_rules::SupportedLocalesOf(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let availableLocales be %PluralRules%.[[AvailableLocales]].
    JSHandle<TaggedArray> available_locales = JSPluralRules::GetAvailableLocales(thread);

    // 2. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Return ? SupportedLocales(availableLocales, requestedLocales, options).
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSArray> result = JSLocale::SupportedLocales(thread, available_locales, requested_locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 15.4.3 Intl.PluralRules.prototype.select( value )
JSTaggedValue plural_rules::proto::Select(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let pr be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(pr, [[InitializedPluralRules]]).
    if (!this_value->IsJSPluralRules()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not pr object", JSTaggedValue::Exception());
    }

    // 3. Let n be ? ToNumber(value).
    double x = 0.0;
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber temp = JSTaggedValue::ToNumber(thread, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    x = temp.GetNumber();

    // 4. Return ? ResolvePlural(pr, n).
    JSHandle<JSPluralRules> plural_rules = JSHandle<JSPluralRules>::Cast(this_value);
    JSHandle<EcmaString> result = JSPluralRules::ResolvePlural(thread, plural_rules, x);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 15.4.4 Intl.PluralRules.prototype.resolvedOptions ()
JSTaggedValue plural_rules::proto::ResolvedOptions(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let thisValue be the this value;
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(pr, [[InitializedPluralRules]]).
    if (!this_value->IsJSPluralRules()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not pr object", JSTaggedValue::Exception());
    }

    // 3. Let options be ! ObjectCreate(%ObjectPrototype%).
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<JSTaggedValue> ctor = env->GetObjectFunction();
    JSHandle<JSObject> options(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), ctor));

    // 4. Perform resolvedOptions
    JSHandle<JSPluralRules> plural_rules = JSHandle<JSPluralRules>::Cast(this_value);
    JSPluralRules::ResolvedOptions(thread, plural_rules, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Return options.
    return options.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
