/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/compiler/optimizer/optimizations/ecma_inlining.h"

namespace panda::compiler {

AnyBaseType InlineIntrinsics::FetchInputs(CallInst *call_inst)
{
    types_.clear();
    saved_inputs_.clear();
    AnyBaseType type = AnyBaseType::ECMASCRIPT_INT_TYPE;
    for (auto &input : call_inst->GetInputs().SubSpan(GetGraph()->GetRuntime()->GetDynamicNumFixedArgs())) {
        auto input_inst = input.GetInst();
        if (input_inst->IsSaveState()) {
            continue;
        }
        auto input_type = GetAssumedAnyType(input_inst);

        if (input_type != AnyBaseType::ECMASCRIPT_INT_TYPE) {
            type = input_type;
        }
        types_.emplace_back(input_type);
        saved_inputs_.emplace_back(input_inst);
    }
    // last input is SaveState
    ASSERT(GetGraph()->GetRuntime()->GetDynamicNumFixedArgs() + types_.size() + 1 == call_inst->GetInputsCount());
    if (type == AnyBaseType::UNDEFINED_TYPE) {
        type = AnyBaseType::ECMASCRIPT_DOUBLE_TYPE;
    }
    ASSERT((type == AnyBaseType::ECMASCRIPT_INT_TYPE) || (type == AnyBaseType::ECMASCRIPT_DOUBLE_TYPE));
    return type;
}

void InlineIntrinsics::ResolveInputs(AnyBaseType type, CallInst *call_inst)
{
    if (type == AnyBaseType::ECMASCRIPT_DOUBLE_TYPE) {
        for (size_t i = 0; i < types_.size(); i++) {
            if (types_[i] == AnyBaseType::ECMASCRIPT_INT_TYPE) {
                auto any_check = GetGraph()->CreateInstAnyTypeCheck(DataType::ANY, call_inst->GetPc(), saved_inputs_[i], call_inst->GetSaveState(), AnyBaseType::ECMASCRIPT_INT_TYPE);
                call_inst->InsertBefore(any_check);

                auto cati = GetGraph()->CreateInstCastAnyTypeValue(call_inst->GetPc(), any_check, AnyBaseType::ECMASCRIPT_INT_TYPE);
                call_inst->InsertBefore(cati);

                auto cast = GetGraph()->CreateInstCast(AnyBaseTypeToDataType(AnyBaseType::ECMASCRIPT_DOUBLE_TYPE), call_inst->GetPc(), cati, DataType::NO_TYPE);
                call_inst->InsertBefore(cast);
                saved_inputs_[i] = cast;
            } else if ((types_[i] == AnyBaseType::ECMASCRIPT_DOUBLE_TYPE) || (types_[i] == AnyBaseType::UNDEFINED_TYPE)) {
                auto any_check = GetGraph()->CreateInstAnyTypeCheck(DataType::ANY, call_inst->GetPc(), saved_inputs_[i], call_inst->GetSaveState(), AnyBaseType::ECMASCRIPT_DOUBLE_TYPE);
                if (saved_inputs_[i]->GetOpcode() == Opcode::AnyTypeCheck) {
                    any_check->SetAllowedInputType(saved_inputs_[i]->CastToAnyTypeCheck()->GetAllowedInputType());
                    any_check->SetIsIntegerWasSeen(saved_inputs_[i]->CastToAnyTypeCheck()->IsIntegerWasSeen());
                }
                call_inst->InsertBefore(any_check);

                auto cati = GetGraph()->CreateInstCastAnyTypeValue(call_inst->GetPc(), any_check, AnyBaseType::ECMASCRIPT_DOUBLE_TYPE);
                call_inst->InsertBefore(cati);
                saved_inputs_[i] = cati;
            } else {
                UNREACHABLE();
            }
        }
    } else if (type == AnyBaseType::ECMASCRIPT_INT_TYPE) {
        for (size_t i = 0; i < types_.size(); i++) {
            if ((types_[i] == AnyBaseType::ECMASCRIPT_INT_TYPE) || (types_[i] == AnyBaseType::UNDEFINED_TYPE)) {
                auto any_check = GetGraph()->CreateInstAnyTypeCheck(DataType::ANY, call_inst->GetPc(), saved_inputs_[i], call_inst->GetSaveState(), AnyBaseType::ECMASCRIPT_INT_TYPE);
                call_inst->InsertBefore(any_check);

                auto cati = GetGraph()->CreateInstCastAnyTypeValue(call_inst->GetPc(), any_check, AnyBaseType::ECMASCRIPT_INT_TYPE);
                call_inst->InsertBefore(cati);
                saved_inputs_[i] = cati;
            } else {
                UNREACHABLE();
            }
        }
    }
}

% Builtins.spaces.each do |space|
%   space.inlinable_methods.each do |method|
bool InlineIntrinsics::TryInline<%= space.name %><%= method.gen_cpp_id %>Gen(CallInst *call_inst)
{
%       raise "Only trivial overloads are currently supported" if !method.trivial_inline_overloads?
    [[maybe_unused]] auto resolved_type = FetchInputs(call_inst);

%       if method.inline_info[0].args_type.size != 0
    if (types_.size() != <%= method.inline_info[0].args_type.size %>) {
        return false;
    }
    
%           if method.get_integer_overload
    if (resolved_type == AnyBaseType::ECMASCRIPT_INT_TYPE) {
        ecmascript::EcmaInlining::BuildGuard(call_inst, call_inst->GetCallMethod());
        ResolveInputs(AnyBaseType::ECMASCRIPT_INT_TYPE, call_inst);
        <%= method.get_integer_overload.do_inline %>
        return true;
    }
%           end
%           if method.get_double_overload
    if ((resolved_type == AnyBaseType::ECMASCRIPT_DOUBLE_TYPE) || (resolved_type == AnyBaseType::ECMASCRIPT_INT_TYPE)) {
        ecmascript::EcmaInlining::BuildGuard(call_inst, call_inst->GetCallMethod());
        ResolveInputs(AnyBaseType::ECMASCRIPT_DOUBLE_TYPE, call_inst);
        <%= method.get_double_overload.do_inline %>
        return true;
    }
%          end
    return false;
%       else
    if (!types_.empty()) {
        return false;
    }
    ecmascript::EcmaInlining::BuildGuard(call_inst, call_inst->GetCallMethod());
    <%= method.get_zero_overload.do_inline -%>
    return true;
%       end
}

%   end
% end

bool InlineIntrinsics::TryInlineNativeMethodGenECMASCRIPT(CallInst *call_inst)
{
    ASSERT(call_inst->GetCallMethod() != nullptr);
    switch(GetGraph()->GetRuntime()->ResolveInlinableNativeMethod(call_inst->GetCallMethod())) {
% Builtins.spaces.each do |space|
%   space.inlinable_methods.each do |method|
    case panda::ecmascript::EcmaRuntimeCallerId::BUILTINS_ID_<%= space.name %>_<%= method.gen_cpp_id %>:
        return TryInline<%= space.name %><%= method.gen_cpp_id %>Gen(call_inst);
%   end
% end
    default:
        return false;
    }
}

}  // namespace panda::compiler
