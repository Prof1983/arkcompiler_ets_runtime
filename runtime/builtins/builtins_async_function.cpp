/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_eval.h"

namespace panda::ecmascript {
// ecma2017 25.5.1.1 AsyncFunction (p1, p2, ... , pn, body)
JSTaggedValue builtins::async_function::AsyncFunctionConstructor(EcmaRuntimeCallInfo *argv)
{
    return EvalUtils::CreateDynamicFunction(argv, EvalUtils::DynamicFunctionKind::ASYNC);
}
}  // namespace panda::ecmascript
