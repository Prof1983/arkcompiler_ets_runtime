/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_iterator.h"

namespace panda::ecmascript::builtins {

JSTaggedValue iterator::proto::Next([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    return JSTaggedValue::Undefined();
}

JSTaggedValue iterator::proto::Throw([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    return JSTaggedValue::Undefined();
}

JSTaggedValue iterator::proto::Return(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), IteratorPrototype, Return);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> iter_result = JSIterator::CreateIterResultObject(thread, value, true);
    return iter_result.GetTaggedValue();
}

JSTaggedValue iterator::proto::Iterator(EcmaRuntimeCallInfo *argv)
{
    return ecmascript::builtins_common::GetThis(argv).GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
