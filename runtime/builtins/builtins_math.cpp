/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include <cmath>
#include <random>
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "utils/bit_utils.h"

namespace panda::ecmascript::builtins {
using NumberHelper = ecmascript::base::NumberHelper;

// 20.2.2.1
JSTaggedValue math::Abs(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Abs);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    if (number_value.IsDouble()) {
        // if number_value is double,NaN,Undefine, deal in this case
        // if number_value is a String ,which can change to double. e.g."100",deal in this case
        return builtins_common::GetTaggedDouble(std::fabs(number_value.GetDouble()));
    }
    // if number_value is int,boolean,null, deal in this case
    return builtins_common::GetTaggedInt(std::abs(number_value.GetInt()));
}

// 20.2.2.2
JSTaggedValue math::Acos(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Acos);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // value == -NaN , <-1  or > 1,result is  NaN
    if (!std::isnan(std::abs(value)) && value <= 1 && value >= -1) {
        result = std::acos(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.3
JSTaggedValue math::Acosh(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Acosh);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    if (value >= 1) {
        result = std::acosh(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.4
JSTaggedValue math::Asin(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Asin);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    if (value >= -1 && value <= 1) {
        result = std::asin(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.5
JSTaggedValue math::Asinh(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Asinh);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // value == -NaN, NaN, result is  NaN
    if (!std::isnan(std::abs(value))) {
        result = std::asinh(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.6
JSTaggedValue math::Atan(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Atan);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // value == -NaN, NaN, result is  NaN
    if (!std::isnan(std::abs(value))) {
        result = std::atan(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.7
JSTaggedValue math::Atanh(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Atanh);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    if (value >= -1 && value <= 1) {
        result = std::atanh(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.8
JSTaggedValue math::Atan2(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Atan2);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg_y = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> msg_x = builtins_common::GetCallArg(argv, 1);
    double result = ecmascript::base::NAN_VALUE;
    JSTaggedNumber number_value_y = JSTaggedValue::ToNumber(thread, msg_y);
    JSTaggedNumber number_value_x = JSTaggedValue::ToNumber(thread, msg_x);
    double value_y = number_value_y.GetNumber();
    double value_x = number_value_x.GetNumber();
    // y = +0 and x > +0, return +0
    // y = -0 and x > +0, return -0
    if (value_y == 0 && value_x > 0) {
        result = value_y;
    } else if (std::isfinite(value_y) && value_x == std::numeric_limits<double>::infinity()) {
        // y < 0 and y is finite and x is POSITIVE_INFINITY,return -0
        // y >= 0 and y is finite and x is POSITIVE_INFINITY,return +0
        result = value_y >= 0 ? 0 : -0.0;
    } else if (!std::isnan(std::abs(value_y)) && !std::isnan(std::abs(value_x))) {
        // If either x or y is NaN, the result is NaN
        result = std::atan2(value_y, value_x);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.9
JSTaggedValue math::Cbrt(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Cbrt);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // if value == -NaN, NaN, result is NaN
    if (!std::isnan(std::abs(value))) {
        result = std::cbrt(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.10
JSTaggedValue math::Ceil(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Ceil);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN or -NaN, +infinite, -infinite,return value
    if (!std::isfinite(value)) {
        // if value is -NaN , return NaN, else return value
        if (!std::isnan(std::abs(value))) {
            result = value;
        }
    } else {
        result = std::ceil(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.11
JSTaggedValue math::Clz32(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Clz32);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    constexpr int DEFAULT_VALUE = 32;
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    auto tmp_value = std::abs(value);
    auto result = number_value.ToUint32();
    if (!std::isfinite(tmp_value) || tmp_value == 0 || result == 0) {
        // If value is NaN or -NaN, +infinite, -infinite, 0,return 32
        return builtins_common::GetTaggedInt(DEFAULT_VALUE);
    }
    return builtins_common::GetTaggedInt(__builtin_clz(result));
}

// 20.2.2.12
JSTaggedValue math::Cos(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Cos);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    //  If value is NaN or -NaN, +infinite, -infinite, result is NaN
    if (std::isfinite(std::abs(value))) {
        result = std::cos(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.13
JSTaggedValue math::Cosh(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Cosh);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // if value is NaN or -NaN, result is NaN
    if (!std::isnan(std::abs(value))) {
        result = std::cosh(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.14
JSTaggedValue math::Exp(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Exp);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // if value is NaN or -NaN, result is NaN
    if (!std::isnan(std::abs(value))) {
        result = std::exp(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.15
JSTaggedValue math::Expm1(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Expm1);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // if value is NaN or -NaN, result is NaN
    if (!std::isnan(std::abs(value))) {
        result = std::expm1(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.16
JSTaggedValue math::Floor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Floor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN or -NaN, +infinite, -infinite, +0, -0, return value
    if (!std::isfinite(value) || value == 0) {
        // If value is -NaN, return NaN, else return value
        if (!std::isnan(std::abs(value))) {
            result = value;
        }
    } else if (value > 0 && value < 1) {
        // If x is greater than 0 but less than 1, the result is +0
        result = 0;
    } else {
        result = std::floor(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.17
JSTaggedValue math::Fround(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Fround);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result;
    if (std::isnan(std::abs(value))) {
        // If result is NaN or -NaN, the result is NaN
        result = ecmascript::base::NAN_VALUE;
    } else {
        result = static_cast<float>(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.18
JSTaggedValue math::Hypot(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Hypot);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    double result = 0;
    double value = 0;
    int arg_len = argv->GetArgsNumber();
    auto number_value = JSTaggedNumber(0);
    for (int i = 0; i < arg_len; i++) {
        JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, i);
        number_value = JSTaggedValue::ToNumber(thread, msg);
        value = number_value.GetNumber();
        result = std::hypot(result, value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.19
JSTaggedValue math::Imul(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Imul);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> msg2 = builtins_common::GetCallArg(argv, 1);
    JSTaggedNumber number_value1 = JSTaggedValue::ToNumber(thread, msg1);
    JSTaggedNumber number_value2 = JSTaggedValue::ToNumber(thread, msg2);
    auto value1 = number_value1.GetNumber();
    auto value2 = number_value2.GetNumber();
    if (!std::isfinite(value1) || !std::isfinite(value2)) {
        // If value is NaN or -NaN, +infinite, -infinite
        return builtins_common::GetTaggedInt(0);
    }
    value1 = number_value1.ToInt32();
    value2 = number_value2.ToInt32();
    // purposely ignoring overflow
    auto result = static_cast<int32_t>(static_cast<int64_t>(value1) * static_cast<int64_t>(value2));
    return builtins_common::GetTaggedInt(result);
}

// 20.2.2.20
JSTaggedValue math::Log(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Log);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN , -NaN , or < 0,result is NaN
    if (!std::isnan(std::abs(value)) && value >= 0) {
        result = std::log(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.21
JSTaggedValue math::Log1p(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Log1p);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN , -NaN , or < -1,result is NaN
    if (!std::isnan(std::abs(value)) && value >= -1) {
        result = std::log1p(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.22
JSTaggedValue math::Log10(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Log10);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN , -NaN , or < 0,result is NaN
    if (!std::isnan(std::abs(value)) && value >= 0) {
        result = std::log10(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.23
JSTaggedValue math::Log2(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Log2);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN , -NaN , or < 0,result is NaN
    if (!std::isnan(std::abs(value)) && value >= 0) {
        result = std::log2(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

inline bool IsNegZero(double value)
{
    return (value == 0.0 &&
            (bit_cast<uint64_t>(value) & ecmascript::base::DOUBLE_SIGN_MASK) == ecmascript::base::DOUBLE_SIGN_MASK);
}

// 20.2.2.24
JSTaggedValue math::Max(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Max);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    int arg_len = argv->GetArgsNumber();
    auto number_value = JSTaggedNumber(-ecmascript::base::POSITIVE_INFINITY);
    // If no arguments are given, the result is -inf
    auto result = JSTaggedNumber(-ecmascript::base::POSITIVE_INFINITY);
    auto tmp_max = -ecmascript::base::POSITIVE_INFINITY;
    auto value = -ecmascript::base::POSITIVE_INFINITY;
    for (int i = 0; i < arg_len; i++) {
        JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, i);
        number_value = JSTaggedValue::ToNumber(thread, msg);
        value = number_value.GetNumber();
        if (std::isnan(std::abs(value))) {
            // If any value is NaN, or -NaN, the max result is NaN
            result = number_value;
            break;
        }
        if (value > tmp_max) {
            result = number_value;
            tmp_max = value;
        } else if (value == 0 && tmp_max == 0 && IsNegZero(tmp_max) && !IsNegZero(value)) {
            // if tmp_max is -0, value is 0, max is 0
            result = number_value;
            tmp_max = value;
        }
    }
    return result;
}

// 20.2.2.25
JSTaggedValue math::Min(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Min);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    int arg_len = argv->GetArgsNumber();
    auto number_value = JSTaggedNumber(ecmascript::base::POSITIVE_INFINITY);
    // If no arguments are given, the result is inf
    auto result = JSTaggedNumber(ecmascript::base::POSITIVE_INFINITY);
    auto tmp_min = ecmascript::base::POSITIVE_INFINITY;
    auto value = ecmascript::base::POSITIVE_INFINITY;
    for (int i = 0; i < arg_len; i++) {
        JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, i);
        number_value = JSTaggedValue::ToNumber(thread, msg);
        value = number_value.GetNumber();
        if (std::isnan(std::abs(value))) {
            // If any value is NaN or -NaN, the min result is NaN
            result = number_value;
            break;
        }
        if (value < tmp_min) {
            result = number_value;
            tmp_min = value;
        } else if (value == 0 && tmp_min == 0 && !IsNegZero(tmp_min) && IsNegZero(value)) {
            // if tmp_min is 0, value is -0, min is -0
            result = number_value;
            tmp_min = value;
        }
    }
    return result;
}

// 20.2.2.26
JSTaggedValue math::Pow(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Pow);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg_x = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> msg_y = builtins_common::GetCallArg(argv, 1);
    JSTaggedNumber number_value_x = JSTaggedValue::ToNumber(thread, msg_x);
    JSTaggedNumber number_value_y = JSTaggedValue::ToNumber(thread, msg_y);
    double value_x = number_value_x.GetNumber();
    double value_y = number_value_y.GetNumber();
    return base::NumberHelper::Pow(value_x, value_y);
}

// 20.2.2.27
JSTaggedValue math::Random([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Random);
    // result range [0,1)
    std::uniform_real_distribution<double> dis(0.0, 1.0);
    double result = dis(argv->GetThread()->GetEcmaVM()->GetRandomEngine());
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.28
JSTaggedValue math::Round(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Round);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    auto result = ecmascript::base::NAN_VALUE;
    const double diff = 0.5;
    double abs_value = std::abs(value);
    if (!std::isfinite(abs_value) || abs_value == 0) {
        // If value is NaN, +infinite, or -infinite, VRegisterTag is DOUBLE
        if (!std::isnan(abs_value)) {
            // If value is NaN or -NaN, the result is default NaN, else is value
            result = value;
        }
        return builtins_common::GetTaggedDouble(result);
    }
    // If x is less than 0 but greater than or equal to -0.5, the result is -0
    if (value < 0 && value >= -diff) {
        return builtins_common::GetTaggedDouble(-0.0);
    }
    // If x is greater than 0 but less than 0.5, the result is +0
    if (value > 0 && value < diff) {
        return builtins_common::GetTaggedInt(0);
    }
    // For huge integers
    result = std::ceil(value);
    if (result - value > diff) {
        result -= 1;
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.29
JSTaggedValue math::Sign(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Sign);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    if (std::isnan(std::abs(value))) {
        return builtins_common::GetTaggedDouble(std::abs(value));
    }
    if (value == 0.0) {
        return builtins_common::GetTaggedDouble(value);
    }
    if (value < 0) {
        return builtins_common::GetTaggedInt(-1);
    }
    return builtins_common::GetTaggedInt(1);
}

// 20.2.2.30
JSTaggedValue math::Sin(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Sin);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN or -NaN, the result is NaN
    if (std::isfinite(std::abs(value))) {
        result = std::sin(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.31
JSTaggedValue math::Sinh(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Sinh);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN or -NaN, the result is NaN
    if (!std::isnan(std::abs(value))) {
        result = std::sinh(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.32
JSTaggedValue math::Sqrt(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Sqrt);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN or -NaN, or value < 0, the result is NaN
    if (!std::isnan(std::abs(value)) && value >= 0) {
        result = std::sqrt(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.33
JSTaggedValue math::Tan(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Tan);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    // If value is NaN or -NaN, +infinite, -infinite, result is NaN
    if (std::isfinite(value)) {
        result = std::tan(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.34
JSTaggedValue math::Tanh(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Tanh);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    if (!std::isnan(std::abs(value))) {
        result = std::tanh(value);
    }
    return builtins_common::GetTaggedDouble(result);
}

// 20.2.2.35
JSTaggedValue math::Trunc(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Math, Trunc);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber number_value = JSTaggedValue::ToNumber(thread, msg);
    double value = number_value.GetNumber();
    double result = ecmascript::base::NAN_VALUE;
    if (!std::isfinite(value)) {
        // if value is +infinite, -infinite, NaN, -NaN, VRegisterTag is double
        if (!std::isnan(std::abs(value))) {
            // if value is +infinite, -infinite, result is value ;
            result = value;
        }
    } else {
        result = std::trunc(value);
    }
    return builtins_common::GetTaggedDouble(result);
}
}  // namespace panda::ecmascript::builtins
