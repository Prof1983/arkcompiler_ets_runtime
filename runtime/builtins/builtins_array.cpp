/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cmath>

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/array_helper.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"
#include "plugins/ecmascript/runtime/base/typed_array_helper.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_stable_array.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript::builtins {
using ArrayHelper = ecmascript::base::ArrayHelper;
using TypedArrayHelper = ecmascript::base::TypedArrayHelper;

constexpr uint8_t INDEX_TWO = 2;

// 22.1.1
JSTaggedValue builtins::array::ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Array, ArrayConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    // 1. Let numberOfArgs be the number of arguments passed to this function call.
    uint32_t argc = argv->GetArgsNumber();

    // 3. If NewTarget is undefined, let new_target be the active function object, else let new_target be NewTarget.
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        new_target = constructor;
    }

    // 4. Let proto be GetPrototypeFromConstructor(new_target, "%ArrayPrototype%").
    // In NewJSObjectByConstructor(), will get prototype.
    // 5. ReturnIfAbrupt(proto).

    // 22.1.1.1 Array ( )
    if (argc == 0) {
        // 6. Return ArrayCreate(0, proto).
        return JSTaggedValue(JSArray::ArrayCreate(thread, JSTaggedNumber(0), new_target).GetObject<JSArray>());
    }

    // 22.1.1.2 Array(len)
    if (argc == 1) {
        // 6. Let array be ArrayCreate(0, proto).
        uint32_t new_len = 0;
        JSHandle<JSObject> new_array_handle(JSArray::ArrayCreate(thread, JSTaggedNumber(new_len), new_target));
        JSHandle<JSTaggedValue> len = builtins_common::GetCallArg(argv, 0);
        // 7. If Type(len) is not Number, then
        //   a. Let defineStatus be CreateDataProperty(array, "0", len).
        //   b. Assert: defineStatus is true.
        //   c. Let intLen be 1.
        // 8. Else,
        //   a. Let intLen be ToUint32(len).
        //   b. If intLen ≠ len, throw a RangeError exception.
        // 9. Let setStatus be Set(array, "length", intLen, true).
        // 10. Assert: setStatus is not an abrupt completion.
        if (!len->IsNumber()) {
            JSHandle<JSTaggedValue> key0(factory->NewFromCanBeCompressString("0"));
            JSObject::CreateDataProperty(thread, new_array_handle, key0, len);
            new_len = 1;
        } else {
            new_len = JSTaggedValue::ToUint32(thread, len);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (JSTaggedNumber(len.GetTaggedValue()).GetNumber() != new_len) {
                THROW_RANGE_ERROR_AND_RETURN(thread, "The length is out of range.", JSTaggedValue::Exception());
            }
        }
        JSArray::Cast(*new_array_handle)->SetArrayLength(thread, new_len);

        // 11. Return array.
        return new_array_handle.GetTaggedValue();
    }

    // 22.1.1.3 Array(...items )
    JSTaggedValue new_array = JSArray::ArrayCreate(thread, JSTaggedNumber(argc), new_target).GetTaggedValue();
    JSHandle<JSObject> new_array_handle(thread, new_array);
    if (!new_array.IsArray(thread)) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Failed to create array.", JSTaggedValue::Exception());
    }

    // 8. Let k be 0.
    // 9. Let items be a zero-origined List containing the argument items in order.
    // 10. Repeat, while k < numberOfArgs
    //   a. Let Pk be ToString(k).
    //   b. Let itemK be items[k].
    //   c. Let defineStatus be CreateDataProperty(array, Pk, itemK).
    //   d. Assert: defineStatus is true.
    //   e. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    for (uint32_t k = 0; k < argc; k++) {
        key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> item_k = builtins_common::GetCallArg(argv, k);
        JSObject::CreateDataProperty(thread, new_array_handle, key, item_k);
    }

    // 11. Assert: the value of array’s length property is numberOfArgs.
    // 12. Return array.
    JSArray::Cast(*new_array_handle)->SetArrayLength(thread, argc);
    return new_array_handle.GetTaggedValue();
}

// 22.1.2.1 Array.from ( items [ , mapfn [ , thisArg ] ] )
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue builtins::array::From(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Array, From);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    // 1. Let C be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If mapfn is undefined, let mapping be false.
    bool mapping = false;
    // 3. else
    //   a. If IsCallable(mapfn) is false, throw a TypeError exception.
    //   b. If thisArg was supplied, let T be thisArg; else let T be undefined.
    //   c. Let mapping be true
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, INDEX_TWO);
    JSHandle<JSTaggedValue> mapfn = builtins_common::GetCallArg(argv, 1);
    if (!mapfn->IsUndefined()) {
        if (!mapfn->IsCallable()) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "the mapfn is not callable.", JSTaggedValue::Exception());
        }
        mapping = true;
    }
    // 4. Let usingIterator be GetMethod(items, @@iterator).
    JSHandle<JSTaggedValue> items = builtins_common::GetCallArg(argv, 0);
    if (items->IsNull()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The items is null.", JSTaggedValue::Exception());
    }
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> iterator_symbol = env->GetIteratorSymbol();
    JSHandle<JSTaggedValue> using_iterator = JSObject::GetMethod(thread, items, iterator_symbol);
    // 5. ReturnIfAbrupt(usingIterator).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 6. If usingIterator is not undefined, then
    if (!using_iterator->IsUndefined()) {
        //   a. If IsConstructor(C) is true, then
        //     i. Let A be Construct(C).
        //   b. Else,
        //     i. Let A be ArrayCreate(0).
        //   c. ReturnIfAbrupt(A).
        JSTaggedValue new_array;
        if (this_handle->IsConstructor()) {
            auto info =
                NewRuntimeCallInfo(thread, this_handle, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 0);
            new_array = JSFunction::Construct(info.Get());
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else {
            new_array = JSArray::ArrayCreate(thread, JSTaggedNumber(0)).GetTaggedValue();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        if (!new_array.IsECMAObject()) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Failed to construct the array.", JSTaggedValue::Exception());
        }
        JSHandle<JSObject> new_array_handle(thread, new_array);
        //   d. Let iterator be GetIterator(items, usingIterator).
        JSHandle<JSTaggedValue> iterator = JSIterator::GetIterator(thread, items, using_iterator);
        //   e. ReturnIfAbrupt(iterator).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        //   f. Let k be 0.
        int k = 0;
        //   g. Repeat
        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        JSMutableHandle<JSTaggedValue> map_value(thread, JSTaggedValue::Undefined());
        while (true) {
            key.Update(JSTaggedValue(k));
            //     i. Let Pk be ToString(k).
            //     ii. Let next be IteratorStep(iterator).
            JSHandle<JSTaggedValue> next = JSIterator::IteratorStep(thread, iterator);
            //     iii. ReturnIfAbrupt(next).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            //     iv. If next is false, then
            //       1. Let setStatus be Set(A, "length", k, true).
            //       2. ReturnIfAbrupt(setStatus).
            //       3. Return A.
            if (next->IsFalse()) {
                JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_array_handle), length_key, key,
                                           true);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                return JSTaggedValue(new_array_handle.GetTaggedValue());
            }
            //     v. Let nextValue be IteratorValue(next).
            JSHandle<JSTaggedValue> next_value = JSIterator::IteratorValue(thread, next);
            //     vi. ReturnIfAbrupt(nextValue).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            //     vii. If mapping is true, then
            //       1. Let mappedValue be Call(mapfn, T, «nextValue, k»).
            //       2. If mappedValue is an abrupt completion, return IteratorClose(iterator, mappedValue).
            //       3. Let mappedValue be mappedValue.[[value]].
            //     viii. Else, let mappedValue be nextValue.
            if (mapping) {
                auto info = NewRuntimeCallInfo(thread, mapfn, this_arg_handle, JSTaggedValue::Undefined(), 2);
                info->SetCallArgs(next_value, key);
                JSTaggedValue call_result = JSFunction::Call(info.Get());  // 2: two args
                map_value.Update(call_result);
                JSTaggedValue map_result = JSIterator::IteratorClose(thread, iterator, map_value).GetTaggedValue();
                RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue(map_result));
            } else {
                map_value.Update(next_value.GetTaggedValue());
            }
            //     ix. Let defineStatus be CreateDataPropertyOrThrow(A, Pk, mappedValue).
            //     x. If defineStatus is an abrupt completion, return IteratorClose(iterator, defineStatus).
            //     xi. Increase k by 1.
            JSHandle<JSTaggedValue> define_status(
                thread, JSTaggedValue(JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, key, map_value)));
            JSTaggedValue define_result = JSIterator::IteratorClose(thread, iterator, define_status).GetTaggedValue();
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue(define_result));
            k++;
        }
    }
    // 7. Assert: items is not an Iterable so assume it is an array-like object.
    // 8. Let arrayLike be ToObject(items).
    JSHandle<JSObject> array_like_obj = JSTaggedValue::ToObject(thread, items);
    // 9. ReturnIfAbrupt(arrayLike).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> array_like(array_like_obj);
    // 10. Let len be ToLength(Get(arrayLike, "length")).
    double len = ArrayHelper::GetArrayLength(thread, array_like);
    // 11. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 12. If IsConstructor(C) is true, then
    //   a. Let A be Construct(C, «len»).
    // 13. Else,
    //   a. Let A be ArrayCreate(len).
    // 14. ReturnIfAbrupt(A).
    JSTaggedValue new_array;
    if (this_handle->IsConstructor()) {
        auto info = NewRuntimeCallInfo(thread, this_handle, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(JSTaggedValue(len));
        new_array = JSFunction::Construct(info.Get());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    } else {
        new_array = JSArray::ArrayCreate(thread, JSTaggedNumber(len)).GetTaggedValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    if (!new_array.IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Failed to construct the array.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> new_array_handle(thread, new_array);
    // 15. Let k be 0.
    // 16. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kValue be Get(arrayLike, Pk).
    //   d. If mapping is true, then
    //     i. Let mappedValue be Call(mapfn, T, «kValue, k»).
    //   e. Else, let mappedValue be kValue.
    //   f. Let defineStatus be CreateDataPropertyOrThrow(A, Pk, mappedValue).
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> map_value(thread, JSTaggedValue::Undefined());
    double k = 0;
    while (k < len) {
        JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, array_like, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (mapping) {
            key.Update(JSTaggedValue(k));
            auto info = NewRuntimeCallInfo(thread, mapfn, this_arg_handle, JSTaggedValue::Undefined(), 2);
            info->SetCallArgs(k_value, key);
            JSTaggedValue call_result = JSFunction::Call(info.Get());  // 2: two args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            map_value.Update(call_result);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else {
            map_value.Update(k_value.GetTaggedValue());
        }
        JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, k, map_value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        k++;
    }
    // 17. Let setStatus be Set(A, "length", len, true).
    JSHandle<JSTaggedValue> len_handle(thread, JSTaggedValue(len));
    JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_array_handle), length_key, len_handle, true);
    // 18. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 19. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.2.2 Array.isArray ( arg )
JSTaggedValue builtins::array::IsArray(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Array, IsArray);
    // 1. Return IsArray(arg).
    if (builtins_common::GetCallArg(argv, 0)->IsArray(argv->GetThread())) {
        return builtins_common::GetTaggedBoolean(true);
    }
    return builtins_common::GetTaggedBoolean(false);
}

// 22.1.2.3 Array.of ( ...items )
JSTaggedValue builtins::array::Of(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Array, Of);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> length_key = global_const->GetHandledLengthString();

    // 1. Let len be the actual number of arguments passed to this function.
    uint32_t argc = argv->GetArgsNumber();

    // 3. Let C be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 4. If IsConstructor(C) is true, then
    //   a. Let A be Construct(C, «len»).
    // 5. Else,
    //   a. Let A be ArrayCreate(len).
    // 6. ReturnIfAbrupt(A).
    JSHandle<JSTaggedValue> new_array;
    if (this_handle->IsConstructor()) {
        JSHandle<JSTaggedValue> undefined = global_const->GetHandledUndefined();

        auto info = NewRuntimeCallInfo(thread, this_handle, JSTaggedValue::Undefined(), undefined, 1);
        info->SetCallArgs(JSTaggedValue(argc));
        JSTaggedValue tagged_array = JSFunction::Construct(info.Get());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        new_array = JSHandle<JSTaggedValue>(thread, tagged_array);
    } else {
        new_array = JSArray::ArrayCreate(thread, JSTaggedNumber(argc));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    if (!new_array->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Failed to create Object.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> new_array_handle(new_array);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let kValue be items[k].
    //   b. Let Pk be ToString(k).
    //   c. Let defineStatus be CreateDataPropertyOrThrow(A,Pk, kValue).
    //   d. ReturnIfAbrupt(defineStatus).
    //   e. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    for (uint32_t k = 0; k < argc; k++) {
        key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> k_value = builtins_common::GetCallArg(argv, k);
        JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, key, k_value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // 9. Let setStatus be Set(A, "length", len, true).
    JSHandle<JSTaggedValue> len_handle(thread, JSTaggedValue(argc));
    JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_array_handle), length_key, len_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 11. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.2.5 get Array [ @@species ]
JSTaggedValue builtins::array::GetSpecies([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    // 1. Return the this value.
    return builtins_common::GetThis(argv).GetTaggedValue();
}

// 22.1.3.1 Array.prototype.concat ( ...arguments )
JSTaggedValue builtins::array::proto::Concat(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Concat);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    uint32_t argc = argv->GetArgsNumber();

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let A be ArraySpeciesCreate(O, 0).
    uint32_t array_len = 0;
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(array_len));
    // 4. ReturnIfAbrupt(A).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSObject> new_array_handle(thread, new_array);

    // 5. Let n be 0.
    double n = 0;
    JSMutableHandle<JSTaggedValue> from_key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> to_key(thread, JSTaggedValue::Undefined());
    bool is_spreadable = ArrayHelper::IsConcatSpreadable(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (is_spreadable) {
        double this_len = ArrayHelper::GetArrayLength(thread, this_obj_val);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (n + this_len > ecmascript::base::MAX_SAFE_INTEGER) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
        }
        double k = 0;
        while (k < this_len) {
            from_key.Update(JSTaggedValue(k));
            to_key.Update(JSTaggedValue(n));
            bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, from_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (exists) {
                JSHandle<JSTaggedValue> from_val_handle =
                    JSArray::FastGetPropertyByValue(thread, this_obj_val, from_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, to_key, from_val_handle);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            n++;
            k++;
        }
    } else {
        if (n >= ecmascript::base::MAX_SAFE_INTEGER) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
        }
        JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, n, this_obj_val);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        n++;
    }
    // 7. Repeat, while items is not empty
    for (uint32_t i = 0; i < argc; i++) {
        // a. Remove the first element from items and let E be the value of the element
        JSHandle<JSTaggedValue> add_handle = builtins_common::GetCallArg(argv, i);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<JSObject> add_obj_handle(add_handle);

        // b. Let spreadable be IsConcatSpreadable(E).
        is_spreadable = ArrayHelper::IsConcatSpreadable(thread, add_handle);
        // c. ReturnIfAbrupt(spreadable).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // d. If spreadable is true, then
        if (is_spreadable) {
            // ii. Let len be ToLength(Get(E, "length")).
            double len = ArrayHelper::GetArrayLength(thread, JSHandle<JSTaggedValue>::Cast(add_obj_handle));
            // iii. ReturnIfAbrupt(len).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // iv. If n + len > 253-1, throw a TypeError exception.
            if (n + len > ecmascript::base::MAX_SAFE_INTEGER) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
            }
            double k = 0;
            // v. Repeat, while k < len
            while (k < len) {
                from_key.Update(JSTaggedValue(k));
                to_key.Update(JSTaggedValue(n));
                // 1. Let P be ToString(k).
                // 2. Let exists be HasProperty(E, P).
                // 4. If exists is true, then
                bool exists =
                    JSTaggedValue::HasProperty(thread, JSHandle<JSTaggedValue>::Cast(add_obj_handle), from_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                if (exists) {
                    // a. Let subElement be Get(E, P).
                    JSHandle<JSTaggedValue> from_val_handle =
                        JSArray::FastGetPropertyByValue(thread, add_handle, from_key);
                    // b. ReturnIfAbrupt(subElement).
                    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                    JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, to_key, from_val_handle);
                    // d. ReturnIfAbrupt(status).
                    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                }
                // 5. Increase n by 1.
                // 6. Increase k by 1.
                n++;
                k++;
            }
        } else {  // e. Else E is added as a single item rather than spread,
                  // i. If n≥253-1, throw a TypeError exception.
            if (n >= ecmascript::base::MAX_SAFE_INTEGER) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
            }
            // ii. Let status be CreateDataPropertyOrThrow (A, ToString(n), E).
            JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, n, add_handle);
            // iii. ReturnIfAbrupt(status).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            // iv. Increase n by 1.
            n++;
        }
    }
    // 8. Let setStatus be Set(A, "length", n, true).
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> len_handle(thread, JSTaggedValue(n));
    JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_array_handle), length_key, len_handle, true);
    // 9. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 10. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.3.3 Array.prototype.copyWithin (target, start [ , end ] )
JSTaggedValue builtins::array::proto::CopyWithin(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, CopyWithin);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    double copy_to;
    double copy_from;
    double copy_end;

    // 5. Let relativeTarget be ToInteger(target).
    JSTaggedNumber target_temp = JSTaggedValue::ToInteger(thread, builtins_common::GetCallArg(argv, 0));
    // 6. ReturnIfAbrupt(relativeTarget).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double target = target_temp.GetNumber();
    // 7. If relativeTarget < 0, let to be max((len + relativeTarget),0); else let to be min(relativeTarget, len).
    if (target < 0) {
        copy_to = target + len > 0 ? target + len : 0;
    } else {
        copy_to = target < len ? target : len;
    }

    // 8. Let relativeStart be ToInteger(start).
    JSTaggedNumber start_t = JSTaggedValue::ToInteger(thread, builtins_common::GetCallArg(argv, 1));
    // 9. ReturnIfAbrupt(relativeStart).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double start = start_t.GetNumber();
    // 10. If relativeStart < 0, let from be max((len + relativeStart),0); else let from be min(relativeStart, len).
    if (start < 0) {
        copy_from = start + len > 0 ? start + len : 0;
    } else {
        copy_from = start < len ? start : len;
    }

    // 11. If end is undefined, let relativeEnd be len; else let relativeEnd be ToInteger(end).
    double end = len;
    JSHandle<JSTaggedValue> msg3 = builtins_common::GetCallArg(argv, INDEX_TWO);
    if (!msg3->IsUndefined()) {
        JSTaggedNumber temp = JSTaggedValue::ToInteger(thread, msg3);
        // 12. ReturnIfAbrupt(relativeEnd).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        end = temp.GetNumber();
    }

    // 13. If relativeEnd < 0, let final be max((len + relativeEnd),0); else let final be min(relativeEnd, len).
    if (end < 0) {
        copy_end = end + len > 0 ? end + len : 0;
    } else {
        copy_end = end < len ? end : len;
    }

    // 14. Let count be min(final-from, len-to).
    double count = (copy_end - copy_from < len - copy_to) ? (copy_end - copy_from) : (len - copy_to);

    // 15. If from<to and to<from+count
    //   a. Let direction be -1.
    //   b. Let from be from + count -1.
    //   c. Let to be to + count -1.
    // 16. Else,
    //   a. Let direction = 1.
    double direction = 1;
    if (copy_from < copy_to && copy_to < copy_from + count) {
        direction = -1;
        copy_from = copy_from + count - 1;
        copy_to = copy_to + count - 1;
    }

    // 17. Repeat, while count > 0
    //   a. Let fromKey be ToString(from).
    //   b. Let toKey be ToString(to).
    //   c. Let fromPresent be HasProperty(O, fromKey).
    //   d. ReturnIfAbrupt(fromPresent).
    //   e. If fromPresent is true, then
    //     i. Let fromVal be Get(O, fromKey).
    //     ii. ReturnIfAbrupt(fromVal).
    //     iii. Let setStatus be Set(O, toKey, fromVal, true).
    //     iv. ReturnIfAbrupt(setStatus).
    //   f. Else fromPresent is false,
    //     i. Let deleteStatus be DeletePropertyOrThrow(O, toKey).
    //     ii. ReturnIfAbrupt(deleteStatus).
    //   g. Let from be from + direction.
    //   h. Let to be to + direction.
    //   i. Let count be count − 1.
    JSMutableHandle<JSTaggedValue> from_key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> to_key(thread, JSTaggedValue::Undefined());
    while (count > 0) {
        from_key.Update(JSTaggedValue(copy_from));
        to_key.Update(JSTaggedValue(copy_to));
        bool exists = (this_obj_val->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, from_key));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> from_val_handle = JSArray::FastGetPropertyByValue(thread, this_obj_val, from_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSArray::FastSetPropertyByValue(thread, this_obj_val, to_key, from_val_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else {
            if (this_obj_val->IsJSProxy()) {
                to_key.Update(JSTaggedValue::ToString(thread, to_key).GetTaggedValue());
            }
            JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, to_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        copy_from = copy_from + direction;
        copy_to = copy_to + direction;
        count--;
    }

    // 18. Return O.
    return this_obj_handle.GetTaggedValue();
}

// 22.1.3.4 Array.prototype.entries ( )
JSTaggedValue builtins::array::proto::Entries(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Entries);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let O be ToObject(this value).
    // 2. ReturnIfAbrupt(O).
    JSHandle<JSObject> self = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return CreateArrayIterator(O, "key+value").
    JSHandle<JSArrayIterator> iter(factory->NewJSArrayIterator(self, IterationKind::KEY_AND_VALUE));
    return iter.GetTaggedValue();
}

// 22.1.3.5 Array.prototype.every ( callbackfn [ , thisArg] )
JSTaggedValue builtins::array::proto::Every(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let testResult be ToBoolean(Call(callbackfn, T, «kValue, k, O»)).
    //     iv. ReturnIfAbrupt(testResult).
    //     v. If testResult is false, return false.
    //   e. Increase k by 1.

    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    uint32_t k = 0;
    while (k < len) {
        bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            key.Update(JSTaggedValue(k));
            auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
            info->SetCallArgs(k_value, key, this_obj_val);
            JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            bool bool_result = call_result.ToBoolean();
            if (!bool_result) {
                return builtins_common::GetTaggedBoolean(false);
            }
        }
        k++;
    }

    // 9. Return true.
    return builtins_common::GetTaggedBoolean(true);
}

// ES2019 22.1.3.10.1 FlattenIntoArray(target, source, sourceLen, start, depth, [, mapperFunction, thisArg])
static JSTaggedValue FlattenIntoArray(JSThread *thread, const JSHandle<JSObject> &target,
                                      const JSHandle<JSTaggedValue> &source, double source_len, double start,
                                      double depth, const JSHandle<JSTaggedValue> &mapper_func,
                                      const JSHandle<JSTaggedValue> &this_arg)
{
    JSTaggedValue target_idx(start);
    // 1. - 3.
    for (uint32_t src_idx = 0; src_idx < source_len; ++src_idx) {
        // a. Let P be ! ToString(sourceIndex).
        JSTaggedValue prop =
            JSTaggedValue::ToString(thread, JSHandle<JSTaggedValue>(thread, JSTaggedValue(src_idx))).GetTaggedValue();
        JSHandle<JSTaggedValue> prop_handle(thread, prop);

        // b. Let exists be ? HasProperty(source, P).
        // c. If exists is true, then
        bool is_exists = JSTaggedValue::HasProperty(thread, source, prop_handle);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

        if (!is_exists) {
            continue;
        }

        // i. Let element be ? Get(source, P).
        //  ii. If mapperFunction is present, then
        //      1. Assert: thisArg is present.
        //      2. Set element to ? Call(mapperFunction, thisArg , « element, sourceIndex, source »).
        JSHandle<JSTaggedValue> element(JSTaggedValue::GetProperty(thread, source, prop_handle).GetValue());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (!mapper_func->IsHole()) {
            ASSERT(!this_arg->IsHole());

            auto info = NewRuntimeCallInfo(thread, mapper_func, this_arg, JSTaggedValue::Undefined(), 3);
            info->SetCallArgs(element.GetTaggedValue(), JSTaggedNumber(src_idx), source.GetTaggedValue());
            JSTaggedValue res = JSFunction::Call(info.Get());  // 3: three args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            element = JSHandle<JSTaggedValue>(thread, res);
        }

        //  iii. Let shouldFlatten be false.
        //  iv. If depth > 0, then
        //      1. Set shouldFlatten to ? IsArray(element).
        //  v. If shouldFlatten is true, then
        //      1. Let elementLen be ? ToLength(? Get(element, "length")).
        //      2. Set targetIndex to ? FlattenIntoArray(target, element, elementLen, targetIndex, depth - 1).
        //  vi. Else,
        //     1. If targetIndex ≥ 2^53-1, throw a TypeError exception.
        //     2. Perform ? CreateDataPropertyOrThrow(target, ! ToString(targetIndex), element).
        //     3. Increase targetIndex by 1.
        if ((depth > 0) && element->IsJSArray()) {
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            double element_len = ArrayHelper::GetLength(thread, element);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            target_idx = FlattenIntoArray(thread, target, element, element_len, target_idx.GetNumber(), depth - 1,
                                          JSHandle<JSTaggedValue>(thread, JSTaggedValue::Hole()),
                                          JSHandle<JSTaggedValue>(thread, JSTaggedValue::Hole()));
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else {
            if (target_idx.GetNumber() >= ecmascript::base::MAX_SAFE_INTEGER) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "out of range", JSTaggedValue::Exception());
            }
            JSTaggedValue str =
                // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue
                // (issue #I5QOJX)
                // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
                JSTaggedValue::ToString(thread, JSHandle<JSTaggedValue>(thread, target_idx)).GetTaggedValue();
            JSHandle<JSTaggedValue> str_handle(thread, str);
            JSObject::CreateDataPropertyOrThrow(thread, target, str_handle, element);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            target_idx = JSTaggedValue(target_idx.GetNumber() + 1);
        }
    }
    // 4. Return targetIndex.
    // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue (issue #I5QOJX)
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    return target_idx;
}

// ES2019 22.1.3.10 Array.prototype.flat( [ depth ] )
JSTaggedValue builtins::array::proto::Flat(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Flat);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ? ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 2. Let sourceLen be ? ToLength(? Get(O, "length")).
    double source_len = ArrayHelper::GetLength(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Let depthNum be 1.
    // 4. If depth is not undefined, then
    //      a. Set depthNum to ? ToInteger(depth)
    double depth_num = 1;
    JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 0);
    if (!msg1->IsUndefined()) {
        JSTaggedValue depth = JSTaggedValue::ToInteger(thread, msg1);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        depth_num = depth.GetNumber();
    }

    // 5. Let A be ? ArraySpeciesCreate(O, 0)
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(0));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSObject> new_array_handle(thread, new_array);

    // 6. Perform FlattenIntoArray(target, source, sourceLen, start, depth)
    FlattenIntoArray(thread, new_array_handle, this_handle, source_len, 0, depth_num,
                     JSHandle<JSTaggedValue>(thread, JSTaggedValue::Hole()),
                     JSHandle<JSTaggedValue>(thread, JSTaggedValue::Hole()));

    return new_array_handle.GetTaggedValue();
}

// ES2019 22.1.3.11 Array.prototype.flatMap(mapperFunction [ , thisArg ]])
JSTaggedValue builtins::array::proto::FlatMap(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, FlatMap);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    uint32_t argc = argv->GetArgsNumber();

    // 1. Let O be ? ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);

    // 2. Let sourceLen be ? ToLength(? Get(O, "length")).
    double source_len = ArrayHelper::GetLength(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. If IsCallable(mapperFunction) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> mapper_function = builtins_common::GetCallArg(argv, 0);
    if (!mapper_function->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Object is not callable", JSTaggedValue::Exception());
    }
    // 4. If thisArg is present, let T be thisArg; else let T be undefined.
    JSMutableHandle<JSTaggedValue> this_arg(thread, JSTaggedValue::Undefined());
    if (argc > 1) {
        this_arg.Update(builtins_common::GetCallArg(argv, 1).GetTaggedValue());
    }
    // 5. Let A be ? ArraySpeciesCreate(O, 0).
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(0));
    JSHandle<JSObject> new_array_handle(thread, new_array);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. Perform ? FlattenIntoArray(A, O, sourceLen, 0, 1, mapperFunction, T).

    FlattenIntoArray(thread, new_array_handle, this_handle, source_len, 0, 1, mapper_function, this_arg);

    // 7. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.3.6 Array.prototype.fill (value [ , start [ , end ] ] )
JSTaggedValue builtins::array::proto::Fill(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Fill);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    if (this_handle->IsTypedArray()) {
        ContentType content_type = JSHandle<JSTypedArray>::Cast(this_handle)->GetContentType();
        if (content_type == ContentType::BIG_INT) {
            value = JSHandle<JSTaggedValue>(thread, JSTaggedValue::ToBigInt(thread, value));
        } else {
            value = JSHandle<JSTaggedValue>(thread, JSTaggedValue::ToNumber(thread, value));
        }
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Let relativeStart be ToInteger(start).
    double start;
    JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 1);
    JSTaggedNumber arg_start_temp = JSTaggedValue::ToInteger(thread, msg1);
    // 6. ReturnIfAbrupt(relativeStart).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double arg_start = arg_start_temp.GetNumber();
    // 7. If relativeStart < 0, let k be max((len + relativeStart),0); else let k be min(relativeStart, len).
    if (arg_start < 0) {
        start = arg_start + len > 0 ? arg_start + len : 0;
    } else {
        start = arg_start < len ? arg_start : len;
    }

    // 8. If end is undefined, let relativeEnd be len; else let relativeEnd be ToInteger(end).
    double arg_end = len;
    JSHandle<JSTaggedValue> msg2 = builtins_common::GetCallArg(argv, INDEX_TWO);
    if (!msg2->IsUndefined()) {
        JSTaggedNumber arg_end_temp = JSTaggedValue::ToInteger(thread, msg2);
        // 9. ReturnIfAbrupt(relativeEnd).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        arg_end = arg_end_temp.GetNumber();
    }

    // 10. If relativeEnd < 0, let final be max((len + relativeEnd),0); else let final be min(relativeEnd, len).
    double end;
    if (arg_end < 0) {
        end = arg_end + len > 0 ? arg_end + len : 0;
    } else {
        end = arg_end < len ? arg_end : len;
    }

    // 11. Repeat, while k < final
    //   a. Let Pk be ToString(k).
    //   b. Let setStatus be Set(O, Pk, value, true).
    //   c. ReturnIfAbrupt(setStatus).
    //   d. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    double k = start;
    while (k < end) {
        key.Update(JSTaggedValue(k));
        JSArray::FastSetPropertyByValue(thread, this_obj_val, key, value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        k++;
    }

    // 12. Return O.
    return this_obj_handle.GetTaggedValue();
}

// 22.1.3.7 Array.prototype.filter ( callbackfn [ , thisArg ] )
JSTaggedValue builtins::array::proto::Filter(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let A be ArraySpeciesCreate(O, 0).
    int32_t array_len = 0;
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(array_len));
    // 8. ReturnIfAbrupt(A).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSObject> new_array_handle(thread, new_array);

    // 9. Let k be 0.
    // 10. Let to be 0.
    // 11. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let selected be ToBoolean(Call(callbackfn, T, «kValue, k, O»)).
    //     iv. ReturnIfAbrupt(selected).
    //     v. If selected is true, then
    //       1. Let status be CreateDataPropertyOrThrow (A, ToString(to), kValue).
    //       2. ReturnIfAbrupt(status).
    //       3. Increase to by 1.
    //   e. Increase k by 1.
    double to_index = 0;
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> to_index_handle(thread, JSTaggedValue::Undefined());

    uint32_t k = 0;
    while (k < len) {
        bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            key.Update(JSTaggedValue(k));
            auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
            info->SetCallArgs(k_value, key, this_obj_val);
            JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
            bool bool_result = call_result.ToBoolean();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (bool_result) {
                to_index_handle.Update(JSTaggedValue(to_index));
                JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, to_index_handle, k_value);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                to_index++;
            }
        }
        k++;
    }

    // 12. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.3.8 Array.prototype.find ( predicate [ , thisArg ] )
JSTaggedValue builtins::array::proto::Find(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Find);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(predicate) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the predicate is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kValue be Get(O, Pk).
    //   c. ReturnIfAbrupt(kValue).
    //   d. Let testResult be ToBoolean(Call(predicate, T, «kValue, k, O»)).
    //   e. ReturnIfAbrupt(testResult).
    //   f. If testResult is true, return kValue.
    //   g. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());

    uint32_t k = 0;
    while (k < len) {
        JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        key.Update(JSTaggedValue(k));
        auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
        info->SetCallArgs(k_value, key, this_obj_val);
        JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        bool bool_result = call_result.ToBoolean();
        if (bool_result) {
            return k_value.GetTaggedValue();
        }
        k++;
    }

    // 9. Return undefined.
    return JSTaggedValue::Undefined();
}

// 22.1.3.9 Array.prototype.findIndex ( predicate [ , thisArg ] )
JSTaggedValue builtins::array::proto::FindIndex(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, FindIndex);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(predicate) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the predicate is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kValue be Get(O, Pk).
    //   c. ReturnIfAbrupt(kValue).
    //   d. Let testResult be ToBoolean(Call(predicate, T, «kValue, k, O»)).
    //   e. ReturnIfAbrupt(testResult).
    //   f. If testResult is true, return k.
    //   g. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());

    uint32_t k = 0;
    while (k < len) {
        JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        key.Update(JSTaggedValue(k));
        auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
        info->SetCallArgs(k_value, key, this_obj_val);
        JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
        bool bool_result = call_result.ToBoolean();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (bool_result) {
            return builtins_common::GetTaggedDouble(k);
        }
        k++;
    }

    // 9. Return -1.
    return builtins_common::GetTaggedDouble(-1);
}

// 22.1.3.10 Array.prototype.forEach ( callbackfn [ , thisArg ] )
JSTaggedValue builtins::array::proto::ForEach(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let funcResult be Call(callbackfn, T, «kValue, k, O»).
    //     iv. ReturnIfAbrupt(funcResult).
    //   e. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());

    uint32_t k = 0;
    while (k < len) {
        bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            key.Update(JSTaggedValue(k));
            auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
            info->SetCallArgs(k_value, key, this_obj_val);
            JSTaggedValue func_result = JSFunction::Call(info.Get());  // 3: three args
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, func_result);
        }
        k++;
    }

    // 9. Return undefined.
    return JSTaggedValue::Undefined();
}

// ES2021 23.1.3.13 Array.prototype.includes ( searchElement [ , fromIndex ] )
JSTaggedValue builtins::array::proto::Includes(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Includes);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 2. Let len be ? LengthOfArrayLike(O).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. If len is 0, return false.
    if (len == 0) {
        return builtins_common::GetTaggedBoolean(false);
    }

    ArraySizeT argc = argv->GetArgsNumber();
    double from_index = 0;

    if (argc > 1) {
        // 4-5. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be 0.
        JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 1);
        JSTaggedNumber from_index_temp = JSTaggedValue::ToNumber(thread, msg1);

        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        from_index = ecmascript::base::NumberHelper::TruncateDouble(from_index_temp.GetNumber());

        // 6. If n is positiveInfinity, return false.
        if (JSTaggedValue::Equal(thread, msg1,
                                 JSHandle<JSTaggedValue>(thread, JSTaggedValue(base::POSITIVE_INFINITY)))) {
            return builtins_common::GetTaggedBoolean(false);
        }

        // 7. Else if n is negativeInfinity, set n to 0.
        if (JSTaggedValue::Equal(thread, msg1,
                                 JSHandle<JSTaggedValue>(thread, JSTaggedValue(-base::POSITIVE_INFINITY)))) {
            from_index = 0;
        }
    }

    // 8. If n ≥ 0, then
    //   a. Let k be n.
    // 9. Else,
    //   a. Let k be len + n.
    //   b. If k < 0, set k to 0.
    double from = (from_index >= 0) ? from_index : ((len + from_index) >= 0 ? len + from_index : 0);

    // 10.
    const JSHandle<JSTaggedValue> search_element = builtins_common::GetCallArg(argv, 0);
    while (from < len) {
        // a)
        JSHandle<JSTaggedValue> index_handle(thread, JSTaggedValue(from));
        JSHandle<JSTaggedValue> handle =
            JSHandle<JSTaggedValue>(thread, JSTaggedValue::ToString(thread, index_handle).GetTaggedValue());
        JSHandle<JSTaggedValue> element = JSTaggedValue::GetProperty(thread, this_obj_val, handle).GetValue();

        // b)
        if (JSTaggedValue::SameValueZero(search_element.GetTaggedValue(), element.GetTaggedValue())) {
            return builtins_common::GetTaggedBoolean(true);
        }

        // c)
        from++;
    }

    return builtins_common::GetTaggedBoolean(false);
}

// 22.1.3.11 Array.prototype.indexOf ( searchElement [ , fromIndex ] )
JSTaggedValue builtins::array::proto::IndexOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, IndexOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    uint32_t argc = argv->GetArgsNumber();

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    JSHandle<JSTaggedValue> search_element = builtins_common::GetCallArg(argv, 0);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If len is 0, return −1.
    if (len == 0) {
        return builtins_common::GetTaggedInt(-1);
    }

    // 6. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be 0.
    double from_index = 0;
    if (argc > 1) {
        JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 1);
        JSTaggedNumber from_index_temp = JSTaggedValue::ToNumber(thread, msg1);
        // 7. ReturnIfAbrupt(n).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        from_index = ecmascript::base::NumberHelper::TruncateDouble(from_index_temp.GetNumber());
    }

    // 8. If n ≥ len, return −1.
    if (from_index >= len) {
        return builtins_common::GetTaggedInt(-1);
    }

    // 9. If n ≥ 0, then
    //   a. Let k be n.
    // 10. Else n<0,
    //   a. Let k be len - abs(n).
    //   b. If k < 0, let k be 0.
    double from = (from_index >= 0) ? from_index : ((len + from_index) >= 0 ? len + from_index : 0);

    // 11. Repeat, while k<len
    //   a. Let kPresent be HasProperty(O, ToString(k)).
    //   b. ReturnIfAbrupt(kPresent).
    //   c. If kPresent is true, then
    //     i. Let elementK be Get(O, ToString(k)).
    //     ii. ReturnIfAbrupt(elementK).
    //     iii. Let same be the result of performing Strict Equality Comparison searchElement === elementK.
    //     iv. If same is true, return k.
    //   d. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    while (from < len) {
        key.Update(JSTaggedValue(from));
        bool exists = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, key));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value_handle = JSArray::FastGetPropertyByValue(thread, this_obj_val, key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (JSTaggedValue::StrictEqual(thread, search_element, k_value_handle)) {
                return builtins_common::GetTaggedDouble(from);
            }
        }
        from++;
    }

    // 12. Return -1.
    return builtins_common::GetTaggedInt(-1);
}

// 22.1.3.12 Array.prototype.join (separator)
JSTaggedValue builtins::array::proto::Join(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Join);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);

    // Prevent inifinity joining of recursive arrays
    static std::vector<TaggedType> visited_stack = {};
    if (std::find(visited_stack.begin(), visited_stack.end(), this_handle->GetRawData()) != visited_stack.end()) {
        return builtins_common::GetTaggedString(thread, "");
    }
    visited_stack.push_back(this_handle->GetRawData());
    auto unvisit = std::unique_ptr<void, std::function<void(void *)>>(
        reinterpret_cast<void *>(1), [&](void * /* unused */) { visited_stack.pop_back(); });

    if (this_handle->IsStableJSArray(thread)) {
        return JSStableArray::Join(JSHandle<JSArray>::Cast(this_handle), argv);
    }
    auto factory = thread->GetEcmaVM()->GetFactory();

    // 1. Let O be ToObject(this value).
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If separator is undefined, let separator be the single-element String ",".
    // 6. Let sep be ToString(separator).
    JSHandle<JSTaggedValue> sep_handle;
    if ((builtins_common::GetCallArg(argv, 0)->IsUndefined())) {
        sep_handle = JSHandle<JSTaggedValue>::Cast(factory->NewFromCanBeCompressString(","));
    } else {
        sep_handle = builtins_common::GetCallArg(argv, 0);
    }

    JSHandle<EcmaString> sep_string_handle = JSTaggedValue::ToString(thread, sep_handle);
    // 7. ReturnIfAbrupt(sep).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    int32_t sep_len = sep_string_handle->GetLength();
    std::u16string sep_str;
    if (sep_string_handle->IsUtf16()) {
        sep_str = ecmascript::base::StringHelper::Utf16ToU16String(sep_string_handle->GetDataUtf16(), sep_len);
    } else {
        sep_str = ecmascript::base::StringHelper::Utf8ToU16String(sep_string_handle->GetDataUtf8(), sep_len);
    }

    // 8. If len is zero, return the empty String.
    if (len == 0) {
        return builtins_common::GetTaggedString(thread, "");
    }

    // 9. Let element0 be Get(O, "0").
    // 10. If element0 is undefined or null, let R be the empty String; otherwise, let R be ToString(element0).
    // 11. ReturnIfAbrupt(R).
    // 12. Let k be 1.
    // 13. Repeat, while k < len
    //   a. Let S be the String value produced by concatenating R and sep.
    //   b. Let element be Get(O, ToString(k)).
    //   c. If element is undefined or null, let next be the empty String; otherwise, let next be ToString(element).
    //   d. ReturnIfAbrupt(next).
    //   e. Let R be a String value produced by concatenating S and next.
    //   f. Increase k by 1.
    std::u16string concat_str;
    std::u16string concat_str_new;
    for (int32_t k = 0; k < len; k++) {
        std::u16string next_str;
        JSHandle<JSTaggedValue> element = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (!element->IsUndefined() && !element->IsNull()) {
            JSHandle<EcmaString> next_string_handle = JSTaggedValue::ToString(thread, element);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            int32_t next_len = next_string_handle->GetLength();
            if (next_string_handle->IsUtf16()) {
                next_str =
                    ecmascript::base::StringHelper::Utf16ToU16String(next_string_handle->GetDataUtf16(), next_len);
            } else {
                next_str = ecmascript::base::StringHelper::Utf8ToU16String(next_string_handle->GetDataUtf8(), next_len);
            }
        }
        if (k > 0) {
            concat_str_new = ecmascript::base::StringHelper::Append(concat_str, sep_str);
            concat_str = ecmascript::base::StringHelper::Append(concat_str_new, next_str);
            continue;
        }
        concat_str = ecmascript::base::StringHelper::Append(concat_str, next_str);
    }

    // 14. Return R.
    const char16_t *const_char16t_data = concat_str.data();
    auto *char16t_data = const_cast<char16_t *>(const_char16t_data);
    auto *uint16t_data = reinterpret_cast<uint16_t *>(char16t_data);
    int32_t u16str_size = concat_str.size();
    return factory->NewFromUtf16Literal(uint16t_data, u16str_size).GetTaggedValue();
}

// 22.1.3.13 Array.prototype.keys ( )
JSTaggedValue builtins::array::proto::Keys(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Keys);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let O be ToObject(this value).
    // 2. ReturnIfAbrupt(O).
    JSHandle<JSObject> self = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return CreateArrayIterator(O, "key").
    JSHandle<JSArrayIterator> iter(factory->NewJSArrayIterator(self, IterationKind::KEY));
    return iter.GetTaggedValue();
}

// 22.1.3.14 Array.prototype.lastIndexOf ( searchElement [ , fromIndex ] )
JSTaggedValue builtins::array::proto::LastIndexOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, LastIndexOf);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    uint32_t argc = argv->GetArgsNumber();

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    JSHandle<JSTaggedValue> search_element = builtins_common::GetCallArg(argv, 0);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If len is 0, return −1.
    if (len == 0) {
        return builtins_common::GetTaggedInt(-1);
    }

    // 6. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be len-1.
    double from_index = len - 1;
    if (argc > 1) {
        JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 1);
        JSTaggedNumber from_index_temp = JSTaggedValue::ToNumber(thread, msg1);
        // 7. ReturnIfAbrupt(n).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        from_index = ecmascript::base::NumberHelper::TruncateDouble(from_index_temp.GetNumber());
    }

    // 8. If n ≥ 0, let k be min(n, len – 1).
    // 9. Else n < 0,
    //   a. Let k be len - abs(n).
    double from = (from_index >= 0) ? ((len - 1) < from_index ? len - 1 : from_index) : len + from_index;

    // 10. Repeat, while k≥ 0
    //   a. Let kPresent be HasProperty(O, ToString(k)).
    //   b. ReturnIfAbrupt(kPresent).
    //   c. If kPresent is true, then
    //     i. Let elementK be Get(O, ToString(k)).
    //     ii. ReturnIfAbrupt(elementK).
    //     iii. Let same be the result of performing Strict Equality Comparison searchElement === elementK.
    //     iv. If same is true, return k.
    //   d. Decrease k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    while (from >= 0) {
        key.Update(JSTaggedValue(from));
        bool exists = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, key));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value_handle = JSArray::FastGetPropertyByValue(thread, this_obj_val, key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (JSTaggedValue::StrictEqual(thread, search_element, k_value_handle)) {
                return builtins_common::GetTaggedDouble(from);
            }
        }
        from--;
    }

    // 11. Return -1.
    return builtins_common::GetTaggedInt(-1);
}

// 22.1.3.15 Array.prototype.map ( callbackfn [ , thisArg ] )
JSTaggedValue builtins::array::proto::Map(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Map);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let A be ArraySpeciesCreate(O, len).
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(len));
    // 8. ReturnIfAbrupt(A).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (!new_array.IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Failed to create Object.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> new_array_handle(thread, new_array);

    // 9. Let k be 0.
    // 10. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let mappedValue be Call(callbackfn, T, «kValue, k, O»).
    //     iv. ReturnIfAbrupt(mappedValue).
    //     v. Let status be CreateDataPropertyOrThrow (A, Pk, mappedValue).
    //     vi. ReturnIfAbrupt(status).
    //   e. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> map_result_handle(thread, JSTaggedValue::Undefined());

    uint32_t k = 0;
    while (k < len) {
        bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            key.Update(JSTaggedValue(k));
            auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
            info->SetCallArgs(k_value, key, this_obj_val);
            JSTaggedValue map_result = JSFunction::Call(info.Get());  // 3: three args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            map_result_handle.Update(map_result);
            JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, k, map_result_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        k++;
    }

    // 11. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.3.16 Array.prototype.pop ( )
JSTaggedValue builtins::array::proto::Pop(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Pop);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    if (this_handle->IsStableJSArray(thread)) {
        return JSStableArray::Pop(JSHandle<JSArray>::Cast(this_handle), argv);
    }
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. If len is zero,
    //   a. Let setStatus be Set(O, "length", 0, true).
    //   b. ReturnIfAbrupt(setStatus).
    //   c. Return undefined.
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    if (len == 0) {
        JSHandle<JSTaggedValue> length_value(thread, JSTaggedValue(0));
        JSTaggedValue::SetProperty(thread, this_obj_val, length_key, length_value, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        return JSTaggedValue::Undefined();
    }

    // 6. Else len > 0,
    //   a. Let newLen be len–1.
    //   b. Let indx be ToString(newLen).
    //   c. Let element be Get(O, indx).
    //   d. ReturnIfAbrupt(element).
    //   e. Let deleteStatus be DeletePropertyOrThrow(O, indx).
    //   f. ReturnIfAbrupt(deleteStatus).
    //   g. Let setStatus be Set(O, "length", newLen, true).
    //   h. ReturnIfAbrupt(setStatus).
    //   i. Return element.
    double new_len = len - 1;
    JSHandle<JSTaggedValue> index_handle(thread, JSTaggedValue(new_len));
    JSHandle<JSTaggedValue> element = JSTaggedValue::GetProperty(thread, this_obj_val, index_handle).GetValue();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, index_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSTaggedValue::SetProperty(thread, this_obj_val, length_key, index_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return element.GetTaggedValue();
}

// 22.1.3.17 Array.prototype.push ( ...items )
JSTaggedValue builtins::array::proto::Push(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Push);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    if (this_handle->IsStableJSArray(thread)) {
        return JSStableArray::Push(JSHandle<JSArray>::Cast(this_handle), argv);
    }
    // 6. Let argCount be the number of elements in items.
    uint32_t argc = argv->GetArgsNumber();

    // 1. Let O be ToObject(this value).
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 7. If len + argCount > 253-1, throw a TypeError exception.
    if (len + argc > ecmascript::base::MAX_SAFE_INTEGER) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
    }

    // 8. Repeat, while items is not empty
    //   a. Remove the first element from items and let E be the value of the element.
    //   b. Let setStatus be Set(O, ToString(len), E, true).
    //   c. ReturnIfAbrupt(setStatus).
    //   d. Let len be len+1.
    double k = 0;
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    while (k < argc) {
        key.Update(JSTaggedValue(len));
        JSHandle<JSTaggedValue> k_value = builtins_common::GetCallArg(argv, k);
        JSArray::FastSetPropertyByValue(thread, this_obj_val, key, k_value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        k++;
        len++;
    }

    // 9. Let setStatus be Set(O, "length", len, true).
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    key.Update(JSTaggedValue(len));
    JSTaggedValue::SetProperty(thread, this_obj_val, length_key, key, true);
    // 10. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 11. Return len.
    return builtins_common::GetTaggedDouble(len);
}

// 22.1.3.18 Array.prototype.reduce ( callbackfn [ , initialValue ] )
JSTaggedValue builtins::array::proto::Reduce(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Reduce);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    uint32_t argc = argv->GetArgsNumber();
    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If len is 0 and initialValue is not present, throw a TypeError exception.
    if (len == 0 && argc < 2) {  // 2:2 means the number of parameters
        THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
    }

    // 7. Let k be 0.
    // 8. If initialValue is present, then
    //   a. Set accumulator to initialValue.
    // 9. Else initialValue is not present,
    //   a. Let kPresent be false.
    //   b. Repeat, while kPresent is false and k < len
    //     i. Let Pk be ToString(k).
    //     ii. Let kPresent be HasProperty(O, Pk).
    //     iii. ReturnIfAbrupt(kPresent).
    //     iv. If kPresent is true, then
    //       1. Let accumulator be Get(O, Pk).
    //       2. ReturnIfAbrupt(accumulator).
    //     v. Increase k by 1.
    //   c. If kPresent is false, throw a TypeError exception.
    uint32_t k = 0;
    JSMutableHandle<JSTaggedValue> accumulator(thread, JSTaggedValue::Undefined());
    if (argc == 2) {  // 2:2 means the number of parameters
        accumulator.Update(builtins_common::GetCallArg(argv, 1).GetTaggedValue());
    } else {
        bool k_present = false;
        while (!k_present && k < len) {
            k_present = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, k));
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (k_present) {
                accumulator.Update(JSArray::FastGetPropertyByValue(thread, this_obj_val, k).GetTaggedValue());
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            k++;
        }
        if (!k_present) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "accumulator can't be initialized.", JSTaggedValue::Exception());
        }
    }

    // 10. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let accumulator be Call(callbackfn, undefined, «accumulator, kValue, k, O»).
    //     iv. ReturnIfAbrupt(accumulator).
    //   e. Increase k by 1.
    JSTaggedValue call_result = JSTaggedValue::Undefined();
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());

    while (k < len) {
        bool exists = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, k));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            key.Update(JSTaggedValue(k));
            JSHandle<JSTaggedValue> this_arg_handle = global_const->GetHandledUndefined();
            auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 4);
            info->SetCallArgs(accumulator, k_value, key, this_obj_val);
            call_result = JSFunction::Call(info.Get());  // 4: four args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            accumulator.Update(call_result);
        }
        k++;
    }

    // 11. Return accumulator.
    return accumulator.GetTaggedValue();
}

// 22.1.3.19 Array.prototype.reduceRight ( callbackfn [ , initialValue ] )
JSTaggedValue builtins::array::proto::ReduceRight(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, ReduceRight);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    uint32_t argc = argv->GetArgsNumber();

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If len is 0 and initialValue is not present, throw a TypeError exception.
    if (len == 0 && argc < 2) {  // 2:2 means the number of parameters
        THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
    }

    // 7. Let k be len-1.
    double k = len - 1;
    // 8. If initialValue is present, then
    //   a. Set accumulator to initialValue.
    // 9. Else initialValue is not present,
    //   a. Let kPresent be false.
    //   b. Repeat, while kPresent is false and k ≥ 0
    //     i. Let Pk be ToString(k).
    //     ii. Let kPresent be HasProperty(O, Pk).
    //     iii. ReturnIfAbrupt(kPresent).
    //     iv. If kPresent is true, then
    //       1. Let accumulator be Get(O, Pk).
    //       2. ReturnIfAbrupt(accumulator).
    //     v. Decrease k by 1.
    //   c. If kPresent is false, throw a TypeError exception.
    JSMutableHandle<JSTaggedValue> accumulator(thread, JSTaggedValue::Undefined());
    if (argc == 2) {  // 2:2 means the number of parameters
        accumulator.Update(builtins_common::GetCallArg(argv, 1).GetTaggedValue());
    } else {
        bool k_present = false;
        while (!k_present && k >= 0) {
            k_present = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, k));
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (k_present) {
                accumulator.Update(JSArray::FastGetPropertyByValue(thread, this_obj_val, k).GetTaggedValue());
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            k--;
        }
        if (!k_present) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "accumulator can't be initialized.", JSTaggedValue::Exception());
        }
    }

    // 10. Repeat, while k ≥ 0
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let accumulator be Call(callbackfn, undefined, «accumulator, kValue, k, O»).
    //     iv. ReturnIfAbrupt(accumulator).
    //   e. Decrease k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    JSTaggedValue call_result = JSTaggedValue::Undefined();

    while (k >= 0) {
        key.Update(JSTaggedValue(k));
        bool exists = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, key));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSHandle<JSTaggedValue> this_arg_handle = global_const->GetHandledUndefined();
            auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 4);
            info->SetCallArgs(accumulator, k_value, key, this_obj_val);
            call_result = JSFunction::Call(info.Get());  // 4: four args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            accumulator.Update(call_result);
        }
        k--;
    }

    // 11. Return accumulator.
    return accumulator.GetTaggedValue();
}

// 22.1.3.20 Array.prototype.reverse ( )
JSTaggedValue builtins::array::proto::Reverse(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Reverse);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Let middle be floor(len/2).
    double middle = std::floor(len / 2);

    // 6. Let lower be 0.
    double lower = 0;

    // 7. Repeat, while lower != middle
    //   a. Let upper be len-lower-1.
    //   b. Let upperP be ToString(upper).
    //   c. Let lowerP be ToString(lower).
    //   d. Let lowerExists be HasProperty(O, lowerP).
    //   e. ReturnIfAbrupt(lowerExists).
    //   f. If lowerExists is true, then
    //     i. Let lowerValue be Get(O, lowerP).
    //     ii. ReturnIfAbrupt(lowerValue).
    //   g. Let upperExists be HasProperty(O, upperP).
    //   h. ReturnIfAbrupt(upperExists).
    //     i. If upperExists is true, then
    //     i. Let upperValue be Get(O, upperP).
    //     ii. ReturnIfAbrupt(upperValue).
    //   j. If lowerExists is true and upperExists is true, then
    //     i. Let setStatus be Set(O, lowerP, upperValue, true).
    //     ii. ReturnIfAbrupt(setStatus).
    //     iii. Let setStatus be Set(O, upperP, lowerValue, true).
    //     iv. ReturnIfAbrupt(setStatus).
    //   k. Else if lowerExists is false and upperExists is true, then
    //     i. Let setStatus be Set(O, lowerP, upperValue, true).
    //     ii. ReturnIfAbrupt(setStatus).
    //     iii. Let deleteStatus be DeletePropertyOrThrow (O, upperP).
    //     iv. ReturnIfAbrupt(deleteStatus).
    //   l. Else if lowerExists is true and upperExists is false, then
    //     i. Let deleteStatus be DeletePropertyOrThrow (O, lowerP).
    //     ii. ReturnIfAbrupt(deleteStatus).
    //     iii. Let setStatus be Set(O, upperP, lowerValue, true).
    //     iv. ReturnIfAbrupt(setStatus).
    //   m. Else both lowerExists and upperExists are false,
    //     i. No action is required.
    //   n. Increase lower by 1.
    JSMutableHandle<JSTaggedValue> lower_p(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> upper_p(thread, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> lower_value_handle(thread, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> upper_value_handle(thread, JSTaggedValue::Undefined());
    while (lower != middle) {
        double upper = len - lower - 1;
        lower_p.Update(JSTaggedValue(lower));
        upper_p.Update(JSTaggedValue(upper));
        bool lower_exists = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, lower_p));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (lower_exists) {
            lower_value_handle = JSArray::FastGetPropertyByValue(thread, this_obj_val, lower_p);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        bool upper_exists = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, upper_p));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (upper_exists) {
            upper_value_handle = JSArray::FastGetPropertyByValue(thread, this_obj_val, upper_p);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        if (lower_exists && upper_exists) {
            JSArray::FastSetPropertyByValue(thread, this_obj_val, lower_p, upper_value_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSArray::FastSetPropertyByValue(thread, this_obj_val, upper_p, lower_value_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else if (upper_exists) {
            JSArray::FastSetPropertyByValue(thread, this_obj_val, lower_p, upper_value_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, upper_p);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else if (lower_exists) {
            JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, lower_p);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSArray::FastSetPropertyByValue(thread, this_obj_val, upper_p, lower_value_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else {
        }
        lower++;
    }

    // 8. Return O .
    return this_obj_handle.GetTaggedValue();
}

// 22.1.3.21 Array.prototype.shift ( )
JSTaggedValue builtins::array::proto::Shift(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Shift);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    if (this_handle->IsStableJSArray(thread)) {
        return JSStableArray::Shift(JSHandle<JSArray>::Cast(this_handle), argv);
    }
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. If len is zero, then
    //   a. Let setStatus be Set(O, "length", 0, true).
    //   b. ReturnIfAbrupt(setStatus).
    //   c. Return undefined.
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    if (len == 0) {
        JSHandle<JSTaggedValue> zero_len_handle(thread, JSTaggedValue(len));
        JSTaggedValue::SetProperty(thread, this_obj_val, length_key, zero_len_handle, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        return JSTaggedValue::Undefined();
    }

    // 6. Let first be Get(O, "0").
    JSHandle<JSTaggedValue> first_key(thread, JSTaggedValue(0));
    JSHandle<JSTaggedValue> first_value = JSTaggedValue::GetProperty(thread, this_obj_val, first_key).GetValue();
    // 7. ReturnIfAbrupt(first).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 8. Let k be 1.
    // 9. Repeat, while k < len
    //   a. Let from be ToString(k).
    //   b. Let to be ToString(k–1).
    //   c. Let fromPresent be HasProperty(O, from).
    //   d. ReturnIfAbrupt(fromPresent).
    //   e. If fromPresent is true, then
    //     i. Let fromVal be Get(O, from).
    //     ii. ReturnIfAbrupt(fromVal).
    //     iii. Let setStatus be Set(O, to, fromVal, true).
    //     iv. ReturnIfAbrupt(setStatus).
    //   f. Else fromPresent is false,
    //     i. Let deleteStatus be DeletePropertyOrThrow(O, to).
    //     ii. ReturnIfAbrupt(deleteStatus).
    //   g. Increase k by 1.
    JSMutableHandle<JSTaggedValue> to_key(thread, JSTaggedValue::Undefined());
    double k = 1;
    while (k < len) {
        bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> from_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSArray::FastSetPropertyByValue(thread, this_obj_val, k - 1, from_value);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else {
            to_key.Update(JSTaggedValue(k - 1));
            JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, to_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        k++;
    }
    // 10. Let deleteStatus be DeletePropertyOrThrow(O, ToString(len–1)).
    JSHandle<JSTaggedValue> delete_key(thread, JSTaggedValue(len - 1));
    JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, delete_key);
    // 11. ReturnIfAbrupt(deleteStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 12. Let setStatus be Set(O, "length", len–1, true).
    JSHandle<JSTaggedValue> new_len_handle(thread, JSTaggedValue(len - 1));
    JSTaggedValue::SetProperty(thread, this_obj_val, length_key, new_len_handle, true);
    // 13. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 14. Return first.
    return first_value.GetTaggedValue();
}

// 22.1.3.22 Array.prototype.slice (start, end)
JSTaggedValue builtins::array::proto::Slice(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Slice);
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Let relativeStart be ToInteger(start).
    JSHandle<JSTaggedValue> msg0 = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber arg_start_temp = JSTaggedValue::ToInteger(thread, msg0);
    // 6. ReturnIfAbrupt(relativeStart).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double arg_start = arg_start_temp.GetNumber();

    double k;
    // 7. If relativeStart < 0, let k be max((len + relativeStart),0); else let k be min(relativeStart, len).
    if (arg_start < 0) {
        k = arg_start + len > 0 ? arg_start + len : 0;
    } else {
        k = arg_start < len ? arg_start : len;
    }
    // 8. If end is undefined, let relativeEnd be len; else let relativeEnd be ToInteger(end).
    // 9. ReturnIfAbrupt(relativeEnd).
    // 10. If relativeEnd < 0, let final be max((len + relativeEnd),0); else let final be min(relativeEnd, len).
    JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 1);
    double arg_end = len;
    if (!msg1->IsUndefined()) {
        JSTaggedNumber arg_end_temp = JSTaggedValue::ToInteger(thread, msg1);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        arg_end = arg_end_temp.GetNumber();
    }
    double final;
    if (arg_end < 0) {
        final = arg_end + len > 0 ? arg_end + len : 0;
    } else {
        final = arg_end < len ? arg_end : len;
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 11. Let count be max(final – k, 0).
    double count = (final - k) > 0 ? (final - k) : 0;

    // 12. Let A be ArraySpeciesCreate(O, count).
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(count));
    // 13. ReturnIfAbrupt(A).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (count == 0) {
        return new_array;
    }
    JSHandle<JSObject> new_array_handle(thread, new_array);

    if (this_handle->IsStableJSArray(thread) && new_array.IsStableJSArray(thread)) {
        TaggedArray *dest_elements = *JSObject::GrowElementsCapacity(thread, new_array_handle, count);
        TaggedArray *src_elements = TaggedArray::Cast(this_obj_handle->GetElements().GetTaggedObject());

        for (uint32_t idx = 0; idx < count; idx++) {
            dest_elements->Set(thread, idx, src_elements->Get(k + idx));
        }

        JSHandle<JSArray>::Cast(new_array_handle)->SetArrayLength(thread, count);
        return new_array_handle.GetTaggedValue();
    }

    // 14. Let n be 0.
    // 15. Repeat, while k < final
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let status be CreateDataPropertyOrThrow(A, ToString(n), kValue ).
    //     iv. ReturnIfAbrupt(status).
    //   e. Increase k by 1.
    //   f. Increase n by 1.
    double n = 0;
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> n_key(thread, JSTaggedValue::Undefined());
    while (k < final) {
        key.Update(JSTaggedValue(k));
        bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, key);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            n_key.Update(JSTaggedValue(n));
            JSHandle<JSTaggedValue> k_value_handle = JSArray::FastGetPropertyByValue(thread, this_obj_val, key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, n_key, k_value_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        k++;
        n++;
    }

    // 16. Let setStatus be Set(A, "length", n, true).
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> new_len_handle(thread, JSTaggedValue(n));
    JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_array_handle), length_key, new_len_handle,
                               true);
    // 17. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 18. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.3.23 Array.prototype.some ( callbackfn [ , thisArg ] )
JSTaggedValue builtins::array::proto::Some(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Some);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let testResult be ToBoolean(Call(callbackfn, T, «kValue, k, and O»)).
    //     iv. ReturnIfAbrupt(testResult).
    //     v. If testResult is true, return true.
    //   e. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());

    double k = 0;
    while (k < len) {
        bool exists = (this_handle->IsTypedArray() || JSTaggedValue::HasProperty(thread, this_obj_val, k));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            key.Update(JSTaggedValue(k));
            JSHandle<JSTaggedValue> k_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
            info->SetCallArgs(k_value, key, this_obj_val);
            JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
            bool bool_result = call_result.ToBoolean();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (bool_result) {
                return builtins_common::GetTaggedBoolean(true);
            }
        }
        k++;
    }

    // 9. Return false.
    return builtins_common::GetTaggedBoolean(false);
}

// 22.1.3.24 Array.prototype.sort (comparefn)
JSTaggedValue builtins::array::proto::Sort(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Sort);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let obj be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsUndefined() && !callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Callable is false", JSTaggedValue::Exception());
    }

    // 2. Let len be ToLength(Get(obj, "length")).
    double len = ArrayHelper::GetArrayLength(thread, JSHandle<JSTaggedValue>(this_obj_handle));
    // 3. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSMutableHandle<JSTaggedValue> present_value(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> middle_value(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> previous_value(thread, JSTaggedValue::Undefined());
    for (int i = 1; i < len; i++) {
        int begin_index = 0;
        int end_index = i;
        present_value.Update(
            FastRuntimeStub::FastGetPropertyByIndex<true>(thread, this_obj_handle.GetTaggedValue(), i));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        while (begin_index < end_index) {
            int middle_index = (begin_index + end_index) / 2;  // 2 : half
            middle_value.Update(
                FastRuntimeStub::FastGetPropertyByIndex<true>(thread, this_obj_handle.GetTaggedValue(), middle_index));
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            int32_t compare_result = ArrayHelper::SortCompare(thread, callback_fn_handle, middle_value, present_value);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (compare_result > 0) {
                end_index = middle_index;
            } else {
                begin_index = middle_index + 1;
            }
        }

        if (end_index >= 0 && end_index < i) {
            for (int j = i; j > end_index; j--) {
                previous_value.Update(
                    FastRuntimeStub::FastGetPropertyByIndex<true>(thread, this_obj_handle.GetTaggedValue(), j - 1));
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                FastRuntimeStub::FastSetPropertyByIndex(thread, this_obj_handle.GetTaggedValue(), j,
                                                        previous_value.GetTaggedValue());
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            FastRuntimeStub::FastSetPropertyByIndex(thread, this_obj_handle.GetTaggedValue(), end_index,
                                                    present_value.GetTaggedValue());
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
    }

    return this_obj_handle.GetTaggedValue();
}

// 22.1.3.25 Array.prototype.splice (start, deleteCount , ...items )
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue builtins::array::proto::Splice(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Splice);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    uint32_t argc = argv->GetArgsNumber();
    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);
    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. Let relativeStart be ToInteger(start).
    double start = 0;
    double insert_count = 0;
    double actual_delete_count = 0;
    double end = len;
    double arg_start = 0;
    if (argc > 0) {
        JSHandle<JSTaggedValue> msg0 = builtins_common::GetCallArg(argv, 0);
        JSTaggedNumber arg_start_temp = JSTaggedValue::ToInteger(thread, msg0);
        // 6. ReturnIfAbrupt(relativeStart).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        arg_start = arg_start_temp.GetNumber();
        // 7. If relativeStart < 0, let actualStart be max((len + relativeStart),0); else let actualStart be
        // min(relativeStart, len).
        if (arg_start < 0) {
            start = arg_start + len > 0 ? arg_start + len : 0;
        } else {
            start = arg_start < end ? arg_start : end;
        }
        actual_delete_count = len - start;
    }
    // 8. If the number of actual arguments is 0, then
    //   a. Let insertCount be 0.
    //   b. Let actualDeleteCount be 0.
    // 9. Else if the number of actual arguments is 1, then
    //   a. Let insertCount be 0.
    //   b. Let actualDeleteCount be len – actualStart.
    // 10. Else,
    //   a. Let insertCount be the number of actual arguments minus 2.
    //   b. Let dc be ToInteger(deleteCount).
    //   c. ReturnIfAbrupt(dc).
    //   d. Let actualDeleteCount be min(max(dc,0), len – actualStart).
    if (argc > 1) {
        insert_count = argc - 2;  // 2:2 means there are two arguments before the insert items.
        JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 1);
        JSTaggedNumber arg_delete_count = JSTaggedValue::ToInteger(thread, msg1);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        double delete_count = arg_delete_count.GetNumber();
        delete_count = delete_count > 0 ? delete_count : 0;
        actual_delete_count = delete_count < (len - start) ? delete_count : len - start;
    }
    // 11. If len+insertCount−actualDeleteCount > 253-1, throw a TypeError exception.
    if (len + insert_count - actual_delete_count > ecmascript::base::MAX_SAFE_INTEGER) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
    }

    if (this_handle->IsStableJSArray(thread)) {
        return JSStableArray::Splice(JSHandle<JSArray>::Cast(this_handle), argv, start, insert_count,
                                     actual_delete_count);
    }
    // 12. Let A be ArraySpeciesCreate(O, actualDeleteCount).
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(actual_delete_count));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSObject> new_array_handle(thread, new_array);
    // 14. Let k be 0.
    // 15. Repeat, while k < actualDeleteCount
    //   a. Let from be ToString(actualStart+k).
    //   b. Let fromPresent be HasProperty(O, from).
    //   d. If fromPresent is true, then
    //     i. Let fromValue be Get(O, from).
    //     iii. Let status be CreateDataPropertyOrThrow(A, ToString(k), fromValue).
    //   e. Increment k by 1.
    JSMutableHandle<JSTaggedValue> from_key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> to_key(thread, JSTaggedValue::Undefined());
    double k = 0;
    while (k < actual_delete_count) {
        double from = start + k;
        from_key.Update(JSTaggedValue(from));
        bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, from_key);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (exists) {
            JSHandle<JSTaggedValue> from_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, from_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            to_key.Update(JSTaggedValue(k));
            if (new_array_handle->IsJSProxy()) {
                to_key.Update(JSTaggedValue::ToString(thread, to_key).GetTaggedValue());
            }
            JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, to_key, from_value);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        k++;
    }
    // 16. Let setStatus be Set(A, "length", actualDeleteCount, true).
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> delete_count_handle(thread, JSTaggedValue(actual_delete_count));
    JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_array_handle), length_key, delete_count_handle,
                               true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 19. Let itemCount be the number of elements in items.
    // 20. If itemCount < actualDeleteCount, then
    //   a. Let k be actualStart.
    //   b. Repeat, while k < (len – actualDeleteCount)
    //     i. Let from be ToString(k+actualDeleteCount).
    //     ii. Let to be ToString(k+itemCount).
    //     iii. Let fromPresent be HasProperty(O, from).
    //     v. If fromPresent is true, then
    //       1. Let fromValue be Get(O, from).
    //       3. Let setStatus be Set(O, to, fromValue, true).
    //     vi. Else fromPresent is false,
    //       1. Let deleteStatus be DeletePropertyOrThrow(O, to).
    //     vii. Increase k by 1.
    //   c. Let k be len.
    //   d. Repeat, while k > (len – actualDeleteCount + itemCount)
    //     i. Let deleteStatus be DeletePropertyOrThrow(O, ToString(k–1)).
    //     iii. Decrease k by 1.
    if (insert_count < actual_delete_count) {
        k = start;
        while (k < len - actual_delete_count) {
            from_key.Update(JSTaggedValue(k + actual_delete_count));
            to_key.Update(JSTaggedValue(k + insert_count));
            bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, from_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (exists) {
                JSHandle<JSTaggedValue> from_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, from_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                JSArray::FastSetPropertyByValue(thread, this_obj_val, to_key, from_value);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            } else {
                JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, to_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            k++;
        }
        k = len;
        JSMutableHandle<JSTaggedValue> delete_key(thread, JSTaggedValue::Undefined());
        while (k > len - actual_delete_count + insert_count) {
            delete_key.Update(JSTaggedValue(k - 1));
            JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, delete_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            k--;
        }
    } else if (insert_count > actual_delete_count) {
        // 21. Else if itemCount > actualDeleteCount, then
        //   a. Let k be (len – actualDeleteCount).
        //   b. Repeat, while k > actualStart
        //     i. Let from be ToString(k + actualDeleteCount – 1).
        //     ii. Let to be ToString(k + itemCount – 1)
        //     iii. Let fromPresent be HasProperty(O, from).
        //     iv. ReturnIfAbrupt(fromPresent).
        //     v. If fromPresent is true, then
        //       1. Let fromValue be Get(O, from).
        //       2. ReturnIfAbrupt(fromValue).
        //       3. Let setStatus be Set(O, to, fromValue, true).
        //       4. ReturnIfAbrupt(setStatus).
        //     vi. Else fromPresent is false,
        //       1. Let deleteStatus be DeletePropertyOrThrow(O, to).
        //       2. ReturnIfAbrupt(deleteStatus).
        //     vii. Decrease k by 1.
        k = len - actual_delete_count;
        while (k > start) {
            from_key.Update(JSTaggedValue(k + actual_delete_count - 1));
            to_key.Update(JSTaggedValue(k + insert_count - 1));
            bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, from_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (exists) {
                JSHandle<JSTaggedValue> from_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, from_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                JSArray::FastSetPropertyByValue(thread, this_obj_val, to_key, from_value);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            } else {
                JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, to_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            k--;
        }
    }
    // 22. Let k be actualStart.
    k = start;
    // 23. Repeat, while items is not empty
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    for (uint32_t i = 2; i < argc; i++) {
        JSHandle<JSTaggedValue> item_value = builtins_common::GetCallArg(argv, i);
        key.Update(JSTaggedValue(k));
        JSArray::FastSetPropertyByValue(thread, this_obj_val, key, item_value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        k++;
    }
    // 24. Let setStatus be Set(O, "length", len – actualDeleteCount + itemCount, true).
    double new_len = len - actual_delete_count + insert_count;
    JSHandle<JSTaggedValue> new_len_handle(thread, JSTaggedValue(new_len));
    JSTaggedValue::SetProperty(thread, this_obj_val, length_key, new_len_handle, true);
    // 25. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 26. Return A.
    return new_array_handle.GetTaggedValue();
}

// 22.1.3.26 Array.prototype.toLocaleString ( [ reserved1 [ , reserved2 ] ] )
JSTaggedValue builtins::array::proto::ToLocaleString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, ToLocaleString);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Let separator be the String value for the list-separator String appropriate for the host environment’s
    // current locale (this is derived in an implementation-defined way).
    JSHandle<JSTaggedValue> sep_handle;
    if ((builtins_common::GetCallArg(argv, 0)->IsUndefined())) {
        sep_handle = JSHandle<JSTaggedValue>::Cast(ecma_vm->GetFactory()->NewFromCanBeCompressString(","));
    } else {
        sep_handle = builtins_common::GetCallArg(argv, 0);
    }
    JSHandle<EcmaString> sep_string_handle = JSTaggedValue::ToString(thread, sep_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    PandaString sep_string = ConvertToPandaString(*sep_string_handle);
    // 6. If len is zero, return the empty String.
    if (len == 0) {
        return builtins_common::GetTaggedString(thread, "");
    }

    // Inject locales and options argument into a taggedArray
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);

    PandaString concat_str;
    // 7. Let firstElement be Get(array, "0").
    // 8. ReturnIfAbrupt(firstElement).
    // 9. If firstElement is undefined or null, then
    //   a. Let R be the empty String.
    // 10. Else
    //   a. Let R be ToString(Invoke(firstElement, "toLocaleString")).
    //   b. ReturnIfAbrupt(R).
    // 11. Let k be 1.
    // 12. Repeat, while k < len
    //   a. Let S be a String value produced by concatenating R and separator.
    //   b. Let nextElement be Get(array, ToString(k)).
    //   c. ReturnIfAbrupt(nextElement).
    //   d. If nextElement is undefined or null, then
    //     i. Let R be the empty String.
    //   e. Else
    //     i. Let R be ToString(Invoke(nextElement, "toLocaleString")).
    //     ii. ReturnIfAbrupt(R).
    //   f. Let R be a String value produced by concatenating S and R.
    //   g. Increase k by 1.
    auto global_const = thread->GlobalConstants();

    for (int32_t k = 0; k < len; k++) {
        JSTaggedValue next = global_const->GetEmptyString();
        JSHandle<JSTaggedValue> next_element = JSArray::FastGetPropertyByValue(thread, this_obj_val, k);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (!next_element->IsUndefined() && !next_element->IsNull()) {
            JSHandle<JSTaggedValue> next_value_handle = next_element;
            JSHandle<JSTaggedValue> key = global_const->GetHandledToLocaleStringString();
            auto info = NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), next_value_handle,
                                           JSTaggedValue::Undefined(), 2);  // 2: two args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            info->SetCallArgs(locales, options);
            JSTaggedValue call_result = JSFunction::Invoke(info.Get(), key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            next = call_result;
        }
        JSHandle<JSTaggedValue> next_handle(thread, next);
        JSHandle<EcmaString> next_string_handle = JSTaggedValue::ToString(thread, next_handle);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        PandaString next_string = ConvertToPandaString(*next_string_handle);
        if (k > 0) {
            concat_str += sep_string;
            concat_str += next_string;
            continue;
        }
        concat_str += next_string;
    }

    // 13. Return R.
    return factory->NewFromString(concat_str).GetTaggedValue();
}

// 22.1.3.27 Array.prototype.toString ( )
JSTaggedValue builtins::array::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, ToString);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. Let array be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(array).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let func be Get(array, "join").
    JSHandle<JSTaggedValue> join_key(factory->NewFromCanBeCompressString("join"));
    JSHandle<JSTaggedValue> callback_fn_handle = JSTaggedValue::GetProperty(thread, this_obj_val, join_key).GetValue();

    // 4. ReturnIfAbrupt(func).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(func) is false, let func be the intrinsic function %ObjProto_toString% (19.1.3.6).
    if (!callback_fn_handle->IsCallable()) {
        JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
        JSHandle<JSTaggedValue> object_prototype = env->GetObjectFunctionPrototype();
        JSHandle<JSTaggedValue> to_string_key = thread->GlobalConstants()->GetHandledToStringString();
        callback_fn_handle = JSTaggedValue::GetProperty(thread, object_prototype, to_string_key).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    auto info =
        NewRuntimeCallInfo(thread, callback_fn_handle, this_obj_val, JSTaggedValue::Undefined(), argv->GetArgsNumber());
    if (argv->GetArgsNumber() > 0) {
        info->SetCallArg(argv->GetArgsNumber(),
                         reinterpret_cast<JSTaggedType *>(argv->GetArgAddress(js_method_args::NUM_MANDATORY_ARGS)));
    }
    return JSFunction::Call(info.Get());
}

// 22.1.3.28 Array.prototype.unshift ( ...items )
JSTaggedValue builtins::array::proto::Unshift(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Unshift);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 5. Let argCount be the number of actual arguments.
    uint32_t argc = argv->GetArgsNumber();

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    double len = ArrayHelper::GetArrayLength(thread, this_obj_val);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. If argCount > 0, then
    //   a. If len+ argCount > 253-1, throw a TypeError exception.
    //   b. Let k be len.
    //   c. Repeat, while k > 0,
    //     i. Let from be ToString(k–1).
    //     ii. Let to be ToString(k+argCount –1).
    //     iii. Let fromPresent be HasProperty(O, from).
    //     iv. ReturnIfAbrupt(fromPresent).
    //     v. If fromPresent is true, then
    //       1. Let fromValue be Get(O, from).
    //       2. ReturnIfAbrupt(fromValue).
    //       3. Let setStatus be Set(O, to, fromValue, true).
    //       4. ReturnIfAbrupt(setStatus).
    //     vi. Else fromPresent is false,
    //       1. Let deleteStatus be DeletePropertyOrThrow(O, to).
    //       2. ReturnIfAbrupt(deleteStatus).
    //     vii. Decrease k by 1.
    if (argc > 0) {
        if (len + argc > ecmascript::base::MAX_SAFE_INTEGER) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "out of range.", JSTaggedValue::Exception());
        }
        JSMutableHandle<JSTaggedValue> from_key(thread, JSTaggedValue::Undefined());
        JSMutableHandle<JSTaggedValue> to_key(thread, JSTaggedValue::Undefined());
        double k = len;
        while (k > 0) {
            from_key.Update(JSTaggedValue(k - 1));
            to_key.Update(JSTaggedValue(k + argc - 1));
            bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, from_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (exists) {
                JSHandle<JSTaggedValue> from_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, from_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                JSArray::FastSetPropertyByValue(thread, this_obj_val, to_key, from_value);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            } else {
                JSTaggedValue::DeletePropertyOrThrow(thread, this_obj_val, to_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            k--;
        }
        //   d. Let j be 0.
        //   e. Let items be a List whose elements are, in left to right order, the arguments that were passed to this
        //   function invocation.
        //   f. Repeat, while items is not empty
        //     i. Remove the first element from items and let E be the value of that element.
        //     ii. Let setStatus be Set(O, ToString(j), E, true).
        //     iii. ReturnIfAbrupt(setStatus).
        //     iv. Increase j by 1.
        double j = 0;
        while (j < argc) {
            to_key.Update(JSTaggedValue(j));
            JSHandle<JSTaggedValue> to_value = builtins_common::GetCallArg(argv, j);
            JSArray::FastSetPropertyByValue(thread, this_obj_val, to_key, to_value);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            j++;
        }
    }

    // 7. Let setStatus be Set(O, "length", len+argCount, true).
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    double new_len = len + argc;
    JSHandle<JSTaggedValue> new_len_handle(thread, JSTaggedValue(new_len));
    JSTaggedValue::SetProperty(thread, this_obj_val, length_key, new_len_handle, true);
    // 8. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 9. Return len+argCount.
    return builtins_common::GetTaggedDouble(new_len);
}

// 22.1.3.29 Array.prototype.values ( )
JSTaggedValue builtins::array::proto::Values(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayPrototype, Values);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let O be ToObject(this value).
    // 2. ReturnIfAbrupt(O).
    JSHandle<JSObject> self = JSTaggedValue::ToObject(thread, builtins_common::GetThis(argv));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return CreateArrayIterator(O, "value").
    JSHandle<JSArrayIterator> iter(factory->NewJSArrayIterator(self, IterationKind::VALUE));
    return iter.GetTaggedValue();
}
// 22.1.3.31 Array.prototype [ @@unscopables ]
JSTaggedValue builtins::array::proto::GetUnscopables(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> null_handle(thread, JSTaggedValue::Null());
    JSHandle<JSObject> unscopable_list = factory->OrdinaryNewJSObjectCreate(null_handle);

    JSHandle<JSTaggedValue> true_val(thread, JSTaggedValue::True());
    JSHandle<JSTaggedValue> copy_with_key(factory->NewFromCanBeCompressString("copyWithin"));
    JSObject::CreateDataProperty(thread, unscopable_list, copy_with_key, true_val);

    JSHandle<JSTaggedValue> entries_key(factory->NewFromCanBeCompressString("entries"));
    JSObject::CreateDataProperty(thread, unscopable_list, entries_key, true_val);

    JSHandle<JSTaggedValue> fill_key(factory->NewFromCanBeCompressString("fill"));
    JSObject::CreateDataProperty(thread, unscopable_list, fill_key, true_val);

    JSHandle<JSTaggedValue> find_key(factory->NewFromCanBeCompressString("find"));
    JSObject::CreateDataProperty(thread, unscopable_list, find_key, true_val);

    JSHandle<JSTaggedValue> find_index_key(factory->NewFromCanBeCompressString("findIndex"));
    JSObject::CreateDataProperty(thread, unscopable_list, find_index_key, true_val);

    JSHandle<JSTaggedValue> flat_key(factory->NewFromCanBeCompressString("flat"));
    JSObject::CreateDataProperty(thread, unscopable_list, flat_key, true_val);

    JSHandle<JSTaggedValue> flat_map_key(factory->NewFromCanBeCompressString("flatMap"));
    JSObject::CreateDataProperty(thread, unscopable_list, flat_map_key, true_val);

    JSHandle<JSTaggedValue> includes_key(factory->NewFromCanBeCompressString("includes"));
    JSObject::CreateDataProperty(thread, unscopable_list, includes_key, true_val);

    JSHandle<JSTaggedValue> keys_key(factory->NewFromCanBeCompressString("keys"));
    JSObject::CreateDataProperty(thread, unscopable_list, keys_key, true_val);

    JSHandle<JSTaggedValue> values_key(factory->NewFromCanBeCompressString("values"));
    JSObject::CreateDataProperty(thread, unscopable_list, values_key, true_val);

    return unscopable_list.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
