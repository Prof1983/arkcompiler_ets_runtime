/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_collator.h"
#include "plugins/ecmascript/runtime/js_intl.h"

namespace panda::ecmascript::builtins {
constexpr uint32_t FUNCTION_LENGTH_TWO = 2;

// 11.1.2 Intl.Collator ( [ locales [ , options ] ] )
JSTaggedValue collator::CollatorConstructor(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. If NewTarget is undefined, let new_target be the active function object, else let new_target be NewTarget.
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        new_target = constructor;
    }
    // 2. Let internalSlotsList be « [[InitializedCollator]], [[Locale]], [[Usage]], [[Sensitivity]],
    //    [[IgnorePunctuation]], [[Collation]], [[BoundCompare]] ».
    // 3. If %Collator%.[[RelevantExtensionKeys]] contains "kn", then
    //    a. Append [[Numeric]] as the last element of internalSlotsList.
    // 4. If %Collator%.[[RelevantExtensionKeys]] contains "kf", then
    //    a. Append [[CaseFirst]] as the last element of internalSlotsList.

    // 5. Let collator be ? OrdinaryCreateFromConstructor(new_target, "%CollatorPrototype%", internalSlotsList).
    JSHandle<JSCollator> collator =
        JSHandle<JSCollator>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. Return ? InitializeCollator(collator, locales, options).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSCollator> result = JSCollator::InitializeCollator(thread, collator, locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 11.2.2 Intl.Collator.supportedLocalesOf ( locales [ , options ] )
JSTaggedValue collator::SupportedLocalesOf(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let availableLocales be %Collator%.[[AvailableLocales]].
    JSHandle<TaggedArray> available_locales = JSCollator::GetAvailableLocales(thread);

    // 2. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Return ? SupportedLocales(availableLocales, requestedLocales, options).
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSArray> result = JSLocale::SupportedLocales(thread, available_locales, requested_locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 11.3.3.1 Collator Compare Functions
static JSTaggedValue AnonymousCollator(EcmaRuntimeCallInfo *argv)
{
    // A Collator compare function is an anonymous built-in function that has a [[Collator]] internal slot.
    // When a Collator compare function F is called with arguments x and y, the following steps are taken:
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSIntlBoundFunction> intl_bound_func =
        JSHandle<JSIntlBoundFunction>::Cast(builtins_common::GetConstructor(argv));

    // 1. Let collator be F.[[Collator]].
    JSHandle<JSTaggedValue> collator(thread, intl_bound_func->GetCollator());

    // 2. Assert: Type(collator) is Object and collator has an [[InitializedCollator]] internal slot.
    ASSERT_PRINT(collator->IsJSObject() && collator->IsJSCollator(), "collator is not object or JSCollator");

    // 3. If x is not provided, let x be undefined.
    JSHandle<JSTaggedValue> x = builtins_common::GetCallArg(argv, 0);

    // 4. If y is not provided, let y be undefined.
    JSHandle<JSTaggedValue> y = builtins_common::GetCallArg(argv, 1);

    // 5. Let X be ? ToString(x).
    JSHandle<EcmaString> x_value = JSTaggedValue::ToString(thread, x);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Undefined());
    // 6. Let Y be ? ToString(y).
    JSHandle<EcmaString> y_value = JSTaggedValue::ToString(thread, y);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Undefined());
    // 7. Return CompareStrings(collator, X, Y).
    icu::Collator *icu_collator = (JSHandle<JSCollator>::Cast(collator))->GetIcuCollator();
    return JSCollator::CompareStrings(icu_collator, x_value, y_value);
}

// 11.3.3  get Intl.Collator.prototype.compare
JSTaggedValue collator::proto::GetCompare(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let collator be this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(collator, [[InitializedCollator]]).
    if (!this_value->IsJSCollator()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not collator", JSTaggedValue::Exception());
    }
    // 3. If collator.[[BoundCompare]] is undefined, then
    //    a. Let F be a new built-in function object as defined in 11.3.3.1.
    //    b. Set F.[[Collator]] to collator.
    //    c. Set collator.[[BoundCompare]] to F.
    // 4. Return collator.[[BoundCompare]].
    JSHandle<JSCollator> collator = JSHandle<JSCollator>::Cast(this_value);
    JSHandle<JSTaggedValue> bound_compare(thread, collator->GetBoundCompare());
    if (bound_compare->IsUndefined()) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        JSHandle<JSIntlBoundFunction> intl_bound_func =
            factory->NewJSIntlBoundFunction(reinterpret_cast<void *>(AnonymousCollator), FUNCTION_LENGTH_TWO);
        intl_bound_func->SetCollator(thread, collator);
        collator->SetBoundCompare(thread, intl_bound_func);
    }
    return collator->GetBoundCompare();
}

// 11.3.4 Intl.Collator.prototype.resolvedOptions ()
JSTaggedValue collator::proto::ResolvedOptions(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    if (!this_value->IsJSCollator()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Collator object", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> options = JSCollator::ResolvedOptions(thread, JSHandle<JSCollator>::Cast(this_value));
    return options.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
