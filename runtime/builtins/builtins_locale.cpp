/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_locale.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {
// 10.1.3 Intl.Locale( tag [, options] )
JSTaggedValue locale::LocaleConstructor(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. If NewTarget is undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "new_target is undefined", JSTaggedValue::Exception());
    }

    // 6. Let locale be ? OrdinaryCreateFromConstructor(NewTarget, %LocalePrototype%, internalSlotsList).
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSLocale> locale =
        JSHandle<JSLocale>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 7. If Type(tag) is not String or Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> tag = builtins_common::GetCallArg(argv, 0);
    if (!tag->IsString() && !tag->IsJSObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "tag is not String or Object", JSTaggedValue::Exception());
    }

    // 8. If Type(tag) is Object and tag has an [[InitializedLocale]] internal slot, then
    //    a.Let tag be tag.[[Locale]].
    // 9. Else,
    //    a.Let tag be ? ToString(tag).
    JSHandle<EcmaString> locale_string = factory->GetEmptyString();
    if (!tag->IsJSLocale()) {
        locale_string = JSTaggedValue::ToString(thread, tag);
    } else {
        icu::Locale *icu_locale = (JSHandle<JSLocale>::Cast(tag))->GetIcuLocale();
        locale_string = JSLocale::ToLanguageTag(thread, *icu_locale);
    }
    // 10. If options is undefined, then
    //    a.Let options be ! ObjectCreate(null).
    // 11. Else
    //    a.Let options be ? ToObject(options).
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSObject> options_obj;
    if (options->IsUndefined()) {
        options_obj = factory->OrdinaryNewJSObjectCreate(JSHandle<JSTaggedValue>(thread, JSTaggedValue::Null()));
    } else {
        options_obj = JSTaggedValue::ToObject(thread, options);
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSLocale> result = JSLocale::InitializeLocale(thread, locale, locale_string, options_obj);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 10.3.3 Intl.Locale.prototype.maximize ()
JSTaggedValue locale::proto::Maximize(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Let maximal be the result of the Add Likely Subtags algorithm applied to loc.[[Locale]]. If an error is
    //    signaled, set maximal to loc.[[Locale]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    icu::Locale source(*(locale->GetIcuLocale()));
    UErrorCode status = U_ZERO_ERROR;
    source.addLikelySubtags(status);
    ASSERT(U_SUCCESS(status));
    ASSERT(!source.isBogus());

    // 4. Return ! Construct(%Locale%, maximal).
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> ctor = env->GetLocaleFunction();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), ctor);
    factory->NewJSIntlIcuData(JSHandle<JSLocale>::Cast(obj), source, JSLocale::FreeIcuLocale);
    return obj.GetTaggedValue();
}

// 10.3.4 Intl.Locale.prototype.minimize ()
JSTaggedValue locale::proto::Minimize(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }

    // 3. Let minimal be the result of the Remove Likely Subtags algorithm applied to loc.[[Locale]].
    //    If an error is signaled, set minimal to loc.[[Locale]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    icu::Locale source(*(locale->GetIcuLocale()));
    UErrorCode status = U_ZERO_ERROR;
    source.minimizeSubtags(status);
    ASSERT(U_SUCCESS(status));
    ASSERT(!source.isBogus());

    [[maybe_unused]] auto res = source.toLanguageTag<PandaString>(status);

    // 4. Return ! Construct(%Locale%, minimal).
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    JSHandle<JSTaggedValue> ctor = env->GetLocaleFunction();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), ctor);
    factory->NewJSIntlIcuData(JSHandle<JSLocale>::Cast(obj), source, JSLocale::FreeIcuLocale);
    return obj.GetTaggedValue();
}

// 10.3.5 Intl.Locale.prototype.toString ()
JSTaggedValue locale::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Return loc.[[Locale]].
    JSHandle<EcmaString> result = JSLocale::ToString(thread, JSHandle<JSLocale>::Cast(loc));
    return result.GetTaggedValue();
}

// 10.3.6 get Intl.Locale.prototype.baseName
JSTaggedValue locale::proto::GetBaseName(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Let locale be loc.[[Locale]].
    // 4. Return the substring of locale corresponding to the unicode_language_id production.
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    icu::Locale icu_locale = icu::Locale::createFromName(locale->GetIcuLocale()->getBaseName());
    JSHandle<EcmaString> base_name = JSLocale::ToLanguageTag(thread, icu_locale);
    return base_name.GetTaggedValue();
}

// 10.3.7 get Intl.Locale.prototype.calendar
JSTaggedValue locale::proto::GetCalendar(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Return loc.[[Calendar]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    JSHandle<EcmaString> calendar = JSLocale::NormalizeKeywordValue(thread, locale, "ca");
    return calendar.GetTaggedValue();
}

// 10.3.8 get Intl.Locale.prototype.caseFirst
JSTaggedValue locale::proto::GetCaseFirst(EcmaRuntimeCallInfo *argv)
{
    // This property only exists if %Locale%.[[RelevantExtensionKeys]] contains "kf".
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Return loc.[[CaseFirst]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    JSHandle<EcmaString> case_first = JSLocale::NormalizeKeywordValue(thread, locale, "kf");
    return case_first.GetTaggedValue();
}

// 10.3.9 get Intl.Locale.prototype.collation
JSTaggedValue locale::proto::GetCollation(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Return loc.[[Collation]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    JSHandle<EcmaString> collation = JSLocale::NormalizeKeywordValue(thread, locale, "co");
    return collation.GetTaggedValue();
}

// 10.3.10 get Intl.Locale.prototype.hourCycle
JSTaggedValue locale::proto::GetHourCycle(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Return loc.[[HourCycle]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    JSHandle<EcmaString> hour_cycle = JSLocale::NormalizeKeywordValue(thread, locale, "hc");
    return hour_cycle.GetTaggedValue();
}

// 10.3.11 get Intl.Locale.prototype.numeric
JSTaggedValue locale::proto::GetNumeric(EcmaRuntimeCallInfo *argv)
{
    // This property only exists if %Locale%.[[RelevantExtensionKeys]] contains "kn".
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Return loc.[[Numeric]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    icu::Locale *icu_locale = locale->GetIcuLocale();
    UErrorCode status = U_ZERO_ERROR;
    auto numeric = icu_locale->getUnicodeKeywordValue<PandaString>("kn", status);
    JSTaggedValue result = (numeric == "true") ? JSTaggedValue::True() : JSTaggedValue::False();
    return result;
}

// 10.3.12 get Intl.Locale.prototype.numberingSystem
JSTaggedValue locale::proto::GetNumberingSystem(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Return loc.[[NumberingSystem]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    JSHandle<EcmaString> numbering_system = JSLocale::NormalizeKeywordValue(thread, locale, "nu");
    return numbering_system.GetTaggedValue();
}

// 10.3.13 get Intl.Locale.prototype.language
JSTaggedValue locale::proto::GetLanguage(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Let locale be loc.[[Locale]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    // 4. Assert: locale matches the unicode_locale_id production.
    // 5. Return the substring of locale corresponding to the unicode_language_subtag production of the
    //    unicode_language_id.
    JSHandle<EcmaString> result = factory->NewFromString("undefined");
    PandaString language = locale->GetIcuLocale()->getLanguage();
    if (language.empty()) {
        return result.GetTaggedValue();
    }
    result = factory->NewFromString(language);
    return result.GetTaggedValue();
}

// 10.3.14 get Intl.Locale.prototype.script
JSTaggedValue locale::proto::GetScript(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Let locale be loc.[[Locale]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);

    // 4. Assert: locale matches the unicode_locale_id production.
    // 5. If the unicode_language_id production of locale does not contain the ["-" unicode_script_subtag] sequence,
    //    return undefined.
    // 6. Return the substring of locale corresponding to the unicode_script_subtag production of the
    //    unicode_language_id.
    JSHandle<EcmaString> result(thread, JSTaggedValue::Undefined());
    PandaString script = locale->GetIcuLocale()->getScript();
    if (script.empty()) {
        return result.GetTaggedValue();
    }
    result = factory->NewFromString(script);
    return result.GetTaggedValue();
}

// 10.3.15 get Intl.Locale.prototype.region
JSTaggedValue locale::proto::GetRegion(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    ObjectFactory *factory = ecma_vm->GetFactory();
    // 1. Let loc be the this value.
    JSHandle<JSTaggedValue> loc = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(loc, [[InitializedLocale]]).
    if (!loc->IsJSLocale()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "not locale", JSTaggedValue::Exception());
    }
    // 3. Let locale be loc.[[Locale]].
    JSHandle<JSLocale> locale = JSHandle<JSLocale>::Cast(loc);
    // 4. Assert: locale matches the unicode_locale_id production.
    // 5. If the unicode_language_id production of locale does not contain the ["-" unicode_region_subtag] sequence,
    //    return undefined.
    // 6. Return the substring of locale corresponding to the unicode_region_subtag production of the
    //    unicode_language_id.
    PandaString region = locale->GetIcuLocale()->getCountry();
    if (region.empty()) {
        return global_const->GetUndefined();
    }
    return factory->NewFromString(region).GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
