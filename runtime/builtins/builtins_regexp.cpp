/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/builtins/builtins_regexp.h"
#include <cmath>
#include "assert_gc_scope.h"
#include "plugins/ecmascript/runtime/ecma_string-inl.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_regexp.h"
#include "plugins/ecmascript/runtime/js_regexp_iterator.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/regexp/regexp_parser_cache.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript::builtins {

constexpr uint32_t MIN_REPLACE_STRING_LENGTH = 1000;
constexpr uint32_t MAX_SPLIT_LIMIT = 0xFFFFFFFFU;
static MatchResult Matcher(JSThread *thread, const JSHandle<JSTaggedValue> &regexp, const uint8_t *buffer,
                           size_t length, int32_t last_index, bool is_utf16);
static bool GetFlagsInternal(JSThread *thread, const JSHandle<JSTaggedValue> &obj, uint8_t mask);

// 21.2.5.2.2 Runtime Semantics: RegExpBuiltinExec ( R, S )
static JSTaggedValue RegExpBuiltinExec(JSThread *thread, const JSHandle<JSTaggedValue> &regexp,
                                       const JSHandle<JSTaggedValue> &input_str, bool use_cache);

// 21.2.3.2.1 Runtime Semantics: RegExpAlloc ( new_target )
static JSTaggedValue RegExpAlloc(JSThread *thread, const JSHandle<JSTaggedValue> &new_target);

static uint32_t UpdateExpressionFlags(JSThread *thread, const PandaString &check_str);

// 21.2.3.2.2 Runtime Semantics: RegExpInitialize ( obj, pattern, flags )
static JSTaggedValue RegExpInitialize(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                      const JSHandle<JSTaggedValue> &pattern, const JSHandle<JSTaggedValue> &flags);
// 21.2.3.2.4 Runtime Semantics: EscapeRegExpPattern ( P, F )
static EcmaString *EscapeRegExpPattern(JSThread *thread, const JSHandle<JSTaggedValue> &src,
                                       const JSHandle<JSTaggedValue> &flags);
static JSTaggedValue RegExpReplaceFast(JSThread *thread, JSHandle<JSTaggedValue> &regexp,
                                       JSHandle<EcmaString> input_string, uint32_t input_length);

// 21.2.3.1
JSTaggedValue reg_exp::RegExpConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExp, RegExpConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> new_target_temp = builtins_common::GetNewTarget(argv);
    JSHandle<JSTaggedValue> pattern = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> flags = builtins_common::GetCallArg(argv, 1);
    // 1. Let patternIsRegExp be IsRegExp(pattern).
    bool pattern_is_reg_exp = JSObject::IsRegExp(thread, pattern);
    // 2. ReturnIfAbrupt(patternIsRegExp).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. If NewTarget is not undefined, let new_target be NewTarget.
    JSHandle<JSTaggedValue> new_target;
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    if (!new_target_temp->IsUndefined()) {
        new_target = new_target_temp;
    } else {
        auto ecma_vm = thread->GetEcmaVM();
        JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
        // disable gc
        [[maybe_unused]] DisallowGarbageCollection no_gc;
        // 4.a Let new_target be the active function object.
        new_target = env->GetRegExpFunction();
        JSHandle<JSTaggedValue> constructor_string = global_const->GetHandledConstructorString();
        // 4.b If patternIsRegExp is true and flags is undefined
        if (pattern_is_reg_exp && flags->IsUndefined()) {
            // 4.b.i Let patternConstructor be Get(pattern, "constructor").
            JSTaggedValue pattern_constructor =
                FastRuntimeStub::FastGetPropertyByValue(thread, pattern, constructor_string);
            // 4.b.ii ReturnIfAbrupt(patternConstructor).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // 4.b.iii If SameValue(new_target, patternConstructor) is true, return pattern.
            if (JSTaggedValue::SameValue(new_target.GetTaggedValue(), pattern_constructor)) {
                return pattern.GetTaggedValue();
            }
        }
    }
    // 5. If Type(pattern) is Object and pattern has a [[RegExpMatcher]] internal slot
    bool is_js_reg = false;
    if (pattern->IsECMAObject()) {
        JSHandle<JSObject> pattern_obj = JSHandle<JSObject>::Cast(pattern);
        is_js_reg = pattern_obj->IsJSRegExp();
    }
    JSHandle<JSTaggedValue> pattern_temp;
    JSHandle<JSTaggedValue> flags_temp;
    if (is_js_reg) {
        JSHandle<JSRegExp> pattern_reg(thread, JSRegExp::Cast(pattern->GetTaggedObject()));
        // 5.a Let P be the value of pattern’s [[OriginalSource]] internal slot.
        pattern_temp = JSHandle<JSTaggedValue>(thread, pattern_reg->GetOriginalSource());
        if (flags->IsUndefined()) {
            // 5.b If flags is undefined, let F be the value of pattern’s [[OriginalFlags]] internal slot.
            flags_temp = JSHandle<JSTaggedValue>(thread, pattern_reg->GetOriginalFlags());
        } else {
            // 5.c Else, let F be flags.
            flags_temp = JSHandle<JSTaggedValue>(thread, *JSTaggedValue::ToString(thread, flags));
        }
        // 6. Else if patternIsRegExp is true
    } else if (pattern_is_reg_exp) {
        JSHandle<JSTaggedValue> source_string(global_const->GetHandledSourceString());
        JSHandle<JSTaggedValue> flags_string(global_const->GetHandledFlagsString());
        // disable gc
        [[maybe_unused]] DisallowGarbageCollection no_gc;
        // 6.a Let P be Get(pattern, "source").
        pattern_temp = JSObject::GetProperty(thread, pattern, source_string).GetValue();
        // 6.b ReturnIfAbrupt(P).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // 6.c If flags is undefined
        if (flags->IsUndefined()) {
            // 6.c.i Let F be Get(pattern, "flags").
            flags_temp = JSObject::GetProperty(thread, pattern, flags_string).GetValue();
            // 6.c.ii ReturnIfAbrupt(F).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        } else {
            // 6.d Else, let F be flags.
            flags_temp = JSHandle<JSTaggedValue>(thread, *JSTaggedValue::ToString(thread, flags));
        }
    } else {
        // 7.a Let P be pattern.
        pattern_temp = pattern;
        // 7.b Let F be flags.
        if (flags->IsUndefined()) {
            flags_temp = flags;
        } else {
            flags_temp = JSHandle<JSTaggedValue>(thread, *JSTaggedValue::ToString(thread, flags));
        }
    }
    // 8. Let O be RegExpAlloc(new_target).
    JSHandle<JSTaggedValue> object(thread, RegExpAlloc(thread, new_target));
    // 9. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 10. Return RegExpInitialize(O, P, F).
    JSTaggedValue result = RegExpInitialize(thread, object, pattern_temp, flags_temp);
    return JSTaggedValue(result);
}

static bool ExecCachingAllowed(JSThread *thread, JSHandle<JSTaggedValue> &this_val)
{
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    if (!this_val->IsJSObject()) {
        return false;
    }
    JSObject *this_obj = JSObject::Cast(this_val->GetTaggedObject());
    JSHClass *this_class = this_obj->GetJSHClass();

    JSHandle<JSFunction> regex_fn(thread->GetEcmaVM()->GetGlobalEnv()->GetRegExpFunction());
    if (JSTaggedValue(this_class) != regex_fn->GetProtoOrDynClass()) {
        return false;
    }

    JSTaggedValue regex_proto =
        JSHClass::Cast(global_const->GetHandledJSRegExpClass()->GetTaggedObject())->GetPrototype();
    return this_class->GetPrototype() == regex_proto;
}

// prototype
// 20.2.5.2
JSTaggedValue reg_exp::proto::Exec(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, Exec);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let R be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    // 4. Let S be ToString(string).
    JSHandle<JSTaggedValue> input_str = builtins_common::GetCallArg(argv, 0);
    JSHandle<EcmaString> string_handle = JSTaggedValue::ToString(thread, input_str);
    // 5. ReturnIfAbrupt(S).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> string = JSHandle<JSTaggedValue>::Cast(string_handle);
    // 2. If Type(R) is not Object, throw a TypeError exception.
    if (!this_obj->IsECMAObject()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    // 3. If R does not have a [[RegExpMatcher]] internal slot, throw a TypeError exception.
    if (!this_obj->IsJSRegExp()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this does not have [[RegExpMatcher]]", JSTaggedValue::Exception());
    }

    bool use_cache = ExecCachingAllowed(thread, this_obj);
    JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
    if (cache_table->GetLargeStrCount() == 0 || cache_table->GetConflictCount() == 0) {
        use_cache = false;
    }

    // 6. Return RegExpBuiltinExec(R, S).
    JSTaggedValue result = RegExpBuiltinExec(thread, this_obj, string, use_cache);
    return JSTaggedValue(result);
}

// 20.2.5.13
JSTaggedValue reg_exp::proto::Test(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, Test);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let R be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    JSHandle<JSTaggedValue> input_str = builtins_common::GetCallArg(argv, 0);
    // 3. Let string be ToString(S).
    // 4. ReturnIfAbrupt(string).
    JSHandle<EcmaString> string_handle = JSTaggedValue::ToString(thread, input_str);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> string = JSHandle<JSTaggedValue>::Cast(string_handle);
    // 2. If Type(R) is not Object, throw a TypeError exception.
    if (!this_obj->IsECMAObject()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }

    // 5. Let match be RegExpExec(R, string).
    JSTaggedValue match_result = reg_exp::RegExpExec(thread, this_obj, string, false);
    // 6. ReturnIfAbrupt(match).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 7. If match is not null, return true; else return false.
    return builtins_common::GetTaggedBoolean(!match_result.IsNull());
}

// 20.2.5.14
JSTaggedValue reg_exp::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, ToString);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let R be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    auto ecma_vm = thread->GetEcmaVM();
    // 2. If Type(R) is not Object, throw a TypeError exception.
    if (!this_obj->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    ObjectFactory *factory = ecma_vm->GetFactory();
    const GlobalEnvConstants *global_constants = thread->GlobalConstants();
    JSHandle<JSTaggedValue> source_string(global_constants->GetHandledSourceString());
    JSHandle<JSTaggedValue> flags_string(global_constants->GetHandledFlagsString());
    // 3. Let pattern be ToString(Get(R, "source")).
    JSHandle<JSTaggedValue> get_source(JSObject::GetProperty(thread, this_obj, source_string).GetValue());
    JSHandle<JSTaggedValue> get_flags(JSObject::GetProperty(thread, this_obj, flags_string).GetValue());
    JSHandle<EcmaString> source_str_handle = JSTaggedValue::ToString(thread, get_source);
    // 4. ReturnIfAbrupt(pattern).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. Let flags be ToString(Get(R, "flags")).
    JSHandle<EcmaString> flags_str_handle = JSTaggedValue::ToString(thread, get_flags);
    // 4. ReturnIfAbrupt(flags).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<EcmaString> slash_str = JSHandle<EcmaString>::Cast(global_constants->GetHandledBackslashString());
    // 7. Let result be the String value formed by concatenating "/", pattern, and "/", and flags.
    JSHandle<EcmaString> temp_str = factory->ConcatFromString(slash_str, source_str_handle);
    JSHandle<EcmaString> result_temp = factory->ConcatFromString(temp_str, slash_str);
    return factory->ConcatFromString(result_temp, flags_str_handle).GetTaggedValue();
}

// 20.2.5.3
JSTaggedValue reg_exp::proto::GetFlags(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, GetFlags);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let R be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    // 2. If Type(R) is not Object, throw a TypeError exception.
    if (!this_obj->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    // 3. Let result be the empty String.
    // 4. ~ 19.

    if (JSHandle<JSObject>::Cast(this_obj)->IsJSRegExp()) {
        uint8_t flags_bits =
            static_cast<uint8_t>(JSRegExp::Cast(this_obj->GetTaggedObject())->GetOriginalFlags().GetInt());
        return reg_exp::FlagsBitsToString(thread, flags_bits);
    }

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
    std::array<char, 7U> flags_str;  // six flags + \0
    {
        char *flags_cur = flags_str.data();

        auto add_flag_val = [&](char const *prop_str, char const flag_char) {
            JSHandle<JSTaggedValue> prop(factory->NewFromString(prop_str));
            JSHandle<JSTaggedValue> get_result(JSObject::GetProperty(thread, this_obj, prop).GetValue());
            if (get_result->ToBoolean()) {
                // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                *flags_cur++ = flag_char;
            }
        };
        add_flag_val("global", 'g');
        add_flag_val("ignoreCase", 'i');
        add_flag_val("multiline", 'm');
        add_flag_val("dotAll", 's');
        add_flag_val("unicode", 'u');
        add_flag_val("sticky", 'y');
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        *flags_cur = '\0';
    }

    return factory->NewFromString(flags_str.data()).GetTaggedValue();
}

// 20.2.5.4
JSTaggedValue reg_exp::proto::GetGlobal(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    bool result = GetFlagsInternal(thread, this_obj, RegExpParser::FLAG_GLOBAL);
    return builtins_common::GetTaggedBoolean(result);
}

// 20.2.5.5
JSTaggedValue reg_exp::proto::GetIgnoreCase(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    bool result = GetFlagsInternal(thread, this_obj, RegExpParser::FLAG_IGNORECASE);
    return builtins_common::GetTaggedBoolean(result);
}

// 20.2.5.7
JSTaggedValue reg_exp::proto::GetMultiline(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    bool result = GetFlagsInternal(thread, this_obj, RegExpParser::FLAG_MULTILINE);
    return builtins_common::GetTaggedBoolean(result);
}

JSTaggedValue reg_exp::proto::GetDotAll(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    bool result = GetFlagsInternal(thread, this_obj, RegExpParser::FLAG_DOTALL);
    return builtins_common::GetTaggedBoolean(result);
}

// 20.2.5.10
JSTaggedValue reg_exp::proto::GetSource(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let R be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    // 2. If Type(R) is not Object, throw a TypeError exception.
    // 3. If R does not have an [[OriginalSource]] internal slot, throw a TypeError exception.
    // 4. If R does not have an [[OriginalFlags]] internal slot, throw a TypeError exception.
    if (!this_obj->IsECMAObject()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    if (!this_obj->IsJSRegExp()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this does not have [[OriginalSource]]", JSTaggedValue::Exception());
    }
    // 5. Let src be the value of R’s [[OriginalSource]] internal slot.
    JSHandle<JSRegExp> regexp_obj(thread, JSRegExp::Cast(this_obj->GetTaggedObject()));
    JSHandle<JSTaggedValue> source(thread, regexp_obj->GetOriginalSource());
    // 6. Let flags be the value of R’s [[OriginalFlags]] internal slot.
    uint8_t flags_bits = static_cast<uint8_t>(regexp_obj->GetOriginalFlags().GetInt());
    JSHandle<JSTaggedValue> flags(thread, reg_exp::FlagsBitsToString(thread, flags_bits));
    // 7. Return EscapeRegExpPattern(src, flags).
    return JSTaggedValue(EscapeRegExpPattern(thread, source, flags));
}

// 20.2.5.12
JSTaggedValue reg_exp::proto::GetSticky(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    bool result = GetFlagsInternal(thread, this_obj, RegExpParser::FLAG_STICKY);
    return builtins_common::GetTaggedBoolean(result);
}

// 20.2.5.15
JSTaggedValue reg_exp::proto::GetUnicode(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    bool result = GetFlagsInternal(thread, this_obj, RegExpParser::FLAG_UTF16);
    return builtins_common::GetTaggedBoolean(result);
}

// 21.2.4.2
JSTaggedValue reg_exp::GetSpecies(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return builtins_common::GetThis(argv).GetTaggedValue();
}

// 21.2.5.6
JSTaggedValue reg_exp::proto::Match(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, Match);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let rx be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    // 3. Let S be ToString(string)
    JSHandle<JSTaggedValue> input_string = builtins_common::GetCallArg(argv, 0);
    JSHandle<EcmaString> string_handle = JSTaggedValue::ToString(thread, input_string);
    bool use_cache = true;
    JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
    if (cache_table->GetLargeStrCount() == 0 || cache_table->GetConflictCount() == 0) {
        use_cache = false;
    }
    // 4. ReturnIfAbrupt(string).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> string = JSHandle<JSTaggedValue>::Cast(string_handle);
    if (!this_obj->IsECMAObject()) {
        // 2. If Type(rx) is not Object, throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    use_cache &= ExecCachingAllowed(thread, this_obj);

    // 5. Let global be ToBoolean(Get(rx, "global")).
    JSHandle<JSTaggedValue> global = global_const->GetHandledGlobalString();
    JSTaggedValue global_value = FastRuntimeStub::FastGetPropertyByValue(thread, this_obj, global);
    // 6. ReturnIfAbrupt(global).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSRegExp> regexp_obj(this_obj);
    JSMutableHandle<JSTaggedValue> pattern(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> flags(thread, JSTaggedValue::Undefined());
    if (this_obj->IsJSRegExp()) {
        pattern.Update(regexp_obj->GetOriginalSource());
        flags.Update(regexp_obj->GetOriginalFlags());
    }
    bool is_global = global_value.ToBoolean();
    // 7. If global is false, then
    if (!is_global) {
        // a. Return RegExpExec(rx, S).
        if (use_cache) {
            JSTaggedValue cache_result = cache_table->FindCachedResult(thread, pattern, flags, input_string,
                                                                       RegExpExecResultCache::EXEC_TYPE, this_obj);
            if (cache_result != JSTaggedValue::Undefined()) {
                return cache_result;
            }
        }
        JSTaggedValue result = reg_exp::RegExpExec(thread, this_obj, string, use_cache);
        return JSTaggedValue(result);
    }

    if (use_cache) {
        JSTaggedValue cache_result = cache_table->FindCachedResult(thread, pattern, flags, input_string,
                                                                   RegExpExecResultCache::MATCH_TYPE, this_obj);
        if (cache_result != JSTaggedValue::Undefined()) {
            return cache_result;
        }
    }

    // 8. Else global is true
    // a. Let fullUnicode be ToBoolean(Get(rx, "unicode")).
    JSHandle<JSTaggedValue> unicode = global_const->GetHandledUnicodeString();
    JSTaggedValue uincode_value = FastRuntimeStub::FastGetPropertyByValue(thread, this_obj, unicode);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    bool full_unicode = uincode_value.ToBoolean();
    // b. ReturnIfAbrupt(fullUnicode)
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // c. Let setStatus be Set(rx, "lastIndex", 0, true).
    JSHandle<JSTaggedValue> last_index_string(global_const->GetHandledLastIndexString());
    FastRuntimeStub::FastSetPropertyByValue(thread, JSHandle<JSTaggedValue>(this_obj), last_index_string,
                                            thread->GlobalConstants()->GetHandledZero());
    // d. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // e. Let A be ArrayCreate(0).
    JSHandle<JSObject> array(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    // f. Let n be 0.
    int result_num = 0;
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue(0));
    // g. Repeat,
    while (true) {
        // i. Let result be RegExpExec(rx, S).
        result.Update(reg_exp::RegExpExec(thread, this_obj, string, use_cache));

        // ii. ReturnIfAbrupt(result).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // iii. If result is null, then
        if (result->IsNull()) {
            // 1. If n=0, return null.
            if (result_num == 0) {
                return JSTaggedValue::Null();
            }
            if (use_cache) {
                RegExpExecResultCache::AddResultInCache(thread, cache_table, pattern, flags, input_string,
                                                        JSHandle<JSTaggedValue>(array),
                                                        RegExpExecResultCache::MATCH_TYPE, 0);
            }
            // 2. Else, return A.
            return array.GetTaggedValue();
        }
        // iv. Else result is not null,
        // 1. Let matchStr be ToString(Get(result, "0")).
        JSHandle<JSTaggedValue> zero_string = global_const->GetHandledZeroString();
        JSTaggedValue match_val = FastRuntimeStub::FastGetPropertyByValue(thread, result, zero_string);
        JSHandle<JSTaggedValue> match_str(thread, match_val);
        JSHandle<EcmaString> match_string = JSTaggedValue::ToString(thread, match_str);
        // 2. ReturnIfAbrupt(matchStr).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<JSTaggedValue> match_value = JSHandle<JSTaggedValue>::Cast(match_string);
        // 3. Let status be CreateDataProperty(A, ToString(n), matchStr).
        JSObject::CreateDataProperty(thread, array, result_num, match_value);
        // 5. If matchStr is the empty String, then
        if (JSTaggedValue::ToString(thread, match_value)->GetLength() == 0) {
            // a. Let thisIndex be ToLength(Get(rx, "lastIndex")).
            JSTaggedValue last_index = FastRuntimeStub::FastGetPropertyByValue(thread, this_obj, last_index_string);
            JSHandle<JSTaggedValue> last_index_handle(thread, last_index);
            JSTaggedNumber this_index = JSTaggedValue::ToLength(thread, last_index_handle);
            // b. ReturnIfAbrupt(thisIndex).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // c. Let nextIndex be AdvanceStringIndex(S, thisIndex, fullUnicode).
            // d. Let setStatus be Set(rx, "lastIndex", nextIndex, true).
            auto next_index = JSTaggedValue(AdvanceStringIndex(string, this_index.GetNumber(), full_unicode));
            FastRuntimeStub::FastSetPropertyByValue(thread, JSHandle<JSTaggedValue>(this_obj), last_index_string,
                                                    HandleFromLocal(&next_index));
            // e. ReturnIfAbrupt(setStatus).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        // 6. Increase n.
        result_num++;
    }
}

JSTaggedValue reg_exp::proto::MatchAll(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    BUILTINS_API_TRACE(thread, RegExpPrototype, MatchAll);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let R be the this value.
    // 2. If Type(R) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    auto ecma_vm = thread->GetEcmaVM();
    if (!this_obj->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }

    // 3. Let S be ? ToString(string).
    JSHandle<JSTaggedValue> input_string = builtins_common::GetCallArg(argv, 0);
    JSHandle<EcmaString> string_handle = JSTaggedValue::ToString(thread, input_string);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4. Let C be ? SpeciesConstructor(R, %RegExp%).
    JSHandle<JSTaggedValue> default_constructor = ecma_vm->GetGlobalEnv()->GetRegExpFunction();
    JSHandle<JSObject> obj_handle(this_obj);
    JSHandle<JSTaggedValue> constructor = JSObject::SpeciesConstructor(thread, obj_handle, default_constructor);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    const GlobalEnvConstants *global_constants = thread->GlobalConstants();
    // 5. Let flags be ? ToString(? Get(R, "flags")).
    JSHandle<JSTaggedValue> flags_string(global_constants->GetHandledFlagsString());
    JSHandle<JSTaggedValue> get_flags(JSObject::GetProperty(thread, this_obj, flags_string).GetValue());
    JSHandle<EcmaString> flags_str_handle = JSTaggedValue::ToString(thread, get_flags);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. Let matcher be ? Construct(C, « R, flags »).
    JSHandle<JSTaggedValue> undefined = global_constants->GetHandledUndefined();

    auto info = NewRuntimeCallInfo(thread, constructor, JSTaggedValue::Undefined(), undefined, 2);
    info->SetCallArgs(this_obj.GetTaggedValue(), flags_str_handle.GetTaggedValue());
    JSTaggedValue tagged_matcher = JSFunction::Construct(info.Get());
    JSHandle<JSTaggedValue> matcher_handle(thread, tagged_matcher);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 7. Let lastIndex be ? ToLength(? Get(R, "lastIndex")).
    JSHandle<JSTaggedValue> last_index_string(global_constants->GetHandledLastIndexString());
    JSHandle<JSTaggedValue> get_last_index(JSObject::GetProperty(thread, this_obj, last_index_string).GetValue());
    JSTaggedNumber this_last_index = JSTaggedValue::ToLength(thread, get_last_index);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 8. Perform ? Set(matcher, "lastIndex", lastIndex, true).
    FastRuntimeStub::FastSetPropertyByValue(thread, matcher_handle, last_index_string,
                                            HandleFromLocal(&this_last_index));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 9. If flags contains "g", let global be true.
    // 10. Else, let global be false.
    JSHandle<EcmaString> g_string(global_constants->GetHandledGString());
    bool global = false;
    if (base::StringHelper::Contains(*flags_str_handle, *g_string)) {
        global = true;
    }

    // 11. If flags contains "u", let fullUnicode be true.
    // 12. Else, let fullUnicode be false.
    JSHandle<EcmaString> u_string(global_constants->GetHandledUString());
    bool full_unicode = false;
    if (base::StringHelper::Contains(*flags_str_handle, *u_string)) {
        full_unicode = true;
    }

    // 13. Return ! CreateRegExpStringIterator(matcher, S, global, fullUnicode).
    return JSRegExpIterator::CreateRegExpStringIterator(thread, matcher_handle, string_handle, global, full_unicode)
        .GetTaggedValue();
}

JSTaggedValue RegExpReplaceFast(JSThread *thread, JSHandle<JSTaggedValue> &regexp, JSHandle<EcmaString> input_string,
                                uint32_t input_length)
{
    ASSERT(regexp->IsJSRegExp());
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // get bytecode
    JSTaggedValue buffer_data = JSRegExp::Cast(regexp->GetTaggedObject())->GetByteCodeBuffer();
    void *dyn_buf = JSNativePointer::Cast(buffer_data.GetTaggedObject())->GetExternalPointer();
    // get flags
    auto bytecode_buffer = reinterpret_cast<uint8_t *>(dyn_buf);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    uint32_t flags = *reinterpret_cast<uint32_t *>(bytecode_buffer + RegExpParser::FLAGS_OFFSET);
    JSHandle<JSTaggedValue> last_index_handle(thread->GlobalConstants()->GetHandledLastIndexString());
    uint32_t last_index;
    JSHandle<JSRegExp> regexp_handle(regexp);
    bool use_cache = false;
    if ((flags & (RegExpParser::FLAG_STICKY | RegExpParser::FLAG_GLOBAL)) == 0) {
        last_index = 0;
    } else {
        JSTaggedValue this_index = FastRuntimeStub::FastGetPropertyByValue(thread, regexp, last_index_handle);
        if (this_index.IsInt()) {
            last_index = static_cast<uint32_t>(this_index.GetInt());
        } else {
            JSHandle<JSTaggedValue> this_index_handle(thread, this_index);
            last_index = JSTaggedValue::ToLength(thread, this_index_handle).GetNumber();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
    }

    auto global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> tag_input_string = JSHandle<JSTaggedValue>::Cast(input_string);
    JSHandle<JSTaggedValue> pattern(thread, regexp_handle->GetOriginalSource());
    JSHandle<JSTaggedValue> flags_bits(thread, regexp_handle->GetOriginalFlags());

    JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
    uint32_t length = input_string->GetLength();
    uint32_t large_str_count = cache_table->GetLargeStrCount();
    if (large_str_count != 0) {
        if (length > MIN_REPLACE_STRING_LENGTH) {
            cache_table->SetLargeStrCount(thread, --large_str_count);
        }
    } else {
        cache_table->SetStrLenThreshold(thread, MIN_REPLACE_STRING_LENGTH);
    }
    if (length > cache_table->GetStrLenThreshold()) {
        use_cache = ExecCachingAllowed(thread, regexp);
    }
    if (use_cache) {
        JSTaggedValue cache_result =
            cache_table->FindCachedResult(thread, pattern, flags_bits, tag_input_string,
                                          RegExpExecResultCache::REPLACE_TYPE, regexp, global_const->GetEmptyString());
        if (cache_result != JSTaggedValue::Undefined()) {
            return cache_result;
        }
    }

    std::string result_string;
    uint32_t next_position = 0;

    // 12. Let done be false.
    // 13. Repeat, while done is false
    for (;;) {
        if (last_index > input_length) {
            break;
        }

        bool is_utf16 = input_string->IsUtf16();
        const uint8_t *str_buffer;
        PandaVector<uint8_t> u8_buffer;
        PandaVector<uint16_t> u16_buffer;
        if (is_utf16) {
            u16_buffer = PandaVector<uint16_t>(input_length);
            input_string->CopyDataUtf16(u16_buffer.data(), input_length);
            str_buffer = reinterpret_cast<uint8_t *>(u16_buffer.data());
        } else {
            u8_buffer = PandaVector<uint8_t>(input_length + 1);
            input_string->CopyDataUtf8(u8_buffer.data(), input_length + 1);
            str_buffer = u8_buffer.data();
        }

        MatchResult match_result = Matcher(thread, regexp, str_buffer, input_length, last_index, is_utf16);
        if (!match_result.is_success) {
            if ((flags & (RegExpParser::FLAG_STICKY | RegExpParser::FLAG_GLOBAL)) != 0) {
                last_index = 0;
                FastRuntimeStub::FastSetPropertyByValue(thread, JSHandle<JSTaggedValue>(regexp), last_index_handle,
                                                        thread->GlobalConstants()->GetHandledZero());
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            break;
        }
        uint32_t start_index = match_result.index;
        uint32_t end_index = match_result.end_index;
        last_index = end_index;
        if (next_position < start_index) {
            result_string += base::StringHelper::SubString(input_string, next_position, start_index - next_position);
        }
        next_position = end_index;
        if ((flags & RegExpParser::FLAG_GLOBAL) == 0) {
            // a. Let setStatus be Set(R, "lastIndex", e, true).
            JSTaggedValue last_index_value(last_index);
            FastRuntimeStub::FastSetPropertyByValue(thread, JSHandle<JSTaggedValue>(regexp), last_index_handle,
                                                    HandleFromLocal(&last_index_value));
            // b. ReturnIfAbrupt(setStatus).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            break;
        }
        if (end_index == start_index) {
            bool unicode = input_string->IsUtf16() && ((flags & RegExpParser::FLAG_UTF16) != 0);
            end_index = reg_exp::AdvanceStringIndex(tag_input_string, end_index, unicode);
        }
        last_index = end_index;
    }
    result_string += base::StringHelper::SubString(input_string, next_position, input_length - next_position);
    auto result_value = factory->NewFromStdString(result_string);
    if (use_cache) {
        RegExpExecResultCache::AddResultInCache(
            thread, cache_table, pattern, flags_bits, tag_input_string, JSHandle<JSTaggedValue>(result_value),
            RegExpExecResultCache::REPLACE_TYPE, last_index, global_const->GetEmptyString());
    }
    return result_value.GetTaggedValue();
}

// 21.2.5.8
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue reg_exp::proto::Replace(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, Replace);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let rx be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    if (!this_obj->IsECMAObject()) {
        // 2. If Type(rx) is not Object, throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    // 3. Let S be ToString(string).
    JSHandle<JSTaggedValue> string = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> input_replace_value = builtins_common::GetCallArg(argv, 1);
    JSHandle<EcmaString> sr_panda_string = JSTaggedValue::ToString(thread, string);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    // 4. ReturnIfAbrupt(S).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> input_str = JSHandle<JSTaggedValue>::Cast(sr_panda_string);
    // 5. Let lengthS be the number of code unit elements in S.
    uint32_t length = sr_panda_string->GetLength();
    // 6. Let functionalReplace be IsCallable(replaceValue).
    bool functional_replace = input_replace_value->IsCallable();
    JSHandle<EcmaString> replace_value_handle;
    if (!functional_replace) {
        replace_value_handle = JSTaggedValue::ToString(thread, input_replace_value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    JSHandle<JSTaggedValue> last_index = global_const->GetHandledLastIndexString();
    // 8. Let global be ToBoolean(Get(rx, "global")).
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> global = global_const->GetHandledGlobalString();
    JSTaggedValue global_value = FastRuntimeStub::FastGetPropertyByValue(thread, this_obj, global);
    // 9. ReturnIfAbrupt(global).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    bool is_global = global_value.ToBoolean();

    // 10. If global is true, then
    bool full_unicode = false;
    if (is_global) {
        // a. Let fullUnicode be ToBoolean(Get(rx, "unicode")).
        JSHandle<JSTaggedValue> unicode = global_const->GetHandledUnicodeString();
        JSTaggedValue full_unicode_tag = FastRuntimeStub::FastGetPropertyByValue(thread, this_obj, unicode);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        full_unicode = full_unicode_tag.ToBoolean();
        // b. ReturnIfAbrupt(fullUnicode).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // c. Let setStatus be Set(rx, "lastIndex", 0, true).
        FastRuntimeStub::FastSetPropertyByValue(thread, this_obj, last_index,
                                                thread->GlobalConstants()->GetHandledZero());
        // d. ReturnIfAbrupt(setStatus).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    // Add cache for regexp replace
    bool use_cache = false;
    JSMutableHandle<JSTaggedValue> pattern(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> flags_bits(thread, JSTaggedValue::Undefined());
    JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
    if (is_global && !functional_replace && this_obj->IsJSRegExp()) {
        JSHClass *hclass = JSHandle<JSObject>::Cast(this_obj)->GetJSHClass();
        JSHClass *origin_h_class = JSHClass::Cast(global_const->GetJSRegExpClass().GetTaggedObject());
        if (hclass == origin_h_class) {
            if (replace_value_handle->GetLength() == 0) {
                return RegExpReplaceFast(thread, this_obj, sr_panda_string, length);
            }
            JSHandle<JSRegExp> regexp_handle(this_obj);
            if (regexp_handle->IsJSRegExp()) {
                pattern.Update(regexp_handle->GetOriginalSource());
                flags_bits.Update(regexp_handle->GetOriginalFlags());
            }
            uint32_t str_length = replace_value_handle->GetLength();
            uint32_t large_str_count = cache_table->GetLargeStrCount();
            if (large_str_count != 0) {
                if (str_length > MIN_REPLACE_STRING_LENGTH) {
                    cache_table->SetLargeStrCount(thread, --large_str_count);
                }
            } else {
                cache_table->SetStrLenThreshold(thread, MIN_REPLACE_STRING_LENGTH);
            }
            if (str_length > cache_table->GetStrLenThreshold()) {
                use_cache = ExecCachingAllowed(thread, this_obj);
                JSTaggedValue cache_result = cache_table->FindCachedResult(
                    thread, pattern, flags_bits, string, RegExpExecResultCache::REPLACE_TYPE, this_obj,
                    input_replace_value.GetTaggedValue());
                if (cache_result != JSTaggedValue::Undefined()) {
                    return cache_result;
                }
            }
        }
    }

    JSHandle<JSTaggedValue> matched_str = global_const->GetHandledZeroString();
    // 11. Let results be a new empty List.
    JSHandle<JSObject> results_list(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    int results_index = 0;
    // 12. Let done be false.
    // 13. Repeat, while done is false
    JSMutableHandle<JSTaggedValue> next_index_handle(thread, JSTaggedValue(0));
    JSMutableHandle<JSTaggedValue> exec_result(thread, JSTaggedValue(0));
    for (;;) {
        // a. Let result be RegExpExec(rx, S).
        exec_result.Update(RegExpExec(thread, this_obj, input_str, use_cache));
        // b. ReturnIfAbrupt(result).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // c. If result is null, set done to true.
        if (exec_result->IsNull()) {
            break;
        }
        // d. Else result is not null, i. Append result to the end of results.
        JSObject::CreateDataProperty(thread, results_list, results_index, exec_result);
        results_index++;
        // ii. If global is false, set done to true.
        if (!is_global) {
            break;
        }
        // iii. Else, 1. Let matchStr be ToString(Get(result, "0")).
        JSTaggedValue get_match_val = FastRuntimeStub::FastGetPropertyByValue(thread, exec_result, matched_str);
        JSHandle<JSTaggedValue> get_match(thread, get_match_val);
        JSHandle<EcmaString> match_string = JSTaggedValue::ToString(thread, get_match);
        // 2. ReturnIfAbrupt(matchStr).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // 3. If matchStr is the empty String, then
        if (match_string->GetLength() == 0) {
            // a. Let thisIndex be ToLength(Get(rx, "lastIndex")).
            JSTaggedValue this_index_val = FastRuntimeStub::FastGetPropertyByValue(thread, this_obj, last_index);
            JSHandle<JSTaggedValue> this_index_handle(thread, this_index_val);
            uint32_t this_index = 0;
            if (this_index_handle->IsInt()) {
                this_index = static_cast<uint32_t>(this_index_handle->GetInt());
            } else {
                this_index_val = JSTaggedValue::ToLength(thread, this_index_handle);
                // b. ReturnIfAbrupt(thisIndex).
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                this_index = this_index_val.GetNumber();
            }
            // c. Let nextIndex be AdvanceStringIndex(S, thisIndex, fullUnicode).
            uint32_t next_index = AdvanceStringIndex(input_str, this_index, full_unicode);
            next_index_handle.Update(JSTaggedValue(next_index));
            // d. Let setStatus be Set(rx, "lastIndex", nextIndex, true).
            FastRuntimeStub::FastSetPropertyByValue(thread, this_obj, last_index, next_index_handle);
            // e. ReturnIfAbrupt(setStatus).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
    }
    // 14. Let accumulatedResult be the empty String value.
    std::string accumulated_result;
    // 15. Let nextSourcePosition be 0.
    uint32_t next_source_position = 0;
    JSHandle<JSTaggedValue> get_match_string;
    JSMutableHandle<JSTaggedValue> result_values(thread, JSTaggedValue(0));
    JSMutableHandle<JSTaggedValue> ncaptures_handle(thread, JSTaggedValue(0));
    JSMutableHandle<JSTaggedValue> cap_n(thread, JSTaggedValue(0));
    // 16. Repeat, for each result in results,
    for (int i = 0; i < results_index; i++) {
        result_values.Update(FastRuntimeStub::FastGetPropertyByIndex(thread, results_list.GetTaggedValue(), i));
        // a. Let nCaptures be ToLength(Get(result, "length")).
        JSHandle<JSTaggedValue> length_handle = global_const->GetHandledLengthString();
        ncaptures_handle.Update(FastRuntimeStub::FastGetPropertyByValue(thread, result_values, length_handle));
        uint32_t ncaptures = JSTaggedValue::ToUint32(thread, ncaptures_handle);
        // b. ReturnIfAbrupt(nCaptures).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // c. Let nCaptures be max(nCaptures − 1, 0).
        ncaptures = std::max<uint32_t>((ncaptures - 1), 0);
        // d. Let matched be ToString(Get(result, "0")).
        JSTaggedValue value = FastRuntimeStub::GetPropertyByIndex(thread, result_values.GetTaggedValue(), 0);
        get_match_string = JSHandle<JSTaggedValue>(thread, value);
        JSHandle<EcmaString> match_string = JSTaggedValue::ToString(thread, get_match_string);
        // e. ReturnIfAbrupt(matched).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // f. Let matchLength be the number of code units in matched.
        uint32_t match_length = match_string->GetLength();
        // g. Let position be ToInteger(Get(result, "index")).
        JSHandle<JSTaggedValue> result_index = global_const->GetHandledIndexString();
        JSTaggedValue position_tag = FastRuntimeStub::FastGetPropertyByValue(thread, result_values, result_index);
        JSHandle<JSTaggedValue> position_handle(thread, position_tag);
        uint32_t position = 0;
        if (position_handle->IsInt()) {
            position = static_cast<uint32_t>(position_handle->GetInt());
        } else {
            position = JSTaggedValue::ToUint32(thread, position_handle);
            // h. ReturnIfAbrupt(position).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        // i. Let position be max(min(position, lengthS), 0).
        position = std::max<uint32_t>(std::min<uint32_t>(position, length), 0);
        // j. Let n be 1.
        uint32_t index = 1;
        // k. Let captures be an empty List.
        JSHandle<TaggedArray> captures_list = factory->NewTaggedArray(ncaptures);
        // l. Repeat while n ≤ nCaptures
        while (index <= ncaptures) {
            // i. Let capN be Get(result, ToString(n)).
            cap_n.Update(FastRuntimeStub::FastGetPropertyByIndex(thread, result_values.GetTaggedValue(), index));
            // ii. ReturnIfAbrupt(capN).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // iii. If capN is not undefined, then
            if (!cap_n->IsUndefined()) {
                // 1. Let capN be ToString(capN).
                JSHandle<EcmaString> cap_n_str = JSTaggedValue::ToString(thread, cap_n);
                // 2. ReturnIfAbrupt(capN).
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                JSHandle<JSTaggedValue> capn_str = JSHandle<JSTaggedValue>::Cast(cap_n_str);
                captures_list->Set(thread, index - 1, capn_str);
            } else {
                // iv. Append capN as the last element of captures.
                captures_list->Set(thread, index - 1, cap_n);
            }
            // v. Let n be n+1
            ++index;
        }

        // j. Let namedCaptures be ? Get(result, "groups").
        JSHandle<JSTaggedValue> groups_key = global_const->GetHandledGroupsString();
        JSTaggedValue named = FastRuntimeStub::FastGetPropertyByValue(thread, result_values, groups_key);
        JSHandle<JSTaggedValue> named_captures(thread, named);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // m. If functionalReplace is true, then
        PandaString replacement;
        int empty_arr_length = 0;
        if (named_captures->IsUndefined()) {
            empty_arr_length = 3;  // 3: «matched, pos, and string»
        } else {
            empty_arr_length = 4;  // 4: «matched, pos, string, and groups»
        }
        JSHandle<TaggedArray> replacer_args = factory->NewTaggedArray(empty_arr_length + captures_list->GetLength());
        if (functional_replace) {
            // i. Let replacerArgs be «matched».
            replacer_args->Set(thread, 0, get_match_string.GetTaggedValue());
            // ii. Append in list order the elements of captures to the end of the List replacerArgs.
            // iii. Append position and S as the last two elements of replacerArgs.
            index = 0;
            while (index < captures_list->GetLength()) {
                replacer_args->Set(thread, index + 1, captures_list->Get(index));
                ++index;
            }
            replacer_args->Set(thread, index + 1, JSTaggedValue(position));
            replacer_args->Set(thread, index + 2, input_str.GetTaggedValue());  // 2: position of string
            if (!named_captures->IsUndefined()) {
                replacer_args->Set(thread, index + 3, named_captures.GetTaggedValue());  // 3: position of groups
            }
            // iv. Let replValue be Call(replaceValue, undefined, replacerArgs).
            const int32_t args_length = replacer_args->GetLength();
            auto info = NewRuntimeCallInfo(thread, input_replace_value, JSTaggedValue::Undefined(),
                                           JSTaggedValue::Undefined(), args_length);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            info->SetCallArg(args_length, replacer_args->GetData());
            JSTaggedValue replace_result = JSFunction::Call(info.Get());
            JSHandle<JSTaggedValue> repl_value(thread, replace_result);
            // v. Let replacement be ToString(replValue).
            JSHandle<EcmaString> replacement_string = JSTaggedValue::ToString(thread, repl_value);
            // o. ReturnIfAbrupt(replacement).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            replacement = ConvertToPandaString(*replacement_string, StringConvertedUsage::LOGICOPERATION);
        } else {
            // n. Else,
            if (!named_captures->IsUndefined()) {
                JSHandle<JSObject> named_captures_obj = JSTaggedValue::ToObject(thread, named_captures);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                named_captures = JSHandle<JSTaggedValue>::Cast(named_captures_obj);
            }
            JSHandle<JSTaggedValue> replacement_handle(
                thread, string::GetSubstitution(thread, match_string, sr_panda_string, position, captures_list,
                                                named_captures, replace_value_handle));
            replacement = ConvertToPandaString(EcmaString::Cast(replacement_handle->GetTaggedObject()),
                                               StringConvertedUsage::LOGICOPERATION);
        }
        // p. If position ≥ nextSourcePosition, then
        if (position >= next_source_position) {
            // ii. Let accumulatedResult be the String formed by concatenating the code units of the current value
            // of accumulatedResult with the substring of S consisting of the code units from nextSourcePosition
            // (inclusive) up to position (exclusive) and with the code units of replacement.
            accumulated_result += base::StringHelper::SubString(
                JSHandle<EcmaString>::Cast(input_str), next_source_position, (position - next_source_position));
            accumulated_result += replacement;
            // iii. Let nextSourcePosition be position + matchLength.
            next_source_position = position + match_length;
        }
    }
    // 17. If nextSourcePosition ≥ lengthS, return accumulatedResult.
    if (next_source_position >= length) {
        JSHandle<EcmaString> result_value = factory->NewFromStdString(accumulated_result);
        if (use_cache) {
            RegExpExecResultCache::AddResultInCache(
                thread, cache_table, pattern, flags_bits, string, JSHandle<JSTaggedValue>(result_value),
                RegExpExecResultCache::REPLACE_TYPE, next_index_handle->GetInt(), input_replace_value.GetTaggedValue());
        }
        return result_value.GetTaggedValue();
    }
    // 18. Return the String formed by concatenating the code units of accumulatedResult with the substring of S
    // consisting of the code units from nextSourcePosition (inclusive) up through the final code unit of S(inclusive).
    accumulated_result += base::StringHelper::SubString(JSHandle<EcmaString>::Cast(input_str), next_source_position,
                                                        (length - next_source_position));
    JSHandle<EcmaString> result_value = factory->NewFromStdString(accumulated_result);
    if (use_cache) {
        RegExpExecResultCache::AddResultInCache(
            thread, cache_table, pattern, flags_bits, string, JSHandle<JSTaggedValue>(result_value),
            RegExpExecResultCache::REPLACE_TYPE, next_index_handle->GetInt(), input_replace_value.GetTaggedValue());
    }
    return result_value.GetTaggedValue();
}

// 21.2.5.9
JSTaggedValue reg_exp::proto::Search(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, Search);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let rx be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    // 3. Let S be ToString(string).
    JSHandle<JSTaggedValue> input_str = builtins_common::GetCallArg(argv, 0);
    JSHandle<EcmaString> string_handle = JSTaggedValue::ToString(thread, input_str);

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> string = JSHandle<JSTaggedValue>::Cast(string_handle);
    if (!this_obj->IsECMAObject()) {
        // 2. If Type(rx) is not Object, throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    // 4. Let previousLastIndex be ? Get(rx, "lastIndex").
    JSHandle<JSTaggedValue> last_index_string(thread->GlobalConstants()->GetHandledLastIndexString());
    JSHandle<JSTaggedValue> previous_last_index = JSObject::GetProperty(thread, this_obj, last_index_string).GetValue();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. If SameValue(previousLastIndex, 0) is false, then
    // Perform ? Set(rx, "lastIndex", 0, true).
    if (!JSTaggedValue::SameValue(previous_last_index.GetTaggedValue(), JSTaggedValue(0))) {
        JSHandle<JSTaggedValue> value(thread, JSTaggedValue(0));
        JSObject::SetProperty(thread, this_obj, last_index_string, value, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // 6. Let result be ? RegExpExec(rx, S).
    JSHandle<JSTaggedValue> result(thread, RegExpExec(thread, this_obj, string, false));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 7. Let currentLastIndex be ? Get(rx, "lastIndex").
    JSHandle<JSTaggedValue> current_last_index = JSObject::GetProperty(thread, this_obj, last_index_string).GetValue();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 8. If SameValue(currentLastIndex, previousLastIndex) is false, then
    // Perform ? Set(rx, "lastIndex", previousLastIndex, true).
    if (!JSTaggedValue::SameValue(previous_last_index.GetTaggedValue(), current_last_index.GetTaggedValue())) {
        JSObject::SetProperty(thread, this_obj, last_index_string, previous_last_index, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // 9. If result is null, return -1.
    if (result->IsNull()) {
        return JSTaggedValue(-1);
    }
    // 10. Return ? Get(result, "index").
    JSHandle<JSTaggedValue> index(thread->GlobalConstants()->GetHandledIndexString());
    return JSObject::GetProperty(thread, result, index).GetValue().GetTaggedValue();
}

// 21.2.5.11
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue reg_exp::proto::Split(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), RegExpPrototype, Split);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    bool use_cache = false;
    // 1. Let rx be the this value.
    JSHandle<JSTaggedValue> this_obj = builtins_common::GetThis(argv);
    auto ecma_vm = thread->GetEcmaVM();
    // 3. Let S be ToString(string).
    JSHandle<JSTaggedValue> input_string = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> limit = builtins_common::GetCallArg(argv, 1);
    JSHandle<EcmaString> string_handle = JSTaggedValue::ToString(thread, input_string);

    // 4. ReturnIfAbrupt(string).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> js_string = JSHandle<JSTaggedValue>::Cast(string_handle);
    if (!this_obj->IsECMAObject()) {
        // 2. If Type(rx) is not Object, throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", JSTaggedValue::Exception());
    }
    // 5. Let C be SpeciesConstructor(rx, %RegExp%).
    JSHandle<JSTaggedValue> default_constructor = ecma_vm->GetGlobalEnv()->GetRegExpFunction();
    JSHandle<JSObject> obj_handle(this_obj);
    JSHandle<JSTaggedValue> constructor = JSObject::SpeciesConstructor(thread, obj_handle, default_constructor);
    // 6. ReturnIfAbrupt(C).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 7. Let flags be ToString(Get(rx, "flags")).
    ObjectFactory *factory = ecma_vm->GetFactory();
    const GlobalEnvConstants *global_constants = thread->GlobalConstants();
    JSHandle<JSTaggedValue> flags_string(global_constants->GetHandledFlagsString());
    JSHandle<JSTaggedValue> tagged_flags = JSObject::GetProperty(thread, this_obj, flags_string).GetValue();
    JSHandle<EcmaString> flags;

    if (tagged_flags->IsUndefined()) {
        flags = factory->GetEmptyString();
    } else {
        flags = JSTaggedValue::ToString(thread, tagged_flags);
    }
    //  8. ReturnIfAbrupt(flags).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 9. If flags contains "u", let unicodeMatching be true.
    // 10. Else, let unicodeMatching be false.
    JSHandle<EcmaString> u_string_handle(global_constants->GetHandledUString());
    bool unicode_matching = base::StringHelper::Contains(*flags, *u_string_handle);
    // 11. If flags contains "y", let newFlags be flags.
    JSHandle<EcmaString> new_flags_handle;
    JSHandle<EcmaString> y_string_handle = JSHandle<EcmaString>::Cast(global_constants->GetHandledYString());
    if (base::StringHelper::Contains(*flags, *y_string_handle)) {
        new_flags_handle = flags;
    } else {
        // 12. Else, let newFlags be the string that is the concatenation of flags and "y".
        JSHandle<EcmaString> y_str = JSHandle<EcmaString>::Cast(global_constants->GetHandledYString());
        new_flags_handle = factory->ConcatFromString(flags, y_str);
    }

    // 17. If limit is undefined, let lim be 2^32–1; else let lim be ToUint32(limit).
    uint32_t lim;
    if (limit->IsUndefined()) {
        lim = MAX_SPLIT_LIMIT;
    } else {
        lim = JSTaggedValue::ToUint32(thread, limit);
        // 18. ReturnIfAbrupt(lim).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    if (lim == MAX_SPLIT_LIMIT) {
        use_cache = ExecCachingAllowed(thread, this_obj);
    }

    JSHandle<JSRegExp> regexp_handle(this_obj);
    JSMutableHandle<JSTaggedValue> pattern(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> flags_bits(thread, JSTaggedValue::Undefined());
    if (this_obj->IsJSRegExp()) {
        pattern.Update(regexp_handle->GetOriginalSource());
        flags_bits.Update(regexp_handle->GetOriginalFlags());
    }
    JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
    if (use_cache) {
        JSTaggedValue cache_result = cache_table->FindCachedResult(thread, pattern, flags_bits, input_string,
                                                                   RegExpExecResultCache::SPLIT_TYPE, this_obj);
        if (cache_result != JSTaggedValue::Undefined()) {
            return cache_result;
        }
    }

    // 13. Let splitter be Construct(C, «rx, newFlags»).
    JSHandle<JSObject> global_object(thread, thread->GetEcmaVM()->GetGlobalEnv()->GetGlobalObject());
    JSHandle<JSTaggedValue> undefined = global_constants->GetHandledUndefined();

    auto info = NewRuntimeCallInfo(thread, constructor, JSTaggedValue::Undefined(), undefined, 2);
    info->SetCallArgs(this_obj.GetTaggedValue(), new_flags_handle.GetTaggedValue());
    JSTaggedValue tagged_splitter = JSFunction::Construct(info.Get());
    // 14. ReturnIfAbrupt(splitter).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSTaggedValue> splitter(thread, tagged_splitter);
    // 15. Let A be ArrayCreate(0).
    JSHandle<JSObject> array(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    // 16. Let lengthA be 0.
    uint32_t a_length = 0;

    // 19. Let size be the number of elements in S.
    uint32_t size = static_cast<EcmaString *>(js_string->GetTaggedObject())->GetLength();
    // 20. Let p be 0.
    uint32_t start_index = 0;
    // 21. If lim = 0, return A.
    if (lim == 0) {
        return JSTaggedValue(static_cast<JSArray *>(array.GetTaggedValue().GetTaggedObject()));
    }
    // 22. If size = 0, then
    if (size == 0) {
        // a. Let z be RegExpExec(splitter, S).
        JSHandle<JSTaggedValue> exec_result(thread, RegExpExec(thread, splitter, js_string, use_cache));
        // b. ReturnIfAbrupt(z).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // c. If z is not null, return A.
        if (!exec_result->IsNull()) {
            return JSTaggedValue(static_cast<JSArray *>(array.GetTaggedValue().GetTaggedObject()));
        }
        // d. Assert: The following call will never result in an abrupt completion.
        // e. Perform CreateDataProperty(A, "0", S).
        JSObject::CreateDataProperty(thread, array, 0, js_string);
        // f. Return A.
        return JSTaggedValue(static_cast<JSArray *>(array.GetTaggedValue().GetTaggedObject()));
    }
    // 23. Let q be p.
    uint32_t end_index = start_index;
    JSMutableHandle<JSTaggedValue> last_indexvalue(thread, JSTaggedValue(end_index));
    // 24. Repeat, while q < size
    JSHandle<JSTaggedValue> last_index_string = global_constants->GetHandledLastIndexString();
    while (end_index < size) {
        // a. Let setStatus be Set(splitter, "lastIndex", q, true).
        last_indexvalue.Update(JSTaggedValue(end_index));
        JSObject::SetProperty(thread, splitter, last_index_string, last_indexvalue, true);
        // b. ReturnIfAbrupt(setStatus).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<JSTaggedValue> exec_result(thread, RegExpExec(thread, splitter, js_string, use_cache));
        // d. ReturnIfAbrupt(z).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // e. If z is null, let q be AdvanceStringIndex(S, q, unicodeMatching).
        if (exec_result->IsNull()) {
            end_index = AdvanceStringIndex(js_string, end_index, unicode_matching);
        } else {
            // f. Else z is not null,
            // i. Let e be ToLength(Get(splitter, "lastIndex")).
            JSHandle<JSTaggedValue> last_index_handle =
                JSObject::GetProperty(thread, splitter, last_index_string).GetValue();
            JSTaggedNumber last_index_number = JSTaggedValue::ToLength(thread, last_index_handle);
            // ii. ReturnIfAbrupt(e).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            uint32_t last_index = last_index_number.GetNumber();
            // iii. If e = p, let q be AdvanceStringIndex(S, q, unicodeMatching).
            if (last_index == start_index) {
                end_index = AdvanceStringIndex(js_string, end_index, unicode_matching);
            } else {
                // iv. Else e != p,
                // 1. Let T be a String value equal to the substring of S consisting of the elements at indices p
                // (inclusive) through q (exclusive).
                std::string std_str_t = base::StringHelper::SubString(JSHandle<EcmaString>::Cast(js_string),
                                                                      start_index, (end_index - start_index));
                // 2. Assert: The following call will never result in an abrupt completion.
                // 3. Perform CreateDataProperty(A, ToString(lengthA), T).
                JSHandle<JSTaggedValue> t_value(factory->NewFromStdString(std_str_t));
                JSObject::CreateDataProperty(thread, array, a_length, t_value);
                // 4. Let lengthA be lengthA +1.
                ++a_length;
                // 5. If lengthA = lim, return A.
                if (a_length == lim) {
                    if (use_cache) {
                        RegExpExecResultCache::AddResultInCache(thread, cache_table, pattern, flags_bits, input_string,
                                                                JSHandle<JSTaggedValue>(array),
                                                                RegExpExecResultCache::SPLIT_TYPE, last_index);
                    }
                    return array.GetTaggedValue();
                }
                // 6. Let p be e.
                start_index = last_index;
                // 7. Let numberOfCaptures be ToLength(Get(z, "length")).
                JSHandle<JSTaggedValue> length_string(thread->GlobalConstants()->GetHandledLengthString());
                JSHandle<JSTaggedValue> captures_handle =
                    JSObject::GetProperty(thread, exec_result, length_string).GetValue();
                JSTaggedNumber number_of_captures_number = JSTaggedValue::ToLength(thread, captures_handle);
                // 8. ReturnIfAbrupt(numberOfCaptures).
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                uint32_t number_of_captures = number_of_captures_number.GetNumber();
                // 9. Let numberOfCaptures be max(numberOfCaptures-1, 0).
                number_of_captures = (number_of_captures == 0) ? 0 : number_of_captures - 1;
                // 10. Let i be 1.
                uint32_t i = 1;
                // 11. Repeat, while i ≤ numberOfCaptures.
                while (i <= number_of_captures) {
                    // a. Let nextCapture be Get(z, ToString(i)).
                    JSHandle<JSTaggedValue> next_capture = JSObject::GetProperty(thread, exec_result, i).GetValue();
                    // b. ReturnIfAbrupt(nextCapture).
                    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                    // c. Perform CreateDataProperty(A, ToString(lengthA), nextCapture).
                    JSObject::CreateDataProperty(thread, array, a_length, next_capture);
                    // d. Let i be i + 1.
                    ++i;
                    // e. Let lengthA be lengthA +1.
                    ++a_length;
                    // f. If lengthA = lim, return A.
                    if (a_length == lim) {
                        if (use_cache) {
                            RegExpExecResultCache::AddResultInCache(thread, cache_table, pattern, flags_bits,
                                                                    input_string, JSHandle<JSTaggedValue>(array),
                                                                    RegExpExecResultCache::SPLIT_TYPE, last_index);
                        }
                        return array.GetTaggedValue();
                    }
                }
                // 12. Let q be p.
                end_index = start_index;
            }
        }
    }
    // 25. Let T be a String value equal to the substring of S consisting of the elements at indices p (inclusive)
    // through size (exclusive).
    std::string std_str_t =
        base::StringHelper::SubString(JSHandle<EcmaString>::Cast(js_string), start_index, (size - start_index));
    // 26. Assert: The following call will never result in an abrupt completion.
    // 27. Perform CreateDataProperty(A, ToString(lengthA), t).
    JSHandle<JSTaggedValue> t_value(factory->NewFromStdString(std_str_t));
    JSObject::CreateDataProperty(thread, array, a_length, t_value);
    if (lim == MAX_SPLIT_LIMIT) {
        RegExpExecResultCache::AddResultInCache(thread, cache_table, pattern, flags_bits, input_string,
                                                JSHandle<JSTaggedValue>(array), RegExpExecResultCache::SPLIT_TYPE,
                                                end_index);
    }
    // 28. Return A.
    return array.GetTaggedValue();
}

// NOLINTNEXTLINE(readability-non-const-parameter)
MatchResult Matcher(JSThread *thread, const JSHandle<JSTaggedValue> &regexp, const uint8_t *buffer, size_t length,
                    int32_t last_index, bool is_utf16)
{
    // get bytecode
    JSTaggedValue buffer_data = JSRegExp::Cast(regexp->GetTaggedObject())->GetByteCodeBuffer();
    void *dyn_buf = JSNativePointer::Cast(buffer_data.GetTaggedObject())->GetExternalPointer();
    auto bytecode_buffer = reinterpret_cast<uint8_t *>(dyn_buf);
    // execute
    RegExpExecutor executor = RegExpExecutor();
    if (last_index < 0) {
        last_index = 0;
    }
    bool ret = executor.Execute(buffer, last_index, static_cast<uint32_t>(length), bytecode_buffer, is_utf16);
    MatchResult result = executor.GetResult(thread, ret);
    return result;
}

uint32_t reg_exp::AdvanceStringIndex(const JSHandle<JSTaggedValue> &input_str, uint32_t index, bool unicode)
{
    // 1. Assert: Type(S) is String.
    ASSERT(input_str->IsString());
    // 2. Assert: index is an integer such that 0≤index≤2^53 - 1
    ASSERT(index <= pow(2, 53) - 1);
    // 3. Assert: Type(unicode) is Boolean.
    // 4. If unicode is false, return index+1.
    if (!unicode) {
        return index + 1;
    }
    // 5. Let length be the number of code units in S.
    uint32_t length = static_cast<EcmaString *>(input_str->GetTaggedObject())->GetLength();
    // 6. If index+1 ≥ length, return index+1.
    if (index + 1 >= length) {
        return index + 1;
    }
    // 7. Let first be the code unit value at index index in S.
    uint16_t first = static_cast<EcmaString *>(input_str->GetTaggedObject())->At(index);
    // 8. If first < 0xD800 or first > 0xDFFF, return index+1.
    if (first < 0xD800 || first > 0xDFFF) {  // NOLINT(readability-magic-numbers)
        return index + 1;
    }
    // 9. Let second be the code unit value at index index+1 in S.
    uint16_t second = static_cast<EcmaString *>(input_str->GetTaggedObject())->At(index + 1);
    // 10. If second < 0xDC00 or second > 0xDFFF, return index+1.
    if (second < 0xDC00 || second > 0xDFFF) {  // NOLINT(readability-magic-numbers)
        return index + 1;
    }
    // 11. Return index + 2.
    return index + 2;
}

bool GetFlagsInternal(JSThread *thread, const JSHandle<JSTaggedValue> &obj, const uint8_t mask)
{
    // 1. Let R be the this value.
    // 2. If Type(R) is not Object, throw a TypeError exception.
    if (!obj->IsECMAObject()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not Object", false);
    }
    // 3. If R does not have an [[OriginalFlags]] internal slot, throw a TypeError exception.
    JSHandle<JSObject> pattern_obj = JSHandle<JSObject>::Cast(obj);
    if (!pattern_obj->IsJSRegExp()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this does not have [[OriginalFlags]]", false);
    }
    // 4. Let flags be the value of R’s [[OriginalFlags]] internal slot.
    JSHandle<JSRegExp> regexp_obj(thread, JSRegExp::Cast(obj->GetTaggedObject()));
    // 5. If flags contains the code unit "[flag]", return true.
    // 6. Return false.
    uint8_t flags = static_cast<uint8_t>(regexp_obj->GetOriginalFlags().GetInt());
    return (flags & mask) != 0;
}

// 21.2.5.2.2
JSTaggedValue RegExpBuiltinExec(JSThread *thread, const JSHandle<JSTaggedValue> &regexp,
                                const JSHandle<JSTaggedValue> &input_str, bool use_cache)
{
    ASSERT(JSObject::IsRegExp(thread, regexp));
    ASSERT(input_str->IsString());

    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> last_index_handle = global_const->GetHandledLastIndexString();
    JSTaggedValue result = FastRuntimeStub::FastGetPropertyByValue(thread, regexp, last_index_handle);
    int32_t last_index = 0;
    if (result.IsInt()) {
        last_index = result.GetInt();
    } else {
        JSHandle<JSTaggedValue> last_index_result(thread, result);
        JSTaggedNumber last_index_number = JSTaggedValue::ToLength(thread, last_index_result);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        last_index = last_index_number.GetNumber();
    }

    JSHandle<JSTaggedValue> global_handle = global_const->GetHandledGlobalString();
    bool global = FastRuntimeStub::FastGetPropertyByValue(thread, regexp, global_handle).ToBoolean();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> sticky_handle = global_const->GetHandledStickyString();
    bool sticky = FastRuntimeStub::FastGetPropertyByValue(thread, regexp, sticky_handle).ToBoolean();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (!global && !sticky) {
        last_index = 0;
    }

    JSHandle<JSRegExp> regexp_obj(regexp);
    JSMutableHandle<JSTaggedValue> pattern(thread, regexp_obj->GetOriginalSource());
    JSMutableHandle<JSTaggedValue> flags(thread, regexp_obj->GetOriginalFlags());

    JSHandle<RegExpExecResultCache> cache_table(thread->GetEcmaVM()->GetRegExpCache());
    uint32_t length = static_cast<EcmaString *>(input_str->GetTaggedObject())->GetLength();
    if (last_index > static_cast<int32_t>(length)) {
        FastRuntimeStub::FastSetPropertyByValue(thread, regexp, last_index_handle,
                                                thread->GlobalConstants()->GetHandledZero());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        return JSTaggedValue::Null();
    }
    JSHandle<EcmaString> input_string = JSTaggedValue::ToString(thread, input_str);
    bool is_utf16 = input_string->IsUtf16();
    const uint8_t *str_buffer;
    size_t string_length = input_string->GetLength();
    PandaVector<uint8_t> u8_buffer;
    PandaVector<uint16_t> u16_buffer;
    if (is_utf16) {
        u16_buffer = PandaVector<uint16_t>(string_length);
        input_string->CopyDataUtf16(u16_buffer.data(), string_length);
        str_buffer = reinterpret_cast<uint8_t *>(u16_buffer.data());
    } else {
        u8_buffer = PandaVector<uint8_t>(string_length + 1);
        input_string->CopyDataUtf8(u8_buffer.data(), string_length + 1);
        str_buffer = u8_buffer.data();
    }
    MatchResult match_result = Matcher(thread, regexp, str_buffer, string_length, last_index, is_utf16);
    if (!match_result.is_success) {
        if (global || sticky) {
            JSHandle<JSTaggedValue> last_index_value(thread, JSTaggedValue(0));
            FastRuntimeStub::FastSetPropertyByValue(thread, regexp, last_index_handle,
                                                    thread->GlobalConstants()->GetHandledZero());
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        return JSTaggedValue::Null();
    }
    uint32_t end_index = match_result.end_index;
    if (global || sticky) {
        // a. Let setStatus be Set(R, "lastIndex", e, true).
        JSTaggedValue end_index_val(end_index);
        FastRuntimeStub::FastSetPropertyByValue(thread, regexp, last_index_handle, HandleFromLocal(&end_index_val));
        // b. ReturnIfAbrupt(setStatus).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    uint32_t captures_size = match_result.captures.size();
    JSHandle<JSObject> results(JSArray::ArrayCreate(thread, JSTaggedNumber(captures_size)));
    uint32_t match_index = match_result.index;
    // 24. Perform CreateDataProperty(A, "index", matchIndex).
    JSHandle<JSTaggedValue> index_key = global_const->GetHandledIndexString();
    JSHandle<JSTaggedValue> index_value(thread, JSTaggedValue(match_index));
    JSObject::CreateDataProperty(thread, results, index_key, index_value);
    // 25. Perform CreateDataProperty(A, "input", S).
    JSHandle<JSTaggedValue> input_key = global_const->GetHandledInputString();

    JSHandle<JSTaggedValue> input_value(thread, static_cast<EcmaString *>(input_str->GetTaggedObject()));
    JSObject::CreateDataProperty(thread, results, input_key, input_value);
    // 27. Perform CreateDataProperty(A, "0", matched_substr).
    JSHandle<JSTaggedValue> zero_value(match_result.captures[0].second);
    JSObject::CreateDataProperty(thread, results, 0, zero_value);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> group_name(thread, regexp_obj->GetGroupName());
    JSMutableHandle<JSTaggedValue> groups(thread, JSTaggedValue::Undefined());
    if (!group_name->IsUndefined()) {
        JSHandle<JSTaggedValue> null_handle(thread, JSTaggedValue::Null());
        JSHandle<JSObject> null_obj = factory->OrdinaryNewJSObjectCreate(null_handle);
        groups.Update(null_obj.GetTaggedValue());
    }
    JSHandle<JSTaggedValue> groups_key = global_const->GetHandledGroupsString();
    JSObject::CreateDataProperty(thread, results, groups_key, groups);
    // 28. For each integer i such that i > 0 and i <= n
    for (uint32_t i = 1; i < captures_size; i++) {
        // a. Let capture_i be ith element of r's captures List
        JSTaggedValue captured_value;
        if (match_result.captures[i].first) {
            captured_value = JSTaggedValue::Undefined();
        } else {
            captured_value = match_result.captures[i].second.GetTaggedValue();
        }
        JSHandle<JSTaggedValue> i_value(thread, captured_value);
        JSObject::CreateDataProperty(thread, results, i, i_value);
        if (!group_name->IsUndefined()) {
            JSHandle<JSObject> group_object = JSHandle<JSObject>::Cast(groups);
            TaggedArray *group_array = TaggedArray::Cast(regexp_obj->GetGroupName().GetTaggedObject());
            if (group_array->GetLength() > i - 1) {
                JSHandle<JSTaggedValue> skey(thread, group_array->Get(i - 1));
                JSObject::CreateDataProperty(thread, group_object, skey, i_value);
            }
        }
    }
    if (last_index == 0 && use_cache) {
        RegExpExecResultCache::AddResultInCache(thread, cache_table, pattern, flags, input_str,
                                                JSHandle<JSTaggedValue>(results), RegExpExecResultCache::EXEC_TYPE,
                                                end_index);
    }
    // 29. Return A.
    return results.GetTaggedValue();
}

// 21.2.5.2.1
JSTaggedValue reg_exp::RegExpExec(JSThread *thread, const JSHandle<JSTaggedValue> &regexp,
                                  const JSHandle<JSTaggedValue> &input_string, bool use_cache)
{
    // 1. Assert: Type(R) is Object.
    ASSERT(regexp->IsECMAObject());
    // 2. Assert: Type(S) is String.
    ASSERT(input_string->IsString());
    // 3. Let exec be Get(R, "exec").
    JSHandle<EcmaString> input_str = JSTaggedValue::ToString(thread, input_string);

    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> exec_handle = global_const->GetHandledExecString();
    JSTaggedValue exec_val = FastRuntimeStub::FastGetPropertyByValue(thread, regexp, exec_handle);
    JSHandle<JSTaggedValue> exec(thread, exec_val);
    // 4. ReturnIfAbrupt(exec).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. If IsCallable(exec) is true, then
    if (exec->IsCallable()) {
        auto info = NewRuntimeCallInfo(thread, exec, regexp, JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(input_str.GetTaggedValue());
        JSTaggedValue result = JSFunction::Call(info.Get());
        // b. ReturnIfAbrupt(result).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (!result.IsECMAObject() && !result.IsNull()) {
            // throw a TypeError exception.
            THROW_TYPE_ERROR_AND_RETURN(thread, "exec result is null or is not Object", JSTaggedValue::Exception());
        }
        return result;
    }
    // 6. If R does not have a [[RegExpMatcher]] internal slot, throw a TypeError exception.
    if (!regexp->IsJSRegExp()) {
        // throw a TypeError exception.
        THROW_TYPE_ERROR_AND_RETURN(thread, "this does not have a [[RegExpMatcher]]", JSTaggedValue::Exception());
    }
    // 7. Return RegExpBuiltinExec(R, S).
    return RegExpBuiltinExec(thread, regexp, input_string, use_cache);
}

// 21.2.3.2.1
JSTaggedValue RegExpAlloc(JSThread *thread, const JSHandle<JSTaggedValue> &new_target)
{
    /**
     * 1. Let obj be OrdinaryCreateFromConstructor(new_target, "%RegExpPrototype%",
     * «[[RegExpMatcher]],[[OriginalSource]], [[OriginalFlags]]»).
     */
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> func = env->GetRegExpFunction();
    JSHandle<JSTaggedValue> obj(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(func), new_target));
    // 2. ReturnIfAbrupt(obj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. Return obj.
    return obj.GetTaggedValue();
}

uint32_t UpdateExpressionFlags(JSThread *thread, const PandaString &check_str)
{
    uint32_t flags_bits = 0;
    uint32_t flags_bits_temp = 0;
    for (char i : check_str) {
        switch (i) {
            case 'g':
                flags_bits_temp = RegExpParser::FLAG_GLOBAL;
                break;
            case 'i':
                flags_bits_temp = RegExpParser::FLAG_IGNORECASE;
                break;
            case 'm':
                flags_bits_temp = RegExpParser::FLAG_MULTILINE;
                break;
            case 's':
                flags_bits_temp = RegExpParser::FLAG_DOTALL;
                break;
            case 'u':
                flags_bits_temp = RegExpParser::FLAG_UTF16;
                break;
            case 'y':
                flags_bits_temp = RegExpParser::FLAG_STICKY;
                break;
            default: {
                ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
                JSHandle<JSObject> syntax_error =
                    factory->GetJSError(base::ErrorType::SYNTAX_ERROR, "invalid regular expression flags");
                THROW_NEW_ERROR_AND_RETURN_VALUE(thread, syntax_error.GetTaggedValue(), 0);
            }
        }
        if ((flags_bits & flags_bits_temp) != 0) {
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            JSHandle<JSObject> syntax_error =
                factory->GetJSError(base::ErrorType::SYNTAX_ERROR, "invalid regular expression flags");
            THROW_NEW_ERROR_AND_RETURN_VALUE(thread, syntax_error.GetTaggedValue(), 0);
        }
        flags_bits |= flags_bits_temp;
    }
    return flags_bits;
}

JSTaggedValue reg_exp::FlagsBitsToString(JSThread *thread, uint8_t flags)
{
    ASSERT((flags & static_cast<uint8_t>(0xC0)) == 0);  // 0xC0: first 2 bits of flags must be 0

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
    std::array<uint8_t, 7U> flags_str;  // 7: maximum 6 flags + '\0'
    size_t flags_len = 0;
    if ((flags & RegExpParser::FLAG_GLOBAL) != 0) {
        flags_str[flags_len] = 'g';
        flags_len++;
    }
    if ((flags & RegExpParser::FLAG_IGNORECASE) != 0) {
        flags_str[flags_len] = 'i';
        flags_len++;
    }
    if ((flags & RegExpParser::FLAG_MULTILINE) != 0) {
        flags_str[flags_len] = 'm';
        flags_len++;
    }
    if ((flags & RegExpParser::FLAG_DOTALL) != 0) {
        flags_str[flags_len] = 's';
        flags_len++;
    }
    if ((flags & RegExpParser::FLAG_UTF16) != 0) {
        flags_str[flags_len] = 'u';
        flags_len++;
    }
    if ((flags & RegExpParser::FLAG_STICKY) != 0) {
        flags_str[flags_len] = 'y';
        flags_len++;
    }
    flags_str[flags_len] = '\0';
    JSHandle<EcmaString> flags_string = thread->GetEcmaVM()->GetFactory()->NewFromUtf8(flags_str.data(), flags_len);

    return flags_string.GetTaggedValue();
}

// 21.2.3.2.2
JSTaggedValue RegExpInitialize(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                               const JSHandle<JSTaggedValue> &pattern, const JSHandle<JSTaggedValue> &flags)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> pattern_str_handle;
    uint8_t flags_bits = 0;
    // 1. If pattern is undefined, let P be the empty String.
    if (pattern->IsUndefined()) {
        pattern_str_handle = factory->GetEmptyString();
    } else {
        // 2. Else, let P be ToString(pattern).
        pattern_str_handle = JSTaggedValue::ToString(thread, pattern);
        // 3. ReturnIfAbrupt(P).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // 4. If flags is undefined, let F be the empty String.
    if (flags->IsUndefined()) {
        flags_bits = 0;
    } else if (flags->IsInt()) {
        flags_bits = static_cast<uint8_t>(flags->GetInt());
    } else {
        // 5. Else, let F be ToString(flags).
        JSHandle<EcmaString> flags_str_handle = JSTaggedValue::ToString(thread, flags);
        // 6. ReturnIfAbrupt(F).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        /**
         * 7. If F contains any code unit other than "g", "i", "m", "u", or "y" or if it contains the same code
         * unit more than once, throw a SyntaxError exception.
         */
        PandaString check_str = ConvertToPandaString(*flags_str_handle, StringConvertedUsage::LOGICOPERATION);
        flags_bits = static_cast<uint8_t>(UpdateExpressionFlags(thread, check_str));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // String -> PandaString
    PandaString pattern_std_str = ConvertToPandaString(*pattern_str_handle, StringConvertedUsage::LOGICOPERATION);
    // 9. 10.
    RegExpParser parser = RegExpParser();
    RegExpParserCache *reg_exp_parser_cache = thread->GetEcmaVM()->GetRegExpParserCache();
    PandaVector<PandaString> group_name;
    auto get_cache = reg_exp_parser_cache->GetCache(*pattern_str_handle, flags_bits, group_name);
    if (get_cache.first == JSTaggedValue::Hole()) {
        parser.Init(const_cast<char *>(reinterpret_cast<const char *>(pattern_std_str.c_str())), pattern_std_str.size(),
                    flags_bits);
        parser.Parse();
        if (parser.IsError()) {
            JSHandle<JSObject> syntax_error =
                factory->GetJSError(base::ErrorType::SYNTAX_ERROR, parser.GetErrorMsg().c_str());
            THROW_NEW_ERROR_AND_RETURN_VALUE(thread, syntax_error.GetTaggedValue(), JSTaggedValue::Exception());
        }
        group_name = parser.GetGroupNames();
    }
    JSHandle<JSRegExp> regexp(thread, JSRegExp::Cast(obj->GetTaggedObject()));
    // 11. Set the value of obj’s [[OriginalSource]] internal slot to P.
    regexp->SetOriginalSource(thread, pattern_str_handle.GetTaggedValue());
    // 12. Set the value of obj’s [[OriginalFlags]] internal slot to F.
    regexp->SetOriginalFlags(thread, JSTaggedValue(flags_bits));
    if (!group_name.empty()) {
        JSHandle<TaggedArray> tagged_array = factory->NewTaggedArray(group_name.size());
        for (size_t i = 0; i < group_name.size(); ++i) {
            JSHandle<JSTaggedValue> flags_key(factory->NewFromString(group_name[i]));
            tagged_array->Set(thread, i, flags_key);
        }
        regexp->SetGroupName(thread, tagged_array);
    }
    // 13. Set obj’s [[RegExpMatcher]] internal slot.
    if (get_cache.first == JSTaggedValue::Hole()) {
        auto buffer_size = parser.GetOriginBufferSize();
        auto buffer = parser.GetOriginBuffer();
        factory->NewJSRegExpByteCodeData(regexp, buffer, buffer_size);
        reg_exp_parser_cache->SetCache(*pattern_str_handle, flags_bits, regexp->GetByteCodeBuffer(), buffer_size,
                                       std::move(group_name));
    } else {
        regexp->SetByteCodeBuffer(thread, get_cache.first);
        regexp->SetLength(thread, JSTaggedValue(static_cast<uint32_t>(get_cache.second)));
    }
    // 14. Let setStatus be Set(obj, "lastIndex", 0, true).
    JSHandle<JSTaggedValue> last_index_string = thread->GlobalConstants()->GetHandledLastIndexString();
    FastRuntimeStub::FastSetPropertyByValue(thread, obj, last_index_string,
                                            thread->GlobalConstants()->GetHandledZero());
    // 15. ReturnIfAbrupt(setStatus).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 16. Return obj.
    return obj.GetTaggedValue();
}

JSTaggedValue reg_exp::RegExpCreate(JSThread *thread, const JSHandle<JSTaggedValue> &pattern,
                                    const JSHandle<JSTaggedValue> &flags)
{
    BUILTINS_API_TRACE(thread, RegExp, RegExpCreate);
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> new_target = env->GetRegExpFunction();
    // 1. Let obj be RegExpAlloc(%RegExp%).
    JSHandle<JSTaggedValue> object(thread, RegExpAlloc(thread, new_target));
    // 2. ReturnIfAbrupt(obj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 3. Return RegExpInitialize(obj, P, F).
    return RegExpInitialize(thread, object, pattern, flags);
}

// 21.2.3.2.4
EcmaString *EscapeRegExpPattern(JSThread *thread, const JSHandle<JSTaggedValue> &src,
                                const JSHandle<JSTaggedValue> &flags)
{
    // String -> PandaString
    JSHandle<EcmaString> src_str(thread, static_cast<EcmaString *>(src->GetTaggedObject()));
    JSHandle<EcmaString> flags_str(thread, static_cast<EcmaString *>(flags->GetTaggedObject()));
    PandaString src_std_str = ConvertToPandaString(*src_str, StringConvertedUsage::LOGICOPERATION);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // "" -> (?:)
    if (src_std_str.empty()) {
        src_std_str = "(?:)";
    }
    // "/" -> "\/"
    src_std_str = base::StringHelper::RepalceAll(src_std_str, "/", "\\/");
    // "\\" -> "\"
    src_std_str = base::StringHelper::RepalceAll(src_std_str, "\\", "\\");

    return *factory->NewFromString(src_std_str);
}

JSTaggedValue RegExpExecResultCache::CreateCacheTable(JSThread *thread)
{
    int length = CACHE_TABLE_HEADER_SIZE + INITIAL_CACHE_NUMBER * ENTRY_SIZE;

    auto table = static_cast<RegExpExecResultCache *>(
        *thread->GetEcmaVM()->GetFactory()->NewTaggedArray(length, JSTaggedValue::Undefined()));
    table->SetLargeStrCount(thread, DEFAULT_LARGE_STRING_COUNT);
    table->SetConflictCount(thread, DEFAULT_CONFLICT_COUNT);
    table->SetStrLenThreshold(thread, 0);
    table->SetHitCount(thread, 0);
    table->SetCacheCount(thread, 0);
    table->SetCacheLength(thread, INITIAL_CACHE_NUMBER);
    return JSTaggedValue(table);
}

JSTaggedValue RegExpExecResultCache::FindCachedResult(JSThread *thread, const JSHandle<JSTaggedValue> &pattern,
                                                      const JSHandle<JSTaggedValue> &flags,
                                                      const JSHandle<JSTaggedValue> &input, CacheType type,
                                                      const JSHandle<JSTaggedValue> &regexp, JSTaggedValue extend)
{
    JSTaggedValue pattern_value = pattern.GetTaggedValue();
    JSTaggedValue flags_value = flags.GetTaggedValue();
    JSTaggedValue input_value = input.GetTaggedValue();

    if (!pattern->IsString() || !flags->IsInt() || !input->IsString()) {
        return JSTaggedValue::Undefined();
    }

    uint32_t hash = pattern->GetKeyHashCode() + static_cast<uint32_t>(flags->GetInt()) + input->GetKeyHashCode();
    uint32_t entry = hash & static_cast<uint32_t>(GetCacheLength() - 1);
    if (!Match(entry, pattern_value, flags_value, input_value, extend)) {
        uint32_t entry2 = (entry + 1) & static_cast<uint32_t>(GetCacheLength() - 1);
        if (!Match(entry2, pattern_value, flags_value, input_value, extend)) {
            return JSTaggedValue::Undefined();
        }
        entry = entry2;
    }
    uint32_t index = CACHE_TABLE_HEADER_SIZE + entry * ENTRY_SIZE;
    JSTaggedValue result;
    switch (type) {
        case REPLACE_TYPE:
            result = Get(index + RESULT_REPLACE_INDEX);
            break;
        case SPLIT_TYPE:
            result = Get(index + RESULT_SPLIT_INDEX);
            break;
        case MATCH_TYPE:
            result = Get(index + RESULT_MATCH_INDEX);
            break;
        case EXEC_TYPE:
            result = Get(index + RESULT_EXEC_INDEX);
            break;
        default:
            UNREACHABLE();
            break;
    }
    SetHitCount(thread, GetHitCount() + 1);
    JSHandle<JSTaggedValue> last_index_handle = thread->GlobalConstants()->GetHandledLastIndexString();
    JSTaggedValue last_index = Get(index + LAST_INDEX_INDEX);  // must be primitive
    FastRuntimeStub::FastSetPropertyByValue(thread, regexp, last_index_handle, HandleFromLocal(&last_index));
    return result;
}

void RegExpExecResultCache::AddResultInCache(JSThread *thread, JSHandle<RegExpExecResultCache> cache,
                                             const JSHandle<JSTaggedValue> &pattern,
                                             const JSHandle<JSTaggedValue> &flags, const JSHandle<JSTaggedValue> &input,
                                             const JSHandle<JSTaggedValue> &result_array, CacheType type,
                                             uint32_t last_index, JSTaggedValue extend)
{
    if (!pattern->IsString() || !flags->IsInt() || !input->IsString()) {
        return;
    }

    JSTaggedValue pattern_value = pattern.GetTaggedValue();
    JSTaggedValue flags_value = flags.GetTaggedValue();
    JSTaggedValue input_value = input.GetTaggedValue();
    JSTaggedValue last_index_value(last_index);

    uint32_t hash =
        pattern_value.GetKeyHashCode() + static_cast<uint32_t>(flags_value.GetInt()) + input_value.GetKeyHashCode();
    uint32_t entry = hash & static_cast<uint32_t>(cache->GetCacheLength() - 1);
    uint32_t index = CACHE_TABLE_HEADER_SIZE + entry * ENTRY_SIZE;
    if (cache->Get(index) == JSTaggedValue::Undefined()) {
        cache->SetCacheCount(thread, cache->GetCacheCount() + 1);
        cache->SetEntry(thread, entry, pattern_value, flags_value, input_value, last_index_value, extend);
        cache->UpdateResultArray(thread, entry, result_array.GetTaggedValue(), type);
    } else if (cache->Match(entry, pattern_value, flags_value, input_value, extend)) {
        cache->UpdateResultArray(thread, entry, result_array.GetTaggedValue(), type);
    } else {
        uint32_t entry2 = (entry + 1) & static_cast<uint32_t>(cache->GetCacheLength() - 1);
        uint32_t index2 = CACHE_TABLE_HEADER_SIZE + entry2 * ENTRY_SIZE;
        JSHandle<JSTaggedValue> extend_handle(thread, extend);
        if (cache->GetCacheLength() < DEFAULT_CACHE_NUMBER) {
            GrowRegexpCache(thread, cache);
            // update value after gc.
            pattern_value = pattern.GetTaggedValue();
            flags_value = flags.GetTaggedValue();
            input_value = input.GetTaggedValue();

            cache->SetCacheLength(thread, DEFAULT_CACHE_NUMBER);
            entry2 = hash & static_cast<uint32_t>(cache->GetCacheLength() - 1);
            index2 = CACHE_TABLE_HEADER_SIZE + entry2 * ENTRY_SIZE;
        }
        JSTaggedValue extend_value = extend_handle.GetTaggedValue();
        if (cache->Get(index2) == JSTaggedValue::Undefined()) {
            cache->SetCacheCount(thread, cache->GetCacheCount() + 1);
            cache->SetEntry(thread, entry2, pattern_value, flags_value, input_value, last_index_value, extend_value);
            cache->UpdateResultArray(thread, entry2, result_array.GetTaggedValue(), type);
        } else if (cache->Match(entry2, pattern_value, flags_value, input_value, extend_value)) {
            cache->UpdateResultArray(thread, entry2, result_array.GetTaggedValue(), type);
        } else {
            cache->SetConflictCount(thread, cache->GetConflictCount() > 1 ? (cache->GetConflictCount() - 1) : 0);
            cache->SetCacheCount(thread, cache->GetCacheCount() - 1);
            cache->ClearEntry(thread, entry2);
            cache->SetEntry(thread, entry, pattern_value, flags_value, input_value, last_index_value, extend_value);
            cache->UpdateResultArray(thread, entry, result_array.GetTaggedValue(), type);
        }
    }
}

void RegExpExecResultCache::GrowRegexpCache(JSThread *thread, JSHandle<RegExpExecResultCache> cache)
{
    int length = CACHE_TABLE_HEADER_SIZE + DEFAULT_CACHE_NUMBER * ENTRY_SIZE;
    auto factory = thread->GetEcmaVM()->GetFactory();
    auto new_cache = factory->ExtendArray(JSHandle<TaggedArray>(cache), length, JSTaggedValue::Undefined());
    thread->GetEcmaVM()->SetRegExpCache(new_cache.GetTaggedValue());
}

void RegExpExecResultCache::SetEntry(JSThread *thread, int entry, JSTaggedValue &pattern, JSTaggedValue &flags,
                                     JSTaggedValue &input, JSTaggedValue &last_index_value, JSTaggedValue &extend_value)
{
    int index = CACHE_TABLE_HEADER_SIZE + entry * ENTRY_SIZE;
    Set(thread, index + PATTERN_INDEX, pattern);
    Set(thread, index + FLAG_INDEX, flags);
    Set(thread, index + INPUT_STRING_INDEX, input);
    Set(thread, index + LAST_INDEX_INDEX, last_index_value);
    Set(thread, index + EXTEND_INDEX, extend_value);
}

void RegExpExecResultCache::UpdateResultArray(JSThread *thread, int entry, JSTaggedValue result_array, CacheType type)
{
    int index = CACHE_TABLE_HEADER_SIZE + entry * ENTRY_SIZE;
    switch (type) {
        break;
        case REPLACE_TYPE:
            Set(thread, index + RESULT_REPLACE_INDEX, result_array);
            break;
        case SPLIT_TYPE:
            Set(thread, index + RESULT_SPLIT_INDEX, result_array);
            break;
        case MATCH_TYPE:
            Set(thread, index + RESULT_MATCH_INDEX, result_array);
            break;
        case EXEC_TYPE:
            Set(thread, index + RESULT_EXEC_INDEX, result_array);
            break;
        default:
            UNREACHABLE();
            break;
    }
}

void RegExpExecResultCache::ClearEntry(JSThread *thread, int entry)
{
    int index = CACHE_TABLE_HEADER_SIZE + entry * ENTRY_SIZE;
    JSTaggedValue undefined = JSTaggedValue::Undefined();
    for (int i = 0; i < ENTRY_SIZE; i++) {
        Set(thread, index + i, undefined);
    }
}
bool RegExpExecResultCache::Match(int entry, JSTaggedValue &pattern, JSTaggedValue &flags, JSTaggedValue &input,
                                  JSTaggedValue &extend)
{
    int index = CACHE_TABLE_HEADER_SIZE + entry * ENTRY_SIZE;
    JSTaggedValue key_pattern = Get(index + PATTERN_INDEX);
    JSTaggedValue key_flags = Get(index + FLAG_INDEX);
    JSTaggedValue key_input = Get(index + INPUT_STRING_INDEX);
    JSTaggedValue key_extend = Get(index + EXTEND_INDEX);

    if (key_pattern == JSTaggedValue::Undefined()) {
        return false;
    }

    EcmaString *pattern_str = EcmaString::Cast(pattern.GetTaggedObject());
    uint8_t flags_bits = flags.GetInt();
    EcmaString *input_str = EcmaString::Cast(input.GetTaggedObject());
    EcmaString *key_pattern_str = EcmaString::Cast(key_pattern.GetTaggedObject());
    uint8_t key_flags_bits = key_flags.GetInt();
    EcmaString *key_input_str = EcmaString::Cast(key_input.GetTaggedObject());
    bool extend_equal = false;
    if (extend.IsString() && key_extend.IsString()) {
        EcmaString *extend_str = EcmaString::Cast(extend.GetTaggedObject());
        EcmaString *key_extend_str = EcmaString::Cast(key_extend.GetTaggedObject());
        extend_equal = EcmaString::StringsAreEqual(extend_str, key_extend_str);
    } else if (extend.IsUndefined() && key_extend.IsUndefined()) {
        extend_equal = true;
    } else {
        return false;
    }
    return EcmaString::StringsAreEqual(pattern_str, key_pattern_str) && flags_bits == key_flags_bits &&
           EcmaString::StringsAreEqual(input_str, key_input_str) && extend_equal;
}
}  // namespace panda::ecmascript::builtins
