/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/jobs/micro_job_queue.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {

static JSHandle<CompletionRecord> PerformPromiseAll(JSThread *thread, const JSHandle<PromiseIteratorRecord> &it_record,
                                                    const JSHandle<JSTaggedValue> &ctor,
                                                    const JSHandle<PromiseCapability> &capa);

static JSHandle<CompletionRecord> PerformPromiseRace(JSThread *thread,
                                                     const JSHandle<PromiseIteratorRecord> &iterator_record,
                                                     const JSHandle<PromiseCapability> &capability,
                                                     const JSHandle<JSTaggedValue> &constructor);

// 25.4.3.1 Promise ( executor )
JSTaggedValue promise::PromiseConstructor([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Promise, PromiseConstructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 1. If NewTarget is undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "PromiseConstructor: NewTarget is undefined", JSTaggedValue::Exception());
    }
    // 2. If IsCallable(executor) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> executor = builtins_common::GetCallArg(argv, 0);
    if (!executor->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "PromiseConstructor: executor is not callable", JSTaggedValue::Exception());
    }

    // 3. Let promise be OrdinaryCreateFromConstructor(NewTarget, "%PromisePrototype%",
    // «[[PromiseState]], [[PromiseResult]], [[PromiseFulfillReactions]], [[PromiseRejectReactions]]» ).
    // 4. ReturnIfAbrupt(promise).
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSPromise> instance_promise =
        JSHandle<JSPromise>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Set promise's [[PromiseState]] internal slot to "pending".
    // 6. Set promise's [[PromiseFulfillReactions]] internal slot to a new empty List.
    // 7. Set promise's [[PromiseRejectReactions]] internal slot to a new empty List.
    // 8. Let resolvingFunctions be CreateResolvingFunctions(promise).
    JSHandle<ResolvingFunctionsRecord> resolving_function =
        JSPromise::CreateResolvingFunctions(thread, instance_promise);
    // 9. Let completion be Call(executor, undefined, «resolvingFunctions.[[Resolve]], resolvingFunctions.[[reject]])
    auto resolve_func = resolving_function->GetResolveFunction();
    auto reject_func = resolving_function->GetRejectFunction();

    JSTaggedValue tagged_value;
    JSHandle<JSTaggedValue> this_value = global_const->GetHandledUndefined();
    {
        auto info = NewRuntimeCallInfo(thread, executor, this_value, JSTaggedValue::Undefined(), 2);
        info->SetCallArgs(resolve_func, reject_func);
        tagged_value = JSFunction::Call(info.Get());  // 2: two args
    }
    JSHandle<JSTaggedValue> completion_value(thread, tagged_value);

    // 10. If completion is an abrupt completion, then
    // a. Let status be Call(resolvingFunctions.[[Reject]], undefined, «completion.[[value]]»).
    // b. ReturnIfAbrupt(status).
    if (UNLIKELY(thread->HasPendingException())) {
        completion_value = JSPromise::IfThrowGetThrowValue(thread);
        thread->ClearException();
        JSHandle<JSTaggedValue> reject(thread, resolving_function->GetRejectFunction());
        auto info = NewRuntimeCallInfo(thread, reject, this_value, JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(completion_value);
        JSFunction::Call(info.Get());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    // 11. Return promise.
    return instance_promise.GetTaggedValue();
}

// 25.4.4.1 Promise.all ( iterable )
JSTaggedValue promise::All(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Promise, All);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. Let C be the this value.
    JSHandle<JSTaggedValue> ctor = builtins_common::GetThis(argv);
    // 2. If Type(C) is not Object, throw a TypeError exception.
    if (!ctor->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Promise ALL: this value is not object", JSTaggedValue::Exception());
    }
    // 3. Let S be Get(C, @@species).
    // 4. ReturnIfAbrupt(S).
    JSHandle<JSTaggedValue> species_symbol = env->GetSpeciesSymbol();
    JSHandle<JSTaggedValue> sctor = JSObject::GetProperty(thread, ctor, species_symbol).GetValue();
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, sctor.GetTaggedValue());

    // 5. If S is neither undefined nor null, let C be S.
    if (!sctor->IsUndefined() && !sctor->IsNull()) {
        ctor = sctor;
    }
    // 6. Let promiseCapability be NewPromiseCapability(C).
    JSHandle<PromiseCapability> capa = JSPromise::NewPromiseCapability(thread, ctor);
    // 7. ReturnIfAbrupt(promiseCapability).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, capa.GetTaggedValue());
    // 8. Let iterator be GetIterator(iterable).
    JSHandle<JSTaggedValue> itor = JSIterator::GetIterator(thread, builtins_common::GetCallArg(argv, 0));
    // 9. IfAbruptRejectPromise(iterator, promiseCapability).
    if (UNLIKELY(thread->HasPendingException())) {
        itor = JSPromise::IfThrowGetThrowValue(thread);
    }
    RETURN_REJECT_PROMISE_IF_ABRUPT(thread, itor, capa);

    // 10. Let iteratorRecord be Record {[[iterator]]: iterator, [[done]]: false}.
    JSHandle<JSTaggedValue> done(thread, JSTaggedValue::False());
    JSHandle<PromiseIteratorRecord> it_record = factory->NewPromiseIteratorRecord(itor, done);
    // 11. Let result be PerformPromiseAll(iteratorRecord, C, promiseCapability).
    JSHandle<CompletionRecord> result = PerformPromiseAll(thread, it_record, ctor, capa);
    // 12. If result is an abrupt completion,
    if (result->IsThrow()) {
        thread->ClearException();
        // a. If iteratorRecord.[[done]] is false, let result be IteratorClose(iterator, result).
        // b. IfAbruptRejectPromise(result, promiseCapability).
        if (it_record->GetDone().IsFalse()) {
            JSHandle<JSTaggedValue> close_val =
                JSIterator::IteratorClose(thread, itor, JSHandle<JSTaggedValue>::Cast(result));
            if (close_val.GetTaggedValue().IsRecord()) {
                result = JSHandle<CompletionRecord>::Cast(close_val);
                RETURN_REJECT_PROMISE_IF_ABRUPT(thread, result, capa);
                return result->GetValue();
            }
        }
        RETURN_REJECT_PROMISE_IF_ABRUPT(thread, result, capa);
        return result->GetValue();
    }
    // 13. Return Completion(result).
    return result->GetValue();
}

// 25.4.4.3 Promise.race ( iterable )
JSTaggedValue promise::Race(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Promise, Race);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    // 1. Let C be the this value.
    // 2. If Type(C) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    if (!this_value->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Race: this value is not object", JSTaggedValue::Exception());
    }
    // 3. Let S be Get(C, @@species).
    // 4. ReturnIfAbrupt(S).
    // 5. If S is neither undefined nor null, let C be S.
    JSHandle<JSTaggedValue> species_symbol = env->GetSpeciesSymbol();
    JSHandle<JSTaggedValue> species_constructor = JSObject::GetProperty(thread, this_value, species_symbol).GetValue();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (!(species_constructor->IsUndefined() || species_constructor->IsNull())) {
        this_value = species_constructor;
    }

    // 6. Let promiseCapability be NewPromiseCapability(C).
    // 7. ReturnIfAbrupt(promiseCapability).
    JSHandle<PromiseCapability> promise_capability = JSPromise::NewPromiseCapability(thread, this_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 8. Let iterator be GetIterator(iterable).
    // 9. IfAbruptRejectPromise(iterator, promiseCapability).
    JSHandle<JSTaggedValue> iterable = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> iterator = JSIterator::GetIterator(thread, iterable);
    if (UNLIKELY(thread->HasPendingException())) {
        iterator = JSPromise::IfThrowGetThrowValue(thread);
    }
    RETURN_REJECT_PROMISE_IF_ABRUPT(thread, iterator, promise_capability);

    // 10. Let iteratorRecord be Record {[[iterator]]: iterator, [[done]]: false}.
    JSHandle<JSTaggedValue> done(thread, JSTaggedValue::False());
    JSHandle<PromiseIteratorRecord> iterator_record = factory->NewPromiseIteratorRecord(iterator, done);

    // 11. Let result be PerformPromiseRace(iteratorRecord, promiseCapability, C).
    // 12. If result is an abrupt completion, then
    //     a. If iteratorRecord.[[done]] is false, let result be IteratorClose(iterator,result).
    //     b. IfAbruptRejectPromise(result, promiseCapability).
    // 13. Return Completion(result).
    JSHandle<CompletionRecord> result = PerformPromiseRace(thread, iterator_record, promise_capability, this_value);
    if (result->IsThrow()) {
        thread->ClearException();
        if (iterator_record->GetDone().IsFalse()) {
            JSHandle<JSTaggedValue> value =
                JSIterator::IteratorClose(thread, iterator, JSHandle<JSTaggedValue>::Cast(result));
            if (value.GetTaggedValue().IsCompletionRecord()) {
                result = JSHandle<CompletionRecord>(value);
                RETURN_REJECT_PROMISE_IF_ABRUPT(thread, result, promise_capability);
                return result->GetValue();
            }
        }
        RETURN_REJECT_PROMISE_IF_ABRUPT(thread, result, promise_capability);
        return result->GetValue();
    }
    return result->GetValue();
}

// 25.4.4.5 Promise.resolve ( x )
JSTaggedValue promise::Resolve(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Promise, Resolve);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let C be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If Type(C) is not Object, throw a TypeError exception.
    if (!this_value->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Resolve: this value is not object", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> x_value = builtins_common::GetCallArg(argv, 0);
    return JSPromise::PromiseResolve(thread, this_value, x_value);
}

// 25.4.4.4 Promise.reject ( r )
JSTaggedValue promise::Reject(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), Promise, Reject);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    // 1. Let C be the this value.
    // 2. If Type(C) is not Object, throw a TypeError exception.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    if (!this_value->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Reject: this value is not object", JSTaggedValue::Exception());
    }

    // 3. Let promiseCapability be NewPromiseCapability(C).
    // 4. ReturnIfAbrupt(promiseCapability).
    JSHandle<PromiseCapability> promise_capability = JSPromise::NewPromiseCapability(thread, this_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Let rejectResult be Call(promiseCapability.[[Reject]], undefined, «r»).
    // 6. ReturnIfAbrupt(rejectResult).
    JSHandle<JSTaggedValue> reason = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> reject(thread, promise_capability->GetReject());
    JSHandle<JSTaggedValue> undefined = global_const->GetHandledUndefined();

    auto info = NewRuntimeCallInfo(thread, reject, undefined, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(reason);
    JSFunction::Call(info.Get());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 7. Return promiseCapability.[[Promise]].
    JSHandle<JSObject> promise(thread, promise_capability->GetPromise());
    return promise.GetTaggedValue();
}

// 25.4.4.6 get Promise [ @@species ]
JSTaggedValue promise::GetSpecies([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    return JSTaggedValue(builtins_common::GetThis(argv).GetTaggedValue());
}

// 25.4.5.1 Promise.prototype.catch ( onRejected )
JSTaggedValue promise::proto::Catch(EcmaRuntimeCallInfo *argv)
{
    // 1. Let promise be the this value.
    // 2. Return Invoke(promise, "then", «undefined, onRejected»).
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromisePrototype, Catch);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> promise = builtins_common::GetThis(argv);
    JSHandle<JSTaggedValue> then_key = global_const->GetHandledPromiseThenString();
    JSHandle<JSTaggedValue> reject = builtins_common::GetCallArg(argv, 0);

    auto info = NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), promise, JSTaggedValue::Undefined(), 2);
    info->SetCallArgs(JSTaggedValue::Undefined(), reject);
    return JSFunction::Invoke(info.Get(), then_key);  // 2: two args
}

// 25.4.5.3 Promise.prototype.then ( onFulfilled , onRejected )
JSTaggedValue promise::proto::Then(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromisePrototype, Then);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    // 1. Let promise be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If IsPromise(promise) is false, throw a TypeError exception.
    if (!this_value->IsJSPromise()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Then: thisValue is not promise!", JSTaggedValue::Exception());
    }
    // 3. Let C be SpeciesConstructor(promise, %Promise%).
    // 4. ReturnIfAbrupt(C).
    JSHandle<JSObject> promise = JSHandle<JSObject>::Cast(this_value);
    JSHandle<JSTaggedValue> default_func = JSHandle<JSTaggedValue>::Cast(env->GetPromiseFunction());
    JSHandle<JSTaggedValue> constructor = JSObject::SpeciesConstructor(thread, promise, default_func);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Let resultCapability be NewPromiseCapability(C).
    // 6. ReturnIfAbrupt(resultCapability).
    JSHandle<PromiseCapability> result_capability = JSPromise::NewPromiseCapability(thread, constructor);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSTaggedValue> on_fulfilled = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> on_rejected = builtins_common::GetCallArg(argv, 1);

    // 7. Return PerformPromiseThen(promise, onFulfilled, onRejected, resultCapability).
    return promise::PerformPromiseThen(thread, JSHandle<JSPromise>::Cast(promise), on_fulfilled, on_rejected,
                                       JSHandle<JSTaggedValue>::Cast(result_capability));
}

JSTaggedValue promise::PerformPromiseThen(JSThread *thread, const JSHandle<JSPromise> &promise,
                                          const JSHandle<JSTaggedValue> &on_fulfilled,
                                          const JSHandle<JSTaggedValue> &on_rejected,
                                          const JSHandle<JSTaggedValue> &capability)
{
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<job::MicroJobQueue> job = ecma_vm->GetMicroJobQueue();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSMutableHandle<JSTaggedValue> fulfilled(thread, on_fulfilled.GetTaggedValue());
    auto global_const = thread->GlobalConstants();
    if (!fulfilled->IsCallable()) {
        fulfilled.Update(global_const->GetIdentityString());
    }
    JSMutableHandle<JSTaggedValue> rejected(thread, on_rejected.GetTaggedValue());
    if (!rejected->IsCallable()) {
        rejected.Update(global_const->GetThrowerString());
    }
    JSHandle<PromiseReaction> fulfill_reaction = factory->NewPromiseReaction();
    fulfill_reaction->SetPromiseCapability(thread, capability.GetTaggedValue());
    fulfill_reaction->SetHandler(thread, fulfilled.GetTaggedValue());

    JSHandle<PromiseReaction> reject_reaction = factory->NewPromiseReaction();
    reject_reaction->SetPromiseCapability(thread, capability.GetTaggedValue());
    reject_reaction->SetHandler(thread, rejected.GetTaggedValue());

    if (JSTaggedValue::SameValue(promise->GetPromiseState(),
                                 JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING)))) {
        JSHandle<TaggedQueue> fulfill_reactions(thread, promise->GetPromiseFulfillReactions());
        TaggedQueue *new_queue =
            TaggedQueue::Push(thread, fulfill_reactions, JSHandle<JSTaggedValue>::Cast(fulfill_reaction));
        promise->SetPromiseFulfillReactions(thread, JSTaggedValue(new_queue));

        JSHandle<TaggedQueue> reject_reactions(thread, promise->GetPromiseRejectReactions());
        new_queue = TaggedQueue::Push(thread, reject_reactions, JSHandle<JSTaggedValue>::Cast(reject_reaction));
        promise->SetPromiseRejectReactions(thread, JSTaggedValue(new_queue));
    } else if (JSTaggedValue::SameValue(promise->GetPromiseState(),
                                        JSTaggedValue(static_cast<int32_t>(PromiseStatus::FULFILLED)))) {
        JSHandle<TaggedArray> argv = factory->NewTaggedArray(2);  // 2: 2 means two args stored in array
        argv->Set(thread, 0, fulfill_reaction.GetTaggedValue());
        argv->Set(thread, 1, promise->GetPromiseResult());

        JSHandle<JSFunction> promise_reactions_job(env->GetPromiseReactionJob());
        job::MicroJobQueue::EnqueueJob(thread, job, job::QueueType::QUEUE_PROMISE, promise_reactions_job, argv);
    } else if (JSTaggedValue::SameValue(promise->GetPromiseState(),
                                        JSTaggedValue(static_cast<int32_t>(PromiseStatus::REJECTED)))) {
        JSHandle<TaggedArray> argv = factory->NewTaggedArray(2);  // 2: 2 means two args stored in array
        argv->Set(thread, 0, reject_reaction.GetTaggedValue());
        argv->Set(thread, 1, promise->GetPromiseResult());
        // When a handler is added to a rejected promise for the first time, it is called with its operation
        // argument set to "handle".
        if (!promise->GetPromiseIsHandled().ToBoolean()) {
            JSHandle<JSTaggedValue> reason(thread, JSTaggedValue::Null());
            thread->GetEcmaVM()->PromiseRejectionTracker(promise, reason, ecmascript::PromiseRejectionEvent::HANDLE);
        }
        JSHandle<JSFunction> promise_reactions_job(env->GetPromiseReactionJob());
        job::MicroJobQueue::EnqueueJob(thread, job, job::QueueType::QUEUE_PROMISE, promise_reactions_job, argv);
    }

    promise->SetPromiseIsHandled(thread, JSTaggedValue::True());

    if (capability->IsUndefined()) {
        return capability.GetTaggedValue();
    }

    return PromiseCapability::Cast(capability->GetHeapObject())->GetPromise();
}

JSHandle<CompletionRecord> PerformPromiseAll(JSThread *thread, const JSHandle<PromiseIteratorRecord> &it_record,
                                             const JSHandle<JSTaggedValue> &ctor,
                                             const JSHandle<PromiseCapability> &capa)
{
    auto ecma_vm = thread->GetEcmaVM();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    ObjectFactory *factory = ecma_vm->GetFactory();
    // 1. Assert: constructor is a constructor function.
    ASSERT_PRINT(ctor->IsConstructor(), "PerformPromiseAll is not constructor");
    // 2. Assert: resultCapability is a PromiseCapability record. (not need)
    // 3. Let values be a new empty List.
    JSHandle<PromiseRecord> values = factory->NewPromiseRecord();
    JSHandle<TaggedArray> empty_array = factory->EmptyArray();
    values->SetValue(thread, empty_array);
    // 4. Let remainingElementsCount be a new Record { [[value]]: 1 }.
    JSHandle<PromiseRecord> remain_cnt = factory->NewPromiseRecord();
    remain_cnt->SetValue(thread, JSTaggedNumber(1));
    // 5. Let index be 0.
    uint32_t index = 0;
    // 6. Repeat
    JSHandle<JSTaggedValue> itor(thread, it_record->GetIterator());
    JSMutableHandle<JSTaggedValue> next(thread, global_const->GetUndefined());
    while (true) {
        // a. Let next be IteratorStep(iteratorRecord.[[iterator]]).
        next.Update(JSIterator::IteratorStep(thread, itor).GetTaggedValue());
        // b. If next is an abrupt completion, set iteratorRecord.[[done]] to true.
        if (UNLIKELY(thread->HasPendingException())) {
            it_record->SetDone(thread, JSTaggedValue::True());
            next.Update(JSPromise::IfThrowGetThrowValue(thread).GetTaggedValue());
        }
        // c. ReturnIfAbrupt(next).
        RETURN_COMPLETION_IF_ABRUPT(thread, next);
        // d. If next is false,
        if (next->IsFalse()) {
            // i. Set iteratorRecord.[[done]] to true.
            it_record->SetDone(thread, JSTaggedValue::True());
            // ii. Set remainingElementsCount.[[value]] to remainingElementsCount.[[value]] − 1.
            remain_cnt->SetValue(thread, --JSTaggedNumber(remain_cnt->GetValue()));
            // iii. If remainingElementsCount.[[value]] is 0,
            if (remain_cnt->GetValue().IsZero()) {
                // 1. Let valuesArray be CreateArrayFromList(values).
                JSHandle<JSArray> js_array_values =
                    JSArray::CreateArrayFromList(thread, JSHandle<TaggedArray>(thread, values->GetValue()));
                // 2. Let resolveResult be Call(resultCapability.[[Resolve]], undefined, «valuesArray»).
                JSHandle<JSTaggedValue> res_capa_func(thread, capa->GetResolve());

                auto info = NewRuntimeCallInfo(thread, res_capa_func, global_const->GetHandledUndefined(),
                                               JSTaggedValue::Undefined(), 1);
                info->SetCallArgs(js_array_values.GetTaggedValue());
                JSTaggedValue resolve_res = JSFunction::Call(info.Get());
                // 3. ReturnIfAbrupt(resolveResult)
                JSHandle<JSTaggedValue> resolve_abrupt(thread, resolve_res);
                RETURN_COMPLETION_IF_ABRUPT(thread, resolve_abrupt);
            }
            // iv. Return resultCapability.[[Promise]].
            JSHandle<CompletionRecord> res_record = factory->NewCompletionRecord(
                CompletionRecord::NORMAL, JSHandle<JSTaggedValue>(thread, capa->GetPromise()));
            return res_record;
        }
        // e. Let nextValue be IteratorValue(next).
        JSHandle<JSTaggedValue> next_val = JSIterator::IteratorValue(thread, next);
        // f. If nextValue is an abrupt completion, set iteratorRecord.[[done]] to true.
        if (UNLIKELY(thread->HasPendingException())) {
            it_record->SetDone(thread, JSTaggedValue::True());
            if (thread->GetException().IsObjectWrapper()) {
                JSHandle<ObjectWrapper> wrapper_val(thread, thread->GetException());
                JSHandle<JSTaggedValue> throw_val(thread, wrapper_val->GetValue());
                next_val = throw_val;
            } else {
                next_val = JSHandle<JSTaggedValue>(thread, thread->GetException());
            }
        }

        // g. ReturnIfAbrupt(nextValue).
        RETURN_COMPLETION_IF_ABRUPT(thread, next_val);
        // h. Append undefined to values.
        JSHandle<TaggedArray> values_array =
            JSHandle<TaggedArray>::Cast(JSHandle<JSTaggedValue>(thread, values->GetValue()));
        values_array = TaggedArray::SetCapacity(thread, values_array, index + 1);
        values_array->Set(thread, index, JSTaggedValue::Undefined());
        values->SetValue(thread, values_array);
        // i. Let nextPromise be Invoke(constructor, "resolve", «‍nextValue»).
        JSHandle<JSTaggedValue> resolve_key = global_const->GetHandledPromiseResolveString();
        JSTaggedValue tagged_next_promise;
        {
            auto info = NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), ctor, JSTaggedValue::Undefined(), 1);
            info->SetCallArgs(next_val);
            tagged_next_promise = JSFunction::Invoke(info.Get(), resolve_key);
        }
        // j. ReturnIfAbrupt(nextPromise).
        JSHandle<JSTaggedValue> next_promise(thread, tagged_next_promise);
        RETURN_COMPLETION_IF_ABRUPT(thread, next_promise);
        // k. Let resolveElement be a new built-in function object as defined in Promise.all
        //    Resolve Element Functions.
        JSHandle<JSPromiseAllResolveElementFunction> resoleve_element = factory->NewJSPromiseAllResolveElementFunction(
            reinterpret_cast<void *>(promise_handler::ResolveElementFunction));
        // l. Set the [[AlreadyCalled]] internal slot of resolveElement to a new Record {[[value]]: false }.
        JSHandle<PromiseRecord> false_record = factory->NewPromiseRecord();
        false_record->SetValue(thread, JSTaggedValue::False());
        resoleve_element->SetAlreadyCalled(thread, false_record);
        // m. Set the [[Index]] internal slot of resolveElement to index.
        resoleve_element->SetIndex(thread, JSTaggedValue(index));
        // n. Set the [[Values]] internal slot of resolveElement to values.
        resoleve_element->SetValues(thread, values);
        // o. Set the [[Capabilities]] internal slot of resolveElement to resultCapability.
        resoleve_element->SetCapabilities(thread, capa);
        // p. Set the [[RemainingElements]] internal slot of resolveElement to remainingElementsCount.
        resoleve_element->SetRemainingElements(thread, remain_cnt);
        // q. Set remainingElementsCount.[[value]] to remainingElementsCount.[[value]] + 1.
        remain_cnt->SetValue(thread, ++JSTaggedNumber(remain_cnt->GetValue()));
        // r. Let result be Invoke(nextPromise, "then", «‍resolveElement, resultCapability.[[Reject]]»).
        JSTaggedValue tagged_result;
        {
            JSHandle<JSTaggedValue> then_key = global_const->GetHandledPromiseThenString();
            auto info =
                NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), next_promise, JSTaggedValue::Undefined(), 2);
            info->SetCallArgs(resoleve_element, capa->GetReject());
            tagged_result = JSFunction::Invoke(info.Get(), then_key);
        }

        JSHandle<JSTaggedValue> result(thread, tagged_result);
        // s. ReturnIfAbrupt(result).
        RETURN_COMPLETION_IF_ABRUPT(thread, result);
        // t. Set index to index + 1.
        ++index;
    }
}

JSHandle<CompletionRecord> PerformPromiseRace(JSThread *thread, const JSHandle<PromiseIteratorRecord> &iterator_record,
                                              const JSHandle<PromiseCapability> &capability,
                                              const JSHandle<JSTaggedValue> &constructor)
{
    // 1. Repeat
    //    a. Let next be IteratorStep(iteratorRecord.[[iterator]]).
    //    b. If next is an abrupt completion, set iteratorRecord.[[done]] to true.
    //    c. ReturnIfAbrupt(next).
    //    d. If next is false, then
    //       i. Set iteratorRecord.[[done]] to true.
    //       ii. Return promiseCapability.[[Promise]].
    //    e. Let nextValue be IteratorValue(next).
    //    f. If nextValue is an abrupt completion, set iteratorRecord.[[done]] to true.
    //    g. ReturnIfAbrupt(nextValue).
    //    h. Let nextPromise be Invoke(C, "resolve", «nextValue»).
    //    i. ReturnIfAbrupt(nextPromise).
    //    j. Let result be Invoke(nextPromise, "then", «promiseCapability.[[Resolve]], promiseCapability.[[Reject]]»).
    //    k. ReturnIfAbrupt(result).
    auto ecma_vm = thread->GetEcmaVM();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<JSTaggedValue> iterator(thread, iterator_record->GetIterator());
    JSMutableHandle<JSTaggedValue> next(thread, global_const->GetUndefined());
    while (true) {
        next.Update(JSIterator::IteratorStep(thread, iterator).GetTaggedValue());
        if (UNLIKELY(thread->HasPendingException())) {
            iterator_record->SetDone(thread, JSTaggedValue::True());
            next.Update(JSPromise::IfThrowGetThrowValue(thread).GetTaggedValue());
        }
        RETURN_COMPLETION_IF_ABRUPT(thread, next);
        if (next->IsFalse()) {
            iterator_record->SetDone(thread, JSTaggedValue::True());
            JSHandle<JSTaggedValue> promise(thread, capability->GetPromise());
            JSHandle<CompletionRecord> completion_record =
                factory->NewCompletionRecord(CompletionRecord::NORMAL, promise);
            return completion_record;
        }
        JSHandle<JSTaggedValue> next_value = JSIterator::IteratorValue(thread, next);
        if (UNLIKELY(thread->HasPendingException())) {
            iterator_record->SetDone(thread, JSTaggedValue::True());
            next_value = JSPromise::IfThrowGetThrowValue(thread);
        }
        RETURN_COMPLETION_IF_ABRUPT(thread, next_value);
        JSHandle<JSTaggedValue> resolve_str = global_const->GetHandledPromiseResolveString();

        JSTaggedValue result;
        {
            auto info =
                NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), constructor, JSTaggedValue::Undefined(), 1);
            info->SetCallArgs(next_value);
            result = JSFunction::Invoke(info.Get(), resolve_str);
        }

        JSHandle<JSTaggedValue> next_promise(thread, result);
        if (UNLIKELY(thread->HasPendingException())) {
            next_promise = JSPromise::IfThrowGetThrowValue(thread);
        }
        RETURN_COMPLETION_IF_ABRUPT(thread, next_promise);

        {
            JSHandle<JSTaggedValue> then_str = global_const->GetHandledPromiseThenString();
            auto info =
                NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), next_promise, JSTaggedValue::Undefined(), 2);
            info->SetCallArgs(capability->GetResolve(), capability->GetReject());
            result = JSFunction::Invoke(info.Get(), then_str);  // 2: two args
        }

        JSHandle<JSTaggedValue> handle_result(thread, result);
        if (UNLIKELY(thread->HasPendingException())) {
            handle_result = JSPromise::IfThrowGetThrowValue(thread);
        }
        RETURN_COMPLETION_IF_ABRUPT(thread, handle_result);
    }
}
}  // namespace panda::ecmascript::builtins
