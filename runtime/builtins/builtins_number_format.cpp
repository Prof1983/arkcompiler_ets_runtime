/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_intl.h"
#include "plugins/ecmascript/runtime/js_locale.h"
#include "plugins/ecmascript/runtime/js_number_format.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {

static JSTaggedValue NumberFormatInternalFormatNumber(EcmaRuntimeCallInfo *argv);

// 13.2.1 Intl.NumberFormat  ( [ locales [ , options ] ] )
JSTaggedValue number_format::NumberFormatConstructor(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. If NewTarget is undefined, let new_target be the active function object, else let new_target be NewTarget.
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        new_target = constructor;
    }

    // Let numberFormat be ? OrdinaryCreateFromConstructor(new_target, "%NumberFormatPrototype%",
    // « [[InitializedNumberFormat]], [[Locale]], [[DataLocale]], [[NumberingSystem]], [[Style]], [[Unit]],
    // [[UnitDisplay]], [[Currency]], [[CurrencyDisplay]], [[CurrencySign]], [[MinimumIntegerDigits]],
    // [[MinimumFractionDigits]], [[MaximumFractionDigits]], [[MinimumSignificantDigits]], [[MaximumSignificantDigits]],
    // [[RoundingType]], [[Notation]], [[CompactDisplay]], [[UseGrouping]], [[SignDisplay]], [[BoundFormat]] »).
    JSHandle<JSNumberFormat> number_format = JSHandle<JSNumberFormat>::Cast(
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Perform ? InitializeNumberFormat(numberFormat, locales, options).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSNumberFormat::InitializeNumberFormat(thread, number_format, locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4. Let this be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 5. If NewTarget is undefined and Type(this) is Object and ? InstanceofOperator(this, %NumberFormat%) is true,
    //    then
    //    a. Perform ? DefinePropertyOrThrow(this, %Intl%.[[FallbackSymbol]], PropertyDescriptor{
    //       [[Value]]: numberFormat, [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: false }).
    //    b. Return this.
    bool is_instance_of = JSObject::InstanceOf(thread, this_value, env->GetNumberFormatFunction());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (new_target->IsUndefined() && this_value->IsJSObject() && is_instance_of) {
        PropertyDescriptor descriptor(thread, JSHandle<JSTaggedValue>::Cast(number_format), false, false, false);
        JSHandle<JSTaggedValue> key(thread, JSHandle<JSIntl>::Cast(env->GetIntlFunction())->GetFallbackSymbol());
        JSTaggedValue::DefinePropertyOrThrow(thread, this_value, key, descriptor);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        return this_value.GetTaggedValue();
    }

    // 6. Return numberFormat.
    return number_format.GetTaggedValue();
}

// 13.3.2 Intl.NumberFormat.supportedLocalesOf ( locales [ , options ] )
JSTaggedValue number_format::SupportedLocalesOf(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let availableLocales be %NumberFormat%.[[AvailableLocales]].
    JSHandle<TaggedArray> available_locales = JSNumberFormat::GetAvailableLocales(thread);

    // 2. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Return ? SupportedLocales(availableLocales, requestedLocales, options).
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSArray> result = JSLocale::SupportedLocales(thread, available_locales, requested_locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 13.4.3 get Intl.NumberFormat.prototype.format
JSTaggedValue number_format::proto::GetFormat(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let nf be this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If Type(nf) is not Object, throw a TypeError exception.
    if (!this_value->IsJSObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "nf is not object", JSTaggedValue::Exception());
    }
    // 3. Let nf be ? UnwrapNumberFormat(nf).
    JSHandle<JSTaggedValue> nf = JSNumberFormat::UnwrapNumberFormat(thread, this_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (nf->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "nf is not object", JSTaggedValue::Exception());
    }

    JSHandle<JSNumberFormat> typped_nf = JSHandle<JSNumberFormat>::Cast(nf);
    JSHandle<JSTaggedValue> bound_func(thread, typped_nf->GetBoundFormat());
    // 4. If nf.[[BoundFormat]] is undefined, then
    //      a. Let F be a new built-in function object as defined in Number Format Functions (12.1.4).
    //      b. Set F.[[NumberFormat]] to nf.
    //      c. Set nf.[[BoundFormat]] to F.
    if (bound_func->IsUndefined()) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        JSHandle<JSIntlBoundFunction> intl_bound_func =
            factory->NewJSIntlBoundFunction(reinterpret_cast<void *>(NumberFormatInternalFormatNumber));
        intl_bound_func->SetNumberFormat(thread, typped_nf);
        typped_nf->SetBoundFormat(thread, intl_bound_func);
    }
    return typped_nf->GetBoundFormat();
}

// 13.4.4 Intl.NumberFormat.prototype.formatToParts ( date )
JSTaggedValue number_format::proto::FormatToParts(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let nf be the this value.
    JSHandle<JSTaggedValue> nf = builtins_common::GetThis(argv);
    // 2. Perform ? RequireInternalSlot(nf, [[InitializedNumberFormat]]).
    if (!nf->IsJSNumberFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", JSTaggedValue::Exception());
    }
    // 3. Let x be ? ToNumeric(value).
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber x = JSTaggedValue::ToNumber(thread, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSArray> result = JSNumberFormat::FormatNumericToParts(thread, JSHandle<JSNumberFormat>::Cast(nf), x);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return result.GetTaggedValue();
}
// 13.4.5 Intl.NumberFormat.prototype.resolvedOptions ()
JSTaggedValue number_format::proto::ResolvedOptions(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    // 1. Let nf be this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If Type(nf) is not Object, throw a TypeError exception.
    if (!this_value->IsJSObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not object", JSTaggedValue::Exception());
    }
    // 3. Let nf be ? UnwrapNumberFormat(nf).
    JSHandle<JSTaggedValue> nf = JSNumberFormat::UnwrapNumberFormat(thread, this_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 4. Let options be ! ObjectCreate(%ObjectPrototype%).
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<JSTaggedValue> ctor = env->GetObjectFunction();
    JSHandle<JSObject> options(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), ctor));

    // 5. For each row of Table 5, except the header row, in table order, do
    //  Let p be the Property value of the current row.
    //  Let v be the value of nf's internal slot whose name is the Internal Slot value of the current row.
    //  If v is not undefined, then
    //  Perform ! CreateDataPropertyOrThrow(options, p, v).
    JSNumberFormat::ResolvedOptions(thread, JSHandle<JSNumberFormat>::Cast(nf), options);
    return options.GetTaggedValue();
}

JSTaggedValue NumberFormatInternalFormatNumber(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSIntlBoundFunction> intl_bound_func =
        JSHandle<JSIntlBoundFunction>::Cast(builtins_common::GetConstructor(argv));

    // 1. Let nf be F.[[NumberFormat]].
    JSHandle<JSTaggedValue> nf(thread, intl_bound_func->GetNumberFormat());
    // 2. Assert: Type(nf) is Object and nf has an [[InitializedNumberFormat]] internal slot.
    ASSERT(nf->IsJSObject() && nf->IsJSNumberFormat());
    // 3. If value is not provided, let value be undefined.
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    // 4 Let x be ? ToNumeric(value).
    JSTaggedNumber x = JSTaggedValue::ToNumber(thread, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5 Return ? FormatNumeric(nf, x).
    JSHandle<JSTaggedValue> result = JSNumberFormat::FormatNumeric(thread, JSHandle<JSNumberFormat>::Cast(nf), x);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
