/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_string_iterator.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {
// 21.1.5.2.1
JSTaggedValue string_iterator::proto::Next(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), StringIteratorPrototype, Next);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    // 3. If O does not have all of the internal slots of an String Iterator Instance (21.1.5.3),
    // throw a TypeError exception.
    if (!this_value->IsStringIterator()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "is not StringIterator", JSTaggedValue::Exception());
    }
    // 4. Let s be the value of the [[IteratedString]] internal slot of O.
    JSHandle<JSTaggedValue> string(thread, this_value.GetObject<JSStringIterator>()->GetIteratedString());
    // 5. If s is undefined, return CreateIterResultObject(undefined, true).
    if (string->IsUndefined()) {
        return JSIterator::CreateIterResultObject(thread, string, true).GetTaggedValue();
    }
    // 6. Let position be the value of the [[StringIteratorNextIndex]] internal slot of O.
    double position =
        JSTaggedNumber(this_value.GetObject<JSStringIterator>()->GetStringIteratorNextIndex()).GetNumber();

    // 7. Let len be the number of elements in s.
    uint32_t len = string.GetObject<EcmaString>()->GetLength();
    // If position ≥ len, then
    // a. Set the value of the [[IteratedString]] internal slot of O to
    // b. Return CreateIterResultObject(undefined, true).
    if (position >= len) {
        this_value.GetObject<JSStringIterator>()->SetIteratedString(thread, JSTaggedValue::Undefined());
        JSHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
        return JSIterator::CreateIterResultObject(thread, result, true).GetTaggedValue();
    }

    // 9. Let first be the code unit value at index position in s.
    uint16_t first = string.GetObject<EcmaString>()->At<false>(position);
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    uint32_t result_size = 1;
    // 10. If first < 0xD800 or first > 0xDBFF or position+1 = len, let resultString be the string consisting of the
    // single code unit first.
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (position + 1 == len || first < utf::DECODE_LEAD_LOW || first > utf::DECODE_LEAD_HIGH) {
        std::vector<uint16_t> result_string {first, 0x0};
        result.Update(factory->NewFromUtf16UnCheck(result_string.data(), 1, true).GetTaggedValue());
    } else {
        // 11. Else,
        // a. Let second be the code unit value at index position+1 in the String S.
        // b. If second < 0xDC00 or second > 0xDFFF, let resultString be the string consisting of the single code unit
        // first.
        // c. Else, let resultString be the string consisting of the code unit first followed by the code unit second.
        uint16_t second = string.GetObject<EcmaString>()->At<false>(position + 1);
        if (second < utf::DECODE_TRAIL_LOW || second > utf::DECODE_TRAIL_HIGH) {
            std::vector<uint16_t> result_string {first, 0x0};
            result.Update(factory->NewFromUtf16UnCheck(result_string.data(), 1, false).GetTaggedValue());
        } else {
            std::vector<uint16_t> result_string {first, second, 0x0};
            result.Update(
                factory->NewFromUtf16UnCheck(result_string.data(), 2, false).GetTaggedValue());  // 2: two bytes
            result_size = 2;  // 2: 2 means that two bytes represent a character string
        }
    }
    // 12. Let resultSize be the number of code units in resultString.
    // 13. Set the value of the [[StringIteratorNextIndex]] internal slot of O to position+ resultSize.
    this_value.GetObject<JSStringIterator>()->SetStringIteratorNextIndex(thread, JSTaggedValue(position + result_size));

    // 14. Return CreateIterResultObject(resultString, false).
    return JSIterator::CreateIterResultObject(thread, result, false).GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
