/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_arguments.h"
#include "plugins/ecmascript/runtime/js_eval.h"
#include "plugins/ecmascript/runtime/js_stable_array.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript::builtins {
// ecma 19.2.1 Function (p1, p2, ... , pn, body)
JSTaggedValue function::FunctionConstructor(EcmaRuntimeCallInfo *argv)
{
    return EvalUtils::CreateDynamicFunction(argv, EvalUtils::DynamicFunctionKind::NORMAL);
}

// ecma 19.2.3 The Function prototype object is itself a built-in function object.
//             When invoked, it accepts any arguments and returns undefined.
JSTaggedValue function::FunctionPrototypeInvokeSelf([[maybe_unused]] EcmaRuntimeCallInfo *argv)
{
    return JSTaggedValue::Undefined();
}

namespace {

size_t MakeArgListWithHole(JSThread *thread, TaggedArray *argv, size_t length)
{
    if (length > argv->GetLength()) {
        length = argv->GetLength();
    }
    for (size_t index = 0; index < length; ++index) {
        JSTaggedValue value = argv->Get(thread, index);
        if (value.IsHole()) {
            argv->Set(thread, index, JSTaggedValue::Undefined());
        }
    }
    return length;
}

std::pair<TaggedArray *, size_t> BuildArgumentsListFast(JSThread *thread, const JSHandle<JSTaggedValue> &array_obj)
{
    if (!array_obj->HasStableElements(thread)) {
        return std::make_pair(nullptr, 0);
    }
    if (array_obj->IsStableJSArguments(thread)) {
        JSHandle<JSArguments> arg_list = JSHandle<JSArguments>::Cast(array_obj);
        TaggedArray *elements = TaggedArray::Cast(arg_list->GetElements().GetTaggedObject());
        auto env = thread->GetEcmaVM()->GetGlobalEnv();
        if (arg_list->GetClass() != env->GetArgumentsClass().GetObject<JSHClass>()) {
            return std::make_pair(nullptr, 0);
        }
        auto result = arg_list->GetPropertyInlinedProps(JSArguments::LENGTH_INLINE_PROPERTY_INDEX);
        if (!result.IsInt()) {
            return std::make_pair(nullptr, 0);
        }
        auto length = static_cast<size_t>(result.GetInt());
        size_t res = MakeArgListWithHole(thread, elements, length);
        return std::make_pair(elements, res);
    }

    if (array_obj->IsStableJSArray(thread)) {
        JSHandle<JSArray> arg_list = JSHandle<JSArray>::Cast(array_obj);
        TaggedArray *elements = TaggedArray::Cast(arg_list->GetElements().GetTaggedObject());
        size_t length = arg_list->GetArrayLength();
        size_t res = MakeArgListWithHole(thread, elements, length);
        return std::make_pair(elements, res);
    }
    UNREACHABLE();
}
}  // anonymous namespace

// ecma 19.2.3.1 Function.prototype.apply (thisArg, argArray)
JSTaggedValue function::proto::Apply(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), FunctionPrototype, Apply);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. If IsCallable(func) is false, throw a TypeError exception.
    if (!builtins_common::GetThis(argv)->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "apply target is not callable", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> func = builtins_common::GetThis(argv);
    JSHandle<JSTaggedValue> this_arg = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> undefined = thread->GlobalConstants()->GetHandledUndefined();
    // 2. If argArray is null or undefined, then
    if (builtins_common::GetCallArg(argv, 1)->IsUndefined()) {  // null will also get undefined
        // a. Return Call(func, thisArg).
        auto info = NewRuntimeCallInfo(thread, func, this_arg, undefined, 0);
        return JSFunction::Call(info.Get());
    }
    // 3. Let argList be CreateListFromArrayLike(argArray).
    JSHandle<JSTaggedValue> array_obj = builtins_common::GetCallArg(argv, 1);
    auto [array, length] = BuildArgumentsListFast(thread, array_obj);
    if (array == nullptr) {
        auto arg_list_res = JSObject::CreateListFromArrayLike(thread, array_obj);
        // 4. ReturnIfAbrupt(argList).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<TaggedArray> arg_list = JSHandle<TaggedArray>::Cast(arg_list_res);
        const auto args_length = static_cast<uint32_t>(arg_list->GetLength());
        auto info = NewRuntimeCallInfo(thread, func, this_arg, undefined, args_length);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        info->SetCallArg(args_length, arg_list->GetData());
        return JSFunction::Call(info.Get());
    }
    // 6. Return Call(func, thisArg, argList).
    const auto args_length = static_cast<uint32_t>(length);
    auto info = NewRuntimeCallInfo(thread, func, this_arg, undefined, args_length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    info->SetCallArg(args_length, array->GetData());
    return JSFunction::Call(info.Get());
}

// ecma 19.2.3.2 Function.prototype.bind (thisArg , ...args)
JSTaggedValue function::proto::Bind(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), FunctionPrototype, Bind);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let Target be the this value.
    JSHandle<JSTaggedValue> target = builtins_common::GetThis(argv);
    // 2. If IsCallable(Target) is false, throw a TypeError exception.
    if (!target->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "bind target is not callable", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> this_arg = builtins_common::GetCallArg(argv, 0);
    uint32_t args_length = 0;
    if (argv->GetArgsNumber() > 1) {
        args_length = argv->GetArgsNumber() - 1;
    }

    // 3. Let args be a new (possibly empty) List consisting of all of the argument
    //    values provided after thisArg in order.
    JSHandle<TaggedArray> args_array = factory->NewTaggedArray(args_length);
    for (uint32_t index = 0; index < args_length; ++index) {
        args_array->Set(thread, index, builtins_common::GetCallArg(argv, index + 1));
    }
    // 4. Let F be BoundFunctionCreate(Target, thisArg, args).
    JSHandle<JSFunctionBase> target_function = JSHandle<JSFunctionBase>::Cast(target);
    JSHandle<JSBoundFunction> bound_function = factory->NewJSBoundFunction(target_function, this_arg, args_array);
    // 5. ReturnIfAbrupt(F)
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. Let targetHasLength be HasOwnProperty(Target, "length").
    auto global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> length_key = global_const->GetHandledLengthString();
    bool target_has_length =
        JSTaggedValue::HasOwnProperty(thread, JSHandle<JSTaggedValue>::Cast(target_function), length_key);
    // 7. ReturnIfAbrupt(targetHasLength).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    double length_value = 0.0;
    // 8. If targetHasLength is true, then
    if (target_has_length) {
        // a. Let targetLen be Get(Target, "length").
        JSHandle<JSTaggedValue> target_len = JSObject::GetProperty(thread, target, length_key).GetValue();
        // b. ReturnIfAbrupt(targetLen).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

        // c. If Type(targetLen) is not Number, let L be 0.
        // d. Else,
        //    i. Let targetLen be ToInteger(targetLen).
        //    ii. Let L be the larger of 0 and the result of targetLen minus the number of elements of args.
        if (target_len->IsNumber()) {
            // argv include thisArg
            length_value = std::max(0.0, JSTaggedValue::ToNumber(thread, target_len).GetNumber() -
                                             static_cast<double>(args_length));
        }
    }
    // 9. Else let L be 0.

    // 10. Let status be DefinePropertyOrThrow(F, "length", PropertyDescriptor {[[Value]]: L,
    //     [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: true}).
    PropertyDescriptor desc(thread, JSHandle<JSTaggedValue>(thread, JSTaggedValue(length_value)), false, false, true);
    [[maybe_unused]] bool status =
        JSTaggedValue::DefinePropertyOrThrow(thread, JSHandle<JSTaggedValue>(bound_function), length_key, desc);
    // 11. Assert: status is not an abrupt completion.
    ASSERT_PRINT(status, "DefinePropertyOrThrow failed");

    // 12. Let targetName be Get(Target, "name").
    JSHandle<JSTaggedValue> name_key = global_const->GetHandledNameString();
    JSHandle<JSTaggedValue> target_name = JSObject::GetProperty(thread, target, name_key).GetValue();
    // 13. ReturnIfAbrupt(targetName).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSTaggedValue> bound_name(factory->NewFromCanBeCompressString("bound"));
    // 14. If Type(targetName) is not String, let targetName be the empty string.
    // 15. Perform SetFunctionName(F, targetName, "bound").
    if (!target_name->IsString()) {
        JSHandle<JSTaggedValue> empty_string(factory->GetEmptyString());
        status =
            JSFunction::SetFunctionName(thread, JSHandle<JSFunctionBase>(bound_function), empty_string, bound_name);
    } else {
        status = JSFunction::SetFunctionName(thread, JSHandle<JSFunctionBase>(bound_function), target_name, bound_name);
    }
    // Assert: status is not an abrupt completion.
    ASSERT_PRINT(status, "SetFunctionName failed");

    // 16. Return F.
    return bound_function.GetTaggedValue();
}

// ecma 19.2.3.3 Function.prototype.call (thisArg , ...args)
JSTaggedValue function::proto::Call(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), FunctionPrototype, Call);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. If IsCallable(func) is false, throw a TypeError exception.
    if (!builtins_common::GetThis(argv)->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "call target is not callable", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> func = builtins_common::GetThis(argv);
    JSHandle<JSTaggedValue> this_arg = builtins_common::GetCallArg(argv, 0);
    uint32_t args_length = 0;
    if (argv->GetArgsNumber() > 1) {
        args_length = argv->GetArgsNumber() - 1;
    }
    // 2. Let argList be an empty List.
    // 3. If this method was called with more than one argument then in left to right order,
    //    starting with the second argument, append each argument as the last element of argList.
    // 5. Return Call(func, thisArg, argList).
    JSHandle<JSTaggedValue> undefined = thread->GlobalConstants()->GetHandledUndefined();
    auto info = NewRuntimeCallInfo(thread, func, this_arg, undefined, args_length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (args_length > 0) {
        info->SetCallArg(args_length,
                         reinterpret_cast<JSTaggedType *>(argv->GetArgAddress(js_method_args::NUM_MANDATORY_ARGS + 1)));
    }
    return JSFunction::Call(info.Get());
}

// ecma 19.2.3.5 Function.prototype.toString ()
JSTaggedValue function::proto::ToString(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), FunctionPrototype, ToString);
    // not implement due to that runtime can not get JS Source Code now.
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    if (!this_value->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "function.toString() target is not callable", JSTaggedValue::Exception());
    }
    return builtins_common::GetTaggedString(
        thread, "Not support function.toString() due to Runtime can not obtain Source Code yet.");
}

// ecma 19.2.3.6 Function.prototype[@@hasInstance] (V)
JSTaggedValue function::proto::HasInstance(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), FunctionPrototype, HasInstance);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    // 1. Let F be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    // 2. Return OrdinaryHasInstance(F, V).
    JSHandle<JSTaggedValue> arg = builtins_common::GetCallArg(argv, 0);
    return JSFunction::OrdinaryHasInstance(argv->GetThread(), this_value, arg)
               ? builtins_common::GetTaggedBoolean(true)
               : builtins_common::GetTaggedBoolean(false);
}
}  // namespace panda::ecmascript::builtins
