/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/js_eval.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"

namespace panda::ecmascript::builtins {
// 26.2.1.1 GeneratorFunction(p1, p2, … , pn, body)
JSTaggedValue generator_function::GeneratorFunctionConstructor(EcmaRuntimeCallInfo *argv)
{
    return EvalUtils::CreateDynamicFunction(argv, EvalUtils::DynamicFunctionKind::GENERATOR);
}

// 26.4.1.2 Generator.prototype.next(value)
JSTaggedValue generator::proto::Next(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), GeneratorPrototype, Next);
    // 1.Let g be the this value.
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetThis(argv);
    if (!msg->IsGeneratorObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Not a generator object.", JSTaggedValue::Exception());
    }
    JSHandle<JSGeneratorObject> generator(thread, JSGeneratorObject::Cast(*JSTaggedValue::ToObject(thread, msg)));
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);

    // 2.Return ? GeneratorResume(g, value).
    JSHandle<JSTaggedValue> result = JSGeneratorObject::GeneratorResume(thread, generator, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 26.4.1.3 Generator.prototype.return(value)
JSTaggedValue generator::proto::Return(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), GeneratorPrototype, Return);
    // 1.Let g be the this value.
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetThis(argv);
    if (!msg->IsGeneratorObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Not a generator object.", JSTaggedValue::Exception());
    }
    JSHandle<JSGeneratorObject> generator(thread, JSGeneratorObject::Cast(*JSTaggedValue::ToObject(thread, msg)));

    // 2.Let C be Completion { [[Type]]: return, [[Value]]: value, [[Target]]: empty }.
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<CompletionRecord> completion_record = factory->NewCompletionRecord(CompletionRecord::RETURN, value);

    // 3.Return ? GeneratorResumeAbrupt(g, C).
    JSHandle<JSTaggedValue> result = JSGeneratorObject::GeneratorResumeAbrupt(thread, generator, completion_record);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 26.4.1.4 Generator.prototype.throw(exception)
JSTaggedValue generator::proto::Throw(EcmaRuntimeCallInfo *argv)
{
    BUILTINS_API_TRACE(argv->GetThread(), GeneratorPrototype, Throw);
    // 1.Let g be the this value.
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> msg = builtins_common::GetThis(argv);
    if (!msg->IsGeneratorObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Not a generator object.", JSTaggedValue::Exception());
    }
    JSHandle<JSGeneratorObject> generator(thread, JSGeneratorObject::Cast(*JSTaggedValue::ToObject(thread, msg)));

    // 2.Let C be ThrowCompletion(exception).
    JSHandle<JSTaggedValue> exception = builtins_common::GetCallArg(argv, 0);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<CompletionRecord> completion_record = factory->NewCompletionRecord(CompletionRecord::THROW, exception);

    // 3.Return ? GeneratorResumeAbrupt(g, C).
    JSHandle<JSTaggedValue> result = JSGeneratorObject::GeneratorResumeAbrupt(thread, generator, completion_record);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
