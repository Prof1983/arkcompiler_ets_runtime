/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include <cmath>
#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"
#include "plugins/ecmascript/runtime/base/typed_array_helper.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::builtins {
using TypedArrayHelper = ecmascript::base::TypedArrayHelper;

// 22.2.1
JSTaggedValue typed_array::TypedArrayBaseConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArray, TypedArrayBaseConstructor);
    THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "TypedArray Constructor cannot be called.",
                                JSTaggedValue::Exception());
}

JSTaggedValue int8_array::Int8ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledInt8ArrayString());
}

JSTaggedValue uint8_array::Uint8ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledUint8ArrayString());
}

JSTaggedValue uint8_clamped_array::Uint8ClampedArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv,
                                                   thread->GlobalConstants()->GetHandledUint8ClampedArrayString());
}

JSTaggedValue int16_array::Int16ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledInt16ArrayString());
}

JSTaggedValue uint16_array::Uint16ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledUint16ArrayString());
}

JSTaggedValue int32_array::Int32ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledInt32ArrayString());
}

JSTaggedValue uint32_array::Uint32ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledUint32ArrayString());
}

JSTaggedValue float32_array::Float32ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledFloat32ArrayString());
}

JSTaggedValue float64_array::Float64ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledFloat64ArrayString());
}

JSTaggedValue big_int64_array::BigInt64ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledBigInt64ArrayString());
}

JSTaggedValue big_uint64_array::BigUint64ArrayConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    return TypedArrayHelper::TypedArrayConstructor(argv, thread->GlobalConstants()->GetHandledBigUint64ArrayString());
}

// 22.2.2.1 %TypedArray%.from ( source [ , mapfn [ , thisArg ] ] )
JSTaggedValue typed_array::From(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArray, From);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    // 1. Let C be the this value.
    // 2. If IsConstructor(C) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    if (!this_handle->IsConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the this value is not a Constructor.", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    // 3. If mapfn is undefined, let mapping be false.
    // 4. Else,
    //   a. If IsCallable(mapfn) is false, throw a TypeError exception.
    //   b. Let mapping be true.
    bool mapping = false;
    JSHandle<JSTaggedValue> mapfn = builtins_common::GetCallArg(argv, 1);
    if (!mapfn->IsUndefined()) {
        if (!mapfn->IsCallable()) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "the mapfn is not callable.", JSTaggedValue::Exception());
        }
        mapping = true;
    }
    // 5. Let usingIterator be ? GetMethod(source, @@iterator).
    JSHandle<JSTaggedValue> source = builtins_common::GetCallArg(argv, 0);
    if (!source->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the source is not an object.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> source_obj(source);
    JSHandle<JSTaggedValue> iterator_symbol = env->GetIteratorSymbol();
    JSHandle<JSTaggedValue> using_iterator =
        JSObject::GetMethod(thread, JSHandle<JSTaggedValue>::Cast(source_obj), iterator_symbol);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. If usingIterator is not undefined, then
    //   a. Let values be ? IterableToList(source, usingIterator).
    //   b. Let len be the number of elements in values.
    //   c. Let targetObj be ? TypedArrayCreate(C, « len »).
    if (!using_iterator->IsUndefined()) {
        PandaVector<JSHandle<JSTaggedValue>> vec;
        JSHandle<JSTaggedValue> iterator = JSIterator::GetIterator(thread, source, using_iterator);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<JSTaggedValue> next(thread, JSTaggedValue::True());
        while (!next->IsFalse()) {
            next = JSIterator::IteratorStep(thread, iterator);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (!next->IsFalse()) {
                JSHandle<JSTaggedValue> next_value = JSIterator::IteratorValue(thread, next);
                RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, next_value.GetTaggedValue());
                vec.push_back(next_value);
            }
        }
        uint32_t len = vec.size();
        std::array<JSTaggedType, 1> args = {JSTaggedValue(len).GetRawData()};
        JSHandle<JSObject> target_obj =
            TypedArrayHelper::TypedArrayCreate(thread, this_handle, args.size(), args.data());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        //   d. Let k be 0.
        //   e. Repeat, while k < len
        //     i. Let Pk be ! ToString(k).
        //     ii. Let kValue be the first element of values and remove that element from values.
        //     iii. If mapping is true, then
        //       1. Let mappedValue be ? Call(mapfn, thisArg, « kValue, k »).
        //     iv. Else, let mappedValue be kValue.
        //     v. Perform ? Set(targetObj, Pk, mappedValue, true).
        //     vi. Set k to k + 1.
        JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
        JSMutableHandle<JSTaggedValue> map_value(thread, JSTaggedValue::Undefined());
        double k = 0;
        while (k < len) {
            t_key.Update(JSTaggedValue(k));
            JSHandle<JSTaggedValue> k_value = vec[k];
            if (mapping) {
                auto info = NewRuntimeCallInfo(thread, mapfn, this_arg_handle, JSTaggedValue::Undefined(), 2);
                info->SetCallArgs(k_value, t_key);
                JSTaggedValue call_result = JSFunction::Call(info.Get());  // 2: two args
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                map_value.Update(call_result);
            } else {
                map_value.Update(k_value.GetTaggedValue());
            }
            JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
            JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(target_obj), k_key, map_value, true);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            k++;
        }
        //   f. Assert: values is now an empty List.
        //   g. Return targetObj.
        return target_obj.GetTaggedValue();
    }

    // 7. NOTE: source is not an Iterable so assume it is already an array-like object.
    // 8. Let arrayLike be ! ToObject(source).
    JSHandle<JSObject> array_like_obj = JSTaggedValue::ToObject(thread, source);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> array_like(array_like_obj);
    // 9. Let len be ? LengthOfArrayLike(arrayLike).
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> len_result = JSTaggedValue::GetProperty(thread, array_like, length_key).GetValue();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSTaggedNumber t_len = JSTaggedValue::ToLength(thread, len_result);
    // 6. ReturnIfAbrupt(relativeTarget).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double len = t_len.GetNumber();

    // 10. Let targetObj be ? TypedArrayCreate(C, « len »).

    std::array<JSTaggedType, 1> args = {JSTaggedValue(len).GetRawData()};
    JSHandle<JSObject> target_obj = TypedArrayHelper::TypedArrayCreate(thread, this_handle, args.size(), args.data());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 11. Let k be 0.
    // 12. Repeat, while k < len
    //   a. Let Pk be ! ToString(k).
    //   b. Let kValue be ? Get(arrayLike, Pk).
    //   c. If mapping is true, then
    //     i. Let mappedValue be ? Call(mapfn, thisArg, « kValue, k »).
    //   d. Else, let mappedValue be kValue.
    //   e. Perform ? Set(targetObj, Pk, mappedValue, true).
    //   f. Set k to k + 1.
    JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
    double k = 0;
    while (k < len) {
        t_key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
        JSHandle<JSTaggedValue> k_value = JSTaggedValue::GetProperty(thread, array_like, k_key).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<JSTaggedValue> map_value;
        if (mapping) {
            auto info = NewRuntimeCallInfo(thread, mapfn, this_arg_handle, JSTaggedValue::Undefined(), 2);
            info->SetCallArgs(k_value, t_key);
            JSTaggedValue call_result = JSFunction::Call(info.Get());  // 2: two args
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            map_value = JSHandle<JSTaggedValue>(thread, call_result);
        } else {
            map_value = k_value;
        }
        JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(target_obj), k_key, map_value, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        k++;
    }
    // 13. Return targetObj.
    return target_obj.GetTaggedValue();
}

// 22.2.2.2 %TypedArray%.of ( ...items )
JSTaggedValue typed_array::Of(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArray, Of);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let len be the actual number of arguments passed to this function.
    uint32_t len = argv->GetArgsNumber();
    // 2. Let items be the List of arguments passed to this function.
    // 3. Let C be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 4. If IsConstructor(C) is false, throw a TypeError exception.
    if (!this_handle->IsConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the this value is not a Constructor.", JSTaggedValue::Exception());
    }
    // 5. Let newObj be TypedArrayCreate(C, « len »).

    std::array<JSTaggedType, 1> args = {JSTaggedValue(len).GetRawData()};
    JSHandle<JSObject> new_obj = TypedArrayHelper::TypedArrayCreate(thread, this_handle, args.size(), args.data());
    // 6. ReturnIfAbrupt(newObj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let kValue be items[k].
    //   b. Let Pk be ! ToString(k).
    //   c. Perform ? Set(newObj, Pk, kValue, true).
    //   d. ReturnIfAbrupt(status).
    //   e. Set k to k + 1.
    JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
    double k = 0;
    while (k < len) {
        t_key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
        JSHandle<JSTaggedValue> k_value = builtins_common::GetCallArg(argv, k);
        JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_obj), k_key, k_value, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        k++;
    }
    // 9. Return newObj.
    return new_obj.GetTaggedValue();
}

// 22.2.2.4
JSTaggedValue typed_array::GetSpecies(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    // 1. Return the this value.
    return builtins_common::GetThis(argv).GetTaggedValue();
}

// prototype
// 22.2.3.1 get %TypedArray%.prototype.buffer
JSTaggedValue typed_array::proto::GetBuffer(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, GetBuffer);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value is not an object.", JSTaggedValue::Exception());
    }
    // 3. If O does not have a [[ViewedArrayBuffer]] internal slot, throw a TypeError exception.
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value does not have a [[ViewedArrayBuffer]] internal slot.",
                                    JSTaggedValue::Exception());
    }
    // 4. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = JSHandle<JSTypedArray>::Cast(this_handle)->GetViewedArrayBuffer();
    // 5. Return buffer.
    return buffer;
}

// 22.2.3.2
JSTaggedValue typed_array::proto::GetByteLength(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, GetByteLength);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value is not an object.", JSTaggedValue::Exception());
    }
    // 3. If O does not have a [[ViewedArrayBuffer]] internal slot, throw a TypeError exception.
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value does not have a [[ViewedArrayBuffer]] internal slot.",
                                    JSTaggedValue::Exception());
    }
    // 4. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = JSHandle<JSTypedArray>::Cast(this_handle)->GetViewedArrayBuffer();
    // 5. If IsDetachedBuffer(buffer) is true, return 0.
    if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
        return JSTaggedValue(0);
    }
    // 6. Let size be the value of O’s [[ByteLength]] internal slot.
    // 7. Return size.
    return JSHandle<JSTypedArray>(this_handle)->GetByteLength();
}

// 22.2.3.3
JSTaggedValue typed_array::proto::GetByteOffset(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, GetByteOffset);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value is not an object.", JSTaggedValue::Exception());
    }
    // 3. If O does not have a [[ViewedArrayBuffer]] internal slot, throw a TypeError exception.
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value does not have a [[ViewedArrayBuffer]] internal slot.",
                                    JSTaggedValue::Exception());
    }
    // 4. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = JSHandle<JSTypedArray>::Cast(this_handle)->GetViewedArrayBuffer();
    // 5. If IsDetachedBuffer(buffer) is true, return 0.
    if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
        return JSTaggedValue(0);
    }
    // 6. Let offset be the value of O’s [[ByteOffset]] internal slot.
    int32_t offset = TypedArrayHelper::GetByteOffset(thread, JSHandle<JSObject>(this_handle));
    // 7. Return offset.
    return JSTaggedValue(offset);
}

// 22.2.3.5
JSTaggedValue typed_array::proto::CopyWithin(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, CopyWithin);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::CopyWithin(argv);
}

// 22.2.3.6
JSTaggedValue typed_array::proto::Entries(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Entries);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. Let valid be ValidateTypedArray(O).
    TypedArrayHelper::ValidateTypedArray(thread, this_handle);
    // 3. ReturnIfAbrupt(valid).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(argv->GetThread());
    JSHandle<JSObject> self(this_handle);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 4. Return CreateArrayIterator(O, "key+value").
    JSHandle<JSArrayIterator> iter(factory->NewJSArrayIterator(self, IterationKind::KEY_AND_VALUE));
    return iter.GetTaggedValue();
}

// 22.2.3.7
JSTaggedValue typed_array::proto::Every(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Every);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    int32_t len = TypedArrayHelper::GetArrayLength(thread, this_obj_handle);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let testResult be ToBoolean(Call(callbackfn, T, «kValue, k, O»)).
    //     iv. ReturnIfAbrupt(testResult).
    //     v. If testResult is false, return false.
    //   e. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());

    int32_t k = 0;
    while (k < len) {
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<JSTaggedValue> k_value = JSTaggedValue::GetProperty(thread, this_obj_val, k).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        key.Update(JSTaggedValue(k));
        auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
        info->SetCallArgs(k_value, key, this_obj_val);
        JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        bool bool_result = call_result.ToBoolean();
        if (!bool_result) {
            return builtins_common::GetTaggedBoolean(false);
        }
        k++;
    }

    // 9. Return true.
    return builtins_common::GetTaggedBoolean(true);
}

// 22.2.3.8
JSTaggedValue typed_array::proto::Fill(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::Fill(argv);
}

// 22.2.3.9 %TypedArray%.prototype.filter ( callbackfn [ , thisArg ] )
JSTaggedValue typed_array::proto::Filter(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Filter);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. Let valid be ValidateTypedArray(O).
    TypedArrayHelper::ValidateTypedArray(thread, this_handle);
    // 3. ReturnIfAbrupt(valid).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSObject> this_obj(this_handle);
    // 4. Let len be the value of O’s [[ArrayLength]] internal slot.
    int32_t len = TypedArrayHelper::GetArrayLength(thread, this_obj);
    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }
    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 10. Let kept be a new empty List.
    JSHandle<TaggedArray> kept(factory->NewTaggedArray(len));

    // 11. Let k be 0.
    // 12. Let captured be 0.
    // 13. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kValue be Get(O, Pk).
    //   c. ReturnIfAbrupt(kValue).
    //   d. Let selected be ToBoolean(Call(callbackfn, T, «kValue, k, O»)).
    //   e. ReturnIfAbrupt(selected).
    //   f. If selected is true, then
    //     i. Append kValue to the end of kept.
    //     ii. Increase captured by 1.
    //   g. Increase k by 1.
    int32_t captured = 0;
    JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());

    for (int32_t k = 0; k < len; k++) {
        t_key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
        JSHandle<JSTaggedValue> k_value = JSTaggedValue::GetProperty(thread, this_handle, k_key).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
        info->SetCallArgs(k_value, t_key, this_handle);
        JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
        bool test_result = call_result.ToBoolean();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (test_result) {
            kept->Set(thread, captured, k_value);
            captured++;
        }
    }
    // es11 9. Let A be ? TypedArraySpeciesCreate(O, « captured »).
    std::array<JSTaggedType, 1> args = {JSTaggedValue(captured).GetRawData()};
    JSHandle<JSObject> new_arr_obj =
        TypedArrayHelper::TypedArraySpeciesCreate(thread, this_obj, args.size(), args.data());
    // 15. ReturnIfAbrupt(A).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 16. Let n be 0.
    // 17. For each element e of kept
    //   a. Let status be Set(A, ToString(n), e, true ).
    //   b. ReturnIfAbrupt(status).
    //   c. Increment n by 1.
    JSMutableHandle<JSTaggedValue> value_handle(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> nt_key(thread, JSTaggedValue::Undefined());
    for (int32_t n = 0; n < captured; n++) {
        value_handle.Update(kept->Get(n));
        nt_key.Update(JSTaggedValue(n));
        JSHandle<JSTaggedValue> n_key(JSTaggedValue::ToString(thread, nt_key));
        JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>(new_arr_obj), n_key, value_handle, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    // 18. Return A.
    return new_arr_obj.GetTaggedValue();
}

// 22.2.3.10
JSTaggedValue typed_array::proto::Find(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::Find(argv);
}

// 22.2.3.11
JSTaggedValue typed_array::proto::FindIndex(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::FindIndex(argv);
}

// 22.2.3.12
JSTaggedValue typed_array::proto::ForEach(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, ForEach);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    // 2. ReturnIfAbrupt(O).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    // 3. Let len be ToLength(Get(O, "length")).
    int32_t len = TypedArrayHelper::GetArrayLength(thread, this_obj_handle);
    // 4. ReturnIfAbrupt(len).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callback_fn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }

    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);

    // 7. Let k be 0.
    // 8. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kPresent be HasProperty(O, Pk).
    //   c. ReturnIfAbrupt(kPresent).
    //   d. If kPresent is true, then
    //     i. Let kValue be Get(O, Pk).
    //     ii. ReturnIfAbrupt(kValue).
    //     iii. Let funcResult be Call(callbackfn, T, «kValue, k, O»).
    //     iv. ReturnIfAbrupt(funcResult).
    //   e. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());

    int32_t k = 0;
    while (k < len) {
        JSHandle<JSTaggedValue> k_value = JSTaggedValue::GetProperty(thread, this_obj_val, k).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        key.Update(JSTaggedValue(k));
        auto info = NewRuntimeCallInfo(thread, callback_fn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
        info->SetCallArgs(k_value, key, this_obj_val);
        JSTaggedValue func_result = JSFunction::Call(info.Get());  // 3: three args
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, func_result);
        k++;
    }

    // 9. Return undefined.
    return JSTaggedValue::Undefined();
}

// ES2021 23.2.3.13
JSTaggedValue typed_array::proto::Includes(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Includes);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);

    // 2. Perform ? ValidateTypedArray(O).
    TypedArrayHelper::ValidateTypedArray(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Let len be ToLength(Get(O, "length")).
    JSHandle<JSObject> this_obj(this_handle);
    int32_t len = TypedArrayHelper::GetArrayLength(thread, this_obj);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. If len is 0, return false.
    if (len == 0) {
        return builtins_common::GetTaggedBoolean(false);
    }

    ArraySizeT argc = argv->GetArgsNumber();
    double from_index = 0;

    if (argc > 1) {
        // 4-5. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be 0.
        JSHandle<JSTaggedValue> msg1 = builtins_common::GetCallArg(argv, 1);
        JSTaggedNumber from_index_temp = JSTaggedValue::ToNumber(thread, msg1);

        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        from_index = ecmascript::base::NumberHelper::TruncateDouble(from_index_temp.GetNumber());

        // 6. If n is positiveInfinity, return false.
        if (JSTaggedValue::Equal(thread, msg1,
                                 JSHandle<JSTaggedValue>(thread, JSTaggedValue(base::POSITIVE_INFINITY)))) {
            return builtins_common::GetTaggedBoolean(false);
        }

        // 7. Else if n is negativeInfinity, set n to 0.
        if (JSTaggedValue::Equal(thread, msg1,
                                 JSHandle<JSTaggedValue>(thread, JSTaggedValue(-base::POSITIVE_INFINITY)))) {
            from_index = 0;
        }
    }

    // 8. If n ≥ 0, then
    //   a. Let k be n.
    // 9. Else,
    //   a. Let k be len + n.
    //   b. If k < 0, set k to 0.
    double from = (from_index >= 0) ? from_index : ((len + from_index) >= 0 ? len + from_index : 0);

    // 10.
    const JSHandle<JSTaggedValue> search_element = builtins_common::GetCallArg(argv, 0);
    while (from < len) {
        // a)
        JSHandle<JSTaggedValue> index_handle(thread, JSTaggedValue(from));
        JSHandle<JSTaggedValue> handle =
            JSHandle<JSTaggedValue>(thread, JSTaggedValue::ToString(thread, index_handle).GetTaggedValue());
        JSHandle<JSTaggedValue> element = JSTaggedValue::GetProperty(thread, this_handle, handle).GetValue();

        // b)
        if (JSTaggedValue::SameValueZero(search_element.GetTaggedValue(), element.GetTaggedValue())) {
            return builtins_common::GetTaggedBoolean(true);
        }

        // c)
        from++;
    }

    return builtins_common::GetTaggedBoolean(false);
}
// 22.2.3.13
JSTaggedValue typed_array::proto::IndexOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::IndexOf(argv);
}

// 22.2.3.14
JSTaggedValue typed_array::proto::Join(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::Join(argv);
}

// 22.2.3.15
JSTaggedValue typed_array::proto::Keys(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Keys);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. Let valid be ValidateTypedArray(O).
    TypedArrayHelper::ValidateTypedArray(thread, this_handle);
    // 3. ReturnIfAbrupt(valid).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(argv->GetThread());
    JSHandle<JSObject> self(this_handle);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 4. Return CreateArrayIterator(O, "key").
    JSHandle<JSArrayIterator> iter(factory->NewJSArrayIterator(self, IterationKind::KEY));
    return iter.GetTaggedValue();
}

// 22.2.3.16
JSTaggedValue typed_array::proto::LastIndexOf(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::LastIndexOf(argv);
}

// 22.2.3.17
JSTaggedValue typed_array::proto::GetLength(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, GetLength);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value is not an object.", JSTaggedValue::Exception());
    }
    // 3. If O does not have a [[TypedArrayName]] internal slot, throw a TypeError exception.
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value does not have a [[TypedArrayName]] internal slot.",
                                    JSTaggedValue::Exception());
    }
    // 4. Assert: O has [[ViewedArrayBuffer]] and [[ArrayLength]] internal slots.
    // 5. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = JSHandle<JSTypedArray>::Cast(this_handle)->GetViewedArrayBuffer();
    // 6. If IsDetachedBuffer(buffer) is true, return 0.
    if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
        return JSTaggedValue(0);
    }
    // 7. Let length be the value of O’s [[ArrayLength]] internal slot.
    int32_t length = TypedArrayHelper::GetArrayLength(thread, JSHandle<JSObject>(this_handle));
    // 8. Return length.
    return JSTaggedValue(length);
}

// 22.2.3.18 %TypedArray%.prototype.map ( callbackfn [ , thisArg ] )
JSTaggedValue typed_array::proto::Map(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Map);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. Let valid be ValidateTypedArray(O).
    TypedArrayHelper::ValidateTypedArray(thread, this_handle);
    // 3. ReturnIfAbrupt(valid).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSObject> this_obj(this_handle);
    // 4. Let len be the value of O’s [[ArrayLength]] internal slot.
    int32_t len = TypedArrayHelper::GetArrayLength(thread, this_obj);
    // 5. If IsCallable(callbackfn) is false, throw a TypeError exception.
    JSHandle<JSTaggedValue> callbackfn_handle = builtins_common::GetCallArg(argv, 0);
    if (!callbackfn_handle->IsCallable()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "the callbackfun is not callable.", JSTaggedValue::Exception());
    }
    // 6. If thisArg was supplied, let T be thisArg; else let T be undefined.
    JSHandle<JSTaggedValue> this_arg_handle = builtins_common::GetCallArg(argv, 1);
    // es11 5. Let A be ? TypedArraySpeciesCreate(O, « len »).

    std::array<JSTaggedType, 1> args = {JSTaggedValue(len).GetRawData()};
    JSHandle<JSObject> new_arr_obj =
        TypedArrayHelper::TypedArraySpeciesCreate(thread, this_obj, args.size(), args.data());
    // 11. ReturnIfAbrupt(A).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 12. Let k be 0.
    // 13. Repeat, while k < len
    //   a. Let Pk be ToString(k).
    //   b. Let kValue be Get(O, Pk).
    //   c. ReturnIfAbrupt(kValue).
    //   d. Let mappedValue be Call(callbackfn, T, «kValue, k, O»).
    //   e. ReturnIfAbrupt(mappedValue).
    //   f. Let status be Set(A, Pk, mappedValue, true ).
    //   g. ReturnIfAbrupt(status).
    //   h. Increase k by 1.
    JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> map_value(thread, JSTaggedValue::Undefined());
    for (int32_t k = 0; k < len; k++) {
        key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> k_value = JSTaggedValue::GetProperty(thread, this_handle, key).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        auto info = NewRuntimeCallInfo(thread, callbackfn_handle, this_arg_handle, JSTaggedValue::Undefined(), 3);
        info->SetCallArgs(k_value, key, this_handle);
        JSTaggedValue call_result = JSFunction::Call(info.Get());  // 3: three args
        map_value.Update(call_result);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>(new_arr_obj), key, map_value, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    // 14. Return A.
    return new_arr_obj.GetTaggedValue();
}

// 22.2.3.19
JSTaggedValue typed_array::proto::Reduce(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::Reduce(argv);
}

// 22.2.3.20
JSTaggedValue typed_array::proto::ReduceRight(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::ReduceRight(argv);
}

// 22.2.3.21
JSTaggedValue typed_array::proto::Reverse(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::Reverse(argv);
}

// 22.2.3.22 %TypedArray%.prototype.set ( overloaded [ , offset ])
// NOLINTNEXTLINE(readability-function-size)
JSTaggedValue typed_array::proto::Set(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Set);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    // 1. Assert: array is any ECMAScript language value other than an Object with a [[TypedArrayName]] internal slot.
    // If it is such an Object, the definition in 22.2.3.22.2 applies.
    // 2. Let target be the this value.
    JSHandle<JSTaggedValue> target = builtins_common::GetThis(argv);
    // 3. If Type(target) is not Object, throw a TypeError exception.
    if (!target->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value is not an object.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> target_obj(target);
    // 4. If target does not have a [[TypedArrayName]] internal slot, throw a TypeError exception.
    if (!target->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value does not have a [[TypedArrayName]] internal slot.",
                                    JSTaggedValue::Exception());
    }

    // 5. Assert: target has a [[ViewedArrayBuffer]] internal slot.
    // 6. Let targetOffset be ToInteger (offset).
    JSTaggedNumber t_target_offset = JSTaggedValue::ToInteger(thread, builtins_common::GetCallArg(argv, 1));
    // 7. ReturnIfAbrupt(targetOffset).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double target_offset = t_target_offset.GetNumber();
    // 8. If targetOffset < 0, throw a RangeError exception.
    if (target_offset < 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "The targetOffset of This value is less than 0.",
                                     JSTaggedValue::Exception());
    }
    // 9. Let targetBuffer be the value of target’s [[ViewedArrayBuffer]] internal slot.
    JSHandle<JSTaggedValue> target_buffer(thread, JSTypedArray::Cast(*target_obj)->GetViewedArrayBuffer());
    // 10. If IsDetachedBuffer(targetBuffer) is true, throw a TypeError exception.
    if (builtins::array_buffer::IsDetachedBuffer(target_buffer.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The targetBuffer of This value is detached buffer.",
                                    JSTaggedValue::Exception());
    }
    // 11. Let targetLength be the value of target’s [[ArrayLength]] internal slot.
    // 12. Let targetName be the String value of target’s [[TypedArrayName]] internal slot.
    // 13. Let targetElementSize be the Number value of the Element Size value specified in Table 49 for targetName.
    // 14. Let targetType be the String value of the Element Type value in Table 49 for targetName.
    // 15. Let targetByteOffset be the value of target’s [[ByteOffset]] internal slot.
    int32_t target_length = TypedArrayHelper::GetArrayLength(thread, target_obj);
    JSHandle<JSTaggedValue> target_name(thread, JSTypedArray::Cast(*target_obj)->GetTypedArrayName());
    int32_t target_element_size = TypedArrayHelper::GetSizeFromName(thread, target_name);
    DataViewType target_type = TypedArrayHelper::GetTypeFromName(thread, target_name);
    int32_t target_byte_offset = TypedArrayHelper::GetByteOffset(thread, target_obj);

    JSHandle<JSTaggedValue> arg_array = builtins_common::GetCallArg(argv, 0);

    // 22.2.3.22.1 %TypedArray%.prototype.set (array [ , offset ] )
    if (!arg_array->IsTypedArray()) {
        // 16. Let src be ToObject(array).
        JSHandle<JSObject> src = JSTaggedValue::ToObject(thread, arg_array);
        // 17. ReturnIfAbrupt(src).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // 18. Let srcLength be ToLength(Get(src, "length")).
        JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
        JSHandle<JSTaggedValue> len_result =
            JSTaggedValue::GetProperty(thread, JSHandle<JSTaggedValue>::Cast(src), length_key).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSTaggedNumber t_src_len = JSTaggedValue::ToLength(thread, len_result);
        // 19. ReturnIfAbrupt(srcLength).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        double src_len = t_src_len.GetNumber();
        // 20. If srcLength + targetOffset > targetLength, throw a RangeError exception.
        if (src_len + target_offset > target_length) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "The sum of srcLength and targetOffset is greater than targetLength.",
                                         JSTaggedValue::Exception());
        }
        // 21. Let targetByteIndex be targetOffset × targetElementSize + targetByteOffset.
        int32_t target_byte_index = target_offset * target_element_size + target_byte_offset;
        // 22. Let k be 0.
        // 23. Let limit be targetByteIndex + targetElementSize × srcLength.
        int32_t k = 0;
        int32_t limit = target_byte_index + target_element_size * src_len;
        // 24. Repeat, while targetByteIndex < limit
        //   a. Let Pk be ToString(k).
        //   b. Let kNumber be ToNumber(Get(src, Pk)).
        //   c. ReturnIfAbrupt(kNumber).
        //   d. If IsDetachedBuffer(targetBuffer) is true, throw a TypeError exception.
        //   e. Perform SetValueInBuffer(targetBuffer, targetByteIndex, targetType, kNumber).
        //   f. Set k to k + 1.
        //   g. Set targetByteIndex to targetByteIndex + targetElementSize.
        JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
        JSMutableHandle<JSTaggedValue> k_number_handle(thread, JSTaggedValue::Undefined());
        while (target_byte_index < limit) {
            t_key.Update(JSTaggedValue(k));
            JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
            JSHandle<JSTaggedValue> k_value =
                JSTaggedValue::GetProperty(thread, JSHandle<JSTaggedValue>::Cast(src), k_key).GetValue();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (builtins::array_buffer::IsDetachedBuffer(target_buffer.GetTaggedValue())) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "The targetBuffer of This value is detached buffer.",
                                            JSTaggedValue::Exception());
            }

            ContentType content_type = JSHandle<JSTypedArray>::Cast(target)->GetContentType();
            if (content_type == ContentType::BIG_INT) {
                k_number_handle.Update(JSTaggedValue::ToBigInt(thread, k_value));
            } else {
                k_number_handle.Update(JSTaggedValue::ToNumber(thread, k_value));
            }
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            builtins::array_buffer::SetValueInBuffer(thread, target_buffer, target_byte_index, target_type,
                                                     k_number_handle, true);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            k++;
            target_byte_index = target_byte_index + target_element_size;
        }
        // 25. Return undefined.
        return JSTaggedValue::Undefined();
    }

    // 22.2.3.22.2 %TypedArray%.prototype.set(typedArray [, offset ] )
    JSHandle<JSObject> typed_array(arg_array);
    // 12. Let srcBuffer be the value of typedArray’s [[ViewedArrayBuffer]] internal slot.
    // 13. If IsDetachedBuffer(srcBuffer) is true, throw a TypeError exception.
    JSMutableHandle<JSTaggedValue> src_buffer(thread, JSTypedArray::Cast(*typed_array)->GetViewedArrayBuffer());
    if (builtins::array_buffer::IsDetachedBuffer(src_buffer.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The ArrayBuffer of typedArray is detached buffer.",
                                    JSTaggedValue::Exception());
    }
    // 18. Let srcName be the String value of typedArray’s [[TypedArrayName]] internal slot.
    // 19. Let srcType be the String value of the Element Type value in Table 49 for srcName .
    // 20. Let srcElementSize be the Number value of the Element Size value specified in Table 49 for srcName.
    // 21. Let srcLength be the value of typedArray’s [[ArrayLength]] internal slot.
    // 22. Let srcByteOffset be the value of typedArray’s [[ByteOffset]] internal slot.
    JSHandle<JSTaggedValue> src_name(thread, JSTypedArray::Cast(*typed_array)->GetTypedArrayName());
    DataViewType src_type = TypedArrayHelper::GetTypeFromName(thread, src_name);
    int32_t src_element_size = TypedArrayHelper::GetSizeFromName(thread, src_name);
    int32_t src_length = TypedArrayHelper::GetArrayLength(thread, typed_array);
    int32_t src_byte_offset = TypedArrayHelper::GetByteOffset(thread, typed_array);
    // 23. If srcLength + targetOffset > targetLength, throw a RangeError exception.
    if (src_length + target_offset > target_length) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "The sum of srcLength and targetOffset is greater than targetLength.",
                                     JSTaggedValue::Exception());
    }
    // 24. If SameValue(srcBuffer, targetBuffer) is true, then
    //   a. Let srcBuffer be CloneArrayBuffer(targetBuffer, srcByteOffset, %ArrayBuffer%).
    //   b. NOTE: %ArrayBuffer% is used to clone targetBuffer because is it known to not have any observable
    //      side-effects.
    //   c. ReturnIfAbrupt(srcBuffer).
    //   d. Let srcByteIndex be 0.
    // 25. Else, let srcByteIndex be srcByteOffset.
    int32_t src_byte_index;
    if (JSTaggedValue::SameValue(src_buffer.GetTaggedValue(), target_buffer.GetTaggedValue())) {
        src_buffer.Update(builtins::array_buffer::CloneArrayBuffer(thread, target_buffer, src_byte_offset,
                                                                   env->GetArrayBufferFunction()));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        src_byte_index = 0;
    } else {
        src_byte_index = src_byte_offset;
    }
    // 26. Let targetByteIndex be targetOffset × targetElementSize + targetByteOffset.
    int32_t target_byte_index = target_offset * target_element_size + target_byte_offset;
    // 27. Let limit be targetByteIndex + targetElementSize × srcLength.
    int32_t limit = target_byte_index + target_element_size * src_length;
    // 28. If SameValue(srcType, targetType) is false, then
    //   a. Repeat, while targetByteIndex < limit
    //     i. Let value be GetValueFromBuffer(srcBuffer, srcByteIndex, srcType).
    //     ii. Perform SetValueInBuffer (targetBuffer, targetByteIndex, targetType, value).
    //     iii. Set srcByteIndex to srcByteIndex + srcElementSize.
    //     iv. Set targetByteIndex to targetByteIndex + targetElementSize.
    JSMutableHandle<JSTaggedValue> value(thread, JSTaggedValue::Undefined());
    if (src_type != target_type) {
        while (target_byte_index < limit) {
            JSTaggedValue tagged_data =
                builtins::array_buffer::GetValueFromBuffer(thread, src_buffer, src_byte_index, src_type, true);
            value.Update(tagged_data);
            builtins::array_buffer::SetValueInBuffer(thread, target_buffer, target_byte_index, target_type, value,
                                                     true);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            src_byte_index = src_byte_index + src_element_size;
            target_byte_index = target_byte_index + target_element_size;
        }
    } else {
        // 29. Else,
        //   a. NOTE: If srcType and targetType are the same the transfer must be performed in a manner that preserves
        //   the bit-level encoding of the source data.
        //   b. Repeat, while targetByteIndex < limit
        //     i. Let value be GetValueFromBuffer(srcBuffer, srcByteIndex, "Uint8").
        //     ii. Perform SetValueInBuffer (targetBuffer, targetByteIndex, "Uint8", value).
        //     iii. Set srcByteIndex to srcByteIndex + 1.
        //     iv. Set targetByteIndex to targetByteIndex + 1.
        while (target_byte_index < limit) {
            JSTaggedValue tagged_data =
                // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue
                // (issue #I5QOJX)
                // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
                builtins::array_buffer::GetValueFromBuffer(thread, src_buffer, src_byte_index, DataViewType::UINT8,
                                                           true);
            value.Update(tagged_data);
            builtins::array_buffer::SetValueInBuffer(thread, target_buffer, target_byte_index, DataViewType::UINT8,
                                                     value, true);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            src_byte_index = src_byte_index + 1;
            target_byte_index = target_byte_index + 1;
        }
    }
    // 30. Return undefined.
    return JSTaggedValue::Undefined();
}

// 22.2.3.23 %TypedArray%.prototype.slice ( start, end )
JSTaggedValue typed_array::proto::Slice(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Slice);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. Let valid be ValidateTypedArray(O).
    TypedArrayHelper::ValidateTypedArray(thread, this_handle);
    // 3. ReturnIfAbrupt(valid).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSObject> this_obj(this_handle);
    // 4. Let len be the value of O’s [[ArrayLength]] internal slot.
    int32_t len = TypedArrayHelper::GetArrayLength(thread, this_obj);

    double k;
    // 5. Let relativeStart be ToInteger(start).
    JSTaggedNumber t_relative_start = JSTaggedValue::ToInteger(thread, builtins_common::GetCallArg(argv, 0));
    // 6. ReturnIfAbrupt(relativeStart).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double relative_start = t_relative_start.GetNumber();
    // 7. If relativeStart < 0, let k be max((len + relativeStart),0); else let k be min(relativeStart, len).
    if (relative_start < 0) {
        k = relative_start + len > 0 ? relative_start + len : 0;
    } else {
        k = relative_start < len ? relative_start : len;
    }
    // 8. If end is undefined, let relativeEnd be len; else let relativeEnd be ToInteger(end).
    double relative_end = len;
    JSHandle<JSTaggedValue> end = builtins_common::GetCallArg(argv, 1);
    if (!end->IsUndefined()) {
        JSTaggedNumber t_relative_end = JSTaggedValue::ToInteger(thread, end);
        // 9. ReturnIfAbrupt(relativeEnd).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        relative_end = t_relative_end.GetNumber();
    }

    // 10. If relativeEnd < 0, let final be max((len + relativeEnd),0); else let final be min(relativeEnd, len).
    double final = 0;
    if (relative_end < 0) {
        final = relative_end + len > 0 ? relative_end + len : 0;
    } else {
        final = relative_end < len ? relative_end : len;
    }
    // 11. Let count be max(final – k, 0).
    double count = (final - k) > 0 ? (final - k) : 0;
    // es11 9. Let A be ? TypedArraySpeciesCreate(O, « count »).

    std::array<JSTaggedType, 1> args = {JSTaggedValue(count).GetRawData()};
    JSHandle<JSObject> new_arr_obj =
        TypedArrayHelper::TypedArraySpeciesCreate(thread, this_obj, args.size(), args.data());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 17. Let srcName be the String value of O’s [[TypedArrayName]] internal slot.
    // 18. Let srcType be the String value of the Element Type value in Table 49 for srcName.
    JSHandle<JSTaggedValue> src_name(thread, JSTypedArray::Cast(*this_obj)->GetTypedArrayName());
    DataViewType src_type = TypedArrayHelper::GetTypeFromName(thread, src_name);
    // 19. Let targetName be the String value of A’s [[TypedArrayName]] internal slot.
    // 20. Let targetType be the String value of the Element Type value in Table 49 for targetName.
    JSHandle<JSTaggedValue> target_name(thread, JSTypedArray::Cast(*new_arr_obj)->GetTypedArrayName());
    DataViewType target_type = TypedArrayHelper::GetTypeFromName(thread, target_name);
    // 21. If SameValue(srcType, targetType) is false, then
    //   a. Let n be 0.
    //   b. Repeat, while k < final
    //     i. Let Pk be ToString(k).
    //     ii. Let kValue be Get(O, Pk).
    //     iii. ReturnIfAbrupt(kValue).
    //     iv. Let status be Set(A, ToString(n), kValue, true ).
    //     v. ReturnIfAbrupt(status).
    //     vi. Increase k by 1.
    //     vii. Increase n by 1.
    JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> k_value(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> nt_key(thread, JSTaggedValue::Undefined());
    if (src_type != target_type) {
        double n = 0;
        while (k < final) {
            t_key.Update(JSTaggedValue(k));
            JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
            k_value.Update(JSTaggedValue::GetProperty(thread, this_handle, k_key).GetValue().GetTaggedValue());
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            nt_key.Update(JSTaggedValue(n));
            JSHandle<JSTaggedValue> n_key(JSTaggedValue::ToString(thread, nt_key));
            JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>(new_arr_obj), n_key, k_value, true);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            n++;
            k++;
        }
    } else if (count > 0) {
        // 22. Else if count > 0,
        //   a. Let srcBuffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
        //   b. If IsDetachedBuffer(srcBuffer) is true, throw a TypeError exception.
        JSHandle<JSTaggedValue> src_buffer(thread, JSTypedArray::Cast(*this_obj)->GetViewedArrayBuffer());
        if (builtins::array_buffer::IsDetachedBuffer(src_buffer.GetTaggedValue())) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "The ArrayBuffer of this value is detached buffer.",
                                        JSTaggedValue::Exception());
        }
        //   c. Let targetBuffer be the value of A’s [[ViewedArrayBuffer]] internal slot.
        JSHandle<JSTaggedValue> target_buffer(thread, JSTypedArray::Cast(*new_arr_obj)->GetViewedArrayBuffer());
        //   d. Let elementSize be the Number value of the Element Size value specified in Table 49 for srcType.
        int32_t element_size = TypedArrayHelper::GetSizeFromName(thread, src_name);
        //   e. NOTE: If srcType and targetType are the same the transfer must be performed in a manner that
        //   preserves the bit-level encoding of the source data. f. Let srcByteOffset be the value of O’s
        //   [[ByteOffset]] internal slot.
        int32_t src_byte_offset = TypedArrayHelper::GetByteOffset(thread, this_obj);
        //   g. Let targetByteIndex be 0.
        //   h. Let srcByteIndex be (k × elementSize) + srcByteOffset.

        int32_t src_byte_index = k * element_size + src_byte_offset;
        //   i. Repeat, while targetByteIndex < count × elementSize
        //     i. Let value be GetValueFromBuffer(srcBuffer, srcByteIndex, "Uint8").
        //     ii. Perform SetValueInBuffer (targetBuffer, targetByteIndex, "Uint8", value).
        //     iii. Increase srcByteIndex by 1.
        //     iv. Increase targetByteIndex by 1.
        JSMutableHandle<JSTaggedValue> value(thread, JSTaggedValue::Undefined());
        for (int32_t target_byte_index = 0; target_byte_index < count * element_size;
             src_byte_index++, target_byte_index++) {
            JSTaggedValue tagged_data = builtins::array_buffer::GetValueFromBuffer(thread, src_buffer, src_byte_index,
                                                                                   DataViewType::UINT8, true);
            value.Update(tagged_data);
            builtins::array_buffer::SetValueInBuffer(thread, target_buffer, target_byte_index, DataViewType::UINT8,
                                                     value, true);
        }
    }
    // 23. Return A.
    return new_arr_obj.GetTaggedValue();
}

// 22.2.3.24
JSTaggedValue typed_array::proto::Some(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::Some(argv);
}

// 22.2.3.25
JSTaggedValue typed_array::proto::Sort(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Sort);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let obj be ToObject(this value).
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }

    JSHandle<JSObject> this_obj_handle = JSTaggedValue::ToObject(thread, this_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);

    JSHandle<JSTaggedValue> buffer;
    buffer = JSHandle<JSTaggedValue>(thread, TypedArrayHelper::ValidateTypedArray(thread, this_handle));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double len = TypedArrayHelper::GetArrayLength(thread, this_obj_handle);

    JSHandle<JSTaggedValue> callback_fn_handle = builtins_common::GetCallArg(argv, 0);

    uint32_t i = 0;
    while (i < len - 1) {
        uint32_t j = len - 1;
        while (j > i) {
            JSHandle<JSTaggedValue> x_value = JSTaggedValue::GetProperty(thread, this_obj_val, j - 1).GetValue();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            JSHandle<JSTaggedValue> y_value = JSTaggedValue::GetProperty(thread, this_obj_val, j).GetValue();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            int32_t compare_result;
            compare_result = TypedArrayHelper::SortCompare(thread, callback_fn_handle, buffer, x_value, y_value);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (compare_result > 0) {
                JSTaggedValue::SetProperty(thread, this_obj_val, j - 1, y_value, true);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                JSTaggedValue::SetProperty(thread, this_obj_val, j, x_value, true);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            j--;
        }
        i++;
    }
    return JSTaggedValue::ToObject(thread, this_handle).GetTaggedValue();
}

// 22.2.3.26 %TypedArray%.prototype.subarray( [ begin [ , end ] ] )
JSTaggedValue typed_array::proto::Subarray(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Subarray);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, throw a TypeError exception.
    if (!this_handle->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value is not an object.", JSTaggedValue::Exception());
    }
    JSHandle<JSObject> this_obj(this_handle);
    // 3. If O does not have a [[TypedArrayName]] internal slot, throw a TypeError exception.
    if (!this_handle->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "This value does not have a [[TypedArrayName]] internal slot.",
                                    JSTaggedValue::Exception());
    }
    // 4. Assert: O has a [[ViewedArrayBuffer]] internal slot.
    // 6. Let srcLength be the value of O’s [[ArrayLength]] internal slot.
    int32_t src_length = TypedArrayHelper::GetArrayLength(thread, this_obj);
    // 7. Let relativeBegin be ToInteger(begin).
    JSTaggedNumber t_relative_begin = JSTaggedValue::ToInteger(thread, builtins_common::GetCallArg(argv, 0));
    // 8. ReturnIfAbrupt(relativeBegin).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double relative_begin = t_relative_begin.GetNumber();

    double begin_index;
    // 9. If relativeBegin < 0, let beginIndex be max((srcLength + relativeBegin), 0); else let beginIndex be
    // min(relativeBegin, srcLength).
    if (relative_begin < 0) {
        begin_index = relative_begin + src_length > 0 ? relative_begin + src_length : 0;
    } else {
        begin_index = relative_begin < src_length ? relative_begin : src_length;
    }

    // 10. If end is undefined, let relativeEnd be srcLength; else, let relativeEnd be ToInteger(end).
    double relative_end = src_length;
    JSHandle<JSTaggedValue> end = builtins_common::GetCallArg(argv, 1);
    if (!end->IsUndefined()) {
        JSTaggedNumber t_relative_end = JSTaggedValue::ToInteger(thread, end);
        // 11. ReturnIfAbrupt(relativeEnd).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        relative_end = t_relative_end.GetNumber();
    }
    // 12. If relativeEnd < 0, let endIndex be max((srcLength + relativeEnd), 0); else let endIndex be
    // min(relativeEnd, srcLength).
    double end_index;
    if (relative_end < 0) {
        end_index = relative_end + src_length > 0 ? relative_end + src_length : 0;
    } else {
        end_index = relative_end < src_length ? relative_end : src_length;
    }
    // 13. Let new_length be max(endIndex – beginIndex, 0).
    double new_length = (end_index - begin_index) > 0 ? (end_index - begin_index) : 0;
    // 14. Let constructorName be the String value of O’s [[TypedArrayName]] internal slot.
    // 15. Let elementSize be the Number value of the Element Size value specified in Table 49 for constructorName.
    // 16. Let srcByteOffset be the value of O’s [[ByteOffset]] internal slot.
    // 17. Let beginByteOffset be srcByteOffset + beginIndex × elementSize.
    JSHandle<JSTaggedValue> constructor_name(thread, JSTypedArray::Cast(*this_obj)->GetTypedArrayName());
    int32_t element_size = TypedArrayHelper::GetSizeFromName(thread, constructor_name);
    int32_t src_byte_offset = TypedArrayHelper::GetByteOffset(thread, this_obj);
    int32_t begin_byte_offset = src_byte_offset + begin_index * element_size;
    // 21. Let argumentsList be «buffer, beginByteOffset, new_length».
    // 5. Let buffer be the value of O’s [[ViewedArrayBuffer]] internal slot.
    JSTaggedValue buffer = JSTypedArray::Cast(*this_obj)->GetViewedArrayBuffer();
    // 22. Return Construct(constructor, argumentsList).

    std::array<JSTaggedType, 3> args = {buffer.GetRawData(), JSTaggedValue(begin_byte_offset).GetRawData(),
                                        JSTaggedValue(new_length).GetRawData()};
    JSHandle<JSObject> new_arr =
        TypedArrayHelper::TypedArraySpeciesCreate(thread, this_obj, args.size(), args.data());  // 3: three args
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return new_arr.GetTaggedValue();
}

// 22.2.3.27
JSTaggedValue typed_array::proto::ToLocaleString(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    if (!builtins_common::GetThis(argv)->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(argv->GetThread(), "This is not a TypedArray.", JSTaggedValue::Exception());
    }
    return builtins::array::proto::ToLocaleString(argv);
}

// 22.2.3.29
JSTaggedValue typed_array::proto::Values(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, Values);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. Let valid be ValidateTypedArray(O).
    TypedArrayHelper::ValidateTypedArray(thread, this_handle);
    // 3. ReturnIfAbrupt(valid).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(argv->GetThread());
    JSHandle<JSObject> self(this_handle);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 4. Return CreateArrayIterator(O, "value").
    JSHandle<JSArrayIterator> iter(factory->NewJSArrayIterator(self, IterationKind::VALUE));
    return iter.GetTaggedValue();
}

// 22.2.3.31
JSTaggedValue typed_array::proto::GetToStringTag(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), TypedArrayPrototype, GetToStringTag);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. Let O be the this value.
    JSHandle<JSTaggedValue> this_handle = builtins_common::GetThis(argv);
    // 2. If Type(O) is not Object, return undefined.
    if (!this_handle->IsECMAObject()) {
        return JSTaggedValue::Undefined();
    }
    // 3. If O does not have a [[TypedArrayName]] internal slot, return undefined.
    if (!this_handle->IsTypedArray()) {
        return JSTaggedValue::Undefined();
    }
    // 4. Let name be the value of O’s [[TypedArrayName]] internal slot.
    JSTaggedValue name = JSHandle<JSTypedArray>::Cast(this_handle)->GetTypedArrayName();
    // 5. Assert: name is a String value.
    ASSERT(name.IsString());
    // 6. Return name.
    return name;
}
}  // namespace panda::ecmascript::builtins
