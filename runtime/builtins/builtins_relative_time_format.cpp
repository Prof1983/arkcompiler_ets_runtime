/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/js_relative_time_format.h"

namespace panda::ecmascript::builtins {
// 14.2.1 Intl.RelativeTimeFormat ([ locales [ , options ]])
JSTaggedValue relative_time_format::RelativeTimeFormatConstructor(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. If NewTarget is undefined, throw a TypeError exception.
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "new_target is undefined", JSTaggedValue::Exception());
    }

    // 2. Let relativeTimeFormat be ? OrdinaryCreateFromConstructor
    // (NewTarget, "%RelativeTimeFormatPrototype%", « [[InitializedRelativeTimeFormat]],
    // [[Locale]], [[DataLocale]], [[Style]], [[Numeric]], [[NumberFormat]], [[NumberingSystem]], [[PluralRules]] »).
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSRelativeTimeFormat> relative_time_format = JSHandle<JSRelativeTimeFormat>::Cast(
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Perform ? InitializeRelativeTimeFormat(relativeTimeFormat, locales, options).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSRelativeTimeFormat::InitializeRelativeTimeFormat(thread, relative_time_format, locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 4.  Intl.RelativeTimeFormat.prototype[ @@toStringTag ]
    // This property has the attributes { [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: true }.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);
    bool is_instance_of = JSObject::InstanceOf(thread, this_value, env->GetRelativeTimeFormatFunction());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (new_target->IsUndefined() && this_value->IsJSObject() && is_instance_of) {
        PropertyDescriptor descriptor(thread, JSHandle<JSTaggedValue>::Cast(relative_time_format), false, false, true);
        JSHandle<JSTaggedValue> key(thread, JSHandle<JSIntl>::Cast(env->GetIntlFunction())->GetFallbackSymbol());
        JSTaggedValue::DefinePropertyOrThrow(thread, this_value, key, descriptor);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        return this_value.GetTaggedValue();
    }

    return relative_time_format.GetTaggedValue();
}

// 14.3.1 Intl.RelativeTimeFormat.supportedLocalesOf ( locales [ , options ] )
JSTaggedValue relative_time_format::SupportedLocalesOf(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let availableLocales be %RelativeTimeFormat%.[[AvailableLocales]].
    JSHandle<TaggedArray> available_locales = JSLocale::GetAvailableLocales(thread, "calendar", nullptr);

    // 2. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<JSTaggedValue> locales = builtins_common::GetCallArg(argv, 0);
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. Return ? SupportedLocales(availableLocales, requestedLocales, options).
    JSHandle<JSTaggedValue> options = builtins_common::GetCallArg(argv, 1);
    JSHandle<JSArray> result = JSLocale::SupportedLocales(thread, available_locales, requested_locales, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 14.4.3 Intl.RelativeTimeFormat.prototype.format( value, unit )
JSTaggedValue relative_time_format::proto::Format(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let relativeTimeFormat be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(relativeTimeFormat, [[InitializedRelativeTimeFormat]]).
    if (!this_value->IsJSRelativeTimeFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not rtf object", JSTaggedValue::Exception());
    }

    // 3. Let value be ? ToNumber(value).
    double x = 0.0;
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber temp = JSTaggedValue::ToNumber(thread, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    x = temp.GetNumber();

    // 4. Let unit be ? ToString(unit).
    JSHandle<JSTaggedValue> unit_value = builtins_common::GetCallArg(argv, 1);
    JSHandle<EcmaString> unit = JSTaggedValue::ToString(thread, unit_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Return ? FormatRelativeTime(relativeTimeFormat, value, unit).
    JSHandle<JSRelativeTimeFormat> relative_time_format = JSHandle<JSRelativeTimeFormat>::Cast(this_value);
    JSHandle<EcmaString> result = JSRelativeTimeFormat::Format(thread, x, unit, relative_time_format);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 14.4.4 Intl.RelativeTimeFormat.prototype.formatToParts( value, unit )
JSTaggedValue relative_time_format::proto::FormatToParts(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let relativeTimeFormat be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(relativeTimeFormat, [[InitializedRelativeTimeFormat]]).
    if (!this_value->IsJSRelativeTimeFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not rtf object", JSTaggedValue::Exception());
    }

    // 3. Let value be ? ToNumber(value).
    double x = 0.0;
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber temp = JSTaggedValue::ToNumber(thread, value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    x = temp.GetNumber();

    // 4. Let unit be ? ToString(unit).
    JSHandle<JSTaggedValue> unit_value = builtins_common::GetCallArg(argv, 1);
    JSHandle<EcmaString> unit = JSTaggedValue::ToString(thread, unit_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 5. Return ? FormatRelativeTime(relativeTimeFormat, value, unit).
    JSHandle<JSRelativeTimeFormat> relative_time_format = JSHandle<JSRelativeTimeFormat>::Cast(this_value);
    JSHandle<JSArray> result = JSRelativeTimeFormat::FormatToParts(thread, x, unit, relative_time_format);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

// 14.4.5 Intl.RelativeTimeFormat.prototype.resolvedOptions ()
JSTaggedValue relative_time_format::proto::ResolvedOptions(EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);

    // 1. Let relativeTimeFormat be the this value.
    JSHandle<JSTaggedValue> this_value = builtins_common::GetThis(argv);

    // 2. Perform ? RequireInternalSlot(relativeTimeFormat, [[InitializedRelativeTimeFormat]]).
    if (!this_value->IsJSRelativeTimeFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not rtf object", JSTaggedValue::Exception());
    }

    // 3. Let options be ! ObjectCreate(%ObjectPrototype%).
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<JSTaggedValue> ctor = env->GetObjectFunction();
    JSHandle<JSObject> options(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor), ctor));

    // 4. perform resolvedOptions
    JSHandle<JSRelativeTimeFormat> relative_time_format = JSHandle<JSRelativeTimeFormat>::Cast(this_value);
    JSRelativeTimeFormat::ResolvedOptions(thread, relative_time_format, options);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 5. Return options.
    return options.GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins