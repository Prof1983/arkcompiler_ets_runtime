/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/jobs/micro_job_queue.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_async_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript::builtins {
// es6 25.4.1.3.2 Promise Resolve Functions
JSTaggedValue promise_handler::Resolve(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseHandler, Resolve);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();

    // 1. Assert: F has a [[Promise]] internal slot whose value is an Object.
    JSHandle<JSPromiseReactionsFunction> resolve =
        JSHandle<JSPromiseReactionsFunction>::Cast(builtins_common::GetConstructor(argv));
    ASSERT_PRINT(resolve->GetPromise().IsECMAObject(), "Resolve: promise must be js object");

    // 2. Let promise be the value of F's [[Promise]] internal slot.
    // 3. Let alreadyResolved be the value of F's [[AlreadyResolved]] internal slot.
    // 4. If alreadyResolved.[[value]] is true, return undefined.
    // 5. Set alreadyResolved.[[value]] to true.
    JSHandle<JSPromise> resolve_promise(thread, resolve->GetPromise());
    JSHandle<PromiseRecord> already_resolved(thread, resolve->GetAlreadyResolved());
    if (already_resolved->GetValue().IsTrue()) {
        return JSTaggedValue::Undefined();
    }
    already_resolved->SetValue(thread, JSTaggedValue::True());

    // 6. If SameValue(resolution, promise) is true, then
    //     a. Let selfResolutionError be a newly created TypeError object.
    //     b. Return RejectPromise(promise, selfResolutionError).
    JSHandle<JSTaggedValue> resolution = builtins_common::GetCallArg(argv, 0);
    if (JSTaggedValue::SameValue(resolution.GetTaggedValue(), resolve_promise.GetTaggedValue())) {
        JSHandle<JSObject> resolution_error =
            factory->GetJSError(ErrorType::TYPE_ERROR, "Resolve: The promise and resolution cannot be the same.");
        JSPromise::RejectPromise(thread, resolve_promise, JSHandle<JSTaggedValue>::Cast(resolution_error));
        return JSTaggedValue::Undefined();
    }
    // 7. If Type(resolution) is not Object, then
    //     a. Return FulfillPromise(promise, resolution).
    if (!resolution.GetTaggedValue().IsECMAObject()) {
        JSPromise::FulfillPromise(thread, resolve_promise, resolution);
        return JSTaggedValue::Undefined();
    }
    // 8. Let then be Get(resolution, "then").
    // 9. If then is an abrupt completion, then
    //     a. Return RejectPromise(promise, then.[[value]]).
    JSHandle<JSTaggedValue> then_key(thread->GlobalConstants()->GetHandledPromiseThenString());
    JSHandle<JSTaggedValue> then_value = JSObject::GetProperty(thread, resolution, then_key).GetValue();
    if (UNLIKELY(thread->HasPendingException())) {
        if (!then_value->IsJSError()) {
            if (thread->GetException().IsObjectWrapper()) {
                JSHandle<ObjectWrapper> wrapper_value(thread, thread->GetException());
                JSHandle<JSTaggedValue> throw_value(thread, wrapper_value->GetValue());
                then_value = throw_value;
            } else {
                then_value = JSHandle<JSTaggedValue>(thread, thread->GetException());
            }
        }
        thread->ClearException();
        return JSPromise::RejectPromise(thread, resolve_promise, then_value);
    }
    // 10. Let thenAction be then.[[value]].
    // 11. If IsCallable(thenAction) is false, then
    //     a. Return FulfillPromise(promise, resolution).
    if (!then_value->IsCallable()) {
        JSPromise::FulfillPromise(thread, resolve_promise, resolution);
        return JSTaggedValue::Undefined();
    }
    // 12. Perform EnqueueJob ("PromiseJobs", PromiseResolveThenableJob, «promise, resolution, thenAction»)
    JSHandle<TaggedArray> arguments = factory->NewTaggedArray(3);  // 3: 3 means three args stored in array
    arguments->Set(thread, 0, resolve_promise);
    arguments->Set(thread, 1, resolution);
    arguments->Set(thread, 2, then_value);  // 2: 2 means index of array is 2

    JSHandle<JSFunction> promise_resolve_thenable_job(env->GetPromiseResolveThenableJob());
    JSHandle<job::MicroJobQueue> job = thread->GetEcmaVM()->GetMicroJobQueue();
    job::MicroJobQueue::EnqueueJob(thread, job, job::QueueType::QUEUE_PROMISE, promise_resolve_thenable_job, arguments);

    // 13. Return undefined.
    return JSTaggedValue::Undefined();
}

// es6 25.4.1.3.1 Promise Reject Functions
JSTaggedValue promise_handler::Reject(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseHandler, Reject);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Assert: F has a [[Promise]] internal slot whose value is an Object.
    JSHandle<JSPromiseReactionsFunction> reject =
        JSHandle<JSPromiseReactionsFunction>::Cast(builtins_common::GetConstructor(argv));
    ASSERT_PRINT(reject->GetPromise().IsECMAObject(), "Reject: promise must be js object");

    // 2. Let promise be the value of F's [[Promise]] internal slot.
    // 3. Let alreadyResolved be the value of F's [[AlreadyResolved]] internal slot.
    // 4. If alreadyResolved.[[value]] is true, return undefined.
    // 5. Set alreadyResolved.[[value]] to true.
    JSHandle<JSPromise> reject_promise(thread, reject->GetPromise());
    JSHandle<PromiseRecord> already_resolved(thread, reject->GetAlreadyResolved());
    if (already_resolved->GetValue().IsTrue()) {
        return JSTaggedValue::Undefined();
    }
    already_resolved->SetValue(thread, JSTaggedValue::True());

    // 6. Return RejectPromise(promise, reason).
    JSHandle<JSTaggedValue> reason = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> result(thread, JSPromise::RejectPromise(thread, reject_promise, reason));
    return result.GetTaggedValue();
}

// es6 25.4.1.5.1 GetCapabilitiesExecutor Functions
JSTaggedValue promise_handler::Executor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseHandler, Executor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Assert: F has a [[Capability]] internal slot whose value is a PromiseCapability Record.
    JSHandle<JSPromiseExecutorFunction> executor =
        JSHandle<JSPromiseExecutorFunction>::Cast(builtins_common::GetConstructor(argv));
    ASSERT_PRINT(executor->GetCapability().IsRecord(),
                 "Executor: F has a [[Capability]] internal slot whose value is a PromiseCapability Record.");

    // 2. Let promiseCapability be the value of F's [[Capability]] internal slot.
    // 3. If promiseCapability.[[Resolve]] is not undefined, throw a TypeError exception.
    JSHandle<PromiseCapability> promise_capability(thread, executor->GetCapability());
    if (!promise_capability->GetResolve().IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Executor: resolve should be undefine!", JSTaggedValue::Undefined());
    }
    // 4. If promiseCapability.[[Reject]] is not undefined, throw a TypeError exception.
    if (!promise_capability->GetReject().IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Executor: reject should be undefine!", JSTaggedValue::Undefined());
    }
    // 5. Set promiseCapability.[[Resolve]] to resolve.
    // 6. Set promiseCapability.[[Reject]] to reject.
    JSHandle<JSTaggedValue> resolve = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> reject = builtins_common::GetCallArg(argv, 1);
    promise_capability->SetResolve(thread, resolve);
    promise_capability->SetReject(thread, reject);
    // 7. Return undefined.
    return JSTaggedValue::Undefined();
}

// es6 25.4.4.1.2 Promise.all Resolve Element Functions
JSTaggedValue promise_handler::ResolveElementFunction(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseHandler, ResolveElementFunction);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSPromiseAllResolveElementFunction> func =
        JSHandle<JSPromiseAllResolveElementFunction>::Cast(builtins_common::GetConstructor(argv));
    // 1. Let alreadyCalled be the value of F's [[AlreadyCalled]] internal slot.
    JSHandle<PromiseRecord> already_called =
        JSHandle<PromiseRecord>::Cast(JSHandle<JSTaggedValue>(thread, func->GetAlreadyCalled()));
    // 2. If alreadyCalled.[[value]] is true, return undefined.
    if (already_called->GetValue().IsTrue()) {
        return JSTaggedValue::Undefined();
    }
    // 3. Set alreadyCalled.[[value]] to true.
    already_called->SetValue(thread, JSTaggedValue::True());
    // 4. Let index be the value of F's [[Index]] internal slot.
    // 5. Let values be the value of F's [[Values]] internal slot.
    JSHandle<PromiseRecord> values = JSHandle<PromiseRecord>::Cast(JSHandle<JSTaggedValue>(thread, func->GetValues()));
    // 6. Let promiseCapability be the value of F's [[Capabilities]] internal slot.
    JSHandle<PromiseCapability> capa =
        JSHandle<PromiseCapability>::Cast(JSHandle<JSTaggedValue>(thread, func->GetCapabilities()));
    // 7. Let remainingElementsCount be the value of F's [[RemainingElements]] internal slot.
    JSHandle<PromiseRecord> remain_cnt =
        JSHandle<PromiseRecord>::Cast(JSHandle<JSTaggedValue>(thread, func->GetRemainingElements()));
    // 8. Set values[index] to x.
    JSHandle<TaggedArray> array_values =
        JSHandle<TaggedArray>::Cast(JSHandle<JSTaggedValue>(thread, values->GetValue()));
    auto index = JSTaggedValue::ToUint32(thread, JSHandle<JSTaggedValue>(thread, func->GetIndex()));
    array_values->Set(thread, index, builtins_common::GetCallArg(argv, 0).GetTaggedValue());
    // 9. Set remainingElementsCount.[[value]] to remainingElementsCount.[[value]] - 1.
    remain_cnt->SetValue(thread, --JSTaggedNumber(remain_cnt->GetValue()));
    // 10. If remainingElementsCount.[[value]] is 0,
    if (remain_cnt->GetValue().IsZero()) {
        // a. Let valuesArray be CreateArrayFromList(values).
        JSHandle<JSArray> js_array_values = JSArray::CreateArrayFromList(thread, array_values);
        // b. Return Call(promiseCapability.[[Resolve]], undefined, «valuesArray»).
        JSHandle<JSTaggedValue> capa_resolve(thread, capa->GetResolve());
        JSHandle<JSTaggedValue> undefine = global_const->GetHandledUndefined();

        auto info = NewRuntimeCallInfo(thread, capa_resolve, undefine, JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(js_array_values);
        return JSFunction::Call(info.Get());
    }
    // 11. Return undefined.
    return JSTaggedValue::Undefined();
}

// es2017 25.5.5.4 AsyncFunction Awaited Fulfilled
JSTaggedValue promise_handler::AsyncAwaitFulfilled(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseHandler, AsyncAwaitFulfilled);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());

    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSAsyncAwaitStatusFunction> func(builtins_common::GetConstructor(argv));
    return JSAsyncAwaitStatusFunction::AsyncFunctionAwaitFulfilled(argv->GetThread(), func, value).GetTaggedValue();
}

// es2017 25.5.5.5 AsyncFunction Awaited Rejected
JSTaggedValue promise_handler::AsyncAwaitRejected(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), PromiseHandler, AsyncAwaitRejected);
    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());

    JSHandle<JSTaggedValue> reason = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSAsyncAwaitStatusFunction> func(builtins_common::GetConstructor(argv));
    return JSAsyncAwaitStatusFunction::AsyncFunctionAwaitRejected(argv->GetThread(), func, reason).GetTaggedValue();
}
}  // namespace panda::ecmascript::builtins
