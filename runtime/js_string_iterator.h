/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_STRING_ITERATOR_H
#define ECMASCRIPT_JS_STRING_ITERATOR_H

#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript {
class JSStringIterator : public JSObject {
public:
    CAST_CHECK(JSStringIterator, IsStringIterator);

    static JSHandle<JSStringIterator> CreateStringIterator(const JSThread *thread, const JSHandle<EcmaString> &string);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, IteratedString)
    ACCESSORS(1, StringIteratorNextIndex)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_JS_STRING_ITERATOR_H
