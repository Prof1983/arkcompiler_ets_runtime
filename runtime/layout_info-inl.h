/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_LAYOUT_INFO_INL_H
#define ECMASCRIPT_LAYOUT_INFO_INL_H

#include "plugins/ecmascript/runtime/layout_info.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/ic/properties_cache-inl.h"

namespace panda::ecmascript {
inline int32_t LayoutInfo::GetPropertiesCapacity() const
{
    return static_cast<int>((GetLength() - ELEMENTS_START_INDEX) >> 1U);
}

inline int32_t LayoutInfo::NumberOfElements() const
{
    return TaggedArray::Get(NUMBER_OF_PROPERTIES_INDEX).GetInt();
}

inline void LayoutInfo::SetNumberOfElements(const JSThread *thread, int32_t properties)
{
    return TaggedArray::Set(thread, NUMBER_OF_PROPERTIES_INDEX, JSTaggedValue(properties));
}

inline uint32_t LayoutInfo::GetKeyIndex(int32_t index) const
{
    return ELEMENTS_START_INDEX + (static_cast<uint32_t>(index) << 1U);
}

inline uint32_t LayoutInfo::GetAttrIndex(int32_t index) const
{
    return ELEMENTS_START_INDEX + (static_cast<uint32_t>(index) << 1U) + 1;
}

inline void LayoutInfo::SetPropertyInit(const JSThread *thread, int32_t index, const JSTaggedValue &key,
                                        const PropertyAttributes &attr)
{
    uint32_t fixed_idx = GetKeyIndex(index);
    TaggedArray::Set(thread, fixed_idx, key);
    TaggedArray::Set(thread, fixed_idx + 1, attr.GetNormalTagged());
}

inline void LayoutInfo::SetNormalAttr(const JSThread *thread, int32_t index, const PropertyAttributes &attr)
{
    uint32_t fixed_idx = GetAttrIndex(index);
    PropertyAttributes old_attr(TaggedArray::Get(fixed_idx));
    old_attr.SetNormalAttr(attr.GetNormalAttr());
    TaggedArray::Set(thread, fixed_idx, old_attr.GetTaggedValue());
}

inline JSTaggedValue LayoutInfo::GetKey(int32_t index) const
{
    uint32_t fixed_idx = GetKeyIndex(index);
    return TaggedArray::Get(fixed_idx);
}

inline PropertyAttributes LayoutInfo::GetAttr(int32_t index) const
{
    uint32_t fixed_idx = GetAttrIndex(index);
    return PropertyAttributes(TaggedArray::Get(fixed_idx));
}

inline JSTaggedValue LayoutInfo::GetSortedKey(int32_t index) const
{
    uint32_t fixed_idx = GetSortedIndex(index);
    return GetKey(fixed_idx);
}

inline uint32_t LayoutInfo::GetSortedIndex(int32_t index) const
{
    return GetAttr(index).GetSortedIndex();
}

inline void LayoutInfo::SetSortedIndex(const JSThread *thread, int32_t index, int32_t sorted_index)
{
    uint32_t fixed_idx = GetAttrIndex(index);
    PropertyAttributes attr(TaggedArray::Get(fixed_idx));
    attr.SetSortedIndex(sorted_index);
    TaggedArray::Set(thread, fixed_idx, attr.GetTaggedValue());
}

inline int32_t LayoutInfo::FindElementWithCache(JSThread *thread, JSHClass *cls, JSTaggedValue key,
                                                int32_t properties_number)
{
    ASSERT(NumberOfElements() >= properties_number);
    const int32_t max_elements_liner_search = 9;  // 9: Builtins Object properties number is nine;
    if (properties_number <= max_elements_liner_search) {
        Span<struct Properties> sp(GetProperties(), properties_number);
        for (int32_t i = 0; i < properties_number; i++) {
            if (sp[i].key == key) {
                return i;
            }
        }
        return -1;
    }

    PropertiesCache *cache = thread->GetPropertiesCache();
    int32_t index = cache->Get(cls, key);
    if (index == PropertiesCache::NOT_FOUND) {
        index = BinarySearch(key, properties_number);
        cache->Set(cls, key, index);
    }
    return index;
}

inline int32_t LayoutInfo::BinarySearch(JSTaggedValue key, int32_t properties_number)
{
    ASSERT(NumberOfElements() >= properties_number);
    int32_t low = 0;
    int32_t elements = NumberOfElements();
    int32_t high = elements - 1;
    uint32_t key_hash = key.GetKeyHashCode();

    ASSERT(low <= high);

    while (low <= high) {
        int32_t mid = low + (high - low) / 2;  // 2: half
        JSTaggedValue mid_key = GetSortedKey(mid);
        uint32_t mid_hash = mid_key.GetKeyHashCode();
        if (mid_hash > key_hash) {
            high = mid - 1;
        } else if (mid_hash < key_hash) {
            low = mid + 1;
        } else {
            int32_t sort_index = GetSortedIndex(mid);
            JSTaggedValue current_key = GetKey(sort_index);
            if (current_key == key) {
                return sort_index < properties_number ? sort_index : -1;
            }
            int32_t mid_left = mid;
            int32_t mid_right = mid;
            while (mid_left - 1 >= 0) {
                sort_index = GetSortedIndex(--mid_left);
                current_key = GetKey(sort_index);
                if (current_key.GetKeyHashCode() == key_hash) {
                    if (current_key == key) {
                        return sort_index < properties_number ? sort_index : -1;
                    }
                } else {
                    break;
                }
            }
            while (mid_right + 1 < elements) {
                sort_index = GetSortedIndex(++mid_right);
                current_key = GetKey(sort_index);
                if (current_key.GetKeyHashCode() == key_hash) {
                    if (current_key == key) {
                        return sort_index < properties_number ? sort_index : -1;
                    }
                } else {
                    break;
                }
            }
            return -1;
        }
    }
    return -1;
}
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_LAYOUT_INFO_INL_H
