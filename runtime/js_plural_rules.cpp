/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_plural_rules.h"

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/global_env_constants.h"
#include "plugins/ecmascript/runtime/js_number_format.h"

namespace panda::ecmascript {
enum class TypeOption : uint8_t { CARDINAL = 0x01, ORDINAL, EXCEPTION };

constexpr int32_t STRING_SEPARATOR_LENGTH = 4;

icu::number::LocalizedNumberFormatter *JSPluralRules::GetIcuNumberFormatter() const
{
    ASSERT(GetIcuNF().IsJSNativePointer());
    auto result = JSNativePointer::Cast(GetIcuNF().GetTaggedObject())->GetExternalPointer();
    return reinterpret_cast<icu::number::LocalizedNumberFormatter *>(result);
}

void JSPluralRules::FreeIcuNumberFormatter(void *pointer, [[maybe_unused]] void *hint)
{
    if (pointer == nullptr) {
        return;
    }
    auto icu_number_formatter = reinterpret_cast<icu::number::LocalizedNumberFormatter *>(pointer);
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(icu_number_formatter);
}

void JSPluralRules::SetIcuNumberFormatter(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                          const icu::number::LocalizedNumberFormatter &icu_nf,
                                          const DeleteEntryPoint &callback)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    icu::number::LocalizedNumberFormatter *icu_pointer =
        Runtime::GetCurrent()->GetInternalAllocator()->New<icu::number::LocalizedNumberFormatter>(icu_nf);
    ASSERT(icu_pointer != nullptr);
    JSTaggedValue data = plural_rules->GetIcuNF();
    if (data.IsHeapObject() && data.IsJSNativePointer()) {
        JSNativePointer *native = JSNativePointer::Cast(data.GetTaggedObject());
        native->ResetExternalPointer(icu_pointer);
        return;
    }
    JSHandle<JSNativePointer> pointer = factory->NewJSNativePointer(icu_pointer);
    pointer->SetDeleter(callback);
    plural_rules->SetIcuNF(thread, pointer.GetTaggedValue());
    ecma_vm->PushToArrayDataList(*pointer);
}

icu::PluralRules *JSPluralRules::GetIcuPluralRules() const
{
    ASSERT(GetIcuPR().IsJSNativePointer());
    auto result = JSNativePointer::Cast(GetIcuPR().GetTaggedObject())->GetExternalPointer();
    return reinterpret_cast<icu::PluralRules *>(result);
}

void JSPluralRules::FreeIcuPluralRules(void *pointer, [[maybe_unused]] void *hint)
{
    if (pointer == nullptr) {
        return;
    }
    auto icu_plural_rules = reinterpret_cast<icu::PluralRules *>(pointer);
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(icu_plural_rules);
}

void JSPluralRules::SetIcuPluralRules(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                      const icu::PluralRules &icu_pr, const DeleteEntryPoint &callback)
{
    [[maybe_unused]] EcmaHandleScope scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    icu::PluralRules *icu_pointer = Runtime::GetCurrent()->GetInternalAllocator()->New<icu::PluralRules>(icu_pr);
    ASSERT(icu_pointer != nullptr);
    JSTaggedValue data = plural_rules->GetIcuPR();
    if (data.IsHeapObject() && data.IsJSNativePointer()) {
        JSNativePointer *native = JSNativePointer::Cast(data.GetTaggedObject());
        native->ResetExternalPointer(icu_pointer);
        return;
    }
    JSHandle<JSNativePointer> pointer = factory->NewJSNativePointer(icu_pointer);
    pointer->SetDeleter(callback);
    plural_rules->SetIcuPR(thread, pointer.GetTaggedValue());
    ecma_vm->PushToArrayDataList(*pointer);
}

JSHandle<TaggedArray> JSPluralRules::BuildLocaleSet(JSThread *thread,
                                                    const std::set<std::string> &icu_available_locales)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    JSHandle<TaggedArray> locales = factory->NewTaggedArray(icu_available_locales.size());
    int32_t index = 0;

    for (const std::string &locale : icu_available_locales) {
        JSHandle<EcmaString> locale_str = factory->NewFromStdString(locale);
        locales->Set(thread, index++, locale_str);
    }
    return locales;
}

bool GetNextLocale(icu::StringEnumeration *locales, std::string &locale_str, int32_t *len)
{
    UErrorCode status = U_ZERO_ERROR;
    const char *locale = nullptr;
    locale = locales->next(len, status);
    if ((U_SUCCESS(status) == 0) || locale == nullptr) {
        locale_str = "";
        return false;
    }
    locale_str = std::string(locale);
    return true;
}

JSHandle<TaggedArray> JSPluralRules::GetAvailableLocales(JSThread *thread)
{
    UErrorCode status = U_ZERO_ERROR;
    std::unique_ptr<icu::StringEnumeration> locales(icu::PluralRules::getAvailableLocales(status));
    ASSERT(U_SUCCESS(status));
    std::set<std::string> set;
    std::string locale_str;
    int32_t len = 0;
    while (GetNextLocale(locales.get(), locale_str, &len)) {
        if (len >= STRING_SEPARATOR_LENGTH) {
            std::replace(locale_str.begin(), locale_str.end(), '_', '-');
        }
        set.insert(locale_str);
    }
    return BuildLocaleSet(thread, set);
}

// InitializePluralRules ( pluralRules, locales, options )
JSHandle<JSPluralRules> JSPluralRules::InitializePluralRules(JSThread *thread,
                                                             const JSHandle<JSPluralRules> &plural_rules,
                                                             const JSHandle<JSTaggedValue> &locales,
                                                             const JSHandle<JSTaggedValue> &options)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    auto global_const = thread->GlobalConstants();

    // 1. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSPluralRules, thread);

    // 2&3. If options is undefined, then Let options be ObjectCreate(null). else Let options be ? ToObject(options).
    JSHandle<JSObject> pr_options;
    if (!options->IsUndefined()) {
        pr_options = JSTaggedValue::ToObject(thread, options);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSPluralRules, thread);
    } else {
        pr_options = factory->OrdinaryNewJSObjectCreate(JSHandle<JSTaggedValue>(thread, JSTaggedValue::Null()));
    }

    // 5. Let matcher be ? GetOption(options, "localeMatcher", "string", « "lookup", "best fit" », "best fit").
    LocaleMatcherOption matcher =
        JSLocale::GetOptionOfString(thread, pr_options, global_const->GetHandledLocaleMatcherString(),
                                    {LocaleMatcherOption::LOOKUP, LocaleMatcherOption::BEST_FIT},
                                    {"lookup", "best fit"}, LocaleMatcherOption::BEST_FIT);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSPluralRules, thread);

    // 7. Let t be ? GetOption(options, "type", "string", « "cardinal", "ordinal" », "cardinal").
    JSHandle<JSTaggedValue> property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledTypeString());
    TypeOption type =
        JSLocale::GetOptionOfString(thread, pr_options, property, {TypeOption::CARDINAL, TypeOption::ORDINAL},
                                    {"cardinal", "ordinal"}, TypeOption::CARDINAL);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSPluralRules, thread);

    // set pluralRules.[[type]] to type
    JSHandle<JSTaggedValue> type_value;
    if (type == TypeOption::CARDINAL) {
        type_value = global_const->GetHandledCardinalString();
    } else {
        type_value = global_const->GetHandledOrdinalString();
    }
    plural_rules->SetType(thread, type_value.GetTaggedValue());

    // Let r be ResolveLocale(%PluralRules%.[[AvailableLocales]], requestedLocales, opt,
    // %PluralRules%.[[RelevantExtensionKeys]], localeData).
    JSHandle<TaggedArray> available_locales;
    if (requested_locales->GetLength() == 0) {
        available_locales = factory->EmptyArray();
    } else {
        available_locales = GetAvailableLocales(thread);
    }
    std::set<std::string> relevant_extension_keys {""};
    ResolvedLocale r =
        JSLocale::ResolveLocale(thread, available_locales, requested_locales, matcher, relevant_extension_keys);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSPluralRules, thread);
    icu::Locale icu_locale = r.locale_data;

    // Get ICU numberFormatter with given locale
    icu::number::LocalizedNumberFormatter icu_number_formatter =
        icu::number::NumberFormatter::withLocale(icu_locale).roundingMode(UNUM_ROUND_HALFUP);

    bool sucess = true;
    UErrorCode status = U_ZERO_ERROR;
    UPluralType icu_type = UPLURAL_TYPE_CARDINAL;
    // Trans typeOption to ICU typeOption
    switch (type) {
        case TypeOption::ORDINAL:
            icu_type = UPLURAL_TYPE_ORDINAL;
            break;
        case TypeOption::CARDINAL:
            icu_type = UPLURAL_TYPE_CARDINAL;
            break;
        default:
            UNREACHABLE();
    }
    std::unique_ptr<icu::PluralRules> icu_plural_rules(icu::PluralRules::forLocale(icu_locale, icu_type, status));
    if (U_FAILURE(status) != 0) {  // NOLINT(readability-implicit-bool-conversion)
        sucess = false;
    }

    // Trans typeOption to ICU typeOption
    if (!sucess || icu_plural_rules == nullptr) {
        icu::Locale no_extension_locale(icu_locale.getBaseName());
        status = U_ZERO_ERROR;
        switch (type) {
            case TypeOption::ORDINAL:
                icu_type = UPLURAL_TYPE_ORDINAL;
                break;
            case TypeOption::CARDINAL:
                icu_type = UPLURAL_TYPE_CARDINAL;
                break;
            default:
                UNREACHABLE();
        }
        icu_plural_rules.reset(icu::PluralRules::forLocale(icu_locale, icu_type, status));
    }
    if (U_FAILURE(status) || icu_plural_rules == nullptr) {  // NOLINT(readability-implicit-bool-conversion)
        THROW_RANGE_ERROR_AND_RETURN(thread, "cannot create icuPluralRules", plural_rules);
    }

    // 9. Perform ? SetNumberFormatDigitOptions(pluralRules, options, 0, 3, "standard").
    JSLocale::SetNumberFormatDigitOptions(thread, plural_rules, JSHandle<JSTaggedValue>::Cast(pr_options), MNFD_DEFAULT,
                                          MXFD_DEFAULT, NotationOption::STANDARD);
    icu_number_formatter = JSNumberFormat::SetICUFormatterDigitOptions(icu_number_formatter, plural_rules);

    // Set pluralRules.[[IcuPluralRules]] to icuPluralRules
    SetIcuPluralRules(thread, plural_rules, *icu_plural_rules, JSPluralRules::FreeIcuPluralRules);

    // Set pluralRules.[[IcuNumberFormat]] to icuNumberFormatter
    SetIcuNumberFormatter(thread, plural_rules, icu_number_formatter, JSPluralRules::FreeIcuNumberFormatter);

    // 12. Set pluralRules.[[Locale]] to the value of r.[[locale]].
    JSHandle<EcmaString> locale_str = JSLocale::ToLanguageTag(thread, icu_locale);
    plural_rules->SetLocale(thread, locale_str.GetTaggedValue());

    // 13. Return pluralRules.
    return plural_rules;
}

JSHandle<EcmaString> FormatNumericToString(JSThread *thread, const icu::number::LocalizedNumberFormatter *icu_formatter,
                                           const icu::PluralRules *icu_plural_rules, double n)
{
    UErrorCode status = U_ZERO_ERROR;
    icu::number::FormattedNumber formatted = icu_formatter->formatDouble(n, status);
    if (U_FAILURE(status) != 0) {  // NOLINT(readability-implicit-bool-conversion)
        JSHandle<JSTaggedValue> exception(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "invalid resolve number", JSHandle<EcmaString>::Cast(exception));
    }

    icu::UnicodeString u_string = icu_plural_rules->select(formatted, status);
    if (U_FAILURE(status) != 0) {  // NOLINT(readability-implicit-bool-conversion)
        JSHandle<JSTaggedValue> exception(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "invalid resolve number", JSHandle<EcmaString>::Cast(exception));
    }

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> result =
        factory->NewFromUtf16(reinterpret_cast<const uint16_t *>(u_string.getBuffer()), u_string.length());
    return result;
}
JSHandle<EcmaString> JSPluralRules::ResolvePlural(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                                  double n)
{
    icu::PluralRules *icu_plural_rules = plural_rules->GetIcuPluralRules();
    icu::number::LocalizedNumberFormatter *icu_formatter = plural_rules->GetIcuNumberFormatter();
    if (icu_plural_rules == nullptr || icu_formatter == nullptr) {
        return JSHandle<EcmaString>(thread, JSTaggedValue::Undefined());
    }

    JSHandle<EcmaString> result = FormatNumericToString(thread, icu_formatter, icu_plural_rules, n);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(EcmaString, thread);
    return result;
}

void JSPluralRules::ResolvedOptions(JSThread *thread, const JSHandle<JSPluralRules> &plural_rules,
                                    const JSHandle<JSObject> &options)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    auto global_const = thread->GlobalConstants();

    // [[Locale]]
    JSHandle<JSTaggedValue> property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledLocaleString());
    JSHandle<EcmaString> locale(thread, plural_rules->GetLocale());
    PropertyDescriptor locale_desc(thread, JSHandle<JSTaggedValue>::Cast(locale), true, true, true);
    JSObject::DefineOwnProperty(thread, options, property, locale_desc);

    // [[type]]
    property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledTypeString());
    PropertyDescriptor type_desc(thread, JSHandle<JSTaggedValue>(thread, plural_rules->GetType()), true, true, true);
    JSObject::DefineOwnProperty(thread, options, property, type_desc);

    // [[MinimumIntegerDigits]]
    property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledMinimumIntegerDigitsString());
    JSHandle<JSTaggedValue> minimum_integer_digits(thread, plural_rules->GetMinimumIntegerDigits());
    JSObject::CreateDataPropertyOrThrow(thread, options, property, minimum_integer_digits);

    RoundingType rounding_type = static_cast<RoundingType>(plural_rules->GetRoundingType().GetInt());
    if (rounding_type == RoundingType::SIGNIFICANTDIGITS) {
        // [[MinimumSignificantDigits]]
        property = global_const->GetHandledMinimumSignificantDigitsString();
        JSHandle<JSTaggedValue> minimum_significant_digits(thread, plural_rules->GetMinimumSignificantDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, minimum_significant_digits);
        // [[MaximumSignificantDigits]]
        property = global_const->GetHandledMaximumSignificantDigitsString();
        JSHandle<JSTaggedValue> maximum_significant_digits(thread, plural_rules->GetMaximumSignificantDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, maximum_significant_digits);
    } else {
        // [[MinimumFractionDigits]]
        property = global_const->GetHandledMinimumFractionDigitsString();
        JSHandle<JSTaggedValue> minimum_fraction_digits(thread, plural_rules->GetMinimumFractionDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, minimum_fraction_digits);
        // [[MaximumFractionDigits]]
        property = global_const->GetHandledMaximumFractionDigitsString();
        JSHandle<JSTaggedValue> maximum_fraction_digits(thread, plural_rules->GetMaximumFractionDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, maximum_fraction_digits);
    }

    // 5. Let pluralCategories be a List of Strings representing the possible results of PluralRuleSelect
    // for the selected locale pr.[[Locale]]. This List consists of unique String values,
    // from the the list "zero", "one", "two", "few", "many" and "other",
    // that are relevant for the locale whose localization is specified in LDML Language Plural Rules.
    UErrorCode status = U_ZERO_ERROR;
    icu::PluralRules *icu_plural_rules = plural_rules->GetIcuPluralRules();
    ASSERT(icu_plural_rules != nullptr);
    std::unique_ptr<icu::StringEnumeration> categories(icu_plural_rules->getKeywords(status));
    int32_t count = categories->count(status);
    ASSERT(U_SUCCESS(status));
    JSHandle<TaggedArray> plural_categories = factory->NewTaggedArray(count);
    for (int32_t i = 0; i < count; i++) {
        const icu::UnicodeString *category = categories->snext(status);
        ASSERT(U_SUCCESS(status));
        JSHandle<EcmaString> value = JSLocale::IcuToString(thread, *category);
        plural_categories->Set(thread, i, value);
    }

    // 6. Perform ! CreateDataProperty(options, "pluralCategories", CreateArrayFromList(pluralCategories)).
    property = global_const->GetHandledPluralCategoriesString();
    JSHandle<JSArray> js_plural_categories = JSArray::CreateArrayFromList(thread, plural_categories);
    JSObject::CreateDataPropertyOrThrow(thread, options, property, JSHandle<JSTaggedValue>::Cast(js_plural_categories));
}
}  // namespace panda::ecmascript
