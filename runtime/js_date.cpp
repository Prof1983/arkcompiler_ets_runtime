/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_date.h"
#include <ctime>
#include <regex>
#include <sys/time.h>
#include "base/builtins_base.h"

namespace panda::ecmascript {
using NumberHelper = base::NumberHelper;
void DateUtils::TransferTimeToDate(int64_t time_ms, std::array<int64_t, DATE_LENGTH> *date)
{
    (*date)[HOUR] = Mod(time_ms, MS_PER_DAY);                                // ms from hour, minutes, second, ms
    (*date)[DAYS] = (time_ms - (*date)[HOUR]) / MS_PER_DAY;                  // days from year, month, day
    (*date)[MS] = (*date)[HOUR] % MS_PER_SECOND;                             // ms
    (*date)[HOUR] = ((*date)[HOUR] - (*date)[MS]) / MS_PER_SECOND;           // s from hour, minutes, second
    (*date)[SEC] = (*date)[HOUR] % SEC_PER_MINUTE;                           // second
    (*date)[HOUR] = ((*date)[HOUR] - (*date)[SEC]) / SEC_PER_MINUTE;         // min from hour, minutes
    (*date)[MIN] = (*date)[HOUR] % SEC_PER_MINUTE;                           // min
    (*date)[HOUR] = ((*date)[HOUR] - (*date)[MIN]) / SEC_PER_MINUTE;         // hour
    (*date)[WEEKDAY] = Mod(((*date)[DAYS] + LEAP_NUMBER[0]), DAY_PER_WEEK);  // weekday
    (*date)[YEAR] = GetYearFromDays(&((*date)[DAYS]));                       // year
}
// static
bool DateUtils::IsLeap(int64_t year)
{
    return year % LEAP_NUMBER[0] == 0 && (year % LEAP_NUMBER[1] != 0 || year % LEAP_NUMBER[2] == 0);  // 2: means index
}

// static
int64_t DateUtils::GetDaysInYear(int64_t year)
{
    int64_t number;
    number = IsLeap(year) ? (DAYS_IN_YEAR + 1) : DAYS_IN_YEAR;
    return number;
}

// static
int64_t DateUtils::GetDaysFromYear(int64_t year)
{
    return DAYS_IN_YEAR * (year - YEAR_NUMBER[0]) + FloorDiv(year - YEAR_NUMBER[1], LEAP_NUMBER[0]) -
           FloorDiv(year - YEAR_NUMBER[2], LEAP_NUMBER[1]) +  // 2: year index
           FloorDiv(year - YEAR_NUMBER[3], LEAP_NUMBER[2]);   // 3, 2: year index
}

// static
int64_t DateUtils::FloorDiv(int64_t a, int64_t b)
{
    ASSERT(b != 0);
    int64_t m = a % b;
    int64_t res = m < 0 ? ((a - m - b) / b) : ((a - m) / b);
    return res;
}

// static
int64_t DateUtils::GetYearFromDays(int64_t *days)
{
    int64_t real_day;
    int64_t day_temp = 0;
    int64_t d = *days;
    int64_t year = FloorDiv(d * APPROXIMATION_NUMBER[0], APPROXIMATION_NUMBER[1]) + YEAR_NUMBER[0];
    real_day = d - GetDaysFromYear(year);
    while (real_day != 0) {
        if (real_day < 0) {
            year--;
        } else {
            day_temp = GetDaysInYear(year);
            if (real_day < day_temp) {
                break;
            }
            year++;
        }
        real_day = d - GetDaysFromYear(year);
    }
    *days = real_day;
    return year;
}

// static
int64_t DateUtils::Mod(int64_t a, int b)
{
    ASSERT(b != 0);
    int64_t m = a % b;
    int64_t res = m < 0 ? (m + b) : m;
    return res;
}

// static
// 20.4.1.11
double JSDate::MakeTime(double hour, double min, double sec, double ms)
{
    if (std::isfinite(hour) && std::isfinite(min) && std::isfinite(sec) && std::isfinite(ms)) {
        double hour_integer = NumberHelper::TruncateDouble(hour);
        double min_integer = NumberHelper::TruncateDouble(min);
        double sec_integer = NumberHelper::TruncateDouble(sec);
        double ms_integer = NumberHelper::TruncateDouble(ms);
        return hour_integer * MS_PER_HOUR + min_integer * MS_PER_MINUTE + sec_integer * MS_PER_SECOND + ms_integer;
    }
    return base::NAN_VALUE;
}

// static
// 20.4.1.12
double JSDate::MakeDay(double year, double month, double date)
{
    if (std::isfinite(year) && std::isfinite(month) && std::isfinite(date)) {
        double year_integer = NumberHelper::TruncateDouble(year);
        double month_integer = NumberHelper::TruncateDouble(month);
        int64_t y = static_cast<int64_t>(year_integer) + static_cast<int64_t>(month_integer / MOUTH_PER_YEAR);
        int64_t m = static_cast<int64_t>(month_integer) % MOUTH_PER_YEAR;
        if (m < 0) {
            m += MOUTH_PER_YEAR;
            y -= 1;
        }

        int64_t days = DateUtils::GetDaysFromYear(y);
        int index = DateUtils::IsLeap(year) ? 1 : 0;
        days += DAYS_FROM_MONTH[index][m];
        return static_cast<double>(days - 1) + NumberHelper::TruncateDouble(date);
    }
    return base::NAN_VALUE;
}

// static
// 20.4.1.13
double JSDate::MakeDate(double day, double time)
{
    if (std::isfinite(day) && std::isfinite(time)) {
        return time + day * MS_PER_DAY;
    }
    return base::NAN_VALUE;
}

// static
// 20.4.1.14
double JSDate::TimeClip(double time)
{
    if (-MAX_TIME_IN_MS <= time && time <= MAX_TIME_IN_MS) {
        return NumberHelper::TruncateDouble(time);
    }
    return base::NAN_VALUE;
}

// 20.4.1.8
double JSDate::LocalTime(double time_ms) const
{
    return time_ms + GetLocalOffsetFromOS(time_ms, true);
}

// 20.4.1.9
double JSDate::UTCTime(double time_ms) const
{
    return time_ms - GetLocalOffsetFromOS(time_ms, false);
}

// static
int JSDate::GetSignedNumFromString(const PandaString &str, int len, int *index)
{
    int res = 0;
    GetNumFromString(str, len, index, &res);
    if (str.at(0) == NEG) {
        return -res;
    }
    return res;
}

// static
bool JSDate::GetNumFromString(const PandaString &str, int len, int *index, int *num)
{
    int index_str = *index;
    char one_byte = 0;
    while (index_str < len) {
        one_byte = str.at(index_str);
        if (one_byte >= '0' && one_byte <= '9') {
            break;
        }
        index_str++;
    }
    if (index_str >= len) {
        return false;
    }
    int value = 0;
    while (index_str < len) {
        one_byte = str.at(index_str);
        int val = one_byte - '0';
        if (val >= 0 && val <= NUM_NINE) {
            value = value * TEN + val;
            index_str++;
        } else {
            break;
        }
    }
    *num = value;
    *index = index_str;
    return true;
}

// 20.4.1.7
int64_t JSDate::GetLocalOffsetInMin(const JSThread *thread, int64_t time_ms, bool is_local)
{
    if (!is_local) {
        return 0;
    }
    double local_offset = this->GetLocalOffset().GetDouble();
    if (local_offset == MAX_DOUBLE) {
        local_offset = static_cast<double>(GetLocalOffsetFromOS(time_ms, is_local));
        SetLocalOffset(thread, JSTaggedValue(local_offset));
    }
    return local_offset;
}

// static
JSTaggedValue JSDate::LocalParseStringToMs(const PandaString &str)
{
    int year = 0;
    int month = 0;
    int date = 1;
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    int ms = 0;
    int index = 0;
    int len = str.length();
    bool is_local = false;
    PandaString::size_type index_gmt;
    PandaString::size_type index_plus = PandaString::npos;
    std::array<PandaString, MOUTH_PER_YEAR> month_name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    int local_time = 0;
    int local_hours = 0;
    int local_minutes = 0;
    int64_t local_ms = 0;
    PandaString::size_type local_space;
    local_space = str.find(' ', index);
    PandaString str_month = str.substr(local_space + 1, LENGTH_MONTH_NAME);
    for (int i = 0; i < MOUTH_PER_YEAR; i++) {
        if (str_month == month_name[i]) {
            month = i;
            break;
        }
    }
    index += (LENGTH_MONTH_NAME + 1);
    GetNumFromString(str, len, &index, &date);
    GetNumFromString(str, len, &index, &year);
    index_gmt = str.find("GMT", index);
    if (index_gmt == PandaString::npos) {
        GetNumFromString(str, len, &index, &hours);
        GetNumFromString(str, len, &index, &minutes);
        GetNumFromString(str, len, &index, &seconds);
        is_local = true;
        local_ms -= (GetLocalOffsetFromOS(local_ms, true) * MS_PER_MINUTE);
    } else {
        index_plus = str.find(PLUS, index_gmt);
        int index_local = static_cast<int>(index_gmt);
        GetNumFromString(str, index_gmt, &index, &hours);
        GetNumFromString(str, index_gmt, &index, &minutes);
        GetNumFromString(str, index_gmt, &index, &seconds);
        GetNumFromString(str, len, &index_local, &local_time);
        local_hours = local_time / HUNDRED;
        local_minutes = local_time % HUNDRED;
        local_ms = static_cast<int64_t>(MakeTime(local_hours, local_minutes, 0, 0));
        if (index_plus != PandaString::npos) {
            local_ms = -local_ms;
        }
    }
    double day = MakeDay(year, month, date);
    double time = MakeTime(hours, minutes, seconds, ms);
    double time_value = TimeClip(MakeDate(day, time));
    if (std::isnan(time_value)) {
        return JSTaggedValue(time_value);
    }
    if (is_local && time_value < CHINA_1901_MS && (-local_ms / MS_PER_MINUTE) == CHINA_AFTER_1901_MIN) {
        time_value += static_cast<double>(local_ms - CHINA_BEFORE_1901_MS);
    } else {
        time_value += local_ms;
    }
    return JSTaggedValue(time_value);
}

// static
JSTaggedValue JSDate::UtcParseStringToMs(const PandaString &str)
{
    int year = 0;
    int month = 0;
    int date = 1;
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    int ms = 0;
    int index = 0;
    int len = str.length();
    PandaString::size_type index_gmt;
    PandaString::size_type index_plus = PandaString::npos;
    int local_time = 0;
    int local_hours = 0;
    int local_minutes = 0;
    int64_t local_ms = 0;
    bool is_local = false;
    std::array<PandaString, MOUTH_PER_YEAR> month_name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    GetNumFromString(str, len, &index, &date);
    PandaString str_month = str.substr(index + 1, LENGTH_MONTH_NAME);
    for (int i = 0; i < MOUTH_PER_YEAR; i++) {
        if (str_month == month_name[i]) {
            month = i;
            break;
        }
    }
    index += (LENGTH_MONTH_NAME + 1);
    GetNumFromString(str, len, &index, &year);
    index_gmt = str.find("GMT", index);
    if (index_gmt == PandaString::npos) {
        GetNumFromString(str, len, &index, &hours);
        GetNumFromString(str, len, &index, &minutes);
        GetNumFromString(str, len, &index, &seconds);
        is_local = true;
        local_ms -= (GetLocalOffsetFromOS(local_ms, true) * MS_PER_MINUTE);
    } else {
        index_plus = str.find(PLUS, index_gmt);
        int index_local = static_cast<int>(index_gmt);
        GetNumFromString(str, index_gmt, &index, &hours);
        GetNumFromString(str, index_gmt, &index, &minutes);
        GetNumFromString(str, index_gmt, &index, &seconds);
        GetNumFromString(str, len, &index_local, &local_time);
        local_hours = local_time / HUNDRED;
        local_minutes = local_time % HUNDRED;
        local_ms = static_cast<int64_t>(MakeTime(local_hours, local_minutes, 0, 0));
        if (index_plus != PandaString::npos) {
            local_ms = -local_ms;
        }
    }
    double day = MakeDay(year, month, date);
    double time = MakeTime(hours, minutes, seconds, ms);
    double time_value = TimeClip(MakeDate(day, time));
    if (std::isnan(time_value)) {
        return JSTaggedValue(time_value);
    }
    if (is_local && time_value < CHINA_1901_MS && (-local_ms / MS_PER_MINUTE) == CHINA_AFTER_1901_MIN) {
        time_value += static_cast<double>(local_ms - CHINA_BEFORE_1901_MS);
    } else {
        time_value += local_ms;
    }
    return JSTaggedValue(time_value);
}
// static
JSTaggedValue JSDate::IsoParseStringToMs(const PandaString &str)
{
    char flag = 0;
    int year;
    int month = 1;
    int date = 1;
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    int ms = 0;
    int index = 0;
    int len = str.length();
    year = GetSignedNumFromString(str, len, &index);
    PandaString::size_type index_t = str.find(FLAG_TIME, index);
    PandaString::size_type index_z = str.find(FLAG_UTC, index);
    PandaString::size_type index_end_flag = 0;
    int64_t local_ms = 0;
    if (index_z != PandaString::npos) {
        index_end_flag = index_z;
    } else if (len >= MIN_LENGTH && str.at(len - INDEX_PLUS_NEG) == NEG) {
        index_end_flag = len - INDEX_PLUS_NEG;
        flag = NEG;
    } else if (len >= MIN_LENGTH && str.at(len - INDEX_PLUS_NEG) == PLUS) {
        index_end_flag = len - INDEX_PLUS_NEG;
        flag = PLUS;
    }
    if (index_t != PandaString::npos) {
        if ((index_t - index) == LENGTH_PER_TIME) {
            GetNumFromString(str, len, &index, &month);
        } else if ((index_t - index) == (LENGTH_PER_TIME + LENGTH_PER_TIME)) {
            GetNumFromString(str, len, &index, &month);
            GetNumFromString(str, len, &index, &date);
        }
        GetNumFromString(str, len, &index, &hours);
        GetNumFromString(str, len, &index, &minutes);
        if (index_end_flag > 0) {
            if (index_end_flag - index == LENGTH_PER_TIME) {
                GetNumFromString(str, len, &index, &seconds);
            } else if (index_end_flag - index == (LENGTH_PER_TIME + LENGTH_PER_TIME + 1)) {
                GetNumFromString(str, len, &index, &seconds);
                GetNumFromString(str, len, &index, &ms);
            }
        } else {
            if (len - index == LENGTH_PER_TIME) {
                GetNumFromString(str, len, &index, &seconds);
            } else if (len - index == (LENGTH_PER_TIME + LENGTH_PER_TIME + 1)) {
                GetNumFromString(str, len, &index, &seconds);
                GetNumFromString(str, len, &index, &ms);
            }
        }
    } else {
        GetNumFromString(str, len, &index, &month);
        GetNumFromString(str, len, &index, &date);
    }
    if (index_end_flag > 0) {
        int local_hours = 0;
        int local_minutes = 0;
        if (index_z == PandaString::npos) {
            GetNumFromString(str, len, &index, &local_hours);
            GetNumFromString(str, len, &index, &local_minutes);
            if (flag == PLUS) {
                local_ms = static_cast<int64_t>(-MakeTime(local_hours, local_minutes, 0, 0));
            } else {
                local_ms = static_cast<int64_t>(MakeTime(local_hours, local_minutes, 0, 0));
            }
        }
    }
    if (index_end_flag == 0 && index_t != PandaString::npos) {
        local_ms -= (GetLocalOffsetFromOS(local_ms, true) * MS_PER_MINUTE);
    }

    double day = MakeDay(year, month - 1, date);
    double time = MakeTime(hours, minutes, seconds, ms);
    double time_value = TimeClip(MakeDate(day, time));
    if (std::isnan(time_value)) {
        return JSTaggedValue(time_value);
    }
    if (flag == 0 && time_value < CHINA_1901_MS && (-local_ms / MS_PER_MINUTE) == CHINA_AFTER_1901_MIN) {
        time_value += static_cast<double>(local_ms - CHINA_BEFORE_1901_MS);
    } else {
        time_value += local_ms;
    }
    return JSTaggedValue(time_value);
}

// 20.4.3.2 static
JSTaggedValue JSDate::Parse(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    const PandaString iso_pri_str = "(^|(\\+|-)(\\d{2}))";
    const PandaString iso_date_str =
        "(((\\d{4})-(0?[1-9]|1[0-2])-(0?[1-9]|1[0-9]|2[0-9]|3[0-1]))"
        "|((\\d{4})-(0?[1-9]|1[0-2]))|(\\d{4}))";
    const PandaString iso_time_str =
        "((T([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])"
        "\\.(\\d{3}))|(T([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))|"
        "(T([01][0-9]|2[0-3]):([0-5][0-9]))|(T([01][0-9]|2[0-3])))"
        "($|Z|((\\+|-)(([01][0-9]|2[0-3]):([0-5][0-9]))))";
    const PandaString iso_reg_str = iso_pri_str + iso_date_str + "($|Z|(" + iso_time_str + "))";
    const PandaString utc_date_str =
        "^\\D*(0?[1-9]|1[0-9]|2[0-9]|3[0-1]) "
        "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) (\\d{4})";
    const PandaString time_str =
        "(( ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))|( ([01][0-9]|2[0-3]):([0-5][0-9])))"
        "($| *| GMT *| GMT((\\+|-)(\\d{4})) *)";
    const PandaString utc_reg_str = utc_date_str + "($| *| GMT *| GMT((\\+|-)(\\d{4})) *|(" + time_str + "))";
    const PandaString local_date_str =
        "^[a-zA-Z]* (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) "
        "(0?[1-9]|1[0-9]|2[0-9]|3[0-1]) (\\d{4})";
    const PandaString local_reg_str = local_date_str + "($| *| GMT *| GMT((\\+|-)(\\d{4})) *|(" + time_str + "))";

    std::regex iso_reg(iso_reg_str);
    std::regex utc_reg(utc_reg_str);
    std::regex local_reg(local_reg_str);
    JSHandle<JSTaggedValue> msg = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSTaggedValue> str = JSHandle<JSTaggedValue>::Cast(JSTaggedValue::ToString(thread, msg));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
    PandaString date = ConvertToPandaString(EcmaString::Cast(str->GetTaggedObject()));
    if (std::regex_match(date, iso_reg)) {
        return IsoParseStringToMs(date);
    }
    if (std::regex_match(date, utc_reg)) {
        return UtcParseStringToMs(date);
    }
    if (std::regex_match(date, local_reg)) {
        return LocalParseStringToMs(date);
    }
    return JSTaggedValue(base::NAN_VALUE);
}

// 20.4.3.1
JSTaggedValue JSDate::Now()
{
    // time from now is in ms.
    int64_t ans;
    struct timeval tv {};
    gettimeofday(&tv, nullptr);
    ans = static_cast<int64_t>(tv.tv_sec) * MS_PER_SECOND + (tv.tv_usec / MS_PER_SECOND);
    return JSTaggedValue(static_cast<double>(ans));
}

// 20.4.4.2 static
JSTaggedValue JSDate::UTC(EcmaRuntimeCallInfo *argv)
{
    double year;
    double month = 0;
    double date = 1;
    double hours = 0;
    double minutes = 0;
    double seconds = 0;
    double ms = 0;
    JSThread *thread = argv->GetThread();
    JSHandle<JSTaggedValue> year_arg = builtins_common::GetCallArg(argv, 0);
    JSTaggedNumber year_value = JSTaggedValue::ToNumber(thread, year_arg);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
    if (year_value.IsNumber()) {
        year = year_value.GetNumber();
        if (std::isfinite(year) && !year_value.IsInt()) {
            year = NumberHelper::TruncateDouble(year);
        }
        if (year >= 0 && year <= (HUNDRED - 1)) {
            year = year + NINETEEN_HUNDRED_YEAR;
        }
    } else {
        year = base::NAN_VALUE;
    }
    uint32_t index = 1;
    uint32_t num_args = argv->GetArgsNumber();
    JSTaggedValue res;
    if (num_args > index) {
        JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, index);
        res = JSTaggedValue::ToNumber(thread, value);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        month = res.GetNumber();
        index++;
    }
    if (num_args > index) {
        JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, index);
        res = JSTaggedValue::ToNumber(thread, value);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        date = res.GetNumber();
        index++;
    }
    if (num_args > index) {
        JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, index);
        res = JSTaggedValue::ToNumber(thread, value);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        hours = res.GetNumber();
        index++;
    }
    if (num_args > index) {
        JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, index);
        res = JSTaggedValue::ToNumber(thread, value);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        minutes = res.GetNumber();
        index++;
    }
    if (num_args > index) {
        JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, index);
        res = JSTaggedValue::ToNumber(thread, value);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        seconds = res.GetNumber();
        index++;
    }
    if (num_args > index) {
        JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, index);
        res = JSTaggedValue::ToNumber(thread, value);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::Exception());
        ms = res.GetNumber();
    }
    double day = MakeDay(year, month, date);
    double time = MakeTime(hours, minutes, seconds, ms);
    return JSTaggedValue(TimeClip(MakeDate(day, time)));
}

int64_t JSDate::GetLocalOffsetFromOS(int64_t time_ms, bool is_local)
{
    // Preserve the old behavior for non-ICU implementation by ignoring both timeMs and is_utc.
    if (!is_local) {
        return 0;
    }
    time_ms /= JSDate::THOUSAND;
    time_t tv = std::time(reinterpret_cast<time_t *>(&time_ms));
    struct tm tm {};
    // localtime_r is only suitable for linux.
    struct tm *t = localtime_r(&tv, &tm);
    // tm_gmtoff includes any daylight savings offset.
    return t->tm_gmtoff / SEC_PER_MINUTE;
}

// 20.4.4.10
JSTaggedValue JSDate::GetTime() const
{
    return GetTimeValue();
}

// static
PandaString JSDate::StrToTargetLength(const PandaString &str, int length)
{
    int len;
    if (str[0] == NEG) {
        len = static_cast<int>(str.length() - 1);
    } else {
        len = static_cast<int>(str.length());
    }
    int dif = length - len;
    PandaString sub;
    for (int i = 0; i < dif; i++) {
        sub += '0';
    }
    if (str[0] == NEG) {
        sub = NEG + sub + str.substr(1, len);
    } else {
        sub = sub + str;
    }
    return sub;
}

bool JSDate::GetThisDateValues(std::array<int64_t, DATE_LENGTH> *date, bool is_local) const
{
    double time_ms = this->GetTimeValue().GetDouble();
    if (std::isnan(time_ms)) {
        return false;
    }
    GetDateValues(time_ms, date, is_local);
    return true;
}

// 20.4.4.35
JSTaggedValue JSDate::ToDateString(JSThread *thread) const
{
    std::array<PandaString, MOUTH_PER_YEAR> month_name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    std::array<PandaString, DAY_PER_WEEK> weekday_name = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, true)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString year = StrToTargetLength(ToPandaString(fields[YEAR]), STR_LENGTH_YEAR);
    PandaString day = StrToTargetLength(ToPandaString(fields[DAYS]), STR_LENGTH_OTHERS);
    PandaString str = weekday_name[fields[WEEKDAY]] + SPACE + month_name[fields[MONTH]] + SPACE + day + SPACE + year;
    JSHandle<EcmaString> result = thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str);
    return result.GetTaggedValue();
}

// static
PandaString JSDate::ToDateString(double time_ms)
{
    if (std::isnan(time_ms)) {
        return "Invalid Date";
    }
    std::array<PandaString, MOUTH_PER_YEAR> month_name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    std::array<PandaString, DAY_PER_WEEK> weekday_name = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    std::array<int64_t, DATE_LENGTH> fields = {0};
    GetDateValues(time_ms, &fields, true);
    PandaString local_time;
    int local_min = 0;
    local_min = GetLocalOffsetFromOS(local_min, true);
    if (time_ms < CHINA_BEFORE_1900_MS && local_min == CHINA_AFTER_1901_MIN) {
        local_min = CHINA_BEFORE_1901_MIN;
    }
    if (local_min >= 0) {
        local_time += PLUS;
    } else if (local_min < 0) {
        local_time += NEG;
        local_min = -local_min;
    }
    local_time = local_time + StrToTargetLength(ToPandaString(local_min / MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
    local_time = local_time + StrToTargetLength(ToPandaString(local_min % MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
    PandaString year = ToPandaString(fields[YEAR]);
    year = StrToTargetLength(year, STR_LENGTH_YEAR);
    PandaString weekday = weekday_name[fields[WEEKDAY]];
    PandaString month = month_name[fields[MONTH]];
    PandaString day = StrToTargetLength(ToPandaString(fields[DAYS]), STR_LENGTH_OTHERS);
    PandaString hour = StrToTargetLength(ToPandaString(fields[HOUR]), STR_LENGTH_OTHERS);
    PandaString minute = StrToTargetLength(ToPandaString(fields[MIN]), STR_LENGTH_OTHERS);
    PandaString second = StrToTargetLength(ToPandaString(fields[SEC]), STR_LENGTH_OTHERS);
    PandaString str = weekday + SPACE + month + SPACE + day + SPACE + year + SPACE + hour + COLON + minute + COLON +
                      second + SPACE + "GMT" + local_time;
    return str;
}
// 20.4.4.36
JSTaggedValue JSDate::ToISOString(JSThread *thread) const
{
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, false)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString year = ToPandaString(fields[YEAR]);
    if (year[0] == NEG) {
        year = StrToTargetLength(year, STR_LENGTH_YEAR + STR_LENGTH_OTHERS);
    } else if (year.length() > STR_LENGTH_YEAR) {
        year = PLUS + StrToTargetLength(year, STR_LENGTH_YEAR + STR_LENGTH_OTHERS);
    } else {
        year = StrToTargetLength(year, STR_LENGTH_YEAR);
    }
    PandaString month = StrToTargetLength(ToPandaString(fields[MONTH] + 1), STR_LENGTH_OTHERS);
    PandaString day = StrToTargetLength(ToPandaString(fields[DAYS]), STR_LENGTH_OTHERS);
    PandaString hour = StrToTargetLength(ToPandaString(fields[HOUR]), STR_LENGTH_OTHERS);
    PandaString minute = StrToTargetLength(ToPandaString(fields[MIN]), STR_LENGTH_OTHERS);
    PandaString second = StrToTargetLength(ToPandaString(fields[SEC]), STR_LENGTH_OTHERS);
    PandaString ms = StrToTargetLength(ToPandaString(fields[MS]), STR_LENGTH_OTHERS + 1);
    PandaString str =
        year + NEG + month + NEG + day + FLAG_TIME + hour + COLON + minute + COLON + second + POINT + ms + FLAG_UTC;
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str).GetTaggedValue();
}

PandaString JSDate::GetLocaleTimeStr(const std::array<int64_t, DATE_LENGTH> &fields) const
{
    PandaString hour;
    if (fields[HOUR] > MOUTH_PER_YEAR) {
        hour = ToPandaString(fields[HOUR] - MOUTH_PER_YEAR);
    } else {
        hour = ToPandaString(fields[HOUR]);
    }
    PandaString minute = ToPandaString(fields[MIN]);
    PandaString second = StrToTargetLength(ToPandaString(fields[SEC]), STR_LENGTH_OTHERS);
    PandaString str = hour + COLON + minute + COLON + second;
    if (fields[HOUR] >= MOUTH_PER_YEAR) {
        str = "下午" + str;
    } else {
        str = "上午" + str;
    }
    return str;
}

PandaString JSDate::GetLocaleDateStr(const std::array<int64_t, DATE_LENGTH> &fields) const
{
    PandaString year;
    if (fields[YEAR] < 0) {
        year = ToPandaString(-fields[YEAR] + 1);
    } else {
        year = ToPandaString(fields[YEAR]);
    }
    PandaString month = ToPandaString(fields[MONTH] + 1);
    PandaString day = ToPandaString(fields[DAYS]);
    PandaString str = year + VIRGULE + month + VIRGULE + day;
    return str;
}

// 20.4.4.38
JSTaggedValue JSDate::ToLocaleDateString(JSThread *thread) const
{
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, true)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString str = GetLocaleDateStr(fields);
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str).GetTaggedValue();
}

// 20.4.4.39
JSTaggedValue JSDate::ToLocaleString(JSThread *thread) const
{
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, true)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString str_date = GetLocaleDateStr(fields);
    PandaString str_time = GetLocaleTimeStr(fields);
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str_date + SPACE + str_time).GetTaggedValue();
}

// 20.4.4.40
JSTaggedValue JSDate::ToLocaleTimeString(JSThread *thread) const
{
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, true)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString str = GetLocaleTimeStr(fields);
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str).GetTaggedValue();
}

// 20.4.4.41
JSTaggedValue JSDate::ToString(JSThread *thread) const
{
    std::array<PandaString, DAY_PER_WEEK> weekday_name = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    std::array<PandaString, MOUTH_PER_YEAR> month_name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    int local_min = 0;
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, true)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString local_time;
    local_min = GetLocalOffsetFromOS(local_min, true);
    if (static_cast<int64_t>(this->GetTimeValue().GetDouble()) < CHINA_BEFORE_1900_MS &&
        local_min == CHINA_AFTER_1901_MIN) {
        local_min = CHINA_BEFORE_1901_MIN;
    }
    if (local_min >= 0) {
        local_time += PLUS;
    } else if (local_min < 0) {
        local_time += NEG;
        local_min = -local_min;
    }
    local_time = local_time + StrToTargetLength(ToPandaString(local_min / MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
    local_time = local_time + StrToTargetLength(ToPandaString(local_min % MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
    PandaString year = ToPandaString(fields[YEAR]);
    year = StrToTargetLength(year, STR_LENGTH_YEAR);
    PandaString weekday = weekday_name[fields[WEEKDAY]];
    PandaString month = month_name[fields[MONTH]];
    PandaString day = StrToTargetLength(ToPandaString(fields[DAYS]), STR_LENGTH_OTHERS);
    PandaString hour = StrToTargetLength(ToPandaString(fields[HOUR]), STR_LENGTH_OTHERS);
    PandaString minute = StrToTargetLength(ToPandaString(fields[MIN]), STR_LENGTH_OTHERS);
    PandaString second = StrToTargetLength(ToPandaString(fields[SEC]), STR_LENGTH_OTHERS);
    PandaString str = weekday + SPACE + month + SPACE + day + SPACE + year + SPACE + hour + COLON + minute + COLON +
                      second + SPACE + "GMT" + local_time;
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str).GetTaggedValue();
}

// 20.4.4.42
JSTaggedValue JSDate::ToTimeString(JSThread *thread) const
{
    int local_min = 0;
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, true)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString local_time;
    local_min = GetLocalOffsetFromOS(local_min, true);
    if (static_cast<int64_t>(this->GetTimeValue().GetDouble()) < CHINA_BEFORE_1900_MS &&
        local_min == CHINA_AFTER_1901_MIN) {
        local_min = CHINA_BEFORE_1901_MIN;
    }
    if (local_min >= 0) {
        local_time += PLUS;
    } else {
        local_time += NEG;
        local_min = -local_min;
    }
    local_time = local_time + StrToTargetLength(ToPandaString(local_min / MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
    local_time = local_time + StrToTargetLength(ToPandaString(local_min % MINUTE_PER_HOUR), STR_LENGTH_OTHERS);
    PandaString hour = StrToTargetLength(ToPandaString(fields[HOUR]), STR_LENGTH_OTHERS);
    PandaString minute = StrToTargetLength(ToPandaString(fields[MIN]), STR_LENGTH_OTHERS);
    PandaString second = StrToTargetLength(ToPandaString(fields[SEC]), STR_LENGTH_OTHERS);
    PandaString str = hour + COLON + minute + COLON + second + SPACE + "GMT" + local_time;
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str).GetTaggedValue();
}

// 20.4.4.43
JSTaggedValue JSDate::ToUTCString(JSThread *thread) const
{
    std::array<PandaString, DAY_PER_WEEK> weekday_name = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    std::array<PandaString, MOUTH_PER_YEAR> month_name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    std::array<int64_t, DATE_LENGTH> fields = {0};
    if (!GetThisDateValues(&fields, false)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    PandaString year = ToPandaString(fields[YEAR]);
    year = StrToTargetLength(year, STR_LENGTH_YEAR);
    PandaString weekday = weekday_name[fields[WEEKDAY]];
    PandaString month = month_name[fields[MONTH]];
    PandaString day = StrToTargetLength(ToPandaString(fields[DAYS]), STR_LENGTH_OTHERS);
    PandaString hour = StrToTargetLength(ToPandaString(fields[HOUR]), STR_LENGTH_OTHERS);
    PandaString minute = StrToTargetLength(ToPandaString(fields[MIN]), STR_LENGTH_OTHERS);
    PandaString second = StrToTargetLength(ToPandaString(fields[SEC]), STR_LENGTH_OTHERS);
    PandaString ms = StrToTargetLength(ToPandaString(fields[MS]), STR_LENGTH_OTHERS);
    PandaString str = weekday + COMMA + SPACE + day + SPACE + month + SPACE + year + SPACE + hour + COLON + minute +
                      COLON + second + SPACE + "GMT";
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str).GetTaggedValue();
}

// 20.4.4.44
JSTaggedValue JSDate::ValueOf() const
{
    return this->GetTimeValue();
}

// static
void JSDate::GetDateValues(double time_ms, std::array<int64_t, DATE_LENGTH> *date, bool is_local)
{
    int64_t tz = 0;
    int64_t time_ms_int;
    int month = 0;
    time_ms_int = static_cast<int64_t>(time_ms);
    if (is_local) {  // timezone offset
        tz = GetLocalOffsetFromOS(time_ms_int, is_local);
        if (time_ms_int < CHINA_BEFORE_1900_MS && tz == CHINA_AFTER_1901_MIN) {
            time_ms_int += CHINA_BEFORE_1901_ADDMS;
            time_ms_int += tz * MS_PER_SECOND * SEC_PER_MINUTE;
            tz = CHINA_BEFORE_1901_MIN;
        } else {
            time_ms_int += tz * MS_PER_SECOND * SEC_PER_MINUTE;
        }
    }

    DateUtils::TransferTimeToDate(time_ms_int, date);

    int index = DateUtils::IsLeap((*date)[YEAR]) ? 1 : 0;
    int left = 0;
    int right = MONTH_PER_YEAR;
    while (left <= right) {
        int middle = (left + right) / 2;  // 2 : half
        if (DAYS_FROM_MONTH[index][middle] <= (*date)[DAYS] && DAYS_FROM_MONTH[index][middle + 1] > (*date)[DAYS]) {
            month = middle;
            (*date)[DAYS] -= DAYS_FROM_MONTH[index][month];
            break;
        }
        if ((*date)[DAYS] > DAYS_FROM_MONTH[index][middle]) {
            left = middle + 1;
        } else if ((*date)[DAYS] < DAYS_FROM_MONTH[index][middle]) {
            right = middle - 1;
        }
    }

    (*date)[MONTH] = month;
    (*date)[DAYS] = (*date)[DAYS] + 1;
    (*date)[TIMEZONE] = -tz;
}

double JSDate::GetDateValue(double time_ms, uint8_t code, bool is_local) const
{
    if (std::isnan(time_ms)) {
        return base::NAN_VALUE;
    }
    std::array<int64_t, DATE_LENGTH> date = {0};
    GetDateValues(time_ms, &date, is_local);
    return static_cast<double>(date[code]);
}

JSTaggedValue JSDate::SetDateValue(EcmaRuntimeCallInfo *argv, uint32_t code, bool is_local) const
{
    // get date values.
    std::array<int64_t, DATE_LENGTH> date = {0};
    double time_ms = this->GetTimeValue().GetDouble();
    if (std::isnan(time_ms)) {
        return JSTaggedValue(base::NAN_VALUE);
    }

    // get values from argv.
    uint32_t argc = argv->GetArgsNumber();
    if (argc == 0) {
        return JSTaggedValue(base::NAN_VALUE);
    }

    uint32_t first_value = code & CODE_FLAG;
    uint32_t end_value = (code >> CODE_4_BIT) & CODE_FLAG;
    uint32_t count = end_value - first_value;

    if (argc < count) {
        count = argc;
    }

    GetDateValues(time_ms, &date, is_local);

    [[maybe_unused]] EcmaHandleScope handle_scope(argv->GetThread());
    for (uint32_t i = 0; i < count; i++) {
        JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, i);
        JSThread *thread = argv->GetThread();
        JSTaggedNumber res = JSTaggedValue::ToNumber(thread, value);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        double temp = res.GetNumber();
        if (std::isnan(temp)) {
            return JSTaggedValue(base::NAN_VALUE);
        }
        date[first_value + i] = NumberHelper::TruncateDouble(temp);
    }
    // set date values.
    return JSTaggedValue(SetDateValues(&date, is_local));
}

// static
double JSDate::SetDateValues(const std::array<int64_t, DATE_LENGTH> *date, bool is_local)
{
    int64_t month = DateUtils::Mod((*date)[MONTH], MONTH_PER_YEAR);
    int64_t year = (*date)[YEAR] + ((*date)[MONTH] - month) / MONTH_PER_YEAR;
    int64_t days = DateUtils::GetDaysFromYear(year);
    int index = DateUtils::IsLeap(year) ? 1 : 0;
    days += DAYS_FROM_MONTH[index][month];

    days += (*date)[DAYS] - 1;
    int64_t millisecond =
        (((*date)[HOUR] * MIN_PER_HOUR + (*date)[MIN]) * SEC_PER_MINUTE + (*date)[SEC]) * MS_PER_SECOND + (*date)[MS];
    int64_t result = days * MS_PER_DAY + millisecond;
    if (is_local) {
        int64_t offset = GetLocalOffsetFromOS(result, is_local) * SEC_PER_MINUTE * MS_PER_SECOND;
        if (result < CHINA_1901_MS && (offset / MS_PER_MINUTE) == CHINA_AFTER_1901_MIN) {
            offset += CHINA_BEFORE_1901_ADDMS;
        }
        result -= offset;
    }
    return TimeClip(result);
}
}  // namespace panda::ecmascript
