/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/generator_helper.h"

#include "plugins/ecmascript/runtime/interpreter/interpreter-inl.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "libpandafile/bytecode_instruction-inl.h"

namespace panda::ecmascript {
JSHandle<JSTaggedValue> GeneratorHelper::Continue(JSThread *thread, const JSHandle<GeneratorContext> &gen_context,
                                                  GeneratorResumeMode resume_mode, JSTaggedValue value)
{
    EcmaInterpreter::ChangeGenContext(thread, gen_context);

    JSHandle<JSGeneratorObject> gen_object(thread, gen_context->GetGeneratorObject());
    gen_object->SetResumeMode(thread, JSTaggedValue(static_cast<int32_t>(resume_mode)));
    gen_object->SetResumeResult(thread, value);

    JSHandle<JSTaggedValue> result(thread, EcmaInterpreter::GeneratorReEnterInterpreter(thread, gen_context));

    EcmaInterpreter::ResumeContext(thread);
    return result;
}
}  // namespace panda::ecmascript
