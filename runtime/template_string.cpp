/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/template_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/template_map.h"

namespace panda::ecmascript {
JSHandle<JSTaggedValue> TemplateString::GetTemplateObject(JSThread *thread, JSHandle<JSTaggedValue> template_literal)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> raw_strings_tag = JSObject::GetProperty(thread, template_literal, 0).GetValue();
    JSHandle<JSTaggedValue> template_map_tag = env->GetTemplateMap();
    JSHandle<TemplateMap> template_map(template_map_tag);
    int32_t element = template_map->FindEntry(raw_strings_tag.GetTaggedValue());
    if (element != -1) {
        return JSHandle<JSTaggedValue>(thread, template_map->GetValue(element));
    }
    JSHandle<JSTaggedValue> cooked_strings_tag = JSObject::GetProperty(thread, template_literal, 1).GetValue();
    JSHandle<JSArray> cooked_strings(cooked_strings_tag);
    int32_t count = cooked_strings->GetArrayLength();
    auto count_num = JSTaggedNumber(count);
    JSHandle<JSTaggedValue> template_arr = JSArray::ArrayCreate(thread, count_num);
    JSHandle<JSTaggedValue> raw_arr = JSArray::ArrayCreate(thread, count_num);
    JSHandle<JSObject> template_obj(template_arr);
    JSHandle<JSObject> raw_obj(raw_arr);
    for (int32_t i = 0; i < count; i++) {
        JSHandle<JSTaggedValue> cooked_value = JSObject::GetProperty(thread, cooked_strings_tag, i).GetValue();
        PropertyDescriptor desc_cooked(thread, cooked_value, true, false, false);
        JSArray::DefineOwnProperty(thread, template_obj, i, desc_cooked);
        JSHandle<JSTaggedValue> raw_value = JSObject::GetProperty(thread, raw_strings_tag, i).GetValue();
        PropertyDescriptor desc_raw(thread, raw_value, true, false, false);
        JSArray::DefineOwnProperty(thread, raw_obj, i, desc_raw);
    }
    JSObject::SetIntegrityLevel(thread, raw_obj, IntegrityLevel::FROZEN);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> raw(factory->NewFromCanBeCompressString("raw"));
    PropertyDescriptor desc(thread, raw_arr, false, false, false);
    JSArray::DefineOwnProperty(thread, template_obj, raw, desc);
    JSObject::SetIntegrityLevel(thread, template_obj, IntegrityLevel::FROZEN);
    TemplateMap::Insert(thread, template_map, raw_strings_tag, template_arr);
    env->SetTemplateMap(thread, template_map.GetTaggedValue());
    return template_arr;
}
}  // namespace panda::ecmascript
