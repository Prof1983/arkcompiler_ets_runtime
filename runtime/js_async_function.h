/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_ASYNC_FUNCTION_H
#define ECMASCRIPT_JS_ASYNC_FUNCTION_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"

namespace panda::ecmascript {
class JSAsyncAwaitStatusFunction : public JSFunction {
public:
    CAST_CHECK(JSAsyncAwaitStatusFunction, IsJSAsyncAwaitStatusFunction);

    static JSHandle<JSTaggedValue> AsyncFunctionAwaitFulfilled(JSThread *thread,
                                                               const JSHandle<JSAsyncAwaitStatusFunction> &func,
                                                               const JSHandle<JSTaggedValue> &value);

    static JSHandle<JSTaggedValue> AsyncFunctionAwaitRejected(JSThread *thread,
                                                              const JSHandle<JSAsyncAwaitStatusFunction> &func,
                                                              const JSHandle<JSTaggedValue> &reason);

    ACCESSORS_BASE(JSFunction)
    ACCESSORS(0, AsyncContext)
    ACCESSORS_FINISH(1)

    DECL_DUMP()
};

class JSAsyncFuncObject : public JSGeneratorObject {
public:
    CAST_CHECK(JSAsyncFuncObject, IsAsyncFuncObject);

    static JSTaggedValue AsyncFunctionAwait(JSThread *thread, const JSHandle<JSAsyncFuncObject> &async_func_obj,
                                            const JSHandle<JSTaggedValue> &value);

    ACCESSORS_BASE(JSGeneratorObject)
    ACCESSORS(0, Promise)
    ACCESSORS_FINISH(1)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_ASYNC_FUNCTION_H
