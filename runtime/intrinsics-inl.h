/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PLUGINS_ECMASCRIPT_RUNTIME_INTRINSICS_INL_H
#define PLUGINS_ECMASCRIPT_RUNTIME_INTRINSICS_INL_H

#include <limits>
#include "intrinsics.h"

#include "macros.h"
#include "runtime/include/method-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/interpreter/slow_runtime_stub.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter-inl.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/thread_environment_api.h"
#include "plugins/ecmascript/runtime/ic/ic_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_eval.h"

namespace panda::ecmascript::intrinsics {
#ifndef INLINE_ECMA_INTRINSICS
#define INLINE_ECMA_INTRINSICS ALWAYS_INLINE inline
#endif

#if ECMASCRIPT_ENABLE_IC
static inline JSTaggedValue GetRuntimeProfileTypeInfo(JSFunction *func)
{
    return func->GetProfileTypeInfo();
}
#endif

template <bool THIS_CALL = false>
static inline JSTaggedValue HandlerCall(JSThread *thread, EcmaRuntimeCallInfo *info, JSTaggedValue fn_object)
{
    if (UNLIKELY(!fn_object.IsCallable())) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSObject> error = GetFactory(thread)->GetJSError(ErrorType::TYPE_ERROR, "is not callable");
        thread->SetException(error.GetTaggedValue());
        return JSTaggedValue(JSTaggedValue::VALUE_EXCEPTION);
    }

    ECMAObject *js_object = ECMAObject::Cast(fn_object.GetHeapObject());
    Method *method = js_object->GetCallTarget();

    // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
    if constexpr (!THIS_CALL) {
        ASSERT(info->GetThis().GetTaggedValue() == JSTaggedValue::Undefined());
        if (fn_object.IsJSFunction() && !JSFunction::Cast(js_object)->IsStrict()) {
            // Set this=GlobalObject for sloppy function
            info->SetThis(thread->GetEcmaVM()->GetGlobalEnv()->GetGlobalObject());
        }
    }

    if (method->IsNative()) {
        ASSERT(method->GetNumVregs() == 0);

        LOG(DEBUG, INTERPRETER) << "Native function";

        LOG(DEBUG, INTERPRETER) << "Entry: Runtime Call.";
        JSTaggedValue ret_value = EcmaInterpreter::ExecuteNative(info);
        if (UNLIKELY(thread->HasPendingException())) {
            return JSTaggedValue::Exception();
        }
        LOG(DEBUG, INTERPRETER) << "Exit: Runtime Call.";
        return ret_value;
    }

    JSFunction *js_function = JSFunction::Cast(js_object);
    if (UNLIKELY(js_function->IsClassConstructor())) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSObject> error = thread->GetEcmaVM()->GetFactory()->GetJSError(
            ErrorType::TYPE_ERROR, "class constructor cannot called without 'new'");
        thread->SetException(error.GetTaggedValue());
        return JSTaggedValue::Exception();
    }

    LOG(DEBUG, INTERPRETER) << "Method name: " << method->GetName().data;
    JSTaggedValue ret_value = EcmaInterpreter::ExecuteInvoke(info, fn_object);

    if (UNLIKELY(thread->HasPendingException())) {
        return JSTaggedValue::Exception();
    }
    return ret_value;
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldnan()
{
    return JSTaggedValue(base::NAN_VALUE).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldinfinity()
{
    return JSTaggedValue(base::POSITIVE_INFINITY).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldglobalthis(JSThread *thread)
{
    return GetGlobalObject(thread).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldundefined()
{
    return JSTaggedValue::Undefined().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldboolean([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldnumber([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldstring([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldbigint([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t number_big_int)
{
    INTERPRETER_TRACE(thread, Ldbigint);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> big_int_handle(thread, JSTaggedValue(number_big_int));
    return JSTaggedValue::ToBigInt(thread, big_int_handle).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldnull()
{
    return JSTaggedValue::Null().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldsymbol(JSThread *thread)
{
    return GetGlobalEnv(thread)->GetSymbolFunction().GetTaggedValue().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldobject([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                         [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldfunction([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                           [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldtrue()
{
    return JSTaggedValue::True().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldfalse()
{
    return JSTaggedValue::False().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Add2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    if (left.IsInt() && right.IsInt()) {
        int32_t a0 = left.GetInt();
        int32_t a1 = right.GetInt();
        if ((a0 > 0 && a1 > INT32_MAX - a0) || (a0 < 0 && a1 < INT32_MIN - a0)) {
            auto ret = static_cast<double>(a0) + static_cast<double>(a1);
            return JSTaggedValue(ret).GetRawData();
        }
        return JSTaggedValue(a0 + a1).GetRawData();
    }
    if (left.IsNumber() && right.IsNumber()) {
        double a0_double = left.IsInt() ? left.GetInt() : left.GetDouble();
        double a1_double = right.IsInt() ? right.GetInt() : right.GetDouble();
        return JSTaggedValue(a0_double + a1_double).GetRawData();
    }
    auto ecma_vm = thread->GetEcmaVM();
    return SlowRuntimeStub::Add2Dyn(thread, ecma_vm, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Sub2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    if (left.IsInt() && right.IsInt()) {
        int32_t a0 = left.GetInt();
        int32_t a1 = -right.GetInt();
        if ((a0 > 0 && a1 > INT32_MAX - a0) || (a0 < 0 && a1 < INT32_MIN - a0)) {
            auto ret = static_cast<double>(a0) + static_cast<double>(a1);
            return JSTaggedValue(ret).GetRawData();
        }
        return JSTaggedValue(a0 + a1).GetRawData();
    }
    if (left.IsNumber() && right.IsNumber()) {
        double a0_double = left.IsInt() ? left.GetInt() : left.GetDouble();
        double a1_double = right.IsInt() ? right.GetInt() : right.GetDouble();
        return JSTaggedValue(a0_double - a1_double).GetRawData();
    }
    return SlowRuntimeStub::Sub2Dyn(thread, left, right).GetRawData();
}

// 12.6.3 Runtime Semantics: Evaluation
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Mul2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    auto res = FastRuntimeStub::FastMul(left, right);
    if (!res.IsHole()) {
        return res.GetRawData();
    }
    return SlowRuntimeStub::Mul2Dyn(thread, left, right).GetRawData();
}

// 12.6.3 Runtime Semantics: Evaluation
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Div2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    auto res = FastRuntimeStub::FastDiv(left, right);
    if (!res.IsHole()) {
        return res.GetRawData();
    }
    return SlowRuntimeStub::Div2Dyn(thread, left, right).GetRawData();
}

// 12.6.3 Runtime Semantics: Evaluation
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Mod2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    auto res = FastRuntimeStub::FastMod(left, right);
    if (!res.IsHole()) {
        return res.GetRawData();
    }
    return SlowRuntimeStub::Mod2Dyn(thread, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ExpDyn(JSThread *thread, uint64_t base_exp, uint64_t exp)
{
    auto base = JSTaggedValue(base_exp);
    auto exponent = JSTaggedValue(exp);

    JSTaggedValue res;

    if (base.IsNumber() && exponent.IsNumber()) {
        double double_base = base.IsInt() ? base.GetInt() : base.GetDouble();
        double double_exponent = exponent.IsInt() ? exponent.GetInt() : exponent.GetDouble();
        res = base::NumberHelper::Pow(double_base, double_exponent);
    } else {
        res = SlowRuntimeStub::ExpDyn(thread, base, exponent);
    }
    return res.GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t EqDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    auto res = FastRuntimeStub::FastEqual(left, right);
    if (!res.IsHole()) {
        return res.GetRawData();
    }
    return SlowRuntimeStub::EqDyn(thread, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t NotEqDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    auto res = FastRuntimeStub::FastEqual(left, right);
    if (!res.IsHole()) {
        return res.IsTrue() ? JSTaggedValue::False().GetRawData() : JSTaggedValue::True().GetRawData();
    }
    return SlowRuntimeStub::NotEqDyn(thread, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StrictEqDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);
    return JSTaggedValue(FastRuntimeStub::FastStrictEqual(thread, left, right)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StrictNotEqDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);
    return JSTaggedValue(!FastRuntimeStub::FastStrictEqual(thread, left, right)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LessDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    if (left.IsNumber() && right.IsNumber()) {
        double value_a = left.IsInt() ? static_cast<double>(left.GetInt()) : left.GetDouble();
        double value_b = right.IsInt() ? static_cast<double>(right.GetInt()) : right.GetDouble();
        bool ret = JSTaggedValue::StrictNumberCompare(value_a, value_b) == ComparisonResult::LESS;
        return ret ? JSTaggedValue::True().GetRawData() : JSTaggedValue::False().GetRawData();
    }
    return SlowRuntimeStub::LessDyn(thread, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LessEqDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    if (left.IsNumber() && right.IsNumber()) {
        double value_a = left.IsInt() ? static_cast<double>(left.GetInt()) : left.GetDouble();
        double value_b = right.IsInt() ? static_cast<double>(right.GetInt()) : right.GetDouble();
        bool ret = JSTaggedValue::StrictNumberCompare(value_a, value_b) <= ComparisonResult::EQUAL;
        return ret ? JSTaggedValue::True().GetRawData() : JSTaggedValue::False().GetRawData();
    }
    return SlowRuntimeStub::LessEqDyn(thread, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GreaterDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    if (left.IsNumber() && right.IsNumber()) {
        double value_a = left.IsInt() ? static_cast<double>(left.GetInt()) : left.GetDouble();
        double value_b = right.IsInt() ? static_cast<double>(right.GetInt()) : right.GetDouble();
        bool ret = JSTaggedValue::StrictNumberCompare(value_a, value_b) == ComparisonResult::GREAT;
        return ret ? JSTaggedValue::True().GetRawData() : JSTaggedValue::False().GetRawData();
    }
    return SlowRuntimeStub::GreaterDyn(thread, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GreaterEqDyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    auto left = JSTaggedValue(lhs);
    auto right = JSTaggedValue(rhs);

    if (left.IsNumber() && right.IsNumber()) {
        double value_a = left.IsInt() ? static_cast<double>(left.GetInt()) : left.GetDouble();
        double value_b = right.IsInt() ? static_cast<double>(right.GetInt()) : right.GetDouble();
        ComparisonResult comparison = JSTaggedValue::StrictNumberCompare(value_a, value_b);
        bool ret = (comparison == ComparisonResult::GREAT) || (comparison == ComparisonResult::EQUAL);
        return ret ? JSTaggedValue::True().GetRawData() : JSTaggedValue::False().GetRawData();
    }
    return SlowRuntimeStub::GreaterEqDyn(thread, left, right).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t TryLdGlobalByValue([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint16_t slot_id,
                                                   [[maybe_unused]] uint64_t prop_value)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void TryStGlobalByValue([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint16_t slot_id,
                                               [[maybe_unused]] uint64_t prop_value,
                                               [[maybe_unused]] uint64_t val_value)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t TryLdGlobalByName(JSThread *thread, uint64_t prop, [[maybe_unused]] uint16_t slot_id,
                                                  [[maybe_unused]] uint64_t func)
{
    JSTaggedType global_obj = GetGlobalObject(thread).GetRawData();
    JSTaggedValue result = JSTaggedValue::Hole();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetHeapObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        result = ICRuntimeStub::LoadGlobalICByName(thread, js_func, JSTaggedValue(global_obj), JSTaggedValue(prop),
                                                   slot_id, true);
        if (!result.IsHole()) {
            return result.GetRawData();
        }
    }
#endif

    bool found = false;
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    result = FastRuntimeStub::GetGlobalOwnProperty(JSTaggedValue(global_obj), JSTaggedValue(prop), &found);
    if (found) {
        return result.GetRawData();
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    return SlowRuntimeStub::TryLdGlobalByName(thread, JSTaggedValue(global_obj), JSTaggedValue(prop)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Tonumber(JSThread *thread, uint64_t a0)
{
    auto value = JSTaggedValue(a0);
    if (value.IsNumber()) {
        return value.GetRawData();
    }
    return SlowRuntimeStub::ToNumber(thread, value).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t NegDyn(JSThread *thread, uint64_t val)
{
    auto value = JSTaggedValue(val);

    if (value.IsInt()) {
        if (value.GetInt() == 0) {
            return JSTaggedValue(-0.0).GetRawData();
        }
        if (value.GetInt() == std::numeric_limits<int32_t>::min()) {
            return JSTaggedValue(std::numeric_limits<int32_t>::max() + 1.0).GetRawData();
        }
        return JSTaggedValue(-value.GetInt()).GetRawData();
    }
    if (value.IsDouble()) {
        return JSTaggedValue(-value.GetDouble()).GetRawData();
    }
    return SlowRuntimeStub::NegDyn(thread, value).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t NotDyn(JSThread *thread, uint64_t val)
{
    auto value = JSTaggedValue(val);
    if (value.IsInt()) {
        auto number = static_cast<int32_t>(value.GetInt());
        return JSTaggedValue(~number).GetRawData();  // NOLINT(hicpp-signed-bitwise)
    }
    if (value.IsDouble()) {
        int32_t number = base::NumberHelper::DoubleToInt(value.GetDouble(), base::INT32_BITS);
        return JSTaggedValue(~number).GetRawData();  // NOLINT(hicpp-signed-bitwise)
    }
    return SlowRuntimeStub::NotDyn(thread, value).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t IncDyn(JSThread *thread, uint64_t val)
{
    auto value = JSTaggedValue(val);

    if (value.IsInt()) {
        int32_t a0 = value.GetInt();
        if (UNLIKELY(a0 == INT32_MAX)) {
            return JSTaggedValue(static_cast<double>(a0) + 1.0).GetRawData();
        }
        return JSTaggedValue(a0 + 1).GetRawData();
    }
    if (value.IsDouble()) {
        return JSTaggedValue(value.GetDouble() + 1.0).GetRawData();
    }
    return SlowRuntimeStub::IncDyn(thread, value).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DecDyn(JSThread *thread, uint64_t val)
{
    auto value = JSTaggedValue(val);

    if (value.IsInt()) {
        int32_t a0 = value.GetInt();
        if (UNLIKELY(a0 == INT32_MIN)) {
            return JSTaggedValue(static_cast<double>(a0) - 1.0).GetRawData();
        }
        return JSTaggedValue(a0 - 1).GetRawData();
    }
    if (value.IsDouble()) {
        return JSTaggedValue(value.GetDouble() - 1.0).GetRawData();
    }
    return SlowRuntimeStub::DecDyn(thread, value).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Defineglobalvar([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                                [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Definelocalvar([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                               [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Definefuncexpr([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                               [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefinefuncDyn(JSThread *thread, uint64_t method, uint64_t env, uint64_t cp)
{
    auto result = JSFunction::Cast(JSTaggedValue(method).GetHeapObject());
    auto lexenv = JSTaggedValue(env);
    if (!result->GetLexicalEnv().IsUndefined()) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSTaggedValue> lexenv_handle(thread, lexenv);
        auto res = SlowRuntimeStub::DefinefuncDyn(thread, result->GetCallTarget());
        result = JSFunction::Cast(res.GetHeapObject());
        auto constpool = ConstantPool::Cast(cp);  // May not be moved by GC
        lexenv = lexenv_handle.GetTaggedValue();  // May be moved by GC
        result->SetConstantPool(thread, JSTaggedValue(constpool));
    }
    result->SetLexicalEnv(thread, lexenv);
    return JSTaggedValue(result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t NewobjDynrange(JSThread *thread, uint16_t num_args, uint64_t func, uint64_t new_target,
                                               void *stkargs_raw)
{
    return SlowRuntimeStub::NewObjDynRange(thread, num_args, JSTaggedValue(func), JSTaggedValue(new_target),
                                           reinterpret_cast<JSTaggedType *>(stkargs_raw))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t NewobjDynrangeHandled(JSThread *thread, uint16_t num_args, uint64_t func,
                                                      uint64_t new_target, void *stkargs_raw)
{
    JSSpanHandle args_handle(thread, Span(reinterpret_cast<JSTaggedValue *>(stkargs_raw), num_args));

    return SlowRuntimeStub::NewObjDynRange(thread, num_args, JSTaggedValue(func), JSTaggedValue(new_target),
                                           args_handle.DataTaggedType())
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t RefeqDyn([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                         [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t TypeofDyn(JSThread *thread, uint64_t acc)
{
    return FastRuntimeStub::FastTypeOf(thread, JSTaggedValue(acc)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Callruntimerange([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                                 [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdlexenvDyn([[maybe_unused]] JSThread *thread)
{
    UNREACHABLE();
    // return GetLexicalEnvRawValue();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetUnmappedArgs([[maybe_unused]] JSThread *thread, uint32_t num_args, void *args)
{
    ASSERT(thread->IsCurrentFrameCompiled());
    uint32_t actual_num_args = num_args - js_method_args::NUM_MANDATORY_ARGS;
    return SlowRuntimeStub::GetUnmappedArgs(thread, actual_num_args, reinterpret_cast<JSTaggedType *>(args))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetUnmappedArgsInterp(JSThread *thread)
{
    ASSERT(!thread->IsCurrentFrameCompiled());
    uint32_t actual_num_args = thread->GetCurrentFrame()->GetNumActualArgs() - js_method_args::NUM_MANDATORY_ARGS;
    uint32_t start_idx = thread->GetCurrentFrame()->GetMethod()->GetNumVregs() + js_method_args::NUM_MANDATORY_ARGS;
    auto args = reinterpret_cast<JSTaggedType *>(&thread->GetCurrentFrame()->GetVReg(start_idx));
    return SlowRuntimeStub::GetUnmappedArgs(thread, actual_num_args, reinterpret_cast<JSTaggedType *>(args))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Negate(uint64_t value)
{
    return JSTaggedValue(value).ToBoolean() ? JSTaggedValue::False().GetRawData() : JSTaggedValue::True().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t IsUndefined(uint64_t value)
{
    return JSTaggedValue(value).IsUndefined() ? JSTaggedValue::True().GetRawData()
                                              : JSTaggedValue::False().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t IsTrue(uint64_t value)
{
    return JSTaggedValue(value).IsTrue() ? JSTaggedValue::True().GetRawData() : JSTaggedValue::False().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t IsFalse(uint64_t value)
{
    return JSTaggedValue(value).IsFalse() ? JSTaggedValue::True().GetRawData() : JSTaggedValue::False().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t IsCoercible(uint64_t uvalue)
{
    JSTaggedValue value(uvalue);
    if (value.IsUndefined() || value.IsNull()) {
        return JSTaggedValue::False().GetRawData();
    }

    return JSTaggedValue::True().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SuspendGenerator([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t a0,
                                                 [[maybe_unused]] uint64_t a1)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SuspendGeneratorInterp(JSThread *thread, uint64_t gen_obj, uint64_t value, uint64_t pc)
{
    ASSERT(!thread->IsCurrentFrameCompiled());
    thread->GetCurrentFrame()->SetBytecodeOffset(pc);
    return SlowRuntimeStub::SuspendGenerator(thread, JSTaggedValue(gen_obj), JSTaggedValue(value)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SuspendAsyncGenerator([[maybe_unused]] JSThread *thread,
                                                      [[maybe_unused]] uint64_t gen_obj,
                                                      [[maybe_unused]] uint64_t value)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SuspendAsyncGeneratorInterp(JSThread *thread, uint64_t gen_obj, uint64_t value,
                                                            uint64_t pc)
{
    ASSERT(!thread->IsCurrentFrameCompiled());
    thread->GetCurrentFrame()->SetBytecodeOffset(pc);
    return SlowRuntimeStub::SuspendAsyncGenerator(thread, JSTaggedValue(gen_obj), JSTaggedValue(value)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ThrowUndefined([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t obj_value)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Copyrestargs([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint16_t index)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CopyrestargsInterp(JSThread *thread, uint16_t index)
{
    ASSERT(!thread->IsCurrentFrameCompiled());
    auto *frame = thread->GetCurrentFrame();
    uint32_t num_vregs = frame->GetMethod()->GetNumVregs();
    int32_t actual_num_args = frame->GetNumActualArgs() - js_method_args::NUM_MANDATORY_ARGS;
    int32_t tmp = actual_num_args - index;
    uint32_t rest_num_args = (tmp > 0) ? tmp : 0;
    uint32_t start_idx = num_vregs + js_method_args::NUM_MANDATORY_ARGS + index;

    auto args = reinterpret_cast<JSTaggedType *>(&thread->GetCurrentFrame()->GetVReg(start_idx));

    return SlowRuntimeStub::CopyRestArgs(thread, rest_num_args, args).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldhole()
{
    return JSTaggedValue::Hole().GetRawData();
}

// Just declared here, not used
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ReturnUndefined([[maybe_unused]] JSThread *thread)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call0Dyn(JSThread *thread, uint64_t raw_fn_object)
{
    std::array args = {
        JSTaggedValue(raw_fn_object),
        JSTaggedValue::Undefined(),
        JSTaggedValue::Undefined(),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call1Dyn(JSThread *thread, uint64_t raw_fn_object, uint64_t raw_a0)
{
    std::array args = {
        JSTaggedValue(raw_fn_object),
        JSTaggedValue::Undefined(),
        JSTaggedValue::Undefined(),
        JSTaggedValue(raw_a0),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call2Dyn(JSThread *thread, uint64_t raw_fn_object, uint64_t raw_a0, uint64_t raw_a1)
{
    std::array args = {
        JSTaggedValue(raw_fn_object), JSTaggedValue::Undefined(), JSTaggedValue::Undefined(),
        JSTaggedValue(raw_a0),        JSTaggedValue(raw_a1),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call3Dyn(JSThread *thread, uint64_t raw_fn_object, uint64_t raw_a0, uint64_t raw_a1,
                                         uint64_t raw_a2)
{
    std::array args = {
        JSTaggedValue(raw_fn_object), JSTaggedValue::Undefined(), JSTaggedValue::Undefined(),
        JSTaggedValue(raw_a0),        JSTaggedValue(raw_a1),      JSTaggedValue(raw_a2),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// Just declared here, not used
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CalliRangeDyn([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint16_t num,
                                              [[maybe_unused]] uint64_t obj_value)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call0ThisDyn(JSThread *thread, uint64_t raw_fn_object, uint64_t raw_this)
{
    std::array args = {
        JSTaggedValue(raw_fn_object),
        JSTaggedValue::Undefined(),
        JSTaggedValue(raw_this),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall<true>(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call1ThisDyn(JSThread *thread, uint64_t raw_fn_object, uint64_t raw_this,
                                             uint64_t raw_a0)
{
    std::array args = {
        JSTaggedValue(raw_fn_object),
        JSTaggedValue::Undefined(),
        JSTaggedValue(raw_this),
        JSTaggedValue(raw_a0),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall<true>(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call2ThisDyn(JSThread *thread, uint64_t raw_fn_object, uint64_t raw_this,
                                             uint64_t raw_a0, uint64_t raw_a1)
{
    std::array args = {
        JSTaggedValue(raw_fn_object), JSTaggedValue::Undefined(), JSTaggedValue(raw_this),
        JSTaggedValue(raw_a0),        JSTaggedValue(raw_a1),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall<true>(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Call3ThisDyn(JSThread *thread, uint64_t raw_fn_object, uint64_t raw_this,
                                             uint64_t raw_a0, uint64_t raw_a1, uint64_t raw_a2)
{
    std::array args = {
        JSTaggedValue(raw_fn_object), JSTaggedValue::Undefined(), JSTaggedValue(raw_this),
        JSTaggedValue(raw_a0),        JSTaggedValue(raw_a1),      JSTaggedValue(raw_a2),
    };
    EcmaRuntimeCallInfo info(thread, args.size(), args.data());
    return HandlerCall<true>(thread, &info, JSTaggedValue(raw_fn_object)).GetRawData();
}

// Just declared here, not used
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CalliRangeDynInterp(JSThread *thread, uint16_t args_num, uint64_t args_ptr)
{
    auto args = reinterpret_cast<JSTaggedValue *>(args_ptr);
    auto fn_obj = args[0];     // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto *fn_args = &args[1];  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)

    // TODO(vpukhov): copy in-place
    auto info = NewRuntimeCallInfo(thread, fn_obj, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), args_num);
    for (size_t i = 0; i < args_num; ++i) {
        info->SetCallArg(i, fn_args[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    }

    return HandlerCall<true>(thread, info.Get(), fn_obj).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CalliThisRangeDyn(JSThread *thread, uint16_t args_num, uint64_t args_ptr)
{
    auto args = reinterpret_cast<JSTaggedValue *>(args_ptr);
    auto fn_obj = args[0];      // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto this_obj = args[1];    // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto *fn_args = &args[2U];  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    args_num--;

    // TODO(vpukhov): copy in-place
    auto info = NewRuntimeCallInfo(thread, fn_obj, this_obj, JSTaggedValue::Undefined(), args_num);
    for (size_t i = 0; i < args_num; ++i) {
        info->SetCallArg(i, fn_args[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    }

    return HandlerCall<true>(thread, info.Get(), fn_obj).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StOwnByIndex(JSThread *thread, uint32_t idx, uint64_t uobj, uint64_t uvalue)
{
    JSTaggedValue obj(uobj);
    JSTaggedValue value(uvalue);
    // fast path
    if (obj.IsECMAObject() && !obj.IsClassConstructor() && !obj.IsClassPrototype() &&
        !FastRuntimeStub::IsSpecialIndexedObjForSet(obj)) {
        if (!FastRuntimeStub::SetOwnElement(thread, obj, idx, value)) {
            return JSTaggedValue(JSTaggedValue::VALUE_EXCEPTION).GetRawData();
        }
        return JSTaggedValue(JSTaggedValue::VALUE_HOLE).GetRawData();
    }

    return SlowRuntimeStub::StOwnByIndex(thread, obj, idx, value).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ldglobal(JSThread *thread)
{
    return GetGlobalObject(thread).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdObjByValue(JSThread *thread, uint64_t rec, uint64_t pkey,
                                             [[maybe_unused]] uint16_t slot_id, [[maybe_unused]] uint64_t func)
{
    // If ICRuntimeStub::LoadICByValue triggers GC it mustn't return Hole.
    // Using rec and pkey after ICRuntimeStub::LoadICByValue is valid.
    // To suppress CSA keep pointers in uint64_t.
    JSTaggedValue res = JSTaggedValue::Hole();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetHeapObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        res = ICRuntimeStub::LoadICByValue(thread, js_func, JSTaggedValue(rec), JSTaggedValue(pkey), slot_id);
        if (LIKELY(!res.IsHole())) {
            return res.GetRawData();
        }
    }
#endif

    // fast path
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> receiver(thread, JSTaggedValue(rec));
    JSHandle<JSTaggedValue> prop_key(thread, JSTaggedValue(pkey));
    if (LIKELY(receiver->IsHeapObject())) {
        res = FastRuntimeStub::GetPropertyByValue(thread, receiver, prop_key);
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }
    // slow path
    return SlowRuntimeStub::LdObjByValue(thread, receiver.GetTaggedValue(), prop_key.GetTaggedValue(), false,
                                         JSTaggedValue::Undefined())
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StObjByValue(JSThread *thread, uint64_t rec, uint64_t pkey, uint64_t val,
                                             [[maybe_unused]] uint16_t slot_id, [[maybe_unused]] uint64_t func)
{
    // If ICRuntimeStub::StoreICByValue triggers GC it mustn't return Hole.
    // Using rec and pkey after ICRuntimeStub::StoreICByValue is valid.
    // To suppress CSA keep pointers in uint64_t.
    JSTaggedValue res = JSTaggedValue::Hole();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetTaggedObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        res = ICRuntimeStub::StoreICByValue(thread, js_func, JSTaggedValue(rec), JSTaggedValue(pkey),
                                            JSTaggedValue(val), slot_id);
        if (LIKELY(!res.IsHole())) {
            return res.GetRawData();
        }
    }
#endif

    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> receiver(thread, JSTaggedValue(rec));
    JSHandle<JSTaggedValue> prop_key(thread, JSTaggedValue(pkey));
    JSHandle<JSTaggedValue> value(thread, JSTaggedValue(val));

    if (receiver->IsHeapObject()) {
        res = FastRuntimeStub::SetPropertyByValue(thread, receiver, prop_key, value);
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }

    return SlowRuntimeStub::StObjByValue(thread, receiver.GetTaggedValue(), prop_key.GetTaggedValue(),
                                         value.GetTaggedValue())
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t TryStGlobalByName(JSThread *thread, uint64_t prop_key, uint64_t value,
                                                  [[maybe_unused]] uint16_t slot_id, [[maybe_unused]] uint64_t func)
{
    JSTaggedType global_obj = GetGlobalObject(thread).GetRawData();

    JSTaggedValue result = JSTaggedValue::Hole();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetHeapObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        result = ICRuntimeStub::StoreGlobalICByName(thread, js_func, JSTaggedValue(global_obj), JSTaggedValue(prop_key),
                                                    JSTaggedValue(value), slot_id, true);
        if (!result.IsHole()) {
            return result.GetRawData();
        }
    }
#endif

    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    bool found = false;
    // 2. find from global object
    FastRuntimeStub::GetGlobalOwnProperty(JSTaggedValue(global_obj), JSTaggedValue(prop_key), &found);
    if (!found) {
        result = SlowRuntimeStub::ThrowReferenceError(thread, JSTaggedValue(prop_key), " is not defined");
        ASSERT(result.IsException());
        return result.GetRawData();
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    return SlowRuntimeStub::StGlobalVar(thread, JSTaggedValue(prop_key), JSTaggedValue(value)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdGlobalVar(JSThread *thread, uint64_t prop_key, [[maybe_unused]] uint16_t slot_id,
                                            [[maybe_unused]] uint64_t func)
{
    JSTaggedType global_obj = thread->GetGlobalObject().GetRawData();
    JSTaggedValue result = JSTaggedValue::Hole();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();

#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetHeapObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        result = ICRuntimeStub::LoadGlobalICByName(thread, js_func, JSTaggedValue(global_obj), JSTaggedValue(prop_key),
                                                   slot_id);
        if (!result.IsHole()) {
            return result.GetRawData();
        }
    }
#endif

    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    bool found = false;
    result = FastRuntimeStub::GetGlobalOwnProperty(JSTaggedValue(global_obj), JSTaggedValue(prop_key), &found);
    if (found) {
        return result.GetRawData();
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    return SlowRuntimeStub::LdGlobalVar(thread, JSTaggedValue(global_obj), JSTaggedValue(prop_key)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StGlobalVar(JSThread *thread, uint64_t prop, uint64_t value,
                                            [[maybe_unused]] uint16_t slot_id, [[maybe_unused]] uint64_t func)
{
    JSTaggedType global_obj = thread->GetGlobalObject().GetRawData();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();

#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetHeapObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        JSTaggedValue result = ICRuntimeStub::StoreGlobalICByName(thread, js_func, JSTaggedValue(global_obj),
                                                                  JSTaggedValue(prop), JSTaggedValue(value), slot_id);
        if (!result.IsHole()) {
            return result.GetRawData();
        }
    }
#endif

    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    return SlowRuntimeStub::StGlobalVar(thread, JSTaggedValue(prop), JSTaggedValue(value)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdObjByName(JSThread *thread, uint64_t prop_key, uint64_t object,
                                            [[maybe_unused]] uint16_t slot_id, [[maybe_unused]] uint64_t func)
{
    // CSA reports usage object and prop_key after GC triggered in ICRuntimeStub::LoadICByName.
    // If ICRuntimeStub::LoadICByName triggers GC it mustn't return Hole.
    // Using object and prop_key after ICRuntimeStub::LoadICByName is valid.
    // To suppress CSA keep pointers in uint64_t.
    JSTaggedValue res = JSTaggedValue::Hole();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetHeapObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        res = ICRuntimeStub::LoadICByName(thread, js_func, JSTaggedValue(object), JSTaggedValue(prop_key), slot_id);
        if (LIKELY(!res.IsHole())) {
            return res.GetRawData();
        }
    }
#endif

    if (LIKELY(JSTaggedValue(object).IsHeapObject())) {
        ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
        res = FastRuntimeStub::GetPropertyByName(thread, JSTaggedValue(object), JSTaggedValue(prop_key));
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    return SlowRuntimeStub::LdObjByName(thread, JSTaggedValue(object), JSTaggedValue(prop_key), false,
                                        JSTaggedValue::Undefined())
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StObjByName(JSThread *thread, uint64_t prop_key, uint64_t object, uint64_t val,
                                            [[maybe_unused]] uint16_t slot_id, [[maybe_unused]] uint64_t func)
{
    // CSA reports usage object, prop_key and val after GC triggered in ICRuntimeStub::StoreICByName.
    // If ICRuntimeStub::StoreICByName triggers GC it mustn't return Hole.
    // Using object, prop_key and val after ICRuntimeStub::StoreICByName is valid.
    // To suppress CSA keep pointers in uint64_t.
    JSTaggedValue res = JSTaggedValue::Hole();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
#if ECMASCRIPT_ENABLE_IC
    auto js_func = JSFunction::Cast(JSTaggedValue(func).GetHeapObject());
    if (ICRuntimeStub::HaveICForFunction(js_func)) {
        res = ICRuntimeStub::StoreICByName(thread, js_func, JSTaggedValue(object), JSTaggedValue(prop_key),
                                           JSTaggedValue(val), slot_id);
        if (LIKELY(!res.IsHole())) {
            return res.GetRawData();
        }
    }
#endif

    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    if (JSTaggedValue(object).IsHeapObject()) {
        res = FastRuntimeStub::SetPropertyByName(thread, JSTaggedValue(object), JSTaggedValue(prop_key),
                                                 JSTaggedValue(val));
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }
    return SlowRuntimeStub::StObjByName(thread, JSTaggedValue(object), JSTaggedValue(prop_key), JSTaggedValue(val))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdObjByIndex(JSThread *thread, uint32_t idx, uint64_t uobj, void *prof = nullptr)
{
    // fast path
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
    if (LIKELY(JSTaggedValue(uobj).IsHeapObject())) {
        JSTaggedValue res = FastRuntimeStub::GetPropertyByIndex(thread, JSTaggedValue(uobj), idx, prof);
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    // not meet fast condition or fast path return hole, walk slow path
    // slow stub not need receiver
    return SlowRuntimeStub::LdObjByIndex(thread, JSTaggedValue(uobj), idx, false, JSTaggedValue::Undefined())
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StObjByIndex(JSThread *thread, uint32_t idx, uint64_t uobj, uint64_t uval,
                                             void *prof = nullptr)
{
    // fast path
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
    if (JSTaggedValue(uobj).IsHeapObject()) {
        JSTaggedValue res =
            FastRuntimeStub::SetPropertyByIndex(thread, JSTaggedValue(uobj), idx, JSTaggedValue(uval), prof);
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    // slow path
    return SlowRuntimeStub::StObjByIndex(thread, JSTaggedValue(uobj), idx, JSTaggedValue(uval)).GetRawData();
}

template <JSHandle<BigInt> BIGINT_OP(JSThread *, JSHandle<BigInt>, JSHandle<BigInt>), bool IS_U_LEFT = false,
          bool IS_U_RIGHT = false>
ARK_INLINE inline static JSTaggedValue GetLeftRightInt(JSThread *thread, uint64_t lhs, uint64_t rhs, int32_t &left,
                                                       int32_t &right)
{
    auto jleft = JSTaggedValue(lhs);
    auto jright = JSTaggedValue(rhs);

    if (jleft.IsInt() && jright.IsInt()) {
        left = jleft.GetInt();
        right = jright.GetInt();
        return JSTaggedValue::Hole();
    }

    if (jleft.IsNumber() && jright.IsNumber()) {
        left = jleft.IsInt() ? jleft.GetInt() : base::NumberHelper::DoubleToInt(jleft.GetDouble(), base::INT32_BITS);
        right =
            jright.IsInt() ? jright.GetInt() : base::NumberHelper::DoubleToInt(jright.GetDouble(), base::INT32_BITS);
        return JSTaggedValue::Hole();
    }

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> right_handle(thread, jright);
    JSHandle<JSTaggedValue> left_handle(thread, jleft);

    JSHandle<JSTaggedValue> left_value = JSTaggedValue::ToNumeric(thread, left_handle);

    if (left_value->IsException()) {
        return JSTaggedValue::Exception();
    }

    JSHandle<JSTaggedValue> right_value = JSTaggedValue::ToNumeric(thread, right_handle);

    if (right_value->IsException()) {
        return JSTaggedValue::Exception();
    }

    if (left_value->IsBigInt() || right_value->IsBigInt()) {
        if (left_value->IsBigInt() && right_value->IsBigInt()) {
            return BIGINT_OP(thread, JSHandle<BigInt>::Cast(left_value), JSHandle<BigInt>::Cast(right_value))
                .GetTaggedValue();
        }

        JSHandle<JSObject> error = GetFactory(thread)->GetJSError(
            ErrorType::TYPE_ERROR, "Cannot mix BigInt and other types, use explicit conversions");
        thread->SetException(error.GetTaggedValue());
        return JSTaggedValue::Exception();
    }

    jleft = IS_U_LEFT ? SlowRuntimeStub::ToJSTaggedValueWithUint32(thread, left_value.GetTaggedValue())
                      : SlowRuntimeStub::ToJSTaggedValueWithInt32(thread, left_value.GetTaggedValue());

    if (jleft.IsException()) {
        return JSTaggedValue::Exception();
    }

    jright = IS_U_RIGHT ? SlowRuntimeStub::ToJSTaggedValueWithUint32(thread, right_value.GetTaggedValue())
                        : SlowRuntimeStub::ToJSTaggedValueWithInt32(thread, right_value.GetTaggedValue());

    if (jright.IsException()) {
        return JSTaggedValue::Exception();
    }

    left = jleft.GetInt();
    right = jright.GetInt();
    return JSTaggedValue::Hole();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t And2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    int32_t left {};
    int32_t right {};

    JSTaggedValue res = GetLeftRightInt<BigInt::BitwiseAND>(thread, lhs, rhs, left, right);
    if (res.IsException()) {
        return TaggedValue::VALUE_EXCEPTION;
    }

    if (!res.IsHole()) {
        return res.GetRawData();
    }

    // NOLINT(hicpp-signed-bitwise)
    auto ret = static_cast<uint32_t>(left) & static_cast<uint32_t>(right);
    return JSTaggedValue(static_cast<int32_t>(ret)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Or2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    int32_t left {};
    int32_t right {};

    JSTaggedValue res = GetLeftRightInt<BigInt::BitwiseOR>(thread, lhs, rhs, left, right);
    if (res.IsException()) {
        return TaggedValue::VALUE_EXCEPTION;
    }

    if (!res.IsHole()) {
        return res.GetRawData();
    }

    auto ret = static_cast<uint32_t>(left) | static_cast<uint32_t>(right);
    return JSTaggedValue(static_cast<int32_t>(ret)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Xor2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    int32_t left {};
    int32_t right {};

    JSTaggedValue res = GetLeftRightInt<BigInt::BitwiseXOR>(thread, lhs, rhs, left, right);
    if (res.IsException()) {
        return TaggedValue::VALUE_EXCEPTION;
    }

    if (!res.IsHole()) {
        return res.GetRawData();
    }

    auto ret = static_cast<uint32_t>(left) ^ static_cast<uint32_t>(right);
    return JSTaggedValue(static_cast<int32_t>(ret)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Shl2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    int32_t left {};
    int32_t right {};

    JSTaggedValue res = GetLeftRightInt<BigInt::LeftShift, false, true>(thread, lhs, rhs, left, right);
    if (res.IsException()) {
        return TaggedValue::VALUE_EXCEPTION;
    }

    if (!res.IsHole()) {
        return res.GetRawData();
    }

    uint32_t shift = static_cast<uint32_t>(right) & 0x1f;  // NOLINT(hicpp-signed-bitwise, readability-magic-numbers)
    using UnsignedType = std::make_unsigned_t<int32_t>;
    return JSTaggedValue(static_cast<int32_t>(static_cast<UnsignedType>(left) << shift))
        .GetRawData();  // NOLINT(hicpp-signed-bitwise)
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Shr2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    int32_t left {};
    int32_t right {};

    JSTaggedValue res = GetLeftRightInt<BigInt::SignedRightShift, false, true>(thread, lhs, rhs, left, right);
    if (res.IsException()) {
        return TaggedValue::VALUE_EXCEPTION;
    }

    if (!res.IsHole()) {
        return res.GetRawData();
    }

    uint32_t shift = static_cast<uint32_t>(right) & 0x1f;  // NOLINT(hicpp-signed-bitwise, readability-magic-numbers)
    return JSTaggedValue(static_cast<int32_t>(left >> shift)).GetRawData();  // NOLINT(hicpp-signed-bitwise)
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Ashr2Dyn(JSThread *thread, uint64_t lhs, uint64_t rhs)
{
    int32_t left {};
    int32_t right {};

    JSTaggedValue res = GetLeftRightInt<BigInt::UnsignedRightShift, true, true>(thread, lhs, rhs, left, right);
    if (res.IsException()) {
        return TaggedValue::VALUE_EXCEPTION;
    }

    if (!res.IsHole()) {
        return res.GetRawData();
    }

    uint32_t shift = static_cast<uint32_t>(right) & 0x1f;  // NOLINT(hicpp-signed-bitwise, readability-magic-numbers)
    using UnsignedType = std::make_unsigned_t<uint32_t>;
    return JSTaggedValue(static_cast<uint32_t>(static_cast<UnsignedType>(left) >> shift))
        .GetRawData();  // NOLINT(hicpp-signed-bitwise)
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t RethrowDyn(JSThread *thread, uint64_t obj)
{
    JSTaggedValue obj_value(obj);

    if (JSTaggedValue(obj).IsHole()) {
        return obj_value.GetRawData();
    }

    SlowRuntimeStub::ThrowDyn(thread, obj_value);
    return JSTaggedValue(JSTaggedValue::VALUE_EXCEPTION).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ThrowDyn(JSThread *thread, uint64_t obj)
{
    SlowRuntimeStub::ThrowDyn(thread, JSTaggedValue(obj));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Delobjprop(JSThread *thread, uint64_t obj, uint64_t prop)
{
    return SlowRuntimeStub::DelObjProp(thread, JSTaggedValue(obj), JSTaggedValue(prop)).GetRawData();
}

// define not constructor function
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefineNCFuncDyn(JSThread *thread, uint64_t method, uint64_t env, uint64_t home_objv,
                                                uint64_t cp)
{
    auto home_obj = JSTaggedValue(home_objv);

    auto result = JSFunction::Cast(JSTaggedValue(method).GetHeapObject());
    auto lexenv = JSTaggedValue(env);

    if (!result->GetLexicalEnv().IsUndefined()) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSTaggedValue> home_obj_handle(thread, home_obj);
        JSHandle<JSTaggedValue> lexenv_handle(thread, lexenv);
        auto res = SlowRuntimeStub::DefineNCFuncDyn(thread, result->GetCallTarget());
        if (res.IsException()) {
            return res.GetRawData();
        }
        result = JSFunction::Cast(res.GetHeapObject());
        home_obj = home_obj_handle.GetTaggedValue();         // May be moved by GC
        lexenv = lexenv_handle.GetTaggedValue();             // May be moved by GC
        result->SetConstantPool(thread, JSTaggedValue(cp));  // ConstantPool is non-movable
    }
    result->SetLexicalEnv(thread, lexenv);
    result->SetHomeObject(thread, home_obj);
    return JSTaggedValue(result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdLexVarDyn([[maybe_unused]] JSThread *thread, uint16_t level, uint16_t slot,
                                            uint64_t lex_env)
{
    LexicalEnv *env = LexicalEnv::Cast(lex_env);
    for (int i = 0; i < level; i++) {
        JSTaggedValue tagged_parent_env = env->GetParentEnv();
        ASSERT(!tagged_parent_env.IsUndefined());
        env = LexicalEnv::Cast(tagged_parent_env.GetHeapObject());
    }
    return env->GetProperties(slot).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdLexDyn([[maybe_unused]] JSThread *thread, uint64_t binding_name, uint16_t level,
                                         uint16_t slot, uint64_t lex_env)
{
    JSTaggedValue binding(LdLexVarDyn(level, slot, lex_env));

    if (!binding.IsHole()) {
        return binding.GetRawData();
    }

    SlowRuntimeStub::ThrowTdz(GetJSThread(), JSTaggedValue(binding_name));
    return JSTaggedValue(JSTaggedValue::VALUE_EXCEPTION).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t PopLexenvDyn([[maybe_unused]] JSThread *thread, uint64_t lex_env)
{
    return LexicalEnv::Cast(lex_env)->GetParentEnv().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t IsinDyn(JSThread *thread, uint64_t prop, uint64_t obj)
{
    return SlowRuntimeStub::IsInDyn(thread, JSTaggedValue(prop), JSTaggedValue(obj)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t InstanceofDyn(JSThread *thread, uint64_t obj, uint64_t target)
{
    return SlowRuntimeStub::InstanceofDyn(thread, JSTaggedValue(obj), JSTaggedValue(target)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t NewobjspreadDyn(JSThread *thread, uint64_t func, uint64_t target, uint64_t arr)
{
    return SlowRuntimeStub::NewObjSpreadDyn(thread, JSTaggedValue(func), JSTaggedValue(target), JSTaggedValue(arr))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CallspreadDyn(JSThread *thread, uint64_t func, uint64_t obj, uint64_t arr)
{
    return SlowRuntimeStub::CallSpreadDyn(thread, JSTaggedValue(func), JSTaggedValue(obj), JSTaggedValue(arr))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void JfalseDyn([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint16_t offs,
                                      [[maybe_unused]] uint64_t acc)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void JtrueDyn([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint16_t offs,
                                     [[maybe_unused]] uint64_t acc)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ReturnDyn([[maybe_unused]] JSThread *thread, [[maybe_unused]] uint64_t acc)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t NewlexenvDyn(JSThread *thread, uint16_t num_slots, uint64_t lex_env)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> env(thread, LexicalEnv::Cast(lex_env));

    JSTaggedValue res = FastRuntimeStub::NewLexicalEnvDyn(thread, factory, num_slots);
    if (res.IsHole()) {
        res = SlowRuntimeStub::NewLexicalEnvDyn(thread, num_slots);
        if (res.IsException()) {
            return res.GetRawData();
        }
    }
    LexicalEnv::Cast(res.GetHeapObject())->SetParentEnv(thread, env.GetTaggedValue());
    return res.GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CopylexenvDyn(JSThread *thread, uint64_t lex_env)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<LexicalEnv> old_env(thread, LexicalEnv::Cast(lex_env));
    JSHandle<JSTaggedValue> parent_env(thread, old_env->GetParentEnv());
    ArraySizeT num_of_slots = old_env->GetLength();

    JSTaggedValue res = FastRuntimeStub::NewLexicalEnvDyn(thread, factory, num_of_slots);
    if (res.IsHole()) {
        res = SlowRuntimeStub::NewLexicalEnvDyn(thread, num_of_slots);
        if (res.IsException()) {
            return res.GetRawData();
        }
    }
    LexicalEnv::Cast(res.GetHeapObject())->SetParentEnv(thread, parent_env.GetTaggedValue());

    for (ArraySizeT i = 0; i < num_of_slots; i++) {
        LexicalEnv::Cast(res.GetHeapObject())->Set(thread, i, old_env->Get(i));
    }
    return res.GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ThrowTdz(JSThread *thread, uint64_t prop_key)
{
    SlowRuntimeStub::ThrowTdz(thread, JSTaggedValue(prop_key));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void StLexVarDyn(JSThread *thread, uint16_t level, uint16_t slot, uint64_t value,
                                        uint64_t lex_env)
{
    JSTaggedValue current_lexenv(LexicalEnv::Cast(lex_env));
    JSTaggedValue env(current_lexenv);

    for (int i = 0; i < level; i++) {
        JSTaggedValue tagged_parent_env = LexicalEnv::Cast(env.GetHeapObject())->GetParentEnv();
        ASSERT(!tagged_parent_env.IsUndefined());
        env = tagged_parent_env;
    }
    LexicalEnv::Cast(env.GetHeapObject())->SetProperties(thread, slot, JSTaggedValue(value));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StLexDyn(JSThread *thread, uint64_t prop_key, uint16_t level, uint16_t slot,
                                         uint64_t value, uint64_t lex_env)
{
    JSTaggedValue current_lexenv(LexicalEnv::Cast(lex_env));
    JSTaggedValue env(current_lexenv);

    for (int i = 0; i < level; i++) {
        JSTaggedValue tagged_parent_env = LexicalEnv::Cast(env.GetHeapObject())->GetParentEnv();
        ASSERT(!tagged_parent_env.IsUndefined());
        env = tagged_parent_env;
    }

    LexicalEnv *lexenv = LexicalEnv::Cast(env.GetHeapObject());

    if (lexenv->GetProperties(slot).IsHole()) {
        ThrowTdz(thread, prop_key);
        return JSTaggedValue(JSTaggedValue::VALUE_EXCEPTION).GetRawData();
    }

    lexenv->SetProperties(thread, slot, JSTaggedValue(value));
    return value;
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t Toboolean(uint64_t obj)
{
    return JSTaggedValue(obj).ToBoolean() ? JSTaggedValue::True().GetRawData() : JSTaggedValue::False().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetPropIterator(JSThread *thread, uint64_t obj)
{
    return SlowRuntimeStub::GetPropIterator(thread, JSTaggedValue(obj)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefineGeneratorFunc(JSThread *thread, uint64_t method, uint64_t env, uint64_t cp)
{
    auto *result = JSFunction::Cast(JSTaggedValue(method).GetHeapObject());
    auto lexenv = JSTaggedValue(env);
    if (!result->GetLexicalEnv().IsUndefined()) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSTaggedValue> lexenv_handle(thread, lexenv);
        auto res = SlowRuntimeStub::DefineGeneratorFunc(thread, result->GetCallTarget());  // Can trigger GC
        if (res.IsException()) {
            return res.GetRawData();
        }
        result = JSFunction::Cast(res.GetHeapObject());
        lexenv = lexenv_handle.GetTaggedValue();             // May be moved by GC
        result->SetConstantPool(thread, JSTaggedValue(cp));  // ConstantPool is non-movable
    }
    result->SetLexicalEnv(thread, lexenv);
    return JSTaggedValue(result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateIterResultObj(JSThread *thread, uint64_t value, uint8_t flag)
{
    return SlowRuntimeStub::CreateIterResultObj(thread, JSTaggedValue(value), flag != 0).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ResumeGenerator(uint64_t genobj)
{
    JSGeneratorObject *obj = JSGeneratorObject::Cast(JSTaggedValue(genobj).GetHeapObject());
    return obj->GetResumeResult().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetResumeMode(uint64_t genobj)
{
    JSGeneratorObject *obj = JSGeneratorObject::Cast(JSTaggedValue(genobj).GetHeapObject());
    return obj->GetResumeMode().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateGeneratorObj(JSThread *thread, uint64_t gen_func)
{
    return SlowRuntimeStub::CreateGeneratorObj(thread, JSTaggedValue(gen_func)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SetGeneratorState(JSThread *thread, uint64_t gen_func, uint8_t state)
{
    return SlowRuntimeStub::SetGeneratorState(thread, JSTaggedValue(gen_func), state).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateAsyncGeneratorObj(JSThread *thread, uint64_t gen_func)
{
    return SlowRuntimeStub::CreateAsyncGeneratorObj(thread, JSTaggedValue(gen_func)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefineAsyncFunc(JSThread *thread, uint64_t method, uint64_t env, uint64_t cp)
{
    auto *result = JSFunction::Cast(JSTaggedValue(method).GetHeapObject());
    auto lexenv = JSTaggedValue(env);
    if (!result->GetLexicalEnv().IsUndefined()) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSTaggedValue> lexenv_handle(thread, lexenv);
        auto res = SlowRuntimeStub::DefineAsyncFunc(thread, result->GetCallTarget());  // Can trigger GC
        if (res.IsException()) {
            return res.GetRawData();
        }
        result = JSFunction::Cast(res.GetHeapObject());
        lexenv = lexenv_handle.GetTaggedValue();             // May be moved by GC
        result->SetConstantPool(thread, JSTaggedValue(cp));  // ConstantPool is non-movable
    }
    result->SetLexicalEnv(thread, lexenv);
    return JSTaggedValue(result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefineAsyncGeneratorFunc(JSThread *thread, uint64_t method, uint64_t env, uint64_t cp)
{
    auto *result = JSFunction::Cast(JSTaggedValue(method).GetHeapObject());
    auto lexenv = JSTaggedValue(env);
    if (!result->GetLexicalEnv().IsUndefined()) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSTaggedValue> lexenv_handle(thread, lexenv);
        auto res = SlowRuntimeStub::DefineAsyncGeneratorFunc(thread, result->GetCallTarget());  // Can trigger GC
        if (res.IsException()) {
            return res.GetRawData();
        }
        result = JSFunction::Cast(res.GetHeapObject());
        lexenv = lexenv_handle.GetTaggedValue();             // May be moved by GC
        result->SetConstantPool(thread, JSTaggedValue(cp));  // ConstantPool is non-movable
    }
    result->SetLexicalEnv(thread, lexenv);
    return JSTaggedValue(result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t AsyncFunctionEnter(JSThread *thread)
{
    return SlowRuntimeStub::AsyncFunctionEnter(thread).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t AsyncFunctionAwait(JSThread *thread, uint64_t async_fn_obj, uint64_t value)
{
    return SlowRuntimeStub::AsyncFunctionAwait(thread, JSTaggedValue(async_fn_obj), JSTaggedValue(value)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t AsyncFunctionResolve(JSThread *thread, uint64_t async_fn_obj, uint64_t value)
{
    return SlowRuntimeStub::AsyncFunctionResolveOrReject(thread, JSTaggedValue(async_fn_obj), JSTaggedValue(value),
                                                         true)
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t AsyncFunctionReject(JSThread *thread, uint64_t async_fn_obj, uint64_t value)
{
    return SlowRuntimeStub::AsyncFunctionResolveOrReject(thread, JSTaggedValue(async_fn_obj), JSTaggedValue(value),
                                                         false)
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t AsyncGeneratorResolve(JSThread *thread, uint64_t async_fn_obj, uint64_t value)
{
    return SlowRuntimeStub::AsyncGeneratorResolve(thread, JSTaggedValue(async_fn_obj), JSTaggedValue(value))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t AsyncGeneratorReject(JSThread *thread, uint64_t async_fn_obj, uint64_t value)
{
    return SlowRuntimeStub::AsyncGeneratorReject(thread, JSTaggedValue(async_fn_obj), JSTaggedValue(value))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ThrowConstAssignment(JSThread *thread, uint64_t name)
{
    return SlowRuntimeStub::ThrowConstAssignment(thread, JSTaggedValue(name));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetMethod(JSThread *thread, uint64_t prop_key, uint64_t object)
{
    return SlowRuntimeStub::GetMethod(thread, JSTaggedValue(object), JSTaggedValue(prop_key)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetTemplateObject(JSThread *thread, uint64_t literal)
{
    return SlowRuntimeStub::GetTemplateObject(thread, JSTaggedValue(literal)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetNextPropName(JSThread *thread, uint64_t iter)
{
    return SlowRuntimeStub::GetNextPropName(thread, JSTaggedValue(iter)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateEmptyObject(JSThread *thread)
{
    auto factory = thread->GetEcmaVM()->GetFactory();
    auto global_env = thread->GetEcmaVM()->GetGlobalEnv();
    return SlowRuntimeStub::CreateEmptyObject(thread, factory, global_env).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateObjectWithBuffer(JSThread *thread, uint64_t literal)
{
    JSObject *result = JSObject::Cast(JSTaggedValue(literal).GetHeapObject());
    return SlowRuntimeStub::CreateObjectWithBuffer(thread, thread->GetEcmaVM()->GetFactory(), result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SetObjectWithProto(JSThread *thread, uint64_t proto, uint64_t obj)
{
    return SlowRuntimeStub::SetObjectWithProto(thread, JSTaggedValue(proto), JSTaggedValue(obj)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CopyDataProperties(JSThread *thread, uint64_t dst, uint64_t src)
{
    return SlowRuntimeStub::CopyDataProperties(thread, JSTaggedValue(dst), JSTaggedValue(src)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefineGetterSetterByValue(JSThread *thread, uint64_t obj, uint64_t prop,
                                                          uint64_t getter, uint64_t setter, uint64_t flag)
{
    return SlowRuntimeStub::DefineGetterSetterByValue(thread, JSTaggedValue(obj), JSTaggedValue(prop),
                                                      JSTaggedValue(getter), JSTaggedValue(setter), bool(flag))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateEmptyArray(JSThread *thread)
{
    auto factory = thread->GetEcmaVM()->GetFactory();
    auto global_env = thread->GetEcmaVM()->GetGlobalEnv();

    return SlowRuntimeStub::CreateEmptyArray(thread, factory, global_env).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateArrayWithBuffer(JSThread *thread, uint64_t array)
{
    auto factory = thread->GetEcmaVM()->GetFactory();
    JSArray *result = JSArray::Cast(JSTaggedValue(array).GetHeapObject());
    return SlowRuntimeStub::CreateArrayWithBuffer(thread, factory, result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateRegExpWithLiteral(JSThread *thread, uint64_t pattern, uint8_t flags)
{
    return SlowRuntimeStub::CreateRegExpWithLiteral(thread, JSTaggedValue(pattern), flags).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StOwnByIndex(JSThread *thread, uint64_t object, uint64_t idx, uint64_t val)
{
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
    if (JSTaggedValue(object).IsHeapObject() && !JSTaggedValue(object).IsClassConstructor() &&
        !JSTaggedValue(object).IsClassPrototype()) {
        JSTaggedValue res =
            FastRuntimeStub::SetPropertyByIndex<true>(thread, JSTaggedValue(object), idx, JSTaggedValue(val));
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }

    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    return SlowRuntimeStub::StOwnByIndex(thread, JSTaggedValue(object), idx, JSTaggedValue(val)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StOwnByName(JSThread *thread, uint64_t prop, uint64_t object, uint64_t val)
{
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
    if (JSTaggedValue(object).IsJSObject() && !JSTaggedValue(object).IsClassConstructor() &&
        !JSTaggedValue(object).IsClassPrototype()) {
        JSTaggedValue res = FastRuntimeStub::SetPropertyByName<true>(thread, JSTaggedValue(object), JSTaggedValue(prop),
                                                                     JSTaggedValue(val));
        if (!res.IsHole()) {
            return res.GetRawData();
        }
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");

    return SlowRuntimeStub::StOwnByName(thread, JSTaggedValue(object), JSTaggedValue(prop), JSTaggedValue(val))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StOwnByValue(JSThread *thread, uint64_t rec, uint64_t pkey, uint64_t val)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> receiver(thread, JSTaggedValue(rec));
    JSHandle<JSTaggedValue> prop(thread, JSTaggedValue(pkey));
    JSHandle<JSTaggedValue> value(thread, JSTaggedValue(val));

    if (receiver->IsHeapObject() && !receiver->IsClassConstructor() && !receiver->IsClassPrototype()) {
        JSTaggedValue res = FastRuntimeStub::SetPropertyByValue<true>(thread, receiver, prop, value);
        if (!res.IsHole()) {
            if (value->IsJSFunction()) {
                JSFunction::SetFunctionNameNoPrefix(thread, JSFunction::Cast(value.GetTaggedValue().GetHeapObject()),
                                                    prop.GetTaggedValue());
            }
            return res.GetRawData();
        }
    }
    return SlowRuntimeStub::StOwnByValue(thread, receiver, prop, value).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StArraySpread(JSThread *thread, uint64_t dst, uint64_t index, uint64_t src)
{
    return SlowRuntimeStub::StArraySpread(thread, JSTaggedValue(dst), JSTaggedValue(index), JSTaggedValue(src))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetIterator(JSThread *thread, uint64_t object)
{
    return SlowRuntimeStub::GetIterator(thread, JSTaggedValue(object)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetAsyncIterator(JSThread *thread, uint64_t object)
{
    return SlowRuntimeStub::GetIterator(thread, JSTaggedValue(object), true).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ThrowIfNotObject(JSThread *thread, uint64_t object)
{
    if (JSTaggedValue(object).IsECMAObject()) {
        return JSTaggedValue(JSTaggedValue::VALUE_HOLE).GetRawData();
    }
    SlowRuntimeStub::ThrowIfNotObject(thread);
    return JSTaggedValue(JSTaggedValue::VALUE_EXCEPTION).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ThrowThrowNotExists(JSThread *thread)
{
    SlowRuntimeStub::ThrowThrowNotExists(thread);
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateObjectWithExcludedKeys(JSThread *thread, uint16_t num_keys, uint64_t obj_value,
                                                             uint64_t excluded_keys)
{
    return SlowRuntimeStub::CreateObjectWithExcludedKeys(thread, num_keys, JSTaggedValue(obj_value),
                                                         reinterpret_cast<JSTaggedType *>(excluded_keys))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ThrowPatternNonCoercible(JSThread *thread)
{
    SlowRuntimeStub::ThrowPatternNonCoercible(thread);
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CloseIterator(JSThread *thread, uint64_t iter, uint64_t completion)
{
    JSTaggedValue completion_value(completion);

    if (!completion_value.IsHole()) {
        SlowRuntimeStub::ThrowDyn(thread, completion_value);
    }

    return SlowRuntimeStub::CloseIterator(thread, JSTaggedValue(iter)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ImportModule(JSThread *thread, uint64_t prop)
{
    return SlowRuntimeStub::ImportModule(thread, JSTaggedValue(prop)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void StModuleVar(JSThread *thread, uint64_t prop, uint64_t value)
{
    SlowRuntimeStub::StModuleVar(thread, JSTaggedValue(prop), JSTaggedValue(value));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void CopyModule(JSThread *thread, uint64_t module)
{
    SlowRuntimeStub::CopyModule(thread, JSTaggedValue(module));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdModvarByName(JSThread *thread, uint64_t item_name, uint64_t module_obj)
{
    return SlowRuntimeStub::LdModvarByName(thread, JSTaggedValue(module_obj), JSTaggedValue(item_name)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ClassFieldAdd(JSThread *thread, uint64_t ctor, uint64_t prop, uint64_t value)
{
    return SlowRuntimeStub::ClassFieldAdd(thread, JSTaggedValue(ctor), JSTaggedValue(prop), JSTaggedValue(value))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void DefineClassPrivateFields(JSThread *thread, uint64_t private_buf, uint64_t env,
                                                     uint64_t ctor, uint64_t cp)
{
    auto *constpool = ConstantPool::Cast(cp);
    SlowRuntimeStub::DefineClassPrivateFields(thread, constpool, JSTaggedValue(env), JSTaggedValue(ctor),
                                              JSTaggedValue(private_buf));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ClassPrivateMethodOrAccessorAdd(JSThread *thread, uint64_t ctor, uint64_t obj)
{
    return SlowRuntimeStub::ClassPrivateMethodOrAccessorAdd(thread, JSTaggedValue(ctor), JSTaggedValue(obj))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ClassPrivateFieldAdd(JSThread *thread, uint64_t prop, uint64_t ctor, uint64_t obj,
                                                     uint64_t value)
{
    return SlowRuntimeStub::ClassPrivateFieldAdd(thread, JSTaggedValue(ctor), JSTaggedValue(obj), JSTaggedValue(prop),
                                                 JSTaggedValue(value))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ClassPrivateFieldGet(JSThread *thread, uint64_t prop, uint64_t ctor, uint64_t obj)
{
    return SlowRuntimeStub::ClassPrivateFieldGet(thread, JSTaggedValue(ctor), JSTaggedValue(obj), JSTaggedValue(prop))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ClassPrivateFieldSet(JSThread *thread, uint64_t prop, uint64_t ctor, uint64_t obj,
                                                     uint64_t value)
{
    return SlowRuntimeStub::ClassPrivateFieldSet(thread, JSTaggedValue(ctor), JSTaggedValue(obj), JSTaggedValue(prop),
                                                 JSTaggedValue(value))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ClassPrivateFieldIn(JSThread *thread, uint64_t prop, uint64_t ctor, uint64_t obj)
{
    return SlowRuntimeStub::ClassPrivateFieldIn(thread, JSTaggedValue(ctor), JSTaggedValue(obj), JSTaggedValue(prop))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefineClassWithBuffer(JSThread *thread, uint64_t ctor, uint16_t imm, uint64_t lexenv,
                                                      uint64_t proto, uint64_t cp)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto *constpool = ConstantPool::Cast(cp);
    JSHandle<JSTaggedValue> lex_env(thread, JSTaggedValue(lexenv));
    JSFunction *class_template = JSFunction::Cast(JSTaggedValue(ctor).GetTaggedObject());
    ASSERT(class_template != nullptr);

    TaggedArray *literal_buffer = TaggedArray::Cast(constpool->GetObjectFromCache(imm).GetTaggedObject());
    // TaggedArray *literal_buffer = TaggedArray::Cast(JSTaggedValue(buffer).GetTaggedObject());
    JSTaggedValue res;
    if (LIKELY(!class_template->IsResolved())) {
        res = SlowRuntimeStub::ResolveClass(thread, JSTaggedValue(class_template), literal_buffer, JSTaggedValue(proto),
                                            JSTaggedValue(lexenv), constpool);
    } else {
        res = SlowRuntimeStub::CloneClassFromTemplate(thread, JSTaggedValue(class_template), JSTaggedValue(proto),
                                                      JSTaggedValue(lexenv), constpool);
    }

    if (res.IsException()) {
        return res.GetRawData();
    }

    ASSERT(res.IsClassConstructor());
    JSFunction *cls = JSFunction::Cast(res.GetTaggedObject());
    cls->SetLexicalEnv(thread, lex_env.GetTaggedValue());
    SlowRuntimeStub::SetClassConstructorLength(thread, res, cls->GetMethod()->GetLength());
    return res.GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void SetClassComputedFields(JSThread *thread, uint64_t class_reg, uint64_t computed_fields)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSConstructorFunction> cls(thread, JSTaggedValue(class_reg));
    JSHandle<JSArray> computed_fields_handle(thread, JSTaggedValue(computed_fields));
    cls->SetComputedFields(thread, computed_fields_handle);
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LoadClassComputedInstanceFields(JSThread *thread, uint64_t class_reg)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSConstructorFunction> cls(thread, JSTaggedValue(class_reg));
    JSTaggedValue computed_instance_fields = cls->GetComputedFields();
    ASSERT(computed_instance_fields.IsJSArray());
    cls->SetComputedFields(thread, JSTaggedValue::Hole());

    return computed_instance_fields.GetRawData();
}

// Just declared here, not used
// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SuperCall(JSThread *thread, uint16_t range, uint64_t args, uint64_t new_target,
                                          uint64_t obj_value)
{
    return SlowRuntimeStub::SuperCall(thread, JSTaggedValue(obj_value), JSTaggedValue(new_target), range,
                                      reinterpret_cast<JSTaggedType *>(args))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t SuperCallSpread(JSThread *thread, uint64_t array, uint64_t new_target,
                                                uint64_t this_func)
{
    return SlowRuntimeStub::SuperCallSpread(thread, JSTaggedValue(this_func), JSTaggedValue(new_target),
                                            JSTaggedValue(array))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DefineMethod(JSThread *thread, uint64_t method, uint64_t tagged_cur_env,
                                             uint64_t home_object, uint64_t cp)
{
    auto result = JSFunction::Cast(JSTaggedValue(method).GetHeapObject());
    auto home = JSTaggedValue(home_object);
    auto env = JSTaggedValue(tagged_cur_env);
    if (!result->GetLexicalEnv().IsUndefined()) {
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSTaggedValue> home_handle(thread, home);
        JSHandle<JSTaggedValue> env_handle(thread, env);
        auto res = SlowRuntimeStub::DefineMethod(thread, result->GetCallTarget(), home_handle);
        if (res.IsException()) {
            return res.GetRawData();
        }
        result = JSFunction::Cast(res.GetHeapObject());
        env = env_handle.GetTaggedValue();                   // Maybe moved by GC
        result->SetConstantPool(thread, JSTaggedValue(cp));  // ConstantPool is non-movable
    } else {
        result->SetHomeObject(thread, home);
    }
    result->SetLexicalEnv(thread, env);
    return JSTaggedValue(result).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StSuperByName(JSThread *thread, uint64_t prop_key, uint64_t obj, uint64_t value,
                                              uint64_t func)
{
    return SlowRuntimeStub::StSuperByValue(thread, JSTaggedValue(obj), JSTaggedValue(prop_key), JSTaggedValue(value),
                                           JSTaggedValue(func))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdSuperByName(JSThread *thread, uint64_t prop_key, uint64_t obj, uint64_t func)
{
    return SlowRuntimeStub::LdSuperByValue(thread, JSTaggedValue(obj), JSTaggedValue(prop_key), JSTaggedValue(func))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StSuperByValue(JSThread *thread, uint64_t receiver, uint64_t prop_key, uint64_t value,
                                               uint64_t func)
{
    return SlowRuntimeStub::StSuperByValue(thread, JSTaggedValue(receiver), JSTaggedValue(prop_key),
                                           JSTaggedValue(value), JSTaggedValue(func))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdSuperByValue(JSThread *thread, uint64_t receiver, uint64_t prop_key, uint64_t func)
{
    return SlowRuntimeStub::LdSuperByValue(thread, JSTaggedValue(receiver), JSTaggedValue(prop_key),
                                           JSTaggedValue(func))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t CreateObjectHavingMethod(JSThread *thread, uint64_t literal, uint64_t env, uint64_t cp)
{
    auto constpool = ConstantPool::Cast(cp);
    JSObject *result = JSObject::Cast(JSTaggedValue(literal).GetHeapObject());
    auto factory = GetFactory(thread);

    return SlowRuntimeStub::CreateObjectHavingMethod(thread, factory, result, JSTaggedValue(env), constpool)
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t ThrowIfSuperNotCorrectCall(JSThread *thread, uint16_t index,
                                                           [[maybe_unused]] uint64_t this_value)
{
    return SlowRuntimeStub::ThrowIfSuperNotCorrectCall(thread, index, JSTaggedValue(this_value)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdHomeObject([[maybe_unused]] JSThread *thread, uint64_t func)
{
    return JSFunction::Cast(JSTaggedValue(func).GetHeapObject())->GetHomeObject().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void ThrowDeleteSuperProperty(JSThread *thread)
{
    SlowRuntimeStub::ThrowDeleteSuperProperty(thread);
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdEvalBindings([[maybe_unused]] JSThread *thread, uint64_t result)
{
    return result;
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DirectEval(JSThread *thread, uint32_t status, uint64_t arg0, uint64_t eval_bindings)
{
    return EvalUtils::DirectEval(thread, status, JSTaggedValue(arg0), JSTaggedValue(eval_bindings)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdEvalVar(JSThread *thread, uint64_t name, uint64_t lexical_context)
{
    return SlowRuntimeStub::LdEvalVar(thread, JSTaggedValue(name), JSTaggedValue(lexical_context)).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StEvalVar(JSThread *thread, uint64_t name, uint64_t value, uint64_t lexical_context)
{
    return SlowRuntimeStub::StEvalVar(thread, JSTaggedValue(name), JSTaggedValue(lexical_context), JSTaggedValue(value))
        .GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void Debugger()
{
    // no-op
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void NativeMethodWrapper([[maybe_unused]] uint64_t arg)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint8_t GetObjectClassType([[maybe_unused]] uint64_t arg)
{
    UNREACHABLE();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t IsNan([[maybe_unused]] double arg)
{
    return static_cast<uint64_t>(std::isnan(arg));
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t GetWeakReferent(uint64_t ref)
{
    return JSTaggedValue(JSTaggedValue(ref).GetWeakReferentUnChecked()).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint8_t DynClassIsDictionaryElement([[maybe_unused]] uint64_t obj)
{
    JSHClass *cls = JSHClass::Cast(JSTaggedValue(obj).GetTaggedObject());
    return static_cast<uint8_t>(cls->IsDictionaryElement());
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint8_t DynClassIsExtensible([[maybe_unused]] uint64_t obj)
{
    JSHClass *cls = JSHClass::Cast(JSTaggedValue(obj).GetTaggedObject());
    return static_cast<uint8_t>(cls->IsExtensible());
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t DynObjectGetClass(uint64_t obj)
{
    JSTaggedValue tagged_value(obj);
    return JSTaggedValue(tagged_value.GetTaggedObject()->GetClass()).GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS void DynObjectSetClass(uint64_t obj, uint64_t hclass)
{
    auto *cls = JSHClass::Cast(JSTaggedValue(hclass).GetTaggedObject());
    JSTaggedValue(obj).GetTaggedObject()->SetClass(cls);
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint32_t DynClassNumberOfProps(uint64_t obj)
{
    auto *cls = JSHClass::Cast(JSTaggedValue(obj).GetTaggedObject());
    return cls->NumberOfProps();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint32_t DynClassGetHash(uint64_t obj)
{
    auto *cls = JSHClass::Cast(JSTaggedValue(obj).GetTaggedObject());
    return JSHClass::Hash(cls);
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t LdObjDynByName([[maybe_unused]] uint64_t obj, [[maybe_unused]] uint64_t key,
                                               [[maybe_unused]] uint16_t ic_slot)
{
    return JSTaggedValue::Hole().GetRawData();
}

// NOLINTNEXTLINE(misc-definitions-in-headers)
INLINE_ECMA_INTRINSICS uint64_t StObjDynByName([[maybe_unused]] uint64_t obj, [[maybe_unused]] uint64_t key,
                                               [[maybe_unused]] uint64_t value, [[maybe_unused]] uint16_t ic_slot)
{
    return JSTaggedValue::Hole().GetRawData();
}

}  // namespace panda::ecmascript::intrinsics

#endif  // PLUGINS_ECMASCRIPT_RUNTIME_INTRINSICS_INL_H
