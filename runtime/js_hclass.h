/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_HCLASS_H
#define ECMASCRIPT_JS_HCLASS_H

#include "include/mem/allocator.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/mem/tagged_object.h"
#include "plugins/ecmascript/runtime/property_attributes.h"
#include "runtime/include/coretypes/dyn_objects.h"
#include "runtime/include/hclass.h"

#include "utils/bit_field.h"

/*
 *                         JS Object and JS HClass Layout
 *
 *      Properties                         JS Object                    JS HClass
 *      +------------+                     +------------+               +------------------+
 *      |arrayClass  + <---------|         |JS HClass   +-------------->|HClass class      |
 *      +------------+           |         +------------+               +------------------+
 *      |property 0  |           |         |Hash        |               |BitField          |
 *      +------------+           |         +------------+               +------------------+
 *      |property 1  |           |-------  |Properties  |               |BitField1         |
 *      +------------+                     +------------+               +------------------+
 *      |...         |           |-------  |Elements    |               |Proto             |
 *      +------------+           |         +------------+               +------------------+
 *                               |         |in-obj 0    |               |Layout            |
 *      Elements                 |         +------------+               +------------------+
 *      +------------+           |         |in-obj 1    |               |Transitions       |
 *      |arrayClass  + <---------|         +------------+               +------------------+
 *      +------------+                     |...         |               |Parent            |
 *      |value 0     |                     +------------+               +------------------+
 *      +------------+                                                  |ProtoChangeMarker |
 *      |value 1     |                                                  +------------------+
 *      +------------+                                                  |ProtoChangeDetails|
 *      |...         |                                                  +------------------+
 *      +------------+                                                  |EnumCache         |
 *                                                                      +------------------+
 *
 *                          Proto: [[Prototype]] in Ecma spec
 *                          Layout: record key and attr
 *                          ProtoChangeMarker, ProtoChangeDetails: monitor [[prototype]] chain
 *                          EnumCache: use for for-in syntax
 *
 */
namespace panda::ecmascript {
class ProtoChangeDetails;

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define JSTYPE_DECL       /* //////////////////////////////////////////////////////////////////////////////-PADDING */ \
    INVALID = 0,          /* //////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_OBJECT,        /* JS_OBJECT_BEGIN ////////////////////////////////////////////////////////////////////// */ \
        JS_REALM,         /* //////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_FUNCTION_BASE, /* //////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_FUNCTION,      /* //////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_PROXY_REVOC_FUNCTION,       /* /////////////////////////////////////////////////////////////////-PADDING */ \
        JS_PROMISE_REACTIONS_FUNCTION, /* /////////////////////////////////////////////////////////////////-PADDING */ \
        JS_PROMISE_EXECUTOR_FUNCTION,  /* /////////////////////////////////////////////////////////////////-PADDING */ \
        JS_PROMISE_ALL_RESOLVE_ELEMENT_FUNCTION, /* ///////////////////////////////////////////////////////-PADDING */ \
        JS_GENERATOR_FUNCTION, /* /////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ASYNC_FUNCTION, /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ASYNC_GENERATOR_FUNCTION, /* ///////////////////////////////////////////////////////////////////-PADDING */ \
        JS_INTL_BOUND_FUNCTION, /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ASYNC_AWAIT_STATUS_FUNCTION, /* ////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ASYNC_GENERATOR_RESOLVE_NEXT_FUNCTION, /* //////////////////////////////////////////////////////-PADDING */ \
        JS_ASYNC_FROM_SYNC_ITERATOR_VALUE_UNWRAP_FUNCTION, /* /////////////////////////////////////////////-PADDING */ \
        JS_BOUND_FUNCTION, /*  //////////////////////////////////////////////////////////////////////////////////// */ \
                                                                                                                       \
        JS_ERROR,           /* JS_ERROR_BEGIN /////////////////////////////////////////////////////////////-PADDING */ \
        JS_EVAL_ERROR,      /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_RANGE_ERROR,     /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_REFERENCE_ERROR, /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_TYPE_ERROR,      /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_URI_ERROR,       /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_SYNTAX_ERROR,    /* JS_ERROR_END /////////////////////////////////////////////////////////////////////// */ \
                                                                                                                       \
        JS_REG_EXP,  /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_SET,      /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_MAP,      /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_WEAK_REF, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_WEAK_MAP, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_WEAK_SET, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_DATE,     /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ITERATOR, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_FORIN_ITERATOR, /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_MAP_ITERATOR,   /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_REG_EXP_ITERATOR, /* ///////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_SET_ITERATOR,    /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ARRAY_ITERATOR,  /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_STRING_ITERATOR, /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_INTL, /* ///////////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_LOCALE, /* /////////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_DATE_TIME_FORMAT, /* ///////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_RELATIVE_TIME_FORMAT, /* ///////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_NUMBER_FORMAT, /* //////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_COLLATOR, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_PLURAL_RULES, /* ///////////////////////////////////////////////////////////////////////////////-PADDING */ \
        FINALIZATION_REGISTRY, /* /////////////////////////////////////////////////////////////////////////-PADDING */ \
                                                                                                                       \
        JS_ARRAY_BUFFER, /* ///////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_PROMISE,      /* ///////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_DATA_VIEW,    /* /////////////////////////////////////////////////////////////////////////////////////// */ \
        JS_ARGUMENTS, /* //////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_GENERATOR_OBJECT, /* //////////////////////////////////////////////////////////////////////////-PADDING */  \
        JS_ASYNC_FROM_SYNC_ITERATOR_OBJECT, /* ////////////////////////////////////////////////////////////-PADDING */ \
        JS_ASYNC_FUNC_OBJECT, /* //////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ASYNC_GENERATOR_OBJECT, /* /////////////////////////////////////////////////////////////////////-PADDING */ \
                                                                                                                       \
        /* SPECIAL indexed objects begin, DON'T CHANGE HERE ///////////////////////////////////////////////-PADDING */ \
        JS_ARRAY,      /* ////////////////////////////////////////////////////////////////////////////////-PADDING */  \
        JS_ARRAY_LIST, /* /////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_QUEUE,      /* /////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_TYPED_ARRAY, /* JS_TYPED_ARRAY_BEGIN /////////////////////////////////////////////////////////////////// */ \
        JS_INT8_ARRAY,  /* ////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_UINT8_ARRAY, /* ////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_UINT8_CLAMPED_ARRAY, /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_INT16_ARRAY,         /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_UINT16_ARRAY,        /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_INT32_ARRAY,         /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_UINT32_ARRAY,        /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_FLOAT32_ARRAY,       /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_FLOAT64_ARRAY,       /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_BIGINT64_ARRAY,      /* ////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_BIGUINT64_ARRAY,     /* JS_TYPED_ARRAY_END ///////////////////////////////////////////////////////////// */ \
        JS_PRIMITIVE_REF, /* number\boolean\string. SPECIAL indexed objects end, DON'T CHANGE HERE ////////-PADDING */ \
        JS_GLOBAL_OBJECT, /* JS_OBJECT_END/////////////////////////////////////////////////////////////////-PADDING */ \
        JS_PROXY, /* ECMA_OBJECT_END ////////////////////////////////////////////////////////////////////////////// */ \
                                                                                                                       \
        HCLASS, /* ////////////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        STRING, /* ////////////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        BIGINT, /* ////////////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        TAGGED_ARRAY, /* //////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        TAGGED_DICTIONARY, /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        LINKED_HASH_SET, /* ///////////////////////////////////////////////////////////////////////////////-PADDING */ \
        LINKED_HASH_MAP, /* ///////////////////////////////////////////////////////////////////////////////-PADDING */ \
        FREE_OBJECT_WITH_ONE_FIELD, /* ////////////////////////////////////////////////////////////////////-PADDING */ \
        FREE_OBJECT_WITH_NONE_FIELD, /* ///////////////////////////////////////////////////////////////////-PADDING */ \
        FREE_OBJECT_WITH_TWO_FIELD, /* ////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_NATIVE_POINTER, /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        GLOBAL_ENV,        /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        ACCESSOR_DATA,     /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        INTERNAL_ACCESSOR, /* /////////////////////////////////////////////////////////////////////////////-PADDING */ \
        SYMBOL, /* ////////////////////////////////////////////////////////////////////////////////////////-PADDING */ \
        OBJECT_WRAPPER,       /* //////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_GENERATOR_CONTEXT, /* //////////////////////////////////////////////////////////////////////////-PADDING */ \
        PROTOTYPE_HANDLER,    /* //////////////////////////////////////////////////////////////////////////-PADDING */ \
        TRANSITION_HANDLER,   /* //////////////////////////////////////////////////////////////////////////-PADDING */ \
        PROPERTY_BOX, /* /////////////////////////////////////////////////////////////////////////////////-PADDING */  \
        PROTO_CHANGE_MARKER, /* ///////////////////////////////////////////////////////////////////////////-PADDING */ \
        PROTOTYPE_INFO, /* ////////////////////////////////////////////////////////////////////////////-PADDING */     \
        TEMPLATE_MAP,   /* ////////////////////////////////////////////////////////////////////////////-PADDING */     \
        PROGRAM, /* /////////////////////////////////////////////////////////////////////////////////-PADDING */       \
        LEXICAL_FUNCTION, /* ////////////////////////////////////////////////////////////////////////-PADDING */       \
                                                                                                                       \
        PROMISE_CAPABILITY, /* JS_RECORD_BEGIN //////////////////////////////////////////////////////////////////// */ \
        PROMISE_RECORD,     /* ////////////////////////////////////////////////////////////////////////////-PADDING */ \
        RESOLVING_FUNCTIONS_RECORD, /* ////////////////////////////////////////////////////////////////////-PADDING */ \
        PROMISE_REACTIONS,          /* ////////////////////////////////////////////////////////////////////-PADDING */ \
        PROMISE_ITERATOR_RECORD,    /* ////////////////////////////////////////////////////////////////////-PADDING */ \
        MICRO_JOB_QUEUE, /* /////////////////////////////////////////////////////////////////////////////-PADDING */   \
        PENDING_JOB,     /* /////////////////////////////////////////////////////////////////////////////-PADDING */   \
        FUNCTION_EXTRA_INFO, /* ////////////////////////////////////////////////////////////////////////-PADDING */    \
        COMPLETION_RECORD, /* JS_RECORD_END /////////////////////////////////////////////////////////////////////// */ \
        ECMA_MODULE, /*  /////////////////////////////////////////////////////////////////////////////////-PADDING */  \
        CLASS_INFO_EXTRACTOR, /* //////////////////////////////////////////////////////////////////////////-PADDING */ \
        JS_TYPE_LAST = CLASS_INFO_EXTRACTOR, /* ///////////////////////////////////////////////////////////-PADDING */ \
                                                                                                                       \
        JS_FUNCTION_BEGIN = JS_FUNCTION, /* ///////////////////////////////////////////////////////////////-PADDING */ \
        JS_FUNCTION_END = JS_ASYNC_FROM_SYNC_ITERATOR_VALUE_UNWRAP_FUNCTION, /* ///////////////////////////-PADDING */ \
                                                                                                                       \
        JS_OBJECT_BEGIN = JS_OBJECT, /* ///////////////////////////////////////////////////////////////////-PADDING */ \
        JS_OBJECT_END = JS_GLOBAL_OBJECT, /* //////////////////////////////////////////////////////////////-PADDING */ \
                                                                                                                       \
        ECMA_OBJECT_BEGIN = JS_OBJECT, /* /////////////////////////////////////////////////////////////////-PADDING */ \
        ECMA_OBJECT_END = JS_PROXY,    /* /////////////////////////////////////////////////////////////////-PADDING */ \
                                                                                                                       \
        JS_ERROR_BEGIN = JS_ERROR,      /* ////////////////////////////////////////////////////////////////-PADDING */ \
        JS_ERROR_END = JS_SYNTAX_ERROR, /* ////////////////////////////////////////////////////////////////-PADDING */ \
                                                                                                                       \
        JS_ITERATOR_BEGIN = JS_ITERATOR,      /* //////////////////////////////////////////////////////////-PADDING */ \
        JS_ITERATOR_END = JS_STRING_ITERATOR, /* //////////////////////////////////////////////////////////-PADDING */ \
                                                                                                                       \
        JS_RECORD_BEGIN = PROMISE_CAPABILITY, /* //////////////////////////////////////////////////////////-PADDING */ \
        JS_RECORD_END = COMPLETION_RECORD,    /* ///////////////////////////////////////////////////////-PADDING */    \
                                                                                                                       \
        TAGGED_ARRAY_BEGIN = TAGGED_ARRAY, /* /////////////////////////////////////////////////////////////-PADDING */ \
        TAGGED_ARRAY_END = LINKED_HASH_MAP, /* ///////////////////////////////////////////////////////////-PADDING */  \
                                                                                                                       \
        JS_TYPED_ARRAY_BEGIN = JS_TYPED_ARRAY, /* /////////////////////////////////////////////////////////-PADDING */ \
        JS_TYPED_ARRAY_END = JS_BIGUINT64_ARRAY /* ////////////////////////////////////////////////////////-PADDING */

enum class JSType : uint8_t {
    JSTYPE_DECL,
};

class JSHClass : public TaggedObject {
public:
    static constexpr int TYPE_BITFIELD_NUM = 8;
    using ObjectTypeBits = BitField<JSType, 0, TYPE_BITFIELD_NUM>;  // 7
    using Unused1 = ObjectTypeBits::NextFlag;
    using ConstructorBit = Unused1::NextFlag;  // 9
    using Unused2 = ConstructorBit::NextFlag;  // 10
    using ExtensibleBit = Unused2::NextFlag;
    using IsPrototypeBit = ExtensibleBit::NextFlag;
    using ElementRepresentationBits = IsPrototypeBit::NextField<Representation, 3>;  // 3 means next 3 bit
    using DictionaryElementBits = ElementRepresentationBits::NextFlag;               // 16
    using IsDictionaryBit = DictionaryElementBits::NextFlag;                         // 17
    using IsStableElementsBit = IsDictionaryBit::NextFlag;                           // 18
    using HasConstructorBits = IsStableElementsBit::NextFlag;                        // 19
    using IsLiteralBit = HasConstructorBits::NextFlag;                               // 20
    using ClassConstructorBit = IsLiteralBit::NextFlag;                              // 21
    using ClassPrototypeBit = ClassConstructorBit::NextFlag;                         // 22
    using WeakContainerBit = ClassPrototypeBit::NextFlag;                            // 23

    static constexpr uint32_t DEFAULT_CAPACITY_OF_IN_OBJECTS = 4;
    static constexpr uint32_t MAX_CAPACITY_OF_OUT_OBJECTS =
        PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES - DEFAULT_CAPACITY_OF_IN_OBJECTS;
    static constexpr uint32_t OFFSET_MAX_OBJECT_SIZE_IN_WORDS_WITHOUT_INLINED = 5;
    static constexpr uint32_t OFFSET_MAX_OBJECT_SIZE_IN_WORDS =
        PropertyAttributes::OFFSET_BITFIELD_NUM + OFFSET_MAX_OBJECT_SIZE_IN_WORDS_WITHOUT_INLINED;
    static constexpr uint32_t MAX_OBJECT_SIZE_IN_WORDS = (1U << OFFSET_MAX_OBJECT_SIZE_IN_WORDS) - 1;

    using NumberOfPropsBits = BitField<uint32_t, 0, PropertyAttributes::OFFSET_BITFIELD_NUM>;  // 10
    using InlinedPropsStartBits = NumberOfPropsBits::NextField<uint32_t,
                                                               OFFSET_MAX_OBJECT_SIZE_IN_WORDS_WITHOUT_INLINED>;  // 15

    static uint32_t Hash(JSHClass *cls)
    {
        return static_cast<uint32_t>(reinterpret_cast<uintptr_t>(cls)) >> 3U;
    }

    static JSHClass *Cast(const TaggedObject *object);

    inline bool HasReferenceField();

    // size need to add inlined property numbers
    void Initialize(const JSThread *thread, uint32_t size, JSType type, uint32_t inlined_props, uint32_t flags = 0);

    static JSHandle<JSHClass> Clone(const JSThread *thread, const JSHandle<JSHClass> &jshclass,
                                    bool without_inlined_properties = false);
    static JSHandle<JSHClass> CloneWithoutInlinedProperties(const JSThread *thread, const JSHandle<JSHClass> &jshclass);

    static void TransitionElementsToDictionary(const JSThread *thread, const JSHandle<JSObject> &obj);
    static void AddProperty(const JSThread *thread, const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                            const PropertyAttributes &attr);

    static JSHandle<JSHClass> TransitionExtension(const JSThread *thread, const JSHandle<JSHClass> &jshclass);
    static JSHandle<JSHClass> TransitionProto(const JSThread *thread, const JSHandle<JSHClass> &jshclass,
                                              const JSHandle<JSTaggedValue> &proto);
    static void TransitionToDictionary(const JSThread *thread, const JSHandle<JSObject> &obj);

    static JSHandle<JSTaggedValue> EnableProtoChangeMarker(const JSThread *thread, const JSHandle<JSHClass> &jshclass);

    static void NotifyHclassChanged(const JSThread *thread, JSHandle<JSHClass> old_hclass,
                                    JSHandle<JSHClass> new_hclass);

    static void RegisterOnProtoChain(const JSThread *thread, const JSHandle<JSHClass> &jshclass);

    static bool UnregisterOnProtoChain(const JSThread *thread, const JSHandle<JSHClass> &jshclass);

    static JSHandle<ProtoChangeDetails> GetProtoChangeDetails(const JSThread *thread,
                                                              const JSHandle<JSHClass> &jshclass);

    static JSHandle<ProtoChangeDetails> GetProtoChangeDetails(const JSThread *thread, const JSHandle<JSObject> &obj);

    inline void UpdatePropertyMetaData(const JSThread *thread, const JSTaggedValue &key,
                                       const PropertyAttributes &meta_data);

    static void NoticeRegisteredUser(const JSThread *thread, const JSHandle<JSHClass> &jshclass);

    static void NoticeThroughChain(const JSThread *thread, const JSHandle<JSHClass> &jshclass);

    static void RefreshUsers(const JSThread *thread, const JSHandle<JSHClass> &old_hclass,
                             const JSHandle<JSHClass> &new_hclass);

    inline void ClearBitField()
    {
        SetBitField(0UL);
        SetBitField1(0UL);
    }

    inline JSType GetObjectType() const
    {
        uint32_t bits = GetBitField();
        return ObjectTypeBits::Decode(bits);
    }

    inline void SetObjectType(JSType type)
    {
        uint32_t bits = GetBitField();
        uint32_t new_val = ObjectTypeBits::Update(bits, type);
        SetBitField(new_val);
    }

    inline void SetConstructor(bool flag) const
    {
        ConstructorBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }

    inline void SetExtensible(bool flag) const
    {
        ExtensibleBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }

    inline void SetIsPrototype(bool flag) const
    {
        IsPrototypeBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }

    inline void SetIsLiteral(bool flag) const
    {
        IsLiteralBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }

    inline void SetClassConstructor(bool flag) const
    {
        ClassConstructorBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }

    inline void SetClassPrototype(bool flag) const
    {
        ClassPrototypeBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }

    inline void SetIsDictionaryMode(bool flag) const
    {
        IsDictionaryBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }
    inline void SetWeakContainer(bool flag)
    {
        WeakContainerBit::Set<uint32_t>(flag, GetBitFieldAddr());
    }

    inline bool IsJSObject() const
    {
        JSType js_type = GetObjectType();
        return (JSType::JS_OBJECT_BEGIN <= js_type && js_type <= JSType::JS_OBJECT_END);
    }

    inline bool IsECMAObject() const
    {
        JSType js_type = GetObjectType();
        return (JSType::ECMA_OBJECT_BEGIN <= js_type && js_type <= JSType::ECMA_OBJECT_END);
    }

    inline bool IsRealm() const
    {
        return GetObjectType() == JSType::JS_REALM;
    }

    inline bool IsHClass() const
    {
        return GetObjectType() == JSType::HCLASS;
    }

    inline bool IsString() const
    {
        return GetObjectType() == JSType::STRING;
    }

    inline bool IsSymbol() const
    {
        return GetObjectType() == JSType::SYMBOL;
    }

    inline bool IsStringOrSymbol() const
    {
        JSType js_type = GetObjectType();
        return (js_type == JSType::STRING) || (js_type == JSType::SYMBOL);
    }

    inline bool IsTaggedArray() const
    {
        JSType js_type = GetObjectType();
        return (JSType::TAGGED_ARRAY_BEGIN <= js_type && js_type <= JSType::TAGGED_ARRAY_END);
    }

    inline bool IsDictionary() const
    {
        return GetObjectType() == JSType::TAGGED_DICTIONARY;
    }

    inline bool IsJSNativePointer() const
    {
        return GetObjectType() == JSType::JS_NATIVE_POINTER;
    }

    inline bool IsJSSymbol() const
    {
        return GetObjectType() == JSType::SYMBOL;
    }

    inline bool IsJSArray() const
    {
        return GetObjectType() == JSType::JS_ARRAY;
    }

    inline bool IsBigInt() const
    {
        return GetObjectType() == JSType::BIGINT;
    }

    inline bool IsTypedArray() const
    {
        JSType js_type = GetObjectType();
        return (JSType::JS_TYPED_ARRAY_BEGIN < js_type && js_type <= JSType::JS_TYPED_ARRAY_END);
    }

    inline bool IsJSTypedArray() const
    {
        return GetObjectType() == JSType::JS_TYPED_ARRAY;
    }

    inline bool IsJSInt8Array() const
    {
        return GetObjectType() == JSType::JS_INT8_ARRAY;
    }

    inline bool IsJSUint8Array() const
    {
        return GetObjectType() == JSType::JS_UINT8_ARRAY;
    }

    inline bool IsJSUint8ClampedArray() const
    {
        return GetObjectType() == JSType::JS_UINT8_CLAMPED_ARRAY;
    }

    inline bool IsJSInt16Array() const
    {
        return GetObjectType() == JSType::JS_INT16_ARRAY;
    }

    inline bool IsJSUint16Array() const
    {
        return GetObjectType() == JSType::JS_UINT16_ARRAY;
    }

    inline bool IsJSInt32Array() const
    {
        return GetObjectType() == JSType::JS_INT32_ARRAY;
    }

    inline bool IsJSUint32Array() const
    {
        return GetObjectType() == JSType::JS_UINT32_ARRAY;
    }

    inline bool IsJSFloat32Array() const
    {
        return GetObjectType() == JSType::JS_FLOAT32_ARRAY;
    }

    inline bool IsJSFloat64Array() const
    {
        return GetObjectType() == JSType::JS_FLOAT64_ARRAY;
    }

    inline bool IsJSBigInt64Array() const
    {
        return GetObjectType() == JSType::JS_BIGINT64_ARRAY;
    }

    inline bool IsJSBigUint64Array() const
    {
        return GetObjectType() == JSType::JS_BIGUINT64_ARRAY;
    }

    inline bool IsJsGlobalEnv() const
    {
        return GetObjectType() == JSType::GLOBAL_ENV;
    }

    inline bool IsJSFunctionBase() const
    {
        JSType js_type = GetObjectType();
        return js_type >= JSType::JS_FUNCTION_BASE && js_type <= JSType::JS_BOUND_FUNCTION;
    }

    inline bool IsJsBoundFunction() const
    {
        return GetObjectType() == JSType::JS_BOUND_FUNCTION;
    }

    inline bool IsJSIntlBoundFunction() const
    {
        return GetObjectType() == JSType::JS_INTL_BOUND_FUNCTION;
    }

    inline bool IsJSProxyRevocFunction() const
    {
        return GetObjectType() == JSType::JS_PROXY_REVOC_FUNCTION;
    }

    inline bool IsJSAsyncFunction() const
    {
        return GetObjectType() == JSType::JS_ASYNC_FUNCTION;
    }

    inline bool IsJSAsyncGeneratorFunction() const
    {
        return GetObjectType() == JSType::JS_ASYNC_GENERATOR_FUNCTION;
    }

    inline bool IsJSAsyncAwaitStatusFunction() const
    {
        return GetObjectType() == JSType::JS_ASYNC_AWAIT_STATUS_FUNCTION;
    }

    inline bool IsJSAsyncGeneratorResolveNextFunction() const
    {
        return GetObjectType() == JSType::JS_ASYNC_GENERATOR_RESOLVE_NEXT_FUNCTION;
    }

    inline bool IsJSAsyncFromSyncIteratorValueUnwrapFunction() const
    {
        return GetObjectType() == JSType::JS_ASYNC_FROM_SYNC_ITERATOR_VALUE_UNWRAP_FUNCTION;
    }

    inline bool IsJSPromiseReactionFunction() const
    {
        return GetObjectType() == JSType::JS_PROMISE_REACTIONS_FUNCTION;
    }

    inline bool IsJSPromiseExecutorFunction() const
    {
        return GetObjectType() == JSType::JS_PROMISE_EXECUTOR_FUNCTION;
    }

    inline bool IsJSPromiseAllResolveElementFunction() const
    {
        return GetObjectType() == JSType::JS_PROMISE_ALL_RESOLVE_ELEMENT_FUNCTION;
    }

    inline bool IsJSFunctionExtraInfo() const
    {
        return GetObjectType() == JSType::FUNCTION_EXTRA_INFO;
    }

    inline bool IsMicroJobQueue() const
    {
        return GetObjectType() == JSType::MICRO_JOB_QUEUE;
    }

    inline bool IsPendingJob() const
    {
        return GetObjectType() == JSType::PENDING_JOB;
    }

    inline bool IsJsPrimitiveRef() const
    {
        return GetObjectType() == JSType::JS_PRIMITIVE_REF;
    };

    bool IsJSSet() const
    {
        return GetObjectType() == JSType::JS_SET;
    }

    bool IsJSMap() const
    {
        return GetObjectType() == JSType::JS_MAP;
    }

    bool IsJSWeakRef() const
    {
        return GetObjectType() == JSType::JS_WEAK_REF;
    }

    bool IsJSWeakMap() const
    {
        return GetObjectType() == JSType::JS_WEAK_MAP;
    }

    bool IsJSWeakSet() const
    {
        return GetObjectType() == JSType::JS_WEAK_SET;
    }

    bool IsJSFunction() const
    {
        return GetObjectType() >= JSType::JS_FUNCTION_BEGIN && GetObjectType() <= JSType::JS_FUNCTION_END;
    }

    inline bool IsJSError() const
    {
        JSType js_type = GetObjectType();
        return js_type >= JSType::JS_ERROR_BEGIN && js_type <= JSType::JS_ERROR_END;
    }

    inline bool IsArguments() const
    {
        return GetObjectType() == JSType::JS_ARGUMENTS;
    }

    inline bool IsDate() const
    {
        return GetObjectType() == JSType::JS_DATE;
    }

    inline bool IsJSRegExp() const
    {
        return GetObjectType() == JSType::JS_REG_EXP;
    }

    inline bool IsJSProxy() const
    {
        return GetObjectType() == JSType::JS_PROXY;
    }

    inline bool IsJSLocale() const
    {
        return GetObjectType() == JSType::JS_LOCALE;
    }

    inline bool IsJSIntl() const
    {
        return GetObjectType() == JSType::JS_INTL;
    }

    inline bool IsJSDateTimeFormat() const
    {
        return GetObjectType() == JSType::JS_DATE_TIME_FORMAT;
    }

    inline bool IsJSRelativeTimeFormat() const
    {
        return GetObjectType() == JSType::JS_RELATIVE_TIME_FORMAT;
    }

    inline bool IsJSNumberFormat() const
    {
        return GetObjectType() == JSType::JS_NUMBER_FORMAT;
    }

    inline bool IsJSCollator() const
    {
        return GetObjectType() == JSType::JS_COLLATOR;
    }

    inline bool IsJSPluralRules() const
    {
        return GetObjectType() == JSType::JS_PLURAL_RULES;
    }

    inline bool IsJSArrayList() const
    {
        return GetObjectType() == JSType::JS_ARRAY_LIST;
    }

    inline bool IsJSQueue() const
    {
        return GetObjectType() == JSType::JS_QUEUE;
    }

    inline bool IsSpecialContainer() const
    {
        return GetObjectType() >= JSType::JS_ARRAY_LIST && GetObjectType() <= JSType::JS_QUEUE;
    }

    inline bool IsAccessorData() const
    {
        return GetObjectType() == JSType::ACCESSOR_DATA;
    }

    inline bool IsInternalAccessor() const
    {
        return GetObjectType() == JSType::INTERNAL_ACCESSOR;
    }

    inline bool IsIterator() const
    {
        JSType js_type = GetObjectType();
        return js_type >= JSType::JS_ITERATOR_BEGIN && js_type <= JSType::JS_ITERATOR_END;
    }

    inline bool IsForinIterator() const
    {
        return GetObjectType() == JSType::JS_FORIN_ITERATOR;
    }

    inline bool IsStringIterator() const
    {
        return GetObjectType() == JSType::JS_STRING_ITERATOR;
    }

    inline bool IsArrayBuffer() const
    {
        return GetObjectType() == JSType::JS_ARRAY_BUFFER;
    }

    inline bool IsDataView() const
    {
        return GetObjectType() == JSType::JS_DATA_VIEW;
    }

    inline bool IsJSSetIterator() const
    {
        return GetObjectType() == JSType::JS_SET_ITERATOR;
    }

    inline bool IsJSRegExpIterator() const
    {
        return GetObjectType() == JSType::JS_REG_EXP_ITERATOR;
    }

    inline bool IsJSMapIterator() const
    {
        return GetObjectType() == JSType::JS_MAP_ITERATOR;
    }

    inline bool IsJSArrayIterator() const
    {
        return GetObjectType() == JSType::JS_ARRAY_ITERATOR;
    }
    inline bool IsPrototypeHandler() const
    {
        return GetObjectType() == JSType::PROTOTYPE_HANDLER;
    }

    inline bool IsTransitionHandler() const
    {
        return GetObjectType() == JSType::TRANSITION_HANDLER;
    }

    inline bool IsPropertyBox() const
    {
        return GetObjectType() == JSType::PROPERTY_BOX;
    }
    inline bool IsProtoChangeMarker() const
    {
        return GetObjectType() == JSType::PROTO_CHANGE_MARKER;
    }

    inline bool IsProtoChangeDetails() const
    {
        return GetObjectType() == JSType::PROTOTYPE_INFO;
    }

    inline bool IsProgram() const
    {
        return GetObjectType() == JSType::PROGRAM;
    }

    inline bool IsEcmaModule() const
    {
        return GetObjectType() == JSType::ECMA_MODULE;
    }

    inline bool IsClassInfoExtractor() const
    {
        return GetObjectType() == JSType::CLASS_INFO_EXTRACTOR;
    }

    inline bool IsJSFinalizationRegistry() const
    {
        return GetObjectType() == JSType::FINALIZATION_REGISTRY;
    }

    inline bool IsLexicalFunction() const
    {
        return GetObjectType() == JSType::LEXICAL_FUNCTION;
    }

    inline bool IsCallable() const
    {
        return hclass_.IsCallable();
    }

    inline bool IsConstructor() const
    {
        uint32_t bits = GetBitField();
        return ConstructorBit::Decode(bits);
    }

    inline bool IsExtensible() const
    {
        uint32_t bits = GetBitField();
        return ExtensibleBit::Decode(bits);
    }

    inline bool IsPrototype() const
    {
        uint32_t bits = GetBitField();
        return IsPrototypeBit::Decode(bits);
    }

    inline bool IsLiteral() const
    {
        uint32_t bits = GetBitField();
        return IsLiteralBit::Decode(bits);
    }

    inline bool IsClassConstructor() const
    {
        uint32_t bits = GetBitField();
        return ClassConstructorBit::Decode(bits);
    }

    inline bool IsJSGlobalObject() const
    {
        return GetObjectType() == JSType::JS_GLOBAL_OBJECT;
    }

    inline bool IsClassPrototype() const
    {
        uint32_t bits = GetBitField();
        return ClassPrototypeBit::Decode(bits);
    }

    inline bool IsWeakContainer() const
    {
        uint64_t bits = GetBitField();
        return WeakContainerBit::Decode(bits);
    }

    inline bool IsDictionaryMode() const
    {
        uint32_t bits = GetBitField();
        return IsDictionaryBit::Decode(bits);
    }

    inline bool IsObjectWrapper() const
    {
        return GetObjectType() == JSType::OBJECT_WRAPPER;
    }

    inline bool IsGeneratorFunction() const
    {
        return GetObjectType() == JSType::JS_GENERATOR_FUNCTION;
    }

    inline bool IsGeneratorObject() const
    {
        JSType type = GetObjectType();
        return type == JSType::JS_GENERATOR_OBJECT || type == JSType::JS_ASYNC_FUNC_OBJECT ||
               type == JSType::JS_ASYNC_GENERATOR_OBJECT;
    }

    inline bool IsGeneratorContext() const
    {
        return GetObjectType() == JSType::JS_GENERATOR_CONTEXT;
    }

    inline bool IsAsyncFuncObject() const
    {
        JSType type = GetObjectType();
        return type == JSType::JS_ASYNC_FUNC_OBJECT || type == JSType::JS_ASYNC_GENERATOR_OBJECT;
    }

    inline bool IsAsyncGeneratorFunction() const
    {
        return GetObjectType() == JSType::JS_ASYNC_GENERATOR_FUNCTION;
    }

    inline bool IsAsyncGeneratorObject() const
    {
        return GetObjectType() == JSType::JS_ASYNC_GENERATOR_OBJECT;
    }

    inline bool IsJSAsyncFromSyncIteratorObject() const
    {
        return GetObjectType() == JSType::JS_ASYNC_FROM_SYNC_ITERATOR_OBJECT;
    }

    inline bool IsJSPromise() const
    {
        return GetObjectType() == JSType::JS_PROMISE;
    }

    inline bool IsResolvingFunctionsRecord() const
    {
        return GetObjectType() == JSType::RESOLVING_FUNCTIONS_RECORD;
    }

    inline bool IsPromiseRecord() const
    {
        return GetObjectType() == JSType::PROMISE_RECORD;
    }

    inline bool IsPromiseIteratorRecord() const
    {
        return GetObjectType() == JSType::PROMISE_ITERATOR_RECORD;
    }

    inline bool IsPromiseCapability() const
    {
        return GetObjectType() == JSType::PROMISE_CAPABILITY;
    }

    inline bool IsPromiseReaction() const
    {
        return GetObjectType() == JSType::PROMISE_REACTIONS;
    }

    inline bool IsCompletionRecord() const
    {
        return GetObjectType() == JSType::COMPLETION_RECORD;
    }

    inline bool IsRecord() const
    {
        JSType js_type = GetObjectType();
        return js_type >= JSType::JS_RECORD_BEGIN && js_type <= JSType::JS_RECORD_END;
    }

    inline bool IsTemplateMap() const
    {
        return GetObjectType() == JSType::TEMPLATE_MAP;
    }

    inline bool IsFreeObjectWithOneField() const
    {
        return GetObjectType() == JSType::FREE_OBJECT_WITH_ONE_FIELD;
    }

    inline bool IsFreeObjectWithNoneField() const
    {
        return GetObjectType() == JSType::FREE_OBJECT_WITH_NONE_FIELD;
    }

    inline bool IsFreeObjectWithTwoField() const
    {
        return GetObjectType() == JSType::FREE_OBJECT_WITH_TWO_FIELD;
    }

    inline void SetElementRepresentation(Representation representation)
    {
        uint32_t bits = GetBitField();
        uint32_t new_val = ElementRepresentationBits::Update(bits, representation);
        SetBitField(new_val);
    }

    inline Representation GetElementRepresentation() const
    {
        uint32_t bits = GetBitField();
        return ElementRepresentationBits::Decode(bits);
    }

    inline void UpdateRepresentation(JSTaggedValue value)
    {
        Representation rep = PropertyAttributes::UpdateRepresentation(GetElementRepresentation(), value);
        SetElementRepresentation(rep);
    }

    inline void SetIsDictionaryElement(bool value)
    {
        JSTaggedType new_val = DictionaryElementBits::Update(GetBitField(), value);
        SetBitField(new_val);
    }
    inline bool IsDictionaryElement() const
    {
        return DictionaryElementBits::Decode(GetBitField());
    }
    inline void SetIsStableElements(bool value)
    {
        JSTaggedType new_val = IsStableElementsBit::Update(GetBitField(), value);
        SetBitField(new_val);
    }
    inline bool IsStableElements() const
    {
        return IsStableElementsBit::Decode(GetBitField());
    }
    inline bool IsStableJSArguments() const
    {
        uint32_t bits = GetBitField();
        auto type = ObjectTypeBits::Decode(bits);
        return IsStableElementsBit::Decode(bits) && (type == JSType::JS_ARGUMENTS);
    }
    inline bool IsStableJSArray() const
    {
        uint32_t bits = GetBitField();
        auto type = ObjectTypeBits::Decode(bits);
        return IsStableElementsBit::Decode(bits) && (type == JSType::JS_ARRAY);
    }
    inline void SetHasConstructor(bool value)
    {
        TaggedType new_val = HasConstructorBits::Update(GetBitField(), value);
        SetBitField(new_val);
    }
    inline bool HasConstructor() const
    {
        return HasConstructorBits::Decode(GetBitField());
    }

    inline void SetNumberOfProps(uint32_t num)
    {
        uint32_t bits = GetBitField1();
        uint32_t new_val = NumberOfPropsBits::Update(bits, num);
        SetBitField1(new_val);
    }

    inline void IncNumberOfProps()
    {
        ASSERT(NumberOfProps() < PropertyAttributes::MAX_CAPACITY_OF_PROPERTIES);
        SetNumberOfProps(NumberOfProps() + 1);
    }

    inline uint32_t NumberOfProps() const
    {
        uint32_t bits = GetBitField1();
        return NumberOfPropsBits::Decode(bits);
    }

    inline int32_t GetNextInlinedPropsIndex() const
    {
        uint32_t inlined_properties = GetInlinedProperties();
        uint32_t number_of_props = NumberOfProps();
        if (number_of_props < inlined_properties) {
            return number_of_props;
        }
        return -1;
    }

    inline int32_t GetNextNonInlinedPropsIndex() const
    {
        uint32_t inlined_properties = GetInlinedProperties();
        uint32_t number_of_props = NumberOfProps();
        if (number_of_props >= inlined_properties) {
            return number_of_props - inlined_properties;
        }
        return -1;
    }

    inline uint32_t GetInlinedPropertiesOffset(uint32_t index) const
    {
        ASSERT(index < GetInlinedProperties());
        return GetInlinedPropertiesIndex(index) * JSTaggedValue::TaggedTypeSize();
    }

    inline uint32_t GetInlinedPropertiesIndex(uint32_t index) const
    {
        ASSERT(index < GetInlinedProperties());
        uint32_t bits = GetBitField1();
        return InlinedPropsStartBits::Decode(bits) + index;
    }

    inline void SetInlinedPropsStart(uint32_t num)
    {
        uint32_t bits = GetBitField1();
        uint32_t new_val = InlinedPropsStartBits::Update(bits, num / JSTaggedValue::TaggedTypeSize());
        SetBitField1(new_val);
    }

    inline uint32_t GetInlinedPropsStartSize() const
    {
        uint32_t bits = GetBitField1();
        return InlinedPropsStartBits::Decode(bits) * JSTaggedValue::TaggedTypeSize();
    }

    inline uint32_t GetInlinedProperties() const
    {
        JSType type = GetObjectType();
        if (JSType::JS_OBJECT_BEGIN <= type && type <= JSType::JS_OBJECT_END) {
            uint32_t bits = GetBitField1();
            return static_cast<uint32_t>(GetObjectSize() / JSTaggedValue::TaggedTypeSize() -
                                         InlinedPropsStartBits::Decode(bits));
        }
        return 0;
    }

    JSTaggedValue GetAccessor(const JSTaggedValue &key);

    HClass *GetHClass()
    {
        return &hclass_;
    }

    const HClass *GetHClass() const
    {
        return &hclass_;
    }

    static constexpr size_t GetHClassOffset()
    {
        return MEMBER_OFFSET(JSHClass, hclass_);
    }

    static JSHClass *FromHClass(HClass *hclass)
    {
        return reinterpret_cast<JSHClass *>(reinterpret_cast<uintptr_t>(hclass) - MEMBER_OFFSET(JSHClass, hclass_));
    }

    static constexpr size_t BIT_FIELD_OFFSET = TaggedObjectSize() + ::panda::HClass::GetDataOffset();
    static constexpr size_t BIT_FIELD1_OFFSET = BIT_FIELD_OFFSET + sizeof(uint32_t);
    static constexpr size_t OBJECT_SIZE_OFFSET = TaggedObjectSize() + BaseClass::GetObjectSizeOffset();
    ACCESSORS_FIXED_SIZE_RAW_FIELD(BitField, uint32_t, uint32_t, BIT_FIELD_OFFSET);
    ACCESSORS_FIXED_SIZE_RAW_FIELD(BitField1, uint32_t, uint32_t, BIT_FIELD1_OFFSET);
    ACCESSORS_FIXED_SIZE_RAW_FIELD(ObjectSize, uint32_t, uint32_t, OBJECT_SIZE_OFFSET);

    ACCESSORS_START(TaggedObjectSize() + sizeof(HClass))
    ACCESSORS(0, Proto)
    ACCESSORS(1, Layout)
    ACCESSORS(2, Transitions)
    ACCESSORS(3, Parent)
    ACCESSORS(4, ProtoChangeMarker)
    ACCESSORS(5, ProtoChangeDetails)
    ACCESSORS(6, EnumCache)
    ACCESSORS_FINISH(7)

    void SetPrototype(const JSThread *thread, JSTaggedValue proto);
    void SetPrototype(const JSThread *thread, const JSHandle<JSTaggedValue> &proto);
    inline JSTaggedValue GetPrototype() const
    {
        return GetProto();
    }
    DECL_DUMP()

    static PandaString DumpJSType(JSType type);

private:
    static inline void AddTransitions(const JSThread *thread, const JSHandle<JSHClass> &parent,
                                      const JSHandle<JSHClass> &child, const JSHandle<JSTaggedValue> &key,
                                      PropertyAttributes attr);
    static inline void AddExtensionTransitions(const JSThread *thread, const JSHandle<JSHClass> &parent,
                                               const JSHandle<JSHClass> &child, const JSHandle<JSTaggedValue> &key);
    static inline void AddProtoTransitions(const JSThread *thread, const JSHandle<JSHClass> &parent,
                                           const JSHandle<JSHClass> &child, const JSHandle<JSTaggedValue> &key,
                                           const JSHandle<JSTaggedValue> &proto);
    inline JSHClass *FindTransitions(const JSTaggedValue &key, const JSTaggedValue &attributes);
    inline JSHClass *FindProtoTransitions(const JSTaggedValue &key, const JSTaggedValue &proto);

    void Copy(const JSThread *thread, const JSHClass *jshclass);

    uint32_t *GetBitFieldAddr() const
    {
        return reinterpret_cast<uint32_t *>(ToUintPtr(this) + BIT_FIELD_OFFSET);
    }

    HClass hclass_;
};
static_assert(JSHClass::BIT_FIELD_OFFSET % GetAlignmentInBytes(TAGGED_OBJECT_ALIGNMENT) == 0);
static_assert(JSHClass::BIT_FIELD_OFFSET == JSHClass::GetHClassOffset() + HClass::GetDataOffset());
static_assert(JSHClass::OBJECT_SIZE_OFFSET == JSHClass::GetHClassOffset() + BaseClass::GetObjectSizeOffset());
static_assert(JSHClass::GetHClassOffset() == coretypes::DynClass::GetHClassOffset());
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_HCLASS_H
