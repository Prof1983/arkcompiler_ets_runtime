/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_GENERATOR_OBJECT_H
#define ECMASCRIPT_JS_GENERATOR_OBJECT_H

#include "plugins/ecmascript/runtime/js_function.h"

namespace panda::ecmascript {
enum class JSGeneratorState {
    UNDEFINED = 0,
    SUSPENDED_START,
    SUSPENDED_YIELD,
    EXECUTING,
    COMPLETED,
    AWAITING_RETURN,
};

class GeneratorContext : TaggedObject {
public:
    CAST_CHECK(GeneratorContext, IsGeneratorContext);

    ACCESSORS_START(TaggedObjectSize())
    ACCESSORS(0, RegsArray)
    ACCESSORS(1, Method)
    ACCESSORS(2, Acc)
    ACCESSORS(3, NRegs)
    ACCESSORS(4, BCOffset)
    ACCESSORS(5, GeneratorObject)
    ACCESSORS(6, LexicalEnv)
    ACCESSORS_FINISH(7)

    DECL_DUMP()
};

class JSGeneratorObject : public JSObject {
public:
    CAST_CHECK(JSGeneratorObject, IsGeneratorObject);

    // 26.4.3.2 GeneratorValidate(generator)
    static JSTaggedValue GeneratorValidate(JSThread *thread, const JSHandle<JSTaggedValue> &obj);

    // 26.4.3.3 GeneratorResume(generator, value)
    static JSHandle<JSTaggedValue> GeneratorResume(JSThread *thread, const JSHandle<JSGeneratorObject> &generator,
                                                   const JSHandle<JSTaggedValue> &value);

    // 26.4.3.4 GeneratorResumeAbrupt(generator, abruptCompletion)
    static JSHandle<JSTaggedValue> GeneratorResumeAbrupt(JSThread *thread, const JSHandle<JSGeneratorObject> &generator,
                                                         const JSHandle<CompletionRecord> &abrupt_completion);

    inline bool IsSuspendYield() const
    {
        return GetGeneratorState() == JSTaggedValue(static_cast<int32_t>(JSGeneratorState::SUSPENDED_YIELD));
    }

    static inline bool IsState(JSTaggedValue state_value, JSGeneratorState state)
    {
        return state_value == JSTaggedValue(static_cast<int32_t>(state));
    }

    inline bool IsExecuting() const
    {
        return GetGeneratorState() == JSTaggedValue(static_cast<int32_t>(JSGeneratorState::EXECUTING));
    }

    inline bool IsSuspendedStart() const
    {
        return GetGeneratorState() == JSTaggedValue(static_cast<int32_t>(JSGeneratorState::SUSPENDED_START));
    }

    inline void SetState(JSThread *thread, JSGeneratorState state)
    {
        SetGeneratorState(thread, JSTaggedValue(static_cast<int32_t>(state)));
    }

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, GeneratorState)
    ACCESSORS(1, GeneratorContext)
    ACCESSORS(2, ResumeResult)
    ACCESSORS(3, ResumeMode)
    ACCESSORS_FINISH(4)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_GENERATOR_OBJECT_H
