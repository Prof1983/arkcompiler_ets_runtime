/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_number_format.h"

namespace panda::ecmascript {
constexpr uint32_t DEFAULT_FRACTION_DIGITS = 2;
constexpr uint32_t PERUNIT_STRING = 5;

JSHandle<JSTaggedValue> OptionToEcmaString(JSThread *thread, StyleOption style)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (style) {
        case StyleOption::DECIMAL:
            result.Update(global_const->GetHandledDecimalString().GetTaggedValue());
            break;
        case StyleOption::CURRENCY:
            result.Update(global_const->GetHandledCurrencyString().GetTaggedValue());
            break;
        case StyleOption::PERCENT:
            result.Update(global_const->GetHandledPercentString().GetTaggedValue());
            break;
        case StyleOption::UNIT:
            result.Update(global_const->GetHandledUnitString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionToEcmaString(JSThread *thread, CurrencyDisplayOption currency_display)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (currency_display) {
        case CurrencyDisplayOption::CODE:
            result.Update(global_const->GetHandledCodeString().GetTaggedValue());
            break;
        case CurrencyDisplayOption::SYMBOL:
            result.Update(global_const->GetHandledSymbolString().GetTaggedValue());
            break;
        case CurrencyDisplayOption::NARROWSYMBOL:
            result.Update(global_const->GetHandledNarrowSymbolString().GetTaggedValue());
            break;
        case CurrencyDisplayOption::NAME:
            result.Update(global_const->GetHandledNameString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionToEcmaString(JSThread *thread, CurrencySignOption currency_sign)
{
    auto global_const = thread->GlobalConstants();
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    switch (currency_sign) {
        case CurrencySignOption::STANDARD:
            result.Update(global_const->GetHandledStandardString().GetTaggedValue());
            break;
        case CurrencySignOption::ACCOUNTING:
            result.Update(global_const->GetHandledAccountingString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionToEcmaString(JSThread *thread, UnitDisplayOption unit_display)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (unit_display) {
        case UnitDisplayOption::SHORT:
            result.Update(global_const->GetHandledShortString().GetTaggedValue());
            break;
        case UnitDisplayOption::NARROW:
            result.Update(global_const->GetHandledNarrowString().GetTaggedValue());
            break;
        case UnitDisplayOption::LONG:
            result.Update(global_const->GetHandledLongString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionToEcmaString(JSThread *thread, NotationOption notation)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (notation) {
        case NotationOption::STANDARD:
            result.Update(global_const->GetHandledStandardString().GetTaggedValue());
            break;
        case NotationOption::SCIENTIFIC:
            result.Update(global_const->GetHandledScientificString().GetTaggedValue());
            break;
        case NotationOption::ENGINEERING:
            result.Update(global_const->GetHandledEngineeringString().GetTaggedValue());
            break;
        case NotationOption::COMPACT:
            result.Update(global_const->GetHandledCompactString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionToEcmaString(JSThread *thread, CompactDisplayOption compact_display)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (compact_display) {
        case CompactDisplayOption::SHORT:
            result.Update(global_const->GetHandledShortString().GetTaggedValue());
            break;
        case CompactDisplayOption::LONG:
            result.Update(global_const->GetHandledLongString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> OptionToEcmaString(JSThread *thread, SignDisplayOption sign_display)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (sign_display) {
        case SignDisplayOption::AUTO:
            result.Update(global_const->GetHandledAutoString().GetTaggedValue());
            break;
        case SignDisplayOption::ALWAYS:
            result.Update(global_const->GetHandledAlwaysString().GetTaggedValue());
            break;
        case SignDisplayOption::NEVER:
            result.Update(global_const->GetHandledNeverString().GetTaggedValue());
            break;
        case SignDisplayOption::EXCEPTZERO:
            result.Update(global_const->GetHandledExceptZeroString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

icu::MeasureUnit ToMeasureUnit(const std::string &sanctioned_unit)
{
    UErrorCode status = U_ZERO_ERROR;
    // Get All ICU measure unit
    int32_t total = icu::MeasureUnit::getAvailable(nullptr, 0, status);
    status = U_ZERO_ERROR;
    std::vector<icu::MeasureUnit> units(total);
    icu::MeasureUnit::getAvailable(units.data(), total, status);
    ASSERT(U_SUCCESS(status));

    // Find measure unit according to sanctioned unit
    //  then return measure unit
    for (auto &unit : units) {
        if (std::strcmp(sanctioned_unit.c_str(), unit.getSubtype()) == 0) {
            return unit;
        }
    }
    return icu::MeasureUnit();
}

// ecma402 #sec-issanctionedsimpleunitidentifier
bool IsSanctionedSimpleUnitIdentifier(const std::string &unit)
{
    // 1. If unitIdentifier is listed in sanctioned unit set, return true.
    // 2. Else, Return false.
    auto it = SANCTIONED_UNIT.find(unit);
    return it != SANCTIONED_UNIT.end();
}

// 6.5.1 IsWellFormedUnitIdentifier ( unitIdentifier )
bool IsWellFormedUnitIdentifier(const std::string &unit, icu::MeasureUnit &icu_unit, icu::MeasureUnit &icu_per_unit)
{
    // 1. If the result of IsSanctionedSimpleUnitIdentifier(unitIdentifier) is true, then
    //      a. Return true.
    icu::MeasureUnit result = icu::MeasureUnit();
    icu::MeasureUnit empty_unit = icu::MeasureUnit();
    auto pos = unit.find("-per-");
    if (IsSanctionedSimpleUnitIdentifier(unit) && pos == std::string::npos) {
        result = ToMeasureUnit(unit);
        icu_unit = result;
        icu_per_unit = empty_unit;
        return true;
    }

    // 2. If the substring "-per-" does not occur exactly once in unitIdentifier,
    //      a. then false
    size_t after_pos = pos + PERUNIT_STRING;
    if (pos == std::string::npos || unit.find("-per-", after_pos) != std::string::npos) {
        return false;
    }

    // 3. Let numerator be the substring of unitIdentifier from the beginning to just before "-per-".
    std::string numerator = unit.substr(0, pos);
    // 4. If the result of IsSanctionedUnitIdentifier(numerator) is false, then
    //      a. return false
    if (IsSanctionedSimpleUnitIdentifier(numerator)) {
        result = ToMeasureUnit(numerator);
    } else {
        return false;
    }

    // 5. Let denominator be the substring of unitIdentifier from just after "-per-" to the end.
    std::string denominator = unit.substr(pos + PERUNIT_STRING);

    // 6. If the result of IsSanctionedUnitIdentifier(denominator) is false, then
    //      a. Return false
    icu::MeasureUnit per_result = icu::MeasureUnit();
    if (IsSanctionedSimpleUnitIdentifier(denominator)) {
        per_result = ToMeasureUnit(denominator);
    } else {
        return false;
    }

    // 7. Return true.
    icu_unit = result;
    icu_per_unit = per_result;
    return true;
}

// 12.1.13 SetNumberFormatUnitOptions ( intlObj, options )
FractionDigitsOption SetNumberFormatUnitOptions(JSThread *thread, const JSHandle<JSNumberFormat> &number_format,
                                                const JSHandle<JSObject> &options_object,
                                                icu::number::LocalizedNumberFormatter *icu_number_formatter)
{
    auto global_const = thread->GlobalConstants();
    FractionDigitsOption fraction_digits_option;
    // 3. Let style be ? GetOption(options, "style", "string", « "decimal", "percent", "currency", "unit" », "decimal").
    JSHandle<JSTaggedValue> property = global_const->GetHandledStyleString();
    auto style = JSLocale::GetOptionOfString<StyleOption>(
        thread, options_object, property,
        {StyleOption::DECIMAL, StyleOption::PERCENT, StyleOption::CURRENCY, StyleOption::UNIT},
        {"decimal", "percent", "currency", "unit"}, StyleOption::DECIMAL);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fraction_digits_option);

    // 4. Set intlObj.[[Style]] to style.
    JSHandle<JSTaggedValue> style_value(thread, JSTaggedValue(static_cast<int>(style)));
    number_format->SetStyle(thread, style_value);

    // 5. Let currency be ? GetOption(options, "currency", "string", undefined, undefined).
    property = global_const->GetHandledCurrencyString();
    JSHandle<JSTaggedValue> undefined_value(thread, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> currency =
        JSLocale::GetOption(thread, options_object, property, OptionType::STRING, undefined_value, undefined_value);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fraction_digits_option);

    // 6. If currency is not undefined, then
    //    a. If the result of IsWellFormedCurrencyCode(currency) is false, throw a RangeError exception.
    if (!currency->IsUndefined()) {
        JSHandle<EcmaString> currency_str = JSHandle<EcmaString>::Cast(currency);
        if (currency_str->IsUtf16()) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "not a utf-8", fraction_digits_option);
        }
        std::string currency_c_str = JSLocale::ConvertToStdString(currency_str);
        if (!JSLocale::IsWellFormedCurrencyCode(currency_c_str)) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "not a wellformed code", fraction_digits_option);
        }
    } else {
        // 7. If style is "currency" and currency is undefined, throw a TypeError exception.
        if (style == StyleOption::CURRENCY) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "style is currency but currency is undefined", fraction_digits_option);
        }
    }

    // 8. Let currencyDisplay be ? GetOption(options, "currencyDisplay", "string",
    //  « "code", "symbol", "narrowSymbol", "name" », "symbol").
    property = global_const->GetHandledCurrencyDisplayString();
    auto currency_display = JSLocale::GetOptionOfString<CurrencyDisplayOption>(
        thread, options_object, property,
        {CurrencyDisplayOption::CODE, CurrencyDisplayOption::SYMBOL, CurrencyDisplayOption::NARROWSYMBOL,
         CurrencyDisplayOption::NAME},
        {"code", "symbol", "narrowSymbol", "name"}, CurrencyDisplayOption::SYMBOL);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fraction_digits_option);
    JSHandle<JSTaggedValue> currency_display_value(thread, JSTaggedValue(static_cast<int>(currency_display)));
    number_format->SetCurrencyDisplay(thread, currency_display_value);

    // 9. Let currencySign be ? GetOption(options, "currencySign", "string", « "standard", "accounting" », "standard").
    property = global_const->GetHandledCurrencySignString();
    auto currency_sign = JSLocale::GetOptionOfString<CurrencySignOption>(
        thread, options_object, property, {CurrencySignOption::STANDARD, CurrencySignOption::ACCOUNTING},
        {"standard", "accounting"}, CurrencySignOption::STANDARD);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fraction_digits_option);
    JSHandle<JSTaggedValue> currency_sign_value(thread, JSTaggedValue(static_cast<int>(currency_sign)));
    number_format->SetCurrencySign(thread, currency_sign_value);

    // 10. Let unit be ? GetOption(options, "unit", "string", undefined, undefined).
    property = global_const->GetHandledUnitString();
    JSHandle<JSTaggedValue> unit =
        JSLocale::GetOption(thread, options_object, property, OptionType::STRING, undefined_value, undefined_value);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fraction_digits_option);
    number_format->SetUnit(thread, unit);

    // 11. If unit is not undefined, then
    // If the result of IsWellFormedUnitIdentifier(unit) is false, throw a RangeError exception.
    icu::MeasureUnit icu_unit;
    icu::MeasureUnit icu_per_unit;
    if (!unit->IsUndefined()) {
        JSHandle<EcmaString> unit_str = JSHandle<EcmaString>::Cast(unit);
        if (unit_str->IsUtf16()) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "Unit input is illegal", fraction_digits_option);
        }
        std::string str = JSLocale::ConvertToStdString(unit_str);
        if (!IsWellFormedUnitIdentifier(str, icu_unit, icu_per_unit)) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "Unit input is illegal", fraction_digits_option);
        }
    } else {
        // 15.12. if style is "unit" and unit is undefined, throw a TypeError exception.
        if (style == StyleOption::UNIT) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "style is unit but unit is undefined", fraction_digits_option);
        }
    }

    // 13. Let unitDisplay be ? GetOption(options, "unitDisplay", "string", « "short", "narrow", "long" », "short").
    property = global_const->GetHandledUnitDisplayString();
    auto unit_display = JSLocale::GetOptionOfString<UnitDisplayOption>(
        thread, options_object, property,
        {UnitDisplayOption::SHORT, UnitDisplayOption::NARROW, UnitDisplayOption::LONG}, {"short", "narrow", "long"},
        UnitDisplayOption::SHORT);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, fraction_digits_option);
    JSHandle<JSTaggedValue> unit_display_value(thread, JSTaggedValue(static_cast<int>(unit_display)));
    number_format->SetUnitDisplay(thread, unit_display_value);

    // 14. If style is "currency", then
    //      a. Let currency be the result of converting currency to upper case as specified in 6.1.
    //      b. Set intlObj.[[Currency]] to currency.
    //      c. Set intlObj.[[CurrencyDisplay]] to currencyDisplay.
    //      d. Set intlObj.[[CurrencySign]] to currencySign.
    icu::UnicodeString currency_u_str;
    UErrorCode status = U_ZERO_ERROR;
    if (style == StyleOption::CURRENCY) {
        JSHandle<EcmaString> currency_str = JSHandle<EcmaString>::Cast(currency);
        std::string currency_c_str = JSLocale::ConvertToStdString(currency_str);
        std::transform(currency_c_str.begin(), currency_c_str.end(), currency_c_str.begin(), toupper);
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        JSHandle<JSTaggedValue> currency_value =
            JSHandle<JSTaggedValue>::Cast(factory->NewFromStdString(currency_c_str));
        number_format->SetCurrency(thread, currency_value);
        currency_u_str = currency_c_str.c_str();
        if (!currency_u_str.isEmpty()) {  // NOLINT(readability-implicit-bool-conversion)
            *icu_number_formatter = icu_number_formatter->unit(icu::CurrencyUnit(currency_u_str.getBuffer(), status));
            ASSERT(U_SUCCESS(status));
            UNumberUnitWidth u_number_unit_width;
            // Trans currencyDisplayOption to ICU format number display option
            switch (currency_display) {
                case CurrencyDisplayOption::CODE:
                    u_number_unit_width = UNumberUnitWidth::UNUM_UNIT_WIDTH_ISO_CODE;
                    break;
                case CurrencyDisplayOption::SYMBOL:
                    u_number_unit_width = UNumberUnitWidth::UNUM_UNIT_WIDTH_SHORT;
                    break;
                case CurrencyDisplayOption::NARROWSYMBOL:
                    u_number_unit_width = UNumberUnitWidth::UNUM_UNIT_WIDTH_NARROW;
                    break;
                case CurrencyDisplayOption::NAME:
                    u_number_unit_width = UNumberUnitWidth::UNUM_UNIT_WIDTH_FULL_NAME;
                    break;
                default:
                    UNREACHABLE();
            }
            *icu_number_formatter = icu_number_formatter->unitWidth(u_number_unit_width);
        }
    }

    // 15. If style is "unit", then
    // if unit is not undefiend set unit to LocalizedNumberFormatter
    //    then if perunit is not undefiend set perunit to LocalizedNumberFormatter
    if (style == StyleOption::UNIT) {
        icu::MeasureUnit empty_unit = icu::MeasureUnit();
        if (icu_unit != empty_unit) {  // NOLINT(readability-implicit-bool-conversion)
            *icu_number_formatter = icu_number_formatter->unit(icu_unit);
        }
        if (icu_per_unit != empty_unit) {  // NOLINT(readability-implicit-bool-conversion)
            *icu_number_formatter = icu_number_formatter->perUnit(icu_per_unit);
        }
    }

    // 17. If style is "currency", then
    //      a. Let cDigits be CurrencyDigits(currency).
    //      b. Let mnfdDefault be cDigits.
    //      c. Let mxfdDefault be cDigits.
    if (style == StyleOption::CURRENCY) {
        int32_t c_digits = JSNumberFormat::CurrencyDigits(currency_u_str);
        fraction_digits_option.mnfd_default = c_digits;
        fraction_digits_option.mxfd_default = c_digits;
    } else {
        // 18. Else,
        //      a. Let mnfdDefault be 0.
        //      b. If style is "percent", then
        //          i. Let mxfdDefault be 0.
        //      c. else,
        //          i. Let mxfdDefault be 3.
        fraction_digits_option.mnfd_default = 0;
        if (style == StyleOption::PERCENT) {
            fraction_digits_option.mxfd_default = 0;
        } else {
            fraction_digits_option.mxfd_default = 3;  // Max decimal precision is 3
        }
    }
    return fraction_digits_option;
}

// 12.1.2 InitializeNumberFormat ( numberFormat, locales, options )
// NOLINTNEXTLINE(readability-function-size)
void JSNumberFormat::InitializeNumberFormat(JSThread *thread, const JSHandle<JSNumberFormat> &number_format,
                                            const JSHandle<JSTaggedValue> &locales,
                                            const JSHandle<JSTaggedValue> &options)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 1. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);

    // 2. If options is undefined, then
    //      a. Let options be ObjectCreate(null).
    // 3. Else,
    //      a. Let options be ? ToObject(options).
    JSHandle<JSObject> options_object;
    if (options->IsUndefined()) {
        options_object = factory->OrdinaryNewJSObjectCreate(JSHandle<JSTaggedValue>(thread, JSTaggedValue::Null()));
    } else {
        options_object = JSTaggedValue::ToObject(thread, options);
        RETURN_IF_ABRUPT_COMPLETION(thread);
    }

    auto global_const = thread->GlobalConstants();
    // 5. Let matcher be ? GetOption(options, "localeMatcher", "string", « "lookup", "best fit" », "best fit").
    JSHandle<JSTaggedValue> property = global_const->GetHandledLocaleMatcherString();
    auto matcher = JSLocale::GetOptionOfString<LocaleMatcherOption>(
        thread, options_object, property, {LocaleMatcherOption::LOOKUP, LocaleMatcherOption::BEST_FIT},
        {"lookup", "best fit"}, LocaleMatcherOption::BEST_FIT);
    RETURN_IF_ABRUPT_COMPLETION(thread);

    // 7. Let numberingSystem be ? GetOption(options, "numberingSystem", "string", undefined, undefined).
    property = global_const->GetHandledNumberingSystemString();
    JSHandle<JSTaggedValue> undefined_value(thread, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> numbering_system_tagged_value =
        JSLocale::GetOption(thread, options_object, property, OptionType::STRING, undefined_value, undefined_value);
    RETURN_IF_ABRUPT_COMPLETION(thread);
    number_format->SetNumberingSystem(thread, numbering_system_tagged_value);

    // 8. If numberingSystem is not undefined, then
    //      a. If numberingSystem does not match the Unicode Locale Identifier type nonterminal,
    //      throw a RangeError exception. `(3*8alphanum) *("-" (3*8alphanum))`
    std::string numbering_system_str;
    if (!numbering_system_tagged_value->IsUndefined()) {
        JSHandle<EcmaString> numbering_system_ecma_string = JSHandle<EcmaString>::Cast(numbering_system_tagged_value);
        if (numbering_system_ecma_string->IsUtf16()) {
            THROW_RANGE_ERROR(thread, "invalid numberingSystem");
        }
        numbering_system_str = JSLocale::ConvertToStdString(numbering_system_ecma_string);
        if (!JSLocale::IsNormativeNumberingSystem(numbering_system_str)) {
            THROW_RANGE_ERROR(thread, "invalid numberingSystem");
        }
    }

    // 10. Let localeData be %NumberFormat%.[[LocaleData]].
    JSHandle<TaggedArray> available_locales;
    if (requested_locales->GetLength() == 0) {
        available_locales = factory->EmptyArray();
    } else {
        available_locales = GetAvailableLocales(thread);
    }

    // 11. Let r be ResolveLocale( %NumberFormat%.[[AvailableLocales]], requestedLocales, opt,
    // %NumberFormat%.[[RelevantExtensionKeys]], localeData).
    std::set<std::string> relevant_extension_keys {"nu"};
    ResolvedLocale r =
        JSLocale::ResolveLocale(thread, available_locales, requested_locales, matcher, relevant_extension_keys);

    // 12. Set numberFormat.[[Locale]] to r.[[locale]].
    icu::Locale icu_locale = r.locale_data;
    JSHandle<EcmaString> locale_str = JSLocale::ToLanguageTag(thread, icu_locale);
    number_format->SetLocale(thread, locale_str.GetTaggedValue());

    // Set numberingSystemStr to UnicodeKeyWord "nu"
    UErrorCode status = U_ZERO_ERROR;
    if (!numbering_system_str.empty()) {
        if (JSLocale::IsWellNumberingSystem(numbering_system_str)) {
            icu_locale.setUnicodeKeywordValue("nu", numbering_system_str, status);
            ASSERT(U_SUCCESS(status));
        }
    }

    icu::number::LocalizedNumberFormatter icu_number_formatter =
        icu::number::NumberFormatter::withLocale(icu_locale).roundingMode(UNUM_ROUND_HALFUP);

    // Get default numbering system associated with the specified locale
    std::unique_ptr<icu::NumberingSystem> numbering_system_ptr(
        icu::NumberingSystem::createInstance(icu_locale, status));

    int32_t mnfd_default = 0;
    int32_t mxfd_default = 0;
    FractionDigitsOption fraction_options =
        SetNumberFormatUnitOptions(thread, number_format, options_object, &icu_number_formatter);
    RETURN_IF_ABRUPT_COMPLETION(thread);
    mnfd_default = fraction_options.mnfd_default;
    mxfd_default = fraction_options.mxfd_default;
    JSTaggedValue unit_display_value = number_format->GetUnitDisplay();
    auto unit_display = static_cast<UnitDisplayOption>(unit_display_value.GetInt());

    // Trans unitDisplay option to ICU display option
    UNumberUnitWidth u_number_unit_width;
    switch (unit_display) {
        case UnitDisplayOption::SHORT:
            u_number_unit_width = UNumberUnitWidth::UNUM_UNIT_WIDTH_SHORT;
            break;
        case UnitDisplayOption::NARROW:
            u_number_unit_width = UNumberUnitWidth::UNUM_UNIT_WIDTH_NARROW;
            break;
        case UnitDisplayOption::LONG:
            // UNUM_UNIT_WIDTH_FULL_NAME Print the full name of the unit, without any abbreviations.
            u_number_unit_width = UNumberUnitWidth::UNUM_UNIT_WIDTH_FULL_NAME;
            break;
        default:
            UNREACHABLE();
    }
    icu_number_formatter = icu_number_formatter.unitWidth(u_number_unit_width);

    // 16. Let style be numberFormat.[[Style]].
    JSTaggedValue style_value = number_format->GetStyle();
    auto style = static_cast<StyleOption>(style_value.GetInt());
    if (style == StyleOption::PERCENT) {
        icu_number_formatter = icu_number_formatter.unit(icu::MeasureUnit::getPercent())
                                   .scale(icu::number::Scale::powerOfTen(2));  // means 10^2
    }

    // 19. Let notation be ? GetOption(
    //  options, "notation", "string", « "standard", "scientific", "engineering", "compact" », "standard").
    property = global_const->GetHandledNotationString();
    auto notation = JSLocale::GetOptionOfString<NotationOption>(
        thread, options_object, property,
        {NotationOption::STANDARD, NotationOption::SCIENTIFIC, NotationOption::ENGINEERING, NotationOption::COMPACT},
        {"standard", "scientific", "engineering", "compact"}, NotationOption::STANDARD);
    RETURN_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> notation_value(thread, JSTaggedValue(static_cast<int>(notation)));
    number_format->SetNotation(thread, notation_value);

    // 21. Perform ? SetNumberFormatDigitOptions(numberFormat, options, mnfdDefault, mxfdDefault, notation).
    JSLocale::SetNumberFormatDigitOptions(thread, number_format, JSHandle<JSTaggedValue>::Cast(options_object),
                                          mnfd_default, mxfd_default, notation);
    icu_number_formatter = SetICUFormatterDigitOptions(icu_number_formatter, number_format);

    // 22. Let compactDisplay be ? GetOptionOfString(options, "compactDisplay", "string", « "short", "long" », "short").
    property = global_const->GetHandledCompactDisplayString();
    auto compact_display = JSLocale::GetOptionOfString<CompactDisplayOption>(
        thread, options_object, property, {CompactDisplayOption::SHORT, CompactDisplayOption::LONG}, {"short", "long"},
        CompactDisplayOption::SHORT);
    JSHandle<JSTaggedValue> compact_display_value(thread, JSTaggedValue(static_cast<int>(compact_display)));
    number_format->SetCompactDisplay(thread, compact_display_value);

    // Trans NotationOption to ICU Noation and set to icuNumberFormatter
    if (notation == NotationOption::COMPACT) {
        switch (compact_display) {
            case CompactDisplayOption::SHORT:
                icu_number_formatter = icu_number_formatter.notation(icu::number::Notation::compactShort());
                break;
            case CompactDisplayOption::LONG:
                icu_number_formatter = icu_number_formatter.notation(icu::number::Notation::compactLong());
                break;
            default:
                break;
        }
    }
    switch (notation) {
        case NotationOption::STANDARD:
            icu_number_formatter = icu_number_formatter.notation(icu::number::Notation::simple());
            break;
        case NotationOption::SCIENTIFIC:
            icu_number_formatter = icu_number_formatter.notation(icu::number::Notation::scientific());
            break;
        case NotationOption::ENGINEERING:
            icu_number_formatter = icu_number_formatter.notation(icu::number::Notation::engineering());
            break;
        default:
            break;
    }

    // 24. Let useGrouping be ? GetOption(options, "useGrouping", "boolean", undefined, true).
    property = global_const->GetHandledUserGroupingString();
    bool use_grouping = false;
    [[maybe_unused]] bool find = JSLocale::GetOptionOfBool(thread, options_object, property, true, &use_grouping);
    RETURN_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> use_grouping_value(thread, JSTaggedValue(use_grouping));
    number_format->SetUseGrouping(thread, use_grouping_value);

    // 25. Set numberFormat.[[UseGrouping]] to useGrouping.
    if (!use_grouping) {
        icu_number_formatter = icu_number_formatter.grouping(UNumberGroupingStrategy::UNUM_GROUPING_OFF);
    }

    // 26. Let signDisplay be ?
    //  GetOption(options, "signDisplay", "string", « "auto", "never", "always", "exceptZero" », "auto").
    property = global_const->GetHandledSignDisplayString();
    auto sign_display = JSLocale::GetOptionOfString<SignDisplayOption>(
        thread, options_object, property,
        {SignDisplayOption::AUTO, SignDisplayOption::NEVER, SignDisplayOption::ALWAYS, SignDisplayOption::EXCEPTZERO},
        {"auto", "never", "always", "exceptZero"}, SignDisplayOption::AUTO);
    RETURN_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> sign_display_value(thread, JSTaggedValue(static_cast<int>(sign_display)));
    number_format->SetSignDisplay(thread, sign_display_value);

    // 27. Set numberFormat.[[SignDisplay]] to signDisplay.
    // The default sign in ICU is UNUM_SIGN_AUTO which is mapped from
    // SignDisplay::AUTO and CurrencySign::STANDARD so we can skip setting
    // under that values for optimization.
    JSTaggedValue currency_sign_value = number_format->GetCurrencySign();
    auto currency_sign = static_cast<CurrencySignOption>(currency_sign_value.GetInt());

    // Trans SignDisPlayOption to ICU UNumberSignDisplay and set to icuNumberFormatter
    switch (sign_display) {
        case SignDisplayOption::AUTO:
            // if CurrencySign is ACCOUNTING, Use the locale-dependent accounting format on negative numbers
            if (currency_sign == CurrencySignOption::ACCOUNTING) {
                icu_number_formatter = icu_number_formatter.sign(UNumberSignDisplay::UNUM_SIGN_ACCOUNTING);
            } else {
                icu_number_formatter = icu_number_formatter.sign(UNumberSignDisplay::UNUM_SIGN_AUTO);
            }
            break;
        case SignDisplayOption::NEVER:
            icu_number_formatter = icu_number_formatter.sign(UNumberSignDisplay::UNUM_SIGN_NEVER);
            break;
        case SignDisplayOption::ALWAYS:
            // if CurrencySign is ACCOUNTING, Use the locale-dependent accounting format on negative numbers
            if (currency_sign == CurrencySignOption::ACCOUNTING) {
                icu_number_formatter = icu_number_formatter.sign(UNumberSignDisplay::UNUM_SIGN_ACCOUNTING_ALWAYS);
            } else {
                icu_number_formatter = icu_number_formatter.sign(UNumberSignDisplay::UNUM_SIGN_ALWAYS);
            }
            break;
        case SignDisplayOption::EXCEPTZERO:
            // if CurrencySign is ACCOUNTING, Use the locale-dependent accounting format on negative numbers
            if (currency_sign == CurrencySignOption::ACCOUNTING) {
                icu_number_formatter = icu_number_formatter.sign(UNumberSignDisplay::UNUM_SIGN_ACCOUNTING_EXCEPT_ZERO);
            } else {
                icu_number_formatter = icu_number_formatter.sign(UNumberSignDisplay::UNUM_SIGN_EXCEPT_ZERO);
            }
            break;
        default:
            break;
    }

    // Set numberFormat.[[IcuNumberForma]] to handleNumberFormatter
    factory->NewJSIntlIcuData(number_format, icu_number_formatter, JSNumberFormat::FreeIcuNumberformat);
    // Set numberFormat.[[BoundFormat]] to undefinedValue
    number_format->SetBoundFormat(thread, undefined_value);
}

// 12.1.3 CurrencyDigits ( currency )
int32_t JSNumberFormat::CurrencyDigits(const icu::UnicodeString &currency)
{
    UErrorCode status = U_ZERO_ERROR;
    // If the ISO 4217 currency and funds code list contains currency as an alphabetic code,
    // return the minor unit value corresponding to the currency from the list; otherwise, return 2.
    int32_t fraction_digits =
        ucurr_getDefaultFractionDigits(reinterpret_cast<const UChar *>(currency.getBuffer()), &status);
    if (U_SUCCESS(status) != 0) {
        return fraction_digits;
    }
    return DEFAULT_FRACTION_DIGITS;
}

// 12.1.8 FormatNumeric( numberFormat, x )
JSHandle<JSTaggedValue> JSNumberFormat::FormatNumeric(JSThread *thread, const JSHandle<JSNumberFormat> &number_format,
                                                      JSTaggedValue x)
{
    icu::number::LocalizedNumberFormatter *icu_number_format = number_format->GetIcuCallTarget();
    ASSERT(icu_number_format != nullptr);

    UErrorCode status = U_ZERO_ERROR;
    icu::number::FormattedNumber formatted_number;
    if (x.IsBigInt()) {
        [[maybe_unused]] EcmaHandleScope scope(thread);

        JSHandle<BigInt> bigint(thread, x);
        JSHandle<EcmaString> bigint_str = BigInt::ToString(thread, bigint);
        std::string std_string = bigint_str->GetCString().get();
        formatted_number = icu_number_format->formatDecimal(icu::StringPiece(std_string), status);
    } else {
        double number = x.GetNumber();
        formatted_number = icu_number_format->formatDouble(number, status);
    }
    if (U_FAILURE(status) != 0) {
        JSHandle<JSTaggedValue> error_result(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "icu formatter format failed", error_result);
    }
    icu::UnicodeString result = formatted_number.toString(status);
    if (U_FAILURE(status) != 0) {
        JSHandle<JSTaggedValue> error_result(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "formatted number toString failed", error_result);
    }
    JSHandle<EcmaString> string_value = JSLocale::IcuToString(thread, result);
    return JSHandle<JSTaggedValue>::Cast(string_value);
}

void GroupToParts(JSThread *thread, const icu::number::FormattedNumber &formatted, const JSHandle<JSArray> &receiver,
                  const JSHandle<JSNumberFormat> &number_format, JSTaggedValue x)
{
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString formatted_text = formatted.toString(status);
    if (U_FAILURE(status) != 0) {  // NOLINT(readability-implicit-bool-conversion)
        THROW_TYPE_ERROR(thread, "formattedNumber toString failed");
    }
    ASSERT(x.IsNumber());

    StyleOption style_option = static_cast<StyleOption>(number_format->GetStyle().GetInt());

    icu::ConstrainedFieldPosition cfpo;
    // Set constrainCategory to UFIELD_CATEGORY_NUMBER which is specified for UNumberFormatFields
    cfpo.constrainCategory(UFIELD_CATEGORY_NUMBER);
    auto global_const = thread->GlobalConstants();
    JSMutableHandle<JSTaggedValue> type_string(thread, JSTaggedValue::Undefined());
    int index = 0;
    int previous_limit = 0;
    /**
     * From ICU header file document @unumberformatter.h
     * Sets a constraint on the field category.
     *
     * When this instance of ConstrainedFieldPosition is passed to FormattedValue#nextPosition,
     * positions are skipped unless they have the given category.
     *
     * Any previously set constraints are cleared.
     *
     * For example, to loop over only the number-related fields:
     *
     *     ConstrainedFieldPosition cfpo;
     *     cfpo.constrainCategory(UFIELDCATEGORY_NUMBER_FORMAT);
     *     while (fmtval.nextPosition(cfpo, status)) {
     *         // handle the number-related field position
     *     }
     */
    bool last_field_group = false;
    int group_leap_length = 0;
    while (formatted.nextPosition(cfpo, status) != 0) {
        int32_t field_id = cfpo.getField();
        int32_t start = cfpo.getStart();
        int32_t limit = cfpo.getLimit();
        type_string.Update(global_const->GetLiteralString());
        // If start greater than previousLimit, means a literal type exists before number fields
        // so add a literal type with value of formattedText.sub(0, start)
        // Special case when fieldId is UNUM_GROUPING_SEPARATOR_FIELD
        if (static_cast<UNumberFormatFields>(field_id) == UNUM_GROUPING_SEPARATOR_FIELD) {
            JSHandle<EcmaString> substring = JSLocale::IcuToString(thread, formatted_text, previous_limit, start);
            type_string.Update(global_const->GetIntegerString());
            JSLocale::PutElement(thread, index, receiver, type_string, JSHandle<JSTaggedValue>::Cast(substring));
            RETURN_IF_ABRUPT_COMPLETION(thread);
            index++;
            {
                // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue
                // (issue #I5QOJX)
                // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
                type_string.Update(JSLocale::GetNumberFieldType(thread, x, field_id).GetTaggedValue());
                substring = JSLocale::IcuToString(thread, formatted_text, start, limit);
                JSLocale::PutElement(thread, index, receiver, type_string, JSHandle<JSTaggedValue>::Cast(substring));
                RETURN_IF_ABRUPT_COMPLETION(thread);
                index++;
            }
            last_field_group = true;
            group_leap_length = start - previous_limit + 1;
            previous_limit = limit;
            continue;
        }
        if (start > previous_limit) {
            JSHandle<EcmaString> substring = JSLocale::IcuToString(thread, formatted_text, previous_limit, start);
            JSLocale::PutElement(thread, index, receiver, type_string, JSHandle<JSTaggedValue>::Cast(substring));
            RETURN_IF_ABRUPT_COMPLETION(thread);
            index++;
        }
        if (last_field_group) {
            start = start + group_leap_length;
            last_field_group = false;
        }
        // Special case in ICU when style is unit and unit is percent
        if (style_option == StyleOption::UNIT && static_cast<UNumberFormatFields>(field_id) == UNUM_PERCENT_FIELD) {
            type_string.Update(global_const->GetUnitString());
        } else {
            // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue (issue #I5QOJX)
            // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
            type_string.Update(JSLocale::GetNumberFieldType(thread, x, field_id).GetTaggedValue());
        }
        JSHandle<EcmaString> substring = JSLocale::IcuToString(thread, formatted_text, start, limit);
        JSLocale::PutElement(thread, index, receiver, type_string, JSHandle<JSTaggedValue>::Cast(substring));
        RETURN_IF_ABRUPT_COMPLETION(thread);
        index++;
        previous_limit = limit;
    }
    // If iterated length is smaller than formattedText.length, means a literal type exists after number fields
    // so add a literal type with value of formattedText.sub(previousLimit, formattedText.length)
    if (formatted_text.length() > previous_limit) {
        type_string.Update(global_const->GetLiteralString());
        JSHandle<EcmaString> substring =
            JSLocale::IcuToString(thread, formatted_text, previous_limit, formatted_text.length());
        JSLocale::PutElement(thread, index, receiver, type_string, JSHandle<JSTaggedValue>::Cast(substring));
    }
}

// 12.1.9 FormatNumericToParts( numberFormat, x )
JSHandle<JSArray> JSNumberFormat::FormatNumericToParts(JSThread *thread, const JSHandle<JSNumberFormat> &number_format,
                                                       JSTaggedValue x)
{
    ASSERT(x.IsNumber());
    icu::number::LocalizedNumberFormatter *icu_number_formatter = number_format->GetIcuCallTarget();
    ASSERT(icu_number_formatter != nullptr);

    UErrorCode status = U_ZERO_ERROR;
    double number = x.GetNumber();
    icu::number::FormattedNumber formatted_number = icu_number_formatter->formatDouble(number, status);
    if (U_FAILURE(status) != 0) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        JSHandle<JSArray> empty_array = factory->NewJSArray();
        THROW_RANGE_ERROR_AND_RETURN(thread, "icu formatter format failed", empty_array);
    }

    JSHandle<JSTaggedValue> arr = JSArray::ArrayCreate(thread, JSTaggedNumber(0));
    JSHandle<JSArray> result = JSHandle<JSArray>::Cast(arr);
    // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue (issue #I5QOJX)
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    GroupToParts(thread, formatted_number, result, number_format, x);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSArray, thread);
    return result;
}

// 12.1.12 UnwrapNumberFormat( nf )
JSHandle<JSTaggedValue> JSNumberFormat::UnwrapNumberFormat(JSThread *thread, const JSHandle<JSTaggedValue> &nf)
{
    // 1. Assert: Type(nf) is Object.
    ASSERT(nf->IsJSObject());

    // 2. If nf does not have an [[InitializedNumberFormat]] internal slot and ?
    //  InstanceofOperator(nf, %NumberFormat%) is true, then Let nf be ? Get(nf, %Intl%.[[FallbackSymbol]]).
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    bool has_istance = JSObject::InstanceOf(thread, nf, env->GetNumberFormatFunction());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSTaggedValue>(thread, JSTaggedValue::Undefined()));

    bool is_js_number_format = nf->IsJSNumberFormat();
    // If nf does not have an [[InitializedNumberFormat]] internal slot and ?
    // InstanceofOperator(nf, %NumberFormat%) is true, then
    //      a. Let nf be ? Get(nf, %Intl%.[[FallbackSymbol]]).
    if (!is_js_number_format && has_istance) {
        JSHandle<JSTaggedValue> key(thread, JSHandle<JSIntl>::Cast(env->GetIntlFunction())->GetFallbackSymbol());
        OperationResult operation_result = JSTaggedValue::GetProperty(thread, nf, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSTaggedValue>(thread, JSTaggedValue::Undefined()));
        return operation_result.GetValue();
    }
    // 3. Perform ? RequireInternalSlot(nf, [[InitializedNumberFormat]]).
    if (!is_js_number_format) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this is not object",
                                    JSHandle<JSTaggedValue>(thread, JSTaggedValue::Exception()));
    }
    return nf;
}

JSHandle<TaggedArray> JSNumberFormat::GetAvailableLocales(JSThread *thread)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> number_format_locales = env->GetNumberFormatLocales();
    if (!number_format_locales->IsUndefined()) {
        return JSHandle<TaggedArray>::Cast(number_format_locales);
    }
    const char *key = "NumberElements";
    const char *path = nullptr;
    JSHandle<TaggedArray> available_locales = JSLocale::GetAvailableLocales(thread, key, path);
    env->SetNumberFormatLocales(thread, available_locales);
    return available_locales;
}

void JSNumberFormat::ResolvedOptions(JSThread *thread, const JSHandle<JSNumberFormat> &number_format,
                                     const JSHandle<JSObject> &options)
{
    // Table 5: Resolved Options of NumberFormat Instances
    // Internal Slot                    Property
    //    [[Locale]]                      "locale"
    //    [[NumberingSystem]]             "numberingSystem"
    //    [[Style]]                       "style"
    //    [[Currency]]                    "currency"
    //    [[CurrencyDisplay]]             "currencyDisplay"
    //    [[CurrencySign]]                "currencySign"
    //    [[Unit]]                        "unit"
    //    [[UnitDisplay]]                 "unitDisplay"
    //    [[MinimumIntegerDigits]]        "minimumIntegerDigits"
    //    [[MinimumFractionDigits]]       "minimumFractionDigits"
    //    [[MaximumFractionDigits]]       "maximumFractionDigits"
    //    [[MinimumSignificantDigits]]    "minimumSignificantDigits"
    //    [[MaximumSignificantDigits]]    "maximumSignificantDigits"
    //    [[UseGrouping]]                 "useGrouping"
    //    [[Notation]]                    "notation"
    //    [[CompactDisplay]]              "compactDisplay"
    //    [SignDisplay]]                  "signDisplay"
    // [[Locale]]
    auto global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> property = global_const->GetHandledLocaleString();
    JSHandle<JSTaggedValue> locale(thread, number_format->GetLocale());
    JSObject::CreateDataPropertyOrThrow(thread, options, property, locale);

    // [[NumberingSystem]]
    JSHandle<JSTaggedValue> numbering_system(thread, number_format->GetNumberingSystem());
    if (numbering_system->IsUndefined()) {
        numbering_system = global_const->GetHandledLatnString();
    }
    property = global_const->GetHandledNumberingSystemString();
    JSObject::CreateDataPropertyOrThrow(thread, options, property, numbering_system);

    // [[Style]]
    StyleOption style = static_cast<StyleOption>(number_format->GetStyle().GetInt());
    property = global_const->GetHandledStyleString();
    JSHandle<JSTaggedValue> style_string = OptionToEcmaString(thread, style);
    JSObject::CreateDataPropertyOrThrow(thread, options, property, style_string);

    // [[currency]]
    JSHandle<JSTaggedValue> currency(thread, JSTaggedValue::Undefined());
    // If style is not currency the currency should be undefined
    if (style == StyleOption::CURRENCY) {
        currency = JSHandle<JSTaggedValue>(thread, number_format->GetCurrency());
    }
    if (!currency->IsUndefined()) {  // NOLINT(readability-implicit-bool-conversion)
        property = global_const->GetHandledCurrencyString();
        JSObject::CreateDataPropertyOrThrow(thread, options, property, currency);

        // [[CurrencyDisplay]]
        property = global_const->GetHandledCurrencyDisplayString();
        CurrencyDisplayOption currency_display =
            static_cast<CurrencyDisplayOption>(number_format->GetCurrencyDisplay().GetInt());
        JSHandle<JSTaggedValue> currency_display_string = OptionToEcmaString(thread, currency_display);
        JSObject::CreateDataPropertyOrThrow(thread, options, property, currency_display_string);

        // [[CurrencySign]]
        property = global_const->GetHandledCurrencySignString();
        CurrencySignOption currency_sign = static_cast<CurrencySignOption>(number_format->GetCurrencySign().GetInt());
        JSHandle<JSTaggedValue> currency_sign_string = OptionToEcmaString(thread, currency_sign);
        JSObject::CreateDataPropertyOrThrow(thread, options, property, currency_sign_string);
    }

    if (style == StyleOption::UNIT) {
        JSHandle<JSTaggedValue> unit(thread, number_format->GetUnit());
        if (!unit->IsUndefined()) {
            // [[Unit]]
            property = global_const->GetHandledUnitString();
            JSObject::CreateDataPropertyOrThrow(thread, options, property, unit);
        }
        // [[UnitDisplay]]
        property = global_const->GetHandledUnitDisplayString();
        UnitDisplayOption unit_display = static_cast<UnitDisplayOption>(number_format->GetUnitDisplay().GetInt());
        JSHandle<JSTaggedValue> unit_display_string = OptionToEcmaString(thread, unit_display);
        JSObject::CreateDataPropertyOrThrow(thread, options, property, unit_display_string);
    }
    // [[MinimumIntegerDigits]]
    property = global_const->GetHandledMinimumIntegerDigitsString();
    JSHandle<JSTaggedValue> minimum_integer_digits(thread, number_format->GetMinimumIntegerDigits());
    JSObject::CreateDataPropertyOrThrow(thread, options, property, minimum_integer_digits);

    RoundingType rounding_type = static_cast<RoundingType>(number_format->GetRoundingType().GetInt());
    if (rounding_type == RoundingType::SIGNIFICANTDIGITS) {
        // [[MinimumSignificantDigits]]
        property = global_const->GetHandledMinimumSignificantDigitsString();
        JSHandle<JSTaggedValue> minimum_significant_digits(thread, number_format->GetMinimumSignificantDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, minimum_significant_digits);
        // [[MaximumSignificantDigits]]
        property = global_const->GetHandledMaximumSignificantDigitsString();
        JSHandle<JSTaggedValue> maximum_significant_digits(thread, number_format->GetMaximumSignificantDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, maximum_significant_digits);
    } else {
        // [[MinimumFractionDigits]]
        property = global_const->GetHandledMinimumFractionDigitsString();
        JSHandle<JSTaggedValue> minimum_fraction_digits(thread, number_format->GetMinimumFractionDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, minimum_fraction_digits);
        // [[MaximumFractionDigits]]
        property = global_const->GetHandledMaximumFractionDigitsString();
        JSHandle<JSTaggedValue> maximum_fraction_digits(thread, number_format->GetMaximumFractionDigits());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, maximum_fraction_digits);
    }

    // [[UseGrouping]]
    property = global_const->GetHandledUserGroupingString();
    JSObject::CreateDataPropertyOrThrow(thread, options, property,
                                        JSHandle<JSTaggedValue>(thread, number_format->GetUseGrouping()));

    // [[Notation]]
    property = global_const->GetHandledNotationString();
    NotationOption notation = static_cast<NotationOption>(number_format->GetNotation().GetInt());
    JSHandle<JSTaggedValue> notation_string = OptionToEcmaString(thread, notation);
    JSObject::CreateDataPropertyOrThrow(thread, options, property, notation_string);

    // Only output compactDisplay when notation is compact.
    if (notation == NotationOption::COMPACT) {
        // [[CompactDisplay]]
        property = global_const->GetHandledCompactDisplayString();
        CompactDisplayOption compact_display =
            static_cast<CompactDisplayOption>(number_format->GetCompactDisplay().GetInt());
        JSHandle<JSTaggedValue> compact_display_string = OptionToEcmaString(thread, compact_display);
        JSObject::CreateDataPropertyOrThrow(thread, options, property, compact_display_string);
    }

    // [[SignDisplay]]
    property = global_const->GetHandledSignDisplayString();
    SignDisplayOption sign_display = static_cast<SignDisplayOption>(number_format->GetSignDisplay().GetInt());
    JSHandle<JSTaggedValue> sign_display_string = OptionToEcmaString(thread, sign_display);
    JSObject::CreateDataPropertyOrThrow(thread, options, property, sign_display_string);
}
}  // namespace panda::ecmascript
