/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_ECMA_COMPILER_H
#define ECMASCRIPT_ECMA_COMPILER_H

#include "runtime/compiler.h"

namespace panda::ecmascript {

class GlobalHandleCollection;
class JSThread;

class EcmaCompiler : public Compiler {
public:
    explicit EcmaCompiler(CodeAllocator *code_allocator, mem::InternalAllocatorPtr internal_allocator,
                          const RuntimeOptions &options, mem::MemStatsType *mem_stats,
                          compiler::RuntimeInterface *runtime_iface)
        : Compiler(code_allocator, internal_allocator, options, mem_stats, runtime_iface)
    {
    }

    void AddTask(CompilerTask &&task, TaggedValue func) override;
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_ECMA_COMPILER_H