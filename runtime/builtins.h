/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_BUILTINS_H
#define ECMASCRIPT_BUILTINS_H

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "object_factory.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"

#include "plugins/ecmascript/runtime/js_map_iterator.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_for_in_iterator.h"
#include "plugins/ecmascript/runtime/js_regexp_iterator.h"
#include "plugins/ecmascript/runtime/js_set_iterator.h"
#include "plugins/ecmascript/runtime/js_string_iterator.h"

namespace panda::ecmascript::builtins {
using ErrorType = base::ErrorType;

struct ErrorParameter {
    EcmaEntrypoint native_constructor {nullptr};
    EcmaEntrypoint native_method {nullptr};
    const char *native_property_name {nullptr};
    JSType native_jstype {JSType::INVALID};
};

enum FunctionLength : uint8_t { ZERO = 0, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN };

class Builtins {
public:
    explicit Builtins() = default;
    ~Builtins() = default;
    NO_COPY_SEMANTIC(Builtins);
    NO_MOVE_SEMANTIC(Builtins);

    void Initialize(const JSHandle<GlobalEnv> &env, JSThread *thread);

private:
    JSThread *thread_ {nullptr};
    ObjectFactory *factory_ {nullptr};
    EcmaVM *vm_ {nullptr};

    JSHandle<JSFunction> NewBuiltinConstructor(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &prototype,
                                               EcmaEntrypoint ctor_func, const char *name, int length) const;

    JSHandle<JSFunction> NewFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &key,
                                     EcmaEntrypoint func, int length) const;

    void InitializeCtor(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &prototype,
                        const JSHandle<JSFunction> &ctor, const char *name, int length) const;

    void InitializeGlobalObject(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &global_object);

    void InitializeFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &empty_func_dynclass) const;

    void InitializeObject(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj_func_prototype,
                          const JSHandle<JSObject> &obj_func);

    void InitializeNumber(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &prim_ref_obj_dynclass);

    void InitializeBigInt(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeDate(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeBoolean(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &prim_ref_obj_dynclass) const;

    void InitializeSymbol(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeSymbolWithRealm(const JSHandle<GlobalEnv> &realm,
                                   const JSHandle<JSHClass> &obj_func_instance_dynclass);

    void InitializeArray(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &obj_func_prototype_val) const;

    void InitializeTypedArray(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeInt8Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeUint8Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeUint8ClampedArray(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeInt16Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeUint16Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeInt32Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeUint32Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeFloat32Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeFloat64Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeBigInt64Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeBigUint64Array(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeAllTypeError(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeAllTypeErrorWithRealm(const JSHandle<GlobalEnv> &realm) const;

    void InitializeError(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass,
                         const JSType &error_tag) const;

    void SetErrorWithRealm(const JSHandle<GlobalEnv> &realm, const JSType &error_tag) const;

    void InitializeRegExp(const JSHandle<GlobalEnv> &env);

    // for Intl.
    JSHandle<JSFunction> NewIntlConstructor(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &prototype,
                                            EcmaEntrypoint ctor_func, const char *name, int length);
    void InitializeIntlCtor(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &prototype,
                            const JSHandle<JSFunction> &ctor, const char *name, int length);
    void InitializeIntl(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &obj_func_prototype_value);
    void InitializeLocale(const JSHandle<GlobalEnv> &env);
    void InitializeDateTimeFormat(const JSHandle<GlobalEnv> &env);
    void InitializeRelativeTimeFormat(const JSHandle<GlobalEnv> &env);
    void InitializeNumberFormat(const JSHandle<GlobalEnv> &env);
    void InitializeCollator(const JSHandle<GlobalEnv> &env);
    void InitializePluralRules(const JSHandle<GlobalEnv> &env);

    void GeneralUpdateError(ErrorParameter *error, EcmaEntrypoint constructor, EcmaEntrypoint method, const char *name,
                            JSType type) const;

    void InitializeSet(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeWeakRef(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeWeakMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeWeakSet(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeMath(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &obj_func_prototype_val) const;

    void InitializeJson(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &obj_func_prototype_val) const;

    void InitializeString(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &prim_ref_obj_dynclass) const;

    void InitializeIterator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeAsyncIterator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeStringIterator(const JSHandle<GlobalEnv> &env,
                                  const JSHandle<JSHClass> &iterator_func_dynclass) const;

    void InitializeForinIterator(const JSHandle<GlobalEnv> &env,
                                 const JSHandle<JSHClass> &iterator_func_dynclass) const;

    void InitializeMapIterator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &iterator_func_dynclass) const;

    void InitializeSetIterator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &iterator_func_dynclass) const;

    void InitializeArrayIterator(const JSHandle<GlobalEnv> &env,
                                 const JSHandle<JSHClass> &iterator_func_dynclass) const;

    void InitializeRegexpIterator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &iterator_func_class) const;

    void InitializeArrayBuffer(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeDataView(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeProxy(const JSHandle<GlobalEnv> &env);

    void InitializeReflect(const JSHandle<GlobalEnv> &env, const JSHandle<JSTaggedValue> &obj_func_prototype_val) const;

    void InitializeAsyncFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeGeneratorFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeGenerator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeAsyncGeneratorFunction(const JSHandle<GlobalEnv> &env,
                                          const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeAsyncGenerator(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &obj_func_dynclass) const;

    void InitializeAsyncFromSyncIteratorPrototypeObject(const JSHandle<GlobalEnv> &env,
                                                        const JSHandle<JSHClass> &obj_func_dynclass) const;

    JSHandle<JSFunction> InitializeExoticConstructor(const JSHandle<GlobalEnv> &env, EcmaEntrypoint ctor_func,
                                                     const char *name, int length);

    void InitializePromise(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &promise_func_dynclass);

    void InitializePromiseJob(const JSHandle<GlobalEnv> &env);

    void InitializeFinalizationRegistry(const JSHandle<GlobalEnv> &env,
                                        const JSHandle<JSHClass> &obj_func_dynclass) const;

    void SetFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj, const char *key,
                     EcmaEntrypoint func, int length) const;

    void SetFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                     EcmaEntrypoint func, int length) const;

    void SetFunction(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                     const JSHandle<JSTaggedValue> &func) const;

    template <JSSymbol::SymbolType>
    void SetFunctionAtSymbol(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj,
                             const JSHandle<JSTaggedValue> &symbol, const char *name, EcmaEntrypoint func,
                             int length) const;
    template <JSSymbol::SymbolType FLAG>
    void SetFunctionAtSymbol(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &symbol,
                             const JSHandle<JSTaggedValue> &function) const;

    void SetStringTagSymbol(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj, const char *key) const;
    JSHandle<JSTaggedValue> CreateGetter(const JSHandle<GlobalEnv> &env, EcmaEntrypoint func, const char *name,
                                         int length) const;

    void SetConstant(const JSHandle<JSObject> &obj, const char *key, JSTaggedValue value) const;

    void SetGlobalThis(const JSHandle<JSObject> &obj, const char *key, const JSHandle<JSTaggedValue> &global_value);

    void SetAttribute(const JSHandle<JSObject> &obj, const char *key, const char *value) const;

    void SetNoneAttributeProperty(const JSHandle<JSObject> &obj, const char *key,
                                  const JSHandle<JSTaggedValue> &value) const;

    void StrictModeForbiddenAccessCallerArguments(const JSHandle<GlobalEnv> &env,
                                                  const JSHandle<JSObject> &prototype) const;

    JSHandle<JSTaggedValue> CreateSetter(const JSHandle<GlobalEnv> &env, EcmaEntrypoint func, const char *name,
                                         int length) const;
    void SetArgumentsSharedAccessor(const JSHandle<GlobalEnv> &env);
    void SetAccessor(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                     const JSHandle<JSTaggedValue> &getter, const JSHandle<JSTaggedValue> &setter) const;
    void SetGetter(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                   const JSHandle<JSTaggedValue> &getter) const;
    void InitializeGcMarker(const JSHandle<GlobalEnv> &env) const;
    JSHandle<JSObject> InitializeArkTools(const JSHandle<GlobalEnv> &env) const;
    JSHandle<JSObject> InitializeArkPrivate(const JSHandle<GlobalEnv> &env) const;
    JSHandle<JSObject> InitializeRuntimeTesting(const JSHandle<GlobalEnv> &env) const;
    void SetConstantObject(const JSHandle<JSObject> &obj, const char *key, JSHandle<JSTaggedValue> &value) const;
    void SetFrozenFunction(const JSHandle<GlobalEnv> &env, const JSHandle<JSObject> &obj, const char *key,
                           EcmaEntrypoint func, int length) const;
#ifdef FUZZING_FUZZILLI_BUILTIN
    void InitializeFuzzilli(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass);
#endif

#include "plugins/ecmascript/runtime/builtins/generated/builtins_initializers_gen.h"
};
}  // namespace panda::ecmascript::builtins
#endif  // ECMASCRIPT_BUILTINS_H
