/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "runtime_stat.h"

#include <iomanip>

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/runtime_call_id.h"

namespace panda::ecmascript {
// NOLINTNEXTLINE(modernize-avoid-c-arrays)
EcmaRuntimeStat::EcmaRuntimeStat(const char *const runtime_caller_names[], int count)
{
    for (int i = 0; i < count; i++) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        caller_stat_.emplace_back(PandaRuntimeCallerStat(PandaString(runtime_caller_names[i])));
    }
}

void EcmaRuntimeStat::StartCount(PandaRuntimeTimer *timer, int caller_id)
{
    if (current_timer_ != nullptr) {
        timer->SetParent(current_timer_);
    }
    PandaRuntimeTimer *parent = current_timer_;
    current_timer_ = timer;
    PandaRuntimeCallerStat *caller_stat = &caller_stat_[caller_id];
    timer->Start(caller_stat, parent);
}

void EcmaRuntimeStat::StopCount(const PandaRuntimeTimer *now_timer)
{
    if (now_timer != current_timer_) {
        return;
    }
    PandaRuntimeTimer *parent_timer = current_timer_->Stop();
    current_timer_ = parent_timer;
}

void EcmaRuntimeStat::Print() const
{
    if (current_timer_ != nullptr) {
        current_timer_->Snapshot();
    }
    LOG_ECMA(ERROR) << GetAllStats();
}

void EcmaRuntimeStat::ResetAllCount()
{
    while (current_timer_ != nullptr) {
        StopCount(current_timer_);
    }
    for (auto &run_caller_stat : caller_stat_) {
        run_caller_stat.Reset();
    }
}

PandaString EcmaRuntimeStat::GetAllStats() const
{
    PandaStringStream statistic;
    statistic << "panda runtime stat:" << std::endl;
    static constexpr int NAME_RIGHT_ADJUSTMENT = 50;
    static constexpr int NUMBER_RIGHT_ADJUSTMENT = 20;
    statistic << std::right << std::setw(NAME_RIGHT_ADJUSTMENT) << "InterPreter && GC && C++ Builtin Function"
              << std::setw(NUMBER_RIGHT_ADJUSTMENT) << "Time(ns)" << std::setw(NUMBER_RIGHT_ADJUSTMENT) << "Count"
              << std::setw(NUMBER_RIGHT_ADJUSTMENT) << "MaxTime(ns)" << std::setw(NUMBER_RIGHT_ADJUSTMENT)
              << "AverageTime(ns)" << std::endl;

    statistic << "==========================================================================================="
              << "=======================================" << std::endl;
    for (auto &run_caller_stat : caller_stat_) {
        if (run_caller_stat.TotalCount() != 0) {
            statistic << std::right << std::setw(NAME_RIGHT_ADJUSTMENT) << run_caller_stat.Name()
                      << std::setw(NUMBER_RIGHT_ADJUSTMENT) << run_caller_stat.TotalTime()
                      << std::setw(NUMBER_RIGHT_ADJUSTMENT) << run_caller_stat.TotalCount()
                      << std::setw(NUMBER_RIGHT_ADJUSTMENT) << run_caller_stat.MaxTime()
                      << std::setw(NUMBER_RIGHT_ADJUSTMENT)
                      << run_caller_stat.TotalTime() / run_caller_stat.TotalCount() << std::endl;
        }
    }
    return statistic.str();
}
}  // namespace panda::ecmascript
