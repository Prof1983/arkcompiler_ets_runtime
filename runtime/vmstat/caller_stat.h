/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_VMSTAT_CALLER_STAT_H
#define ECMASCRIPT_VMSTAT_CALLER_STAT_H

#include <cstdint>
#include <string>
#include <ctime>

#include "plugins/ecmascript/runtime/mem/ecma_string.h"
#include "runtime/include/mem/panda_string.h"
#include "libpandabase/macros.h"

namespace panda::ecmascript {
class EcmaRuntimeStat;
class PandaRuntimeCallerStat {
public:
    // NOLINTNEXTLINE(modernize-pass-by-value)
    explicit PandaRuntimeCallerStat(const PandaString &name) : name_(name) {}
    PandaRuntimeCallerStat() = default;
    virtual ~PandaRuntimeCallerStat() = default;

    DEFAULT_NOEXCEPT_MOVE_SEMANTIC(PandaRuntimeCallerStat);
    DEFAULT_COPY_SEMANTIC(PandaRuntimeCallerStat);

    void UpdateState(uint64_t elapsed)
    {
        total_count_++;
        total_time_ += elapsed;
        max_time_ = elapsed < max_time_ ? max_time_ : elapsed;
    }
    const char *Name() const
    {
        return name_.c_str();
    }
    uint64_t TotalCount() const
    {
        return total_count_;
    }
    uint64_t TotalTime() const
    {
        return total_time_;
    }
    uint64_t MaxTime() const
    {
        return max_time_;
    }

    void Reset()
    {
        total_count_ = 0;
        total_time_ = 0;
        max_time_ = 0;
    }

private:
    PandaString name_ {};
    uint64_t total_count_ {0};
    uint64_t total_time_ {0};
    uint64_t max_time_ {0};
};

class PandaRuntimeTimer {
public:
    void Start(PandaRuntimeCallerStat *caller_stat, PandaRuntimeTimer *parent);
    inline static uint64_t Now()
    {
        struct timespec time_now = {0, 0};
        clock_gettime(CLOCK_REALTIME, &time_now);
        return time_now.tv_sec * NANOSECONDSINSECOND + time_now.tv_nsec;
    }

    uint64_t Elapsed() const
    {
        return elapsed_;
    }

    inline bool IsStarted() const
    {
        return start_ != 0;
    }

    inline void SetParent(PandaRuntimeTimer *parent)
    {
        parent_ = parent;
    }

    void Snapshot();

    inline void UpdateCallerState()
    {
        caller_stat_->UpdateState(elapsed_);
    }

private:
    static constexpr uint64_t NANOSECONDSINSECOND = 1000000000;
    PandaRuntimeTimer *Stop();
    void Pause(uint64_t now);
    void Resume(uint64_t now);
    PandaRuntimeCallerStat *caller_stat_ {nullptr};
    PandaRuntimeTimer *parent_ {nullptr};
    uint64_t start_ {0};
    uint64_t elapsed_ {0};

    friend class EcmaRuntimeStat;
};
}  // namespace panda::ecmascript
#endif
