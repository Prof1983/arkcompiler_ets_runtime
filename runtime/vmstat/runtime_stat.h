/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_VMSTAT_RUNTIME_STAT_H
#define ECMASCRIPT_VMSTAT_RUNTIME_STAT_H

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/vmstat/caller_stat.h"

namespace panda::ecmascript {
class EcmaRuntimeStat {
public:
    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    explicit EcmaRuntimeStat(const char *const runtime_caller_names[], int count);
    EcmaRuntimeStat() = default;
    virtual ~EcmaRuntimeStat() = default;

    DEFAULT_NOEXCEPT_MOVE_SEMANTIC(EcmaRuntimeStat);
    DEFAULT_COPY_SEMANTIC(EcmaRuntimeStat);

    void StartCount(PandaRuntimeTimer *timer, int caller_id);
    void StopCount(const PandaRuntimeTimer *timer);
    PandaString GetAllStats() const;
    void ResetAllCount();
    void Print() const;

private:
    PandaRuntimeTimer *current_timer_ = nullptr;
    PandaVector<PandaRuntimeCallerStat> caller_stat_ {};
};

class RuntimeTimerScope {
public:
    explicit RuntimeTimerScope(JSThread *thread, int caller_id, EcmaRuntimeStat *stat)
    {
        bool stat_enabled = thread->GetEcmaVM()->IsRuntimeStatEnabled();
        if (!stat_enabled || stat == nullptr) {
            return;
        }
        stats_ = stat;
        stats_->StartCount(&timer_, caller_id);
    }
    RuntimeTimerScope(const EcmaVM *vm, int caller_id, EcmaRuntimeStat *stat)
    {
        bool stat_enabled = vm->IsRuntimeStatEnabled();
        if (!stat_enabled || stat == nullptr) {
            return;
        }
        stats_ = stat;
        stats_->StartCount(&timer_, caller_id);
    }
    ~RuntimeTimerScope()
    {
        if (stats_ != nullptr) {
            stats_->StopCount(&timer_);
        }
    }
    NO_COPY_SEMANTIC(RuntimeTimerScope);
    NO_MOVE_SEMANTIC(RuntimeTimerScope);

private:
    PandaRuntimeTimer timer_ {};
    EcmaRuntimeStat *stats_ {nullptr};
};
}  // namespace panda::ecmascript
#endif
