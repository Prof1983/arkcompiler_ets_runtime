/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_PROMISE_H
#define ECMASCRIPT_PROMISE_H

#include "js_object.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/tagged_queue.h"
#include "plugins/ecmascript/runtime/tagged_queue-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/accessor_data.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript {
enum class PromiseStatus : uint32_t { PENDING = 0, FULFILLED, REJECTED };
enum class PromiseRejectionEvent : uint32_t { REJECT = 0, HANDLE };

class PromiseReaction final : public Record {
public:
    CAST_CHECK(PromiseReaction, IsPromiseReaction);

    ACCESSORS_BASE(Record)
    ACCESSORS(0, PromiseCapability)
    ACCESSORS(1, Type)
    ACCESSORS(2, Handler)
    ACCESSORS_FINISH(3)

    DECL_DUMP()
};

class PromiseCapability final : public Record {
public:
    CAST_CHECK(PromiseCapability, IsPromiseCapability);

    ACCESSORS_BASE(Record)
    ACCESSORS(0, Promise)
    ACCESSORS(1, Resolve)
    ACCESSORS(2, Reject)
    ACCESSORS_FINISH(3)

    DECL_DUMP()
};

class PromiseIteratorRecord final : public Record {
public:
    CAST_CHECK(PromiseIteratorRecord, IsPromiseIteratorRecord);

    ACCESSORS_BASE(Record)
    ACCESSORS(0, Iterator)
    ACCESSORS(1, Done)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};

class PromiseRecord final : public Record {
public:
    CAST_CHECK(PromiseRecord, IsPromiseRecord);

    ACCESSORS_BASE(Record)
    ACCESSORS(0, Value)
    ACCESSORS_FINISH(1)

    DECL_DUMP()
};

class ResolvingFunctionsRecord final : public Record {
public:
    CAST_CHECK(ResolvingFunctionsRecord, IsResolvingFunctionsRecord);

    ACCESSORS_BASE(Record)
    ACCESSORS(0, ResolveFunction)
    ACCESSORS(1, RejectFunction)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};

class JSPromise final : public JSObject {
public:
    CAST_CHECK(JSPromise, IsJSPromise);

    // ES6 25.4.1.3 CreateResolvingFunctions ( promise )
    static JSHandle<ResolvingFunctionsRecord> CreateResolvingFunctions(JSThread *thread,
                                                                       const JSHandle<JSPromise> &promise);

    // ES6 25.4.1.4 FulfillPromise ( promise, value)
    static JSTaggedValue FulfillPromise(JSThread *thread, const JSHandle<JSPromise> &promise,
                                        const JSHandle<JSTaggedValue> &value);

    // 25.4.1.5 NewPromiseCapability ( C )
    static JSHandle<PromiseCapability> NewPromiseCapability(JSThread *thread, const JSHandle<JSTaggedValue> &obj);

    // ES6 24.4.1.6 IsPromise (x)
    static bool IsPromise(const JSHandle<JSTaggedValue> &value);

    // ES6 25.4.1.7 RejectPromise (promise, reason)
    static JSTaggedValue RejectPromise(JSThread *thread, const JSHandle<JSPromise> &promise,
                                       const JSHandle<JSTaggedValue> &reason);

    // 25.4.1.8 TriggerPromiseReactions (reactions, argument)
    static JSTaggedValue TriggerPromiseReactions(JSThread *thread, const JSHandle<TaggedQueue> &reactions,
                                                 const JSHandle<JSTaggedValue> &argument);

    static JSHandle<JSTaggedValue> IfThrowGetThrowValue(JSThread *thread);

    // 27.2.4.7.1 PromiseResolve ( C, x )
    static JSTaggedValue PromiseResolve(JSThread *thread, const JSHandle<JSTaggedValue> &constructor,
                                        const JSHandle<JSTaggedValue> &value);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, PromiseState)
    ACCESSORS(1, PromiseResult)
    ACCESSORS(2, PromiseFulfillReactions)
    ACCESSORS(3, PromiseRejectReactions)
    ACCESSORS(4, PromiseIsHandled)
    ACCESSORS_FINISH(5)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_PROMISE_H
