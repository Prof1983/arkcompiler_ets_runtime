/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JSARRAY_H
#define ECMASCRIPT_JSARRAY_H

#include <limits>
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

namespace panda::ecmascript {
// ecma6 9.4.2 Array Exotic Object
class JSArray : public JSObject {
public:
    static constexpr int LENGTH_INLINE_PROPERTY_INDEX = 0;

    CAST_CHECK(JSArray, IsJSArray);

    PANDA_PUBLIC_API static JSHandle<JSTaggedValue> ArrayCreate(
        JSThread *thread, JSTaggedNumber length, panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    static JSHandle<JSTaggedValue> ArrayCreate(JSThread *thread, JSTaggedNumber length,
                                               const JSHandle<JSTaggedValue> &new_target,
                                               panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    static JSTaggedValue ArraySpeciesCreate(JSThread *thread, const JSHandle<JSObject> &original_array,
                                            JSTaggedNumber length);
    static bool ArraySetLength(JSThread *thread, const JSHandle<JSObject> &array, const PropertyDescriptor &desc);
    PANDA_PUBLIC_API static bool DefineOwnProperty(JSThread *thread, const JSHandle<JSObject> &array,
                                                   const JSHandle<JSTaggedValue> &key, const PropertyDescriptor &desc);
    static bool DefineOwnProperty(JSThread *thread, const JSHandle<JSObject> &array, uint32_t index,
                                  const PropertyDescriptor &desc);

    static bool IsLengthString(JSThread *thread, const JSHandle<JSTaggedValue> &key);
    // ecma6 7.3 Operations on Objects
    PANDA_PUBLIC_API static JSHandle<JSArray> CreateArrayFromList(JSThread *thread,
                                                                  const JSHandle<TaggedArray> &elements)
    {
        return CreateArrayFromList(thread, elements, elements->GetLength());
    }

    PANDA_PUBLIC_API static JSHandle<JSArray> CreateArrayFromList(JSThread *thread,
                                                                  const JSHandle<TaggedArray> &elements,
                                                                  ArraySizeT length);
    // use first inlined property slot for array length
    inline uint32_t GetArrayLength() const
    {
        return GetLength().GetArrayLength();
    }

    inline void SetArrayLength(const JSThread *thread, uint32_t length)
    {
        SetLength(thread, JSTaggedValue(length));
    }

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, Length)
    ACCESSORS_FINISH(1)

    static const uint32_t MAX_ARRAY_INDEX = MAX_ELEMENT_INDEX;
    DECL_DUMP()

    static int32_t GetArrayLengthOffset()
    {
        return GetLengthOffset();
    }

    static bool PropertyKeyToArrayIndex(JSThread *thread, const JSHandle<JSTaggedValue> &key, uint32_t *output);

    static JSTaggedValue LengthGetter(JSThread *thread, const JSHandle<JSObject> &self);

    static bool LengthSetter(JSThread *thread, const JSHandle<JSObject> &self, const JSHandle<JSTaggedValue> &value,
                             bool may_throw = false);

    static JSHandle<JSTaggedValue> FastGetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                          uint32_t index);

    static JSHandle<JSTaggedValue> FastGetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                          const JSHandle<JSTaggedValue> &key);

    static bool FastSetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj, uint32_t index,
                                       const JSHandle<JSTaggedValue> &value);

    static bool FastSetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                       const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value);

private:
    static void SetCapacity(JSThread *thread, const JSHandle<JSObject> &array, uint32_t old_len, uint32_t new_len);
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JSARRAY_H
