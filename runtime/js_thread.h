/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_THREAD_H
#define ECMASCRIPT_JS_THREAD_H

#include "mem/ecma_string.h"
#include "runtime/include/thread.h"

#include "include/managed_thread.h"
#include "plugins/ecmascript/runtime/ecma_global_storage.h"
#include "plugins/ecmascript/runtime/global_env_constants.h"
#include "plugins/ecmascript/runtime/mem/object_xray.h"

namespace panda::ecmascript {
class EcmaVM;
class EcmascriptEnvironment;
class InternalCallParams;
class PropertiesCache;
class JSSpanHandle;
class ScopedCallInfo;

class JSThread : public ManagedThread {
public:
    static constexpr int CONCURRENT_MARKING_BITFIELD_NUM = 2;
    using Address = uintptr_t;
    static JSThread *Cast(ManagedThread *thread)
    {
        ASSERT(thread != nullptr);
        return reinterpret_cast<JSThread *>(thread);
    }

    /**
     * @brief GetCurrentRaw Unsafe method to get current JSThread.
     * It can be used in hotspots to get the best performance.
     * We can only use this method in places where the JSThread exists.
     * @return pointer to JSThread
     */
    static JSThread *GetCurrentRaw()
    {
        ManagedThread *managed_thread = ManagedThread::GetCurrent();
        ASSERT(managed_thread != nullptr);
        ASSERT(managed_thread->GetThreadLang() == panda::panda_file::SourceLang::ECMASCRIPT);
        return JSThread::Cast(managed_thread);
    }

    /**
     * @brief GetCurrent Safe method to gets current JSThread.
     * @return pointer to JSThread or nullptr (if current thread is not a js thread)
     */
    static JSThread *GetCurrent()
    {
        ManagedThread *managed_thread = ManagedThread::GetCurrent();
        if (managed_thread != nullptr && managed_thread->GetThreadLang() == panda::panda_file::SourceLang::ECMASCRIPT) {
            return JSThread::Cast(managed_thread);
        }
        return nullptr;
    }

    JSThread(Runtime *runtime, PandaVM *vm);

    ~JSThread() override;

    PANDA_PUBLIC_API EcmaVM *GetEcmaVM() const;

    static JSThread *Create(Runtime *runtime, PandaVM *vm);

    int GetNestedLevel() const
    {
        return nested_level_;
    }

    void SetNestedLevel(int level)
    {
        nested_level_ = level;
    }

    void Iterate(const RootVisitor &v0, const RootRangeVisitor &v1);

    PANDA_PUBLIC_API uintptr_t *ExpandHandleStorage();
    PANDA_PUBLIC_API void ShrinkHandleStorage(int prev_index);

    JSTaggedType *GetHandleScopeStorageNext() const
    {
        return handle_scope_storage_next_;
    }

    void SetHandleScopeStorageNext(JSTaggedType *value)
    {
        handle_scope_storage_next_ = value;
    }

    JSTaggedType *GetHandleScopeStorageEnd() const
    {
        return handle_scope_storage_end_;
    }

    void SetHandleScopeStorageEnd(JSTaggedType *value)
    {
        handle_scope_storage_end_ = value;
    }

    int GetCurrentHandleStorageIndex()
    {
        return current_handle_storage_index_;
    }

    void HandleScopeCountAdd()
    {
        handle_scope_count_++;
    }

    void HandleScopeCountDec()
    {
        handle_scope_count_--;
    }

    void SetException(JSTaggedValue exception);

    JSTaggedValue GetException() const
    {
        return JSTaggedValue(ManagedThread::GetException());
    }

    bool HasPendingException() const
    {
        return !JSTaggedValue(ManagedThread::GetException()).IsHole();
    }

    PANDA_PUBLIC_API void ClearException();

    EcmaGlobalStorage *GetEcmaGlobalStorage() const
    {
        return global_storage_;
    }

    const GlobalEnvConstants *GlobalConstants() const
    {
        return &global_const_;
    }

    JSTaggedValue GetFunctionalObject() const
    {
        return functional_object_;
    }

    void SetFunctionalObject(JSTaggedValue functional_object)
    {
        functional_object_ = functional_object;
    }

    JSTaggedValue GetInvocationLexicalEnv() const
    {
        // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
        return invocation_lexical_env_;  // GC root
    }

    void SetInvocationLexicalEnv(JSTaggedValue invocation_lexical_env)
    {
        invocation_lexical_env_ = invocation_lexical_env;
    }

    void NotifyStableArrayElementsGuardians(JSHandle<JSObject> receiver);

    bool IsStableArrayElementsGuardiansInvalid() const
    {
        return !stable_array_elements_guardians_;
    }

    void ResetGuardians();

    InternalCallParams *GetInternalCallParams() const
    {
        return internal_call_params_;
    }

    ThreadId GetThreadId() const
    {
        return GetId();
    }

    static ThreadId GetCurrentThreadId()
    {
        return os::thread::GetCurrentThreadId();
    }

    PropertiesCache *GetPropertiesCache() const
    {
        return properties_cache_;
    }

    static constexpr uint32_t GetPropertiesCacheOffset()
    {
        return MEMBER_OFFSET(JSThread, properties_cache_);
    }

    static constexpr uint32_t GetGlobalConstantsOffset()
    {
        return MEMBER_OFFSET(JSThread, global_const_);
    }

    static constexpr uint32_t GetGlobalStorageOffset()
    {
        return MEMBER_OFFSET(JSThread, global_storage_);
    }

    static constexpr uint32_t GetGlobalObjectOffset()
    {
        return MEMBER_OFFSET(JSThread, global_obj_);
    }

    void SetGlobalObject(JSTaggedValue global_obj)
    {
        global_obj_ = global_obj;
    }

    JSTaggedValue GetGlobalObject() const
    {
        return global_obj_;
    }

    void DisableStackOverflowCheck() override;
    void EnableStackOverflowCheck() override;

private:
    void IterateEcmascriptEnvironment(const RootVisitor &v0, const RootRangeVisitor &v1);

    NO_COPY_SEMANTIC(JSThread);
    NO_MOVE_SEMANTIC(JSThread);

    void DumpStack() DUMP_API_ATTR;
    bool TestLockState() const override
    {
        return true;
    }

    static constexpr uint32_t MAX_STACK_SIZE = 128 * 1024;
    static constexpr uint32_t RESERVE_STACK_SIZE = 128;
    static const uint32_t NODE_BLOCK_SIZE_LOG2 = 10;
    static const uint32_t NODE_BLOCK_SIZE = 1U << NODE_BLOCK_SIZE_LOG2;
    static constexpr int32_t MIN_HANDLE_STORAGE_SIZE = 2;

    // MM: handles, global-handles
    int nested_level_ = 0;
    JSTaggedType *handle_scope_storage_next_ {nullptr};
    JSTaggedType *handle_scope_storage_end_ {nullptr};
    PandaVector<std::array<JSTaggedType, NODE_BLOCK_SIZE> *> handle_storage_nodes_ {};
    int32_t current_handle_storage_index_ {-1};
    int32_t handle_scope_count_ {0};
    JSSpanHandle *span_handle_ {nullptr};
    ScopedCallInfo *scoped_call_info_ {nullptr};

    // Run-time state
    bool stable_array_elements_guardians_ {true};
    InternalCallParams *internal_call_params_ {nullptr};

    // GLUE members start, very careful to modify here
    GlobalEnvConstants global_const_;  // Place-Holder
    PropertiesCache *properties_cache_ {nullptr};
    EcmaGlobalStorage *global_storage_ {nullptr};

    JSTaggedValue global_obj_ {JSTaggedValue::Hole()};

    // Uses to forward a0 argument (functional object) to InvokeHelper
    JSTaggedValue functional_object_;
    JSTaggedValue invocation_lexical_env_;

    friend class EcmaHandleScope;
    friend class JSSpanHandle;
    friend class ScopedCallInfo;
    friend class GlobalHandleCollection;
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_THREAD_H
