/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_STRING_TABLE_H
#define ECMASCRIPT_STRING_TABLE_H

#include "plugins/ecmascript/runtime/mem/object_xray.h"

#include "runtime/string_table.h"

namespace panda::ecmascript {
class EcmaString;
class EcmaVM;

class EcmaStringTable {
public:
    explicit EcmaStringTable(const EcmaVM *vm);
    virtual ~EcmaStringTable()
    {
        os::memory::WriteLockHolder holder(table_lock_);
        table_.clear();
    }

    void InternEmptyString(EcmaString *empty_str);
    EcmaString *GetOrInternString(const uint8_t *utf8_data, uint32_t utf8_len, bool can_be_compress,
                                  panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    EcmaString *GetOrInternString(const uint16_t *utf16_data, uint32_t utf16_len, bool can_be_compress,
                                  panda::SpaceType space_type = panda::SpaceType::SPACE_TYPE_OBJECT);
    EcmaString *GetOrInternString(EcmaString *string);

    void SweepWeakReference(const WeakRootVisitor &visitor);

    ObjectHeader *ResolveString(const panda_file::File &pf, panda_file::File::EntityId id);

    void VisitRoots(const StringTable::StringVisitor &visitor,
                    mem::VisitGCRootFlags flags = mem::VisitGCRootFlags::ACCESS_ROOT_ALL);

    void Sweep(const GCObjectVisitor &visitor);
    bool UpdateMoved();

private:
    NO_COPY_SEMANTIC(EcmaStringTable);
    NO_MOVE_SEMANTIC(EcmaStringTable);

    EcmaString *GetString(const uint8_t *utf8_data, uint32_t utf8_len, bool can_be_compress) const;
    EcmaString *GetString(const uint16_t *utf16_data, uint32_t utf16_len) const;
    EcmaString *GetString(EcmaString *string) const;

    void InternString(EcmaString *string);

    void InsertStringIfNotExist(EcmaString *string)
    {
        EcmaString *str = GetString(string);
        if (str == nullptr) {
            InternString(string);
        }
    }

    PandaUnorderedMultiMap<uint32_t, EcmaString *> table_ GUARDED_BY(table_lock_);
    mutable os::memory::RWLock table_lock_;
    const EcmaVM *vm_ {nullptr};
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_STRING_TABLE_H
