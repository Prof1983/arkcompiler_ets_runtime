/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_TAGGED_DICTIONARY_H
#define ECMASCRIPT_TAGGED_DICTIONARY_H

#include "plugins/ecmascript/runtime/tagged_hash_table.h"

namespace panda::ecmascript {
class NameDictionary : public OrderTaggedHashTable<NameDictionary> {
public:
    using OrderHashTableT = OrderTaggedHashTable<NameDictionary>;
    inline static int GetKeyIndex(int entry)
    {
        return OrderHashTableT::TABLE_HEADER_SIZE + entry * GetEntrySize() + ENTRY_KEY_INDEX;
    }
    inline static int GetValueIndex(int entry)
    {
        return OrderHashTableT::TABLE_HEADER_SIZE + entry * GetEntrySize() + ENTRY_VALUE_INDEX;
    }
    inline static int GetEntryIndex(int entry)
    {
        return OrderHashTableT::TABLE_HEADER_SIZE + entry * GetEntrySize();
    }
    inline static int GetEntrySize()
    {
        return ENTRY_SIZE;
    }
    static int Hash(const JSTaggedValue &key);
    static bool IsMatch(const JSTaggedValue &key, const JSTaggedValue &other);
    static JSHandle<NameDictionary> Create(const JSThread *thread,
                                           int number_of_elements = OrderHashTableT::DEFAULT_ELEMENTS_NUMBER);
    // Returns the property meta_data for the property at entry.
    PropertyAttributes GetAttributes(int entry) const;
    void SetAttributes(const JSThread *thread, int entry, const PropertyAttributes &meta_data);
    void SetEntry(const JSThread *thread, int entry, const JSTaggedValue &key, const JSTaggedValue &value,
                  const PropertyAttributes &meta_data);
    void UpdateValueAndAttributes(const JSThread *thread, int entry, const JSTaggedValue &value,
                                  const PropertyAttributes &meta_data);
    void UpdateValue(const JSThread *thread, int entry, const JSTaggedValue &value);
    void UpdateAttributes(int entry, const PropertyAttributes &meta_data);
    void ClearEntry(const JSThread *thread, int entry);
    void GetAllKeys(const JSThread *thread, int offset, TaggedArray *key_array) const;
    void GetAllEnumKeys(const JSThread *thread, int offset, TaggedArray *key_array, uint32_t *keys) const;
    static inline bool CompKey(const std::pair<JSTaggedValue, PropertyAttributes> &a,
                               const std::pair<JSTaggedValue, PropertyAttributes> &b)
    {
        // ES6 9.4.5.6
        // 3. For each own property key P of O such that Type(P) is String and P is
        //      not an array index, in ascending chronological order of property creation, do
        //  a. Add P as the last element of keys.
        // 4. For each own property key P of O such that Type(P) is Symbol,
        //      in ascending chronological order of property creation, do
        //  a. Add P as the last element of keys.

        if ((a.first.IsString() && b.first.IsString()) || (a.first.IsSymbol() && b.first.IsSymbol())) {
            return a.second.GetDictionaryOrder() < b.second.GetDictionaryOrder();
        }

        return a.first.IsString() && b.first.IsSymbol();
    }
    DECL_DUMP()

    static constexpr int ENTRY_KEY_INDEX = 0;
    static constexpr int ENTRY_VALUE_INDEX = 1;
    static constexpr int ENTRY_DETAILS_INDEX = 2;
    static constexpr int ENTRY_SIZE = 3;
};

class NumberDictionary : public OrderTaggedHashTable<NumberDictionary> {
public:
    using OrderHashTableT = OrderTaggedHashTable<NumberDictionary>;
    inline static int GetKeyIndex(int entry)
    {
        return OrderHashTableT::TABLE_HEADER_SIZE + entry * GetEntrySize() + ENTRY_KEY_INDEX;
    }
    inline static int GetValueIndex(int entry)
    {
        return OrderHashTableT::TABLE_HEADER_SIZE + entry * GetEntrySize() + ENTRY_VALUE_INDEX;
    }
    inline static int GetEntryIndex(int entry)
    {
        return OrderHashTableT::TABLE_HEADER_SIZE + entry * GetEntrySize();
    }
    inline static int GetEntrySize()
    {
        return ENTRY_SIZE;
    }
    static int Hash(const JSTaggedValue &key);
    static bool IsMatch(const JSTaggedValue &key, const JSTaggedValue &other);
    static JSHandle<NumberDictionary> Create(const JSThread *thread,
                                             int number_of_elements = OrderHashTableT::DEFAULT_ELEMENTS_NUMBER);
    // Returns the property meta_data for the property at entry.
    PropertyAttributes GetAttributes(int entry) const;
    void SetAttributes(const JSThread *thread, int entry, const PropertyAttributes &meta_data);
    void SetEntry([[maybe_unused]] const JSThread *thread, int entry, const JSTaggedValue &key,
                  const JSTaggedValue &value, const PropertyAttributes &meta_data);
    void UpdateValueAndAttributes(const JSThread *thread, int entry, const JSTaggedValue &value,
                                  const PropertyAttributes &meta_data);
    void UpdateValue(const JSThread *thread, int entry, const JSTaggedValue &value);
    void UpdateAttributes(int entry, const PropertyAttributes &meta_data);
    void ClearEntry(const JSThread *thread, int entry);

    static void GetAllKeys(const JSThread *thread, const JSHandle<NumberDictionary> &obj, int offset,
                           const JSHandle<TaggedArray> &key_array);
    static void GetAllEnumKeys(const JSThread *thread, const JSHandle<NumberDictionary> &obj, int offset,
                               const JSHandle<TaggedArray> &key_array, uint32_t *keys);
    static inline bool CompKey(const JSTaggedValue &a, const JSTaggedValue &b)
    {
        ASSERT(a.IsNumber() && b.IsNumber());
        return a.GetNumber() < b.GetNumber();
    }
    DECL_DUMP()

    static constexpr int ENTRY_KEY_INDEX = 0;
    static constexpr int ENTRY_VALUE_INDEX = 1;
    static constexpr int ENTRY_DETAILS_INDEX = 2;
    static constexpr int ENTRY_SIZE = 3;
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_NEW_DICTIONARY_H
