/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_RUNTIME_ECMASCRIPT_CALL_PARAMS
#define PANDA_RUNTIME_ECMASCRIPT_CALL_PARAMS

#include <cstdint>

namespace panda::ecmascript::js_method_args {
enum : uint8_t {
    FUNC_IDX = 0,
    NEW_TARGET_IDX = 1,
    THIS_IDX = 2,

    NUM_MANDATORY_ARGS = 3,
    FIRST_ARG_IDX = NUM_MANDATORY_ARGS,
};

}  // namespace panda::ecmascript::js_method_args

#endif  // PANDA_RUNTIME_ECMASCRIPT_CALL_PARAMS
