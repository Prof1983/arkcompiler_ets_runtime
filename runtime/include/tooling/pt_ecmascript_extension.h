/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_ECMASCRIPT_EXTENSION_H
#define PANDA_TOOLING_ECMASCRIPT_EXTENSION_H

#include "runtime/include/tooling/vreg_value.h"
#include "runtime/include/tooling/debug_interface.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"

namespace panda::tooling::ecmascript {
using JSTaggedValue = panda::ecmascript::JSTaggedValue;
class PtEcmaScriptExtension {
public:
    PANDA_PUBLIC_API static VRegValue TaggedValueToVRegValue(JSTaggedValue value);
    PANDA_PUBLIC_API static JSTaggedValue VRegValueToTaggedValue(VRegValue value);
};

}  // namespace panda::tooling::ecmascript

#endif  // PANDA_TOOLING_ECMASCRIPT_EXTENSION_H
