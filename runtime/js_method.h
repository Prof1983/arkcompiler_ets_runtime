/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_METHOD_H
#define ECMASCRIPT_JS_METHOD_H

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/mem/ecma_string.h"
#include "include/method.h"
#include "libpandafile/file.h"

namespace panda {
class Class;
}  // namespace panda

static constexpr uint32_t NORMAL_CALL_TYPE = 0;    // 0: normal (without this, new_target, extra, func)
static constexpr uint32_t HAVE_THIS_BIT = 1;       // 1: the last bit means this
static constexpr uint32_t HAVE_NEWTARGET_BIT = 2;  // 2: the 2nd to last bit means new_target
static constexpr uint32_t HAVE_EXTRA_BIT = 4;      // 4: the 3rd to last bit means extra
static constexpr uint32_t HAVE_FUNC_BIT = 8;       // 8: the 4th to last bit means func (for old version, UINT32_MAX)

namespace panda::ecmascript {
class JSMethod : public Method {
public:
    using ICMappingType = uint8_t *;
    using ConstICMappingType = const uint8_t *;
    static constexpr uint32_t MAX_SLOT_SIZE = 0xFFFFFFFF;

    static JSMethod *Cast(Method *method)
    {
        return static_cast<JSMethod *>(method);
    }

    explicit JSMethod(Class *klass, const panda_file::File *pf, panda_file::File::EntityId file_id,
                      panda_file::File::EntityId code_id, uint32_t access_flags, uint32_t num_args,
                      const uint16_t *shorty)
        : Method(klass, pf, file_id, code_id, access_flags, num_args, shorty)
    {
        bytecode_array_ = JSMethod::GetInstructions();
        bytecode_array_size_ = JSMethod::GetCodeSize();
    }

    explicit JSMethod(const Method *method) : Method(method)
    {
        bytecode_array_ = JSMethod::GetInstructions();
        bytecode_array_size_ = JSMethod::GetCodeSize();
    }

    JSMethod() = delete;
    ~JSMethod();
    JSMethod(const JSMethod &) = delete;
    JSMethod(JSMethod &&) = delete;
    JSMethod &operator=(const JSMethod &) = delete;
    JSMethod &operator=(JSMethod &&) = delete;

    static constexpr uint32_t GetBytecodeArrayOffset()
    {
        return MEMBER_OFFSET(JSMethod, bytecode_array_);
    }

    const uint8_t *GetBytecodeArray() const
    {
        return bytecode_array_;
    }

    uint32_t GetBytecodeArraySize() const
    {
        return bytecode_array_size_;
    }

    void SetBytecodeArray(const uint8_t *bc)
    {
        bytecode_array_ = bc;
    }

    uint32_t GetSlotSize() const
    {
        return slot_size_;
    }

    void AddSlotSize(uint32_t size)
    {
        slot_size_ += size;
    }

    ICMappingType GetICMapping()
    {
        return ic_mapping_;
    }

    ConstICMappingType GetICMapping() const
    {
        return ic_mapping_;
    }

    void SetICMapping(ICMappingType mapping)
    {
        ic_mapping_ = mapping;
    }

    static constexpr size_t GetICMappingOffset()
    {
        return MEMBER_OFFSET(JSMethod, ic_mapping_);
    }

    uint32_t GetCallType() const
    {
        return call_type_;
    }

    PandaString ParseFunctionName() const;
    std::string GetFullName([[maybe_unused]] bool with_signature = false) const;
    void SetCallTypeFromAnnotation();

    void InitProfileVector();

    JSTaggedValue GetLength() const;

    uint8_t *GetProfilingVector() const
    {
        return profiling_data_.get();
    }

    uint32_t GetProfileSize() const
    {
        return profile_size_;
    }

private:
    const uint8_t *bytecode_array_ {nullptr};
    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    PandaUniquePtr<uint8_t[]> profiling_data_;
    uint32_t bytecode_array_size_ {0};
    uint32_t slot_size_ {0};
    ICMappingType ic_mapping_ {nullptr};
    uint32_t call_type_ {UINT32_MAX};  // UINT32_MAX means not found
    uint32_t profile_size_ {0};
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_METHOD_H
