/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_INTERPRETER_FAST_RUNTIME_STUB_INL_H
#define ECMASCRIPT_INTERPRETER_FAST_RUNTIME_STUB_INL_H

#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub.h"

#include "plugins/ecmascript/runtime/ecma_profiling.h"
#include "plugins/ecmascript/runtime/global_dictionary-inl.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_arraylist.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_hclass-inl.h"
#include "plugins/ecmascript/runtime/js_proxy.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/object_factory-inl.h"
#include "plugins/ecmascript/runtime/runtime_call_id.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/runtime/vmstat/runtime_stat.h"

namespace panda::ecmascript {

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define SET_PROFILING_ACCESS(access)                      \
    if (prof != nullptr) {                                \
        ObjByIndexOperationProfile::Update(prof, access); \
    }

JSTaggedValue FastRuntimeStub::FastAdd(JSTaggedValue left, JSTaggedValue right)
{
    if (left.IsNumber() && right.IsNumber()) {
        return JSTaggedValue(left.GetNumber() + right.GetNumber());
    }

    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FastSub(JSTaggedValue left, JSTaggedValue right)
{
    if (left.IsNumber() && right.IsNumber()) {
        return JSTaggedValue(left.GetNumber() - right.GetNumber());
    }

    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FastMul(JSTaggedValue left, JSTaggedValue right)
{
    if (left.IsNumber() && right.IsNumber()) {
        return JSTaggedValue(left.GetNumber() * right.GetNumber());
    }

    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FastDiv(JSTaggedValue left, JSTaggedValue right)
{
    if (left.IsNumber() && right.IsNumber()) {
        double d_left = left.IsInt() ? left.GetInt() : left.GetDouble();
        double d_right = right.IsInt() ? right.GetInt() : right.GetDouble();
        if (UNLIKELY(d_right == 0.0)) {
            if (d_left == 0.0 || std::isnan(d_left)) {
                return JSTaggedValue(base::NAN_VALUE);
            }
            uint64_t flag_bit = ((bit_cast<uint64_t>(d_left)) ^ (bit_cast<uint64_t>(d_right))) & base::DOUBLE_SIGN_MASK;
            return JSTaggedValue(bit_cast<double>(flag_bit ^ (bit_cast<uint64_t>(base::POSITIVE_INFINITY))));
        }
        return JSTaggedValue(d_left / d_right);
    }
    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FastMod(JSTaggedValue left, JSTaggedValue right)
{
    if (right.IsInt() && left.IsInt()) {
        int i_right = right.GetInt();
        int i_left = left.GetInt();
        if (i_right > 0 && i_left >= 0) {
            return JSTaggedValue(i_left % i_right);
        }
    }
    if (left.IsNumber() && right.IsNumber()) {
        double d_left = left.IsInt() ? left.GetInt() : left.GetDouble();
        double d_right = right.IsInt() ? right.GetInt() : right.GetDouble();
        if (d_right == 0.0 || std::isnan(d_right) || std::isnan(d_left) || std::isinf(d_left)) {
            return JSTaggedValue(base::NAN_VALUE);
        }
        if (d_left == 0.0 || std::isinf(d_right)) {
            return JSTaggedValue(d_left);
        }
        return JSTaggedValue(std::fmod(d_left, d_right));
    }
    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FastEqual(JSTaggedValue left, JSTaggedValue right)
{
    if (left == right) {
        if (UNLIKELY(left.IsDouble())) {
            return JSTaggedValue(!std::isnan(left.GetDouble()));
        }
        return JSTaggedValue::True();
    }
    if (left.IsNumber()) {
        if (left.IsInt() && right.IsInt()) {
            return JSTaggedValue::False();
        }
    }
    if (right.IsUndefinedOrNull()) {
        if (left.IsHeapObject()) {
            return JSTaggedValue::False();
        }
        if (left.IsUndefinedOrNull()) {
            return JSTaggedValue::True();
        }
    }
    if (left.IsBoolean()) {
        if (right.IsSpecial()) {
            return JSTaggedValue::False();
        }
    }
    return JSTaggedValue::Hole();
}

bool FastRuntimeStub::FastStrictEqual(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    if (left.IsNumber()) {
        if (right.IsNumber()) {
            double d_left = left.IsInt() ? left.GetInt() : left.GetDouble();
            double d_right = right.IsInt() ? right.GetInt() : right.GetDouble();
            return JSTaggedValue::StrictNumberEquals(d_left, d_right);
        }
        return false;
    }
    if (right.IsNumber()) {
        return false;
    }
    if (left == right) {
        return true;
    }
    if (left.IsString() && right.IsString()) {
        return EcmaString::StringsAreEqual(static_cast<EcmaString *>(left.GetTaggedObject()),
                                           static_cast<EcmaString *>(right.GetTaggedObject()));
    }
    if (left.IsBigInt() && right.IsBigInt()) {
        return BigInt::Equal(thread, left, right);
    }
    return false;
}

bool FastRuntimeStub::IsSpecialIndexedObj(JSType js_type)
{
    return js_type > JSType::JS_ARRAY;
}

bool FastRuntimeStub::IsSpecialReceiverObj(JSType js_type)
{
    return js_type > JSType::JS_PRIMITIVE_REF;
}

bool FastRuntimeStub::IsSpecialContainer(JSType js_type)
{
    return js_type >= JSType::JS_ARRAY_LIST && js_type <= JSType::JS_QUEUE;
}

uint32_t FastRuntimeStub::TryToElementsIndex(JSTaggedValue key)
{
    if (LIKELY(key.IsInt())) {
        return key.GetInt();
    }
    if (key.IsString()) {
        uint32_t index = 0;
        if (JSTaggedValue::StringToElementIndex(key, &index)) {
            return index;
        }
    } else if (key.IsDouble()) {
        double number = key.GetDouble();
        auto integer = base::NumberHelper::DoubleToInt(number, base::INT32_BITS);
        if (number == integer) {
            return integer;
        }
    }
    return JSObject::MAX_ELEMENT_INDEX;
}

JSTaggedValue FastRuntimeStub::CallGetter(JSThread *thread, JSTaggedValue receiver, JSTaggedValue holder,
                                          JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, CallGetter);
    // Accessor
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    AccessorData *accessor = AccessorData::Cast(value.GetTaggedObject());
    if (UNLIKELY(accessor->IsInternal())) {
        JSHandle<JSObject> obj_handle(thread, holder);
        return accessor->CallInternalGet(thread, obj_handle);
    }
    JSHandle<JSTaggedValue> obj_handle(thread, receiver);
    return JSObject::CallGetter(thread, accessor, obj_handle);
}

JSTaggedValue FastRuntimeStub::CallSetter(JSThread *thread, JSTaggedValue receiver, JSTaggedValue value,
                                          JSTaggedValue accessor_value)
{
    INTERPRETER_TRACE(thread, CallSetter);
    // Accessor
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> obj_handle(thread, receiver);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    auto accessor = AccessorData::Cast(accessor_value.GetTaggedObject());
    bool success = JSObject::CallSetter(thread, *accessor, obj_handle, value_handle, true);
    return success ? JSTaggedValue::Undefined() : JSTaggedValue::Exception();
}

bool FastRuntimeStub::ShouldCallSetter(JSTaggedValue receiver, JSTaggedValue holder, JSTaggedValue accessor_value,
                                       PropertyAttributes attr)
{
    if (!AccessorData::Cast(accessor_value.GetTaggedObject())->IsInternal()) {
        return true;
    }
    if (receiver != holder) {
        return false;
    }
    return attr.IsWritable();
}

PropertyAttributes FastRuntimeStub::AddPropertyByName(JSThread *thread, JSHandle<JSObject> obj_handle,
                                                      JSHandle<JSTaggedValue> key_handle,
                                                      JSHandle<JSTaggedValue> value_handle, PropertyAttributes attr)
{
    INTERPRETER_TRACE(thread, AddPropertyByName);

    if (obj_handle->IsJSArray() && key_handle.GetTaggedValue() == thread->GlobalConstants()->GetConstructorString()) {
        obj_handle->GetJSHClass()->SetHasConstructor(true);
    }
    int32_t next_inlined_props_index = obj_handle->GetJSHClass()->GetNextInlinedPropsIndex();
    if (next_inlined_props_index >= 0) {
        obj_handle->SetPropertyInlinedProps(thread, next_inlined_props_index, value_handle.GetTaggedValue());
        attr.SetOffset(next_inlined_props_index);
        attr.SetIsInlinedProps(true);
        JSHClass::AddProperty(thread, obj_handle, key_handle, attr);
        return attr;
    }

    JSMutableHandle<TaggedArray> array(thread, obj_handle->GetProperties());
    uint32_t length = array->GetLength();
    if (length == 0) {
        length = JSObject::MIN_PROPERTIES_LENGTH;
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        array.Update(factory->NewTaggedArray(length).GetTaggedValue());
        obj_handle->SetProperties(thread, array.GetTaggedValue());
    }

    if (!array->IsDictionaryMode()) {
        attr.SetIsInlinedProps(false);

        uint32_t non_inlined_props = obj_handle->GetJSHClass()->GetNextNonInlinedPropsIndex();
        ASSERT(length >= non_inlined_props);
        // if array is full, grow array or change to dictionary mode
        if (length == non_inlined_props) {
            if (UNLIKELY(length == JSHClass::MAX_CAPACITY_OF_OUT_OBJECTS)) {
                // change to dictionary and add one.
                JSHandle<NameDictionary> dict(JSObject::TransitionToDictionary(thread, obj_handle));
                JSHandle<NameDictionary> new_dict =
                    NameDictionary::PutIfAbsent(thread, dict, key_handle, value_handle, attr);
                obj_handle->SetProperties(thread, new_dict);
                // index is not essential when fastMode is false;
                return attr;
            }
            // Grow properties array size
            uint32_t capacity = JSObject::ComputePropertyCapacity(length);
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            array.Update(factory->CopyArray(array, length, capacity).GetTaggedValue());
            obj_handle->SetProperties(thread, array.GetTaggedValue());
        }

        attr.SetOffset(non_inlined_props + obj_handle->GetJSHClass()->GetInlinedProperties());
        JSHClass::AddProperty(thread, obj_handle, key_handle, attr);
        array->Set(thread, non_inlined_props, value_handle.GetTaggedValue());
    } else {
        JSHandle<NameDictionary> dict_handle(array);
        JSHandle<NameDictionary> new_dict =
            NameDictionary::PutIfAbsent(thread, dict_handle, key_handle, value_handle, attr);
        obj_handle->SetProperties(thread, new_dict);
    }
    return attr;
}

JSTaggedValue FastRuntimeStub::AddPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                  JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, AddPropertyByIndex);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    if (UNLIKELY(!JSObject::Cast(receiver)->IsExtensible())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot add property in prevent extensions ", JSTaggedValue::Exception());
    }

    bool success = JSObject::AddElementInternal(thread, JSHandle<JSObject>(thread, receiver), index,
                                                JSHandle<JSTaggedValue>(thread, value), PropertyAttributes::Default());
    return success ? JSTaggedValue::Undefined() : JSTaggedValue::Exception();
}

template <bool USE_OWN>
JSTaggedValue FastRuntimeStub::GetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index, void *prof)
{
    INTERPRETER_TRACE(thread, GetPropertyByIndex);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSTaggedValue holder = receiver;
    do {
        auto *hclass = holder.GetTaggedObject()->GetClass();
        JSType js_type = hclass->GetObjectType();
        if (IsSpecialIndexedObj(js_type)) {
            SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ACCESS);
            if (IsSpecialContainer(js_type)) {
                return GetContainerProperty(thread, holder, index, js_type);
            }
            return JSTaggedValue::Hole();
        }
        TaggedArray *elements = TaggedArray::Cast(JSObject::Cast(holder)->GetElements().GetTaggedObject());

        if (!hclass->IsDictionaryElement()) {
            ASSERT(!elements->IsDictionaryMode());
            if (index < elements->GetLength()) {
                JSTaggedValue value = elements->Get(index);
                if (!value.IsHole()) {
                    SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ARRAY_ACCESS);
                    return value;
                }
            } else {
                SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ACCESS);
                return JSTaggedValue::Hole();
            }
        } else {
            SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ACCESS);
            NumberDictionary *dict = NumberDictionary::Cast(elements);
            int entry = dict->FindEntry(JSTaggedValue(static_cast<int>(index)));
            if (entry != -1) {
                auto attr = dict->GetAttributes(entry);
                auto value = dict->GetValue(entry);
                if (UNLIKELY(attr.IsAccessor())) {
                    return CallGetter(thread, receiver, holder, value);
                }
                ASSERT(!value.IsAccessor());
                return value;
            }
        }
        if (USE_OWN) {
            break;
        }
        holder = JSObject::Cast(holder)->GetJSHClass()->GetPrototype();
    } while (holder.IsHeapObject());
    SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ACCESS);
    // not found
    return JSTaggedValue::Undefined();
}

template <bool USE_OWN>
JSTaggedValue FastRuntimeStub::GetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                  JSHandle<JSTaggedValue> key)
{
    INTERPRETER_TRACE(thread, GetPropertyByValue);
    if (UNLIKELY(!key->IsNumber() && !key->IsStringOrSymbol())) {
        return JSTaggedValue::Hole();
    }
    // fast path
    auto index = TryToElementsIndex(key.GetTaggedValue());
    if (LIKELY(index < JSObject::MAX_ELEMENT_INDEX)) {
        return GetPropertyByIndex<USE_OWN>(thread, receiver.GetTaggedValue(), index);
    }
    if (!key->IsNumber()) {
        JSTaggedValue raw_key = key.GetTaggedValue();
        if (raw_key.IsString() && !EcmaString::Cast(raw_key.GetTaggedObject())->IsInternString()) {
            // update string stable
            raw_key = JSTaggedValue(thread->GetEcmaVM()->GetFactory()->InternString(key));
        }
        return FastRuntimeStub::GetPropertyByName<USE_OWN>(thread, receiver.GetTaggedValue(), raw_key);
    }
    return JSTaggedValue::Hole();
}

template <bool USE_OWN>
JSTaggedValue FastRuntimeStub::GetPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key)
{
    INTERPRETER_TRACE(thread, GetPropertyByName);
    // no gc when return hole
    ASSERT(key.IsStringOrSymbol());
    JSTaggedValue holder = receiver;
    do {
        auto *hclass = holder.GetTaggedObject()->GetClass();
        JSType js_type = hclass->GetObjectType();
        if (IsSpecialIndexedObj(js_type)) {
            return JSTaggedValue::Hole();
        }

        if (LIKELY(!hclass->IsDictionaryMode())) {
            ASSERT(!TaggedArray::Cast(JSObject::Cast(holder)->GetProperties().GetTaggedObject())->IsDictionaryMode());

            LayoutInfo *layout_info = LayoutInfo::Cast(hclass->GetLayout().GetTaggedObject());
            int props_number = hclass->NumberOfProps();
            int entry = layout_info->FindElementWithCache(thread, hclass, key, props_number);
            if (entry != -1) {
                PropertyAttributes attr(layout_info->GetAttr(entry));
                ASSERT(static_cast<int>(attr.GetOffset()) == entry);
                auto value = JSObject::Cast(holder)->GetProperty(hclass, attr);
                if (UNLIKELY(attr.IsAccessor())) {
                    return CallGetter(thread, receiver, holder, value);
                }
                ASSERT(!value.IsAccessor());
                return value;
            }
        } else {
            TaggedArray *array = TaggedArray::Cast(JSObject::Cast(holder)->GetProperties().GetTaggedObject());
            ASSERT(array->IsDictionaryMode());
            NameDictionary *dict = NameDictionary::Cast(array);
            int entry = dict->FindEntry(key);
            if (entry != -1) {
                auto value = dict->GetValue(entry);
                auto attr = dict->GetAttributes(entry);
                if (UNLIKELY(attr.IsAccessor())) {
                    return CallGetter(thread, receiver, holder, value);
                }
                ASSERT(!value.IsAccessor());
                return value;
            }
        }
        if (USE_OWN) {
            break;
        }
        holder = hclass->GetPrototype();
    } while (holder.IsHeapObject());
    // not found
    return JSTaggedValue::Undefined();
}

template <bool USE_OWN>
JSTaggedValue FastRuntimeStub::SetPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                                 JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, SetPropertyByName);
    // property
    JSTaggedValue holder = receiver;
    do {
        auto *hclass = holder.GetTaggedObject()->GetClass();
        JSType js_type = hclass->GetObjectType();
        if (IsSpecialIndexedObj(js_type)) {
            if (IsSpecialContainer(js_type)) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot set property on Container", JSTaggedValue::Exception());
            }
            return JSTaggedValue::Hole();
        }
        // UpdateRepresentation
        if (LIKELY(!hclass->IsDictionaryMode())) {
            ASSERT(!TaggedArray::Cast(JSObject::Cast(holder)->GetProperties().GetTaggedObject())->IsDictionaryMode());

            LayoutInfo *layout_info = LayoutInfo::Cast(hclass->GetLayout().GetTaggedObject());

            int props_number = hclass->NumberOfProps();
            int entry = layout_info->FindElementWithCache(thread, hclass, key, props_number);
            if (entry != -1) {
                PropertyAttributes attr(layout_info->GetAttr(entry));
                ASSERT(static_cast<int>(attr.GetOffset()) == entry);
                if (UNLIKELY(attr.IsAccessor())) {
                    auto accessor = JSObject::Cast(holder)->GetProperty(hclass, attr);
                    if (ShouldCallSetter(receiver, holder, accessor, attr)) {
                        return CallSetter(thread, receiver, value, accessor);
                    }
                }
                if (UNLIKELY(!attr.IsWritable())) {
                    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot set readonly property", JSTaggedValue::Exception());
                }
                if (UNLIKELY(holder != receiver)) {
                    break;
                }
                JSObject::Cast(holder)->SetProperty(thread, hclass, attr, value);
                return JSTaggedValue::Undefined();
            }
        } else {
            TaggedArray *properties = TaggedArray::Cast(JSObject::Cast(holder)->GetProperties().GetTaggedObject());
            ASSERT(properties->IsDictionaryMode());
            NameDictionary *dict = NameDictionary::Cast(properties);
            int entry = dict->FindEntry(key);
            if (entry != -1) {
                auto attr = dict->GetAttributes(entry);
                if (UNLIKELY(attr.IsAccessor())) {
                    auto accessor = dict->GetValue(entry);
                    if (ShouldCallSetter(receiver, holder, accessor, attr)) {
                        return CallSetter(thread, receiver, value, accessor);
                    }
                }
                if (UNLIKELY(!attr.IsWritable())) {
                    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot set readonly property", JSTaggedValue::Exception());
                }
                if (UNLIKELY(holder != receiver)) {
                    break;
                }
                dict->UpdateValue(thread, entry, value);
                return JSTaggedValue::Undefined();
            }
        }
        if (USE_OWN) {
            break;
        }
        holder = hclass->GetPrototype();
    } while (holder.IsHeapObject());

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSObject> obj_handle(thread, receiver);
    JSHandle<JSTaggedValue> key_handle(thread, key);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    if (UNLIKELY(!JSObject::Cast(receiver)->IsExtensible())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot add property in prevent extensions ", JSTaggedValue::Exception());
    }
    AddPropertyByName(thread, obj_handle, key_handle, value_handle, PropertyAttributes::Default());
    return JSTaggedValue::Undefined();
}

template <bool USE_OWN>
JSTaggedValue FastRuntimeStub::SetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                  JSTaggedValue value, void *prof)
{
    INTERPRETER_TRACE(thread, SetPropertyByIndex);
    JSTaggedValue holder = receiver;
    do {
        auto *hclass = holder.GetTaggedObject()->GetClass();
        JSType js_type = hclass->GetObjectType();
        if (IsSpecialIndexedObj(js_type)) {
            SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ACCESS);

            if (IsSpecialContainer(js_type)) {
                return SetContainerProperty(thread, holder, index, value, js_type);
            }
            return JSTaggedValue::Hole();
        }
        TaggedArray *elements = TaggedArray::Cast(JSObject::Cast(holder)->GetElements().GetTaggedObject());
        if (!hclass->IsDictionaryElement()) {
            ASSERT(!elements->IsDictionaryMode());
            if (UNLIKELY(holder != receiver)) {
                break;
            }
            if (index < elements->GetLength()) {
                if (!elements->Get(index).IsHole()) {
                    elements->Set(thread, index, value);
                    SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ARRAY_ACCESS);
                    return JSTaggedValue::Undefined();
                }
            }
        } else {
            SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ACCESS);
            return JSTaggedValue::Hole();
        }
        if (USE_OWN) {
            break;
        }
        holder = JSObject::Cast(holder)->GetJSHClass()->GetPrototype();
    } while (holder.IsHeapObject());

    SET_PROFILING_ACCESS(ProfilingIndexedAccessBits::OBJECT_ACCESS);
    return AddPropertyByIndex(thread, receiver, index, value);
}

template <bool USE_OWN>
JSTaggedValue FastRuntimeStub::SetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                  JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> value)
{
    INTERPRETER_TRACE(thread, SetPropertyByValue);
    if (UNLIKELY(!key->IsNumber() && !key->IsStringOrSymbol())) {
        return JSTaggedValue::Hole();
    }
    // fast path
    auto index = TryToElementsIndex(key.GetTaggedValue());
    if (LIKELY(index < JSObject::MAX_ELEMENT_INDEX)) {
        return SetPropertyByIndex<USE_OWN>(thread, receiver.GetTaggedValue(), index, value.GetTaggedValue());
    }
    if (!key->IsNumber()) {
        JSTaggedValue raw_key = key.GetTaggedValue();
        if (raw_key.IsString() && !EcmaString::Cast(raw_key.GetTaggedObject())->IsInternString()) {
            // update string stable
            raw_key = JSTaggedValue(thread->GetEcmaVM()->GetFactory()->InternString(key));
        }
        return FastRuntimeStub::SetPropertyByName<USE_OWN>(thread, receiver.GetTaggedValue(), raw_key,
                                                           value.GetTaggedValue());
    }
    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::GetGlobalOwnProperty(JSTaggedValue receiver, JSTaggedValue key, bool *found)
{
    JSObject *obj = JSObject::Cast(receiver);
    TaggedArray *properties = TaggedArray::Cast(obj->GetProperties().GetTaggedObject());
    GlobalDictionary *dict = GlobalDictionary::Cast(properties);
    int entry = dict->FindEntry(key);
    if (entry != -1) {
        *found = true;
        return dict->GetValue(entry);
    }

    return JSTaggedValue::Undefined();
}

JSTaggedValue FastRuntimeStub::FastTypeOf(JSThread *thread, JSTaggedValue obj)
{
    INTERPRETER_TRACE(thread, FastTypeOf);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    switch (obj.GetRawData()) {
        case JSTaggedValue::VALUE_TRUE:
        case JSTaggedValue::VALUE_FALSE:
            return global_const->GetBooleanString();
        case JSTaggedValue::VALUE_NULL:
            return global_const->GetObjectString();
        case JSTaggedValue::VALUE_UNDEFINED:
            return global_const->GetUndefinedString();
        default:
            if (obj.IsHeapObject()) {
                if (obj.IsString()) {
                    return global_const->GetStringString();
                }
                if (obj.IsSymbol()) {
                    return global_const->GetSymbolString();
                }
                if (obj.IsBigInt()) {
                    return global_const->GetBigIntString();
                }
                if (obj.IsCallable()) {
                    return global_const->GetFunctionString();
                }
                return global_const->GetObjectString();
            }
            if (obj.IsNumber()) {
                return global_const->GetNumberString();
            }
    }
    return global_const->GetUndefinedString();
}

bool FastRuntimeStub::FastSetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                             JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, FastSetPropertyByIndex);
    JSTaggedType raw_receiver = receiver.GetRawData();
    JSTaggedType raw_value = value.GetRawData();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
    JSTaggedValue result = FastRuntimeStub::SetPropertyByIndex(thread, receiver, index, value);
    if (!result.IsHole()) {
        return result != JSTaggedValue::Exception();
    }
    ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    receiver = JSTaggedValue(raw_receiver);
    value = JSTaggedValue(raw_value);
    return JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>(thread, receiver), index,
                                      JSHandle<JSTaggedValue>(thread, value), true);
}

bool FastRuntimeStub::FastSetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                             JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> value)
{
    INTERPRETER_TRACE(thread, FastSetPropertyByValue);
    JSTaggedValue result = FastRuntimeStub::SetPropertyByValue(thread, receiver, key, value);
    if (!result.IsHole()) {
        return result != JSTaggedValue::Exception();
    }
    return JSTaggedValue::SetProperty(thread, receiver, key, value, true);
}

// must not use for interpreter
JSTaggedValue FastRuntimeStub::FastGetPropertyByName(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                     JSHandle<JSTaggedValue> key)
{
    INTERPRETER_TRACE(thread, FastGetPropertyByName);
    ASSERT(key->IsStringOrSymbol());
    JSTaggedType raw_key = key->GetRawData();
    if (key->IsString() && !EcmaString::Cast(key->GetTaggedObject())->IsInternString()) {
        raw_key = ToUintPtr(thread->GetEcmaVM()->GetFactory()->InternString(key));
    }
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
    JSTaggedValue result =
        FastRuntimeStub::GetPropertyByName(thread, receiver.GetTaggedValue(), JSTaggedValue(raw_key));
    if (result.IsHole()) {
        ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        return JSTaggedValue::GetProperty(thread, receiver, JSHandle<JSTaggedValue>(thread, JSTaggedValue(raw_key)))
            .GetValue()
            .GetTaggedValue();
    }
    return result;
}

JSTaggedValue FastRuntimeStub::FastGetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                      JSHandle<JSTaggedValue> key)
{
    INTERPRETER_TRACE(thread, FastGetPropertyByValue);
    JSTaggedValue result = FastRuntimeStub::GetPropertyByValue(thread, receiver, key);
    if (result.IsHole()) {
        return JSTaggedValue::GetProperty(thread, receiver, key).GetValue().GetTaggedValue();
    }
    return result;
}

template <bool USE_HOLE>  // UseHole is only for Array::Sort() which requires Hole order
JSTaggedValue FastRuntimeStub::FastGetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index)
{
    INTERPRETER_TRACE(thread, FastGetPropertyByIndex);
    JSTaggedType raw_receiver = receiver.GetRawData();
    [[maybe_unused]] size_t gc = thread->GetEcmaVM()->GetGC()->GetCounter();
    JSTaggedValue result = FastRuntimeStub::GetPropertyByIndex(thread, receiver, index);
    if (result.IsHole() && !USE_HOLE) {
        ASSERT_PRINT(gc == thread->GetEcmaVM()->GetGC()->GetCounter(), "GC happend where it is not supposed");
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        receiver = JSTaggedValue(raw_receiver);
        return JSTaggedValue::GetProperty(thread, JSHandle<JSTaggedValue>(thread, receiver), index)
            .GetValue()
            .GetTaggedValue();
    }
    return result;
}

JSTaggedValue FastRuntimeStub::NewLexicalEnvDyn(JSThread *thread, ObjectFactory *factory, uint16_t num_vars)
{
    INTERPRETER_TRACE(thread, NewLexicalEnvDyn);
    LexicalEnv *new_env = factory->InlineNewLexicalEnv(num_vars);
    if (UNLIKELY(new_env == nullptr)) {
        return JSTaggedValue::Hole();
    }
    return JSTaggedValue(new_env);
}

// Those interface below is discarded
bool FastRuntimeStub::IsSpecialIndexedObjForGet(JSTaggedValue obj)
{
    JSType js_type = obj.GetTaggedObject()->GetClass()->GetObjectType();
    return js_type > JSType::JS_ARRAY && js_type <= JSType::JS_PRIMITIVE_REF;
}

bool FastRuntimeStub::IsSpecialIndexedObjForSet(JSTaggedValue obj)
{
    JSType js_type = obj.GetTaggedObject()->GetClass()->GetObjectType();
    return js_type >= JSType::JS_ARRAY && js_type <= JSType::JS_PRIMITIVE_REF;
}

JSTaggedValue FastRuntimeStub::GetElement(JSTaggedValue receiver, uint32_t index)
{
    JSTaggedValue holder = receiver;
    while (true) {
        JSTaggedValue val = FindOwnElement(JSObject::Cast(holder), index);
        if (!val.IsHole()) {
            return val;
        }

        holder = JSObject::Cast(holder)->GetJSHClass()->GetPrototype();
        if (!holder.IsHeapObject()) {
            return JSTaggedValue::Undefined();
        }

        if (UNLIKELY(holder.IsJSProxy())) {
            return JSTaggedValue::Hole();
        }
    }
}

JSTaggedValue FastRuntimeStub::GetElementWithArray(JSTaggedValue receiver, uint32_t index)
{
    JSTaggedValue holder = receiver;
    while (true) {
        JSTaggedValue val = FindOwnElement(JSObject::Cast(holder), index);
        if (!val.IsHole()) {
            return val;
        }

        holder = JSObject::Cast(holder)->GetJSHClass()->GetPrototype();
        if (!holder.IsHeapObject()) {
            return val;
        }
    }
}

bool FastRuntimeStub::SetElement(JSThread *thread, JSTaggedValue receiver, uint32_t index, JSTaggedValue value,
                                 bool may_throw)
{
    INTERPRETER_TRACE(thread, SetElement);
    JSTaggedValue holder = receiver;
    bool on_prototype = false;

    while (true) {
        PropertyAttributes attr;
        uint32_t index_or_entry = 0;
        TaggedArray *elements = TaggedArray::Cast(JSObject::Cast(holder)->GetElements().GetHeapObject());
        bool is_dict = elements->IsDictionaryMode();
        JSTaggedValue val = FindOwnElement(elements, index, is_dict, &attr, &index_or_entry);
        if (!val.IsHole()) {
            if (UNLIKELY(on_prototype)) {
                if (UNLIKELY(!JSObject::Cast(receiver)->IsExtensible())) {
                    if (may_throw) {
                        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot add property in prevent extensions ", false);
                    }
                    return false;
                }

                return JSObject::AddElementInternal(thread, JSHandle<JSObject>(thread, receiver), index,
                                                    JSHandle<JSTaggedValue>(thread, value),
                                                    PropertyAttributes::Default());
            }
            if (!attr.IsAccessor()) {
                if (attr.IsWritable()) {
                    elements = TaggedArray::Cast(JSObject::Cast(receiver)->GetElements().GetHeapObject());
                    if (!is_dict) {
                        elements->Set(thread, index_or_entry, value);
                        JSObject::Cast(receiver)->GetJSHClass()->UpdateRepresentation(value);
                        return true;
                    }
                    NumberDictionary::Cast(elements)->UpdateValueAndAttributes(thread, index_or_entry, value, attr);
                    return true;
                }

                if (may_throw) {
                    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot set readonly property", false);
                }
                return false;
            }

            // Accessor
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            JSHandle<JSTaggedValue> obj_handle(thread, receiver);
            JSHandle<JSTaggedValue> value_handle(thread, value);
            AccessorData *access = AccessorData::Cast(val.GetHeapObject());
            return JSObject::CallSetter(thread, *access, obj_handle, value_handle, may_throw);
        }

        holder = JSObject::Cast(holder)->GetJSHClass()->GetPrototype();
        if (!holder.IsHeapObject()) {
            if (UNLIKELY(!JSObject::Cast(receiver)->IsExtensible())) {
                if (may_throw) {
                    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot add property in prevent extensions ", false);
                }
                return false;
            }

            return JSObject::AddElementInternal(thread, JSHandle<JSObject>(thread, receiver), index,
                                                JSHandle<JSTaggedValue>(thread, value), PropertyAttributes::Default());
        }
        if (holder.IsJSProxy()) {
            return JSProxy::SetProperty(
                thread, JSHandle<JSProxy>(thread, holder), JSHandle<JSTaggedValue>(thread, JSTaggedValue(index)),
                JSHandle<JSTaggedValue>(thread, value), JSHandle<JSTaggedValue>(thread, receiver), may_throw);
        }
        on_prototype = true;
    }
}

bool FastRuntimeStub::SetPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                        JSTaggedValue value, bool may_throw)
{
    INTERPRETER_TRACE(thread, SetPropertyByName);
    // property
    JSTaggedValue holder = receiver;
    bool on_prototype = false;

    while (true) {
        TaggedArray *properties = TaggedArray::Cast(JSObject::Cast(holder)->GetProperties().GetHeapObject());
        PropertyAttributes attr;
        uint32_t index_or_entry = 0;
        JSTaggedValue val = FindOwnProperty(thread, JSObject::Cast(holder), properties, key, &attr, &index_or_entry);
        if (!val.IsHole()) {
            if (!attr.IsAccessor()) {
                if (UNLIKELY(on_prototype)) {
                    if (UNLIKELY(!JSObject::Cast(receiver)->IsExtensible() || !attr.IsWritable())) {
                        if (may_throw) {
                            THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot add property in prevent extensions ", false);
                        }
                        return false;
                    }
                    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                    ObjectOperator::FastAdd(thread, receiver, key, JSHandle<JSTaggedValue>(thread, value),
                                            PropertyAttributes::Default());

                    return true;
                }

                if (attr.IsWritable()) {
                    properties = TaggedArray::Cast(JSObject::Cast(receiver)->GetProperties().GetHeapObject());
                    if (!properties->IsDictionaryMode()) {
                        Representation representation =
                            PropertyAttributes::UpdateRepresentation(attr.GetRepresentation(), value);
                        if (attr.GetRepresentation() != representation) {
                            attr.SetRepresentation(representation);
                        }

                        JSObject::Cast(receiver)->GetJSHClass()->UpdatePropertyMetaData(thread, key, attr);
                        if (UNLIKELY(val.IsInternalAccessor())) {
                            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                            AccessorData::Cast(val.GetHeapObject())
                                ->CallInternalSet(thread, JSHandle<JSObject>(thread, receiver),
                                                  JSHandle<JSTaggedValue>(thread, value), may_throw);
                            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
                            return true;
                        }

                        if (attr.IsInlinedProps()) {
                            JSObject::Cast(receiver)->SetPropertyInlinedProps(thread, index_or_entry, value);
                        } else {
                            properties->Set(thread, index_or_entry, value);
                        }
                        return true;
                    }

                    if (receiver.IsJSGlobalObject()) {
                        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                        JSHandle<GlobalDictionary> dict_handle(thread, properties);
                        // globalobj have no internal accessor
                        GlobalDictionary::InvalidatePropertyBox(thread, dict_handle, index_or_entry, attr);
                        return true;
                    }

                    if (UNLIKELY(val.IsInternalAccessor())) {
                        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                        AccessorData::Cast(val.GetHeapObject())
                            ->CallInternalSet(thread, JSHandle<JSObject>(thread, receiver),
                                              JSHandle<JSTaggedValue>(thread, value), may_throw);
                        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
                        return true;
                    }

                    NameDictionary::Cast(properties)->UpdateValueAndAttributes(thread, index_or_entry, value, attr);
                    return true;
                }

                if (may_throw) {
                    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot set readonly property", false);
                }
                return false;
            }

            // Accessor
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            JSHandle<JSTaggedValue> obj_handle(thread, receiver);
            JSHandle<JSTaggedValue> value_handle(thread, value);
            AccessorData *access = AccessorData::Cast(val.GetHeapObject());
            return JSObject::CallSetter(thread, *access, obj_handle, value_handle, may_throw);
        }

        if (holder.IsTypedArray()) {
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            JSHandle<JSTaggedValue> holder_handle(thread, holder);
            JSHandle<JSTaggedValue> value_handle(thread, value);
            JSHandle<JSTaggedValue> receiver_handle(thread, receiver);
            JSHandle<JSTaggedValue> key_handle = JSTypedArray::ToPropKey(thread, JSHandle<JSTaggedValue>(thread, key));
            return JSTypedArray::SetProperty(thread, holder_handle, key_handle, value_handle, receiver_handle,
                                             may_throw);
        }

        holder = JSObject::Cast(holder)->GetJSHClass()->GetPrototype();
        if (!holder.IsHeapObject()) {
            if (UNLIKELY(!JSObject::Cast(receiver)->IsExtensible())) {
                if (may_throw) {
                    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot add property in prevent extensions ", false);
                }
                return false;
            }
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            ObjectOperator::FastAdd(thread, receiver, key, JSHandle<JSTaggedValue>(thread, value),
                                    PropertyAttributes::Default());

            return true;
        }
        if (holder.IsJSProxy()) {
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            return JSProxy::SetProperty(thread, JSHandle<JSProxy>(thread, holder), JSHandle<JSTaggedValue>(thread, key),
                                        JSHandle<JSTaggedValue>(thread, value),
                                        JSHandle<JSTaggedValue>(thread, receiver), may_throw);
        }
        on_prototype = true;
    }
}

bool FastRuntimeStub::SetGlobalOwnProperty(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                           JSTaggedValue value, bool may_throw)
{
    INTERPRETER_TRACE(thread, SetGlobalOwnProperty);
    uint32_t index = 0;
    if (JSTaggedValue::ToElementIndex(key, &index)) {
        return SetElement(thread, receiver, index, value, may_throw);
    }

    JSObject *obj = JSObject::Cast(receiver);
    GlobalDictionary *dict = GlobalDictionary::Cast(obj->GetProperties().GetTaggedObject());
    PropertyAttributes attr = PropertyAttributes::Default();
    if (UNLIKELY(dict->GetLength() == 0)) {
        JSHandle<JSTaggedValue> key_handle(thread, key);
        JSHandle<JSTaggedValue> val_handle(thread, value);
        JSHandle<JSObject> obj_handle(thread, obj);
        JSHandle<GlobalDictionary> dict_handle(GlobalDictionary::Create(thread));

        // Add PropertyBox to global dictionary
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        JSHandle<PropertyBox> box_handle = factory->NewPropertyBox(val_handle);
        box_handle->SetValue(thread, val_handle.GetTaggedValue());
        PropertyBoxType box_type = val_handle->IsUndefined() ? PropertyBoxType::UNDEFINED : PropertyBoxType::CONSTANT;
        attr.SetBoxType(box_type);

        JSHandle<GlobalDictionary> properties =
            GlobalDictionary::PutIfAbsent(thread, dict_handle, key_handle, JSHandle<JSTaggedValue>(box_handle), attr);
        obj_handle->SetProperties(thread, properties);
        return true;
    }

    int entry = dict->FindEntry(key);
    if (entry != -1) {
        attr = dict->GetAttributes(entry);
        JSTaggedValue val = dict->GetValue(entry);
        if (!attr.IsAccessor()) {
            if (attr.IsWritable()) {
                // globalobj have no internal accessor
                JSHandle<GlobalDictionary> dict_handle(thread, dict);
                GlobalDictionary::InvalidatePropertyBox(thread, dict_handle, entry, attr);
                return true;
            }
        }

        // Accessor
        JSTaggedValue setter = AccessorData::Cast(val.GetHeapObject())->GetSetter();
        if (setter.IsUndefined()) {
            if (may_throw) {
                THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot set property when setter is undefined", false);
            }
            return false;
        }

        JSHandle<JSTaggedValue> obj_handle(thread, receiver);
        JSHandle<JSTaggedValue> set_func(thread, setter);

        auto info = NewRuntimeCallInfo(thread, set_func, obj_handle, JSTaggedValue::Undefined(), 1);
        info->SetCallArgs(value);
        JSFunction::Call(info.Get());
        // 10. ReturnIfAbrupt(setterResult).
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);
        return true;
    }

    JSHandle<JSTaggedValue> key_handle(thread, key);
    JSHandle<JSTaggedValue> val_handle(thread, value);
    JSHandle<JSObject> obj_handle(thread, obj);
    JSHandle<GlobalDictionary> dict_handle(thread, dict);

    // Add PropertyBox to global dictionary
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<PropertyBox> box_handle = factory->NewPropertyBox(key_handle);
    box_handle->SetValue(thread, val_handle.GetTaggedValue());
    PropertyBoxType box_type = val_handle->IsUndefined() ? PropertyBoxType::UNDEFINED : PropertyBoxType::CONSTANT;
    attr.SetBoxType(box_type);

    JSHandle<GlobalDictionary> properties =
        GlobalDictionary::PutIfAbsent(thread, dict_handle, key_handle, JSHandle<JSTaggedValue>(box_handle), attr);
    obj_handle->SetProperties(thread, properties);
    return true;
}

// set property that is not accessor and is writable
void FastRuntimeStub::SetOwnPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                           JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, SetOwnPropertyByName);
    TaggedArray *properties = TaggedArray::Cast(JSObject::Cast(receiver)->GetProperties().GetHeapObject());
    PropertyAttributes attr;
    uint32_t index_or_entry = 0;
    JSTaggedValue val = FindOwnProperty(thread, JSObject::Cast(receiver), properties, key, &attr, &index_or_entry);
    if (!val.IsHole()) {
        ASSERT(!attr.IsAccessor() && attr.IsWritable());
        if (!properties->IsDictionaryMode()) {
            Representation representation = PropertyAttributes::UpdateRepresentation(attr.GetRepresentation(), value);
            if (attr.GetRepresentation() != representation) {
                attr.SetRepresentation(representation);
            }

            JSObject::Cast(receiver)->GetJSHClass()->UpdatePropertyMetaData(thread, key, attr);

            if (attr.IsInlinedProps()) {
                JSObject::Cast(receiver)->SetPropertyInlinedProps(thread, index_or_entry, value);
            } else {
                properties->Set(thread, index_or_entry, value);
            }
            return;
        }

        NameDictionary::Cast(properties)->UpdateValueAndAttributes(thread, index_or_entry, value, attr);
        return;
    }
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectOperator::FastAdd(thread, receiver, key, JSHandle<JSTaggedValue>(thread, value),
                            PropertyAttributes::Default());
}

// set element that is not accessor and is writable
bool FastRuntimeStub::SetOwnElement(JSThread *thread, JSTaggedValue receiver, uint32_t index, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, SetOwnElement);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    PropertyAttributes attr;
    uint32_t index_or_entry = 0;
    TaggedArray *elements = TaggedArray::Cast(JSObject::Cast(receiver)->GetElements().GetHeapObject());
    bool is_dict = elements->IsDictionaryMode();
    [[maybe_unused]] JSTaggedValue val = FindOwnElement(elements, index, is_dict, &attr, &index_or_entry);
    if (!val.IsHole()) {
        ASSERT(!attr.IsAccessor() && attr.IsWritable());
        if (!is_dict) {
            elements->Set(thread, index_or_entry, value);
            JSObject::Cast(receiver)->GetJSHClass()->UpdateRepresentation(value);
            return true;
        }
        NumberDictionary::Cast(elements)->UpdateValueAndAttributes(thread, index_or_entry, value, attr);
        return true;
    }

    return JSObject::AddElementInternal(thread, JSHandle<JSObject>(thread, receiver), index,
                                        JSHandle<JSTaggedValue>(thread, value), PropertyAttributes::Default());
}

bool FastRuntimeStub::FastSetProperty(JSThread *thread, JSHandle<JSTaggedValue> receiver, JSHandle<JSTaggedValue> key,
                                      JSHandle<JSTaggedValue> value, bool may_throw)
{
    INTERPRETER_TRACE(thread, FastSetProperty);
    if (receiver->IsJSObject() && !receiver->IsTypedArray() && (key->IsStringOrSymbol())) {
        uint32_t index = 0;
        if (UNLIKELY(JSTaggedValue::ToElementIndex(key.GetTaggedValue(), &index))) {
            if (!FastRuntimeStub::IsSpecialIndexedObjForSet(receiver.GetTaggedValue())) {
                return FastRuntimeStub::SetElement(thread, receiver.GetTaggedValue(), index, value.GetTaggedValue(),
                                                   true);
            }
            return JSTaggedValue::SetProperty(thread, receiver, key, value, may_throw);
        }
        JSTaggedValue raw_key = key.GetTaggedValue();
        if (raw_key.IsString()) {
            raw_key = JSTaggedValue(thread->GetEcmaVM()->GetFactory()->InternString(key));
        }
        return FastRuntimeStub::SetPropertyByName(thread, receiver.GetTaggedValue(), raw_key, value.GetTaggedValue(),
                                                  may_throw);
    }
    return JSTaggedValue::SetProperty(thread, receiver, key, value, may_throw);
}

JSTaggedValue FastRuntimeStub::FastGetProperty(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                               JSHandle<JSTaggedValue> key)
{
    INTERPRETER_TRACE(thread, FastGetProperty);
    JSTaggedValue result;

    if (receiver->IsJSObject() && !receiver->IsTypedArray() && (key->IsStringOrSymbol())) {
        uint32_t index = 0;
        if (UNLIKELY(JSTaggedValue::ToElementIndex(key.GetTaggedValue(), &index))) {
            if (FastRuntimeStub::IsSpecialIndexedObjForSet(receiver.GetTaggedValue())) {
                result = JSTaggedValue::Hole();
            } else {
                result = FastRuntimeStub::GetElement(receiver.GetTaggedValue(), index);
            }
        } else {
            JSTaggedValue raw_key = key.GetTaggedValue();
            if (raw_key.IsString()) {
                raw_key = JSTaggedValue(thread->GetEcmaVM()->GetFactory()->InternString(key));
            }
            result = FastRuntimeStub::GetPropertyByName(thread, receiver.GetTaggedValue(), raw_key);
        }
    }
    if (!result.IsHole()) {
        if (UNLIKELY(result.IsAccessor())) {
            return JSObject::CallGetter(thread, AccessorData::Cast(result.GetHeapObject()), receiver);
        }
        return result;
    }
    return JSTaggedValue::GetProperty(thread, receiver, key).GetValue().GetTaggedValue();
}

JSTaggedValue FastRuntimeStub::FindOwnProperty(JSThread *thread, JSObject *obj, TaggedArray *properties,
                                               JSTaggedValue key, PropertyAttributes *attr, uint32_t *index_or_entry)
{
    INTERPRETER_TRACE(thread, FindOwnProperty);
    if (!properties->IsDictionaryMode()) {
        JSHClass *cls = obj->GetJSHClass();
        JSTaggedValue attrs = cls->GetLayout();
        if (!attrs.IsNull()) {
            LayoutInfo *layout_info = LayoutInfo::Cast(attrs.GetHeapObject());
            int prop_number = cls->NumberOfProps();
            int entry = layout_info->FindElementWithCache(thread, cls, key, prop_number);
            if (entry != -1) {
                *attr = layout_info->GetAttr(entry);
                ASSERT(entry == static_cast<int>(attr->GetOffset()));
                *index_or_entry = entry;
                if (attr->IsInlinedProps()) {
                    return obj->GetPropertyInlinedProps(entry);
                }
                *index_or_entry -= cls->GetInlinedProperties();
                return properties->Get(*index_or_entry);
            }
        }
        return JSTaggedValue::Hole();  // properties == empty properties will return here.
    }

    if (obj->IsJSGlobalObject()) {
        GlobalDictionary *dict = GlobalDictionary::Cast(properties);
        int entry = dict->FindEntry(key);
        if (entry != -1) {
            *index_or_entry = entry;
            *attr = dict->GetAttributes(entry);
            return dict->GetValue(entry);
        }
        return JSTaggedValue::Hole();
    }

    NameDictionary *dict = NameDictionary::Cast(properties);
    int entry = dict->FindEntry(key);
    if (entry != -1) {
        *index_or_entry = entry;
        *attr = dict->GetAttributes(entry);
        return dict->GetValue(entry);
    }

    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FindOwnElement(TaggedArray *elements, uint32_t index, bool is_dict,
                                              PropertyAttributes *attr, uint32_t *index_or_entry)
{
    if (!is_dict) {
        if (elements->GetLength() <= index) {
            return JSTaggedValue::Hole();
        }

        JSTaggedValue value = elements->Get(index);
        if (!value.IsHole()) {
            *attr = PropertyAttributes::Default();
            *index_or_entry = index;
            return value;
        }
    } else {
        NumberDictionary *dict = NumberDictionary::Cast(elements);
        int entry = dict->FindEntry(JSTaggedValue(static_cast<int>(index)));
        if (entry != -1) {
            *index_or_entry = entry;
            *attr = dict->GetAttributes(entry);
            return dict->GetValue(entry);
        }
    }
    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FindOwnProperty(JSThread *thread, JSObject *obj, JSTaggedValue key)
{
    INTERPRETER_TRACE(thread, FindOwnProperty);
    TaggedArray *array = TaggedArray::Cast(obj->GetProperties().GetHeapObject());
    if (!array->IsDictionaryMode()) {
        JSHClass *cls = obj->GetJSHClass();
        JSTaggedValue attrs = cls->GetLayout();
        if (!attrs.IsNull()) {
            LayoutInfo *layout_info = LayoutInfo::Cast(attrs.GetHeapObject());
            int props_number = cls->NumberOfProps();
            int entry = layout_info->FindElementWithCache(thread, cls, key, props_number);
            if (entry != -1) {
                PropertyAttributes attr(layout_info->GetAttr(entry));
                ASSERT(static_cast<int>(attr.GetOffset()) == entry);
                return attr.IsInlinedProps() ? obj->GetPropertyInlinedProps(entry)
                                             : array->Get(entry - cls->GetInlinedProperties());
            }
        }
        return JSTaggedValue::Hole();  // array == empty array will return here.
    }

    if (obj->IsJSGlobalObject()) {
        GlobalDictionary *dict = GlobalDictionary::Cast(array);
        int entry = dict->FindEntry(key);
        if (entry != -1) {
            return dict->GetValue(entry);
        }
        return JSTaggedValue::Hole();
    }

    NameDictionary *dict = NameDictionary::Cast(array);
    int entry = dict->FindEntry(key);
    if (entry != -1) {
        return dict->GetValue(entry);
    }

    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::FindOwnElement(JSObject *obj, uint32_t index)
{
    TaggedArray *elements = TaggedArray::Cast(JSObject::Cast(obj)->GetElements().GetHeapObject());

    if (!elements->IsDictionaryMode()) {
        if (elements->GetLength() <= index) {
            return JSTaggedValue::Hole();
        }

        JSTaggedValue value = elements->Get(index);
        if (!value.IsHole()) {
            return value;
        }
    } else {
        NumberDictionary *dict = NumberDictionary::Cast(elements);
        int entry = dict->FindEntry(JSTaggedValue(static_cast<int>(index)));
        if (entry != -1) {
            return dict->GetValue(entry);
        }
    }
    return JSTaggedValue::Hole();
}

JSTaggedValue FastRuntimeStub::HasOwnProperty(JSThread *thread, JSObject *obj, JSTaggedValue key)
{
    INTERPRETER_TRACE(thread, HasOwnProperty);
    uint32_t index = 0;
    if (UNLIKELY(JSTaggedValue::ToElementIndex(key, &index))) {
        return FastRuntimeStub::FindOwnElement(obj, index);
    }

    return FastRuntimeStub::FindOwnProperty(thread, obj, key);
}

JSTaggedValue FastRuntimeStub::GetContainerProperty(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                    JSType js_type)
{
    JSTaggedValue res = JSTaggedValue::Undefined();
    switch (js_type) {
        case JSType::JS_ARRAY_LIST:
            res = JSArrayList::Cast(receiver.GetTaggedObject())->Get(thread, index);
            break;
        default:
            break;
    }
    return res;
}

JSTaggedValue FastRuntimeStub::SetContainerProperty(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                    JSTaggedValue value, JSType js_type)
{
    JSTaggedValue res = JSTaggedValue::Undefined();
    switch (js_type) {
        case JSType::JS_ARRAY_LIST:
            res = JSArrayList::Cast(receiver.GetTaggedObject())->Set(thread, index, value);
            break;
        default:
            break;
    }
    return res;
}
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_INTERPRETER_FAST_RUNTIME_STUB_INL_H
