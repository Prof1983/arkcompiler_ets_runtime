/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_INTERPRETER_INTERPRETER_H
#define ECMASCRIPT_INTERPRETER_INTERPRETER_H

#include "plugins/ecmascript/runtime/js_method.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"

namespace panda::ecmascript {
class ConstantPool;
class ECMAObject;
class GeneratorContext;

class EcmaInterpreter {
public:
    static inline JSTaggedValue Execute(EcmaRuntimeCallInfo *info);
    static inline JSTaggedValue ExecuteInEnv(EcmaRuntimeCallInfo *info, JSTaggedValue fn_object);
    static inline JSTaggedValue ExecuteInvoke(EcmaRuntimeCallInfo *info, JSTaggedValue fn_object);
    static inline JSTaggedValue ExecuteNative(EcmaRuntimeCallInfo *info);
    static inline JSTaggedValue GeneratorReEnterInterpreter(JSThread *thread, JSHandle<GeneratorContext> context);
    static inline void ChangeGenContext(JSThread *thread, JSHandle<GeneratorContext> context);
    static inline void ResumeContext(JSThread *thread);
    static inline void RunInternal(JSThread *thread, ConstantPool *constpool, const uint8_t *pc, JSTaggedType *sp);
    static inline uint8_t ReadU8(const uint8_t *pc, uint32_t offset);
    static inline void InitStackFrame(JSThread *thread);
    static inline uint32_t FindCatchBlock(JSMethod *caller, uint32_t pc);
    static inline size_t GetJumpSizeAfterCall(const uint8_t *prev_pc);

    static inline JSTaggedValue GetRuntimeProfileTypeInfo(JSTaggedType *sp);
    static inline bool UpdateHotnessCounter(JSThread *thread, JSTaggedType *sp, JSTaggedValue acc, int32_t offset);
    static inline void InterpreterFrameCopyArgs(JSTaggedType *new_sp, uint32_t num_vregs, uint32_t num_actual_args,
                                                uint32_t num_declared_args, bool have_extra_args = true);
    static inline void NotifyBytecodePcChanged(JSThread *thread);
    static inline JSTaggedValue GetThisFunction(JSTaggedType *sp);
    static inline JSTaggedValue GetNewTarget(JSTaggedType *sp);
    static inline uint32_t GetNumArgs(JSTaggedType *sp, uint32_t rest_idx, uint32_t &start_idx);
};

enum EcmaOpcode {
    MOV_DYN_V8_V8,
    MOV_DYN_V16_V16,
    LDA_STR_ID32,
    LDAI_DYN_IMM32,
    FLDAI_DYN_IMM64,
    LDNAN_IMM8,
    LDINFINITY_IMM8,
    LDGLOBALTHIS_IMM8,
    LDUNDEFINED_IMM8,
    LDNULL_IMM8,
    LDSYMBOL_IMM8,
    LDGLOBAL_IMM8,
    LDTRUE_IMM8,
    LDFALSE_IMM8,
    RETHROWDYN_IMM8,
    THROWDYN_IMM8,
    TYPEOFDYN_IMM8,
    LDLEXENVDYN_IMM8,
    POPLEXENVDYN_IMM8,
    GETUNMAPPEDARGS_IMM8,
    TOBOOLEAN_IMM8,
    NEGATE_IMM8,
    ISUNDEFINED_IMM8,
    ISTRUE_IMM8,
    ISFALSE_IMM8,
    ISCOERCIBLE_IMM8,
    GETPROPITERATOR_IMM8,
    ASYNCFUNCTIONENTER_IMM8,
    LDHOLE_IMM8,
    RETURNUNDEFINED_IMM8,
    CREATEEMPTYOBJECT_IMM8,
    CREATEEMPTYARRAY_IMM8,
    GETITERATOR_IMM8,
    GETASYNCITERATOR_IMM8,
    THROWTHROWNOTEXISTS_IMM8,
    THROWPATTERNNONCOERCIBLE_IMM8,
    LDHOMEOBJECT_IMM8,
    THROWDELETESUPERPROPERTY_IMM8,
    DEBUGGER_IMM8,
    JMP_IMM8,
    JMP_IMM16,
    JMP_IMM32,
    JEQZ_IMM8,
    JEQZ_IMM16,
    LDA_DYN_V8,
    STA_DYN_V8,
    LDBOOLEAN_IMM8_V8,
    LDNUMBER_IMM8_V8,
    LDSTRING_IMM8_V8,
    LDBIGINT_IMM8_ID32,
    ADD2DYN_IMM8_V8,
    SUB2DYN_IMM8_V8,
    MUL2DYN_IMM8_V8,
    DIV2DYN_IMM8_V8,
    MOD2DYN_IMM8_V8,
    EQDYN_IMM8_V8,
    NOTEQDYN_IMM8_V8,
    LESSDYN_IMM8_V8,
    LESSEQDYN_IMM8_V8,
    GREATERDYN_IMM8_V8,
    GREATEREQDYN_IMM8_V8,
    SHL2DYN_IMM8_V8,
    SHR2DYN_IMM8_V8,
    ASHR2DYN_IMM8_V8,
    AND2DYN_IMM8_V8,
    OR2DYN_IMM8_V8,
    XOR2DYN_IMM8_V8,
    TONUMBER_IMM8_V8,
    NEGDYN_IMM8_V8,
    NOTDYN_IMM8_V8,
    INCDYN_IMM8_V8,
    DECDYN_IMM8_V8,
    EXPDYN_IMM8_V8,
    ISINDYN_IMM8_V8,
    INSTANCEOFDYN_IMM8_V8,
    STRICTNOTEQDYN_IMM8_V8,
    STRICTEQDYN_IMM8_V8,
    RESUMEGENERATOR_IMM8_V8,
    GETRESUMEMODE_IMM8_V8,
    CREATEGENERATOROBJ_IMM8_V8,
    SETGENERATORSTATE_IMM8_V8_IMM8,
    CREATEASYNCGENERATOROBJ_IMM8_V8,
    THROWUNDEFINED_IMM8_V8,
    THROWCONSTASSIGNMENT_IMM8_ID32,
    GETMETHOD_IMM8_IMM16_V8,
    GETTEMPLATEOBJECT_IMM8_V8,
    GETNEXTPROPNAME_IMM8_V8,
    CALL0DYN_IMM8_V8,
    THROWIFNOTOBJECT_IMM8,
    CLOSEITERATOR_IMM8_V8,
    COPYMODULE_IMM8_V8,
    SUPERCALLSPREAD_IMM8_V8,
    LDOBJECT_IMM8_V8_V8,
    LDFUNCTION_IMM8_V8_V8,
    DELOBJPROP_IMM8_V8_V8,
    DEFINEGLOBALVAR_IMM8_V8_V8,
    DEFINELOCALVAR_IMM8_V8_V8,
    DEFINEFUNCEXPR_IMM8_V8_V8,
    REFEQDYN_IMM8_V8_V8,
    CALLRUNTIMERANGE_IMM8_V8_V8,
    NEWOBJSPREADDYN_IMM8_V8_V8,
    CREATEITERRESULTOBJ_IMM8,
    SUSPENDGENERATOR_IMM8_V8,
    SUSPENDASYNCGENERATOR_IMM8_V8,
    ASYNCFUNCTIONAWAIT_IMM8_V8,
    THROWTDZ_IMM8_ID32,
    CALL1DYN_IMM8_V8_V8,
    COPYDATAPROPERTIES_IMM8_V8_V8,
    STARRAYSPREAD_IMM8_V8_V8,
    SETOBJECTWITHPROTO_IMM8_V8_V8,
    CALLSPREADDYN_IMM8_V8_V8_V8,
    ASYNCFUNCTIONRESOLVE_IMM8_V8,
    ASYNCFUNCTIONREJECT_IMM8_V8,
    ASYNCGENERATORRESOLVE_IMM8_V8,
    ASYNCGENERATORREJECT_IMM8_V8,
    CALL2DYN_IMM8_V8_V8_V8,
    CALL3DYN_IMM8_V8_V8_V8_V8,
    DEFINEGETTERSETTERBYVALUE_IMM8_V8_V8_V8_V8,
    TRYLDGLOBALBYVALUE_IMM8_IMM16_V8,
    NEWOBJDYNRANGE_IMM8_IMM16_V8,
    TRYSTGLOBALBYVALUE_IMM8_IMM16_V8,
    CALLIRANGEDYN_IMM8_IMM16_V8,
    CALLITHISRANGEDYN_IMM8_IMM16_V8,
    SUPERCALL_IMM8_IMM16_V8,
    LDOBJBYVALUE_IMM8_IMM16_V8_V8,
    STOBJBYVALUE_IMM8_IMM16_V8_V8,
    LDOBJBYINDEX_IMM8_IMM32_IMM16_V8,
    STOBJBYINDEX_IMM8_IMM32_IMM16_V8,
    STOWNBYINDEX_IMM8_IMM32_IMM16_V8,
    STOWNBYVALUE_IMM8_IMM16_V8_V8,
    CREATEOBJECTWITHEXCLUDEDKEYS_IMM8_IMM16_V8_V8,
    STSUPERBYVALUE_IMM8_IMM16_V8_V8,
    LDSUPERBYVALUE_IMM8_IMM16_V8_V8,
    IMPORTMODULE_IMM8_ID32,
    STMODULEVAR_IMM8_ID32,
    DEFINEFUNCDYN_IMM8_ID16_V8,
    DEFINENCFUNCDYN_IMM8_ID16_V8,
    DEFINEGENERATORFUNC_IMM8_ID16_V8,
    DEFINEASYNCFUNC_IMM8_ID16_V8,
    DEFINEASYNCGENERATORFUNC_IMM8_ID16_V8,
    DEFINEMETHOD_IMM8_ID16_V8,
    TRYLDGLOBALBYNAME_IMM8_ID32_IMM16,
    TRYSTGLOBALBYNAME_IMM8_ID32_IMM16,
    LDGLOBALVAR_IMM8_ID32_IMM16,
    STGLOBALVAR_IMM8_ID32_IMM16,
    LDOBJBYNAME_IMM8_ID32_IMM16_V8,
    STOBJBYNAME_IMM8_ID32_IMM16_V8,
    STOWNBYNAME_IMM8_ID32_IMM16_V8,
    LDMODVARBYNAME_IMM8_ID32_IMM16_V8,
    LDSUPERBYNAME_IMM8_ID32_IMM16_V8,
    STSUPERBYNAME_IMM8_ID32_IMM16_V8,
    STLEXVARDYN_IMM8_IMM16_IMM16,
    STLEXDYN_IMM8_ID32_IMM16_IMM16,
    LDLEXVARDYN_IMM8_IMM16_IMM16,
    LDLEXDYN_IMM8_ID32_IMM16_IMM16,
    NEWLEXENVDYN_IMM8_IMM16,
    COPYLEXENVDYN_IMM8,
    COPYRESTARGS_IMM8_IMM16,
    CREATEOBJECTWITHBUFFER_IMM8_IMM16,
    CREATEARRAYWITHBUFFER_IMM8_IMM16,
    CREATEREGEXPWITHLITERAL_IMM8_ID32_IMM8,
    CREATEOBJECTHAVINGMETHOD_IMM8_IMM16,
    THROWIFSUPERNOTCORRECTCALL_IMM8_IMM16,
    LOADCLASSCOMPUTEDINSTANCEFIELDS_IMM8_V8,
    SETCLASSCOMPUTEDFIELDS_IMM8_V8_V8,
    DEFINECLASSWITHBUFFER_IMM8_ID16_IMM16_V8_V8,
    RETURN_DYN,
    MOV_V4_V4,
    JNEZ_IMM8,
    JNEZ_IMM16,
    LAST_OPCODE,
};
}  // namespace panda::ecmascript
#endif  // PANDA_RUNTIME_ECMASCRIPT_INTERPRETER_H
