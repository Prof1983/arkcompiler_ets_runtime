/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_RUNTIME_ECMASCRIPT_INTERPRETER_INL_H
#define PANDA_RUNTIME_ECMASCRIPT_INTERPRETER_INL_H

#include "plugins/ecmascript/runtime/class_linker/program_object-inl.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/interpreter/ecma-interpreter.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter.h"
#include "plugins/ecmascript/runtime/interpreter/js_frame-inl.h"
#include "plugins/ecmascript/runtime/interpreter/slow_runtime_stub.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/literal_data_extractor.h"
#include "plugins/ecmascript/runtime/template_string.h"
#include "plugins/ecmascript/runtime/handle_storage_check.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/thread_environment_api.h"
#include "include/method-inl.h"
#include "include/runtime_notification.h"
#include "libpandafile/code_data_accessor.h"
#include "libpandafile/file.h"
#include "libpandafile/method_data_accessor.h"

namespace panda::ecmascript {
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wvoid-ptr-dereference"
#pragma clang diagnostic ignored "-Wgnu-label-as-value"
#elif defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif

class JSInvokeHelper : public InvokeHelperDynamic {
public:
    ALWAYS_INLINE inline static void InterpreterExecute(ManagedThread *thread, const uint8_t *pc, Frame *frame)
    {
        JSThread *js_thread = JSThread::Cast(thread);

        // Get current functional object
        JSTaggedValue func = js_thread->GetFunctionalObject();
        ObjectHeader *lexical_env = js_thread->GetInvocationLexicalEnv().GetHeapObject();

        JSFunction *js_function = JSFunction::Cast(func.GetHeapObject());
        ObjectHeader *constant_pool = js_function->GetConstantPool().GetHeapObject();

        // Init EcmascriptEnvironment
        EcmascriptEnvironment *new_env = JSFrame::GetJSEnv(frame);
        new (new_env) EcmascriptEnvironment(constant_pool, lexical_env, func.GetHeapObject());

        InvokeHelperDynamic::InterpreterExecute(thread, pc, frame);
    }

    ALWAYS_INLINE inline static coretypes::TaggedValue CompiledCodeExecute(ManagedThread *thread, Method *method,
                                                                           uint32_t num_args,
                                                                           coretypes::TaggedValue *args)
    {
        auto ret = InvokeHelperDynamic::CompiledCodeExecute(thread, method, num_args, args);
        return ret;
    }

    ALWAYS_INLINE static Frame *CreateFrame(ManagedThread *thread, uint32_t nregs_size, Method *method, Frame *prev,
                                            uint32_t nregs, uint32_t num_actual_args)
    {
        return ::panda::CreateFrame<JSEnv>(thread->GetStackFrameAllocator(), nregs_size, method, prev, nregs,
                                           num_actual_args);
    }
};

JSTaggedValue EcmaInterpreter::ExecuteNative(EcmaRuntimeCallInfo *info)
{
    JSThread *thread = info->GetThread();
    uint32_t num_args = info->GetInternalArgsNum();
    JSTaggedValue fn_object = info->GetFunction().GetTaggedValue();

    ASSERT(!thread->HasPendingException());

    Frame *current_frame = thread->GetCurrentFrame();
    bool is_compiled = thread->IsCurrentFrameCompiled();

    // Boundary frame expected for c2i call
    ASSERT(!is_compiled || StackWalker::IsBoundaryFrame<FrameKind::INTERPRETER>(current_frame));

    Method *method = ECMAObject::Cast(fn_object.GetHeapObject())->GetCallTarget();
    ASSERT(method->GetNumVregs() == 0);

    uint32_t num_declared_args = method->GetNumArgs();
    uint32_t nregs = std::max(num_declared_args, num_args);
    Frame *frame = JSFrame::CreateNativeFrame(thread, method, current_frame, nregs, num_args);
    JSFrame::InitNativeFrameArgs(frame, num_args, info->GetInternalArgs());

    thread->SetCurrentFrameIsCompiled(false);
    thread->SetCurrentFrame(frame);
    JSTaggedValue ret_value = JSFrame::ExecuteNativeMethod(thread, frame, method, num_args);
    ASSERT(thread->GetCurrentFrame() == frame);
    thread->SetCurrentFrameIsCompiled(is_compiled);
    thread->SetCurrentFrame(frame->GetPrevFrame());

    JSFrame::DestroyNativeFrame(thread, frame);

    return ret_value;
}

JSTaggedValue EcmaInterpreter::ExecuteInvoke(EcmaRuntimeCallInfo *info, JSTaggedValue fn_object)
{
    JSThread *thread = info->GetThread();

    ASSERT(!thread->HasPendingException());
    ASSERT(fn_object.IsJSFunction());

    JSFunction *js_function = JSFunction::Cast(fn_object.GetHeapObject());
    Method *method = js_function->GetCallTarget();

    // Set current functional object to thread so we can get it from InvokeHelper
    thread->SetFunctionalObject(fn_object);
    thread->SetInvocationLexicalEnv(js_function->GetLexicalEnv());
    TaggedValue ret_value =
        method->InvokeDyn<JSInvokeHelper>(thread, info->GetInternalArgsNum(), info->GetInternalArgs());

    return JSTaggedValue(ret_value);
}

JSTaggedValue EcmaInterpreter::ExecuteInEnv(EcmaRuntimeCallInfo *info, JSTaggedValue fn_object)
{
    JSThread *thread = info->GetThread();
    [[maybe_unused]] HandleStorageCheck handle_storage_check(thread);
    if (UNLIKELY(thread->HasPendingException())) {  // TODO(vpukhov): replace with assert
        return JSTaggedValue::Undefined();
    }

    if (UNLIKELY(!fn_object.IsCallable())) {  // TODO(vpukhov): replace with assert
        JSHandle<JSObject> error =
            thread->GetEcmaVM()->GetFactory()->GetJSError(ErrorType::TYPE_ERROR, "is not callable");
        thread->SetException(error.GetTaggedValue());
        return JSTaggedValue::Exception();
    }

    Method *method = ECMAObject::Cast(fn_object.GetHeapObject())->GetCallTarget();

    if (method->IsNative()) {
        auto current_frame = thread->GetCurrentFrame();

        if (thread->IsCurrentFrameCompiled() && !StackWalker::IsBoundaryFrame<FrameKind::INTERPRETER>(current_frame)) {
            C2IBridge bridge {0, reinterpret_cast<uintptr_t>(current_frame), COMPILED_CODE_TO_INTERPRETER,
                              thread->GetNativePc()};
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            thread->SetCurrentFrame(reinterpret_cast<Frame *>(&bridge.v[1]));
            JSTaggedValue ret_val = ExecuteNative(info);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            ASSERT(thread->GetCurrentFrame() == reinterpret_cast<Frame *>(&bridge.v[1]));
            thread->SetCurrentFrameIsCompiled(true);
            thread->SetCurrentFrame(current_frame);
            return ret_val;
        }
        return ExecuteNative(info);
    }

    return ExecuteInvoke(info, fn_object);
}

JSTaggedValue EcmaInterpreter::Execute(EcmaRuntimeCallInfo *info)
{
    return ExecuteInEnv(info, info->GetFunction().GetTaggedValue());
}

JSTaggedValue EcmaInterpreter::GeneratorReEnterInterpreter(JSThread *thread, JSHandle<GeneratorContext> context)
{
    if (UNLIKELY(thread->HasPendingException())) {
        return JSTaggedValue::Exception();
    }

    JSHandle<JSFunction> func = JSHandle<JSFunction>::Cast(JSHandle<JSTaggedValue>(thread, context->GetMethod()));
    Method *method = func->GetCallTarget();

    JSTaggedValue pc_offset = context->GetBCOffset();
    auto format = method->GetPandaFile()->GetHeader()->quickened_flag != 0U ? BytecodeInstruction::Format::V8
                                                                            : BytecodeInstruction::Format::PREF_V8;
    const uint8_t *resume_pc =
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        method->GetInstructions() + static_cast<uint32_t>(pc_offset.GetInt()) + BytecodeInstruction::Size(format);

    auto *acc = reinterpret_cast<TaggedValue *>(ToUintPtr(*context) + context->GetAccOffset());
    uint32_t nregs = context->GetNRegs().GetInt();
    TaggedArray *regs_array = TaggedArray::Cast(context->GetRegsArray().GetHeapObject());
    auto *regs = reinterpret_cast<coretypes::TaggedValue *>(regs_array->GetData());
    ASSERT(nregs == regs_array->GetLength());

    // Set current functional object to thread so we can get it from InvokeHelper
    thread->SetFunctionalObject(func.GetTaggedValue());
    thread->SetInvocationLexicalEnv(context->GetLexicalEnv());
    TaggedValue value = method->InvokeContext<JSInvokeHelper>(thread, resume_pc, acc, nregs, regs);

    return JSTaggedValue(value);
}

void EcmaInterpreter::ChangeGenContext([[maybe_unused]] JSThread *thread,
                                       [[maybe_unused]] JSHandle<GeneratorContext> context)
{
    LOG(DEBUG, INTERPRETER) << "ChangeGenContext";
}

void EcmaInterpreter::ResumeContext([[maybe_unused]] JSThread *thread)
{
    LOG(DEBUG, INTERPRETER) << "ResumeContext";
}

#if defined(__clang__)
#pragma clang diagnostic pop
#elif defined(__GNUC__)
#pragma GCC diagnostic pop
#endif
}  // namespace panda::ecmascript
#endif  // PANDA_RUNTIME_ECMASCRIPT_INTERPRETER_INL_H
