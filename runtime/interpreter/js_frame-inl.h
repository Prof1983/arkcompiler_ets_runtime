/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_PLUGINS_ECMASCRIPT_RUNTIME_NATIVE_FRAME_INL_H
#define PANDA_PLUGINS_ECMASCRIPT_RUNTIME_NATIVE_FRAME_INL_H

#include "plugins/ecmascript/runtime/interpreter/js_frame.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/handle_storage_check.h"
#include "runtime/interpreter/frame.h"

namespace panda::ecmascript {
/* static */
inline Frame *JSFrame::CreateNativeFrame(JSThread *js_thread, Method *method, Frame *prev_frame, uint32_t nregs,
                                         uint32_t num_actual_args)
{
    ASSERT(js_thread == JSThread::GetCurrent());

    Frame *new_frame =
        CreateFrame(js_thread->GetStackFrameAllocator(), nregs, method, prev_frame, nregs, num_actual_args);
    if (UNLIKELY(new_frame == nullptr)) {
        return nullptr;
    }
    LOG_IF(new_frame == nullptr, FATAL, ECMASCRIPT) << "Cannot allocate native frame";
    new_frame->SetInvoke();
    new_frame->SetDynamic();
    return new_frame;
}

/* static */
inline void JSFrame::DestroyNativeFrame(JSThread *js_thread, Frame *frame)
{
    ASSERT(js_thread == JSThread::GetCurrent());
    DestroyFrame(js_thread->GetStackFrameAllocator(), frame);
}

/* static */
inline void JSFrame::InitNativeFrameArgs(Frame *frame, uint32_t num_args, const JSTaggedValue *args)
{
    uint32_t num_actual_args = frame->GetNumActualArgs();
    ASSERT(num_args <= num_actual_args);

    for (uint32_t i = 0; i < num_args; ++i) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        frame->GetVReg(i).SetValue(args[i].GetRawData());
    }
    for (uint32_t i = num_args; i < num_actual_args; ++i) {
        frame->GetVReg(i).SetValue(JSTaggedValue::VALUE_UNDEFINED);
    }
    frame->SetInvoke();
}

/* static */
inline JSTaggedValue JSFrame::ExecuteNativeMethod(JSThread *js_thread, Frame *frame, Method *method,
                                                  uint32_t num_actual_args)
{
    ASSERT(js_thread == JSThread::GetCurrent());
    if (UNLIKELY((!js_thread->StackOverflowCheck<true, true>()))) {
        return JSTaggedValue::Exception();
    }
    [[maybe_unused]] HandleStorageCheck handle_storage_check(js_thread);
    EcmaRuntimeCallInfo call_info(js_thread, num_actual_args, reinterpret_cast<JSTaggedValue *>(&frame->GetVReg(0)));
    auto ecma_entry_point = reinterpret_cast<EcmaEntrypoint>(method->GetNativePointer());
    JSTaggedValue ret_value = ecma_entry_point(&call_info);

    return ret_value;
}
}  // namespace panda::ecmascript

#endif  // PANDA_PLUGINS_ECMASCRIPT_RUNTIME_NATIVE_FRAME_INL_H
