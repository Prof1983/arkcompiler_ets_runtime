/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_ECMASCRIPT_INTERPRETER_H
#define PANDA_ECMASCRIPT_INTERPRETER_H

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "runtime/interpreter/frame.h"

namespace panda::ecmascript {

constexpr size_t ECMA_CALL_PREALLOC_SIZE = 32;

template <class VReg = interpreter::VRegister>
inline JSTaggedType VRegAsTaggedType(VReg const &vreg)
{
    return JSTaggedType(vreg.GetValue());
}

template <class VReg = interpreter::VRegister>
inline JSTaggedValue VRegAsTaggedValue(VReg const &vreg)
{
    return JSTaggedValue(VRegAsTaggedType(vreg));
}

}  // namespace panda::ecmascript

#endif  // PANDA_ECMASCRIPT_INTERPRETER_H
