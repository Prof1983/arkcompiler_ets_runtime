/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_PLUGINS_ECMASCRIPT_RUNTIME_INTERPRETER_JS_FRAME_H
#define PANDA_PLUGINS_ECMASCRIPT_RUNTIME_INTERPRETER_JS_FRAME_H

#include <cstdint>
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/ecmascript_environment.h"

namespace panda {
class Frame;
class Method;

namespace ecmascript {
class JSThread;

using JSExtFrame = ExtFrame<EcmascriptEnvironment>;

using JSEnv = EcmascriptEnvironment;

class JSFrame {
public:
    static Frame *CreateNativeFrame(JSThread *js_thread, Method *method, Frame *prev_frame, uint32_t nregs,
                                    uint32_t num_actual_args);
    static void DestroyNativeFrame(JSThread *js_thread, Frame *frame);

    static void InitNativeFrameArgs(Frame *frame, uint32_t num_args, const JSTaggedValue *args);
    static JSTaggedValue ExecuteNativeMethod(JSThread *js_thread, Frame *frame, Method *method,
                                             uint32_t num_actual_args);

    static JSEnv *GetJSEnv(const Frame *frame)
    {
        return JSExtFrame::FromFrame(frame)->GetExtData();
    }
};
}  // namespace ecmascript
}  // namespace panda

#endif  // PANDA_PLUGINS_ECMASCRIPT_RUNTIME_INTERPRETER_JS_FRAME_H
