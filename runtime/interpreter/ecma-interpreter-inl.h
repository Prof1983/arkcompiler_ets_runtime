/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_ECMASCRIPT_INTERPRETER_INL_H
#define PANDA_ECMASCRIPT_INTERPRETER_INL_H

#include "macros.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/interpreter/ecma-interpreter.h"
#include "plugins/ecmascript/runtime/interpreter/js_frame-inl.h"
#include "plugins/ecmascript/runtime/interpreter/slow_runtime_stub.h"
#include "plugins/ecmascript/runtime/interpreter/js_decode_call_instr.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/intrinsics-inl.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/ecma_profiling.h"
#include "plugins/ecmascript/runtime/handle_storage_check.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/thread_environment_api.h"
#include "runtime/interpreter/interpreter-inl.h"
#include "runtime/interpreter/instruction_handler_base.h"

namespace panda::ecmascript {

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTRINSIC_CALL_SETACC(intrinsic_call)                \
    {                                                        \
        JSTaggedValue result(intrinsic_call);                \
        ASSERT(!this->GetJSThread()->HasPendingException()); \
        SetAccFromTaggedValue(result);                       \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTRINSIC_CALL_CHECK(intrinsic_call)                 \
    {                                                        \
        JSTaggedValue result(intrinsic_call);                \
        if (UNLIKELY(result.IsException())) {                \
            this->MoveToExceptionHandler();                  \
            return;                                          \
        }                                                    \
        ASSERT(!this->GetJSThread()->HasPendingException()); \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTRINSIC_CALL_CHECK_SETACC(intrinsic_call)          \
    {                                                        \
        JSTaggedValue result(intrinsic_call);                \
        if (UNLIKELY(result.IsException())) {                \
            this->MoveToExceptionHandler();                  \
            return;                                          \
        }                                                    \
        ASSERT(!this->GetJSThread()->HasPendingException()); \
        SetAccFromTaggedValue(result);                       \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTRINSIC_CALL_CHECK_SETACC_SETENV(intrinsic_call)   \
    {                                                        \
        JSTaggedValue result(intrinsic_call);                \
        if (UNLIKELY(result.IsException())) {                \
            this->MoveToExceptionHandler();                  \
            return;                                          \
        }                                                    \
        ASSERT(!this->GetJSThread()->HasPendingException()); \
        SetLexicalEnv(result);                               \
        SetAccFromTaggedValue(result);                       \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTERPRETER_RETURN_IF_ABRUPT(result)                 \
    do {                                                     \
        if (result.IsException()) {                          \
            this->MoveToExceptionHandler();                  \
            return;                                          \
        }                                                    \
        ASSERT(!this->GetJSThread()->HasPendingException()); \
    } while (false)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define UPDATE_UNARY_ARITH_PROFILE(lhs) \
    if constexpr (IS_PROFILE_ENABLED) { \
        UpdateUnaryArithProfile(lhs);   \
    } else {                            \
        UNUSED_VAR(lhs);                \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define UPDATE_BINARY_ARITH_PROFILE(lhs, rhs) \
    if constexpr (IS_PROFILE_ENABLED) {       \
        UpdateBinaryArithProfile(lhs, rhs);   \
    } else {                                  \
        UNUSED_VAR(lhs);                      \
        UNUSED_VAR(rhs);                      \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define UPDATE_MOD_PROFILE(lhs, rhs)              \
    if constexpr (IS_PROFILE_ENABLED) {           \
        UpdateBinaryArithProfile<true>(lhs, rhs); \
    } else {                                      \
        UNUSED_VAR(lhs);                          \
        UNUSED_VAR(rhs);                          \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define UPDATE_TYPE_OF_PROFILE(lhs)     \
    if constexpr (IS_PROFILE_ENABLED) { \
        UpdateTypeOfProfile(lhs);       \
    } else {                            \
        UNUSED_VAR(lhs);                \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define UPDATE_CALL_PROFILE(func)       \
    if constexpr (IS_PROFILE_ENABLED) { \
        UpdateCallProfile(func);        \
    } else {                            \
        UNUSED_VAR(func);               \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GET_PROFILE_ADDRESS(addr)       \
    if constexpr (IS_PROFILE_ENABLED) { \
        addr = GetProfileAddress();     \
    } else {                            \
        addr = nullptr;                 \
    }

template <BytecodeInstruction::Opcode OPCODE, bool IS_QUICKENED>
class JSFrameHelper {
public:
    template <BytecodeInstruction::Format FORMAT, class InstructionHandler>
    ALWAYS_INLINE static uint32_t GetNumberActualArgsDyn([[maybe_unused]] InstructionHandler *instr_handler)
    {
        return JSGetNumberActualArgsDyn<FORMAT, OPCODE, IS_QUICKENED>(instr_handler->GetInst());
    }

    template <BytecodeInstruction::Format FORMAT, class InstructionHandler>
    ALWAYS_INLINE static void CopyArgumentsDyn(InstructionHandler *instr_handler, Frame *new_frame, uint32_t num_vregs,
                                               uint32_t num_actual_args)
    {
        Frame *prev_frame = instr_handler->GetFrame();
        BytecodeInstruction prev_inst = instr_handler->GetInst();
        auto *thread = JSThread::Cast(instr_handler->GetThread());

        uint64_t prev_raw_func_obj = JSGetCalleDyn<FORMAT, OPCODE, IS_QUICKENED>(prev_frame, prev_inst);

        JSCopyArgumets<FORMAT, OPCODE, IS_QUICKENED>(thread, prev_frame, prev_raw_func_obj, prev_inst, new_frame,
                                                     num_vregs, num_actual_args);
    }

    template <class RuntimeIfaceT>
    ALWAYS_INLINE static Frame *CreateFrame(ManagedThread *thread, uint32_t nregs_size, Method *method, Frame *prev,
                                            uint32_t nregs, uint32_t num_actual_args)
    {
        return ::panda::CreateFrame<JSEnv>(thread->GetStackFrameAllocator(), nregs_size, method, prev, nregs,
                                           num_actual_args);
    }
};

template <class RuntimeIfaceT, bool IS_DYNAMIC, bool IS_DEBUG, bool IS_PROFILE_ENABLED = false,
          bool IS_QUICKENED = false>
class InstructionHandler : public interpreter::InstructionHandler<RuntimeIfaceT, IS_DYNAMIC, IS_DEBUG> {
public:
    ALWAYS_INLINE inline explicit InstructionHandler(interpreter::InstructionHandlerState *state)
        : interpreter::InstructionHandler<RuntimeIfaceT, IS_DYNAMIC, IS_DEBUG>(state)
    {
    }

    ALWAYS_INLINE JSThread *GetJSThread() const
    {
        return JSThread::Cast(interpreter::InstructionHandler<RuntimeIfaceT, IS_DYNAMIC, IS_DEBUG>::GetThread());
    }

    ALWAYS_INLINE JSTaggedValue GetRegAsTaggedValue(uint16_t v)
    {
        return VRegAsTaggedValue(this->GetFrame()->GetVReg(v));
    }

    ALWAYS_INLINE JSTaggedType GetRegAsTaggedType(uint16_t v)
    {
        return VRegAsTaggedType(this->GetFrame()->GetVReg(v));
    }

    ALWAYS_INLINE JSTaggedValue GetAccAsTaggedValue()
    {
        return VRegAsTaggedValue(this->GetAcc());
    }

    JSTaggedType *GetStkArgs(uint16_t first_arg_idx)
    {
        static_assert(sizeof(interpreter::VRegister) == sizeof(JSTaggedType));
        auto thread = JSThread::Cast(this->GetThread());
        return reinterpret_cast<JSTaggedType *>(&thread->GetCurrentFrame()->GetVReg(first_arg_idx));
    }

    JSTaggedValue GetCurrentFunction()
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto num_vregs = this->GetFrame()->GetMethod()->GetNumVregs();
        return GetRegAsTaggedValue(num_vregs + js_method_args::FUNC_IDX);
    }

    JSTaggedValue GetCurrentNewTarget()
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto num_vregs = this->GetFrame()->GetMethod()->GetNumVregs();
        return GetRegAsTaggedValue(num_vregs + js_method_args::NEW_TARGET_IDX);
    }

    JSTaggedValue GetCurrentThis()
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto num_vregs = this->GetFrame()->GetMethod()->GetNumVregs();
        return GetRegAsTaggedValue(num_vregs + js_method_args::THIS_IDX);
    }

    JSTaggedValue GetGlobalObject()
    {
        auto thread = JSThread::Cast(this->GetThread());
        return thread->GetEcmaVM()->GetGlobalEnv()->GetGlobalObject();
    }

    panda::ecmascript::JSHandle<panda::ecmascript::GlobalEnv> GetGlobalEnv()
    {
        auto thread = JSThread::Cast(this->GetThread());
        return thread->GetEcmaVM()->GetGlobalEnv();
    }

    ObjectFactory *GetFactory()
    {
        auto thread = JSThread::Cast(this->GetThread());
        return thread->GetEcmaVM()->GetFactory();
    }

    ALWAYS_INLINE void SetAccFromTaggedValue(JSTaggedValue v)
    {
        this->GetAcc().Set(v.GetRawData());
    }

    ALWAYS_INLINE void SaveAccToFrame()
    {
        this->GetFrame()->SetAcc(this->GetAcc());
    }

    ALWAYS_INLINE void RestoreAccFromFrame()
    {
        this->GetAcc() = this->GetFrame()->GetAcc();
    }

    JSHandle<JSTaggedValue> GetMethodName(JSHandle<ECMAObject> call_target)
    {
        auto thread = JSThread::Cast(this->GetThread());
        JSHandle<JSFunction> this_func = JSHandle<JSFunction>::Cast(call_target);
        return JSFunctionBase::GetFunctionName(thread, JSHandle<JSFunctionBase>(this_func));
    }

    ALWAYS_INLINE EcmascriptEnvironment *GetCurrentEnv()
    {
        return JSFrame::GetJSEnv(this->GetFrame());
    }

    ALWAYS_INLINE uint64_t GetThisFuncRawValue()
    {
        return JSTaggedValue(GetCurrentEnv()->GetThisFunc()).GetRawData();
    }

    ALWAYS_INLINE ConstantPool *GetConstantPool()
    {
        return ConstantPool::Cast(GetCurrentEnv()->GetConstantPool());
    }

    ALWAYS_INLINE uint64_t GetConstantPoolRawValue()
    {
        return JSTaggedValue(GetCurrentEnv()->GetConstantPool()).GetRawData();
    }

    ALWAYS_INLINE uint64_t LoadFromConstantPool(BytecodeId id)
    {
        return GetConstantPool()->GetObjectFromCache(id.AsIndex()).GetRawData();
    }

    ALWAYS_INLINE LexicalEnv *GetLexicalEnv()
    {
        return LexicalEnv::Cast(GetCurrentEnv()->GetLexicalEnv());
    }

    ALWAYS_INLINE uint64_t GetLexicalEnvRawValue()
    {
        return JSTaggedValue(GetCurrentEnv()->GetLexicalEnv()).GetRawData();
    }

    ALWAYS_INLINE void SetLexicalEnv(JSTaggedValue lex_env)
    {
        return GetCurrentEnv()->SetLexicalEnv(lex_env.GetTaggedObject());
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaReturnDyn()
    {
        LOG_INST() << "ecma.return.dyn";

        auto frame = this->GetFrame();

        if (UNLIKELY(frame->IsInitobj())) {
            auto acc_in = GetAccAsTaggedValue();
            JSFunction *func = JSFunction::Cast(GetCurrentFunction().GetTaggedObject());
            if (LIKELY(acc_in.IsECMAObject())) {
                // preserve current value
            } else if (LIKELY(func->IsBase() || acc_in.IsUndefined())) {
                SetAccFromTaggedValue(GetCurrentThis());
            } else {
                [[maybe_unused]] EcmaHandleScope handle_scope(this->GetJSThread());
                ASSERT(func->IsDerivedConstructor());
                JSHandle<JSObject> error = GetFactory()->GetJSError(
                    ErrorType::TYPE_ERROR, "Derived constructor must return object or undefined");
                this->GetJSThread()->SetException(error.GetTaggedValue());
                this->MoveToExceptionHandler();
                return;
            }
        }
        this->GetFrame()->SetAcc((this->GetAcc()));
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaReturnundefined()
    {
        LOG_INST() << "return.undefined";

        auto frame = this->GetFrame();

        if (UNLIKELY(frame->IsInitobj())) {
            auto acc_in = GetAccAsTaggedValue();
            JSFunction *func = JSFunction::Cast(GetCurrentFunction().GetTaggedObject());
            if (LIKELY(func->IsBase() || acc_in.IsUndefined())) {
                SetAccFromTaggedValue(GetCurrentThis());
            } else {
                [[maybe_unused]] EcmaHandleScope handle_scope(this->GetJSThread());
                ASSERT(func->IsDerivedConstructor());
                JSHandle<JSObject> error = GetFactory()->GetJSError(
                    ErrorType::TYPE_ERROR, "Derived constructor must return object or undefined");
                this->GetJSThread()->SetException(error.GetTaggedValue());
                this->MoveToExceptionHandler();
                return;
            }
        } else {
            SetAccFromTaggedValue(JSTaggedValue::Undefined());
        }
        this->GetFrame()->SetAcc((this->GetAcc()));
    }

    // Redefine method from base class
    ALWAYS_INLINE void HandleReturnStackless()
    {
        interpreter::InstructionHandler<RuntimeIfaceT, IS_DYNAMIC, IS_DEBUG>::HandleReturnStackless();
    }

    template <BytecodeInstruction::Format FORMAT, BytecodeInstruction::Opcode OPCODE>
    ALWAYS_INLINE void DoEcmaCallDyn()
    {
        this->UpdateBytecodeOffset();
        this->SaveAccToFrame();

        JSThread *js_thread = this->GetJSThread();
        [[maybe_unused]] HandleStorageCheck handle_storage_check(js_thread);
        uint64_t function = JSGetCalleDyn<FORMAT, OPCODE, IS_QUICKENED>(js_thread->GetCurrentFrame(), this->GetInst());

        if (UNLIKELY(!JSTaggedValue(function).IsCallable())) {
            [[maybe_unused]] EcmaHandleScope handle_scope(js_thread);
            JSHandle<JSObject> error = GetFactory()->GetJSError(ErrorType::TYPE_ERROR, "is not callable");
            js_thread->SetException(error.GetTaggedValue());
            this->MoveToExceptionHandler();
            return;
        }

        ECMAObject *this_func = ECMAObject::Cast(JSTaggedValue(function).GetHeapObject());
        Method *method = this_func->GetCallTarget();
        if (method->IsNative()) {
            // Native
            ASSERT(method->GetNumVregs() == 0);
            UPDATE_CALL_PROFILE(this_func);

            uint32_t num_actual_args = JSGetNumberActualArgsDyn<FORMAT, OPCODE, IS_QUICKENED>(this->GetInst());
            uint32_t num_args = std::max(method->GetNumArgs(), num_actual_args);

            Frame *prev_frame = this->GetFrame();
            BytecodeInstruction prev_inst = this->GetInst();
            Frame *frame =
                JSFrame::CreateNativeFrame(js_thread, method, js_thread->GetCurrentFrame(), num_args, num_actual_args);

            JSCopyArgumets<FORMAT, OPCODE, IS_QUICKENED>(this->GetJSThread(), prev_frame, function, prev_inst, frame, 0,
                                                         num_actual_args);

            // Call native method
            js_thread->SetCurrentFrame(frame);
            JSTaggedValue ret_value = JSFrame::ExecuteNativeMethod(js_thread, frame, method, num_actual_args);
            ASSERT(js_thread->GetCurrentFrame() == frame);
            js_thread->SetCurrentFrameIsCompiled(false);
            js_thread->SetCurrentFrame(prev_frame);

            JSFrame::DestroyNativeFrame(js_thread, frame);
            if (UNLIKELY(js_thread->HasPendingException())) {
                this->MoveToExceptionHandler();
                return;
            }
            ASSERT(JSTaggedValue(ret_value).IsException() == false);
            SetAccFromTaggedValue(ret_value);
            this->template MoveToNextInst<FORMAT, true>();
            return;
        }

        JSFunction *js_function = JSFunction::Cast(this_func);
        if (UNLIKELY(js_function->IsClassConstructor())) {
            [[maybe_unused]] EcmaHandleScope handle_scope(js_thread);
            JSHandle<JSObject> error =
                GetFactory()->GetJSError(ErrorType::TYPE_ERROR, "class constructor cannot be called without 'new'");
            js_thread->SetException(error.GetTaggedValue());
            this->MoveToExceptionHandler();
            return;
        }

        UPDATE_CALL_PROFILE(this_func);
        if (method->HasCompiledCode()) {  // AOT, JIT
            this->template CallCompiledCode<FORMAT, true>(method);
        } else {  // Interpreter
            [[maybe_unused]] EcmaHandleScope scope(js_thread);
            JSHandle<JSFunction> function_handle(js_thread, js_function);

            method->DecrementHotnessCounter<true>(0, nullptr, false, JSTaggedValue(function));
            if (UNLIKELY(js_thread->HasPendingException())) {
                return;
            }

            // Call stackless interpreter
            this->template CallInterpreterStackless<JSFrameHelper<OPCODE, IS_QUICKENED>, FORMAT,
                                                    /* is_dynamic = */ true,
                                                    /* is_range= */ false, /* accept_acc= */ false,
                                                    /* initobj= */ false, /* call= */ true>(method);
            if (UNLIKELY(js_thread->HasPendingException())) {
                return;
            }

            ConstantPool *constant_pool = ConstantPool::Cast(function_handle->GetConstantPool().GetHeapObject());
            JSTaggedValue lexical_env = function_handle->GetLexicalEnv();

            // Init EcmascriptEnvironment
            EcmascriptEnvironment *new_env = JSFrame::GetJSEnv(this->GetFrame());
            new (new_env) EcmascriptEnvironment(constant_pool, lexical_env.GetHeapObject(), *function_handle);
        }
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaNewobjdynrange()
    {
        auto constexpr OPCODE = BytecodeInstruction::Opcode::ECMA_NEWOBJDYNRANGE_PREF_IMM16_V8_PROF16;

        auto first_arg_reg_idx = this->GetInst().template GetVReg<FORMAT, 0>();
        auto num_range_args = this->GetInst().template GetImm<FORMAT, 0>();

        LOG_INST() << "newobjDynrange " << num_range_args << " v" << first_arg_reg_idx;

        this->UpdateBytecodeOffset();
        // acc: out, thus dead

        auto range_args = GetStkArgs(first_arg_reg_idx);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto ctor_handle = JSHandle<JSTaggedValue>(ToUintPtr(&range_args[0]));
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto new_target_handle = JSHandle<JSTaggedValue>(ToUintPtr(&range_args[1]));

        auto ctor = ctor_handle.GetTaggedValue();
        auto new_target = new_target_handle.GetTaggedValue();

        auto thread = this->GetJSThread();

        if (LIKELY(ctor.IsJSFunction() && ctor.IsConstructor())) {
            auto ctor_func = JSFunction::Cast(ctor.GetTaggedObject());
            auto method = ctor_func->GetMethod();

            UPDATE_CALL_PROFILE(ctor_func);

            if (LIKELY(!method->IsNative() && (ctor_func->IsBase() || ctor_func->IsDerivedConstructor()))) {
                JSTaggedValue this_obj;
                if (ctor_func->IsBase()) {
                    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                    this_obj = GetFactory()
                                   ->NewJSObjectByConstructor(JSHandle<JSFunction>(ctor_handle), new_target_handle)
                                   .GetTaggedValue();
                    INTERPRETER_RETURN_IF_ABRUPT(this_obj);
                    // gc may fire
                    ctor = ctor_handle.GetTaggedValue();
                    ctor_func = JSFunction::Cast(ctor.GetTaggedObject());
                    new_target = new_target_handle.GetTaggedValue();
                } else {
                    this_obj = JSTaggedValue::Undefined();
                }
                SetAccFromTaggedValue(this_obj);

                if (UNLIKELY(method->HasCompiledCode())) {
                    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
                    JSHandle<JSTaggedValue> this_handle(thread, this_obj);

                    // 'this' object to be set from acc in i2c
                    this->template CallCompiledCode<FORMAT, true>(method);

                    auto acc_in = GetAccAsTaggedValue();
                    // gc may fire
                    ctor = ctor_handle.GetTaggedValue();
                    ctor_func = JSFunction::Cast(ctor.GetTaggedObject());
                    if (LIKELY(acc_in.IsECMAObject())) {
                        // preserve current value
                    } else if (LIKELY(ctor_func->IsBase() || acc_in.IsUndefined())) {
                        SetAccFromTaggedValue(this_handle.GetTaggedValue());
                    } else {
                        ASSERT(ctor_func->IsDerivedConstructor());
                        JSHandle<JSObject> error = GetFactory()->GetJSError(
                            ErrorType::TYPE_ERROR, "Derived constructor must return object or undefined");
                        this->GetJSThread()->SetException(error.GetTaggedValue());
                        this->MoveToExceptionHandler();
                        return;
                    }
                } else {
                    method->DecrementHotnessCounter<true>(0, nullptr, false, ctor);
                    if (UNLIKELY(thread->HasPendingException())) {
                        return;
                    }

                    // Call stackless interpreter
                    this->template CallInterpreterStackless<JSFrameHelper<OPCODE, IS_QUICKENED>, FORMAT,
                                                            /* is_dynamic = */ true,
                                                            /* is_range= */ false, /* accept_acc= */ false,
                                                            /* initobj= */ true, /* call= */ true>(method);
                    if (UNLIKELY(thread->HasPendingException())) {
                        return;
                    }
                    this->GetFrame()
                        ->GetVReg(method->GetNumVregs() + js_method_args::THIS_IDX)
                        .SetValue(this_obj.GetRawData());

                    // Init EcmascriptEnvironment
                    auto ctor_func_handle = JSHandle<JSFunction>(ctor_handle);
                    ConstantPool *constant_pool =
                        ConstantPool::Cast(ctor_func_handle->GetConstantPool().GetHeapObject());
                    JSTaggedValue lexical_env = ctor_func_handle->GetLexicalEnv();
                    EcmascriptEnvironment *new_env = JSFrame::GetJSEnv(this->GetFrame());
                    new (new_env) EcmascriptEnvironment(constant_pool, lexical_env.GetHeapObject(), *ctor_func_handle);
                }

                return;
            }

            if (LIKELY(ctor_func->IsBuiltinConstructor())) {
                ASSERT(method->IsNative() && method->GetNumVregs() == 0);

                uint32_t num_actual_args = JSGetNumberActualArgsDyn<FORMAT, OPCODE, IS_QUICKENED>(this->GetInst());
                uint32_t num_args = std::max(method->GetNumArgs(), num_actual_args);

                Frame *prev_frame = this->GetFrame();
                BytecodeInstruction prev_inst = this->GetInst();
                Frame *frame =
                    JSFrame::CreateNativeFrame(thread, method, thread->GetCurrentFrame(), num_args, num_actual_args);

                JSTaggedValue ret_value;
                if (UNLIKELY(frame == nullptr)) {
                    ret_value = JSTaggedValue::Exception();
                } else {
                    JSCopyArgumets<FORMAT, OPCODE, IS_QUICKENED>(this->GetJSThread(), prev_frame, ctor.GetRawData(),
                                                                 prev_inst, frame, 0, num_actual_args);
                    frame->GetVReg(0 + js_method_args::THIS_IDX).SetValue(JSTaggedValue::VALUE_UNDEFINED);

                    // Call native method
                    thread->SetCurrentFrame(frame);
                    ret_value = JSFrame::ExecuteNativeMethod(thread, frame, method, num_actual_args);

                    ASSERT(thread->GetCurrentFrame() == frame);
                    thread->SetCurrentFrameIsCompiled(false);
                    thread->SetCurrentFrame(prev_frame);
                    JSFrame::DestroyNativeFrame(thread, frame);
                }

                if (UNLIKELY(thread->HasPendingException())) {
                    this->MoveToExceptionHandler();
                    return;
                }
                ASSERT(JSTaggedValue(ret_value).IsException() == false);
                SetAccFromTaggedValue(ret_value);
                this->template MoveToNextInst<FORMAT, true>();
                return;
            }
        }

        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::NewobjDynrange(thread, num_range_args - 2U, ctor.GetRawData(),
                                                               new_target.GetRawData(), range_args + 2U));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall0dyn()
    {
        LOG_INST() << "call0.dyn";

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL0DYN_PREF_NONE_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall1dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();

        LOG_INST() << "call1.dyn "
                   << "v" << v0;

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL1DYN_PREF_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "call2.dyn "
                   << "v" << v0 << ", v" << v1;

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL2DYN_PREF_V8_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall3dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        auto v2 = this->GetInst().template GetVReg<FORMAT, 2>();
        LOG_INST() << "call3.dyn "
                   << "v" << v0 << ", v" << v1 << ", v" << v2;

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL3DYN_PREF_V8_V8_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall0thisdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();

        LOG_INST() << "callthis0.dyn "
                   << "this v:" << v0;

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL0THISDYN_PREF_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall1thisdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "callthis1.dyn "
                   << "this v" << v0 << ", v" << v1;

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL1THISDYN_PREF_V8_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall2thisdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        auto v2 = this->GetInst().template GetVReg<FORMAT, 2>();
        LOG_INST() << "callthis2.dyn "
                   << "this v" << v0 << ", v" << v1 << ", v" << v2;

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL2THISDYN_PREF_V8_V8_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCall3thisdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        auto v2 = this->GetInst().template GetVReg<FORMAT, 2>();
        auto v3 = this->GetInst().template GetVReg<FORMAT, 3>();
        LOG_INST() << "callthis3.dyn "
                   << "this v" << v0 << ", v" << v1 << ", v" << v2 << ", v" << v3;

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALL3THISDYN_PREF_V8_V8_V8_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCallirangedyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto num_args = this->GetInst().template GetImm<FORMAT, 0>();

        auto func = GetRegAsTaggedValue(v0);

        LOG_INST() << "calli.rangedyn " << num_args + 3 << ", v" << v0 << " , func:" << func.GetRawData();

        this->template DoEcmaCallDyn<FORMAT, BytecodeInstruction::Opcode::ECMA_CALLIRANGEDYN_PREF_IMM16_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCallithisrangedyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto num_args = this->GetInst().template GetImm<FORMAT, 0>();

        LOG_INST() << "calli.dyn.this.range " << num_args + 2 << ", v" << v0;

        this->template DoEcmaCallDyn<FORMAT,
                                     BytecodeInstruction::Opcode::ECMA_CALLITHISRANGEDYN_PREF_IMM16_V8_PROF16>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCallspreaddyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        auto v2 = this->GetInst().template GetVReg<FORMAT, 2>();

        LOG_INST() << "callspreaddyn"
                   << " v" << v0 << " v" << v1 << " v" << v2;

        uint64_t func = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t array = GetRegAsTaggedValue(v2).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::CallspreadDyn(this->GetJSThread(), func, obj, array));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaJfalse()
    {
        auto imm = this->GetInst().template GetImm<FORMAT>();
        LOG_INST() << "jfalse" << imm;

        auto acc = GetAccAsTaggedValue();

        if (acc == TaggedValue::False()) {
            this->template UpdateBranchStatistics<true>();
            if (!this->InstrumentBranches(imm)) {
                this->template JumpToInst<false>(imm);
            }
        } else {
            this->template UpdateBranchStatistics<false>();
            this->template MoveToNextInst<FORMAT, false>();
        }
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaJtrue()
    {
        auto imm = this->GetInst().template GetImm<FORMAT>();
        LOG_INST() << "jtrue" << imm;

        auto acc = GetAccAsTaggedValue();

        if (acc == TaggedValue::True()) {
            this->template UpdateBranchStatistics<true>();
            if (!this->InstrumentBranches(imm)) {
                this->template JumpToInst<false>(imm);
            }
        } else {
            this->template UpdateBranchStatistics<false>();
            this->template MoveToNextInst<FORMAT, false>();
        }
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdinfinity()
    {
        LOG_INST() << "ldinfinity";

        INTRINSIC_CALL_SETACC(intrinsics::Ldinfinity());

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdglobalthis()
    {
        LOG_INST() << "ldglobalthis";
        INTRINSIC_CALL_SETACC(intrinsics::Ldglobalthis(this->GetJSThread()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdundefined()
    {
        LOG_INST() << "ldundefined";
        INTRINSIC_CALL_SETACC(intrinsics::Ldundefined());
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdnull()
    {
        LOG_INST() << "ldnull";
        INTRINSIC_CALL_SETACC(intrinsics::Ldnull());
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdsymbol()
    {
        LOG_INST() << "ldsymbol";
        INTRINSIC_CALL_SETACC(intrinsics::Ldsymbol(this->GetJSThread()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdglobal()
    {
        LOG_INST() << "ldglobal";
        INTRINSIC_CALL_SETACC(intrinsics::Ldglobal(this->GetJSThread()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdtrue()
    {
        LOG_INST() << "ldtrue";
        INTRINSIC_CALL_SETACC(intrinsics::Ldtrue());
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdfalse()
    {
        LOG_INST() << "ldfalse";
        INTRINSIC_CALL_SETACC(intrinsics::Ldfalse());
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdbigint()
    {
        auto big_int = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "ld.bigint " << big_int << "n";

        auto thread = this->GetJSThread();
        INTRINSIC_CALL_SETACC(intrinsics::Ldbigint(thread, LoadFromConstantPool(big_int)));
        this->template MoveToNextInst<FORMAT, false>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdlexenvdyn()
    {
        LOG_INST() << "ldlexenvDyn ";
        SetAccFromTaggedValue(JSTaggedValue(GetLexicalEnvRawValue()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGetunmappedargs()
    {
        LOG_INST() << "getunmappedargs";

        uint32_t actual_num_args =
            this->GetFrame()->GetNumActualArgs() - js_method_args::NUM_MANDATORY_ARGS;  // not compile-time
        uint32_t start_idx = this->GetFrame()->GetMethod()->GetNumVregs() + js_method_args::NUM_MANDATORY_ARGS;

        auto thread = JSThread::Cast(this->GetThread());
        JSTaggedValue res = SlowRuntimeStub::GetUnmappedArgs(thread, actual_num_args, GetStkArgs(start_idx));

        INTERPRETER_RETURN_IF_ABRUPT(res);
        SetAccFromTaggedValue(res);
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAsyncfunctionenter()
    {
        LOG_INST() << "asyncfunctionenter";
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::AsyncFunctionEnter(this->GetJSThread()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdboolean()
    {
        // it38 Unimplement
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdnumber()
    {
        // it38 Unimplement
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdstring()
    {
        // it38 Unimplement
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaTonumber()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::tonumber"
                   << " v" << v0;
        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();

        UPDATE_UNARY_ARITH_PROFILE(value);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Tonumber(this->GetJSThread(), value));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaNegdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "negdyn"
                   << " v" << v0;

        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();

        UPDATE_UNARY_ARITH_PROFILE(value);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::NegDyn(this->GetJSThread(), value));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaNotdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "notdyn"
                   << " v" << v0;

        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();

        UPDATE_UNARY_ARITH_PROFILE(value);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::NotDyn(this->GetJSThread(), value));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaIncdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "incdyn"
                   << " v" << v0;

        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();

        UPDATE_UNARY_ARITH_PROFILE(value);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::IncDyn(this->GetJSThread(), value));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDecdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "decdyn"
                   << " v" << v0;

        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();

        UPDATE_UNARY_ARITH_PROFILE(value);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::DecDyn(this->GetJSThread(), value));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaRethrowdyn()
    {
        LOG_INST() << "rethrowdyn";
        auto thread = JSThread::Cast(this->GetThread());
        auto acc = GetAccAsTaggedValue();

        if (acc.IsHole()) {
            this->template MoveToNextInst<FORMAT, true>();
            return;
        }

        SlowRuntimeStub::ThrowDyn(thread, acc);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowdyn()
    {
        LOG_INST() << "throwdyn";
        auto thread = JSThread::Cast(this->GetThread());
        auto acc = GetAccAsTaggedValue();
        SlowRuntimeStub::ThrowDyn(thread, acc);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaTypeofdyn()
    {
        LOG_INST() << "typeofdyn";
        uint64_t acc = GetAccAsTaggedValue().GetRawData();

        UPDATE_TYPE_OF_PROFILE(acc);
        INTRINSIC_CALL_SETACC(intrinsics::TypeofDyn(this->GetJSThread(), acc));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaToboolean()
    {
        LOG_INST() << "toboolean";

        UPDATE_UNARY_ARITH_PROFILE(GetAccAsTaggedValue().GetRawData());

        INTRINSIC_CALL_SETACC(intrinsics::Toboolean(GetAccAsTaggedValue().GetRawData()));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaNegate()
    {
        LOG_INST() << "negate";
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        UPDATE_UNARY_ARITH_PROFILE(value);

        INTRINSIC_CALL_SETACC(intrinsics::Negate(value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaIsundefined()
    {
        LOG_INST() << "isundefined";
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_SETACC(intrinsics::IsUndefined(value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaIstrue()
    {
        LOG_INST() << "istrue";
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_SETACC(intrinsics::IsTrue(value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaIsfalse()
    {
        LOG_INST() << "isfalse";
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_SETACC(intrinsics::IsFalse(value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaIscoercible()
    {
        LOG_INST() << "iscoercible";
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_SETACC(intrinsics::IsCoercible(value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGetpropiterator()
    {
        LOG_INST() << "getpropiterator";

        uint64_t acc = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GetPropIterator(this->GetJSThread(), acc));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaResumegenerator()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "resumegenerator"
                   << " v" << v0;

        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_SETACC(intrinsics::ResumeGenerator(value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGetresumemode()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "getresumemode"
                   << " v" << v0;

        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_SETACC(intrinsics::GetResumeMode(value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGetiterator()
    {
        LOG_INST() << "getiterator";

        uint64_t obj = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GetIterator(this->GetJSThread(), obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGetasynciterator()
    {
        LOG_INST() << "getasynciterator";

        uint64_t obj = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GetAsyncIterator(this->GetJSThread(), obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowundefined()
    {
        // the instrunction has beed retired
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowconstassignment()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto prop = JSTaggedValue(LoadFromConstantPool(string_id));
        LOG_INST() << "throwconstassignment "
                   << "string_id:" << ConvertToPandaString(EcmaString::Cast(prop.GetHeapObject()));

        auto thread = JSThread::Cast(this->GetThread());
        SlowRuntimeStub::ThrowConstAssignment(thread, prop);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowthrownotexists()
    {
        LOG_INST() << "throwthrownotexists";
        auto thread = JSThread::Cast(this->GetThread());
        SlowRuntimeStub::ThrowThrowNotExists(thread);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowpatternnoncoercible()
    {
        LOG_INST() << "throwpatternnoncoercible";
        auto thread = JSThread::Cast(this->GetThread());
        SlowRuntimeStub::ThrowPatternNonCoercible(thread);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowifnotobject()
    {
        LOG_INST() << "throwifnotobject";

        if (GetAccAsTaggedValue().IsECMAObject()) {
            this->template MoveToNextInst<FORMAT, true>();
            return;
        }
        auto thread = JSThread::Cast(this->GetThread());
        SlowRuntimeStub::ThrowIfNotObject(thread);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCloseiterator()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "closeiterator"
                   << " v" << v0;

        uint64_t iter = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t acc = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::CloseIterator(this->GetJSThread(), iter, acc));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdobject()
    {
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdfunction()
    {
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAdd2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::add2dyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Add2Dyn(this->GetJSThread(), left, right));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSub2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::sub2dyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Sub2Dyn(this->GetJSThread(), left, right));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaMul2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::mul2dyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Mul2Dyn(this->GetJSThread(), left, right));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDiv2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::div2dyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Div2Dyn(this->GetJSThread(), left, right));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaMod2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::mod2dyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_MOD_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Mod2Dyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaEqdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::eqdyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::EqDyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaNoteqdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "noteqdyn"
                   << " v" << v0;

        uint64_t lhs = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t rhs = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(lhs, rhs);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::NotEqDyn(this->GetJSThread(), lhs, rhs));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLessdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::lessdyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::LessDyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLesseqdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::lesseqdyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::LessEqDyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGreaterdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::greaterdyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GreaterDyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGreatereqdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::greatereqdyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GreaterEqDyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaShl2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "shl2dyn"
                   << " v" << v0;

        uint64_t lhs = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t rhs = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(lhs, rhs);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Shl2Dyn(this->GetJSThread(), lhs, rhs));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaShr2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "shr2dyn"
                   << " v" << v0;

        uint64_t lhs = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t rhs = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(lhs, rhs);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Shr2Dyn(this->GetJSThread(), lhs, rhs));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAshr2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "ashr2dyn"
                   << " v" << v0;

        uint64_t lhs = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t rhs = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Ashr2Dyn(this->GetJSThread(), lhs, rhs));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAnd2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "and2dyn"
                   << " v" << v0;

        uint64_t lhs = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t rhs = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(lhs, rhs);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::And2Dyn(this->GetJSThread(), lhs, rhs));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaOr2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "or2dyn"
                   << " v" << v0;

        uint64_t lhs = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t rhs = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(lhs, rhs);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Or2Dyn(this->GetJSThread(), lhs, rhs));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaXor2dyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "xor2dyn"
                   << " v" << v0;

        uint64_t lhs = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t rhs = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(lhs, rhs);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Xor2Dyn(this->GetJSThread(), lhs, rhs));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDelobjprop()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "delobjprop"
                   << " v0" << v0 << " v1" << v1;

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop = GetRegAsTaggedValue(v1).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::Delobjprop(this->GetJSThread(), obj, prop));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefineglobalvar()
    {
        // it38 Unimplement
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefinelocalvar()
    {
        // it38 Unimplement
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefinefuncexpr()
    {
        // it38 Unimplement
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefinefuncdyn()
    {
        auto method_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "definefuncDyn"
                   << " v" << v0 << std::hex << method_id;

        uint64_t env = GetRegAsTaggedValue(v0).GetRawData();
        auto cp = GetConstantPool();
        uint64_t method = cp->GetObjectFromCache(method_id.AsIndex()).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::DefinefuncDyn(this->GetJSThread(), method, env, JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefinencfuncdyn()
    {
        auto method_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "definencfuncDyn"
                   << " v" << v0 << ", method_id: " << std::hex << method_id;

        uint64_t env = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t home_object = GetAccAsTaggedValue().GetRawData();

        auto cp = GetConstantPool();
        uint64_t method = cp->GetObjectFromCache(method_id.AsIndex()).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::DefineNCFuncDyn(this->GetJSThread(), method, env, home_object, JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefinemethod()
    {
        auto method_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "definemethod"
                   << " v" << v0 << std::hex << method_id;

        uint64_t tagged_cur_env = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t home_object = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        auto cp = GetConstantPool();
        uint64_t method = cp->GetObjectFromCache(method_id.AsIndex()).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::DefineMethod(this->GetJSThread(), method, tagged_cur_env, home_object,
                                                             JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaRefeqdyn()
    {
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaExpdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::expdyn"
                   << " v" << v0;
        uint64_t base = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t exponent = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::ExpDyn(this->GetJSThread(), base, exponent));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCallruntimerange()
    {
        // it38 Unimplement
        LOG_INST() << "-------------";
        UNREACHABLE();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaIsindyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "isindyn"
                   << " v" << v0;

        uint64_t prop = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::IsinDyn(this->GetJSThread(), prop, obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaInstanceofdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "instanceofdyn"
                   << " v" << v0;

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t target = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::InstanceofDyn(this->GetJSThread(), obj, target));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStrictnoteqdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "strictnoteq"
                   << " v" << v0;

        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_SETACC(intrinsics::StrictNotEqDyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStricteqdyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsics::stricteqdyn"
                   << " v" << v0;
        uint64_t left = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t right = GetAccAsTaggedValue().GetRawData();

        UPDATE_BINARY_ARITH_PROFILE(left, right);

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::StrictEqDyn(this->GetJSThread(), left, right));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdlexvardyn()
    {
        auto level = this->GetInst().template GetImm<FORMAT, 0>();
        auto slot = this->GetInst().template GetImm<FORMAT, 1>();
        LOG_INST() << "ldlexvardyn"
                   << " level:" << level << " slot:" << slot;
        auto lex_env = GetLexicalEnvRawValue();
        INTRINSIC_CALL_SETACC(intrinsics::LdLexVarDyn(this->GetJSThread(), level, slot, lex_env));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdlexdyn()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto prop = LoadFromConstantPool(string_id);
        auto level = this->GetInst().template GetImm<FORMAT, 0>();
        auto slot = this->GetInst().template GetImm<FORMAT, 1>();
        LOG_INST() << "ldlexdyn"
                   << " string_id:" << ConvertToPandaString(EcmaString::Cast(JSTaggedValue(prop).GetHeapObject()))
                   << " level:" << level << " slot:" << slot;

        auto lex_env = GetLexicalEnvRawValue();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::LdLexDyn(this->GetJSThread(), prop, level, slot, lex_env));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStlexvardyn()
    {
        auto level = this->GetInst().template GetImm<FORMAT, 0>();
        auto slot = this->GetInst().template GetImm<FORMAT, 1>();

        LOG_INST() << "stlexvardyn"
                   << " level:" << level << " slot:" << slot;

        uint64_t value = GetAccAsTaggedValue().GetRawData();
        uint64_t lex_env = GetLexicalEnvRawValue();
        intrinsics::StLexVarDyn(this->GetJSThread(), level, slot, value, lex_env);

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStlexdyn()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto prop = LoadFromConstantPool(string_id);
        auto level = this->GetInst().template GetImm<FORMAT, 0>();
        auto slot = this->GetInst().template GetImm<FORMAT, 1>();
        LOG_INST() << "stlexdyn"
                   << " string_id:" << ConvertToPandaString(EcmaString::Cast(JSTaggedValue(prop).GetHeapObject()))
                   << " level:" << level << " slot:" << slot;

        uint64_t value = GetAccAsTaggedValue().GetRawData();
        uint64_t lex_env = GetLexicalEnvRawValue();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::StLexDyn(this->GetJSThread(), prop, level, slot, value, lex_env));

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaNewlexenvdyn()
    {
        auto num_vars = this->GetInst().template GetImm<FORMAT, 0>();

        LOG_INST() << "newlexenvdyn"
                   << " imm " << num_vars;

        auto lex_env = GetLexicalEnvRawValue();
        INTRINSIC_CALL_CHECK_SETACC_SETENV(intrinsics::NewlexenvDyn(this->GetJSThread(), num_vars, lex_env));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCopylexenvdyn()
    {
        LOG_INST() << "copylexenvdyn";

        auto lex_env = GetLexicalEnvRawValue();
        INTRINSIC_CALL_CHECK_SETACC_SETENV(intrinsics::CopylexenvDyn(lex_env));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateiterresultobj()
    {
        auto done = this->GetInst().template GetImm<FORMAT, 0>();
        LOG_INST() << "createiterresultobj"
                   << " done " << std::boolalpha << done;

        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::CreateIterResultObj(this->GetJSThread(), value, done));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSuspendgenerator()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "suspendgenerator"
                   << " v" << v0;

        auto gen_obj = GetRegAsTaggedValue(v0);
        auto value = GetAccAsTaggedValue();
        // SuspendGenerator preserves BCOffset and acc
        this->GetFrame()->SetBytecodeOffset(this->GetBytecodeOffset());
        SaveAccToFrame();

        auto thread = JSThread::Cast(this->GetThread());
        auto res = SlowRuntimeStub::SuspendGenerator(thread, gen_obj, value);

        LOG(DEBUG, INTERPRETER) << "Exit: SuspendGenerator: res - " << res.GetRawData();

        INTERPRETER_RETURN_IF_ABRUPT(res);
        SetAccFromTaggedValue(res);
        SaveAccToFrame();
        // return to caller frame
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSuspendasyncgenerator()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "suspendasyncgenerator"
                   << " v" << v0;

        auto gen_obj = GetRegAsTaggedValue(v0);
        auto value = GetAccAsTaggedValue();
        // SuspendAsyncGenerator preserves BCOffset and acc
        this->GetFrame()->SetBytecodeOffset(this->GetBytecodeOffset());
        SaveAccToFrame();

        auto thread = JSThread::Cast(this->GetThread());
        auto res = SlowRuntimeStub::SuspendAsyncGenerator(thread, gen_obj, value);

        LOG(DEBUG, INTERPRETER) << "Exit: SuspendAsyncGenerator: res - " << res.GetRawData();

        INTERPRETER_RETURN_IF_ABRUPT(res);
        SetAccFromTaggedValue(res);
        SaveAccToFrame();
        ASSERT(!this->GetFrame()->IsStackless());
        // return to caller frame
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAsyncfunctionawait()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();

        LOG_INST() << "asyncfunctionawait"
                   << " v" << v0;

        uint64_t async_func_obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::AsyncFunctionAwait(this->GetJSThread(), async_func_obj, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAsyncfunctionresolve()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "asyncfunctionresolve"
                   << " v" << v0;

        uint64_t async_func_obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::AsyncFunctionResolve(this->GetJSThread(), async_func_obj, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAsyncfunctionreject()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "asyncfunctionreject"
                   << " v" << v0;

        uint64_t async_func_obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::AsyncFunctionReject(this->GetJSThread(), async_func_obj, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAsyncgeneratorresolve()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "asyncgeneratorresolve"
                   << " v" << v0;

        uint64_t async_func_obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::AsyncGeneratorResolve(this->GetJSThread(), async_func_obj, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaAsyncgeneratorreject()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "asyncgeneratorreject"
                   << " v" << v0;

        uint64_t async_func_obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::AsyncGeneratorReject(this->GetJSThread(), async_func_obj, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaNewobjspreaddyn()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "intrinsic::newobjspearddyn"
                   << " v" << v0 << " v" << v1;

        uint64_t func = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t new_target = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t array = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::NewobjspreadDyn(this->GetJSThread(), func, new_target, array));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowtdz()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto prop = JSTaggedValue(LoadFromConstantPool(string_id));
        LOG_INST() << "intrinsic::throwundefinedifhole "
                   << "string_id:" << ConvertToPandaString(EcmaString::Cast(prop.GetHeapObject()));

        auto thread = JSThread::Cast(this->GetThread());
        SlowRuntimeStub::ThrowTdz(thread, prop);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStownbyname()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "stownbyname "
                   << "v" << v0 << " string_id:" << id.AsIndex();

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        INTRINSIC_CALL_CHECK(intrinsics::StOwnByName(this->GetJSThread(), LoadFromConstantPool(id), obj, value));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateemptyarray()
    {
        LOG_INST() << "createemptyarray";

        INTRINSIC_CALL_SETACC(intrinsics::CreateEmptyArray(this->GetJSThread()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateemptyobject()
    {
        LOG_INST() << "createemptyobject";

        INTRINSIC_CALL_SETACC(intrinsics::CreateEmptyObject(this->GetJSThread()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateobjectwithbuffer()
    {
        auto literalarray_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "createobjectwithbuffer"
                   << " literalArrayId:" << literalarray_id.AsIndex();

        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::CreateObjectWithBuffer(this->GetJSThread(), LoadFromConstantPool(literalarray_id)));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreatearraywithbuffer()
    {
        auto literalarray_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "createarraywithbuffer"
                   << " literalArrayId:" << literalarray_id.AsIndex();

        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::CreateArrayWithBuffer(this->GetJSThread(), LoadFromConstantPool(literalarray_id)));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateregexpwithliteral()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto flags = this->GetInst().template GetImm<FORMAT, 0>();
        LOG_INST() << "createregexpwithliteral"
                   << " string_id: " << string_id << " flags:" << flags;

        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::CreateRegExpWithLiteral(this->GetJSThread(), LoadFromConstantPool(string_id), flags));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaImportmodule()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "importmodule "
                   << "string_id:" << std::hex << string_id.AsIndex();

        INTRINSIC_CALL_SETACC(intrinsics::ImportModule(this->GetJSThread(), LoadFromConstantPool(string_id)));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStmodulevar()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "stmodulevar "
                   << "string_id:" << string_id;

        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        intrinsics::StModuleVar(this->GetJSThread(), LoadFromConstantPool(string_id), value);
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCopymodule()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "copymodule "
                   << " v" << v0;

        uint64_t src_module = GetRegAsTaggedValue(v0).GetRawData();
        SaveAccToFrame();
        intrinsics::CopyModule(this->GetJSThread(), src_module);
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdmodvarbyname()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto string_id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "ldmodvarbyname "
                   << "string_id:" << string_id.AsIndex();

        uint64_t module_obj = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_SETACC(
            intrinsics::LdModvarByName(this->GetJSThread(), LoadFromConstantPool(string_id), module_obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGetmethod()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto string_id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "getmethod v" << v0 << " string_id:" << string_id;
        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GetMethod(this->GetJSThread(), LoadFromConstantPool(string_id), obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGettemplateobject()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsic::gettemplateobject"
                   << " v" << v0;

        uint64_t literal = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GetTemplateObject(this->GetJSThread(), literal));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaGetnextpropname()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsic::getnextpropname"
                   << " v" << v0;

        uint64_t iter = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::GetNextPropName(this->GetJSThread(), iter));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCopydataproperties()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "intrinsic::copydataproperties"
                   << " v" << v0 << " v" << v1;

        uint64_t dst = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t src = GetRegAsTaggedValue(v1).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::CopyDataProperties(this->GetJSThread(), dst, src));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStownbyindex()
    {
        uint32_t idx = this->GetInst().template GetImm<FORMAT, 0>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "stownbyindex"
                   << " v" << v0 << " imm" << idx;

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        INTRINSIC_CALL_CHECK(intrinsics::StOwnByIndex(this->GetJSThread(), idx, obj, value));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStownbyvalue()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "stownbyvalue"
                   << " v" << v0 << " v" << v1;

        uint64_t receiver = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop_key = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        INTRINSIC_CALL_CHECK(intrinsics::StOwnByValue(this->GetJSThread(), receiver, prop_key, value));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateobjectwithexcludedkeys()
    {
        auto num_keys = this->GetInst().template GetImm<FORMAT, 0>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        uint16_t first_arg_reg_idx = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "createobjectwithexcludedkeys " << num_keys << " v" << v0 << std::hex << first_arg_reg_idx;

        auto obj = GetRegAsTaggedValue(v0);
        auto thread = JSThread::Cast(this->GetThread());

        auto res = SlowRuntimeStub::CreateObjectWithExcludedKeys(thread, num_keys, obj, GetStkArgs(first_arg_reg_idx));
        INTERPRETER_RETURN_IF_ABRUPT(res);
        SetAccFromTaggedValue(res);

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefinegeneratorfunc()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto method_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "define generator function"
                   << " v" << v0;

        uint64_t env = GetRegAsTaggedValue(v0).GetRawData();
        auto cp = GetConstantPool();
        uint64_t method = cp->GetObjectFromCache(method_id.AsIndex()).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::DefineGeneratorFunc(this->GetJSThread(), method, env, JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefineasyncfunc()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto method_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "define async function"
                   << " v" << v0;

        uint64_t env = GetRegAsTaggedValue(v0).GetRawData();
        auto cp = GetConstantPool();
        uint64_t method = cp->GetObjectFromCache(method_id.AsIndex()).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::DefineAsyncFunc(this->GetJSThread(), method, env, JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefineasyncgeneratorfunc()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto method_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "define async generator function"
                   << " v" << v0;

        uint64_t env = GetRegAsTaggedValue(v0).GetRawData();
        auto cp = GetConstantPool();
        uint64_t method = cp->GetObjectFromCache(method_id.AsIndex()).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::DefineAsyncGeneratorFunc(this->GetJSThread(), method, env, JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdhole()
    {
        LOG_INST() << "intrinsic::ldhole";
        INTRINSIC_CALL_SETACC(intrinsics::Ldhole());

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCopyrestargs()
    {
        auto rest_idx = this->GetInst().template GetImm<FORMAT, 0>();
        LOG_INST() << "copyrestargs"
                   << " index: " << rest_idx;

        auto *state = this->GetFrame();
        uint32_t num_vregs = state->GetMethod()->GetNumVregs();
        // Exclude func, new_target and "this"
        int32_t actual_num_args = state->GetNumActualArgs() - js_method_args::NUM_MANDATORY_ARGS;
        int32_t tmp = actual_num_args - rest_idx;
        uint32_t rest_num_args = (tmp > 0) ? tmp : 0;
        uint32_t start_idx = num_vregs + js_method_args::NUM_MANDATORY_ARGS + rest_idx;

        auto thread = JSThread::Cast(this->GetThread());
        JSTaggedValue res = SlowRuntimeStub::CopyRestArgs(thread, rest_num_args, GetStkArgs(start_idx));
        INTERPRETER_RETURN_IF_ABRUPT(res);
        SetAccFromTaggedValue(res);
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefinegettersetterbyvalue()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        auto v2 = this->GetInst().template GetVReg<FORMAT, 2>();
        auto v3 = this->GetInst().template GetVReg<FORMAT, 3>();
        LOG_INST() << "definegettersetterbyvalue"
                   << " v" << v0 << " v" << v1 << " v" << v2 << " v" << v3;

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t getter = GetRegAsTaggedValue(v2).GetRawData();
        uint64_t setter = GetRegAsTaggedValue(v3).GetRawData();
        uint64_t flag = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::DefineGetterSetterByValue(this->GetJSThread(), obj, prop, getter, setter, flag));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdobjbyindex()
    {
        uint32_t idx = this->GetInst().template GetImm<FORMAT, 0>();
        LOG_INST() << "ldobjbyindex"
                   << " imm" << idx;

        uint64_t obj = GetAccAsTaggedValue().GetRawData();
        void *prof_address;
        GET_PROFILE_ADDRESS(prof_address);
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::LdObjByIndex(this->GetJSThread(), idx, obj, prof_address));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStobjbyindex()
    {
        uint32_t idx = this->GetInst().template GetImm<FORMAT, 0>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "stobjbyindex"
                   << " v" << v0 << " imm" << idx;

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();
        void *prof_address;
        GET_PROFILE_ADDRESS(prof_address);
        SaveAccToFrame();
        INTRINSIC_CALL_CHECK(intrinsics::StObjByIndex(this->GetJSThread(), idx, obj, value, prof_address));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdobjbyvalue()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "Ldobjbyvalue"
                   << " v" << v0;

        uint64_t receiver = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop_key = GetAccAsTaggedValue().GetRawData();
        uint64_t func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::LdObjByValue(this->GetJSThread(), receiver, prop_key, this->GetBytecodeOffset(), func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStobjbyvalue()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "stobjbyvalue"
                   << " v" << v0 << " v" << v1;

        uint64_t receiver = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop_key = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK(
            intrinsics::StObjByValue(this->GetJSThread(), receiver, prop_key, value, this->GetBytecodeOffset(), func));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdsuperbyvalue()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "Ldsuperbyvalue"
                   << " v" << v0;

        uint64_t receiver = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop_key = GetAccAsTaggedValue().GetRawData();
        uint64_t func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::LdSuperByValue(this->GetJSThread(), receiver, prop_key, func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStsuperbyvalue()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "stsuperbyvalue"
                   << " v" << v0 << " v" << v1;

        uint64_t receiver = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop_key = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();
        uint64_t func = GetThisFuncRawValue();

        SaveAccToFrame();
        INTRINSIC_CALL_CHECK(intrinsics::StSuperByValue(this->GetJSThread(), receiver, prop_key, value, func));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaTryldglobalbyname()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::TryLdGlobalByName(this->GetJSThread(), LoadFromConstantPool(string_id),
                                                                  this->GetBytecodeOffset(), func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaTrystglobalbyname()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "trystglobalbyname"
                   << " string_id:" << string_id;

        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK(intrinsics::TryStGlobalByName(this->GetJSThread(), LoadFromConstantPool(string_id), value,
                                                           this->GetBytecodeOffset(), func));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdglobalvar()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "ldglobalvar "
                   << " string_id:" << string_id;

        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::LdGlobalVar(this->GetJSThread(), LoadFromConstantPool(string_id),
                                                            this->GetBytecodeOffset(), func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdobjbyname()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "ldobjbyname"
                   << " string_id:" << string_id;
        uint64_t obj = GetAccAsTaggedValue().GetRawData();

        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::LdObjByName(this->GetJSThread(), LoadFromConstantPool(string_id), obj,
                                                            this->GetBytecodeOffset(), func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStobjbyname()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto string_id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "stobjbyname "
                   << "v" << v0 << " string_id:" << string_id.AsIndex();

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK(intrinsics::StObjByName(this->GetJSThread(), LoadFromConstantPool(string_id), obj, value,
                                                     this->GetBytecodeOffset(), func));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdsuperbyname()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "ldsuperbyname"
                   << " string_id:" << string_id.AsIndex();

        uint64_t obj = GetAccAsTaggedValue().GetRawData();
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::LdSuperByName(this->GetJSThread(), LoadFromConstantPool(string_id), obj, func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStsuperbyname()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();

        LOG_INST() << "stsuperbyname"
                   << "v" << v0 << " string_id:" << string_id.AsIndex();

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK(
            intrinsics::StSuperByName(this->GetJSThread(), LoadFromConstantPool(string_id), obj, value, func));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStglobalvar()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "stglobalvar "
                   << "string_id:" << string_id.AsIndex() << ", ";

        uint64_t value = GetAccAsTaggedValue().GetRawData();

        SaveAccToFrame();
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_CHECK(intrinsics::StGlobalVar(this->GetJSThread(), LoadFromConstantPool(string_id), value,
                                                     this->GetBytecodeOffset(), func));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreategeneratorobj()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "creategeneratorobj"
                   << " v" << v0;

        uint64_t gen_func = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::CreateGeneratorObj(this->GetJSThread(), gen_func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSetgeneratorstate()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto state = this->GetInst().template GetImm<FORMAT, 0>();
        LOG_INST() << "setgeneratorstate"
                   << " v" << v0 << " state" << state;

        uint64_t gen_func = GetRegAsTaggedValue(v0).GetRawData();

        intrinsics::SetGeneratorState(this->GetJSThread(), gen_func, state);
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateasyncgeneratorobj()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "createasyncgeneratorobj"
                   << " v" << v0;

        uint64_t gen_func = GetRegAsTaggedValue(v0).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::CreateAsyncGeneratorObj(this->GetJSThread(), gen_func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStarrayspread()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "ecmascript::starrayspread"
                   << " v" << v0 << " v" << v1 << "acc";

        uint64_t dst = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t index = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t src = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::StArraySpread(this->GetJSThread(), dst, index, src));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLoadclasscomputedinstancefields()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "loadclasscomputedinstancefields"
                   << " v" << v0;

        uint64_t ctor = GetRegAsTaggedValue(v0).GetRawData();

        SetAccFromTaggedValue(JSTaggedValue(intrinsics::LoadClassComputedInstanceFields(this->GetJSThread(), ctor)));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSetclasscomputedfields()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "setclasscomputedfields"
                   << " class_reg v:" << v0 << " computed_fields v:" << v1;

        uint64_t class_reg = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t computed_fields = GetRegAsTaggedValue(v1).GetRawData();

        intrinsics::SetClassComputedFields(this->GetJSThread(), class_reg, computed_fields);
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefineclasswithbuffer()
    {
        auto method_id = this->GetInst().template GetId<FORMAT>().AsIndex();
        auto imm = this->GetInst().template GetImm<FORMAT, 0>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "defineclasswithbuffer"
                   << " method id:" << method_id << " literal id:" << imm << " lexenv: v" << v0 << " parent: v" << v1;

        uint64_t lexenv = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t proto = GetRegAsTaggedValue(v1).GetRawData();
        auto cp = GetConstantPool();
        uint64_t method = cp->GetObjectFromCache(method_id).GetRawData();
        // uint64_t buffer = cp->GetObjectFromCache(imm).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(intrinsics::DefineClassWithBuffer(this->GetJSThread(), method, imm, lexenv, proto,
                                                                      JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaClassfieldadd()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "classfieldadd"
                   << " obj: v" << v0 << " prop: v" << v1;

        uint64_t obj = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t prop = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::ClassFieldAdd(this->GetJSThread(), obj, prop, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDefineclassprivatefields()
    {
        auto literalarray_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "ldevalbindings"
                   << " literalArrayId:" << literalarray_id.AsIndex();

        auto v0 = this->GetInst().template GetVReg<FORMAT>();
        LOG_INST() << "defineclassprivatefields"
                   << " literalArrayId:" << literalarray_id.AsIndex() << " v0:" << v0;

        uint64_t env = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t ctor = GetAccAsTaggedValue().GetRawData();
        auto cp = GetConstantPool();
        uint64_t private_buf = cp->GetObjectFromCache(literalarray_id.AsIndex()).GetRawData();
        intrinsics::DefineClassPrivateFields(this->GetJSThread(), private_buf, env, ctor,
                                             JSTaggedValue(cp).GetRawData());
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaClassprivatemethodoraccessoradd()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "classprivatemethodoraccessoradd"
                   << " ctor: v" << v0 << " obj: v" << v1;

        uint64_t ctor = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetRegAsTaggedValue(v1).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::ClassPrivateMethodOrAccessorAdd(this->GetJSThread(), ctor, obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaClassprivatefieldadd()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "classprivatefieldadd"
                   << " string id:" << string_id << " ctor: v" << v0 << " obj: v" << v1;

        uint64_t ctor = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::ClassPrivateFieldAdd(this->GetJSThread(), LoadFromConstantPool(string_id), ctor, obj, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaClassprivatefieldget()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "classprivatefieldadd"
                   << " string id:" << string_id << " private contexts: v" << v0 << " obj: v" << v1;

        uint64_t ctor = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetRegAsTaggedValue(v1).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::ClassPrivateFieldGet(this->GetJSThread(), LoadFromConstantPool(string_id), ctor, obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaClassprivatefieldset()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();
        LOG_INST() << "classprivatefieldset"
                   << " string id:" << string_id << " private contexts: v" << v0 << " obj: v" << v1;

        uint64_t ctor = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetRegAsTaggedValue(v1).GetRawData();
        uint64_t value = GetAccAsTaggedValue().GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::ClassPrivateFieldSet(this->GetJSThread(), LoadFromConstantPool(string_id), ctor, obj, value));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaClassprivatefieldin()
    {
        auto string_id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "classprivatefieldin"
                   << " string id:" << string_id << " private contexts: v" << v0;

        uint64_t ctor = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetAccAsTaggedValue().GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::ClassPrivateFieldIn(this->GetJSThread(), LoadFromConstantPool(string_id), ctor, obj));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSupercall()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto range = this->GetInst().template GetImm<FORMAT, 0>();
        LOG_INST() << "supercall"
                   << "range: " << range << " v" << v0;

        auto num_vregs = this->GetFrame()->GetMethod()->GetNumVregs();

        auto this_func = GetAccAsTaggedValue();
        auto new_target = GetRegAsTaggedValue(num_vregs + 1);

        auto thread = JSThread::Cast(this->GetThread());
        auto res = SlowRuntimeStub::SuperCall(thread, this_func, new_target, range, GetStkArgs(v0));
        INTERPRETER_RETURN_IF_ABRUPT(res);
        SetAccFromTaggedValue(res);

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSupercallspread()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        LOG_INST() << "intrinsic::supercallspread"
                   << " array: v" << v0;

        uint64_t array = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t this_func = GetAccAsTaggedValue().GetRawData();

        auto num_vregs = this->GetFrame()->GetMethod()->GetNumVregs();
        uint64_t new_target = GetRegAsTaggedValue(num_vregs + 1).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::SuperCallSpread(this->GetJSThread(), array, new_target, this_func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaCreateobjecthavingmethod()
    {
        auto literalarray_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "createobjecthavingmethod"
                   << " literalArrayId:" << literalarray_id.AsIndex();

        uint64_t env = GetAccAsTaggedValue().GetRawData();
        auto cp = GetConstantPool();
        uint64_t literal = cp->GetObjectFromCache(literalarray_id.AsIndex()).GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::CreateObjectHavingMethod(this->GetJSThread(), literal, env, JSTaggedValue(cp).GetRawData()));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdnan()
    {
        LOG_INST() << "ldnan";

        INTRINSIC_CALL_SETACC(intrinsics::Ldnan());

        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowifsupernotcorrectcall()
    {
        uint16_t imm = this->GetInst().template GetImm<FORMAT, 0>();
        JSTaggedValue this_value = GetAccAsTaggedValue();
        LOG_INST() << "intrinsic::throwifsupernotcorrectcall"
                   << " imm:" << imm;
        auto thread = JSThread::Cast(this->GetThread());
        JSTaggedValue res = SlowRuntimeStub::ThrowIfSuperNotCorrectCall(thread, imm, this_value);
        INTERPRETER_RETURN_IF_ABRUPT(res);
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaThrowdeletesuperproperty()
    {
        LOG_INST() << "throwdeletesuperproperty";
        auto thread = JSThread::Cast(this->GetThread());
        SlowRuntimeStub::ThrowDeleteSuperProperty(thread);
        this->MoveToExceptionHandler();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdhomeobject()
    {
        LOG_INST() << "ldhomeobject";
        auto func = GetThisFuncRawValue();
        INTRINSIC_CALL_SETACC(intrinsics::LdHomeObject(this->GetJSThread(), func));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaPoplexenvdyn()
    {
        auto thread = this->GetJSThread();
        auto lex_env = GetLexicalEnvRawValue();
        auto res = JSTaggedValue(intrinsics::PopLexenvDyn(thread, lex_env));
        SetLexicalEnv(res);
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaSetobjectwithproto()
    {
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();

        LOG_INST() << "setobjectwithproto"
                   << " v" << v0 << " v" << v1;

        uint64_t proto = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t obj = GetRegAsTaggedValue(v1).GetRawData();

        SaveAccToFrame();
        INTRINSIC_CALL_CHECK(intrinsics::SetObjectWithProto(this->GetJSThread(), proto, obj));
        RestoreAccFromFrame();
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdevalbindings()
    {
        auto literalarray_id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "ldevalbindings"
                   << " literalArrayId:" << literalarray_id.AsIndex();
        INTRINSIC_CALL_SETACC(intrinsics::LdEvalBindings(this->GetJSThread(), LoadFromConstantPool(literalarray_id)));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDirecteval()
    {
        auto imm = this->GetInst().template GetImm<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();
        auto v1 = this->GetInst().template GetVReg<FORMAT, 1>();

        LOG_INST() << "directeval"
                   << " status:" << imm << " v0:" << v0 << " v1:" << v1;

        uint64_t arg0 = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t eval_bindings = GetRegAsTaggedValue(v1).GetRawData();

        INTRINSIC_CALL_CHECK_SETACC(intrinsics::DirectEval(this->GetJSThread(), imm, arg0, eval_bindings));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaLdevalvar()
    {
        auto id = this->GetInst().template GetId<FORMAT>();

        LOG_INST() << "ldevalvar"
                   << " name:" << id.AsIndex();

        uint64_t lexical_context = GetAccAsTaggedValue().GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::LdEvalVar(this->GetJSThread(), LoadFromConstantPool(id), lexical_context));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaStevalvar()
    {
        auto id = this->GetInst().template GetId<FORMAT>();
        auto v0 = this->GetInst().template GetVReg<FORMAT, 0>();

        LOG_INST() << "stevalvar"
                   << " name:" << id.AsIndex() << " v0:" << v0;

        uint64_t value = GetRegAsTaggedValue(v0).GetRawData();
        uint64_t lexical_context = GetAccAsTaggedValue().GetRawData();
        INTRINSIC_CALL_CHECK_SETACC(
            intrinsics::StEvalVar(this->GetJSThread(), LoadFromConstantPool(id), value, lexical_context));
        this->template MoveToNextInst<FORMAT, true>();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEcmaDebugger()
    {
        LOG_INST() << "debugger";
        this->template MoveToNextInst<FORMAT, true>();
    }

    ALWAYS_INLINE JSThread *GetJSThread()
    {
        return JSThread::Cast(this->GetThread());
    }

    void *GetProfileAddress()
    {
        auto method = static_cast<JSMethod *>(this->GetFrame()->GetMethod());
        // Profiling is not initialized
        if (method->GetProfileSize() == 0) {
            return nullptr;
        }
        auto prof_data = method->GetProfilingVector();
        auto prof_id = this->GetInst().GetProfileId();
        ASSERT(prof_id >= 0);
        ASSERT(method->GetProfileSize() > helpers::ToUnsigned(prof_id));
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto prof_value = reinterpret_cast<ObjByIndexOperationProfile::ValueType *>(&prof_data[prof_id]);

        return prof_value;
    }

    void UpdateTypeOfProfile(coretypes::TaggedType value)
    {
        auto method = static_cast<JSMethod *>(this->GetFrame()->GetMethod());
        // Profiling is not initialized
        if (method->GetProfileSize() == 0) {
            return;
        }
        auto prof_data = method->GetProfilingVector();
        auto prof_id = this->GetInst().GetProfileId();
        ASSERT(prof_id >= 0);
        ASSERT(method->GetProfileSize() > helpers::ToUnsigned(prof_id));
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto prof_value = reinterpret_cast<TypeOfOperationProfile::ValueType *>(&prof_data[prof_id]);
        TypeOfOperationProfile::Update(prof_value, JSTaggedValue(value));
    }

    void UpdateUnaryArithProfile(coretypes::TaggedType value)
    {
        auto method = static_cast<JSMethod *>(this->GetFrame()->GetMethod());
        // Profiling is not initialized
        if (method->GetProfileSize() == 0) {
            return;
        }
        auto prof_data = method->GetProfilingVector();
        auto prof_id = this->GetInst().GetProfileId();
        ASSERT(prof_id >= 0);
        ASSERT(method->GetProfileSize() > helpers::ToUnsigned(prof_id));
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto prof_value = reinterpret_cast<UnaryOperationProfile::ValueType *>(&prof_data[prof_id]);
        UnaryOperationProfile::Update(prof_value, JSTaggedValue(value));
    }

    template <bool IS_MOD = false>
    void UpdateBinaryArithProfile(coretypes::TaggedType lhs, coretypes::TaggedType rhs)
    {
        auto left = JSTaggedValue(lhs);
        auto right = JSTaggedValue(rhs);
        auto method = static_cast<JSMethod *>(this->GetFrame()->GetMethod());
        // Profiling is not initialized
        if (method->GetProfileSize() == 0) {
            return;
        }
        auto prof_data = method->GetProfilingVector();
        auto prof_id = this->GetInst().GetProfileId();
        ASSERT(prof_id >= 0);
        ASSERT(method->GetProfileSize() > helpers::ToUnsigned(prof_id));
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto prof_value = reinterpret_cast<BinaryOperationProfile::ValueType *>(&prof_data[prof_id]);
        // NOLINTNEXTLINE(readability-braces-around-statements,bugprone-suspicious-semicolon)
        if constexpr (IS_MOD) {
            BinaryOperationProfile::UpdateMod(prof_value, left, right);
        } else {
            BinaryOperationProfile::Update(prof_value, left, right);
        }
    }

    void UpdateCallProfile(ECMAObject *func)
    {
        auto js_method = (JSMethod *)this->GetFrame()->GetMethod();
        if (js_method->GetProfileSize() == 0) {
            return;
        }
        auto prof_id = this->GetInst().GetProfileId();
        ASSERT(prof_id != -1);
        ASSERT(static_cast<uint32_t>(prof_id) < js_method->GetProfileSize());
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto profile = CallProfile::FromBuffer(js_method->GetProfilingVector() + prof_id);
        ASSERT(profile != nullptr);
        auto *profile_table = this->GetJSThread()->GetEcmaVM()->GetEcmaCallProfileTable();
        profile->Update(func, profile_table);
    }

};  // InstructionHandler

}  // namespace panda::ecmascript

#endif  // PANDA_ECMASCRIPT_INTERPRETER_INL_H
