/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_PLUGINS_ECMASCRIPT_RUNTIME_INTERPRETER_JS_DECODE_CALL_INSTR_H
#define PANDA_PLUGINS_ECMASCRIPT_RUNTIME_INTERPRETER_JS_DECODE_CALL_INSTR_H

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_thread.h"

namespace panda::ecmascript {
template <BytecodeInstruction::Format FORMAT, BytecodeInstruction::Opcode OPCODE, bool IS_QUICKENED>
ALWAYS_INLINE inline uint32_t JSGetNumberActualArgsDyn([[maybe_unused]] BytecodeInstruction inst)
{
    using R = BytecodeInstructionResolver<IS_QUICKENED>;
    constexpr auto OP = R::template Get<OPCODE>();
    if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL0DYN_PREF_NONE_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_NONE_PROF16>());
        return js_method_args::FIRST_ARG_IDX + 0;
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL1DYN_PREF_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_PROF16>());
        return js_method_args::FIRST_ARG_IDX + 1;
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL2DYN_PREF_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_PROF16>());
        return js_method_args::FIRST_ARG_IDX + 2;
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL3DYN_PREF_V8_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_V8_PROF16>());
        return js_method_args::FIRST_ARG_IDX + 3;
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_CALLIRANGEDYN_PREF_IMM16_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_IMM16_V8_PROF16>());
        auto imm = inst.GetImm<FORMAT>();
        return js_method_args::FIRST_ARG_IDX + imm;
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL0THISDYN_PREF_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_PROF16>());
        return js_method_args::FIRST_ARG_IDX;
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL1THISDYN_PREF_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_PROF16>());
        return js_method_args::FIRST_ARG_IDX + 1;
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL2THISDYN_PREF_V8_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_V8_PROF16>());
        return js_method_args::FIRST_ARG_IDX + 2;
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_CALL3THISDYN_PREF_V8_V8_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_V8_V8_PROF16>());
        return js_method_args::FIRST_ARG_IDX + 3;
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_CALLITHISRANGEDYN_PREF_IMM16_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_IMM16_V8_PROF16>());
        auto imm = inst.GetImm<FORMAT>();
        ASSERT(imm >= 1);  // magic, 'this' is counted in range, 'func' - not
        return js_method_args::FIRST_ARG_IDX + (imm - 1);
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_NEWOBJDYNRANGE_PREF_IMM16_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_IMM16_V8_PROF16>());
        auto imm = inst.GetImm<FORMAT>();
        ASSERT(imm >= 2);  // magic, 'func' and 'new_target' are counted in range
        return js_method_args::FIRST_ARG_IDX + (imm - 2);
    } else {
        enum { IMPOSSIBLE_CASE = false };
        static_assert(IMPOSSIBLE_CASE, "Impossible case");
    }

    UNREACHABLE_CONSTEXPR();
    return 0;
}

template <BytecodeInstruction::Format FORMAT, BytecodeInstruction::Opcode OPCODE, bool IS_QUICKENED>
ALWAYS_INLINE inline int64_t JSGetCalleDyn([[maybe_unused]] Frame *frame, [[maybe_unused]] BytecodeInstruction inst)
{
    using R = BytecodeInstructionResolver<IS_QUICKENED>;
    constexpr auto OP = R::template Get<OPCODE>();

    if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL0DYN_PREF_NONE_PROF16>()) {
        return frame->GetAccAsVReg().GetValue();
    } else {
        return frame->GetVReg(inst.GetVReg<FORMAT, 0>()).GetValue();
    }

    UNREACHABLE_CONSTEXPR();
    return 0;
}

template <BytecodeInstruction::Format FORMAT, BytecodeInstruction::Opcode OPCODE, bool IS_QUICKENED>
ALWAYS_INLINE inline uint16_t JSGetCalleRangeStartDyn([[maybe_unused]] BytecodeInstruction inst)
{
    using R = BytecodeInstructionResolver<IS_QUICKENED>;
    constexpr auto OP = R::template Get<OPCODE>();

    if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALLIRANGEDYN_PREF_IMM16_V8_PROF16>() ||
                  OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALLITHISRANGEDYN_PREF_IMM16_V8_PROF16>() ||
                  OP == R::template Get<BytecodeInstruction::Opcode::ECMA_NEWOBJDYNRANGE_PREF_IMM16_V8_PROF16>()) {
        return inst.GetVReg<FORMAT, 0>();
    }

    UNREACHABLE_CONSTEXPR();
    return 0;
}

template <BytecodeInstruction::Format FORMAT, BytecodeInstruction::Opcode OPCODE, bool IS_QUICKENED>
ALWAYS_INLINE inline static void JSCopyArgumets(JSThread *thread, Frame *prev_frame, uint64_t raw_fn_object,
                                                BytecodeInstruction prev_inst, Frame *new_frame, uint32_t num_vregs,
                                                uint32_t num_actual_args)
{
    // ecma.call0dyn
    // ecma.call1dyn
    // ecma.call2dyn
    // ecma.call3dyn
    // ecma.callirangedyn:
    //   dyn_arg[0] - vreg[  0] [functional object]
    //   dyn_arg[1] - undefined [    new.target   ]
    //   dyn_arg[2] - undefined | global object
    //   dyn_arg[3] - vreg[  1] [    args[  0]    ]
    //   dyn_arg[4] - vreg[  2] [    args[  1]    ]
    //   dyn_arg[5] - vreg[  3] [    args[  2]    ]
    //     ...
    //   dyn_arg[N] - vreg[N-2] [    args[N-3]    ]

    // ecma.call0thisdyn
    // ecma.call1thisdyn
    // ecma.call2thisdyn
    // ecma.call3thisdyn
    // ecma.callithisrangedyn:
    //   dyn_arg[0] - vreg[  0] [functional object]
    //   dyn_arg[1] - undefined [    new.target   ]
    //   dyn_arg[2] - vreg[  1] [      this       ]
    //   dyn_arg[3] - vreg[  2] [    args[  0]    ]
    //   dyn_arg[4] - vreg[  3] [    args[  1]    ]
    //     ...
    //   dyn_arg[N] - vreg[N-1] [    args[N-3]    ]

    // Where vreg[N-1] is accumulator

    // ecma.newobjdynrange
    //   dyn_arg[0] - vreg[  0] [functional object]
    //   dyn_arg[1] - vreg[  1] [    new.target   ]
    //   dyn_arg[2] - allocated this, untouched
    //   dyn_arg[3] - vreg[  2] [    args[  0]    ]
    //   dyn_arg[4] - vreg[  3] [    args[  1]    ]
    //     ...
    //   dyn_arg[N] - vreg[N-1] [    args[N-3]    ]

    using R = BytecodeInstructionResolver<IS_QUICKENED>;
    constexpr auto OP = R::template Get<OPCODE>();

    ASSERT(num_actual_args >= js_method_args::FIRST_ARG_IDX);
    new_frame->GetVReg(num_vregs + js_method_args::FUNC_IDX).SetValue(raw_fn_object);
    if constexpr (OP != R::template Get<BytecodeInstruction::Opcode::ECMA_NEWOBJDYNRANGE_PREF_IMM16_V8_PROF16>()) {
        new_frame->GetVReg(num_vregs + js_method_args::NEW_TARGET_IDX).SetValue(JSTaggedValue::VALUE_UNDEFINED);
    }

    if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL0DYN_PREF_NONE_PROF16>() ||
                  OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL1DYN_PREF_V8_PROF16>() ||
                  OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL2DYN_PREF_V8_V8_PROF16>() ||
                  OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL3DYN_PREF_V8_V8_V8_PROF16>() ||
                  OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALLIRANGEDYN_PREF_IMM16_V8_PROF16>()) {
        JSTaggedValue fn_object(raw_fn_object);
        uint64_t this_arg = JSTaggedValue::VALUE_UNDEFINED;
        if (fn_object.IsJSFunction() && !JSFunction::Cast(fn_object.GetHeapObject())->IsStrict()) {
            this_arg = thread->GetGlobalObject().GetRawData();
        }
        new_frame->GetVReg(num_vregs + js_method_args::THIS_IDX).SetValue(this_arg);
    }

    if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL0DYN_PREF_NONE_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_NONE_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX);
        // Do nothing
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL1DYN_PREF_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX + 1);
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 0)
            .SetValue(prev_frame->GetAccAsVReg().GetValue());
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL2DYN_PREF_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX + 2);
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 0) = prev_frame->GetVReg(prev_inst.GetVReg(1));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 1)
            .SetValue(prev_frame->GetAccAsVReg().GetValue());
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL3DYN_PREF_V8_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_V8_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX + 3);
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 0) = prev_frame->GetVReg(prev_inst.GetVReg(1));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 1) = prev_frame->GetVReg(prev_inst.GetVReg(2));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 2)
            .SetValue(prev_frame->GetAccAsVReg().GetValue());
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_CALLIRANGEDYN_PREF_IMM16_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_IMM16_V8_PROF16>());
        uint16_t prev_v0 = JSGetCalleRangeStartDyn<FORMAT, OPCODE, IS_QUICKENED>(prev_inst);
        for (size_t i = 0; i < (num_actual_args - js_method_args::FIRST_ARG_IDX); ++i) {
            new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + i) = prev_frame->GetVReg(prev_v0 + 1 + i);
        }
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL0THISDYN_PREF_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX);
        new_frame->GetVReg(num_vregs + js_method_args::THIS_IDX).SetValue(prev_frame->GetAccAsVReg().GetValue());
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL1THISDYN_PREF_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX + 1);
        new_frame->GetVReg(num_vregs + js_method_args::THIS_IDX) = prev_frame->GetVReg(prev_inst.GetVReg(1));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 0)
            .SetValue(prev_frame->GetAccAsVReg().GetValue());
    } else if constexpr (OP == R::template Get<BytecodeInstruction::Opcode::ECMA_CALL2THISDYN_PREF_V8_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_V8_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX + 2);
        new_frame->GetVReg(num_vregs + js_method_args::THIS_IDX) = prev_frame->GetVReg(prev_inst.GetVReg(1));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 0) = prev_frame->GetVReg(prev_inst.GetVReg(2));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 1)
            .SetValue(prev_frame->GetAccAsVReg().GetValue());
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_CALL3THISDYN_PREF_V8_V8_V8_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_V8_V8_V8_V8_PROF16>());
        ASSERT(num_actual_args == js_method_args::FIRST_ARG_IDX + 3);
        new_frame->GetVReg(num_vregs + js_method_args::THIS_IDX) = prev_frame->GetVReg(prev_inst.GetVReg(1));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 0) = prev_frame->GetVReg(prev_inst.GetVReg(2));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 1) = prev_frame->GetVReg(prev_inst.GetVReg(3));
        new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + 2)
            .SetValue(prev_frame->GetAccAsVReg().GetValue());
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_CALLITHISRANGEDYN_PREF_IMM16_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_IMM16_V8_PROF16>());
        uint16_t prev_v0 = JSGetCalleRangeStartDyn<FORMAT, OPCODE, IS_QUICKENED>(prev_inst);
        new_frame->GetVReg(num_vregs + js_method_args::THIS_IDX) = prev_frame->GetVReg(prev_v0 + 1);
        for (size_t i = 0; i < (num_actual_args - js_method_args::FIRST_ARG_IDX); ++i) {
            new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + i) = prev_frame->GetVReg(prev_v0 + 2 + i);
        }
    } else if constexpr (OP ==
                         R::template Get<BytecodeInstruction::Opcode::ECMA_NEWOBJDYNRANGE_PREF_IMM16_V8_PROF16>()) {
        static_assert(FORMAT == R::template Get<BytecodeInstruction::Format::PREF_IMM16_V8_PROF16>());
        uint16_t prev_v0 = JSGetCalleRangeStartDyn<FORMAT, OPCODE, IS_QUICKENED>(prev_inst);
        new_frame->GetVReg(num_vregs + js_method_args::NEW_TARGET_IDX) = prev_frame->GetVReg(prev_v0 + 1);
        for (size_t i = 0; i < (num_actual_args - js_method_args::FIRST_ARG_IDX); ++i) {
            new_frame->GetVReg(num_vregs + js_method_args::FIRST_ARG_IDX + i) = prev_frame->GetVReg(prev_v0 + 2 + i);
        }
    } else {
        enum { IMPOSSIBLE_CASE = false };
        static_assert(IMPOSSIBLE_CASE, "Impossible case");
    }

    for (size_t i = num_vregs + num_actual_args; i < new_frame->GetSize(); ++i) {
        new_frame->GetVReg(i).SetValue(JSTaggedValue::VALUE_UNDEFINED);
    }
}
}  // namespace panda::ecmascript

#endif  // PANDA_PLUGINS_ECMASCRIPT_RUNTIME_INTERPRETER_JS_DECODE_CALL_INSTR_H
