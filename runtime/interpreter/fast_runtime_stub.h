/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_INTERPRETER_FAST_RUNTIME_STUB_H
#define ECMASCRIPT_INTERPRETER_FAST_RUNTIME_STUB_H

#include <memory>
#include "plugins/ecmascript/runtime/global_env_constants.h"
#include "plugins/ecmascript/runtime/js_hclass.h"

namespace panda::ecmascript {
class GlobalEnv;
class PropertyAttributes;

class FastRuntimeStub {
public:
    /* -------------- Common API Begin, Don't change those interface!!! ----------------- */
    static inline JSTaggedValue FastAdd(JSTaggedValue left, JSTaggedValue right);
    static inline JSTaggedValue FastSub(JSTaggedValue left, JSTaggedValue right);
    static inline JSTaggedValue FastMul(JSTaggedValue left, JSTaggedValue right);
    static inline JSTaggedValue FastDiv(JSTaggedValue left, JSTaggedValue right);
    static inline JSTaggedValue FastMod(JSTaggedValue left, JSTaggedValue right);
    static inline JSTaggedValue FastEqual(JSTaggedValue left, JSTaggedValue right);
    static inline JSTaggedValue FastTypeOf(JSThread *thread, JSTaggedValue obj);
    static inline bool FastStrictEqual(JSThread *thread, JSTaggedValue left, JSTaggedValue right);
    static inline JSTaggedValue NewLexicalEnvDyn(JSThread *thread, ObjectFactory *factory, uint16_t num_vars);
    static inline JSTaggedValue GetGlobalOwnProperty(JSTaggedValue receiver, JSTaggedValue key, bool *found);
    /* -------------- Special API For Multi-Language VM Begin ----------------- */
    static inline bool IsSpecialIndexedObjForGet(JSTaggedValue obj);
    static inline bool IsSpecialIndexedObjForSet(JSTaggedValue obj);
    static inline JSTaggedValue GetElement(JSTaggedValue receiver, uint32_t index);
    static inline JSTaggedValue GetElementWithArray(JSTaggedValue receiver, uint32_t index);
    static inline bool SetElement(JSThread *thread, JSTaggedValue receiver, uint32_t index, JSTaggedValue value,
                                  bool may_throw);
    static inline bool SetPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                         JSTaggedValue value, bool may_throw);
    static inline bool SetGlobalOwnProperty(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                            JSTaggedValue value, bool may_throw);

    // set property that is not accessor and is writable
    static inline void SetOwnPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                            JSTaggedValue value);
    // set element that is not accessor and is writable
    static inline bool SetOwnElement(JSThread *thread, JSTaggedValue receiver, uint32_t index, JSTaggedValue value);
    static inline bool FastSetProperty(JSThread *thread, JSHandle<JSTaggedValue> receiver, JSHandle<JSTaggedValue> key,
                                       JSHandle<JSTaggedValue> value, bool may_throw);
    static inline JSTaggedValue FastGetProperty(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                JSHandle<JSTaggedValue> key);
    static inline JSTaggedValue FindOwnProperty(JSThread *thread, JSObject *obj, TaggedArray *properties,
                                                JSTaggedValue key, PropertyAttributes *attr, uint32_t *index_or_entry);
    static inline JSTaggedValue FindOwnElement(TaggedArray *elements, uint32_t index, bool is_dict,
                                               PropertyAttributes *attr, uint32_t *index_or_entry);
    static inline JSTaggedValue FindOwnProperty(JSThread *thread, JSObject *obj, JSTaggedValue key);

    static inline JSTaggedValue FindOwnElement(JSObject *obj, uint32_t index);

    static inline JSTaggedValue HasOwnProperty(JSThread *thread, JSObject *obj, JSTaggedValue key);
    /* -------------- Special API For Multi-Language VM End ----------------- */
    /* -------------- Common API End, Don't change those interface!!! ----------------- */

    template <bool USE_OWN = false>
    static inline JSTaggedValue GetPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key);
    template <bool USE_OWN = false>
    static inline JSTaggedValue GetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                   JSHandle<JSTaggedValue> key);
    template <bool USE_OWN = false>
    static inline JSTaggedValue GetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                   void *prof = nullptr);
    template <bool USE_OWN = false>
    static inline JSTaggedValue SetPropertyByName(JSThread *thread, JSTaggedValue receiver, JSTaggedValue key,
                                                  JSTaggedValue value);
    template <bool USE_OWN = false>
    static inline JSTaggedValue SetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                   JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> value);
    template <bool USE_OWN = false>
    static inline JSTaggedValue SetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                   JSTaggedValue value, void *prof = nullptr);

    static inline bool FastSetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                              JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> value);
    static inline bool FastSetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                              JSTaggedValue value);
    static inline JSTaggedValue FastGetPropertyByName(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                      JSHandle<JSTaggedValue> key);
    static inline JSTaggedValue FastGetPropertyByValue(JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                       JSHandle<JSTaggedValue> key);
    template <bool USE_HOLE = false>
    static inline JSTaggedValue FastGetPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index);
    static inline PropertyAttributes AddPropertyByName(JSThread *thread, JSHandle<JSObject> obj_handle,
                                                       JSHandle<JSTaggedValue> key_handle,
                                                       JSHandle<JSTaggedValue> value_handle, PropertyAttributes attr);

private:
    friend class ICRuntimeStub;
    static inline bool IsSpecialIndexedObj(JSType js_type);
    static inline bool IsSpecialReceiverObj(JSType js_type);
    static inline bool IsSpecialContainer(JSType js_type);
    static inline uint32_t TryToElementsIndex(JSTaggedValue key);
    static inline JSTaggedValue CallGetter(JSThread *thread, JSTaggedValue receiver, JSTaggedValue holder,
                                           JSTaggedValue value);
    static inline JSTaggedValue CallSetter(JSThread *thread, JSTaggedValue receiver, JSTaggedValue value,
                                           JSTaggedValue accessor_value);
    static inline bool ShouldCallSetter(JSTaggedValue receiver, JSTaggedValue holder, JSTaggedValue accessor_value,
                                        PropertyAttributes attr);
    static inline JSTaggedValue AddPropertyByIndex(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                   JSTaggedValue value);
    static inline JSTaggedValue GetContainerProperty(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                     JSType js_type);
    static inline JSTaggedValue SetContainerProperty(JSThread *thread, JSTaggedValue receiver, uint32_t index,
                                                     JSTaggedValue value, JSType js_type);
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_INTERPRETER_OBJECT_OPERATOR_INL_H
