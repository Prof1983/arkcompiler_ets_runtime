/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "slow_runtime_helper.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/ecma-interpreter.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter-inl.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript {

#ifdef PANDA_QEMU_AARCH64_GCC_8
// Build PANDA_QEMU_AARCH64_GCC_8 hangs without this workaround. Please check issue #8094.
JSTaggedValue GetGlobalObject(JSThread *thread)
{
    return thread->GetGlobalObject();
}
#endif

JSTaggedValue SlowRuntimeHelper::CallBoundFunction(EcmaRuntimeCallInfo *info)
{
    JSThread *thread = info->GetThread();
    JSHandle<JSBoundFunction> bound_func(info->GetFunction());
    JSHandle<JSFunction> target_func(thread, bound_func->GetBoundTarget());
    if (target_func->IsClassConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "class constructor cannot called without 'new'",
                                    JSTaggedValue::Exception());
    }

    JSHandle<TaggedArray> bound_args(thread, bound_func->GetBoundArguments());
    const uint32_t bound_length = bound_args->GetLength();
    const uint32_t args_length = info->GetArgsNumber();
    JSHandle<JSTaggedValue> undefined = thread->GlobalConstants()->GetHandledUndefined();
    auto runtime_info = NewRuntimeCallInfo(thread, JSHandle<JSTaggedValue>(target_func), info->GetThis(), undefined,
                                           bound_length + args_length);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (bound_length != 0) {
        // 0 ~ boundLength is boundArgs; boundLength ~ argsLength is args of EcmaRuntimeCallInfo.
        runtime_info->SetCallArg(bound_length, bound_args->GetData());
    }
    if (args_length != 0) {
        runtime_info->SetCallArg(
            args_length, reinterpret_cast<JSTaggedType *>(info->GetArgAddress(js_method_args::NUM_MANDATORY_ARGS)),
            bound_length);
    }
    return EcmaInterpreter::Execute(runtime_info.Get());
}

JSTaggedValue SlowRuntimeHelper::NewObject(EcmaRuntimeCallInfo *info)
{
    JSThread *thread = info->GetThread();
    JSHandle<JSTaggedValue> func(info->GetFunction());
    if (!func->IsHeapObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "function is nullptr", JSTaggedValue::Exception());
    }

    if (!func->IsJSFunction()) {
        if (func->IsBoundFunction()) {
            JSTaggedValue result = JSBoundFunction::ConstructInternal(info);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            return result;
        }

        if (func->IsJSProxy()) {
            JSTaggedValue js_obj = JSProxy::ConstructInternal(info);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            return js_obj;
        }
        THROW_TYPE_ERROR_AND_RETURN(thread, "Constructed NonConstructable", JSTaggedValue::Exception());
    }

    JSTaggedValue result = JSFunction::ConstructInternal(info);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result;
}

void SlowRuntimeHelper::SaveFrameToContext(JSThread *thread, JSHandle<GeneratorContext> context)
{
    auto frame = thread->GetCurrentFrame();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    uint32_t nregs = frame->GetSize();
    JSHandle<TaggedArray> regs_array = factory->NewTaggedArray(nregs);
    for (uint32_t i = 0; i < nregs; i++) {
        regs_array->Set(thread, i, VRegAsTaggedValue(frame->GetVReg(i)));
    }
    auto num_vregs = frame->GetMethod()->GetNumVregs();
    auto func = VRegAsTaggedValue(frame->GetVReg(num_vregs));
    auto acc = VRegAsTaggedValue(frame->GetAcc());

    context->SetRegsArray(thread, regs_array.GetTaggedValue());
    context->SetMethod(thread, func);

    context->SetAcc(thread, acc);
    context->SetNRegs(thread, JSTaggedValue(nregs));
    context->SetBCOffset(thread, JSTaggedValue(frame->GetBytecodeOffset()));
    context->SetLexicalEnv(thread, JSTaggedValue(JSFrame::GetJSEnv(frame)->GetLexicalEnv()));
}

JSTaggedValue ConstructGeneric(JSThread *thread, JSHandle<JSFunction> ctor, JSHandle<JSTaggedValue> new_tgt,
                               JSHandle<JSTaggedValue> pre_args, uint32_t args_count, JSTaggedType *stkargs)
{
    if (!ctor->IsConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Constructor is false", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> obj(thread, JSTaggedValue::Undefined());
    if (ctor->IsBase()) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        obj = JSHandle<JSTaggedValue>(factory->NewJSObjectByConstructor(ctor, new_tgt));
    }
    ASSERT(ctor->GetCallTarget());

    uint32_t pre_args_size = pre_args->IsUndefined() ? 0 : JSHandle<TaggedArray>::Cast(pre_args)->GetLength();
    const uint32_t size = pre_args_size + args_count;
    auto info = NewRuntimeCallInfo(thread, ctor, obj, new_tgt, size);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // add preArgs when boundfunction is encountered
    uint32_t arg_idx = 0;
    if (pre_args_size > 0) {
        JSHandle<TaggedArray> tga_pre_args = JSHandle<TaggedArray>::Cast(pre_args);
        for (uint32_t i = 0; i < pre_args_size; ++i) {
            info->SetCallArg(arg_idx++, tga_pre_args->Get(i));
        }
    }
    for (uint32_t i = 0; i < args_count; ++i) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        info->SetCallArg(arg_idx++, JSTaggedValue(stkargs[i]));
    }
    JSTaggedValue result_value = EcmaInterpreter::Execute(info.Get());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 9.3.2 [[Construct]] (argumentsList, new_target)
    if (result_value.IsECMAObject()) {
        return result_value;
    }

    if (ctor->IsBase()) {
        return obj.GetTaggedValue();
    }
    if (!result_value.IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "function is non-constructor", JSTaggedValue::Exception());
    }
    return obj.GetTaggedValue();
}

JSTaggedValue ConstructBoundFunction(JSThread *thread, JSHandle<JSBoundFunction> ctor, JSHandle<JSTaggedValue> new_tgt,
                                     JSHandle<JSTaggedValue> pre_args, uint32_t args_count, JSTaggedType *stkargs)
{
    JSHandle<JSTaggedValue> target(thread, ctor->GetBoundTarget());
    ASSERT(target->IsConstructor());

    JSHandle<TaggedArray> bound_args(thread, ctor->GetBoundArguments());
    JSMutableHandle<JSTaggedValue> new_pre_args(thread, pre_args.GetTaggedValue());
    if (new_pre_args->IsUndefined()) {
        new_pre_args.Update(bound_args.GetTaggedValue());
    } else {
        new_pre_args.Update(
            TaggedArray::Append(thread, bound_args, JSHandle<TaggedArray>::Cast(pre_args)).GetTaggedValue());
    }
    JSMutableHandle<JSTaggedValue> new_target_mutable(thread, new_tgt.GetTaggedValue());
    if (JSTaggedValue::SameValue(ctor.GetTaggedValue(), new_tgt.GetTaggedValue())) {
        new_target_mutable.Update(target.GetTaggedValue());
    }
    return SlowRuntimeHelper::Construct(thread, target, new_target_mutable, new_pre_args, args_count, stkargs);
}

JSTaggedValue ConstructProxy(JSThread *thread, JSHandle<JSProxy> ctor, JSHandle<JSTaggedValue> new_tgt,
                             JSHandle<JSTaggedValue> pre_args, uint32_t args_count, JSTaggedType *stkargs)
{
    // step 1 ~ 4 get ProxyHandler and ProxyTarget
    JSHandle<JSTaggedValue> handler(thread, ctor->GetHandler());
    if (handler->IsNull()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Constructor: handler is null", JSTaggedValue::Exception());
    }
    ASSERT(handler->IsJSObject());
    JSHandle<JSTaggedValue> target(thread, ctor->GetTarget());

    // 5.Let trap be GetMethod(handler, "construct").
    JSHandle<JSTaggedValue> key(thread->GlobalConstants()->GetHandledProxyConstructString());
    JSHandle<JSTaggedValue> method = JSObject::GetMethod(thread, handler, key);

    // 6.ReturnIfAbrupt(trap).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 7.If trap is undefined, then
    //   a.Assert: target has a [[Construct]] internal method.
    //   b.Return Construct(target, argumentsList, new_target).
    if (method->IsUndefined()) {
        ASSERT(target->IsConstructor());
        return SlowRuntimeHelper::Construct(thread, target, new_tgt, pre_args, args_count, stkargs);
    }

    // 8.Let argArray be CreateArrayFromList(argumentsList).
    uint32_t pre_args_size = pre_args->IsUndefined() ? 0 : JSHandle<TaggedArray>::Cast(pre_args)->GetLength();
    const uint32_t size = pre_args_size + args_count;
    JSHandle<TaggedArray> args = thread->GetEcmaVM()->GetFactory()->NewTaggedArray(size);
    if (pre_args_size > 0) {
        JSHandle<TaggedArray> tga_pre_args = JSHandle<TaggedArray>::Cast(pre_args);
        for (uint32_t i = 0; i < pre_args_size; ++i) {
            JSTaggedValue value = tga_pre_args->Get(i);
            args->Set(thread, i, value);
        }
    }
    for (ArraySizeT i = 0; i < args_count; ++i) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        args->Set(thread, i + pre_args_size, JSTaggedValue(stkargs[i]));
    }

    // step 8 ~ 9 Call(trap, handler, «target, argArray, new_target »).

    auto info = NewRuntimeCallInfo(thread, method, handler, JSTaggedValue::Undefined(), 3);
    info->SetCallArgs(target, JSHandle<JSTaggedValue>(args), new_tgt);
    JSTaggedValue new_obj_value = JSFunction::Call(info.Get());  // 3: «target, argArray, new_target »
    // 10.ReturnIfAbrupt(newObj).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 11.If Type(newObj) is not Object, throw a TypeError exception.
    if (!new_obj_value.IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "new object is not object", JSTaggedValue::Exception());
    }
    // 12.Return newObj.
    return new_obj_value;
}

JSTaggedValue SlowRuntimeHelper::Construct(JSThread *thread, JSHandle<JSTaggedValue> ctor,
                                           JSHandle<JSTaggedValue> new_target, JSHandle<JSTaggedValue> pre_args,
                                           uint32_t args_count, JSTaggedType *stkargs)
{
    if (new_target->IsUndefined()) {
        new_target = ctor;
    }

    if (!(new_target->IsConstructor() && ctor->IsConstructor())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Constructor is false", JSTaggedValue::Exception());
    }
    if (ctor->IsJSFunction()) {
        return ConstructGeneric(thread, JSHandle<JSFunction>::Cast(ctor), new_target, pre_args, args_count, stkargs);
    }
    if (ctor->IsBoundFunction()) {
        return ConstructBoundFunction(thread, JSHandle<JSBoundFunction>::Cast(ctor), new_target, pre_args, args_count,
                                      stkargs);
    }
    if (ctor->IsJSProxy()) {
        return ConstructProxy(thread, JSHandle<JSProxy>::Cast(ctor), new_target, pre_args, args_count, stkargs);
    }
    THROW_TYPE_ERROR_AND_RETURN(thread, "Constructor NonConstructor", JSTaggedValue::Exception());
}

JSTaggedValue SlowRuntimeHelper::FindPrivateKey(JSThread *thread,
                                                const JSHandle<JSConstructorFunction> &private_context,
                                                const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                                                PropertyDescriptor &prop_desc, PrivateFieldKind &kind)
{
    JSHandle<TaggedArray> desc(thread, private_context->GetPrivateFields());

    for (ArraySizeT i = 0; i < desc->GetLength();) {
        JSHandle<JSTaggedValue> kind_value(thread, desc->Get(thread, i++));
        kind = static_cast<PrivateFieldKind>(kind_value->GetInt());
        JSHandle<JSSymbol> private_symbol(thread, desc->Get(thread, i++));

        switch (kind) {
            case PrivateFieldKind::FIELD:
            case PrivateFieldKind::STATIC_FIELD: {
                break;
            }
            default: {
                i++;
            }
        }

        if (!JSTaggedValue::Equal(thread, JSHandle<JSTaggedValue>(thread, private_symbol->GetDescription()), key)) {
            continue;
        }

        bool exists = JSObject::GetOwnProperty(thread, obj, JSHandle<JSTaggedValue>::Cast(private_symbol), prop_desc);

        if (!exists) {
            prop_desc.SetValue(JSHandle<JSTaggedValue>(thread, JSTaggedValue::Hole()));
        }

        return private_symbol.GetTaggedValue();
    }

    prop_desc.SetValue(JSHandle<JSTaggedValue>(thread, JSTaggedValue::Hole()));
    return JSTaggedValue::Undefined();
}

}  // namespace panda::ecmascript
