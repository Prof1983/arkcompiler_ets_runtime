/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/interpreter/slow_runtime_stub.h"

#include "js_tagged_value.h"
#include "lexical_env.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/builtins/builtins_regexp.h"
#include "plugins/ecmascript/runtime/class_linker/program_object-inl.h"
#include "plugins/ecmascript/runtime/ecma_module.h"
#include "plugins/ecmascript/runtime/global_dictionary-inl.h"
#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/ecma-interpreter.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/interpreter/slow_runtime_helper.h"
#include "plugins/ecmascript/runtime/js_arguments.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_async_from_sync_iterator_object.h"
#include "plugins/ecmascript/runtime/js_async_function.h"
#include "plugins/ecmascript/runtime/js_async_generator_object.h"
#include "plugins/ecmascript/runtime/js_for_in_iterator.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_generator_object.h"
#include "plugins/ecmascript/runtime/js_hclass-inl.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_proxy.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/runtime/runtime_call_id.h"
#include "plugins/ecmascript/runtime/template_string.h"
#include "plugins/ecmascript/runtime/vmstat/runtime_stat.h"
#include "libpandabase/utils/utf.h"

namespace panda::ecmascript {
JSTaggedValue SlowRuntimeStub::CallSpreadDyn(JSThread *thread, JSTaggedValue func, JSTaggedValue obj,
                                             JSTaggedValue array)
{
    INTERPRETER_TRACE(thread, CallSpreadDyn);
    if ((!obj.IsUndefined() && !obj.IsECMAObject()) || !func.IsJSFunction() || !array.IsJSArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "cannot Callspread", JSTaggedValue::Exception());
    }
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSFunction> js_func(thread, func);
    JSHandle<JSTaggedValue> js_array(thread, array);
    JSHandle<JSTaggedValue> tagged_obj(thread, obj);

    JSHandle<TaggedArray> coretypes_array(thread, GetCallSpreadArgs(thread, js_array.GetTaggedValue()));

    auto info =
        NewRuntimeCallInfo(thread, js_func, tagged_obj, JSTaggedValue::Undefined(), coretypes_array->GetLength());
    info->SetCallArg(coretypes_array->GetLength(), coretypes_array->GetData());
    return InvokeJsFunction(info.Get());
}

JSTaggedValue SlowRuntimeStub::NegDyn(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, NegDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> input(thread, value);
    JSHandle<JSTaggedValue> input_val = JSTaggedValue::ToNumeric(thread, input);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    if (input_val->IsBigInt()) {
        JSHandle<BigInt> big_value(input_val);
        return BigInt::UnaryMinus(thread, big_value).GetTaggedValue();
    }

    if (input_val->IsInt()) {
        int32_t int_value = input_val->GetInt();
        if (int_value == 0) {
            return JSTaggedValue(-0.0);
        }
        return JSTaggedValue(-int_value);
    }
    if (input_val->IsDouble()) {
        return JSTaggedValue(-input_val->GetDouble());
    }

    UNREACHABLE();
}

JSTaggedValue SlowRuntimeStub::AsyncFunctionEnter(JSThread *thread)
{
    INTERPRETER_TRACE(thread, AsyncFunctionEnter);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 1. create promise
    JSHandle<GlobalEnv> global_env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> promise_func = global_env->GetPromiseFunction();

    JSHandle<JSPromise> promise_object =
        JSHandle<JSPromise>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(promise_func), promise_func));
    promise_object->SetPromiseState(thread, JSTaggedValue(static_cast<int32_t>(PromiseStatus::PENDING)));
    // 2. create asyncfuncobj
    JSHandle<JSAsyncFuncObject> async_func_obj = factory->NewJSAsyncFuncObject();
    async_func_obj->SetPromise(thread, promise_object);

    JSHandle<GeneratorContext> context = factory->NewGeneratorContext();
    context->SetGeneratorObject(thread, async_func_obj);

    // change state to EXECUTING
    async_func_obj->SetState(thread, JSGeneratorState::EXECUTING);
    async_func_obj->SetGeneratorContext(thread, context);

    // 3. return asyncfuncobj
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return async_func_obj.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::ToNumber(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, Tonumber);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> number(thread, value);
    // may return exception
    return JSTaggedValue::ToNumeric(thread, number).GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::NotDyn(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, NotDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> input_tag(thread, value);
    JSHandle<JSTaggedValue> input_val = JSTaggedValue::ToNumeric(thread, input_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (input_val->IsBigInt()) {
        JSHandle<BigInt> big_value(input_val);
        return BigInt::BitwiseNOT(thread, big_value).GetTaggedValue();
    }
    int32_t number = JSTaggedValue::ToInt32(thread, input_val);
    return JSTaggedValue(~number);  // NOLINT(hicpp-signed-bitwise)
}

JSTaggedValue SlowRuntimeStub::IncDyn(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, IncDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> input_tag(thread, value);
    JSHandle<JSTaggedValue> input_val = JSTaggedValue::ToNumeric(thread, input_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (input_val->IsBigInt()) {
        JSHandle<BigInt> big_value(input_val);
        return BigInt::BigIntAddOne(thread, big_value).GetTaggedValue();
    }
    JSTaggedNumber number(input_val.GetTaggedValue());
    return (++number);
}

JSTaggedValue SlowRuntimeStub::DecDyn(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, DecDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> input_tag(thread, value);
    JSHandle<JSTaggedValue> input_val = JSTaggedValue::ToNumeric(thread, input_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (input_val->IsBigInt()) {
        JSHandle<BigInt> big_value(input_val);
        return BigInt::BigIntSubOne(thread, big_value).GetTaggedValue();
    }
    JSTaggedNumber number(input_val.GetTaggedValue());
    return (--number);
}

void SlowRuntimeStub::ThrowDyn(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, ThrowDyn);

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> obj(thread, value);
    JSHandle<ObjectWrapper> wrapper_object = factory->NewObjectWrapper(obj);

    thread->SetException(wrapper_object.GetTaggedValue());
}

JSTaggedValue SlowRuntimeStub::GetPropIterator(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, GetPropIterator);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, value);
    JSHandle<JSForInIterator> iterator_handle = JSObject::EnumerateObjectProperties(thread, obj_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    iterator_handle->SetFastRemainingIndex(JSTaggedNumber(0));
    return iterator_handle.GetTaggedValue();
}

void SlowRuntimeStub::ThrowConstAssignment(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, ThrowConstAssignment);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<EcmaString> name(thread, value.GetTaggedObject());
    JSHandle<EcmaString> info = factory->NewFromCanBeCompressString("Assignment to const variable ");

    JSHandle<EcmaString> msg = factory->ConcatFromString(info, name);
    THROW_NEW_ERROR_AND_RETURN(thread, factory->NewJSError(base::ErrorType::TYPE_ERROR, msg).GetTaggedValue());
}

JSTaggedValue SlowRuntimeStub::Add2Dyn(JSThread *thread, EcmaVM *ecma_vm, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, Add2Dyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);
    if (left_value->IsString() && right_value->IsString()) {
        EcmaString *new_string =
            EcmaString::Concat(JSHandle<EcmaString>(left_value), JSHandle<EcmaString>(right_value), ecma_vm);
        return JSTaggedValue(new_string);
    }
    JSHandle<JSTaggedValue> primitive_a0(thread, JSTaggedValue::ToPrimitive(thread, left_value));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> primitive_a1(thread, JSTaggedValue::ToPrimitive(thread, right_value));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // contain string
    if (primitive_a0->IsString() || primitive_a1->IsString()) {
        JSHandle<EcmaString> string_a0 = JSTaggedValue::ToString(thread, primitive_a0);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<EcmaString> string_a1 = JSTaggedValue::ToString(thread, primitive_a1);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        EcmaString *new_string = EcmaString::Concat(string_a0, string_a1, ecma_vm);
        return JSTaggedValue(new_string);
    }

    JSHandle<JSTaggedValue> val_left = JSTaggedValue::ToNumeric(thread, primitive_a0);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> val_right = JSTaggedValue::ToNumeric(thread, primitive_a1);

    if (val_left->IsBigInt() || val_right->IsBigInt()) {
        if (val_left->IsBigInt() && val_right->IsBigInt()) {
            JSHandle<BigInt> big_left(val_left);
            JSHandle<BigInt> big_right(val_right);
            return BigInt::Add(thread, big_left, big_right).GetTaggedValue();
        }
        return ThrowTypeError(thread, "Cannot mix BigInt and other types, use explicit conversions");
    }

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double a0_double = val_left->GetNumber();
    double a1_double = val_right->GetNumber();
    return JSTaggedValue(a0_double + a1_double);
}

JSTaggedValue SlowRuntimeStub::Sub2Dyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, Sub2Dyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_handle(thread, left);
    JSHandle<JSTaggedValue> right_handle(thread, right);

    JSHandle<JSTaggedValue> val_left = JSTaggedValue::ToNumeric(thread, left_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> val_right = JSTaggedValue::ToNumeric(thread, right_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    if (val_left->IsBigInt() || val_right->IsBigInt()) {
        if (val_left->IsBigInt() && val_right->IsBigInt()) {
            JSHandle<BigInt> big_left(val_left);
            JSHandle<BigInt> big_right(val_right);
            return BigInt::Subtract(thread, big_left, big_right).GetTaggedValue();
        }
        return ThrowTypeError(thread, "Cannot mix BigInt and other types, use explicit conversions");
    }

    JSTaggedNumber number0(val_left.GetTaggedValue());
    JSTaggedNumber number1(val_right.GetTaggedValue());
    return number0 - number1;
}

JSTaggedValue SlowRuntimeStub::Mul2Dyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, Mul2Dyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);

    // 6. Let lnum be ToNumeric(leftValue).
    JSHandle<JSTaggedValue> val_left = JSTaggedValue::ToNumeric(thread, left_value);
    // 7. ReturnIfAbrupt(lnum).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 8. Let rnum be ToNumeric(rightValue).
    JSHandle<JSTaggedValue> val_right = JSTaggedValue::ToNumeric(thread, right_value);
    // 9. ReturnIfAbrupt(rnum).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    if (val_left->IsBigInt() || val_right->IsBigInt()) {
        if (val_left->IsBigInt() && val_right->IsBigInt()) {
            JSHandle<BigInt> big_left(val_left);
            JSHandle<BigInt> big_right(val_right);
            return BigInt::Multiply(thread, big_left, big_right).GetTaggedValue();
        }
        return ThrowTypeError(thread, "Cannot mix BigInt and other types, use explicit conversions");
    }

    // 12.6.3.1 Applying the * Operator
    JSTaggedNumber number0(val_left.GetTaggedValue());
    JSTaggedNumber number1(val_right.GetTaggedValue());
    return number0 * number1;
}

JSTaggedValue SlowRuntimeStub::Div2Dyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, Div2Dyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_handle(thread, left);
    JSHandle<JSTaggedValue> right_handle(thread, right);

    JSHandle<JSTaggedValue> val_left = JSTaggedValue::ToNumeric(thread, left_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> val_right = JSTaggedValue::ToNumeric(thread, right_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    if (val_left->IsBigInt() || val_right->IsBigInt()) {
        if (val_left->IsBigInt() && val_right->IsBigInt()) {
            JSHandle<BigInt> big_left(val_left);
            JSHandle<BigInt> big_right(val_right);
            return BigInt::Divide(thread, big_left, big_right).GetTaggedValue();
        }
        return ThrowTypeError(thread, "Cannot mix BigInt and other types, use explicit conversions");
    }

    double d_left = val_left->GetNumber();
    double d_right = val_right->GetNumber();

    if (d_right == 0) {
        if (d_left == 0 || std::isnan(d_left)) {
            return JSTaggedValue(base::NAN_VALUE);
        }
        bool positive = (((bit_cast<uint64_t>(d_right)) & base::DOUBLE_SIGN_MASK) ==
                         ((bit_cast<uint64_t>(d_left)) & base::DOUBLE_SIGN_MASK));
        return JSTaggedValue(positive ? base::POSITIVE_INFINITY : -base::POSITIVE_INFINITY);
    }
    return JSTaggedValue(d_left / d_right);
}

JSTaggedValue SlowRuntimeStub::Mod2Dyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, Mod2Dyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_handle(thread, left);
    JSHandle<JSTaggedValue> right_handle(thread, right);

    JSHandle<JSTaggedValue> val_left = JSTaggedValue::ToNumeric(thread, left_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> val_right = JSTaggedValue::ToNumeric(thread, right_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    if (val_left->IsBigInt() || val_right->IsBigInt()) {
        if (val_left->IsBigInt() && val_right->IsBigInt()) {
            JSHandle<BigInt> left_bigint(val_left);
            JSHandle<BigInt> right_bigint(val_right);
            return BigInt::Remainder(thread, left_bigint, right_bigint).GetTaggedValue();
        }
        return ThrowTypeError(thread, "Cannot mix BigInt and other types, use explicit conversions");
    }

    double d_left = val_left->GetNumber();
    double d_right = val_right->GetNumber();

    // 12.6.3.3 Applying the % Operator
    if ((d_right == 0.0) || std::isnan(d_right) || std::isnan(d_left) || std::isinf(d_left)) {
        return JSTaggedValue(base::NAN_VALUE);
    }
    if ((d_left == 0.0) || std::isinf(d_right)) {
        return JSTaggedValue(d_left);
    }

    return JSTaggedValue(std::fmod(d_left, d_right));
}

JSTaggedValue SlowRuntimeStub::EqDyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, EqDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);
    bool ret = JSTaggedValue::Equal(thread, left_value, right_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return (ret ? JSTaggedValue::True() : JSTaggedValue::False());
}

JSTaggedValue SlowRuntimeStub::NotEqDyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, NotEqDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);
    bool ret = JSTaggedValue::Equal(thread, left_value, right_value);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return (ret ? JSTaggedValue::False() : JSTaggedValue::True());
}

JSTaggedValue SlowRuntimeStub::LessDyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, LessDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);
    bool ret = JSTaggedValue::Compare(thread, left_value, right_value) == ComparisonResult::LESS;
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return (ret ? JSTaggedValue::True() : JSTaggedValue::False());
}

JSTaggedValue SlowRuntimeStub::LessEqDyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, LessEqDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);
    bool ret = JSTaggedValue::Compare(thread, left_value, right_value) <= ComparisonResult::EQUAL;
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return (ret ? JSTaggedValue::True() : JSTaggedValue::False());
}

JSTaggedValue SlowRuntimeStub::GreaterDyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, GreaterDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);
    bool ret = JSTaggedValue::Compare(thread, left_value, right_value) == ComparisonResult::GREAT;
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return (ret ? JSTaggedValue::True() : JSTaggedValue::False());
}

JSTaggedValue SlowRuntimeStub::GreaterEqDyn(JSThread *thread, JSTaggedValue left, JSTaggedValue right)
{
    INTERPRETER_TRACE(thread, GreaterEqDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> left_value(thread, left);
    JSHandle<JSTaggedValue> right_value(thread, right);
    ComparisonResult comparison = JSTaggedValue::Compare(thread, left_value, right_value);
    bool ret = (comparison == ComparisonResult::GREAT) || (comparison == ComparisonResult::EQUAL);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return (ret ? JSTaggedValue::True() : JSTaggedValue::False());
}

JSTaggedValue SlowRuntimeStub::ToJSTaggedValueWithInt32(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, ToJSTaggedValueWithInt32);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    int32_t res = JSTaggedValue::ToInt32(thread, value_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue(res);
}

JSTaggedValue SlowRuntimeStub::ToJSTaggedValueWithUint32(JSThread *thread, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, ToJSTaggedValueWithUint32);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    int32_t res = JSTaggedValue::ToUint32(thread, value_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue(res);
}

JSTaggedValue SlowRuntimeStub::DelObjProp(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop)
{
    INTERPRETER_TRACE(thread, Delobjprop);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    JSHandle<JSTaggedValue> js_obj(JSTaggedValue::ToObject(thread, obj_handle));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> prop_key = JSTaggedValue::ToPropertyKey(thread, prop_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    bool ret = JSTaggedValue::DeletePropertyOrThrow(thread, js_obj, prop_key);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue(ret);
}

JSTaggedValue SlowRuntimeStub::NewObjDynRange(JSThread *thread, uint16_t args_count, JSTaggedValue func,
                                              JSTaggedValue new_target, JSTaggedType *stkargs)
{
    INTERPRETER_TRACE(thread, NewobjDynrange);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> func_handle(thread, JSTaggedValue(func));
    JSHandle<JSTaggedValue> new_target_handle(thread, JSTaggedValue(new_target));

    JSHandle<JSTaggedValue> pre_args(thread, JSTaggedValue::Undefined());
    auto tagged = SlowRuntimeHelper::Construct(thread, func_handle, new_target_handle, pre_args, args_count, stkargs);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return tagged;
}

JSTaggedValue SlowRuntimeStub::CreateObjectWithExcludedKeys(JSThread *thread, uint16_t num_keys, JSTaggedValue obj_val,
                                                            JSTaggedType *stkargs)
{
    INTERPRETER_TRACE(thread, CreateObjectWithExcludedKeys);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> obj_tval(thread, obj_val);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> from_value(thread, obj_val);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 3. If source is undefined or null, return target.
    if (from_value->IsNull() || from_value->IsUndefined()) {
        return obj_val;
    }
    // 4. Let from be ! ToObject(source).
    JSHandle<JSObject> from = JSTaggedValue::ToObject(thread, from_value);
    ASSERT_NO_ABRUPT_COMPLETION(thread);

    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    JSTaggedValue vreg(stkargs[0]);
    ArraySizeT num_excluded_keys = vreg.IsUndefined() ? 0 : num_keys + 1;
    // 5.Let keys be obj.[[OwnPropertyKeys]]().
    JSHandle<TaggedArray> from_keys = JSTaggedValue::GetOwnPropertyKeys(thread, JSHandle<JSTaggedValue>::Cast(from));
    // 6. For each element nextKey of keys, do
    JSHandle<JSObject> new_obj = factory->NewEmptyJSObject();

    bool excluded = true;
    for (ArraySizeT i = 0; i < from_keys->GetLength(); i++) {
        JSMutableHandle<JSTaggedValue> key(thread, from_keys->Get(i));
        // a. Let excluded be false.
        excluded = false;
        // b. For each element e of excludedItems, do
        for (ArraySizeT e = 0; e < num_excluded_keys; e++) {
            // i. If SameValue(e, nextKey) is true, then
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            JSTaggedValue ereg(stkargs[e]);
            if (JSTaggedValue::SameValue(key, JSHandle<JSTaggedValue>(thread, ereg))) {
                // 1. Set excluded to true.
                excluded = true;
                break;
            }
        }
        // c. If excluded is false, then
        if (!excluded) {
            // i. Let desc be ? from.[[GetOwnProperty]](nextKey).
            PropertyDescriptor desc(thread);
            bool status = JSTaggedValue::GetOwnProperty(thread, JSHandle<JSTaggedValue>(from), key, desc);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // ii. If desc is not undefined and desc.[[Enumerable]] is true, then
            if (status && desc.IsEnumerable()) {
                JSHandle<JSTaggedValue> prop_value =
                    JSTaggedValue::GetProperty(thread, JSHandle<JSTaggedValue>(from), key).GetValue();
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                JSObject::CreateDataPropertyOrThrow(thread, new_obj, key, prop_value);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
        }
    }
    return new_obj.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::ExpDyn(JSThread *thread, JSTaggedValue base, JSTaggedValue exponent)
{
    INTERPRETER_TRACE(thread, ExpDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> base_tag(thread, base);
    JSHandle<JSTaggedValue> exponent_tag(thread, exponent);
    JSHandle<JSTaggedValue> val_base = JSTaggedValue::ToNumeric(thread, base_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSTaggedValue> val_exponent = JSTaggedValue::ToNumeric(thread, exponent_tag);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    if (val_base->IsBigInt() || val_exponent->IsBigInt()) {
        if (val_base->IsBigInt() && val_exponent->IsBigInt()) {
            JSHandle<BigInt> big_base_value(val_base);
            JSHandle<BigInt> big_exponent_value(val_exponent);
            return BigInt::Exponentiate(thread, big_base_value, big_exponent_value).GetTaggedValue();
        }
        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot mix BigInt and other types, use explicit conversions",
                                    JSTaggedValue::Exception());
    }

    return base::NumberHelper::Pow(base_tag->GetNumber(), exponent.GetNumber());
}

JSTaggedValue SlowRuntimeStub::IsInDyn(JSThread *thread, JSTaggedValue prop, JSTaggedValue obj)
{
    INTERPRETER_TRACE(thread, IsInDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    if (!obj_handle->IsECMAObject()) {
        return ThrowTypeError(thread, "Cannot use 'in' operator in Non-Object");
    }
    JSHandle<JSTaggedValue> prop_key = JSTaggedValue::ToPropertyKey(thread, prop_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    bool ret = JSTaggedValue::HasProperty(thread, obj_handle, prop_key);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue(ret);
}

JSTaggedValue SlowRuntimeStub::InstanceofDyn(JSThread *thread, JSTaggedValue obj, JSTaggedValue target)
{
    INTERPRETER_TRACE(thread, InstanceofDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> target_handle(thread, target);
    bool ret = JSObject::InstanceOf(thread, obj_handle, target_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue(ret);
}

JSTaggedValue SlowRuntimeStub::NewLexicalEnvDyn(JSThread *thread, uint16_t num_vars)
{
    INTERPRETER_TRACE(thread, NewlexenvDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<LexicalEnv> new_env = factory->NewLexicalEnv(num_vars);

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return new_env.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::CreateIterResultObj(JSThread *thread, JSTaggedValue value, bool done)
{
    INTERPRETER_TRACE(thread, CreateIterResultObj);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> value_handle(thread, value);
    JSHandle<JSObject> iter = JSIterator::CreateIterResultObject(thread, value_handle, done);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return iter.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::CreateGeneratorObj(JSThread *thread, JSTaggedValue gen_func)
{
    INTERPRETER_TRACE(thread, CreateGeneratorObj);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> generator_function(thread, gen_func);
    JSHandle<JSGeneratorObject> obj = factory->NewJSGeneratorObject(generator_function);
    JSHandle<GeneratorContext> context = factory->NewGeneratorContext();
    context->SetGeneratorObject(thread, obj.GetTaggedValue());

    // change state to SUSPENDED_START
    obj->SetState(thread, JSGeneratorState::SUSPENDED_START);
    obj->SetGeneratorContext(thread, context);

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return obj.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::SetGeneratorState(JSThread *thread, JSTaggedValue gen_obj, uint8_t state)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSGeneratorObject> generator_object_handle(thread, gen_obj);
    generator_object_handle->SetState(thread, static_cast<JSGeneratorState>(state));

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue::Undefined();
}

JSTaggedValue SlowRuntimeStub::CreateAsyncGeneratorObj(JSThread *thread, JSTaggedValue gen_func)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> generator_function(thread, gen_func);
    JSHandle<JSAsyncGeneratorObject> obj = factory->NewJSAsyncGeneratorObject(generator_function);
    JSHandle<GeneratorContext> context = factory->NewGeneratorContext();
    JSHandle<TaggedQueue> async_queue = factory->NewTaggedQueue(0);
    context->SetGeneratorObject(thread, obj.GetTaggedValue());

    // change state to SUSPENDED_START
    obj->SetState(thread, JSGeneratorState::SUSPENDED_START);
    obj->SetGeneratorContext(thread, context);
    obj->SetAsyncGeneratorQueue(thread, async_queue);

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return obj.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::SuspendGenerator(JSThread *thread, JSTaggedValue gen_obj, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, SuspendGenerator);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSGeneratorObject> generator_object_handle(thread, gen_obj);
    JSHandle<GeneratorContext> gen_context_handle(thread, generator_object_handle->GetGeneratorContext());
    JSHandle<JSTaggedValue> value_handle(thread, value);
    // save stack, should copy cur_frame, function execute over will free cur_frame
    SlowRuntimeHelper::SaveFrameToContext(thread, gen_context_handle);

    return value_handle.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::SuspendAsyncGenerator(JSThread *thread, JSTaggedValue async_gen_obj, JSTaggedValue value)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSGeneratorObject> async_generator_object_handle(thread, async_gen_obj);
    JSHandle<JSTaggedValue> async_generator_handle(thread, async_gen_obj);
    JSHandle<GeneratorContext> gen_context_handle(thread, async_generator_object_handle->GetGeneratorContext());
    JSHandle<JSTaggedValue> value_handle(thread, value);
    // save stack, should copy cur_frame, function execute over will free cur_frame
    SlowRuntimeHelper::SaveFrameToContext(thread, gen_context_handle);

    return JSAsyncGeneratorObject::AsyncGeneratorResolve(thread, async_generator_handle, value_handle, false)
        .GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::AsyncFunctionAwait(JSThread *thread, JSTaggedValue async_func_obj, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, AsyncFunctionAwait);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSAsyncFuncObject> async_func_obj_handle(thread, async_func_obj);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    JSAsyncFuncObject::AsyncFunctionAwait(thread, async_func_obj_handle, value_handle);
    JSHandle<JSPromise> promise(thread, async_func_obj_handle->GetPromise());

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return promise.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::AsyncFunctionResolveOrReject(JSThread *thread, JSTaggedValue async_func_obj,
                                                            JSTaggedValue value, bool is_resolve)
{
    if (is_resolve) {
        INTERPRETER_TRACE(thread, AsyncFunctionResolve);
    } else {
        INTERPRETER_TRACE(thread, AsyncFunctionReject);
    }

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSAsyncFuncObject> async_func_obj_handle(thread, async_func_obj);
    JSHandle<JSPromise> promise(thread, async_func_obj_handle->GetPromise());
    JSHandle<JSTaggedValue> value_handle(thread, value);

    // ActivePromise
    JSHandle<ResolvingFunctionsRecord> reactions = JSPromise::CreateResolvingFunctions(thread, promise);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> this_arg = global_const->GetHandledUndefined();
    JSHandle<JSTaggedValue> active_func;
    if (is_resolve) {
        active_func = JSHandle<JSTaggedValue>(thread, reactions->GetResolveFunction());
    } else {
        active_func = JSHandle<JSTaggedValue>(thread, reactions->GetRejectFunction());
    }
    auto info = NewRuntimeCallInfo(thread, active_func, this_arg, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(value_handle);
    [[maybe_unused]] JSTaggedValue res = JSFunction::Call(info.Get());

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return promise.GetTaggedValue();
}

// 27.6.3.2 AsyncGeneratorStart (generator, generatorBody)
JSTaggedValue SlowRuntimeStub::AsyncGeneratorResolve(JSThread *thread, JSTaggedValue async_gen_obj, JSTaggedValue value)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> async_generator_handle(thread, async_gen_obj);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    // e. Set generator.[[AsyncGeneratorState]] to completed.
    JSAsyncGeneratorObject::Cast(async_gen_obj.GetHeapObject())->SetState(thread, JSGeneratorState::COMPLETED);

    // f. If result is a normal completion, let resultValue be undefined.
    // i. Let resultValue be result.[[Value]].
    // h. Return ! AsyncGeneratorResolve(generator, resultValue, true).
    return JSAsyncGeneratorObject::AsyncGeneratorResolve(thread, async_generator_handle, value_handle, true)
        .GetTaggedValue();
}

// 27.6.3.2 AsyncGeneratorStart (generator, generatorBody)
JSTaggedValue SlowRuntimeStub::AsyncGeneratorReject(JSThread *thread, JSTaggedValue async_gen_obj, JSTaggedValue value)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> async_generator_handle(thread, async_gen_obj);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    // e. Set generator.[[AsyncGeneratorState]] to completed.
    JSAsyncGeneratorObject::Cast(async_gen_obj.GetHeapObject())->SetState(thread, JSGeneratorState::COMPLETED);

    // ii. ii. If result.[[Type]] is not return, then
    // 1. Return ! AsyncGeneratorReject(generator, resultValue).
    // 1. 1. Return ! AsyncGeneratorReject(generator, resultValue).
    return JSAsyncGeneratorObject::AsyncGeneratorReject(thread, async_generator_handle, value_handle).GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::NewObjSpreadDyn(JSThread *thread, JSTaggedValue func, JSTaggedValue new_target,
                                               JSTaggedValue array)
{
    INTERPRETER_TRACE(thread, NewobjspreadDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> js_array(thread, array);
    if (!js_array->IsJSArray()) {
        return ThrowTypeError(thread, "Cannot Newobjspread");
    }

    uint32_t length = JSHandle<JSArray>::Cast(js_array)->GetArrayLength();
    auto info = NewRuntimeCallInfo(thread, func, JSTaggedValue::Undefined(), new_target, length);
    for (uint32_t i = 0; i < length; ++i) {
        auto prop = JSTaggedValue::GetProperty(thread, js_array, i).GetValue();
        info->SetCallArg(i, prop.GetTaggedValue());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }
    return SlowRuntimeHelper::NewObject(info.Get());
}

void SlowRuntimeStub::ThrowTdz(JSThread *thread, JSTaggedValue binding_name)
{
    INTERPRETER_TRACE(thread, ThrowTdz);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> name(thread, binding_name);
    JSHandle<EcmaString> info = factory->NewFromCanBeCompressString(" is not initialized");

    JSHandle<EcmaString> msg = factory->ConcatFromString(name, info);
    THROW_NEW_ERROR_AND_RETURN(thread, factory->NewJSError(base::ErrorType::REFERENCE_ERROR, msg).GetTaggedValue());
}

JSTaggedValue SlowRuntimeStub::ThrowIfSuperNotCorrectCall(JSThread *thread, uint16_t index, JSTaggedValue this_value)
{
    INTERPRETER_TRACE(thread, ThrowIfSuperNotCorrectCall);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    if (index == 0 && (this_value.IsUndefined() || this_value.IsHole())) {
        return ThrowReferenceError(thread, JSTaggedValue::Undefined(), "sub-class must call super before use 'this'");
    }
    if (index == 1 && !this_value.IsUndefined() && !this_value.IsHole()) {
        return ThrowReferenceError(thread, JSTaggedValue::Undefined(), "super() forbidden re-bind 'this'");
    }
    return JSTaggedValue::True();
}

void SlowRuntimeStub::ThrowIfNotObject(JSThread *thread)
{
    INTERPRETER_TRACE(thread, ThrowIfNotObject);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    THROW_TYPE_ERROR(thread, "Inner return result is not object");
}

void SlowRuntimeStub::ThrowThrowNotExists(JSThread *thread)
{
    INTERPRETER_TRACE(thread, ThrowThrowNotExists);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    THROW_TYPE_ERROR(thread, "Throw method is not defined");
}

void SlowRuntimeStub::ThrowPatternNonCoercible(JSThread *thread)
{
    INTERPRETER_TRACE(thread, ThrowPatternNonCoercible);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<EcmaString> msg(thread->GlobalConstants()->GetHandledObjNotCoercibleString());
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    THROW_NEW_ERROR_AND_RETURN(thread, factory->NewJSError(base::ErrorType::TYPE_ERROR, msg).GetTaggedValue());
}

JSTaggedValue SlowRuntimeStub::StOwnByName(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, StOwnByNameDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    ASSERT(prop_handle->IsStringOrSymbol());

    if (obj_handle->IsClassConstructor() &&
        JSTaggedValue::SameValue(prop_handle, global_const->GetHandledPrototypeString())) {
        return ThrowTypeError(thread, "In a class, static property named 'prototype' throw a TypeError");
    }

    // property in class is non-enumerable
    bool enumerable = !(obj_handle->IsClassPrototype() || obj_handle->IsClassConstructor());

    PropertyDescriptor desc(thread, value_handle, true, enumerable, true);
    bool ret = JSTaggedValue::DefineOwnProperty(thread, obj_handle, prop_handle, desc);
    if (!ret) {
        return ThrowTypeError(thread, "SetOwnByName failed");
    }
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::StOwnByNameWithNameSet(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop,
                                                      JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, StOwnByNameDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    ASSERT(prop_handle->IsStringOrSymbol());

    JSHandle<JSTaggedValue> prop_key = JSTaggedValue::ToPropertyKey(thread, prop_handle);

    // property in class is non-enumerable
    bool enumerable = !(obj_handle->IsClassPrototype() || obj_handle->IsClassConstructor());

    PropertyDescriptor desc(thread, value_handle, true, enumerable, true);
    bool ret = JSTaggedValue::DefineOwnProperty(thread, obj_handle, prop_handle, desc);
    if (!ret) {
        return ThrowTypeError(thread, "SetOwnByNameWithNameSet failed");
    }
    JSFunctionBase::SetFunctionName(thread, JSHandle<JSFunctionBase>::Cast(value_handle), prop_key,
                                    JSHandle<JSTaggedValue>(thread, JSTaggedValue::Undefined()));
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::StOwnByIndex(JSThread *thread, JSTaggedValue obj, uint32_t idx, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, StOwnByIdDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> idx_handle(thread, JSTaggedValue(idx));
    JSHandle<JSTaggedValue> value_handle(thread, value);

    // property in class is non-enumerable
    bool enumerable = !(obj_handle->IsClassPrototype() || obj_handle->IsClassConstructor());

    PropertyDescriptor desc(thread, value_handle, true, enumerable, true);
    bool ret = JSTaggedValue::DefineOwnProperty(thread, obj_handle, idx_handle, desc);
    if (!ret) {
        return ThrowTypeError(thread, "SetOwnByIndex failed");
    }
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::StOwnByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                            const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    INTERPRETER_TRACE(thread, StOwnByValueDyn);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    if (obj->IsClassConstructor() && JSTaggedValue::SameValue(key, global_const->GetHandledPrototypeString())) {
        return ThrowTypeError(thread, "In a class, static property named 'prototype' throw a TypeError");
    }

    // propery in class is non-enumerable
    bool enumerable = !(obj->IsClassPrototype() || obj->IsClassConstructor());

    PropertyDescriptor desc(thread, value, true, enumerable, true);
    JSMutableHandle<JSTaggedValue> prop_key(JSTaggedValue::ToPropertyKey(thread, key));
    bool ret = JSTaggedValue::DefineOwnProperty(thread, obj, prop_key, desc);
    if (!ret) {
        return ThrowTypeError(thread, "StOwnByValue failed");
    }
    if (value->IsJSFunction()) {
        if (prop_key->IsNumber()) {
            prop_key.Update(base::NumberHelper::NumberToString(thread, prop_key.GetTaggedValue()).GetTaggedValue());
        }
        JSFunctionBase::SetFunctionName(thread, JSHandle<JSFunctionBase>::Cast(value), prop_key,
                                        JSHandle<JSTaggedValue>(thread, JSTaggedValue::Undefined()));
    }
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::StOwnByValueWithNameSet(JSThread *thread, JSHandle<JSTaggedValue> obj,
                                                       JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> value)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    INTERPRETER_TRACE(thread, StOwnByValueDyn);
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    if (obj->IsClassConstructor() && JSTaggedValue::SameValue(key, global_const->GetHandledPrototypeString())) {
        return ThrowTypeError(thread, "In a class, static property named 'prototype' throw a TypeError");
    }

    // property in class is non-enumerable
    bool enumerable = !(obj->IsClassPrototype() || obj->IsClassConstructor());

    PropertyDescriptor desc(thread, value, true, enumerable, true);
    JSMutableHandle<JSTaggedValue> prop_key(JSTaggedValue::ToPropertyKey(thread, key));
    bool ret = JSTaggedValue::DefineOwnProperty(thread, obj, prop_key, desc);
    if (!ret) {
        return ThrowTypeError(thread, "StOwnByValueWithNameSet failed");
    }
    if (value->IsJSFunction()) {
        if (prop_key->IsNumber()) {
            prop_key.Update(base::NumberHelper::NumberToString(thread, prop_key.GetTaggedValue()).GetTaggedValue());
        }
        JSFunctionBase::SetFunctionName(thread, JSHandle<JSFunctionBase>::Cast(value), prop_key,
                                        JSHandle<JSTaggedValue>(thread, JSTaggedValue::Undefined()));
    }
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::CreateEmptyArray(JSThread *thread, ObjectFactory *factory,
                                                JSHandle<GlobalEnv> global_env)
{
    INTERPRETER_TRACE(thread, CreateEmptyArray);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSFunction> builtin_obj(global_env->GetArrayFunction());
    JSHandle<JSObject> arr = factory->NewJSObjectByConstructor(builtin_obj, JSHandle<JSTaggedValue>(builtin_obj));
    return arr.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::CreateEmptyObject(JSThread *thread, ObjectFactory *factory,
                                                 JSHandle<GlobalEnv> global_env)
{
    INTERPRETER_TRACE(thread, CreateEmptyObject);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSFunction> builtin_obj(global_env->GetObjectFunction());
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(builtin_obj, JSHandle<JSTaggedValue>(builtin_obj));
    return obj.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::CreateObjectWithBuffer(JSThread *thread, ObjectFactory *factory, JSObject *literal)
{
    INTERPRETER_TRACE(thread, CreateObjectWithBuffer);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSObject> obj(thread, literal);
    JSHandle<JSObject> obj_literal = factory->CloneObjectLiteral(obj);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return obj_literal.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::CreateObjectHavingMethod(JSThread *thread, ObjectFactory *factory, JSObject *literal,
                                                        JSTaggedValue env, ConstantPool *constpool)
{
    INTERPRETER_TRACE(thread, CreateObjectHavingMethod);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSObject> obj(thread, literal);
    JSHandle<JSObject> obj_literal = factory->CloneObjectLiteral(
        obj, JSHandle<JSTaggedValue>(thread, env), JSHandle<JSTaggedValue>(thread, JSTaggedValue(constpool)));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return obj_literal.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::SetObjectWithProto(JSThread *thread, JSTaggedValue proto, JSTaggedValue obj)
{
    INTERPRETER_TRACE(thread, SetObjectWithProto);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    if (!proto.IsECMAObject() && !proto.IsNull()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSTaggedValue> proto_handle(thread, proto);
    JSHandle<JSObject> obj_handle(thread, obj);
    JSObject::SetPrototype(thread, obj_handle, proto_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::CloseIterator(JSThread *thread, JSTaggedValue iter)
{
    INTERPRETER_TRACE(thread, CloseIterator);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    JSHandle<JSTaggedValue> iter_handle(thread, iter);
    JSHandle<JSTaggedValue> record;
    if (UNLIKELY(thread->HasPendingException())) {
        JSHandle<JSTaggedValue> exception(thread,
                                          ObjectWrapper::Cast(thread->GetException().GetTaggedObject())->GetValue());
        record = JSHandle<JSTaggedValue>(factory->NewCompletionRecord(CompletionRecord::THROW, exception));
    } else {
        JSHandle<JSTaggedValue> undefined_val = global_const->GetHandledUndefined();
        record = JSHandle<JSTaggedValue>(factory->NewCompletionRecord(CompletionRecord::NORMAL, undefined_val));
    }
    JSHandle<JSTaggedValue> result = JSIterator::IteratorClose(thread, iter_handle, record);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return result.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::ImportModule([[maybe_unused]] JSThread *thread,
                                            [[maybe_unused]] JSTaggedValue module_name)
{
    INTERPRETER_TRACE(thread, ImportModule);
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSTaggedValue> name(thread, module_name);
    JSHandle<JSTaggedValue> module = thread->GetEcmaVM()->GetModuleByName(name);
    return module.GetTaggedValue();  // return moduleRef
}

void SlowRuntimeStub::StModuleVar([[maybe_unused]] JSThread *thread, [[maybe_unused]] JSTaggedValue export_name,
                                  [[maybe_unused]] JSTaggedValue export_obj)
{
    INTERPRETER_TRACE(thread, StModuleVar);
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSTaggedValue> name(thread, export_name);
    JSHandle<JSTaggedValue> value(thread, export_obj);
    thread->GetEcmaVM()->GetModuleManager()->AddModuleItem(thread, name, value);
}

void SlowRuntimeStub::CopyModule(JSThread *thread, JSTaggedValue src_module)
{
    INTERPRETER_TRACE(thread, CopyModule);
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSTaggedValue> src_module_obj(thread, src_module);
    thread->GetEcmaVM()->GetModuleManager()->CopyModule(thread, src_module_obj);
}

JSTaggedValue SlowRuntimeStub::LdModvarByName([[maybe_unused]] JSThread *thread,
                                              [[maybe_unused]] JSTaggedValue module_obj,
                                              [[maybe_unused]] JSTaggedValue item_name)
{
    INTERPRETER_TRACE(thread, LdModvarByName);
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<JSTaggedValue> module(thread, module_obj);
    JSHandle<JSTaggedValue> item(thread, item_name);
    JSHandle<JSTaggedValue> module_var = thread->GetEcmaVM()->GetModuleManager()->GetModuleItem(thread, module, item);
    return module_var.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::ClassFieldAdd(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop_name,
                                             JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, ClassFieldAdd);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    const GlobalEnvConstants *global_consts = thread->GlobalConstants();
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, prop_name);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    ASSERT(prop_handle->IsStringOrSymbol());

    if (obj_handle->IsClassConstructor() &&
        JSTaggedValue::SameValue(prop_handle, global_consts->GetHandledPrototypeString())) {
        return ThrowTypeError(thread, "In a class, static property named 'prototype' throw a TypeError");
    }

    PropertyDescriptor desc(thread, value_handle, true, true, true);
    bool ret = JSTaggedValue::DefineOwnProperty(thread, obj_handle, prop_handle, desc);
    if (!ret) {
        return ThrowTypeError(thread, "ClassFieldAdd failed");
    }
    return JSTaggedValue::True();
}

using PrivateFieldKind = JSConstructorFunction::PrivateFieldKind;

void SlowRuntimeStub::DefineClassPrivateFields(JSThread *thread, ConstantPool *constpool, JSTaggedValue env,
                                               JSTaggedValue ctor, JSTaggedValue private_buf)
{
    INTERPRETER_TRACE(thread, DefineClassPrivateFields);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    auto factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> desc_template(thread, private_buf);
    JSHandle<ConstantPool> constpool_handle(thread, constpool);
    JSHandle<JSTaggedValue> lexical_env(thread, env);
    JSHandle<JSTaggedValue> ctor_handle(thread, ctor);
    JSHandle<TaggedArray> new_desc = factory->NewTaggedArray(desc_template->GetLength());
    JSHandle<LinkedHashMap> accessor_keys = LinkedHashMap::Create(thread);

    for (ArraySizeT i = 0; i < desc_template->GetLength();) {
        JSHandle<JSTaggedValue> kind_value(thread, desc_template->Get(thread, i));
        auto kind = static_cast<PrivateFieldKind>(kind_value->GetInt());
        new_desc->Set(thread, i++, kind_value);

        switch (kind) {
            case PrivateFieldKind::FIELD:
            case PrivateFieldKind::STATIC_FIELD:
            case PrivateFieldKind::METHOD:
            case PrivateFieldKind::STATIC_METHOD: {
                JSHandle<JSTaggedValue> name(thread, desc_template->Get(thread, i));
                JSHandle<JSSymbol> private_key = factory->NewPrivateNameSymbol(name);
                new_desc->Set(thread, i++, private_key.GetTaggedValue());
                break;
            }
            default: {
                JSHandle<JSTaggedValue> name(thread, desc_template->Get(thread, i));
                int hash = LinkedHash::Hash(name.GetTaggedValue());
                JSTaggedValue private_key = accessor_keys->Get(name.GetTaggedValue(), hash);

                if (private_key.IsUndefined()) {
                    JSHandle<JSSymbol> shared_private_key = factory->NewPrivateNameSymbol(name);
                    LinkedHashMap::Set(thread, accessor_keys, name, JSHandle<JSTaggedValue>::Cast(shared_private_key));
                    private_key = shared_private_key.GetTaggedValue();
                }

                new_desc->Set(thread, i++, private_key);
                break;
            }
        }

        switch (kind) {
            case PrivateFieldKind::FIELD:
            case PrivateFieldKind::STATIC_FIELD: {
                break;
            }
            default: {
                JSHandle<JSFunction> value_handle(thread, desc_template->Get(thread, i));
                JSHandle<JSFunction> new_func = factory->CloneJSFuction(value_handle, value_handle->GetFunctionKind());
                new_func->SetLexicalEnv(thread, lexical_env.GetTaggedValue());
                new_func->SetConstantPool(thread, constpool_handle.GetTaggedValue());
                new_desc->Set(thread, i++, new_func.GetTaggedValue());
                break;
            }
        }
    }

    JSConstructorFunction::Cast(ctor_handle->GetHeapObject())->SetPrivateFields(new_desc.GetTaggedValue());
}

JSTaggedValue SlowRuntimeStub::ClassPrivateMethodOrAccessorAdd(JSThread *thread, JSTaggedValue ctor, JSTaggedValue obj)
{
    INTERPRETER_TRACE(thread, ClassPrivateMethodOrAccessorAdd);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    auto factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSConstructorFunction> ctor_handle(thread, ctor);
    JSHandle<JSObject> obj_handle(thread, obj);

    bool is_static = JSTaggedValue::Equal(thread, JSHandle<JSTaggedValue>::Cast(ctor_handle),
                                          JSHandle<JSTaggedValue>::Cast(obj_handle));

    JSHandle<TaggedArray> desc = JSHandle<TaggedArray>(thread, ctor_handle->GetPrivateFields());
    JSHandle<LinkedHashMap> accessor_pairs = LinkedHashMap::Create(thread);

    for (ArraySizeT i = 0; i < desc->GetLength();) {
        JSHandle<JSTaggedValue> kind_value(thread, desc->Get(thread, i++));
        auto kind = static_cast<PrivateFieldKind>(kind_value->GetInt());

        switch (kind) {
            case PrivateFieldKind::FIELD:
            case PrivateFieldKind::STATIC_FIELD: {
                i++;
                continue;
            }
            case PrivateFieldKind::METHOD:
            case PrivateFieldKind::GET:
            case PrivateFieldKind::SET: {
                if (is_static) {
                    i += 2;  // 2: skip key + value
                    continue;
                }
                break;
            }
            case PrivateFieldKind::STATIC_METHOD:
            case PrivateFieldKind::STATIC_GET:
            case PrivateFieldKind::STATIC_SET: {
                if (!is_static) {
                    i += 2;  // 2: skip key + value
                    continue;
                }
                break;
            }
        }

        JSHandle<JSTaggedValue> private_symbol(thread, desc->Get(thread, i++));
        JSHandle<JSTaggedValue> value(thread, desc->Get(thread, i++));

        PropertyDescriptor prop_desc(thread);
        bool exists = JSObject::GetOwnProperty(thread, obj_handle, private_symbol, prop_desc);

        switch (kind) {
            case PrivateFieldKind::METHOD:
            case PrivateFieldKind::STATIC_METHOD: {
                if (exists) {
                    THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot declare same private field twice",
                                                JSTaggedValue::Exception());
                }

                prop_desc = PropertyDescriptor(thread, value, false, false, false);
                JSObject::DefineOwnProperty(thread, obj_handle, private_symbol, prop_desc);
                continue;
            }
            case PrivateFieldKind::GET:
            case PrivateFieldKind::STATIC_GET: {
                int hash = LinkedHash::Hash(private_symbol.GetTaggedValue());
                JSTaggedValue pair = accessor_pairs->Get(private_symbol.GetTaggedValue(), hash);

                if (pair.IsUndefined()) {
                    JSHandle<TaggedArray> arr = factory->NewTaggedArray(2);  // 2: getter-setter pair
                    arr->Set(thread, 0, value);
                    arr->Set(thread, 1, JSTaggedValue::Undefined());
                    LinkedHashMap::Set(thread, accessor_pairs, private_symbol, JSHandle<JSTaggedValue>::Cast(arr));
                    continue;
                }
                TaggedArray::Cast(pair.GetHeapObject())->Set(thread, 0, value);
                continue;
            }
            case PrivateFieldKind::SET:
            case PrivateFieldKind::STATIC_SET: {
                int hash = LinkedHash::Hash(private_symbol.GetTaggedValue());
                JSTaggedValue pair = accessor_pairs->Get(private_symbol.GetTaggedValue(), hash);

                if (pair.IsUndefined()) {
                    JSHandle<TaggedArray> arr = factory->NewTaggedArray(2);  // 2: getter-setter pair
                    arr->Set(thread, 0, JSTaggedValue::Undefined());
                    arr->Set(thread, 1, value);
                    LinkedHashMap::Set(thread, accessor_pairs, private_symbol, JSHandle<JSTaggedValue>::Cast(arr));
                    continue;
                }
                TaggedArray::Cast(pair.GetHeapObject())->Set(thread, 1, value);
                continue;
            }
            default: {
                UNREACHABLE();
                break;
            }
        }
    }

    PropertyDescriptor prop_desc(thread);
    for (int i = 0; i < accessor_pairs->NumberOfElements(); i++) {
        JSHandle<JSTaggedValue> private_symbol(thread, accessor_pairs->GetKey(i));

        int hash = LinkedHash::Hash(private_symbol.GetTaggedValue());
        JSHandle<TaggedArray> accessor_pair(thread, accessor_pairs->Get(private_symbol.GetTaggedValue(), hash));
        JSHandle<JSTaggedValue> getter(thread, accessor_pair->Get(0));
        JSHandle<JSTaggedValue> setter(thread, accessor_pair->Get(1));

        prop_desc.SetGetter(getter);
        prop_desc.SetSetter(setter);
        prop_desc.SetConfigurable(false);
        prop_desc.SetEnumerable(false);
        JSObject::DefineOwnProperty(thread, obj_handle, private_symbol, prop_desc);
    }

    return JSTaggedValue::Undefined();
}

JSTaggedValue SlowRuntimeStub::ClassPrivateFieldAdd(JSThread *thread, JSTaggedValue ctor, JSTaggedValue obj,
                                                    JSTaggedValue prop_name, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, ClassPrivateFieldAdd);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    PropertyDescriptor prop_desc(thread);
    [[maybe_unused]] PrivateFieldKind kind;

    JSHandle<JSObject> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_name_handle(thread, prop_name);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    JSHandle<JSTaggedValue> private_symbol(
        thread, SlowRuntimeHelper::FindPrivateKey(thread, JSHandle<JSConstructorFunction>(thread, ctor), obj_handle,
                                                  prop_name_handle, prop_desc, kind));

    if (!prop_desc.GetValue()->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot declare same private field twice", JSTaggedValue::Exception());
    }

    prop_desc = PropertyDescriptor(thread, value_handle, true, false, false);
    JSObject::DefineOwnProperty(thread, obj_handle, private_symbol, prop_desc);

    return JSTaggedValue::Undefined();
}

JSTaggedValue SlowRuntimeStub::ClassPrivateFieldGet(JSThread *thread, JSTaggedValue ctor, JSTaggedValue obj,
                                                    JSTaggedValue prop_name)
{
    INTERPRETER_TRACE(thread, ClassPrivateFieldGet);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    PropertyDescriptor prop_desc(thread);
    [[maybe_unused]] PrivateFieldKind kind;

    JSHandle<JSConstructorFunction> ctor_handle(thread, ctor);
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_name_handle(thread, prop_name);
    JSHandle<JSObject> js_obj(JSTaggedValue::ToObject(thread, obj_handle));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSTaggedValue> private_symbol(
        thread, SlowRuntimeHelper::FindPrivateKey(thread, ctor_handle, js_obj, prop_name_handle, prop_desc, kind));

    if (prop_desc.GetValue()->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot read private member to an object whose class did not declare it",
                                    JSTaggedValue::Exception());
    }

    if (prop_desc.HasValue()) {
        return prop_desc.GetValue().GetTaggedValue();
    }

    if (!prop_desc.HasGetter()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Private field was defined without a getter", JSTaggedValue::Exception());
    }

    auto info = NewRuntimeCallInfo(thread, prop_desc.GetGetter(), obj_handle, JSTaggedValue::Undefined(), 0);
    JSTaggedValue res = JSFunction::Call(info.Get());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res;
}

JSTaggedValue SlowRuntimeStub::ClassPrivateFieldSet(JSThread *thread, JSTaggedValue ctor, JSTaggedValue obj,
                                                    JSTaggedValue prop_name, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, ClassPrivateFieldSet);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    PropertyDescriptor prop_desc(thread);
    [[maybe_unused]] PrivateFieldKind kind;

    JSHandle<JSConstructorFunction> ctor_handle(thread, ctor);
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_name_handle(thread, prop_name);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    JSHandle<JSObject> js_obj(JSTaggedValue::ToObject(thread, obj_handle));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSHandle<JSTaggedValue> private_symbol(
        thread, SlowRuntimeHelper::FindPrivateKey(thread, ctor_handle, js_obj, prop_name_handle, prop_desc, kind));

    if (prop_desc.GetValue()->IsHole()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot read private member to an object whose class did not declare it",
                                    JSTaggedValue::Exception());
    }

    if (prop_desc.HasValue()) {
        if (kind == PrivateFieldKind::METHOD || kind == PrivateFieldKind::STATIC_METHOD) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Private method is not writable", JSTaggedValue::Exception());
        }

        prop_desc.SetValue(value_handle);
        JSObject::DefineOwnProperty(thread, js_obj, private_symbol, prop_desc);
        return JSTaggedValue::Undefined();
    }

    if (!prop_desc.HasSetter()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Private field was defined without a setter", JSTaggedValue::Exception());
    }

    auto info = NewRuntimeCallInfo(thread, prop_desc.GetSetter(), obj_handle, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(value_handle);
    JSTaggedValue res = JSFunction::Call(info.Get());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res;
}

JSTaggedValue SlowRuntimeStub::ClassPrivateFieldIn(JSThread *thread, JSTaggedValue ctor, JSTaggedValue obj,
                                                   JSTaggedValue prop_name)
{
    INTERPRETER_TRACE(thread, ClassPrivateFieldIn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    PropertyDescriptor prop_desc(thread);
    [[maybe_unused]] PrivateFieldKind kind;

    if (!obj.IsECMAObject()) {
        return ThrowTypeError(thread, "Cannot use 'in' operator in Non-Object");
    }

    JSHandle<JSObject> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_name_handle(thread, prop_name);

    JSHandle<JSTaggedValue> private_symbol(
        thread, SlowRuntimeHelper::FindPrivateKey(thread, JSHandle<JSConstructorFunction>(thread, ctor), obj_handle,
                                                  prop_name_handle, prop_desc, kind));

    return JSTaggedValue(!prop_desc.GetValue()->IsHole());
}

JSTaggedValue SlowRuntimeStub::CreateRegExpWithLiteral(JSThread *thread, JSTaggedValue pattern, uint8_t flags)
{
    INTERPRETER_TRACE(thread, CreateRegExpWithLiteral);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> pattern_handle(thread, pattern);
    JSHandle<JSTaggedValue> flags_handle(thread, JSTaggedValue(flags));

    return builtins::reg_exp::RegExpCreate(thread, pattern_handle, flags_handle);
}

JSTaggedValue SlowRuntimeStub::CreateArrayWithBuffer(JSThread *thread, ObjectFactory *factory, JSArray *literal)
{
    INTERPRETER_TRACE(thread, CreateArrayWithBuffer);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSArray> array(thread, literal);
    JSHandle<JSArray> arr_literal = factory->CloneArrayLiteral(array);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return arr_literal.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::GetMethod(JSThread *thread, JSTaggedValue object, JSTaggedValue prop_key)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj(thread, object);
    JSHandle<JSTaggedValue> key(thread, prop_key);

    JSHandle<JSTaggedValue> method = JSObject::GetMethod(thread, obj, key);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return method.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::GetTemplateObject(JSThread *thread, JSTaggedValue literal)
{
    INTERPRETER_TRACE(thread, GetTemplateObject);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> template_literal(thread, literal);
    JSHandle<JSTaggedValue> template_obj = TemplateString::GetTemplateObject(thread, template_literal);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return template_obj.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::GetNextPropName(JSThread *thread, JSTaggedValue iter)
{
    INTERPRETER_TRACE(thread, GetNextPropName);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> iterator(thread, iter);
    ASSERT(iterator->IsForinIterator());
    std::pair<JSTaggedValue, bool> res =
        JSForInIterator::NextInternal(thread, JSHandle<JSForInIterator>::Cast(iterator));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res.first;
}

JSTaggedValue SlowRuntimeStub::CopyDataProperties(JSThread *thread, JSTaggedValue dst, JSTaggedValue src)
{
    INTERPRETER_TRACE(thread, CopyDataProperties);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> dst_handle(thread, dst);
    JSHandle<JSTaggedValue> src_handle(thread, src);
    if (!src_handle->IsNull() && !src_handle->IsUndefined()) {
        JSHandle<TaggedArray> keys = JSTaggedValue::GetOwnPropertyKeys(thread, src_handle);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        uint32_t keys_len = keys->GetLength();
        for (uint32_t i = 0; i < keys_len; i++) {
            PropertyDescriptor desc(thread);
            key.Update(keys->Get(i));
            bool success = JSTaggedValue::GetOwnProperty(thread, src_handle, key, desc);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            if (success && desc.IsEnumerable()) {
                JSTaggedValue::DefineOwnProperty(thread, dst_handle, key, desc);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
        }
    }
    return dst_handle.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::GetUnmappedArgs(JSThread *thread, uint32_t actual_num_args, JSTaggedType *stkargs)
{
    INTERPRETER_TRACE(thread, GetUnmapedArgs);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> global_env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<TaggedArray> arguments_list = factory->NewTaggedArray(actual_num_args);
    for (uint32_t i = 0; i < actual_num_args; ++i) {
        arguments_list->Set(thread, i,
                            JSTaggedValue(stkargs[i]));  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    }
    // 1. Let len be the number of elements in argumentsList
    int32_t len = arguments_list->GetLength();
    // 2. Let obj be ObjectCreate(%ObjectPrototype%, «[[ParameterMap]]»).
    // 3. Set obj’s [[ParameterMap]] internal slot to undefined.
    JSHandle<JSArguments> obj = factory->NewJSArguments();
    // 4. Perform DefinePropertyOrThrow(obj, "length", PropertyDescriptor{[[Value]]: len, [[Writable]]: true,
    // [[Enumerable]]: false, [[Configurable]]: true}).
    obj->SetPropertyInlinedProps(thread, JSArguments::LENGTH_INLINE_PROPERTY_INDEX, JSTaggedValue(len));
    // 5. Let index be 0.
    // 6. Repeat while index < len,
    //    a. Let val be argumentsList[index].
    //    b. Perform CreateDataProperty(obj, ToString(index), val).
    //    c. Let index be index + 1
    obj->SetElements(thread, arguments_list.GetTaggedValue());
    // 7. Perform DefinePropertyOrThrow(obj, @@iterator, PropertyDescriptor
    // {[[Value]]:%ArrayProto_values%,
    // [[Writable]]: true, [[Enumerable]]: false, [[Configurable]]: true}).
    obj->SetPropertyInlinedProps(thread, JSArguments::ITERATOR_INLINE_PROPERTY_INDEX,
                                 global_env->GetArrayProtoValuesFunction().GetTaggedValue());
    // 9. Perform DefinePropertyOrThrow(obj, "callee", PropertyDescriptor {[[Get]]: %ThrowTypeError%,
    // [[Set]]: %ThrowTypeError%, [[Enumerable]]: false, [[Configurable]]: false}).
    JSHandle<JSTaggedValue> throw_function = global_env->GetThrowTypeError();
    JSHandle<AccessorData> accessor = factory->NewAccessorData();
    accessor->SetGetter(thread, throw_function);
    accessor->SetSetter(thread, throw_function);
    obj->SetPropertyInlinedProps(thread, JSArguments::CALLEE_INLINE_PROPERTY_INDEX, accessor.GetTaggedValue());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 11. Return obj
    return obj.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::CopyRestArgs(JSThread *thread, uint32_t rest_num_args, JSTaggedType *stkargs)
{
    INTERPRETER_TRACE(thread, Copyrestargs);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> rest_array = JSArray::ArrayCreate(thread, JSTaggedNumber(rest_num_args));

    JSMutableHandle<JSTaggedValue> element(thread, JSTaggedValue::Undefined());
    for (uint32_t i = 0; i < rest_num_args; ++i) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        element.Update(JSTaggedValue(stkargs[i]));
        JSObject::SetProperty(thread, rest_array, i, element, true);
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return rest_array.GetTaggedValue();
}

// 7.4.1 GetIterator
JSTaggedValue SlowRuntimeStub::GetIterator(JSThread *thread, JSTaggedValue obj, bool async, JSTaggedValue method)
{
    INTERPRETER_TRACE(thread, GetIterator);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    EcmaVM *vm = thread->GetEcmaVM();
    auto global_const = thread->GlobalConstants();
    JSHandle<GlobalEnv> env = vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSMutableHandle<JSTaggedValue> method_handle(thread, global_const->GetHandledUndefined());

    // 3. If method is not present, then
    if (method.IsHole()) {
        // a. If hint is async, then
        if (async) {
            // i. Set method to ? GetMethod(obj, @@asyncIterator).
            method_handle.Update(JSObject::GetMethod(thread, obj_handle, env->GetAsyncIteratorSymbol()));
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

            if (method_handle->IsUndefined()) {
                // 1. Let syncMethod be ? GetMethod(obj, @@iterator).
                JSHandle<JSTaggedValue> sync_method = JSObject::GetMethod(thread, obj_handle, env->GetIteratorSymbol());
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

                // 2. Let syncIteratorRecord be ? GetIterator(obj, sync, syncMethod).
                JSHandle<JSTaggedValue> sync_iterator(
                    thread, SlowRuntimeStub::GetIterator(thread, obj_handle.GetTaggedValue(), false,
                                                         sync_method.GetTaggedValue()));
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

                // 6. Let nextMethod be ? GetV(iterator, "next").
                JSHandle<JSTaggedValue> sync_next_method =
                    JSTaggedValue::GetProperty(thread, sync_iterator, global_const->GetHandledNextString()).GetValue();
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

                // 3. Return ! CreateAsyncFromSyncIterator(syncIteratorRecord).
                return JSAsyncFromSyncIteratorObject::CreateAsyncFromSyncIterator(thread, sync_iterator,
                                                                                  sync_next_method);
            }
        } else {
            method_handle.Update(JSTaggedValue::GetProperty(thread, obj_handle, env->GetIteratorSymbol()).GetValue());
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
    } else {
        method_handle.Update(method);
    }

    // 4. Let iterator be ? Call(method, obj).
    auto info = NewRuntimeCallInfo(thread, method_handle, obj_handle, JSTaggedValue::Undefined(), 0);
    JSTaggedValue iterator = JSFunction::Call(info.Get());

    // 5. If Type(iterator) is not Object, throw a TypeError exception.
    if (!iterator.IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Iterator is not object", JSTaggedValue::Exception());
    }

    return iterator;
}

JSTaggedValue SlowRuntimeStub::DefineGetterSetterByValue(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop,
                                                         JSTaggedValue getter, JSTaggedValue setter, bool flag)
{
    INTERPRETER_TRACE(thread, DefineGetterSetterByValue);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSObject> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, prop);

    JSHandle<JSTaggedValue> getter_handle(thread, getter);
    JSHandle<JSTaggedValue> setter_handle(thread, setter);
    JSHandle<JSTaggedValue> prop_key = JSTaggedValue::ToPropertyKey(thread, prop_handle);

    auto global_const = thread->GlobalConstants();
    if (obj_handle.GetTaggedValue().IsClassConstructor() &&
        JSTaggedValue::SameValue(prop_key, global_const->GetHandledPrototypeString())) {
        return ThrowTypeError(
            thread,
            "In a class, computed property names for static getter that are named 'prototype' throw a TypeError");
    }

    if (flag) {
        if (!getter_handle->IsUndefined()) {
            if (prop_key->IsNumber()) {
                prop_key = JSHandle<JSTaggedValue>::Cast(
                    base::NumberHelper::NumberToString(thread, prop_key.GetTaggedValue()));
            }
            JSFunctionBase::SetFunctionName(thread, JSHandle<JSFunctionBase>::Cast(getter_handle), prop_key,
                                            JSHandle<JSTaggedValue>(thread, global_const->GetGetString()));
        }

        if (!setter_handle->IsUndefined()) {
            if (prop_key->IsNumber()) {
                prop_key = JSHandle<JSTaggedValue>::Cast(
                    base::NumberHelper::NumberToString(thread, prop_key.GetTaggedValue()));
            }
            JSFunctionBase::SetFunctionName(thread, JSHandle<JSFunctionBase>::Cast(setter_handle), prop_key,
                                            JSHandle<JSTaggedValue>(thread, global_const->GetSetString()));
        }
    }

    // set accessor
    bool enumerable =
        !(obj_handle.GetTaggedValue().IsClassPrototype() || obj_handle.GetTaggedValue().IsClassConstructor());
    PropertyDescriptor desc(thread, true, enumerable, true);
    if (!getter_handle->IsUndefined()) {
        JSHandle<JSFunction>::Cast(getter_handle)->SetFunctionKind(thread, FunctionKind::GETTER_FUNCTION);
        desc.SetGetter(getter_handle);
    }
    if (!setter_handle->IsUndefined()) {
        JSHandle<JSFunction>::Cast(setter_handle)->SetFunctionKind(thread, FunctionKind::SETTER_FUNCTION);
        desc.SetSetter(setter_handle);
    }
    JSObject::DefineOwnProperty(thread, obj_handle, prop_key, desc);

    return obj_handle.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::LdObjByIndex(JSThread *thread, JSTaggedValue obj, uint32_t idx, bool call_getter,
                                            JSTaggedValue receiver)
{
    INTERPRETER_TRACE(thread, LdObjByIndexDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSTaggedValue res;
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    if (call_getter) {
        res = JSObject::CallGetter(thread, AccessorData::Cast(receiver.GetTaggedObject()), obj_handle);
    } else {
        res = JSTaggedValue::GetProperty(thread, obj_handle, idx).GetValue().GetTaggedValue();
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res;
}

JSTaggedValue SlowRuntimeStub::StObjByIndex(JSThread *thread, JSTaggedValue obj, uint32_t idx, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, StObjByIndexDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>(thread, obj), idx,
                               JSHandle<JSTaggedValue>(thread, value), true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::LdObjByName(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop, bool call_getter,
                                           JSTaggedValue receiver)
{
    INTERPRETER_TRACE(thread, LdObjByNameDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSTaggedValue res;
    if (call_getter) {
        res = JSObject::CallGetter(thread, AccessorData::Cast(receiver.GetTaggedObject()), obj_handle);
    } else {
        JSHandle<JSTaggedValue> prop_handle(thread, prop);
        res = JSTaggedValue::GetProperty(thread, obj_handle, prop_handle).GetValue().GetTaggedValue();
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res;
}

JSTaggedValue SlowRuntimeStub::StObjByName(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, StObjByNameDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    JSTaggedValue::SetProperty(thread, obj_handle, prop_handle, value_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::LdObjByValue(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop, bool call_getter,
                                            JSTaggedValue receiver)
{
    INTERPRETER_TRACE(thread, LdObjByValueDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSTaggedValue res;
    if (call_getter) {
        res = JSObject::CallGetter(thread, AccessorData::Cast(receiver.GetTaggedObject()), obj_handle);
    } else {
        JSHandle<JSTaggedValue> prop_key = JSTaggedValue::ToPropertyKey(thread, JSHandle<JSTaggedValue>(thread, prop));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        res = JSTaggedValue::GetProperty(thread, obj_handle, prop_key).GetValue().GetTaggedValue();
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res;
}

JSTaggedValue SlowRuntimeStub::StObjByValue(JSThread *thread, JSTaggedValue obj, JSTaggedValue prop,
                                            JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, StObjByValueDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    JSHandle<JSTaggedValue> value_handle(thread, value);
    JSHandle<JSTaggedValue> prop_key(JSTaggedValue::ToPropertyKey(thread, prop_handle));

    // strict mode is true
    JSTaggedValue::SetProperty(thread, obj_handle, prop_key, value_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::TryLdGlobalByName(JSThread *thread, JSTaggedValue global, JSTaggedValue prop)
{
    INTERPRETER_TRACE(thread, Trygetobjprop);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj(thread, global.GetTaggedObject()->GetClass()->GetPrototype());
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    OperationResult res = JSTaggedValue::GetProperty(thread, obj, prop_handle);
    if (!res.GetPropertyMetaData().IsFound()) {
        return ThrowReferenceError(thread, prop_handle.GetTaggedValue(), " is not defined");
    }
    return res.GetValue().GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::LdGlobalVar(JSThread *thread, JSTaggedValue global, JSTaggedValue prop)
{
    INTERPRETER_TRACE(thread, LdGlobalVar);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> obj_handle(thread, global.GetTaggedObject()->GetClass()->GetPrototype());
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    OperationResult res = JSTaggedValue::GetProperty(thread, obj_handle, prop_handle);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res.GetValue().GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::StGlobalVar(JSThread *thread, JSTaggedValue prop, JSTaggedValue value)
{
    INTERPRETER_TRACE(thread, StGlobalVar);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> global(thread, thread->GetEcmaVM()->GetGlobalEnv()->GetGlobalObject());
    JSHandle<JSTaggedValue> prop_handle(thread, prop);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    JSObject::GlobalSetProperty(thread, prop_handle, value_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::ThrowReferenceError(JSThread *thread, JSTaggedValue prop, const char *desc)
{
    INTERPRETER_TRACE(thread, ThrowReferenceError);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> prop_name = JSTaggedValue::ToString(thread, JSHandle<JSTaggedValue>(thread, prop));
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    JSHandle<EcmaString> info = factory->NewFromString(desc);
    JSHandle<EcmaString> msg = factory->ConcatFromString(prop_name, info);
    THROW_NEW_ERROR_AND_RETURN_VALUE(thread,
                                     factory->NewJSError(base::ErrorType::REFERENCE_ERROR, msg).GetTaggedValue(),
                                     JSTaggedValue::Exception());
}

JSTaggedValue SlowRuntimeStub::ThrowTypeError(JSThread *thread, const char *message)
{
    INTERPRETER_TRACE(thread, ThrowTypeError);
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    THROW_TYPE_ERROR_AND_RETURN(thread, message, JSTaggedValue::Exception());
}

JSTaggedValue SlowRuntimeStub::ThrowSyntaxError(JSThread *thread, const char *message)
{
    INTERPRETER_TRACE(thread, ThrowSyntaxError);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    THROW_SYNTAX_ERROR_AND_RETURN(thread, message, JSTaggedValue::Exception());
}

JSTaggedValue SlowRuntimeStub::StArraySpread(JSThread *thread, JSTaggedValue dst, JSTaggedValue index,
                                             JSTaggedValue src)
{
    INTERPRETER_TRACE(thread, StArraySpread);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> dst_handle(thread, dst);
    JSHandle<JSTaggedValue> src_handle(thread, src);
    JSMutableHandle<JSTaggedValue> index_handle(thread, index);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (src_handle->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "undefined is not iterable", JSTaggedValue::Exception());
    }
    if (src_handle->IsNull()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "null is not iterable", JSTaggedValue::Exception());
    }
    ASSERT(dst_handle->IsJSArray());
    if (src_handle->IsString()) {
        JSHandle<EcmaString> src_string = JSTaggedValue::ToString(thread, src_handle);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        uint32_t dst_len = index_handle.GetTaggedValue().GetInt();
        uint32_t str_len = src_string->GetLength();
        uint32_t prop_counter = 0;
        for (uint32_t i = 0; i < str_len; i++, prop_counter++) {
            uint16_t res = src_string->At<false>(i);
            if (UNLIKELY(utf::IsUTF16SurrogatePair(res))) {
                std::array<uint16_t, 2> res_surrogate_pair {};
                res_surrogate_pair[0] = src_string->At<false>(i);
                res_surrogate_pair[1] = src_string->At<false>(i + 1);
                JSHandle<JSTaggedValue> str_value_surrogate_pair(
                    factory->NewFromUtf16Literal(res_surrogate_pair.data(), 2));
                JSTaggedValue::SetProperty(thread, dst_handle, dst_len + prop_counter, str_value_surrogate_pair, true);
                i++;
            } else {
                JSHandle<JSTaggedValue> str_value(factory->NewFromUtf16Literal(&res, 1));
                JSTaggedValue::SetProperty(thread, dst_handle, dst_len + prop_counter, str_value, true);
            }
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        return JSTaggedValue(dst_len + prop_counter);
    }

    JSHandle<JSTaggedValue> iter;
    auto global_const = thread->GlobalConstants();
    if (src_handle->IsJSArrayIterator() || src_handle->IsJSMapIterator() || src_handle->IsJSSetIterator() ||
        src_handle->IsIterator()) {
        iter = src_handle;
    } else if (src_handle->IsJSArray() || src_handle->IsJSMap() || src_handle->IsTypedArray() ||
               src_handle->IsJSSet()) {
        JSHandle<JSTaggedValue> values_str = global_const->GetHandledValuesString();
        JSHandle<JSTaggedValue> values_method = JSObject::GetMethod(thread, src_handle, values_str);
        iter = JSIterator::GetIterator(thread, src_handle, values_method);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    } else {
        iter = JSIterator::GetIterator(thread, src_handle);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    JSHandle<JSTaggedValue> value_str = global_const->GetHandledValueString();
    PropertyDescriptor desc(thread);
    JSHandle<JSTaggedValue> iter_result;
    do {
        iter_result = JSIterator::IteratorStep(thread, iter);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (iter_result->IsFalse()) {
            break;
        }
        bool success = JSTaggedValue::GetOwnProperty(thread, iter_result, value_str, desc);
        if (success && desc.IsEnumerable()) {
            JSTaggedValue::DefineOwnProperty(thread, dst_handle, index_handle, desc);
            int tmp = index_handle->GetInt();
            index_handle.Update(JSTaggedValue(tmp + 1));
        }
    } while (true);

    return index_handle.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::DefineGeneratorFunc(JSThread *thread, JSMethod *method)
{
    INTERPRETER_TRACE(thread, DefineGeneratorFunc);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSFunction> js_func = factory->NewJSGeneratorFunction(method);
    ASSERT_NO_ABRUPT_COMPLETION(thread);

    // 26.3.4.3 prototype
    // Whenever a GeneratorFunction instance is created another ordinary object is also created and
    // is the initial value of the generator function's "prototype" property.
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> initial_generator_func_prototype =
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSObject::SetPrototype(thread, initial_generator_func_prototype, env->GetGeneratorPrototype());
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    js_func->SetProtoOrDynClass(thread, initial_generator_func_prototype);
    js_func->SetupFunctionLength(thread);

    return js_func.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::DefineAsyncGeneratorFunc(JSThread *thread, JSMethod *method)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSFunction> js_func = factory->NewJSAsyncGeneratorFunction(method);
    ASSERT_NO_ABRUPT_COMPLETION(thread);

    // 27.4.4.3 prototype
    // Whenever a AsyncGeneratorFunction instance is created another ordinary object is also created and
    // is the initial value of the generator function's "prototype" property.
    JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
    JSHandle<JSObject> initial_async_generator_func_prototype =
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_fun), obj_fun);
    JSObject::SetPrototype(thread, initial_async_generator_func_prototype, env->GetAsyncGeneratorPrototype());
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    js_func->SetProtoOrDynClass(thread, initial_async_generator_func_prototype);
    js_func->SetupFunctionLength(thread);

    return js_func.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::DefineAsyncFunc(JSThread *thread, JSMethod *method)
{
    INTERPRETER_TRACE(thread, DefineAsyncFunc);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncFunctionClass());
    JSHandle<JSFunction> js_func = factory->NewJSFunctionByDynClass(method, dynclass, FunctionKind::ASYNC_FUNCTION);
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    return js_func.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::DefineNCFuncDyn(JSThread *thread, JSMethod *method)
{
    INTERPRETER_TRACE(thread, DefineNCFuncDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithoutProto());
    JSHandle<JSFunction> js_func = factory->NewJSFunctionByDynClass(method, dynclass, FunctionKind::ARROW_FUNCTION);
    js_func->SetupFunctionLength(thread);
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    return js_func.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::DefinefuncDyn(JSThread *thread, JSMethod *method)
{
    INTERPRETER_TRACE(thread, DefinefuncDyn);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithProto());
    JSHandle<JSFunction> js_func = factory->NewJSFunctionByDynClass(method, dynclass, FunctionKind::BASE_CONSTRUCTOR);
    js_func->SetupFunctionLength(thread);
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    return js_func.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::SuperCall(JSThread *thread, JSTaggedValue func, JSTaggedValue new_target,
                                         uint16_t args_count, JSTaggedType *stkargs)
{
    INTERPRETER_TRACE(thread, SuperCall);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> func_handle(thread, func);
    JSHandle<JSTaggedValue> new_target_handle(thread, new_target);

    JSHandle<JSTaggedValue> super_func(thread, JSHandle<JSObject>::Cast(func_handle)->GetPrototype(thread));
    ASSERT(super_func->IsJSFunction());
    auto info = NewRuntimeCallInfo(thread, super_func, JSTaggedValue::Undefined(), new_target_handle, args_count);
    info->SetCallArg(args_count, stkargs);
    JSTaggedValue result = JSFunction::Construct(info.Get());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return result;
}

JSTaggedValue SlowRuntimeStub::SuperCallSpread(JSThread *thread, JSTaggedValue func, JSTaggedValue new_target,
                                               JSTaggedValue array)
{
    INTERPRETER_TRACE(thread, SuperCallSpread);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSTaggedValue> func_handle(thread, func);
    JSHandle<JSTaggedValue> new_target_handle(thread, new_target);
    JSHandle<JSTaggedValue> js_array(thread, array);

    JSHandle<JSTaggedValue> super_func(thread, JSHandle<JSObject>::Cast(func_handle)->GetPrototype(thread));
    ASSERT(super_func->IsJSFunction());

    JSHandle<TaggedArray> argv(thread, GetCallSpreadArgs(thread, js_array.GetTaggedValue()));

    auto info =
        NewRuntimeCallInfo(thread, super_func, JSTaggedValue::Undefined(), new_target_handle, argv->GetLength());
    info->SetCallArg(argv->GetLength(), argv->GetData());
    JSTaggedValue result = JSFunction::Construct(info.Get());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return result;
}

JSTaggedValue SlowRuntimeStub::DefineMethod(JSThread *thread, JSMethod *method,
                                            const JSHandle<JSTaggedValue> &home_object)
{
    INTERPRETER_TRACE(thread, DefineMethod);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ASSERT(home_object->IsECMAObject());

    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithoutProto());
    JSHandle<JSFunction> js_func = factory->NewJSFunctionByDynClass(method, dynclass, FunctionKind::NORMAL_FUNCTION);
    js_func->SetHomeObject(thread, home_object);
    js_func->SetupFunctionLength(thread);
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    return js_func.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::LdSuperByValue(JSThread *thread, JSTaggedValue obj, JSTaggedValue key,
                                              JSTaggedValue this_func)
{
    INTERPRETER_TRACE(thread, LdSuperByValue);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ASSERT(this_func.IsJSFunction());
    // get Homeobject form function
    JSHandle<JSTaggedValue> home_object(thread, JSFunction::Cast(this_func.GetTaggedObject())->GetHomeObject());

    if (obj.IsUndefined()) {
        return ThrowReferenceError(thread, obj, "this is uninitialized.");
    }
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, key);

    JSHandle<JSTaggedValue> prop_key(JSTaggedValue::ToPropertyKey(thread, prop_handle));
    JSHandle<JSTaggedValue> super_base(thread, JSTaggedValue::GetSuperBase(thread, home_object));
    JSTaggedValue::RequireObjectCoercible(thread, super_base);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    JSTaggedValue res =
        JSTaggedValue::GetProperty(thread, super_base, prop_key, obj_handle).GetValue().GetTaggedValue();
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return res;
}

JSTaggedValue SlowRuntimeStub::StSuperByValue(JSThread *thread, JSTaggedValue obj, JSTaggedValue key,
                                              JSTaggedValue value, JSTaggedValue this_func)
{
    INTERPRETER_TRACE(thread, StSuperByValue);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ASSERT(this_func.IsJSFunction());
    // get Homeobject form function
    JSHandle<JSTaggedValue> home_object(thread, JSFunction::Cast(this_func.GetTaggedObject())->GetHomeObject());

    if (obj.IsUndefined()) {
        return ThrowReferenceError(thread, obj, "this is uninitialized.");
    }
    JSHandle<JSTaggedValue> obj_handle(thread, obj);
    JSHandle<JSTaggedValue> prop_handle(thread, key);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    JSHandle<JSTaggedValue> prop_key(JSTaggedValue::ToPropertyKey(thread, prop_handle));
    JSHandle<JSTaggedValue> super_base(thread, JSTaggedValue::GetSuperBase(thread, home_object));
    JSTaggedValue::RequireObjectCoercible(thread, super_base);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // check may_throw is false?
    JSTaggedValue::SetProperty(thread, super_base, prop_key, value_handle, obj_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return JSTaggedValue::True();
}

JSTaggedValue SlowRuntimeStub::GetCallSpreadArgs(JSThread *thread, JSTaggedValue array)
{
    INTERPRETER_TRACE(thread, GetCallSpreadArgs);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<JSTaggedValue> js_array(thread, array);
    uint32_t argv_may_max_length = JSHandle<JSArray>::Cast(js_array)->GetArrayLength();
    JSHandle<TaggedArray> argv = factory->NewTaggedArray(argv_may_max_length);
    JSHandle<JSTaggedValue> itor = JSIterator::GetIterator(thread, js_array);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSMutableHandle<JSTaggedValue> next(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> next_arg(thread, JSTaggedValue::Undefined());
    size_t argv_index = 0;
    while (true) {
        next.Update(JSIterator::IteratorStep(thread, itor).GetTaggedValue());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        if (JSTaggedValue::SameValue(next.GetTaggedValue(), JSTaggedValue::False())) {
            break;
        }
        next_arg.Update(JSIterator::IteratorValue(thread, next).GetTaggedValue());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        argv->Set(thread, argv_index++, next_arg);
    }

    argv = factory->CopyArray(argv, argv_may_max_length, argv_index);
    return argv.GetTaggedValue();
}

void SlowRuntimeStub::ThrowDeleteSuperProperty(JSThread *thread)
{
    INTERPRETER_TRACE(thread, ThrowDeleteSuperProperty);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> info = factory->NewFromCanBeCompressString("Can not delete super property");
    JSHandle<JSObject> error_obj = factory->NewJSError(base::ErrorType::REFERENCE_ERROR, info);
    THROW_NEW_ERROR_AND_RETURN(thread, error_obj.GetTaggedValue());
}

JSTaggedValue SlowRuntimeStub::NotifyInlineCache(JSThread *thread, JSFunction *func, JSMethod *method)
{
    INTERPRETER_TRACE(thread, NotifyInlineCache);

    if (method->GetProfileSize() != 0) {
        thread->GetEcmaVM()->AddMethodToProfile(method);
    }

    if (method->GetICMapping() == nullptr) {
        return JSTaggedValue::Undefined();
    }
    uint32_t ic_slot_size = method->GetSlotSize();
    static_assert(std::is_pointer<JSMethod::ICMappingType>::value);
    ASSERT(ic_slot_size <= std::numeric_limits<std::remove_pointer<JSMethod::ICMappingType>::type>::max());
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    JSHandle<JSFunction> func_handle(thread, func);
    JSHandle<ProfileTypeInfo> profile_type_info = factory->NewProfileTypeInfo(ic_slot_size);
    func_handle->SetProfileTypeInfo(thread, profile_type_info.GetTaggedValue());

    return profile_type_info.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::ResolveClass(JSThread *thread, JSTaggedValue ctor, TaggedArray *literal,
                                            JSTaggedValue base, JSTaggedValue lexenv, ConstantPool *constpool)
{
    ASSERT(ctor.IsClassConstructor());
    JSHandle<JSFunction> cls(thread, ctor);
    JSHandle<TaggedArray> literal_buffer(thread, literal);
    JSHandle<JSTaggedValue> lexical_env(thread, lexenv);
    JSHandle<ConstantPool> constpool_handle(thread, constpool);

    SetClassInheritanceRelationship(thread, ctor, base);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    uint32_t literal_buffer_length = literal_buffer->GetLength();

    // only traverse the value of key-value pair
    for (uint32_t index = 1; index < literal_buffer_length - 1; index += 2) {  // 2: key-value pair
        JSTaggedValue value = literal_buffer->Get(index);
        if (LIKELY(value.IsJSFunction())) {
            JSFunction::Cast(value.GetTaggedObject())->SetLexicalEnv(thread, lexical_env.GetTaggedValue());
            JSFunction::Cast(value.GetTaggedObject())->SetConstantPool(thread, constpool_handle.GetTaggedValue());
        }
    }

    cls->SetResolved(thread);
    return cls.GetTaggedValue();
}

// clone class may need re-set inheritance relationship due to extends may be a variable.
JSTaggedValue SlowRuntimeStub::CloneClassFromTemplate(JSThread *thread, JSTaggedValue ctor, JSTaggedValue base,
                                                      JSTaggedValue lexenv, ConstantPool *constpool)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    ASSERT(ctor.IsClassConstructor());
    JSHandle<JSTaggedValue> lexenv_handle(thread, lexenv);
    JSHandle<JSTaggedValue> constpool_handle(thread, JSTaggedValue(constpool));
    JSHandle<JSTaggedValue> base_handle(thread, base);

    JSHandle<JSFunction> cls(thread, ctor);

    JSHandle<JSObject> cls_prototype(thread, cls->GetFunctionPrototype());

    bool can_share_hclass = false;
    if (cls->GetClass()->GetProto() == base_handle.GetTaggedValue()) {
        can_share_hclass = true;
    }

    JSHandle<JSFunction> clone_class = factory->CloneClassCtor(cls, lexenv_handle, can_share_hclass);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSObject> clone_class_prototype = factory->CloneObjectLiteral(
        JSHandle<JSObject>(cls_prototype), lexenv_handle, constpool_handle, can_share_hclass);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // After clone both, reset "constructor" and "prototype" properties.
    clone_class->SetFunctionPrototype(thread, clone_class_prototype.GetTaggedValue());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    PropertyDescriptor ctor_desc(thread, JSHandle<JSTaggedValue>(clone_class), true, false, true);
    JSTaggedValue::DefinePropertyOrThrow(thread, JSHandle<JSTaggedValue>(clone_class_prototype),
                                         global_const->GetHandledConstructorString(), ctor_desc);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    clone_class->SetHomeObject(thread, clone_class_prototype);

    if (!can_share_hclass) {
        SetClassInheritanceRelationship(thread, clone_class.GetTaggedValue(), base_handle.GetTaggedValue());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    return clone_class.GetTaggedValue();
}

JSTaggedValue SlowRuntimeStub::SetClassInheritanceRelationship(JSThread *thread, JSTaggedValue ctor, JSTaggedValue base)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    JSHandle<JSTaggedValue> cls(thread, ctor);
    ASSERT(cls->IsJSFunction());
    JSMutableHandle<JSTaggedValue> parent(thread, base);

    /*
     *         class A / class A extends null                             class A extends B
     *                                       a                                                 a
     *                                       |                                                 |
     *                                       |  __proto__                                      |  __proto__
     *                                       |                                                 |
     *       A            ---->         A.prototype                  A             ---->    A.prototype
     *       |                               |                       |                         |
     *       |  __proto__                    |  __proto__            |  __proto__              |  __proto__
     *       |                               |                       |                         |
     *   Function.prototype       Object.prototype / null            B             ---->    B.prototype
     */

    JSHandle<JSTaggedValue> parent_prototype;
    // hole means parent is not present
    if (parent->IsHole()) {
        JSHandle<JSFunction>::Cast(cls)->SetFunctionKind(thread, FunctionKind::CLASS_CONSTRUCTOR);
        parent_prototype = env->GetObjectFunctionPrototype();
        parent.Update(env->GetFunctionPrototype().GetTaggedValue());
    } else if (parent->IsNull()) {
        JSHandle<JSFunction>::Cast(cls)->SetFunctionKind(thread, FunctionKind::DERIVED_CONSTRUCTOR);
        parent_prototype = JSHandle<JSTaggedValue>(thread, JSTaggedValue::Null());
        parent.Update(env->GetFunctionPrototype().GetTaggedValue());
    } else if (!parent->IsConstructor()) {
        return ThrowTypeError(thread, "parent class is not constructor");
    } else {
        JSHandle<JSFunction>::Cast(cls)->SetFunctionKind(thread, FunctionKind::DERIVED_CONSTRUCTOR);
        parent_prototype =
            JSTaggedValue::GetProperty(thread, parent, global_const->GetHandledPrototypeString()).GetValue();
        if (!parent_prototype->IsECMAObject() && !parent_prototype->IsNull()) {
            return ThrowTypeError(thread, "parent class have no valid prototype");
        }
    }
    cls->GetTaggedObject()->GetClass()->SetPrototype(thread, parent);

    JSHandle<JSObject> cls_prototype(thread, JSHandle<JSFunction>(cls)->GetFunctionPrototype());
    cls_prototype->GetClass()->SetPrototype(thread, parent_prototype);

    return JSTaggedValue::Undefined();
}

JSTaggedValue SlowRuntimeStub::SetClassConstructorLength(JSThread *thread, JSTaggedValue ctor, JSTaggedValue length)
{
    ASSERT(ctor.IsClassConstructor());

    JSFunction *cls = JSFunction::Cast(ctor.GetTaggedObject());
    if (LIKELY(!cls->GetClass()->IsDictionaryMode())) {
        cls->SetPropertyInlinedProps(thread, JSFunction::LENGTH_INLINE_PROPERTY_INDEX, length);
    } else {
        const GlobalEnvConstants *global_const = thread->GlobalConstants();
        cls->UpdatePropertyInDictionary(thread, global_const->GetLengthString(), length);
    }
    return JSTaggedValue::Undefined();
}

JSTaggedValue SlowRuntimeStub::LdEvalVar(JSThread *thread, JSTaggedValue name, JSTaggedValue eval_bindings)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<EcmaString> name_handle(thread, name);
    JSHandle<JSArray> eval_bindings_handle(thread, eval_bindings);

    uint32_t index = 0;
    JSMutableHandle<LexicalEnv> lex_env(
        thread, JSArray::FastGetPropertyByValue(thread, JSHandle<JSTaggedValue>::Cast(eval_bindings_handle), index++));

    ArraySizeT size = eval_bindings_handle->GetArrayLength();

    while (index < size) {
        auto scope = JSHandle<TaggedArray>::Cast(
            JSArray::FastGetPropertyByValue(thread, JSHandle<JSTaggedValue>::Cast(eval_bindings_handle), index++));

        ArraySizeT slot = 0;
        for (ArraySizeT j = 0; j < scope->GetLength(); slot++) {
            JSTaggedValue binding = scope->Get(j++);

            if (binding.IsTrue()) {
                binding = scope->Get(j++);
            }

            if (EcmaString::StringsAreEqual(*name_handle, static_cast<EcmaString *>(binding.GetHeapObject()))) {
                return lex_env->GetProperties(slot);
            }
        }

        lex_env.Update(lex_env->GetParentEnv());
    }

    auto global_obj = thread->GetGlobalObject();

    bool found = false;
    JSTaggedValue result = FastRuntimeStub::GetGlobalOwnProperty(global_obj, name_handle.GetTaggedValue(), &found);
    if (found) {
        return result;
    }
    return TryLdGlobalByName(thread, global_obj, name_handle.GetTaggedValue());
}

JSTaggedValue SlowRuntimeStub::StEvalVar(JSThread *thread, JSTaggedValue name, JSTaggedValue eval_bindings,
                                         JSTaggedValue value)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<EcmaString> name_handle(thread, name);
    JSHandle<JSArray> eval_bindings_handle(thread, eval_bindings);
    JSHandle<JSTaggedValue> value_handle(thread, value);

    uint32_t index = 0;
    JSMutableHandle<LexicalEnv> lex_env(
        thread, JSArray::FastGetPropertyByValue(thread, JSHandle<JSTaggedValue>::Cast(eval_bindings_handle), index++));

    ArraySizeT size = eval_bindings_handle->GetArrayLength();

    while (index < size) {
        auto scope = JSHandle<TaggedArray>::Cast(
            JSArray::FastGetPropertyByValue(thread, JSHandle<JSTaggedValue>::Cast(eval_bindings_handle), index++));

        ArraySizeT slot = 0;
        for (ArraySizeT j = 0; j < scope->GetLength(); slot++) {
            JSTaggedValue binding = scope->Get(j++);
            bool is_const = false;

            if (binding.IsTrue()) {
                is_const = true;
                binding = scope->Get(j++);
            }

            if (EcmaString::StringsAreEqual(*name_handle, static_cast<EcmaString *>(binding.GetHeapObject()))) {
                if (is_const) {
                    return ThrowTypeError(thread, "Assignment to constant variable");
                }
                lex_env->SetProperties(thread, slot, value_handle.GetTaggedValue());
                return value_handle.GetTaggedValue();
            }
        }

        lex_env.Update(lex_env->GetParentEnv());
    }

    JSObject::GlobalSetProperty(thread, JSHandle<JSTaggedValue>::Cast(name_handle), value_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return value_handle.GetTaggedValue();
}

}  // namespace panda::ecmascript
