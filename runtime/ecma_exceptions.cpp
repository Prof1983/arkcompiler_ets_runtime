/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_exceptions.h"
#include "macros.h"
#include "plugins/ecmascript/runtime/base/error_helper.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_object.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript {
void SetException(JSThread *thread, JSObject *error)
{
    if (LIKELY(!thread->HasPendingException())) {
        thread->SetException(JSTaggedValue(error));
    }
}

void ThrowException(JSThread *thread, const char *name, const char *msg)
{
    auto js_thread = static_cast<JSThread *>(thread);
    [[maybe_unused]] EcmaHandleScope handle_scope(js_thread);
    ObjectFactory *factory = js_thread->GetEcmaVM()->GetFactory();
    if (std::strcmp(name, REFERENCE_ERROR_STRING) == 0) {
        SetException(js_thread, *factory->GetJSError(base::ErrorType::REFERENCE_ERROR, msg));
        return;
    }

    if (std::strcmp(name, TYPE_ERROR_STRING) == 0) {
        SetException(js_thread, *factory->GetJSError(base::ErrorType::TYPE_ERROR, msg));
        return;
    }

    if (std::strcmp(name, RANGE_ERROR_STRING) == 0) {
        SetException(js_thread, *factory->GetJSError(base::ErrorType::RANGE_ERROR, msg));
        return;
    }
    UNREACHABLE();
}
}  // namespace panda::ecmascript
