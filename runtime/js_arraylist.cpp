/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_arraylist.h"

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript {
void JSArrayList::Add(JSThread *thread, const JSHandle<JSArrayList> &array_list, const JSHandle<JSTaggedValue> &value)
{
    // GrowCapacity
    uint32_t length = array_list->GetLength().GetArrayLength();
    JSHandle<TaggedArray> elements = GrowCapacity(thread, array_list, length + 1);

    ASSERT(!elements->IsDictionaryMode());
    elements->Set(thread, length, value);
    array_list->SetLength(thread, JSTaggedValue(++length));
}

JSHandle<TaggedArray> JSArrayList::GrowCapacity(const JSThread *thread, const JSHandle<JSArrayList> &obj,
                                                uint32_t capacity)
{
    JSHandle<TaggedArray> old_elements(thread, obj->GetElements());
    uint32_t old_length = old_elements->GetLength();
    if (capacity < old_length) {
        return old_elements;
    }
    uint32_t new_capacity = ComputeCapacity(capacity);
    JSHandle<TaggedArray> new_elements =
        thread->GetEcmaVM()->GetFactory()->CopyArray(old_elements, old_length, new_capacity);

    obj->SetElements(thread, new_elements);
    return new_elements;
}

JSTaggedValue JSArrayList::Get(JSThread *thread, const uint32_t index)
{
    if (index >= GetLength().GetArrayLength()) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Get property index out-of-bounds", JSTaggedValue::Exception());
    }

    TaggedArray *elements = TaggedArray::Cast(GetElements().GetTaggedObject());
    return elements->Get(index);
}

JSTaggedValue JSArrayList::Set(JSThread *thread, const uint32_t index, JSTaggedValue value)
{
    if (index >= GetLength().GetArrayLength()) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Set property index out-of-bounds", JSTaggedValue::Exception());
    }

    TaggedArray *elements = TaggedArray::Cast(GetElements().GetTaggedObject());
    elements->Set(thread, index, value);
    return JSTaggedValue::Undefined();
}

bool JSArrayList::Delete(JSThread *thread, const JSHandle<JSArrayList> &obj, const JSHandle<JSTaggedValue> &key)
{
    uint32_t index = 0;
    if (UNLIKELY(JSTaggedValue::ToElementIndex(key.GetTaggedValue(), &index))) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Can not delete a type other than number", false);
    }
    uint32_t length = obj->GetLength().GetArrayLength();
    if (index >= length) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Delete property index out-of-bounds", false);
    }
    TaggedArray *elements = TaggedArray::Cast(obj->GetElements().GetTaggedObject());
    for (uint32_t i = 0; i < length - 1; i++) {
        elements->Set(thread, i, elements->Get(i + 1));
    }
    obj->SetLength(thread, JSTaggedValue(--length));
    return true;
}

bool JSArrayList::Has(JSTaggedValue value) const
{
    TaggedArray *elements = TaggedArray::Cast(GetElements().GetTaggedObject());
    return !(elements->GetIdx(value) == TaggedArray::MAX_ARRAY_INDEX);
}

JSHandle<TaggedArray> JSArrayList::OwnKeys(JSThread *thread, const JSHandle<JSArrayList> &obj)
{
    uint32_t length = obj->GetLength().GetArrayLength();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> keys = factory->NewTaggedArray(length);

    for (uint32_t i = 0; i < length; i++) {
        keys->Set(thread, i, JSTaggedValue(i));
    }

    return keys;
}

bool JSArrayList::GetOwnProperty(JSThread *thread, const JSHandle<JSArrayList> &obj, const JSHandle<JSTaggedValue> &key,
                                 PropertyDescriptor &desc)
{
    uint32_t index = 0;
    if (UNLIKELY(JSTaggedValue::ToElementIndex(key.GetTaggedValue(), &index))) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Can not get property whose type is not number", false);
    }

    uint32_t length = obj->GetLength().GetArrayLength();
    if (index >= length) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Get property index out-of-bounds", false);
    }
    return JSObject::GetOwnProperty(thread, JSHandle<JSObject>::Cast(obj), key, desc);
}
}  // namespace panda::ecmascript
