/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_TYPED_ARRAY_H
#define ECMASCRIPT_JS_TYPED_ARRAY_H

#include "plugins/ecmascript/runtime/tagged_array.h"
#include "js_object.h"

namespace panda::ecmascript {
enum class ContentType : uint8_t { NONE = 1, NUMBER, BIG_INT };
class JSTypedArray : public JSObject {
public:
    static JSTypedArray *Cast(ObjectHeader *object)
    {
#if ECMASCRIPT_ENABLE_CAST_CHECK
        if (!(JSTaggedValue(object).IsTypedArray() || JSTaggedValue(object).IsJSTypedArray())) {
            std::abort();
        }
#else
        ASSERT(JSTaggedValue(object).IsTypedArray() || JSTaggedValue(object).IsJSTypedArray());
#endif
        return static_cast<JSTypedArray *>(object);
    }

    static JSHandle<JSTaggedValue> ToPropKey(JSThread *thread, const JSHandle<JSTaggedValue> &key);

    // 9.4.5 Integer-Indexed Exotic Objects
    // 9.4.5.1 [[GetOwnProperty]] ( P )
    static bool GetOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                               const JSHandle<JSTaggedValue> &key, PropertyDescriptor &desc);
    // 9.4.5.2 [[HasProperty]] ( P )
    static bool HasProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                            const JSHandle<JSTaggedValue> &key);
    // 9.4.5.3 [[DefineOwnProperty]] ( P, Desc )
    static bool DefineOwnProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                  const JSHandle<JSTaggedValue> &key, const PropertyDescriptor &desc);
    // 9.4.5.4 [[Get]] ( P, Receiver )
    static inline OperationResult GetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                              const JSHandle<JSTaggedValue> &key)
    {
        return GetProperty(thread, typedarray, key, typedarray);
    }
    static inline OperationResult GetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                              uint32_t index)
    {
        return FastElementGet(thread, typedarray, index);
    }
    static OperationResult GetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                       const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &receiver);
    // 9.4.5.5 [[Set]] ( P, V, Receiver )
    static inline bool SetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                   const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value,
                                   bool may_throw = false)
    {
        return SetProperty(thread, typedarray, key, value, typedarray, may_throw);
    }
    static bool SetProperty(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                            const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value,
                            const JSHandle<JSTaggedValue> &receiver, bool may_throw = false);
    // 9.4.5.6 [[OwnPropertyKeys]] ( )
    static JSHandle<TaggedArray> OwnPropertyKeys(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray);
    // 9.4.5.7 IntegerIndexedObjectCreate (prototype, internalSlotsList)
    // 9.4.5.8 IntegerIndexedElementGet ( O, index )
    static OperationResult IntegerIndexedElementGet(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                                    JSTaggedValue index);
    static OperationResult FastElementGet(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray, uint32_t index);
    static bool FastCopyElementToArray(JSThread *thread, const JSHandle<JSTaggedValue> &typed_array,
                                       JSHandle<TaggedArray> &array);
    // 9.4.5.9 IntegerIndexedElementSet ( O, index, value )
    static bool IntegerIndexedElementSet(JSThread *thread, const JSHandle<JSTaggedValue> &typedarray,
                                         JSTaggedValue index, const JSHandle<JSTaggedValue> &value);

    static constexpr size_t VIEWED_ARRAY_BUFFER_OFFSET = JSObject::SIZE;
    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, ViewedArrayBuffer)
    ACCESSORS(1, TypedArrayName)
    ACCESSORS(2, ByteLength)
    ACCESSORS(3, ByteOffset)
    ACCESSORS(4, ArrayLength)
    ACCESSORS_PRIMITIVE_FIELD(5, ContentType, ContentType)
    ACCESSORS_FINISH(6)

    static const uint32_t MAX_TYPED_ARRAY_INDEX = MAX_ELEMENT_INDEX;
    DECL_DUMP()
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_JS_TYPED_ARRAY_H
