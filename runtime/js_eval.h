/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JSEVAL_H
#define ECMASCRIPT_JSEVAL_H

#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::es2panda {
struct CompilerOptions;
}  // namespace panda::es2panda

namespace panda::ecmascript {
class EvalUtils {
public:
    enum class DynamicFunctionKind { NORMAL, GENERATOR, ASYNC, ASYNC_GENERATOR };

    static JSTaggedValue DirectEval(JSThread *thread, uint32_t parser_status, JSTaggedValue arg0,
                                    JSTaggedValue evalbindings);
    static JSTaggedValue Eval(JSThread *thread, const JSHandle<JSTaggedValue> &arg0);
    static JSTaggedValue CreateDynamicFunction(EcmaRuntimeCallInfo *argv, DynamicFunctionKind kind);

private:
    static JSTaggedValue GetEvaluatedScript(JSThread *thread, const JSHandle<EcmaString> &str,
                                            const es2panda::CompilerOptions &options, uint32_t parser_status = 0);
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JSEVAL_H
