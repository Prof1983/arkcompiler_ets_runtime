/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_SET_ITERATOR_H
#define ECMASCRIPT_JS_SET_ITERATOR_H

#include "js_object.h"
#include "js_iterator.h"

namespace panda::ecmascript {
class JSSetIterator : public JSObject {
public:
    CAST_CHECK(JSSetIterator, IsJSSetIterator);

    static JSHandle<JSTaggedValue> CreateSetIterator(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                     IterationKind kind);

    static JSTaggedValue Next(EcmaRuntimeCallInfo *argv);

    void Update(const JSThread *thread);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, IteratedSet)
    ACCESSORS(1, NextIndex)
    ACCESSORS(2, IterationKind)
    ACCESSORS_FINISH(3)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_SET_ITERATOR_H
