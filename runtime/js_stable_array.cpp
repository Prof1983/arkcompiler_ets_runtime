/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_stable_array.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "interpreter/fast_runtime_stub-inl.h"

namespace panda::ecmascript {
JSTaggedValue JSStableArray::Push(JSHandle<JSArray> receiver, EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    uint32_t argc = argv->GetArgsNumber();
    uint32_t old_length = receiver->GetArrayLength();
    uint32_t new_length = argc + old_length;

    TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetTaggedObject());
    if (new_length > elements->GetLength()) {
        elements = *JSObject::GrowElementsCapacity(thread, JSHandle<JSObject>::Cast(receiver), new_length);
    }

    for (uint32_t k = 0; k < argc; k++) {
        JSHandle<JSTaggedValue> value = argv->GetCallArg(k);
        elements->Set(thread, old_length + k, value.GetTaggedValue());
    }
    receiver->SetArrayLength(thread, new_length);

    return JSTaggedValue(new_length);
}

JSTaggedValue JSStableArray::Pop(JSHandle<JSArray> receiver, EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    uint32_t length = receiver->GetArrayLength();
    if (length == 0) {
        return JSTaggedValue::Undefined();
    }

    TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetTaggedObject());
    uint32_t capacity = elements->GetLength();
    uint32_t index = length - 1;
    auto result = elements->Get(index);
    if (TaggedArray::ShouldTrim(thread, capacity, index)) {
        elements->Trim(thread, index);
    } else {
        elements->Set(thread, index, JSTaggedValue::Hole());
    }
    receiver->SetArrayLength(thread, index);
    return result;
}

JSTaggedValue JSStableArray::Splice(JSHandle<JSArray> receiver, EcmaRuntimeCallInfo *argv, double start,
                                    double insert_count, double actual_delete_count)
{
    JSThread *thread = argv->GetThread();
    uint32_t len = receiver->GetArrayLength();
    uint32_t argc = argv->GetArgsNumber();

    JSHandle<JSObject> this_obj_handle(receiver);
    JSTaggedValue new_array = JSArray::ArraySpeciesCreate(thread, this_obj_handle, JSTaggedNumber(actual_delete_count));
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    JSHandle<JSObject> new_array_handle(thread, new_array);

    JSHandle<JSTaggedValue> this_obj_val(this_obj_handle);
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    TaggedArray *src_elements = TaggedArray::Cast(this_obj_handle->GetElements().GetTaggedObject());
    JSHandle<TaggedArray> src_elements_handle(thread, src_elements);
    if (new_array.IsStableJSArray(thread)) {
        TaggedArray *dest_elements = TaggedArray::Cast(new_array_handle->GetElements().GetTaggedObject());
        if (actual_delete_count > dest_elements->GetLength()) {
            dest_elements = *JSObject::GrowElementsCapacity(thread, new_array_handle, actual_delete_count);
        }

        for (uint32_t idx = 0; idx < actual_delete_count; idx++) {
            dest_elements->Set(thread, idx, src_elements_handle->Get(start + idx));
        }
        JSHandle<JSArray>::Cast(new_array_handle)->SetArrayLength(thread, actual_delete_count);
    } else {
        JSMutableHandle<JSTaggedValue> from_key(thread, JSTaggedValue::Undefined());
        JSMutableHandle<JSTaggedValue> to_key(thread, JSTaggedValue::Undefined());
        double k = 0;
        while (k < actual_delete_count) {
            double from = start + k;
            from_key.Update(JSTaggedValue(from));
            bool exists = JSTaggedValue::HasProperty(thread, this_obj_val, from_key);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (exists) {
                JSHandle<JSTaggedValue> from_value = JSArray::FastGetPropertyByValue(thread, this_obj_val, from_key);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                to_key.Update(JSTaggedValue(k));
                if (new_array_handle->IsJSProxy()) {
                    to_key.Update(JSTaggedValue::ToString(thread, to_key).GetTaggedValue());
                }
                JSObject::CreateDataPropertyOrThrow(thread, new_array_handle, to_key, from_value);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            }
            k++;
        }

        JSHandle<JSTaggedValue> delete_count(thread, JSTaggedValue(actual_delete_count));
        JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(new_array_handle), length_key, delete_count,
                                   true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    }

    uint32_t old_capacity = src_elements_handle->GetLength();
    uint32_t new_capacity = len - actual_delete_count + insert_count;
    if (insert_count < actual_delete_count) {
        for (uint32_t idx = start; idx < len - actual_delete_count; idx++) {
            auto element = src_elements_handle->Get(idx + actual_delete_count);
            element = element.IsHole() ? JSTaggedValue::Undefined() : element;
            src_elements_handle->Set(thread, idx + insert_count, element);
        }

        if (TaggedArray::ShouldTrim(thread, old_capacity, new_capacity)) {
            src_elements_handle->Trim(thread, new_capacity);
        } else {
            for (uint32_t idx = new_capacity; idx < len; idx++) {
                src_elements_handle->Set(thread, idx, JSTaggedValue::Hole());
            }
        }
    } else {
        if (new_capacity > old_capacity) {
            src_elements_handle = JSObject::GrowElementsCapacity(thread, this_obj_handle, new_capacity);
        }
        for (uint32_t idx = len - actual_delete_count; idx > start; idx--) {
            auto element = src_elements_handle->Get(idx + actual_delete_count - 1);
            element = element.IsHole() ? JSTaggedValue::Undefined() : element;
            src_elements_handle->Set(thread, idx + insert_count - 1, element);
        }
    }

    for (uint32_t i = 2, idx = start; i < argc; i++, idx++) {
        src_elements_handle->Set(thread, idx, argv->GetCallArg(i));
    }

    JSHandle<JSTaggedValue> new_len_handle(thread, JSTaggedValue(new_capacity));
    JSTaggedValue::SetProperty(thread, this_obj_val, length_key, new_len_handle, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    return new_array_handle.GetTaggedValue();
}

JSTaggedValue JSStableArray::Shift(JSHandle<JSArray> receiver, EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    uint32_t length = receiver->GetArrayLength();
    if (length == 0) {
        return JSTaggedValue::Undefined();
    }

    TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetTaggedObject());
    auto result = elements->Get(0);
    for (uint32_t k = 1; k < length; k++) {
        auto k_value = elements->Get(k);
        if (k_value.IsHole()) {
            elements->Set(thread, k - 1, JSTaggedValue::Undefined());
        } else {
            elements->Set(thread, k - 1, k_value);
        }
    }
    uint32_t capacity = elements->GetLength();
    uint32_t index = length - 1;
    if (TaggedArray::ShouldTrim(thread, capacity, index)) {
        elements->Trim(thread, index);
    } else {
        elements->Set(thread, index, JSTaggedValue::Hole());
    }
    receiver->SetArrayLength(thread, index);
    return result;
}

JSTaggedValue JSStableArray::Join(JSHandle<JSArray> receiver, EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    uint32_t length = receiver->GetArrayLength();
    JSHandle<JSTaggedValue> sep_handle = builtins_common::GetCallArg(argv, 0);
    int sep = ',';
    uint32_t sep_length = 1;
    JSHandle<EcmaString> sep_string_handle;
    if (!sep_handle->IsUndefined()) {
        if (sep_handle->IsString()) {
            sep_string_handle = JSHandle<EcmaString>::Cast(sep_handle);
        } else {
            sep_string_handle = JSTaggedValue::ToString(thread, sep_handle);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        }
        if (sep_string_handle->IsUtf8() && sep_string_handle->GetLength() == 1) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            sep = sep_string_handle->GetDataUtf8()[0];
        } else if (sep_string_handle->GetLength() == 0) {
            sep = JSStableArray::SeparatorFlag::MINUS_TWO;
            sep_length = 0;
        } else {
            sep = JSStableArray::SeparatorFlag::MINUS_ONE;
            sep_length = sep_string_handle->GetLength();
        }
    }
    if (length == 0) {
        const GlobalEnvConstants *global_const = thread->GlobalConstants();
        return global_const->GetEmptyString();
    }
    TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetTaggedObject());
    size_t allocate_length = 0;
    bool is_one_byte = (sep != JSStableArray::SeparatorFlag::MINUS_ONE) || sep_string_handle->IsUtf8();
    PandaVector<JSHandle<EcmaString>> vec;
    JSMutableHandle<JSTaggedValue> element_handle(thread, JSTaggedValue::Undefined());
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    for (uint32_t k = 0; k < length; k++) {
        JSTaggedValue element = elements->Get(k);
        if (!element.IsUndefinedOrNull() && !element.IsHole()) {
            if (!element.IsString()) {
                element_handle.Update(element);
                JSHandle<EcmaString> str_element = JSTaggedValue::ToString(thread, element_handle);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
                element = str_element.GetTaggedValue();
                elements = TaggedArray::Cast(receiver->GetElements().GetTaggedObject());
            }
            auto next_str = EcmaString::Cast(element.GetTaggedObject());
            JSHandle<EcmaString> next_str_handle(thread, next_str);
            vec.push_back(next_str_handle);
            is_one_byte = next_str->IsUtf8() ? is_one_byte : false;
            allocate_length += next_str->GetLength();
        } else {
            vec.push_back(JSHandle<EcmaString>(global_const->GetHandledEmptyString()));
        }
    }
    allocate_length += sep_length * (length - 1);
    auto new_string = EcmaString::AllocStringObject(allocate_length, is_one_byte, thread->GetEcmaVM());
    int current = 0;
    for (uint32_t k = 0; k < length; k++) {
        if (k > 0) {
            if (sep >= 0) {
                new_string->WriteData(static_cast<char>(sep), current);
            } else if (sep != JSStableArray::SeparatorFlag::MINUS_TWO) {
                new_string->WriteData(*sep_string_handle, current, allocate_length - current, sep_length);
            }
            current += sep_length;
        }
        JSHandle<EcmaString> next_str = vec[k];
        int next_length = next_str->GetLength();
        new_string->WriteData(*next_str, current, allocate_length - current, next_length);
        current += next_length;
    }
    return JSTaggedValue(new_string);
}
}  // namespace panda::ecmascript
