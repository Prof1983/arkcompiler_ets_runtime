/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JSPROXY_H
#define ECMASCRIPT_JSPROXY_H

#include "plugins/ecmascript/runtime/tagged_array.h"
#include "js_object.h"

namespace panda::ecmascript {
class JSProxy final : public ECMAObject {
public:
    CAST_CHECK(JSProxy, IsJSProxy);

    // ES6 9.5.15 ProxyCreate(target, handler)
    static JSHandle<JSProxy> ProxyCreate(JSThread *thread, const JSHandle<JSTaggedValue> &target,
                                         const JSHandle<JSTaggedValue> &handler);
    // ES6 9.5.1 [[GetPrototypeOf]] ( )
    static JSTaggedValue GetPrototype(JSThread *thread, const JSHandle<JSProxy> &proxy);
    // ES6 9.5.2 [[SetPrototypeOf]] (V)
    static bool SetPrototype(JSThread *thread, const JSHandle<JSProxy> &proxy, const JSHandle<JSTaggedValue> &proto);
    // ES6 9.5.3 [[IsExtensible]] ( )
    static bool IsExtensible(JSThread *thread, const JSHandle<JSProxy> &proxy);
    // ES6 9.5.4 [[PreventExtensions]] ( )
    static bool PreventExtensions(JSThread *thread, const JSHandle<JSProxy> &proxy);
    // ES6 9.5.5 [[GetOwnProperty]] (P)
    static bool GetOwnProperty(JSThread *thread, const JSHandle<JSProxy> &proxy, const JSHandle<JSTaggedValue> &key,
                               PropertyDescriptor &desc);
    // ES6 9.5.6 [[DefineOwnProperty]] (P, Desc)
    static bool DefineOwnProperty(JSThread *thread, const JSHandle<JSProxy> &proxy, const JSHandle<JSTaggedValue> &key,
                                  const PropertyDescriptor &desc);
    // ES6 9.5.7 [[HasProperty]] (P)
    static bool HasProperty(JSThread *thread, const JSHandle<JSProxy> &proxy, const JSHandle<JSTaggedValue> &key);
    // ES6 9.5.8 [[Get]] (P, Receiver)
    static inline OperationResult GetProperty(JSThread *thread, const JSHandle<JSProxy> &proxy,
                                              const JSHandle<JSTaggedValue> &key)
    {
        return GetProperty(thread, proxy, key, JSHandle<JSTaggedValue>::Cast(proxy));
    }
    static OperationResult GetProperty(JSThread *thread, const JSHandle<JSProxy> &proxy,
                                       const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &receiver);
    // ES6 9.5.9 [[Set]] ( P, V, Receiver)
    static inline bool SetProperty(JSThread *thread, const JSHandle<JSProxy> &proxy, const JSHandle<JSTaggedValue> &key,
                                   const JSHandle<JSTaggedValue> &value, bool may_throw = false)
    {
        return SetProperty(thread, proxy, key, value, JSHandle<JSTaggedValue>::Cast(proxy), may_throw);
    }
    static bool SetProperty(JSThread *thread, const JSHandle<JSProxy> &proxy, const JSHandle<JSTaggedValue> &key,
                            const JSHandle<JSTaggedValue> &value, const JSHandle<JSTaggedValue> &receiver,
                            bool may_throw = false);
    // ES6 9.5.10 [[Delete]] (P)
    static bool DeleteProperty(JSThread *thread, const JSHandle<JSProxy> &proxy, const JSHandle<JSTaggedValue> &key);

    // ES6 9.5.12 [[OwnPropertyKeys]] ()
    static JSHandle<TaggedArray> OwnPropertyKeys(JSThread *thread, const JSHandle<JSProxy> &proxy);

    void SetConstructor(bool constructor) const
    {
        GetClass()->SetConstructor(constructor);
    }

    void SetCallTarget([[maybe_unused]] const JSThread *thread, JSMethod *p)
    {
        SetMethod(p);
    }

    JSHandle<JSTaggedValue> GetSourceTarget(JSThread *thread) const;

    // ES6 9.5.13 [[Call]] (thisArgument, argumentsList)
    static JSTaggedValue CallInternal(EcmaRuntimeCallInfo *info);
    // ES6 9.5.14 [[Construct]] ( argumentsList, new_target)
    static JSTaggedValue ConstructInternal(EcmaRuntimeCallInfo *info);

    ACCESSORS_BASE(ECMAObject)
    ACCESSORS(0, Target)
    ACCESSORS(1, Handler)
    ACCESSORS_NATIVE_FIELD(2, Method, JSMethod)
    ACCESSORS_FINISH(3)

    bool IsArray(JSThread *thread) const;
    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JSPROXY_H
