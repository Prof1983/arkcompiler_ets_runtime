/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include <mem/gc/gc_types.h>
#include "include/mem/panda_containers.h"
#include "include/mem/panda_string.h"
#include "plugins/ecmascript/runtime/bridge.h"

#include "js_tagged_value.h"
#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/builtins.h"
#include "plugins/ecmascript/runtime/builtins/builtins_regexp.h"
#include "plugins/ecmascript/runtime/class_linker/panda_file_translator.h"
#include "plugins/ecmascript/runtime/class_linker/program_object-inl.h"
#include "plugins/ecmascript/runtime/ecma_module.h"
#include "plugins/ecmascript/runtime/ecma_string_table.h"
#include "plugins/ecmascript/runtime/global_dictionary.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/global_env_constants-inl.h"
#include "plugins/ecmascript/runtime/global_env_constants.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/js_frame-inl.h"
#include "plugins/ecmascript/runtime/jobs/micro_job_queue.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_for_in_iterator.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_native_pointer.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/mem/object_xray-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/ecma_profiling.h"
#include "plugins/ecmascript/runtime/regexp/regexp_parser_cache.h"
#include "plugins/ecmascript/runtime/runtime_call_id.h"
#include "plugins/ecmascript/runtime/symbol_table.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/runtime/tagged_queue-inl.h"
#include "plugins/ecmascript/runtime/tagged_queue.h"
#include "plugins/ecmascript/runtime/template_map.h"
#include "plugins/ecmascript/runtime/vmstat/runtime_stat.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/thread_environment_api.h"
#include "plugins/ecmascript/runtime/ecma_class_linker_extension.h"
#include "plugins/ecmascript/runtime/mem/ecma_reference_processor.h"
#include "plugins/ecmascript/runtime/js_finalization_registry.h"
#include "include/runtime_notification.h"
#include "libpandafile/file.h"

#include "runtime/include/profiling_gen.h"

#include "runtime/compiler.h"
#include "runtime/include/thread_scopes.h"
#include "runtime/mem/gc/gc_root.h"
#include "runtime/mem/gc/reference-processor/empty_reference_processor.h"
#include "runtime/mem/object_helpers.h"
#include "plugins/ecmascript/runtime/compiler/ecmascript_runtime_interface.h"
#include "plugins/ecmascript/runtime/ecma_compiler.h"

#include "handle_scope-inl.h"
#include "include/coretypes/native_pointer.h"
#include "include/runtime.h"
#include "include/method.h"
#include "mem/internal_allocator.h"
#include "trace/trace.h"
#include "ecmastdlib_inline_gen.h"
#include "ir-dyn-base-types.h"
#include "ic/profile_type_info.h"
#include "serializer/serializer.h"

namespace panda::ecmascript {
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
static const std::string_view ENTRY_POINTER = "_GLOBAL::func_main_0";
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
static const std::string_view ENTRY_METHOD_POINTER = "func_main_0";
JSRuntimeOptions EcmaVM::options_;  // NOLINT(fuchsia-statically-constructed-objects)

// Create MemoryManager by RuntimeOptions
static mem::MemoryManager *CreateMM(const LanguageContext &ctx, mem::InternalAllocatorPtr internal_allocator,
                                    const RuntimeOptions &options)
{
    mem::MemoryManager::HeapOptions heap_options {
        nullptr,                                      // is_object_finalizeble_func
        nullptr,                                      // register_finalize_reference_func
        options.GetMaxGlobalRefSize(),                // max_global_ref_size
        options.IsGlobalReferenceSizeCheckEnabled(),  // is_global_reference_size_check_enabled
        true,                                         // is_single_thread
        options.IsUseTlabForAllocations(),            // is_use_tlab_for_allocations
        options.IsStartAsZygote(),                    // is_start_as_zygote
    };

    mem::GCTriggerConfig gc_trigger_config(options, panda_file::SourceLang::ECMASCRIPT);

    mem::GCSettings gc_settings(options, panda_file::SourceLang::ECMASCRIPT);

    mem::GCType gc_type = Runtime::GetGCType(options, panda_file::SourceLang::ECMASCRIPT);
    ASSERT(gc_type != mem::GCType::GEN_GC);

    return mem::MemoryManager::Create(ctx, internal_allocator, gc_type, gc_settings, gc_trigger_config, heap_options);
}

/* static */
EcmaVM *EcmaVM::Create(const JSRuntimeOptions &options)
{
    auto runtime = Runtime::GetCurrent();
    auto vm = runtime->GetInternalAllocator()->New<EcmaVM>(options);
    if (UNLIKELY(vm == nullptr)) {
        LOG_ECMA(ERROR) << "Failed to create jsvm";
        return nullptr;
    }
    vm->InitializeGC();
    auto js_thread = JSThread::Create(runtime, vm);
    vm->thread_ = js_thread;
    vm->thread_manager_->SetMainThread(js_thread);
    if (!vm->Initialize()) {
        LOG_ECMA(ERROR) << "Failed to initialize jsvm";
        runtime->GetInternalAllocator()->Delete(vm);
        return nullptr;
    }
    return vm;
}

// static
bool EcmaVM::Destroy(PandaVM *vm)
{
    if (vm != nullptr) {
        vm->SaveProfileInfo();
        vm->UninitializeThreads();
        vm->StopGC();
        auto runtime = Runtime::GetCurrent();
        runtime->GetInternalAllocator()->Delete(vm);
        return true;
    }
    return false;
}

// static
Expected<EcmaVM *, PandaString> EcmaVM::Create(Runtime *runtime, const JSRuntimeOptions &options)
{
    EcmaVM *vm = runtime->GetInternalAllocator()->New<EcmaVM>(options);
    if (UNLIKELY(vm == nullptr)) {
        LOG_ECMA(ERROR) << "Failed to create jsvm";
        return nullptr;
    }
    vm->InitializeGC();
    auto js_thread = ecmascript::JSThread::Create(runtime, vm);
    vm->thread_ = js_thread;
    vm->thread_manager_->SetMainThread(js_thread);
    return vm;
}

EcmaVM::EcmaVM(JSRuntimeOptions options) : string_table_(new EcmaStringTable(this))
{
    auto runtime = Runtime::GetCurrent();
    options_ = std::move(options);
    ic_enable_ = options_.IsIcEnable();
    optional_log_enabled_ = options_.IsEnableOptionalLog();
    framework_abc_file_name_ = options_.GetFrameworkAbcFile();

    auto allocator = runtime->GetInternalAllocator();
    thread_manager_ = allocator->New<SingleThreadManager>();
    rendezvous_ = allocator->New<Rendezvous>(this);
    notification_manager_ = allocator->New<RuntimeNotificationManager>(runtime->GetInternalAllocator());
    notification_manager_->SetRendezvous(rendezvous_);
    InitializeRandomEngine();

    LanguageContext ctx = Runtime::GetCurrent()->GetLanguageContext(panda_file::SourceLang::ECMASCRIPT);
    mm_ = CreateMM(ctx, Runtime::GetCurrent()->GetInternalAllocator(), options_);
    if (options_.IsReferenceProcessorEnable()) {
        ecma_reference_processor_ = MakePandaUnique<panda::mem::ecmascript::EcmaReferenceProcessor>(this);
    } else {
        ecma_reference_processor_ = MakePandaUnique<panda::mem::EmptyReferenceProcessor>();
    }

    is_profiling_enabled_ = options_.IsCompilerEnableJit() || options_.WasSetProfileOutput();

    if (is_profiling_enabled_) {
        call_profiling_table_ = allocator->New<EcmaCallProfilingTable>(options_.GetCallProfilingTableSize());
    }

    auto heap_manager = mm_->GetHeapManager();
    auto internal_allocator = heap_manager->GetInternalAllocator();
    runtime_iface_ = internal_allocator->New<EcmaRuntimeInterface>(this, internal_allocator);
    compiler_ = internal_allocator->New<EcmaCompiler>(heap_manager->GetCodeAllocator(), internal_allocator, options_,
                                                      heap_manager->GetMemStats(), runtime_iface_);
    SetFrameExtSize(ecmascript::JSExtFrame::GetExtSize());
}

void EcmaVM::LoadEcmaStdLib()
{
    if (GetNativeMethodWrapper() != nullptr) {
        // Ecmastdlib was loaded via parameters.
        return;
    }

    size_t fsize = 0;
    unsigned char const *data = panda::ecmascript::ecmastdlib_inline::GetEcmastdlibData(fsize);
    if (fsize == 0) {
        // Ecmastdlib is missing.
        LOG(FATAL, ECMASCRIPT) << "Ecmastdlib is missing!";
        return;
    }

    auto pf = panda_file::OpenPandaFileFromMemory(data, fsize);
    ASSERT(pf);
    Runtime::GetCurrent()->GetClassLinker()->AddPandaFile(std::move(pf));
}

bool EcmaVM::Initialize()
{
    ECMA_BYTRACE_NAME(BYTRACE_TAG_ARK, "EcmaVM::Initialize");
    auto runtime = Runtime::GetCurrent();

    auto global_const = const_cast<GlobalEnvConstants *>(thread_->GlobalConstants());
    reg_exp_parser_cache_ = new RegExpParserCache();
    factory_ = runtime->GetInternalAllocator()->New<ObjectFactory>(thread_);
    if (UNLIKELY(factory_ == nullptr)) {
        LOG_ECMA(FATAL) << "alloc factory_ failed";
        UNREACHABLE();
    }

    LanguageContext ctx = runtime->GetLanguageContext(panda_file::SourceLang::ECMASCRIPT);
    EcmaClassLinkerExtension::Cast(runtime->GetClassLinker()->GetExtension(ctx))->InitClasses(this);

    LoadEcmaStdLib();
    if (!intrinsics::Initialize(panda::panda_file::SourceLang::ECMASCRIPT)) {
        LOG(ERROR, RUNTIME) << "Failed to initialize Ecma intrinsics";
        return false;
    }

    [[maybe_unused]] EcmaHandleScope scope(thread_);
    LOG_ECMA(DEBUG) << "EcmaVM::Initialize run builtins";

    JSHandle<JSHClass> dyn_class_class_handle = factory_->NewEcmaDynClassClass(nullptr, JSHClass::SIZE, JSType::HCLASS);
    JSHClass *dynclass = reinterpret_cast<JSHClass *>(dyn_class_class_handle.GetTaggedValue().GetTaggedObject());
    dynclass->SetClass(dynclass);
    dynclass->GetHClass()->SetNativeFieldMask(JSHClass::NATIVE_FIELDS_MASK);
    JSHandle<JSHClass> global_env_class =
        factory_->CreateDynClass<GlobalEnv>(*dyn_class_class_handle, JSType::GLOBAL_ENV);

    JSHandle<GlobalEnv> global_env_handle = factory_->NewGlobalEnv(*global_env_class);
    global_env_ = global_env_handle.GetTaggedValue();

    // init global env
    global_const->InitRootsClass(thread_, *dyn_class_class_handle);
    factory_->ObtainRootClass(GetGlobalEnv());
    global_const->InitGlobalConstant(thread_);
    global_env_handle->SetEmptyArray(thread_, factory_->NewEmptyArray());
    global_env_handle->SetEmptyWeakArray(thread_, factory_->NewEmptyArray(true));
    auto layout_info_handle = factory_->CreateLayoutInfo(0);
    global_env_handle->SetEmptyLayoutInfo(thread_, layout_info_handle);
    auto symbol_table_handle = SymbolTable::Create(thread_);
    global_env_handle->SetRegisterSymbols(thread_, symbol_table_handle);
    JSTaggedValue empty_str = thread_->GlobalConstants()->GetEmptyString();
    string_table_->InternEmptyString(EcmaString::Cast(empty_str.GetTaggedObject()));
    auto tagged_queue_handle = factory_->NewTaggedQueue(0);
    global_env_handle->SetEmptyTaggedQueue(thread_, tagged_queue_handle);
    auto template_map_handle = TemplateMap::Create(thread_);
    global_env_handle->SetTemplateMap(thread_, template_map_handle);
    auto symbol_table_2_handle = SymbolTable::Create(GetJSThread());
    global_env_handle->SetRegisterSymbols(GetJSThread(), symbol_table_2_handle);
    SetupRegExpResultCache();
    micro_job_queue_ = factory_->NewMicroJobQueue().GetTaggedValue();

    {
        builtins::Builtins builtins;
        builtins.Initialize(global_env_handle, thread_);
    }

    thread_->SetGlobalObject(global_env_handle->GetGlobalObject());

    module_manager_ = new ModuleManager(this);

    InitializeFinish();
    notification_manager_->VmStartEvent();
    notification_manager_->VmInitializationEvent(thread_->GetThreadId());
    return true;
}

void EcmaVM::InitializeEcmaScriptRunStat()
{
    // NOLINTNEXTLINE(modernize-avoid-c-arrays)
    static const char *runtime_caller_names[] = {
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTERPRETER_CALLER_NAME(name) "InterPreter::" #name,
        INTERPRETER_CALLER_LIST(INTERPRETER_CALLER_NAME)  // NOLINTNEXTLINE(bugprone-suspicious-missing-comma)
#undef INTERPRETER_CALLER_NAME
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define BUILTINS_API_NAME(class, name) "BuiltinsApi::" #class "_" #name,
#define BUILTINS_MACRO_GEN_V BUILTINS_API_NAME
#include "plugins/ecmascript/runtime/builtins/generated/builtins_ids_gen.inl"
#undef BUILTINS_MACRO_GEN_V
        BUILTINS_API_LIST(BUILTINS_API_NAME)
#undef BUILTINS_API_NAME
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define ABSTRACT_OPERATION_NAME(class, name) "AbstractOperation::" #class "_" #name,
            ABSTRACT_OPERATION_LIST(ABSTRACT_OPERATION_NAME)
#undef ABSTRACT_OPERATION_NAME
    };
    static_assert(sizeof(runtime_caller_names) == sizeof(const char *) * ecmascript::RUNTIME_CALLER_NUMBER,
                  "Invalid runtime caller number");
    auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
    runtime_stat_ = allocator->New<EcmaRuntimeStat>(runtime_caller_names, ecmascript::RUNTIME_CALLER_NUMBER);
    if (UNLIKELY(runtime_stat_ == nullptr)) {
        LOG_ECMA(FATAL) << "alloc runtime_stat_ failed";
        UNREACHABLE();
    }
}

void EcmaVM::SetRuntimeStatEnable(bool flag)
{
    if (flag) {
        if (runtime_stat_ == nullptr) {
            InitializeEcmaScriptRunStat();
        }
    } else {
        if (runtime_stat_enabled_) {
            runtime_stat_->Print();
            runtime_stat_->ResetAllCount();
        }
    }
    runtime_stat_enabled_ = flag;
}

bool EcmaVM::InitializeFinish()
{
    vm_initialized_ = true;
    return true;
}

void EcmaVM::SaveProfileInfo()
{
    if (GetOptions().WasSetProfileOutput()) {
        if (profiles_methods_.empty()) {
            LOG(WARNING, RUNTIME) << "Profiled methods list is empty";
            return;
        }

        EcmaProfileContainer proto;

        for (auto method : profiles_methods_) {
            auto method_name = method->GetFullName();

            if (method->GetProfilingVector() != nullptr) {
                panda_file::MethodDataAccessor mda(*method->GetPandaFile(), method->GetFileId());
                auto profile_data = method->GetProfilingVector();
                auto profile_size = mda.GetProfileSize().value();
                // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                proto[method_name] = std::vector<uint8_t>(profile_data, profile_data + profile_size);
            }
        }

        if (!proto.empty()) {
            std::ofstream stm(GetOptions().GetProfileOutput());
            if (!stm.is_open()) {
                LOG(FATAL, RUNTIME) << "Failed to create profile output: " << GetOptions().GetProfileOutput();
            }
            std::vector<uint8_t> buffer;
            auto res = serializer::TypeToBuffer(proto, buffer);
            if (!res) {
                LOG(FATAL, RUNTIME) << "Failed to serialize: " << res.Error();
            }
            stm.write(reinterpret_cast<const char *>(buffer.data()), buffer.size());
            LOG(INFO, RUNTIME) << "Profile with size=" << buffer.size() << " was saved to "
                               << GetOptions().GetProfileOutput();
        }
    }
}

void EcmaVM::UninitializeThreads()
{
    thread_->NativeCodeEnd();
    thread_->UpdateStatus(ThreadStatus::FINISHED);
}

void EcmaVM::CleanUpTask(Method *method)
{
    static_cast<EcmaRuntimeInterface *>(GetCompilerRuntimeInterface())->CleanFunction(method);
}

EcmaVM::~EcmaVM()
{
    vm_initialized_ = false;
    ClearNativeMethodsData();

    if (panda::plugins::RuntimeTypeToLang(Runtime::GetCurrent()->GetRuntimeType()) !=
        panda_file::SourceLang::ECMASCRIPT) {
        // If ecmavm is not main, it will be created and deleted via speacial api, so we need cleanup classlinker.
        Runtime::GetCurrent()->GetClassLinker()->ResetExtension(panda_file::SourceLang::ECMASCRIPT);
    }

    if (runtime_stat_ != nullptr && runtime_stat_enabled_) {
        runtime_stat_->Print();
    }

    // clear c_address: c++ pointer delete
    ClearBufferData();

    delete reg_exp_parser_cache_;
    reg_exp_parser_cache_ = nullptr;

    if (string_table_ != nullptr) {
        delete string_table_;
        string_table_ = nullptr;
    }

    if (module_manager_ != nullptr) {
        delete module_manager_;
        module_manager_ = nullptr;
    }

    if (thread_ != nullptr) {
        thread_->DestroyInternalResources();
        delete thread_;
        thread_ = nullptr;
    }

    extractor_cache_.clear();
    framework_program_methods_.clear();

    mem::InternalAllocatorPtr allocator = mm_->GetHeapManager()->GetInternalAllocator();
    allocator->Delete(notification_manager_);
    allocator->Delete(factory_);
    allocator->Delete(runtime_stat_);
    allocator->Delete(rendezvous_);
    allocator->Delete(thread_manager_);
    allocator->Delete(runtime_iface_);
    allocator->Delete(compiler_);
    if (call_profiling_table_ != nullptr) {
        allocator->Delete(call_profiling_table_);
    }

    mm_->Finalize();
    mem::MemoryManager::Destroy(mm_);
}

bool EcmaVM::ExecuteFromPf(std::string_view filename, std::string_view entry_point,
                           const std::vector<std::string> &args, bool is_module)
{
    std::unique_ptr<const panda_file::File> pf;
    if (framework_panda_file_ == nullptr || !IsFrameworkPandaFile(filename)) {
        pf = panda_file::OpenPandaFileOrZip(filename, panda_file::File::READ_WRITE);
        if (pf == nullptr) {
            return false;
        }
        AddPandaFile(pf.get(), is_module);  // Store here prevent from being automatically cleared
        notification_manager_->LoadModuleEvent(pf->GetFilename());
    } else {
        pf.reset(framework_panda_file_);
    }

    return Execute(std::move(pf), entry_point, args, is_module);
}

bool EcmaVM::ExecuteFromBuffer(const void *buffer, size_t size, std::string_view entry_point,
                               const std::vector<std::string> &args)
{
    auto pf = panda_file::OpenPandaFileFromMemory(buffer, size);
    if (pf == nullptr) {
        return false;
    }
    AddPandaFile(pf.get(), false);  // Store here prevent from being automatically cleared
    notification_manager_->LoadModuleEvent(pf->GetFilename());

    return Execute(std::move(pf), entry_point, args);
}

void EcmaVM::DumpHeap(PandaOStringStream *o_str) const
{
    size_t obj_cnt = 0;
    *o_str << "Dumping heap" << std::endl;
    GetHeapManager()->GetObjectAllocator()->IterateOverObjects([&obj_cnt, &o_str](ObjectHeader *mem) {
        JSTaggedValue(mem).Dump(nullptr, *o_str);
        obj_cnt++;
    });
    *o_str << "\nTotal dumped " << obj_cnt << std::endl;
}

PandaString EcmaVM::GetClassesFootprint() const
{
    PandaUnorderedMap<JSType, size_t> classes_footprint;
    PandaStringStream result;
    GetHeapManager()->GetObjectAllocator()->IterateOverObjects([&classes_footprint](ObjectHeader *mem) {
        auto obj_type = JSTaggedValue(mem).GetTaggedObject()->GetClass()->GetObjectType();
        classes_footprint[obj_type] += mem->ObjectSize();
    });
    for (const auto &it : classes_footprint) {
        result << "class: " << JSHClass::DumpJSType(it.first) << ", footprint - " << it.second << std::endl;
    }
    return result.str();
}

tooling::ecmascript::PtJSExtractor *EcmaVM::GetDebugInfoExtractor(const panda_file::File *file)
{
    tooling::ecmascript::PtJSExtractor *res = nullptr;
    auto it = extractor_cache_.find(file);
    if (it == extractor_cache_.end()) {
        auto extractor = std::make_unique<tooling::ecmascript::PtJSExtractor>(file);
        res = extractor.get();
        extractor_cache_[file] = std::move(extractor);
    } else {
        res = it->second.get();
    }
    return res;
}

bool EcmaVM::Execute(std::unique_ptr<const panda_file::File> pf, std::string_view entry_point,
                     const std::vector<std::string> &args, bool is_module)
{
    if (pf == nullptr) {
        return false;
    }

    const panda_file::File *pf_ptr = pf.get();
    Runtime::GetCurrent()->GetClassLinker()->AddPandaFile(std::move(pf));
    // Get ClassName and MethodName
    size_t pos = entry_point.find_last_of("::");
    if (pos == std::string_view::npos) {
        LOG_ECMA(ERROR) << "EntryPoint:" << entry_point << " is illegal";
        return false;
    }
    PandaString method_name(entry_point.substr(pos + 1));

    // For Ark application startup
    if (!is_module) {
        thread_->ManagedCodeBegin();
    }
    InvokeEcmaEntrypoint(*pf_ptr, method_name, args);
    if (!is_module) {
        thread_->ManagedCodeEnd();
    }
    return true;
}

void EcmaVM::SweepVmRefs(const GCObjectVisitor &gc_object_visitor)
{
    GetEcmaStringTable()->Sweep(gc_object_visitor);
    if (HasEcmaCallProfileTable()) {
        GetEcmaCallProfileTable()->Sweep(gc_object_visitor);
    }
    auto it = finalization_registries_.begin();
    while (it != finalization_registries_.end()) {
        if (gc_object_visitor(*it) == ObjectStatus::DEAD_OBJECT) {
            auto to_remove = it++;
            finalization_registries_.erase(to_remove);
        } else {
            ++it;
        }
    }
}

JSHandle<GlobalEnv> EcmaVM::GetGlobalEnv() const
{
    return JSHandle<GlobalEnv>(reinterpret_cast<uintptr_t>(&global_env_));
}

JSHandle<job::MicroJobQueue> EcmaVM::GetMicroJobQueue() const
{
    return JSHandle<job::MicroJobQueue>(reinterpret_cast<uintptr_t>(&micro_job_queue_));
}

Method *EcmaVM::GetNativeMethodWrapper()
{
    if (native_method_wrapper_ != nullptr) {
        return native_method_wrapper_;
    }
    auto mutf8_name = reinterpret_cast<const uint8_t *>("LEcmascript/Intrinsics;");
    auto klass =
        Runtime::GetCurrent()->GetClassLinker()->GetExtension(panda_file::SourceLang::ECMASCRIPT)->GetClass(mutf8_name);
    if (klass == nullptr) {
        return nullptr;
    }

    mutf8_name = reinterpret_cast<const uint8_t *>("NativeMethodWrapper");

    Method::Proto proto;
    auto &shorty = proto.GetShorty();

    shorty.emplace_back(panda_file::Type::TypeId::VOID);
    shorty.emplace_back(panda_file::Type::TypeId::TAGGED);

    native_method_wrapper_ = klass->GetDirectMethod(mutf8_name, proto);
    return native_method_wrapper_;
}

JSMethod *EcmaVM::GetMethodForNativeFunction(const void *func)
{
    Method *n_wrapper = GetNativeMethodWrapper();
    ASSERT(n_wrapper != nullptr);
    auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
    auto method = allocator->New<JSMethod>(n_wrapper);
    method->SetNativePointer(const_cast<void *>(func));

    method->SetCompiledEntryPoint(reinterpret_cast<void *>(CompiledCodeToBuiltinBridge));
    native_methods_.push_back(method);
    return native_methods_.back();
}

void EcmaVM::RedirectMethod(const panda_file::File &pf)
{
    for (auto method : framework_program_methods_) {
        method->SetPandaFile(&pf);
    }
}

void EcmaVM::RegisterFinalizationRegistry(JSHandle<JSFinalizationRegistry> registry)
{
    finalization_registries_.push_back(*registry);
}

Expected<int, Runtime::Error> EcmaVM::InvokeEntrypointImpl(Method *entrypoint, const std::vector<std::string> &args)
{
    // For testcase startup
    const panda_file::File *file = entrypoint->GetPandaFile();
    AddPandaFile(file, false);
    ScopedManagedCodeThread managed_scope(thread_);
    return InvokeEcmaEntrypoint(*file, utf::Mutf8AsCString(entrypoint->GetName().data), args);
}

Expected<JSTaggedValue, Runtime::Error> EcmaVM::GetInvocableFunction(const panda_file::File &pf,
                                                                     const PandaString &method_name)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    JSHandle<Program> program;
    if (&pf != framework_panda_file_) {
        program = PandaFileTranslator::TranslatePandaFile(this, pf, method_name);
    } else {
        JSHandle<EcmaString> string = factory_->NewFromStdStringUnCheck(pf.GetFilename(), true);
        program = JSHandle<Program>(thread_, framework_program_);
        program->SetLocation(thread_, string);
        RedirectMethod(pf);
    }

    SetProgram(*program, &pf);
    if (program.IsEmpty()) {
        LOG_ECMA(ERROR) << "program is empty, invoke entrypoint failed";
        return Unexpected(Runtime::Error::PANDA_FILE_LOAD_ERROR);
    }

    return program->GetMainFunction();
}

Expected<int, Runtime::Error> EcmaVM::InvokeEcmaEntrypoint(const panda_file::File &pf, const PandaString &method_name,
                                                           const std::vector<std::string> &args)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    auto resolve_func = GetInvocableFunction(pf, method_name);

    if (!resolve_func.HasValue()) {
        return Unexpected(resolve_func.Error());
    }

    JSHandle<JSFunction> func(thread_, resolve_func.Value());
    JSHandle<JSTaggedValue> global = GlobalEnv::Cast(global_env_.GetTaggedObject())->GetJSGlobalObject();
    JSHandle<JSTaggedValue> new_target(thread_, JSTaggedValue::Undefined());
    auto info = NewRuntimeCallInfo(thread_, func, global, new_target, args.size());
    uint32_t i = 0;
    for (const std::string &str : args) {
        JSHandle<JSTaggedValue> strobj(factory_->NewFromStdString(str));
        info->SetCallArg(i++, strobj.GetTaggedValue());
    }

    JSRuntimeOptions options = this->GetJSOptions();
    InvokeJsFunction(info.Get());

    if (LIKELY(!thread_->HasPendingException())) {
        job::MicroJobQueue::ExecutePendingJob(thread_, GetMicroJobQueue());
    }

    return 0;
}

void EcmaVM::AddPandaFile(const panda_file::File *pf, bool is_module)
{
    ASSERT(pf != nullptr);
    os::memory::LockHolder lock(panda_file_with_program_lock_);
    panda_file_with_program_.push_back(std::make_tuple(nullptr, pf, is_module));
}

#ifndef NDEBUG
static bool IsNonMovable(ObjectHeader *obj, EcmaVM *vm)
{
    mem::GCType gc_type = vm->GetGC()->GetType();
    if (gc_type == mem::GCType::EPSILON_GC || gc_type == mem::GCType::STW_GC) {
        return true;
    }
    return PoolManager::GetMmapMemPool()->GetSpaceTypeForAddr(obj) == SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT;
}
#endif

void EcmaVM::SetProgram(Program *program, const panda_file::File *pf)
{
    ASSERT(IsNonMovable(program, this));
    os::memory::LockHolder lock(panda_file_with_program_lock_);
    auto it = std::find_if(panda_file_with_program_.begin(), panda_file_with_program_.end(),
                           [pf](auto entry) { return std::get<1>(entry) == pf; });
    ASSERT(it != panda_file_with_program_.end());
    std::get<0>(*it) = program;
}

bool EcmaVM::IsFrameworkPandaFile(std::string_view filename) const
{
    return filename.size() >= framework_abc_file_name_.size() &&
           filename.substr(filename.size() - framework_abc_file_name_.size()) == framework_abc_file_name_;
}

JSHandle<JSTaggedValue> EcmaVM::GetEcmaUncaughtException() const
{
    if (thread_->GetException().IsHole()) {
        return JSHandle<JSTaggedValue>();
    }
    JSHandle<JSTaggedValue> exception_handle(thread_, thread_->GetException());
    thread_->ClearException();  // clear for ohos app
    if (exception_handle->IsObjectWrapper()) {
        JSHandle<ObjectWrapper> wrapper_value = JSHandle<ObjectWrapper>::Cast(exception_handle);
        JSHandle<JSTaggedValue> throw_value(thread_, wrapper_value->GetValue());
        return throw_value;
    }

    return exception_handle;
}

void EcmaVM::EnableUserUncaughtErrorHandler()
{
    is_uncaught_exception_registered_ = true;
}

void EcmaVM::HandleUncaughtException()
{
    if (is_uncaught_exception_registered_) {
        return;
    }
    ScopedManagedCodeThread s(thread_);
    [[maybe_unused]] EcmaHandleScope handle_scope(thread_);
    JSHandle<JSTaggedValue> exception_handle(thread_, JSTaggedValue(thread_->GetException()));
    // if caught exceptionHandle type is JSError
    thread_->ClearException();
    if (exception_handle->IsJSError()) {
        PrintJSErrorInfo(exception_handle);
        return;
    }
    if (exception_handle->IsObjectWrapper()) {
        JSHandle<ObjectWrapper> wrapper_value = JSHandle<ObjectWrapper>::Cast(exception_handle);
        JSHandle<JSTaggedValue> throw_value(thread_, wrapper_value->GetValue());
        if (throw_value->IsJSError()) {
            PrintJSErrorInfo(throw_value);
        } else {
            JSHandle<EcmaString> result = JSTaggedValue::ToString(thread_, throw_value);
            PandaString string = ConvertToPandaString(*result);
            LOG(ERROR, RUNTIME) << string;
        }
    }
}

void EcmaVM::PrintJSErrorInfo(const JSHandle<JSTaggedValue> &exception_info)
{
    JSHandle<JSTaggedValue> name_key = thread_->GlobalConstants()->GetHandledNameString();
    JSHandle<EcmaString> name(JSObject::GetProperty(thread_, exception_info, name_key).GetValue());
    JSHandle<JSTaggedValue> msg_key = thread_->GlobalConstants()->GetHandledMessageString();
    JSHandle<EcmaString> msg(JSObject::GetProperty(thread_, exception_info, msg_key).GetValue());
    JSHandle<JSTaggedValue> stack_key = thread_->GlobalConstants()->GetHandledStackString();
    JSHandle<EcmaString> stack(JSObject::GetProperty(thread_, exception_info, stack_key).GetValue());

    PandaString name_buffer = ConvertToPandaString(*name);
    PandaString msg_buffer = ConvertToPandaString(*msg);
    PandaString stack_buffer = ConvertToPandaString(*stack);
    LOG(ERROR, RUNTIME) << name_buffer << ": " << msg_buffer << "\n" << stack_buffer;
}

void EcmaVM::ProcessReferences(const WeakRootVisitor &v0)
{
    if (reg_exp_parser_cache_ != nullptr) {
        reg_exp_parser_cache_->Clear();
    }

    // array buffer
    for (auto iter = array_buffer_data_list_.begin(); iter != array_buffer_data_list_.end();) {
        JSNativePointer *object = *iter;
        auto fwd = v0(reinterpret_cast<TaggedObject *>(object));
        if (fwd == nullptr) {
            object->Destroy();
            iter = array_buffer_data_list_.erase(iter);
        } else if (fwd != reinterpret_cast<TaggedObject *>(object)) {
            *iter = JSNativePointer::Cast(fwd);
            ++iter;
        } else {
            ++iter;
        }
    }
}

void EcmaVM::HandleGCRoutineInMutator()
{
    // Handle references only in JS thread
    ASSERT(Thread::GetCurrent() == thread_);
    ASSERT(GetMutatorLock()->HasLock());
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    for (JSFinalizationRegistry *registry : finalization_registries_) {
        JSHandle<JSFinalizationRegistry> handle(thread_, registry);
        JSFinalizationRegistry::CallCleanupCallback(thread_, handle);
    }
}

void EcmaVM::PushToArrayDataList(JSNativePointer *array)
{
    if (std::find(array_buffer_data_list_.begin(), array_buffer_data_list_.end(), array) !=
        array_buffer_data_list_.end()) {
        return;
    }
    array_buffer_data_list_.emplace_back(array);
}

void EcmaVM::RemoveArrayDataList(JSNativePointer *array)
{
    auto iter = std::find(array_buffer_data_list_.begin(), array_buffer_data_list_.end(), array);
    if (iter != array_buffer_data_list_.end()) {
        array_buffer_data_list_.erase(iter);
    }
}

bool EcmaVM::VerifyFilePath(const PandaString &file_path) const
{
    if (file_path.size() > PATH_MAX) {
        return false;
    }

    PandaVector<char> resolved_path(PATH_MAX);
    auto result = realpath(file_path.c_str(), resolved_path.data());
    if (result == nullptr) {
        return false;
    }
    std::ifstream file(resolved_path.data());
    if (!file.good()) {
        return false;
    }
    file.close();
    return true;
}

void EcmaVM::ClearBufferData()
{
    for (auto iter : array_buffer_data_list_) {
        iter->Destroy();
    }
    array_buffer_data_list_.clear();

    os::memory::LockHolder lock(panda_file_with_program_lock_);
    for (auto iter = panda_file_with_program_.begin(); iter != panda_file_with_program_.end();) {
        std::get<0>(*iter)->FreeMethodData();
        iter = panda_file_with_program_.erase(iter);
    }
    panda_file_with_program_.clear();
}

bool EcmaVM::ExecutePromisePendingJob() const
{
    if (LIKELY(!thread_->HasPendingException())) {
        job::MicroJobQueue::ExecutePendingJob(thread_, GetMicroJobQueue());
        return true;
    }
    return false;
}

void *EcmaVM::AllocAndRegisterNative(size_t size)
{
    ScopedNativeCodeThread s(ManagedThread::GetCurrent());
    GetGC()->RegisterNativeAllocation(size);
    void *mem = mm_->GetHeapManager()->GetInternalAllocator()->Alloc(size);
    ASSERT(mem != nullptr);
    return mem;
}

void EcmaVM::FreeAndRegisterNative(void *mem, size_t size)
{
    ASSERT(mem != nullptr);
    GetGC()->RegisterNativeFree(size);
    mm_->GetHeapManager()->GetInternalAllocator()->Free(mem);
}

void EcmaVM::CollectGarbage() const
{
    mm_->GetGC()->WaitForGCInManaged(GCTask(GCTaskCause::EXPLICIT_CAUSE));
}

void EcmaVM::Iterate(const RootVisitor &v)
{
    v(Root::ROOT_VM, ObjectSlot(ToUintPtr(&global_env_)));
    v(Root::ROOT_VM, ObjectSlot(ToUintPtr(&micro_job_queue_)));
    v(Root::ROOT_VM, ObjectSlot(ToUintPtr(&module_manager_->ecma_modules_)));
    v(Root::ROOT_VM, ObjectSlot(ToUintPtr(&regexp_cache_)));

    os::memory::LockHolder lock(panda_file_with_program_lock_);
    for (const auto &iter : panda_file_with_program_) {
        v(Root::ROOT_VM, ObjectSlot(ToUintPtr(&std::get<0>(iter))));
    }
}

void EcmaVM::SetGlobalEnv(GlobalEnv *global)
{
    ASSERT(global != nullptr);
    global_env_ = JSTaggedValue(global);
}

void EcmaVM::SetMicroJobQueue(job::MicroJobQueue *queue)
{
    ASSERT(queue != nullptr);
    micro_job_queue_ = JSTaggedValue(queue);
}

const panda_file::File *EcmaVM::GetLastLoadedPandaFile()
{
    os::memory::LockHolder lock(panda_file_with_program_lock_);
    auto current_file_tuple = panda_file_with_program_.back();
    return std::get<1>(current_file_tuple);
}

JSHandle<JSTaggedValue> EcmaVM::GetModuleByName(JSHandle<JSTaggedValue> module_name)
{
    const std::string &current_path_file = GetLastLoadedPandaFile()->GetFilename();
    PandaString relative_file = ConvertToPandaString(EcmaString::Cast(module_name->GetTaggedObject()));

    // generate full path
    PandaString abc_path = module_manager_->GenerateModuleFullPath(current_path_file, relative_file);

    // Uniform module name
    JSHandle<EcmaString> abcmodule_name = factory_->NewFromString(abc_path);

    JSHandle<JSTaggedValue> module = module_manager_->GetModule(thread_, JSHandle<JSTaggedValue>::Cast(abcmodule_name));
    if (module->IsUndefined()) {
        PandaString file = ConvertToPandaString(abcmodule_name.GetObject<EcmaString>());
        std::vector<std::string> argv;
        ExecuteModule(file, ENTRY_POINTER, argv);
        module = module_manager_->GetModule(thread_, JSHandle<JSTaggedValue>::Cast(abcmodule_name));
    }
    return module;
}

void EcmaVM::ExecuteModule(std::string_view module_file, std::string_view entry_point,
                           const std::vector<std::string> &args)
{
    module_manager_->SetCurrentExportModuleName(module_file);
    // Update Current Module
    EcmaVM::ExecuteFromPf(module_file, entry_point, args, true);
    // Restore Current Module
    module_manager_->RestoreCurrentExportModuleName();
}

void EcmaVM::ClearNativeMethodsData()
{
    auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
    for (auto iter : native_methods_) {
        allocator->Delete(iter);
    }
    native_methods_.clear();
}

void EcmaVM::SetupRegExpResultCache()
{
    regexp_cache_ = builtins::RegExpExecResultCache::CreateCacheTable(thread_);
}

void EcmaVM::HandleLdaStr(Frame *frame, BytecodeId string_id)
{
    auto cp = ConstantPool::Cast(JSFrame::GetJSEnv(frame)->GetConstantPool());
    auto value = cp->GetObjectFromCache(string_id.AsIndex());
    frame->GetAcc().Set(value.GetRawData());
}

coretypes::String *EcmaVM::ResolveString(ConstantPool *cp, panda_file::File::EntityId id)
{
    auto value = cp->GetObjectFromCache(id.GetOffset());
    return coretypes::String::Cast(value.GetHeapObject());
}

coretypes::String *EcmaVM::ResolveString(Frame *frame, panda_file::File::EntityId id)
{
    return EcmaVM::ResolveString(ConstantPool::Cast(JSFrame::GetJSEnv(frame)->GetConstantPool()), id);
}

std::unique_ptr<const panda_file::File> EcmaVM::OpenPandaFile(std::string_view location)
{
    panda_file::File::OpenMode open_mode = GetLanguageContext().GetBootPandaFilesOpenMode();
    auto pf = panda_file::OpenPandaFile(location, "", open_mode);
    // PandaFileTranslator can trigger GC.
    ScopedManagedCodeThread s(thread_);
    [[maybe_unused]] EcmaHandleScope handle_scope(GetJSThread());
    auto program = PandaFileTranslator::TranslatePandaFile(this, *pf, ENTRY_METHOD_POINTER.data());
    {
        os::memory::LockHolder lock(panda_file_with_program_lock_);
        panda_file_with_program_.emplace_back(program.GetObject<Program>(), pf.get(), false);
    }
    return pf;
}

coretypes::String *EcmaVM::GetNonMovableString([[maybe_unused]] const panda_file::File &pf,
                                               [[maybe_unused]] panda_file::File::EntityId id) const
{
    return nullptr;
}

void EcmaVM::HandleReturnFrame() {}

void EcmaVM::VisitVmRoots(const GCRootVisitor &visitor)
{
    PandaVM::VisitVmRoots(visitor);
    ObjectXRay root_manager(this);
    auto common_visitor = [&visitor](Root type, ObjectSlot slot) {
        mem::RootType root;
        switch (type) {
            case Root::ROOT_FRAME:
                root = mem::RootType::ROOT_FRAME;
                break;
            default:
                root = mem::RootType::ROOT_VM;
                break;
        }
        JSTaggedValue value(slot.GetTaggedType());
        if (value.IsHeapObject()) {
            visitor(mem::GCRoot(root, value.GetHeapObject()));
        }
    };

    auto single_visitor = [&common_visitor](Root type, ObjectSlot slot) { common_visitor(type, slot); };

    auto range_visitor = [&common_visitor](Root type, ObjectSlot start, ObjectSlot end) {
        for (ObjectSlot slot = start; slot < end; slot++) {
            common_visitor(type, slot);
        }
    };
    root_manager.VisitVMRoots(single_visitor, range_visitor);
    if (HasEcmaCallProfileTable()) {
        GetEcmaCallProfileTable()->VisitRoots(visitor);
    }
}

void EcmaVM::UpdateVmRefs()
{
    PandaVM::UpdateVmRefs();
    ObjectXRay root_manager(this);
    auto single_visitor = []([[maybe_unused]] Root type, ObjectSlot slot) {
        JSTaggedValue value(slot.GetTaggedType());
        if (value.IsHeapObject()) {
            ObjectHeader *object = value.GetHeapObject();
            if (object->IsForwarded()) {
                slot.Update(TaggedObject::Cast(panda::mem::GetForwardAddress(object)));
            }
        }
    };
    auto single_ptr_visitor = []([[maybe_unused]] Root type, ObjectHeader **object) {
        if ((*object)->IsForwarded()) {
            *object = TaggedObject::Cast(panda::mem::GetForwardAddress(*object));
        }
    };

    auto range_visitor = [&single_visitor](Root type, ObjectSlot start, ObjectSlot end) {
        for (ObjectSlot slot = start; slot < end; slot++) {
            single_visitor(type, slot);
        }
    };
    root_manager.VisitVMRoots(single_visitor, range_visitor);

    // Special handling of array_buffer_data_list_
    // Don't handle it in ObjectXRay because these refs shouldn't be marked
    // as gc roots
    // NOLINTNEXTLINE(modernize-loop-convert)
    for (size_t i = 0; i < array_buffer_data_list_.size(); ++i) {
        single_ptr_visitor(Root::ROOT_VM, reinterpret_cast<ObjectHeader **>(&array_buffer_data_list_[i]));
    }

    for (auto &entry : finalization_registries_) {
        single_ptr_visitor(Root::ROOT_VM, reinterpret_cast<ObjectHeader **>(&entry));
    }

    // Update call profiling table
    if (HasEcmaCallProfileTable()) {
        GetEcmaCallProfileTable()->UpdateMoved();
    }
}

}  // namespace panda::ecmascript
