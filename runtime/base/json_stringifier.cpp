/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/json_stringifier.h"
#include <algorithm>
#include <iomanip>
#include <sstream>
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string-inl.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"

namespace panda::ecmascript::base {
constexpr unsigned char CODE_SPACE = 0x20;
constexpr int GAP_MAX_LEN = 10;
constexpr int FOUR_HEX = 4;

bool JsonStringifier::IsFastValueToQuotedString(const char *value)
{
    if (strpbrk(value, "\"\\\b\f\n\r\t") != nullptr) {
        return false;
    }
    while (*value != '\0') {
        if (*value > 0 && *value < CODE_SPACE) {
            return false;
        }
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        value++;
    }
    return true;
}

PandaString JsonStringifier::ValueToQuotedString(const PandaString &str)
{
    PandaString product;
    const char *value = str.c_str();
    // fast mode
    bool is_fast = IsFastValueToQuotedString(value);
    if (is_fast) {
        product += "\"";
        product += str;
        product += "\"";
        return product;
    }
    // 1. Let product be code unit 0x0022 (QUOTATION MARK).
    product += "\"";
    // 2. For each code unit C in value
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    for (const char *c = value; *c != 0; ++c) {
        switch (*c) {
            /*
             * a. If C is 0x0022 (QUOTATION MARK) or 0x005C (REVERSE SOLIDUS), then
             * i. Let product be the concatenation of product and code unit 0x005C (REVERSE SOLIDUS).
             * ii. Let product be the concatenation of product and C.
             */
            case '\"':
                product += "\\\"";
                break;
            case '\\':
                product += "\\\\";
                break;
            /*
             * b. Else if C is 0x0008 (BACKSPACE), 0x000C (FORM FEED), 0x000A (LINE FEED), 0x000D (CARRIAGE RETURN),
             * or 0x000B (LINE TABULATION), then
             * i. Let product be the concatenation of product and code unit 0x005C (REVERSE SOLIDUS).
             * ii. Let abbrev be the String value corresponding to the value of C as follows:
             * BACKSPACE "b"
             * FORM FEED (FF) "f"
             * LINE FEED (LF) "n"
             * CARRIAGE RETURN (CR) "r"
             * LINE TABULATION "t"
             * iii. Let product be the concatenation of product and abbrev.
             */
            case '\b':
                product += "\\b";
                break;
            case '\f':
                product += "\\f";
                break;
            case '\n':
                product += "\\n";
                break;
            case '\r':
                product += "\\r";
                break;
            case '\t':
                product += "\\t";
                break;
            default:
                // c. Else if C has a code unit value less than 0x0020 (SPACE), then
                if (*c > 0 && *c < CODE_SPACE) {
                    /*
                     * i. Let product be the concatenation of product and code unit 0x005C (REVERSE SOLIDUS).
                     * ii. Let product be the concatenation of product and "u".
                     * iii. Let hex be the string result of converting the numeric code unit value of C to a String of
                     * four hexadecimal digits. Alphabetic hexadecimal digits are presented as lowercase Latin letters.
                     * iv. Let product be the concatenation of product and hex.
                     */
                    std::ostringstream oss;
                    oss << "\\u" << std::hex << std::setfill('0') << std::setw(FOUR_HEX) << static_cast<int>(*c);
                    product += oss.str();
                } else {
                    // Else,
                    // i. Let product be the concatenation of product and C.
                    product += *c;
                }
        }
    }
    // 3. Let product be the concatenation of product and code unit 0x0022 (QUOTATION MARK).
    product += "\"";
    // Return product.
    return product;
}

JSHandle<JSTaggedValue> JsonStringifier::Stringify(const JSHandle<JSTaggedValue> &value,
                                                   const JSHandle<JSTaggedValue> &replacer,
                                                   const JSHandle<JSTaggedValue> &gap)
{
    factory_ = thread_->GetEcmaVM()->GetFactory();
    handle_value_.Update(JSTaggedValue::Undefined());
    handle_key_.Update(JSTaggedValue::Undefined());
    // Let isArray be IsArray(replacer).
    bool is_array = replacer->IsArray(thread_);
    // ReturnIfAbrupt(isArray).
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
    // If isArray is true, then
    if (is_array) {
        uint32_t len;
        if (replacer->IsJSArray()) {
            // FastPath
            JSHandle<JSArray> arr(replacer);
            len = arr->GetArrayLength();
        } else {
            // Let len be ToLength(Get(replacer, "length")).
            JSHandle<JSTaggedValue> length_key = thread_->GlobalConstants()->GetHandledLengthString();
            JSHandle<JSTaggedValue> len_result = JSTaggedValue::GetProperty(thread_, replacer, length_key).GetValue();
            // ReturnIfAbrupt(len).
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
            JSTaggedNumber len_number = JSTaggedValue::ToLength(thread_, len_result);
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
            len = len_number.ToUint32();
        }
        if (len > 0) {
            JSMutableHandle<JSTaggedValue> prop_handle(thread_, JSTaggedValue::Undefined());
            // Repeat while k<len.
            for (uint32_t i = 0; i < len; i++) {
                // a. Let v be Get(replacer, ToString(k)).
                JSTaggedValue prop = FastRuntimeStub::FastGetPropertyByIndex(thread_, replacer.GetTaggedValue(), i);
                // b. ReturnIfAbrupt(v).
                RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
                /*
                 * c. Let item be undefined.
                 * d. If Type(v) is String, let item be v.
                 * e. Else if Type(v) is Number, let item be ToString(v).
                 * f. Else if Type(v) is Object, then
                 * i. If v has a [[StringData]] or [[NumberData]] internal slot, let item be ToString(v).
                 * ii. ReturnIfAbrupt(item).
                 * g. If item is not undefined and item is not currently an element of PropertyList, then
                 * i. Append item to the end of PropertyList.
                 * h. Let k be k+1.
                 */
                prop_handle.Update(prop);
                if (prop.IsNumber() || prop.IsString()) {
                    AddDeduplicateProp(prop_handle);
                    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
                } else if (prop.IsJSPrimitiveRef()) {
                    JSTaggedValue primitive = JSPrimitiveRef::Cast(prop.GetTaggedObject())->GetValue();
                    if (primitive.IsNumber() || primitive.IsString()) {
                        AddDeduplicateProp(prop_handle);
                        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
                    }
                }
            }
        }
    }

    // If Type(space) is Object, then
    if (gap->IsJSPrimitiveRef()) {
        JSTaggedValue primitive = JSPrimitiveRef::Cast(gap->GetTaggedObject())->GetValue();
        // a. If space has a [[NumberData]] internal slot, then
        if (primitive.IsNumber()) {
            // i. Let space be ToNumber(space).
            JSTaggedNumber num = JSTaggedValue::ToNumber(thread_, gap);
            // ii. ReturnIfAbrupt(space).
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
            CalculateNumberGap(num);
        } else if (primitive.IsString()) {
            // b. Else if space has a [[StringData]] internal slot, then
            // i. Let space be ToString(space).
            auto str = JSTaggedValue::ToString(thread_, gap);
            // ii. ReturnIfAbrupt(space).
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
            CalculateStringGap(JSHandle<EcmaString>(thread_, str.GetTaggedValue()));
        }
    } else if (gap->IsNumber()) {
        // If Type(space) is Number
        CalculateNumberGap(gap.GetTaggedValue());
    } else if (gap->IsString()) {
        // Else if Type(space) is String
        CalculateStringGap(JSHandle<EcmaString>::Cast(gap));
    }

    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> key(factory_->GetEmptyString());
    JSHandle<JSTaggedValue> proto = env->GetObjectFunction();
    JSHandle<JSObject> wrapper = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(proto), proto);
    bool success = JSObject::CreateDataProperty(thread_, wrapper, key, value);  // key empty string
    if (!success) {
        return JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Exception());
    }

    JSTaggedValue serialize_value = GetSerializeValue(JSHandle<JSTaggedValue>::Cast(wrapper), key, value, replacer);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
    handle_value_.Update(serialize_value);
    JSTaggedValue result = SerializeJSONProperty(handle_value_, replacer);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread_);
    if (!result.IsUndefined()) {
        return JSHandle<JSTaggedValue>(
            factory_->NewFromUtf8Literal(reinterpret_cast<const uint8_t *>(result_.c_str()), result_.size()));
    }
    return thread_->GlobalConstants()->GetHandledUndefined();
}

void JsonStringifier::AddDeduplicateProp(const JSHandle<JSTaggedValue> &property)
{
    uint32_t prop_len = prop_list_.size();
    for (uint32_t i = 0; i < prop_len; i++) {
        if (JSTaggedValue::SameValue(prop_list_[i], property)) {
            return;
        }
    }
    JSHandle<EcmaString> prim_string = JSTaggedValue::ToString(thread_, property);
    RETURN_IF_ABRUPT_COMPLETION(thread_);
    JSHandle<JSTaggedValue> add_val(thread_, *prim_string);
    prop_list_.emplace_back(add_val);
}

bool JsonStringifier::CalculateNumberGap(JSTaggedValue gap)
{
    double num_value = gap.GetNumber();
    int num = static_cast<int>(num_value);
    if (num > 0) {
        int gap_length = std::min(num, GAP_MAX_LEN);
        for (int i = 0; i < gap_length; i++) {
            gap_ += " ";
        }
        gap_.append("\0");
    }
    return true;
}

bool JsonStringifier::CalculateStringGap(const JSHandle<EcmaString> &prim_string)
{
    PandaString gap_string = ConvertToPandaString(*prim_string);
    int gap_len = gap_string.length();
    if (gap_len > 0) {
        int gap_length = std::min(gap_len, GAP_MAX_LEN);
        PandaString str = gap_string.substr(0, gap_length);
        gap_ += str;
        gap_.append("\0");
    }
    return true;
}

JSTaggedValue JsonStringifier::GetSerializeValue(const JSHandle<JSTaggedValue> &object,
                                                 const JSHandle<JSTaggedValue> &key,
                                                 const JSHandle<JSTaggedValue> &value,
                                                 const JSHandle<JSTaggedValue> &replacer)
{
    JSTaggedValue tag_value = value.GetTaggedValue();
    // If Type(value) is Object, then
    if (value->IsECMAObject()) {
        // a. Let toJSON be Get(value, "toJSON").
        JSHandle<JSTaggedValue> to_json = thread_->GlobalConstants()->GetHandledToJsonString();
        JSHandle<JSTaggedValue> to_json_fun(thread_, FastRuntimeStub::FastGetPropertyByValue(thread_, value, to_json));
        // b. ReturnIfAbrupt(toJSON).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);
        // Reread tagValue because GC may move it in FastRuntimeStub::FastGetPropertyByValue
        tag_value = value.GetTaggedValue();

        // c. If IsCallable(toJSON) is true
        if (UNLIKELY(to_json_fun->IsCallable())) {
            // Let value be Call(toJSON, value, «key»).
            auto info = NewRuntimeCallInfo(thread_, to_json_fun, value, JSTaggedValue::Undefined(), 1);
            info->SetCallArgs(key);
            tag_value = JSFunction::Call(info.Get());
            // ii. ReturnIfAbrupt(value).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);
        }
    }

    if (UNLIKELY(replacer->IsCallable())) {
        handle_value_.Update(tag_value);
        // a. Let value be Call(ReplacerFunction, holder, «key, value»).
        auto info = NewRuntimeCallInfo(thread_, replacer, object, JSTaggedValue::Undefined(), 2);
        info->SetCallArgs(key, handle_value_);
        tag_value = JSFunction::Call(info.Get());  // 2: two args
        // b. ReturnIfAbrupt(value).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);
    }
    return tag_value;
}

JSTaggedValue JsonStringifier::SerializeJSONProperty(const JSHandle<JSTaggedValue> &value,
                                                     const JSHandle<JSTaggedValue> &replacer)
{
    JSTaggedValue tag_value = value.GetTaggedValue();
    if (!tag_value.IsHeapObject()) {
        TaggedType type = tag_value.GetRawData();
        switch (type) {
            // If value is false, return "false".
            case JSTaggedValue::VALUE_FALSE:
                result_ += "false";
                return tag_value;
            // If value is true, return "true".
            case JSTaggedValue::VALUE_TRUE:
                result_ += "true";
                return tag_value;
            // If value is null, return "null".
            case JSTaggedValue::VALUE_NULL:
                result_ += "null";
                return tag_value;
            default:
                // If Type(value) is Number, then
                if (tag_value.IsNumber()) {
                    // a. If value is finite, return ToString(value).
                    if (std::isfinite(tag_value.GetNumber())) {
                        result_ += ConvertToPandaString(*base::NumberHelper::NumberToString(thread_, tag_value));
                    } else {
                        // b. Else, return "null".
                        result_ += "null";
                    }
                    // TODO(audovichenko): Remove this suppression when CSA gets recognize primitive TaggedValue
                    // (issue #I5QOJX)
                    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
                    return tag_value;
                }
        }
    } else {
        JSType js_type = tag_value.GetTaggedObject()->GetClass()->GetObjectType();
        JSHandle<JSTaggedValue> val_handle(thread_, tag_value);
        switch (js_type) {
            case JSType::JS_ARRAY: {
                SerializeJSArray(val_handle, replacer);
                RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);
                return val_handle.GetTaggedValue();
            }
            // If Type(value) is String, return QuoteJSONString(value).
            case JSType::STRING: {
                PandaString str = ConvertToPandaString(*JSHandle<EcmaString>(val_handle));
                str = ValueToQuotedString(str);
                result_ += str;
                return val_handle.GetTaggedValue();
            }
            case JSType::JS_PRIMITIVE_REF: {
                SerilaizePrimitiveRef(val_handle);
                return val_handle.GetTaggedValue();
            }
            case JSType::SYMBOL:
                return JSTaggedValue::Undefined();
            default: {
                if (!val_handle->IsCallable()) {
                    if (UNLIKELY(val_handle->IsJSProxy())) {
                        SerializeJSProxy(val_handle, replacer);
                        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);
                    } else {
                        SerializeJSONObject(val_handle, replacer);
                        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);
                    }
                    return val_handle.GetTaggedValue();
                }
            }
        }
    }
    return JSTaggedValue::Undefined();
}

void JsonStringifier::SerializeObjectKey(const JSHandle<JSTaggedValue> &key, bool has_content)
{
    PandaString step_begin;
    PandaString step_end;
    if (has_content) {
        result_ += ",";
    }
    if (!gap_.empty()) {
        step_begin += "\n";
        step_begin += indent_;
        step_end += " ";
    }
    PandaString str;
    if (key->IsString()) {
        str = ConvertToPandaString(EcmaString::Cast(key->GetTaggedObject()));
    } else if (key->IsInt()) {
        str = NumberHelper::IntToString(static_cast<int32_t>(key->GetInt()));
    } else {
        str = ConvertToPandaString(*JSTaggedValue::ToString(thread_, key));
    }
    result_ += step_begin;
    str = ValueToQuotedString(str);
    result_ += str;
    result_ += ":";
    result_ += step_end;
}

bool JsonStringifier::PushValue(const JSHandle<JSTaggedValue> &value)
{
    uint32_t this_len = stack_.size();

    for (uint32_t i = 0; i < this_len; i++) {
        bool equal = JSTaggedValue::SameValue(stack_[i].GetTaggedValue(), value.GetTaggedValue());
        if (equal) {
            return true;
        }
    }

    stack_.emplace_back(value);
    return false;
}

void JsonStringifier::PopValue()
{
    stack_.pop_back();
}

bool JsonStringifier::SerializeJSONObject(const JSHandle<JSTaggedValue> &value, const JSHandle<JSTaggedValue> &replacer)
{
    bool is_contain = PushValue(value);
    if (is_contain) {
        THROW_TYPE_ERROR_AND_RETURN(thread_, "stack contains value", true);
    }

    PandaString stepback = indent_;
    indent_ += gap_;

    result_ += "{";
    bool has_content = false;

    JSHandle<JSObject> obj(value);
    if (!replacer->IsArray(thread_)) {
        uint32_t num_of_keys = obj->GetNumberOfKeys();
        uint32_t num_of_elements = obj->GetNumberOfElements();
        if (num_of_elements > 0) {
            has_content = JsonStringifier::SerializeElements(obj, replacer, has_content);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
        }
        if (num_of_keys > 0) {
            has_content = JsonStringifier::SerializeKeys(obj, replacer, has_content);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
        }
    } else {
        uint32_t prop_len = prop_list_.size();
        for (uint32_t i = 0; i < prop_len; i++) {
            JSTaggedValue tag_val =
                FastRuntimeStub::FastGetPropertyByValue(thread_, JSHandle<JSTaggedValue>(obj), prop_list_[i]);
            handle_value_.Update(tag_val);
            JSTaggedValue serialize_value = GetSerializeValue(value, prop_list_[i], handle_value_, replacer);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
            if (UNLIKELY(serialize_value.IsUndefined() || serialize_value.IsSymbol() ||
                         (serialize_value.IsECMAObject() && serialize_value.IsCallable()))) {
                continue;
            }
            handle_value_.Update(serialize_value);
            SerializeObjectKey(prop_list_[i], has_content);
            JSTaggedValue res = SerializeJSONProperty(handle_value_, replacer);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
            if (!res.IsUndefined()) {
                has_content = true;
            }
        }
    }
    if (has_content && gap_.length() != 0) {
        result_ += "\n";
        result_ += stepback;
    }
    result_ += "}";
    PopValue();
    indent_ = stepback;
    return true;
}

bool JsonStringifier::SerializeJSProxy(const JSHandle<JSTaggedValue> &object, const JSHandle<JSTaggedValue> &replacer)
{
    bool is_contain = PushValue(object);
    if (is_contain) {
        THROW_TYPE_ERROR_AND_RETURN(thread_, "stack contains value", true);
    }

    PandaString stepback = indent_;
    PandaString step_begin;
    indent_ += gap_;

    if (!gap_.empty()) {
        step_begin += "\n";
        step_begin += indent_;
    }
    result_ += "[";
    JSHandle<JSProxy> proxy(object);
    JSHandle<JSTaggedValue> length_key = thread_->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> lengh_handle = JSProxy::GetProperty(thread_, proxy, length_key).GetValue();
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
    JSTaggedNumber len_number = JSTaggedValue::ToLength(thread_, lengh_handle);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
    uint32_t length = len_number.ToUint32();
    for (uint32_t i = 0; i < length; i++) {
        handle_key_.Update(JSTaggedValue(i));
        JSHandle<JSTaggedValue> val_handle = JSProxy::GetProperty(thread_, proxy, handle_key_).GetValue();
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
        if (i > 0) {
            result_ += ",";
        }
        result_ += step_begin;
        JSTaggedValue serialize_value = GetSerializeValue(object, handle_key_, val_handle, replacer);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
        handle_value_.Update(serialize_value);
        JSTaggedValue res = SerializeJSONProperty(handle_value_, replacer);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
        if (res.IsUndefined()) {
            result_ += "null";
        }
    }

    if (length > 0 && !gap_.empty()) {
        result_ += "\n";
        result_ += stepback;
    }
    result_ += "]";
    PopValue();
    indent_ = stepback;
    return true;
}

bool JsonStringifier::SerializeJSArray(const JSHandle<JSTaggedValue> &value, const JSHandle<JSTaggedValue> &replacer)
{
    // If state.[[Stack]] contains value, throw a TypeError exception because the structure is cyclical.
    bool is_contain = PushValue(value);
    if (is_contain) {
        THROW_TYPE_ERROR_AND_RETURN(thread_, "stack contains value", true);
    }

    PandaString stepback = indent_;
    PandaString step_begin;
    indent_ += gap_;

    if (!gap_.empty()) {
        step_begin += "\n";
        step_begin += indent_;
    }
    result_ += "[";
    JSHandle<JSArray> js_arr(value);
    uint32_t len = js_arr->GetArrayLength();
    if (len > 0) {
        for (uint32_t i = 0; i < len; i++) {
            JSTaggedValue tag_val = FastRuntimeStub::FastGetPropertyByIndex(thread_, value.GetTaggedValue(), i);
            if (UNLIKELY(tag_val.IsAccessor())) {
                tag_val = JSObject::CallGetter(thread_, AccessorData::Cast(tag_val.GetTaggedObject()), value);
            }
            handle_value_.Update(tag_val);
            handle_key_.Update(JSTaggedNumber(i).ToString(thread_).GetTaggedValue());

            if (i > 0) {
                result_ += ",";
            }
            result_ += step_begin;
            JSTaggedValue serialize_value = GetSerializeValue(value, handle_key_, handle_value_, replacer);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
            handle_value_.Update(serialize_value);
            JSTaggedValue res = SerializeJSONProperty(handle_value_, replacer);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
            if (res.IsUndefined()) {
                result_ += "null";
            }
        }

        if (!gap_.empty()) {
            result_ += "\n";
            result_ += stepback;
        }
    }

    result_ += "]";
    PopValue();
    indent_ = stepback;
    return true;
}

void JsonStringifier::SerilaizePrimitiveRef(const JSHandle<JSTaggedValue> &primitive_ref)
{
    JSTaggedValue primitive = JSPrimitiveRef::Cast(primitive_ref.GetTaggedValue().GetTaggedObject())->GetValue();
    if (primitive.IsString()) {
        auto pri_str = JSTaggedValue::ToString(thread_, primitive_ref);
        RETURN_IF_ABRUPT_COMPLETION(thread_);
        PandaString str = ConvertToPandaString(*pri_str);
        str = ValueToQuotedString(str);
        result_ += str;
    } else if (primitive.IsNumber()) {
        auto pri_num = JSTaggedValue::ToNumber(thread_, primitive_ref);
        RETURN_IF_ABRUPT_COMPLETION(thread_);
        if (std::isfinite(pri_num.GetNumber())) {
            result_ += ConvertToPandaString(*base::NumberHelper::NumberToString(thread_, pri_num));
        } else {
            result_ += "null";
        }
    } else if (primitive.IsBoolean()) {
        result_ += primitive.IsTrue() ? "true" : "false";
    }
}

bool JsonStringifier::SerializeElements(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &replacer,
                                        bool has_content)
{
    JSHandle<TaggedArray> elements_arr(thread_, obj->GetElements());
    if (!elements_arr->IsDictionaryMode()) {
        uint32_t elements_len = elements_arr->GetLength();
        for (uint32_t i = 0; i < elements_len; ++i) {
            if (!elements_arr->Get(i).IsHole()) {
                handle_key_.Update(JSTaggedValue(i));
                handle_value_.Update(elements_arr->Get(i));
                has_content = JsonStringifier::AppendJsonString(obj, replacer, has_content);
                RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
            }
        }
    } else {
        JSHandle<NumberDictionary> number_dic(elements_arr);
        PandaVector<JSHandle<JSTaggedValue>> sort_arr;
        int size = number_dic->Size();
        for (int hash_index = 0; hash_index < size; hash_index++) {
            JSTaggedValue key = number_dic->GetKey(hash_index);
            if (!key.IsUndefined() && !key.IsHole()) {
                PropertyAttributes attr = number_dic->GetAttributes(hash_index);
                if (attr.IsEnumerable()) {
                    auto number_key = JSTaggedValue(static_cast<uint32_t>(key.GetInt()));
                    sort_arr.emplace_back(JSHandle<JSTaggedValue>(thread_, number_key));
                }
            }
        }
        std::sort(sort_arr.begin(), sort_arr.end(), CompareNumber);
        for (const auto &entry : sort_arr) {
            JSTaggedValue entry_key = entry.GetTaggedValue();
            handle_key_.Update(entry_key);
            int index = number_dic->FindEntry(entry_key);
            JSTaggedValue value = number_dic->GetValue(index);
            handle_value_.Update(value);
            has_content = JsonStringifier::AppendJsonString(obj, replacer, has_content);
            RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
        }
    }
    return has_content;
}

bool JsonStringifier::SerializeKeys(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &replacer,
                                    bool has_content)
{
    JSHandle<TaggedArray> own_keys = JSTaggedValue::GetOwnPropertyKeys(thread_, JSHandle<JSTaggedValue>(obj));

    uint32_t length = own_keys->GetLength();
    for (uint32_t i = 0; i < length; i++) {
        JSTaggedValue key = own_keys->Get(thread_, i);
        if (!key.IsString()) {
            continue;
        }
        handle_key_.Update(key);
        TaggedArray *properties = TaggedArray::Cast(obj->GetProperties().GetHeapObject());
        PropertyAttributes attr;
        uint32_t index_or_entry = 0;
        JSTaggedValue value = FastRuntimeStub::FindOwnProperty(thread_, *obj, properties, key, &attr, &index_or_entry);
        if (!value.IsHole()) {
            if (!attr.IsEnumerable()) {
                continue;
            }

            if (UNLIKELY(value.IsAccessor())) {
                value = JSObject::CallGetter(thread_, AccessorData::Cast(value.GetTaggedObject()),
                                             JSHandle<JSTaggedValue>(obj));
            }
        } else {
            value = JSTaggedValue::Undefined();
        }
        handle_value_.Update(value);
        has_content = JsonStringifier::AppendJsonString(obj, replacer, has_content);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
    }
    return has_content;
}

bool JsonStringifier::AppendJsonString(const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &replacer,
                                       bool has_content)
{
    JSTaggedValue serialize_value =
        GetSerializeValue(JSHandle<JSTaggedValue>(obj), handle_key_, handle_value_, replacer);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
    if (UNLIKELY(serialize_value.IsUndefined() || serialize_value.IsSymbol() ||
                 (serialize_value.IsECMAObject() && serialize_value.IsCallable()))) {
        return has_content;
    }
    handle_value_.Update(serialize_value);
    SerializeObjectKey(handle_key_, has_content);
    JSTaggedValue res = SerializeJSONProperty(handle_value_, replacer);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread_, false);
    if (!res.IsUndefined()) {
        return true;
    }
    return has_content;
}
}  // namespace panda::ecmascript::base
