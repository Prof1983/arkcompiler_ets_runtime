/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/number_helper.h"
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <iomanip>
#include <sstream>
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::base {
enum class Sign { NONE, NEG, POS };

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define RETURN_IF_CONVERSION_END(p, end, result) \
    if ((p) == (end)) {                          \
        return (result);                         \
    }

constexpr char CHARS[] = "0123456789abcdefghijklmnopqrstuvwxyz";  // NOLINT (modernize-avoid-c-arrays)
constexpr uint64_t MAX_MANTISSA = 0x1ULL << 52U;
constexpr uint8_t JS_DTOA_BUF_SIZE = 128;

static inline uint8_t ToDigit(uint8_t c)
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    }
    if (c >= 'A' && c <= 'Z') {
        return c - 'A' + DECIMAL;
    }
    if (c >= 'a' && c <= 'z') {
        return c - 'a' + DECIMAL;
    }
    return '$';
}

bool NumberHelper::IsNonspace(uint16_t c)
{
    int i;
    int len = sizeof(SPACE_OR_LINE_TERMINAL) / sizeof(SPACE_OR_LINE_TERMINAL[0]);
    for (i = 0; i < len; i++) {
        if (c == SPACE_OR_LINE_TERMINAL[i]) {
            return false;
        }
        if (c < SPACE_OR_LINE_TERMINAL[i]) {
            return true;
        }
    }
    return true;
}

bool NumberHelper::GotoNonspace(uint8_t **ptr, const uint8_t *end)
{
    while (*ptr < end) {
        uint16_t c = **ptr;
        size_t size = 1;
        if (c > INT8_MAX) {
            size = 0;
            uint16_t utf8_bit = INT8_MAX + 1;  // equal 0b1000'0000
            while (utf8_bit > 0 && (c & utf8_bit) == utf8_bit) {
                ++size;
                utf8_bit >>= 1UL;
            }
            if (utf::ConvertRegionUtf8ToUtf16(*ptr, &c, end - *ptr, 1, 0) <= 0) {
                return true;
            }
        }
        if (IsNonspace(c)) {
            return true;
        }
        *ptr += size;  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    }
    return false;
}

static inline double SignedZero(Sign sign)
{
    return sign == Sign::NEG ? -0.0 : 0.0;
}

bool NumberHelper::IsEmptyString(const uint8_t *start, const uint8_t *end)
{
    auto p = const_cast<uint8_t *>(start);
    return !NumberHelper::GotoNonspace(&p, end);
}

JSTaggedValue NumberHelper::DoubleToString(JSThread *thread, double number, int radix)
{
    bool negative = false;
    if (number < 0.0) {
        negative = true;
        number = -number;
    }

    double number_integer = std::floor(number);
    double number_fraction = number - number_integer;

    auto value = bit_cast<uint64_t>(number);
    value += 1;
    double delta = HALF * (bit_cast<double>(value) - number);

    PandaString result;
    if (number_fraction != 0 && number_fraction >= delta) {
        result += ".";
        result += DecimalsToString(&number_integer, number_fraction, radix, delta);
    }

    result = IntegerToString(number_integer, radix) + result;

    if (negative) {
        result = "-" + result;
    }

    return builtins_common::GetTaggedString(thread, result.c_str());
}

JSTaggedValue NumberHelper::DoubleToExponential(JSThread *thread, double number, int digit)
{
    PandaStringStream ss;
    if (digit < 0) {
        ss << std::setiosflags(std::ios::scientific) << std::setprecision(base::MAX_PRECISION) << number;
    } else {
        ss << std::setiosflags(std::ios::scientific) << std::setprecision(digit) << number;
    }
    PandaString result = ss.str();
    size_t found = result.find_last_of('e');
    if (found != PandaString::npos && found < result.size() - 2 && result[found + 2] == '0') {
        result.erase(found + 2, 1);  // 2:offset of e
    }
    if (digit < 0) {
        size_t end = found;
        while (--found > 0) {
            if (result[found] != '0') {
                break;
            }
        }
        if (result[found] == '.') {
            found--;
        }
        if (found < end - 1) {
            result.erase(found + 1, end - found - 1);
        }
    }
    return builtins_common::GetTaggedString(thread, result.c_str());
}

JSTaggedValue NumberHelper::DoubleToFixed(JSThread *thread, double number, int digit)
{
    PandaStringStream ss;
    ss << std::setiosflags(std::ios::fixed) << std::setprecision(digit) << number;
    return builtins_common::GetTaggedString(thread, ss.str().c_str());
}

JSTaggedValue NumberHelper::DoubleToPrecision(JSThread *thread, double number, int digit)
{
    if (number == 0.0) {
        return DoubleToFixed(thread, number, digit - 1);
    }
    PandaStringStream ss;
    double positive_number = number > 0 ? number : -number;
    int log_digit = std::floor(log10(positive_number));
    int radix_digit = digit - log_digit - 1;
    const int max_exponent_digit = 6;
    if ((log_digit >= 0 && radix_digit >= 0) || (log_digit < 0 && radix_digit <= max_exponent_digit)) {
        return DoubleToFixed(thread, number, std::abs(radix_digit));
    }
    return DoubleToExponential(thread, number, digit - 1);
}

JSTaggedValue NumberHelper::StringToDoubleWithRadix(const uint8_t *start, const uint8_t *end, int radix)
{
    auto p = const_cast<uint8_t *>(start);
    JSTaggedValue nan_result = builtins_common::GetTaggedDouble(NAN_VALUE);
    // 1. skip space and line terminal
    if (!NumberHelper::GotoNonspace(&p, end)) {
        return nan_result;
    }

    // 2. sign bit
    bool negative = false;
    if (*p == '-') {
        negative = true;
        RETURN_IF_CONVERSION_END(++p, end, nan_result);
    } else if (*p == '+') {
        RETURN_IF_CONVERSION_END(++p, end, nan_result);
    }
    // 3. 0x or 0X
    bool strip_prefix = true;
    // 4. If R  0, then
    //     a. If R < 2 or R > 36, return NaN.
    //     b. If R  16, let strip_prefix be false.
    if (radix != 0) {
        if (radix < MIN_RADIX || radix > MAX_RADIX) {
            return nan_result;
        }
        if (radix != HEXADECIMAL) {
            strip_prefix = false;
        }
    } else {
        radix = DECIMAL;
    }
    int size = 0;
    if (strip_prefix) {
        if (*p == '0') {
            size++;
            if (++p != end && (*p == 'x' || *p == 'X')) {
                RETURN_IF_CONVERSION_END(++p, end, nan_result);
                radix = HEXADECIMAL;
            }
        }
    }

    double result = 0;
    bool is_done = false;
    do {
        double part = 0;
        uint32_t multiplier = 1;
        for (; p != end; ++p) {
            // The maximum value to ensure that uint32_t will not overflow
            const uint32_t max_multiper = 0xffffffffU / 36;
            uint32_t m = multiplier * radix;
            if (m > max_multiper) {
                break;
            }

            int current_bit = ToDigit(*p);
            if (current_bit >= radix) {
                is_done = true;
                break;
            }
            size++;
            part = part * radix + current_bit;
            multiplier = m;
        }
        result = result * multiplier + part;
        if (is_done) {
            break;
        }
    } while (p != end);

    if (size == 0) {
        return nan_result;
    }

    if (negative) {
        result = -result;
    }
    return builtins_common::GetTaggedDouble(result);
}

char NumberHelper::Carry(char current, int radix)
{
    int digit = (current > '9') ? (current - 'a' + DECIMAL) : (current - '0');
    digit = (digit == (radix - 1)) ? 0 : digit + 1;
    return CHARS[digit];
}

PandaString NumberHelper::IntegerToString(double number, int radix)
{
    ASSERT(radix >= MIN_RADIX && radix <= MAX_RADIX);
    PandaString result;
    while (number / radix > MAX_MANTISSA) {
        number /= radix;
        result = PandaString("0").append(result);
    }
    do {
        double remainder = std::fmod(number, radix);
        result = CHARS[static_cast<int>(remainder)] + result;
        number = (number - remainder) / radix;
    } while (number > 0);
    return result;
}

PandaString NumberHelper::DecimalsToString(double *number_integer, double fraction, int radix, double delta)
{
    PandaString result;
    while (fraction >= delta) {
        fraction *= radix;
        delta *= radix;
        int64_t integer = std::floor(fraction);
        fraction -= integer;
        result += CHARS[integer];
        if (fraction > HALF && fraction + delta > 1) {
            size_t fraction_end = result.size() - 1;
            result[fraction_end] = Carry(*result.rbegin(), radix);
            for (; fraction_end > 0; fraction_end--) {
                if (result[fraction_end] == '0') {
                    result[fraction_end - 1] = Carry(result[fraction_end - 1], radix);
                } else {
                    break;
                }
            }
            if (fraction_end == 0) {
                (*number_integer)++;
            }
            break;
        }
    }
    // delete 0 in the end
    size_t found = result.find_last_not_of('0');
    if (found != PandaString::npos) {
        result.erase(found + 1);
    }

    return result;
}

PandaString NumberHelper::IntToString(int number)
{
    return ToPandaString(number);
}

JSTaggedValue NumberHelper::StringToBigInt(JSThread *thread, JSHandle<JSTaggedValue> str_val)
{
    Span<const uint8_t> str;
    auto str_obj = static_cast<EcmaString *>(str_val->GetTaggedObject());
    size_t str_len = str_obj->GetLength();
    if (str_len == 0) {
        return BigInt::Int32ToBigInt(thread, 0).GetTaggedValue();
    }
    if (UNLIKELY(str_obj->IsUtf16())) {
        PandaVector<uint8_t> buf;
        size_t len = utf::Utf16ToUtf8Size(str_obj->GetDataUtf16(), str_len) - 1;
        buf.reserve(len);
        len = utf::ConvertRegionUtf16ToUtf8(str_obj->GetDataUtf16(), buf.data(), str_len, len, 0);
        str = Span<const uint8_t>(buf.data(), len);
    } else {
        str = Span<const uint8_t>(str_obj->GetDataUtf8(), str_len);
    }
    auto p = const_cast<uint8_t *>(str.begin());
    auto end = str.end();
    // 1. skip space and line terminal
    if (!NumberHelper::GotoNonspace(&p, end)) {
        return BigInt::Int32ToBigInt(thread, 0).GetTaggedValue();
    }
    // 2. get bigint sign
    Sign sign = Sign::NONE;
    if (*p == '+') {
        RETURN_IF_CONVERSION_END(++p, end, JSTaggedValue(NAN_VALUE));
        sign = Sign::POS;
    } else if (*p == '-') {
        RETURN_IF_CONVERSION_END(++p, end, JSTaggedValue(NAN_VALUE));
        sign = Sign::NEG;
    }
    // 3. bigint not allow Infinity, decimal points, or exponents.
    if (isalpha(*p) != 0) {
        return JSTaggedValue(NAN_VALUE);
    }
    // 4. get bigint radix
    uint8_t radix = DECIMAL;
    if (*p == '0') {
        if (++p == end) {
            return BigInt::Int32ToBigInt(thread, 0).GetTaggedValue();
        }
        if (*p == 'x' || *p == 'X') {
            RETURN_IF_CONVERSION_END(++p, end, JSTaggedValue(NAN_VALUE));
            if (sign != Sign::NONE) {
                return JSTaggedValue(NAN_VALUE);
            }
            radix = HEXADECIMAL;
        } else if (*p == 'o' || *p == 'O') {
            RETURN_IF_CONVERSION_END(++p, end, JSTaggedValue(NAN_VALUE));
            if (sign != Sign::NONE) {
                return JSTaggedValue(NAN_VALUE);
            }
            radix = OCTAL;
        } else if (*p == 'b' || *p == 'B') {
            RETURN_IF_CONVERSION_END(++p, end, JSTaggedValue(NAN_VALUE));
            if (sign != Sign::NONE) {
                return JSTaggedValue(NAN_VALUE);
            }
            radix = BINARY;
        }
    }

    auto p_start = p;
    // 5. skip leading '0'
    while (*p == '0') {
        if (++p == end) {
            return BigInt::Int32ToBigInt(thread, 0).GetTaggedValue();
        }
    }
    // 6. parse to bigint
    std::string buffer;
    if (sign == Sign::NEG) {
        buffer += "-";
    }
    do {
        uint8_t c = ToDigit(*p);
        if (c >= radix) {
            if (p_start != p && !NumberHelper::GotoNonspace(&p, end)) {
                break;
            }
            return JSTaggedValue(NAN_VALUE);
        }
        buffer += *p;
    } while (++p != end);
    return BigIntHelper::SetBigInt(thread, buffer, radix).GetTaggedValue();
}

JSTaggedValue NumberHelper::Pow(double base, double exponent)
{
    // The result of base ** exponent when base is 1 or -1 and exponent is +Infinity
    // or -Infinity, or when base is 1 and exponent is NaN, differs from IEEE 754-2019
    // https://262.ecma-international.org/12.0/#sec-numeric-types-number-exponentiate
    if (std::abs(base) == 1 && !std::isfinite(exponent)) {
        return JSTaggedValue(base::NAN_VALUE);
    }

    return JSTaggedValue(std::pow(base, exponent));
}

// NOLINTBEGIN(cppcoreguidelines-pro-bounds-pointer-arithmetic)
static inline void GetBase(double d, uint8_t digits, int &decpt, std::array<char, JS_DTOA_BUF_SIZE> &buf,
                           std::array<char, JS_DTOA_BUF_SIZE> &buf_tmp, int size)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    [[maybe_unused]] int result1 = snprintf_s(buf_tmp.data(), size, size - 1, "%+.*e", digits - 1, d);
    ASSERT(result1 != -1);
    // mantissa
    buf[0] = buf_tmp[1];
    if (digits > 1) {
        [[maybe_unused]] int result2 =
            memcpy_s(buf.data() + 1, digits, buf_tmp.data() + 2, digits);  // 2 means add the point char to buf
        ASSERT(result2 == EOK);
    }
    buf[digits + 1] = '\0';
    // exponent
    constexpr uint8_t BASE = 10;
    decpt = strtol(buf_tmp.data() + digits + 2 + static_cast<uint8_t>(digits > 1), nullptr, BASE) +
            1;  // 2 means ignore the integer and point
}

static inline int GetMinmumDigits(double d, int &decpt, std::array<char, JS_DTOA_BUF_SIZE> &buf)
{
    uint8_t digits = 0;
    std::array<char, JS_DTOA_BUF_SIZE> buf_tmp = {0};

    // find the minimum amount of digits
    uint8_t min_digits = 1;
    uint8_t max_digits = DOUBLE_MAX_PRECISION;
    while (min_digits < max_digits) {
        digits = (min_digits + max_digits) / 2;
        GetBase(d, digits, decpt, buf, buf_tmp, buf_tmp.size());
        if (std::strtod(buf_tmp.data(), nullptr) == d) {
            // no need to keep the trailing zeros
            while (digits >= 2 && buf[digits] == '0') {  // 2 means ignore the integer and point
                digits--;
            }
            max_digits = digits;
        } else {
            min_digits = digits + 1;
        }
    }
    digits = max_digits;
    GetBase(d, digits, decpt, buf, buf_tmp, buf_tmp.size());

    return digits;
}
// NOLINTEND(cppcoreguidelines-pro-bounds-pointer-arithmetic)

// 7.1.12.1 ToString Applied to the Number Type
JSHandle<EcmaString> NumberHelper::NumberToString(const JSThread *thread, JSTaggedValue number)
{
    ASSERT(number.IsNumber());
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (number.IsInt()) {
        return factory->NewFromCanBeCompressString(IntToString(number.GetInt()));
    }

    double d = number.GetDouble();
    if (std::isnan(d)) {
        return factory->NewFromCanBeCompressString("NaN");
    }
    if (d == 0.0) {
        return factory->NewFromCanBeCompressString("0");
    }
    if (d >= INT32_MIN + 1 && d <= INT32_MAX && d == static_cast<double>(static_cast<int32_t>(d))) {
        return factory->NewFromCanBeCompressString(IntToString(static_cast<int32_t>(d)));
    }

    std::string result;
    if (d < 0) {
        result += "-";
        d = -d;
    }

    if (std::isinf(d)) {
        result += "Infinity";
        return factory->NewFromStdStringUnCheck(result, true);
    }

    ASSERT(d > 0);

    // 5. Otherwise, let n, k, and s be integers such that k ≥ 1, 10k−1 ≤ s < 10k, the Number value for s × 10n−k is m,
    // and k is as small as possible. If there are multiple possibilities for s, choose the value of s for which s ×
    // 10n−k is closest in value to m. If there are two such possible values of s, choose the one that is even. Note
    // that k is the number of digits in the decimal representation of s and that s is not divisible by 10.
    std::array<char, JS_DTOA_BUF_SIZE> buffer = {0};
    int n = 0;
    int k = GetMinmumDigits(d, n, buffer);
    PandaString base(buffer.data());

    if (n > 0 && n <= 21) {  // NOLINT(readability-magic-numbers)
        base.erase(1, 1);
        if (k <= n) {
            // 6. If k ≤ n ≤ 21, return the String consisting of the code units of the k digits of the decimal
            // representation of s (in order, with no leading zeroes), followed by n−k occurrences of the code unit
            // 0x0030 (DIGIT ZERO).
            base += PandaString(n - k, '0');
        } else {
            // 7. If 0 < n ≤ 21, return the String consisting of the code units of the most significant n digits of the
            // decimal representation of s, followed by the code unit 0x002E (FULL STOP), followed by the code units of
            // the remaining k−n digits of the decimal representation of s.
            base.insert(n, 1, '.');
        }
    } else if (-6 < n && n <= 0) {  // NOLINT(readability-magic-numbers)
        // 8. If −6 < n ≤ 0, return the String consisting of the code unit 0x0030 (DIGIT ZERO), followed by the code
        // unit 0x002E (FULL STOP), followed by −n occurrences of the code unit 0x0030 (DIGIT ZERO), followed by the
        // code units of the k digits of the decimal representation of s.
        base.erase(1, 1);
        base = PandaString("0.") + PandaString(-n, '0') + base;
    } else {
        if (k == 1) {
            // 9. Otherwise, if k = 1, return the String consisting of the code unit of the single digit of s
            base.erase(1, 1);
        }
        // followed by code unit 0x0065 (LATIN SMALL LETTER E), followed by the code unit 0x002B (PLUS SIGN) or the code
        // unit 0x002D (HYPHEN-MINUS) according to whether n−1 is positive or negative, followed by the code units of
        // the decimal representation of the integer abs(n−1) (with no leading zeroes).
        base += "e" + (n >= 1 ? std::string("+") : "") + std::to_string(n - 1);
    }
    result += base;
    return factory->NewFromStdStringUnCheck(result, true);
}

double NumberHelper::TruncateDouble(double d)
{
    if (std::isnan(d)) {
        return 0;
    }
    if (!std::isfinite(d)) {
        return d;
    }
    // -0 to +0
    if (d == 0.0) {
        return 0;
    }
    return (d >= 0) ? std::floor(d) : std::ceil(d);
}

double NumberHelper::StringToDouble(const uint8_t *start, const uint8_t *end, uint8_t radix, uint32_t flags)
{
    auto p = const_cast<uint8_t *>(start);
    // 1. skip space and line terminal
    if (!NumberHelper::GotoNonspace(&p, end)) {
        return 0.0;
    }

    // 2. get number sign
    Sign sign = Sign::NONE;
    if (*p == '+') {
        RETURN_IF_CONVERSION_END(++p, end, NAN_VALUE);
        sign = Sign::POS;
    } else if (*p == '-') {
        RETURN_IF_CONVERSION_END(++p, end, NAN_VALUE);
        sign = Sign::NEG;
    }
    bool ignore_trailing = (flags & IGNORE_TRAILING) != 0;

    // 3. judge Infinity
    static const char INF[] = "Infinity";  // NOLINT(modernize-avoid-c-arrays)
    if (*p == INF[0]) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        for (const char *i = &INF[1]; *i != '\0'; ++i) {
            if (++p == end || *p != *i) {
                return NAN_VALUE;
            }
        }
        ++p;
        if (!ignore_trailing && NumberHelper::GotoNonspace(&p, end)) {
            return NAN_VALUE;
        }
        return sign == Sign::NEG ? -POSITIVE_INFINITY : POSITIVE_INFINITY;
    }

    // 4. get number radix
    bool leading_zero = false;
    bool prefix_radix = false;
    if (*p == '0' && radix == 0) {
        RETURN_IF_CONVERSION_END(++p, end, SignedZero(sign));
        if (*p == 'x' || *p == 'X') {
            if ((flags & ALLOW_HEX) == 0) {
                return ignore_trailing ? SignedZero(sign) : NAN_VALUE;
            }
            RETURN_IF_CONVERSION_END(++p, end, NAN_VALUE);
            if (sign != Sign::NONE) {
                return NAN_VALUE;
            }
            prefix_radix = true;
            radix = HEXADECIMAL;
        } else if (*p == 'o' || *p == 'O') {
            if ((flags & ALLOW_OCTAL) == 0) {
                return ignore_trailing ? SignedZero(sign) : NAN_VALUE;
            }
            RETURN_IF_CONVERSION_END(++p, end, NAN_VALUE);
            if (sign != Sign::NONE) {
                return NAN_VALUE;
            }
            prefix_radix = true;
            radix = OCTAL;
        } else if (*p == 'b' || *p == 'B') {
            if ((flags & ALLOW_BINARY) == 0) {
                return ignore_trailing ? SignedZero(sign) : NAN_VALUE;
            }
            RETURN_IF_CONVERSION_END(++p, end, NAN_VALUE);
            if (sign != Sign::NONE) {
                return NAN_VALUE;
            }
            prefix_radix = true;
            radix = BINARY;
        } else {
            leading_zero = true;
        }
    }

    if (radix == 0) {
        radix = DECIMAL;
    }
    auto p_start = p;
    // 5. skip leading '0'
    while (*p == '0') {
        RETURN_IF_CONVERSION_END(++p, end, SignedZero(sign));
        leading_zero = true;
    }
    // 6. parse to number
    uint64_t int_number = 0;
    uint64_t number_max = (UINT64_MAX - (radix - 1)) / radix;
    int digits = 0;
    int exponent = 0;
    do {
        uint8_t c = ToDigit(*p);
        if (c >= radix) {
            if (!prefix_radix || ignore_trailing || (p_start != p && !NumberHelper::GotoNonspace(&p, end))) {
                break;
            }
            // "0b" "0x1.2" "0b1e2" ...
            return NAN_VALUE;
        }
        ++digits;
        if (int_number < number_max) {
            int_number = int_number * radix + c;
        } else {
            ++exponent;
        }
    } while (++p != end);

    auto number = static_cast<double>(int_number);
    if (sign == Sign::NEG) {
        if (number == 0) {
            number = -0.0;
        } else {
            number = -number;
        }
    }

    // 7. deal with other radix except DECIMAL
    if (p == end || radix != DECIMAL) {
        if ((digits == 0 && !leading_zero) || (p != end && !ignore_trailing && NumberHelper::GotoNonspace(&p, end))) {
            // no digits there, like "0x", "0xh", or error trailing of "0x3q"
            return NAN_VALUE;
        }
        return number * std::pow(radix, exponent);
    }

    // 8. parse '.'
    if (radix == DECIMAL && *p == '.') {
        RETURN_IF_CONVERSION_END(++p, end, (digits > 0) ? (number * std::pow(radix, exponent)) : NAN_VALUE);
        while (ToDigit(*p) < radix) {
            --exponent;
            ++digits;
            if (++p == end) {
                break;
            }
        }
    }
    if (digits == 0 && !leading_zero) {
        // no digits there, like ".", "sss", or ".e1"
        return NAN_VALUE;
    }
    auto p_end = p;

    // 9. parse 'e/E' with '+/-'
    char exponent_sign = '+';
    int additional_exponent = 0;
    constexpr int MAX_EXPONENT = INT32_MAX / 2;
    if (radix == DECIMAL && (p != end && (*p == 'e' || *p == 'E'))) {
        RETURN_IF_CONVERSION_END(++p, end, NAN_VALUE);

        // 10. parse exponent number
        if (*p == '+' || *p == '-') {
            exponent_sign = static_cast<char>(*p);
            RETURN_IF_CONVERSION_END(++p, end, NAN_VALUE);
        }
        uint8_t digit;
        while ((digit = ToDigit(*p)) < radix) {
            if (additional_exponent > MAX_EXPONENT / radix) {
                additional_exponent = MAX_EXPONENT;
            } else {
                additional_exponent = additional_exponent * radix + digit;
            }
            if (++p == end) {
                break;
            }
        }
    }
    exponent += (exponent_sign == '-' ? -additional_exponent : additional_exponent);
    if (!ignore_trailing && NumberHelper::GotoNonspace(&p, end)) {
        return NAN_VALUE;
    }

    // 10. build StringNumericLiteral string
    PandaString buffer;
    if (sign == Sign::NEG) {
        buffer += "-";
    }
    for (uint8_t *i = p_start; i < p_end; ++i) {  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (*i != static_cast<uint8_t>('.')) {
            buffer += *i;
        }
    }

    // 11. convert none-prefix radix string
    return Strtod(buffer.c_str(), exponent, radix);
}

static double PowHelper(uint64_t number, int16_t exponent, uint8_t radix)
{
    const double log_2_radix {std::log2(radix)};

    double exp_rem = log_2_radix * exponent;
    int exp_i = (int)exp_rem;
    exp_rem = exp_rem - exp_i;

    // NOLINTNEXTLINE(readability-magic-numbers)
    JSDoubleOperator u((double)number * std::pow(2.0, exp_rem));

    exp_i = u.GetExponent() + exp_i;
    if (((exp_i & ~base::DOUBLE_EXPONENT_MASK) != 0) || exp_i == 0) {  // NOLINT(hicpp-signed-bitwise)
        if (exp_i > 0) {
            return std::numeric_limits<double>::infinity();
        }
        if (exp_i < -static_cast<int>(base::DOUBLE_SIGNIFICAND_SIZE)) {
            return -std::numeric_limits<double>::infinity();
        }
        u.SetExponent(0);
        // NOLINTNEXTLINE(hicpp-signed-bitwise)
        u.SetSignificand((u.GetSignificand() | base::DOUBLE_HIDDEN_BIT) >> (1 - exp_i));
    } else {
        u.SetExponent(exp_i);
    }

    return u.GetDouble();
}

double NumberHelper::Strtod(const char *str, int exponent, uint8_t radix)
{
    ASSERT(str != nullptr);
    ASSERT(radix >= base::MIN_RADIX && radix <= base::MAX_RADIX);
    auto p = const_cast<char *>(str);
    Sign sign = Sign::NONE;
    uint64_t number = 0;
    uint64_t number_max = (UINT64_MAX - (radix - 1)) / radix;
    double result = 0.0;
    if (*p == '-') {
        sign = Sign::NEG;
        ++p;
    }
    while (*p == '0') {
        ++p;
    }
    while (*p != '\0') {
        uint8_t digit = ToDigit(static_cast<uint8_t>(*p));
        if (digit >= radix) {
            break;
        }
        if (number < number_max) {
            number = number * radix + digit;
        } else {
            ++exponent;
        }
        ++p;
    }
    if (exponent < 0) {
        result = number / std::pow(radix, -exponent);
    } else {
        result = number * std::pow(radix, exponent);
    }
    if (!std::isfinite(result)) {
        result = PowHelper(number, exponent, radix);
    }
    return sign == Sign::NEG ? -result : result;
}

int32_t NumberHelper::DoubleToInt(double d, size_t bits)
{
    return coretypes::JsCastDoubleToInt(d, bits);
}

int32_t NumberHelper::DoubleInRangeInt32(double d)
{
    if (d > INT_MAX) {
        return INT_MAX;
    }
    if (d < INT_MIN) {
        return INT_MIN;
    }
    return base::NumberHelper::DoubleToInt(d, base::INT32_BITS);
}
}  // namespace panda::ecmascript::base
