/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_BASE_TYPED_ARRAY_HELPER_H
#define ECMASCRIPT_BASE_TYPED_ARRAY_HELPER_H

#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/js_dataview.h"
#include <limits>
#include <string>

namespace panda::ecmascript::base {
enum ElementSize : uint8_t { ONE = 1, TWO = 2, FOUR = 4, EIGHT = 8 };

class TypedArrayHelper {
public:
    static JSTaggedValue TypedArrayConstructor(EcmaRuntimeCallInfo *argv,
                                               const JSHandle<JSTaggedValue> &constructor_name);
    static JSHandle<JSObject> AllocateTypedArray(ObjectFactory *factory, EcmaVM *ecma_vm,
                                                 const JSHandle<JSTaggedValue> &constructor_name,
                                                 const JSHandle<JSTaggedValue> &new_target);
    static JSHandle<JSObject> AllocateTypedArray(ObjectFactory *factory, EcmaVM *ecma_vm,
                                                 const JSHandle<JSTaggedValue> &constructor_name,
                                                 const JSHandle<JSTaggedValue> &new_target, double length);
    static JSHandle<JSObject> TypedArraySpeciesCreate(JSThread *thread, const JSHandle<JSObject> &obj, uint32_t argc,
                                                      const JSTaggedType argv[]  // NOLINT(modernize-avoid-c-arrays)
    );
    static JSHandle<JSObject> TypedArrayCreate(JSThread *thread, const JSHandle<JSTaggedValue> &constructor,
                                               uint32_t argc,
                                               const JSTaggedType argv[]  // NOLINT(modernize-avoid-c-arrays)
    );
    static JSTaggedValue ValidateTypedArray(JSThread *thread, const JSHandle<JSTaggedValue> &value);
    inline static DataViewType GetType(const JSHandle<JSObject> &obj);
    inline static int32_t GetElementSize(const JSHandle<JSObject> &obj);
    inline static DataViewType GetTypeFromName(JSThread *thread, const JSHandle<JSTaggedValue> &type_name);
    inline static JSHandle<JSTaggedValue> GetConstructor(JSThread *thread, const JSHandle<JSTaggedValue> &obj);
    inline static JSHandle<JSFunction> GetConstructorFromName(JSThread *thread,
                                                              const JSHandle<JSTaggedValue> &type_name);
    inline static int32_t GetSizeFromName(JSThread *thread, const JSHandle<JSTaggedValue> &type_name);
    inline static int32_t GetByteOffset(JSThread *thread, const JSHandle<JSObject> &obj);
    inline static int32_t GetArrayLength(JSThread *thread, const JSHandle<JSObject> &obj);
    static int32_t SortCompare(JSThread *thread, const JSHandle<JSTaggedValue> &callbackfn_handle,
                               const JSHandle<JSTaggedValue> &buffer, const JSHandle<JSTaggedValue> &first_value,
                               const JSHandle<JSTaggedValue> &second_value);

private:
    static JSTaggedValue CreateFromOrdinaryObject(EcmaRuntimeCallInfo *argv, const JSHandle<JSObject> &obj);
    static JSTaggedValue CreateFromTypedArray(EcmaRuntimeCallInfo *argv, const JSHandle<JSObject> &obj,
                                              const JSHandle<JSTaggedValue> &constructor_name);
    static JSTaggedValue CreateFromArrayBuffer(EcmaRuntimeCallInfo *argv, const JSHandle<JSObject> &obj,
                                               const JSHandle<JSTaggedValue> &constructor_name);
    static JSHandle<JSObject> AllocateTypedArrayBuffer(JSThread *thread, EcmaVM *ecma_vm, const JSHandle<JSObject> &obj,
                                                       double length);
};
}  // namespace panda::ecmascript::base

#endif  // ECMASCRIPT_BASE_TYPED_ARRAY_HELPER_H
