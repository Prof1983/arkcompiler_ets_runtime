/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/array_helper.h"

#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript::base {
bool ArrayHelper::IsConcatSpreadable(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    // 1. If Type(O) is not Object, return false.
    if (!obj->IsECMAObject()) {
        return false;
    }

    // 2. Let spreadable be Get(O, @@isConcatSpreadable).
    auto ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> is_concatspr_key = env->GetIsConcatSpreadableSymbol();
    JSHandle<JSTaggedValue> spreadable = JSTaggedValue::GetProperty(thread, obj, is_concatspr_key).GetValue();
    // 3. ReturnIfAbrupt(spreadable).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, false);

    // 4. If spreadable is not undefined, return ToBoolean(spreadable).
    if (!spreadable->IsUndefined()) {
        return spreadable->ToBoolean();
    }

    // 5. Return IsArray(O).
    return obj->IsArray(thread);
}

int32_t ArrayHelper::SortCompare(JSThread *thread, const JSHandle<JSTaggedValue> &callbackfn_handle,
                                 const JSHandle<JSTaggedValue> &value_x, const JSHandle<JSTaggedValue> &value_y)
{
    // 1. If x and y are both undefined, return +0.
    if (value_x->IsHole()) {
        if (value_y->IsHole()) {
            return 0;
        }
        return 1;
    }
    if (value_y->IsHole()) {
        return -1;
    }
    if (value_x->IsUndefined()) {
        if (value_y->IsUndefined()) {
            return 0;
        }
        // 2. If x is undefined, return 1.
        return 1;
    }
    // 3. If y is undefined, return -1.
    if (value_y->IsUndefined()) {
        return -1;
    }
    // 4. If the argument comparefn is not undefined, then
    // a. Let v be ToNumber(Call(comparefn, undefined, «x, y»)).
    // b. ReturnIfAbrupt(v).
    // c. If v is NaN, return +0.
    // d. Return v.
    if (!callbackfn_handle->IsUndefined()) {
        JSHandle<JSTaggedValue> this_arg_handle(thread, JSTaggedValue::Undefined());

        auto info = NewRuntimeCallInfo(thread, callbackfn_handle, this_arg_handle, JSTaggedValue::Undefined(), 2);
        info->SetCallArgs(value_x, value_y);
        JSTaggedValue call_result = JSFunction::Call(info.Get());  // 2: two args
        if (call_result.IsInt()) {
            return call_result.GetInt();
        }
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
        JSHandle<JSTaggedValue> test_result(thread, call_result);
        JSTaggedNumber v = JSTaggedValue::ToNumber(thread, test_result);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
        double value = v.GetNumber();
        if (std::isnan(value)) {
            return +0;
        }
        return static_cast<int32_t>(value);
    }
    // 5. Let xString be ToString(x).
    // 6. ReturnIfAbrupt(xString).
    // 7. Let yString be ToString(y).
    // 8. ReturnIfAbrupt(yString).
    // 9. If xString < yString, return -1.
    // 10. If xString > yString, return 1.
    // 11. Return +0.
    JSHandle<JSTaggedValue> x_value_handle(JSTaggedValue::ToString(thread, value_x));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
    JSHandle<JSTaggedValue> y_value_handle(JSTaggedValue::ToString(thread, value_y));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
    ComparisonResult compare_result = JSTaggedValue::Compare(thread, x_value_handle, y_value_handle);
    return compare_result == ComparisonResult::GREAT ? 1 : 0;
}

double ArrayHelper::GetLength(JSThread *thread, const JSHandle<JSTaggedValue> &this_handle)
{
    if (this_handle->IsJSArray()) {
        return JSArray::Cast(this_handle->GetTaggedObject())->GetArrayLength();
    }
    if (this_handle->IsTypedArray()) {
        return TypedArrayHelper::GetArrayLength(thread, JSHandle<JSObject>::Cast(this_handle));
    }
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> len_result = JSTaggedValue::GetProperty(thread, this_handle, length_key).GetValue();
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
    JSTaggedNumber len = JSTaggedValue::ToLength(thread, len_result);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
    return len.GetNumber();
}

double ArrayHelper::GetArrayLength(JSThread *thread, const JSHandle<JSTaggedValue> &this_handle)
{
    if (this_handle->IsJSArray()) {
        return JSArray::Cast(this_handle->GetTaggedObject())->GetArrayLength();
    }
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSHandle<JSTaggedValue> len_result = JSTaggedValue::GetProperty(thread, this_handle, length_key).GetValue();
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
    JSTaggedNumber len = JSTaggedValue::ToLength(thread, len_result);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
    return len.GetNumber();
}
}  // namespace panda::ecmascript::base
