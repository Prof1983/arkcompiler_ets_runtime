/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_RUNTIME_ECMASCRIPT_BASE_OBJECT_HELP_H
#define PANDA_RUNTIME_ECMASCRIPT_BASE_OBJECT_HELP_H

#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"

namespace panda::ecmascript::base {
class ObjectHelper {
public:
    static JSTaggedValue CreateDataPropertyOnObject(EcmaRuntimeCallInfo *argv);
    static JSTaggedValue AddEntriesFromIterable(JSThread *thread, const JSHandle<JSTaggedValue> &target,
                                                const JSHandle<JSTaggedValue> &iterable,
                                                JSHandle<JSTaggedValue> &adder);
};
}  // namespace panda::ecmascript::base
#endif  // PANDA_RUNTIME_ECMASCRIPT_BASE_OBJECT_HELP_H
