/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/typed_array_helper.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/error_helper.h"
#include "plugins/ecmascript/runtime/base/error_type.h"
#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_array_iterator.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript::base {

// es11 22.2.4 The TypedArray Constructors
JSTaggedValue TypedArrayHelper::TypedArrayConstructor(EcmaRuntimeCallInfo *argv,
                                                      const JSHandle<JSTaggedValue> &constructor_name)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    // 2. If NewTarget is undefined, throw a TypeError exception.
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The NewTarget is undefined.", JSTaggedValue::Exception());
    }
    // 3. Let constructorName be the String value of the Constructor Name value specified in Table 61 for this
    // TypedArray constructor.
    // 4. Let O be ? AllocateTypedArray(constructorName, NewTarget, "%TypedArray.prototype%").
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> first_arg = builtins_common::GetCallArg(argv, 0);
    if (!first_arg->IsECMAObject()) {
        // es11 22.2.4.1 TypedArray ( )
        double element_length = 0;
        // es11 22.2.4.2 TypedArray ( length )
        if (!first_arg->IsUndefined()) {
            JSTaggedNumber index = JSTaggedValue::ToIndex(thread, first_arg);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            element_length = index.GetNumber();
        }
        JSHandle<JSObject> obj =
            TypedArrayHelper::AllocateTypedArray(factory, ecma_vm, constructor_name, new_target, element_length);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        return obj.GetTaggedValue();
    }
    JSHandle<JSObject> obj = TypedArrayHelper::AllocateTypedArray(factory, ecma_vm, constructor_name, new_target);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    if (first_arg->IsTypedArray()) {
        return TypedArrayHelper::CreateFromTypedArray(argv, obj, constructor_name);
    }
    if (first_arg->IsArrayBuffer()) {
        return TypedArrayHelper::CreateFromArrayBuffer(argv, obj, constructor_name);
    }
    return TypedArrayHelper::CreateFromOrdinaryObject(argv, obj);
}

// es11 22.2.4.4 TypedArray ( object )
JSTaggedValue TypedArrayHelper::CreateFromOrdinaryObject(EcmaRuntimeCallInfo *argv, const JSHandle<JSObject> &obj)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> object_arg = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> object(object_arg);
    // 5. Let usingIterator be ? GetMethod(object, @@iterator).
    JSHandle<JSTaggedValue> iterator_symbol = env->GetIteratorSymbol();
    JSHandle<JSTaggedValue> using_iterator =
        JSObject::GetMethod(thread, JSHandle<JSTaggedValue>::Cast(object), iterator_symbol);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    // 6. If usingIterator is not undefined, then
    if (!using_iterator->IsUndefined()) {
        PandaVector<JSHandle<JSTaggedValue>> vec;
        // a. Let values be ? IterableToList(object, usingIterator).
        // b. Let len be the number of elements in values.
        // c. Perform ? AllocateTypedArrayBuffer(O, len).
        JSHandle<JSTaggedValue> iterator = JSIterator::GetIterator(thread, object_arg, using_iterator);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSHandle<JSTaggedValue> next(thread, JSTaggedValue::True());
        while (!next->IsFalse()) {
            next = JSIterator::IteratorStep(thread, iterator);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            if (!next->IsFalse()) {
                JSHandle<JSTaggedValue> next_value = JSIterator::IteratorValue(thread, next);
                vec.push_back(next_value);
            }
        }
        int32_t len = vec.size();
        TypedArrayHelper::AllocateTypedArrayBuffer(thread, ecma_vm, obj, len);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // d. Let k be 0.
        // e. Repeat, while k < len
        //   i. Let Pk be ! ToString(k).
        //   ii. Let kValue be the first element of values and remove that element from values.
        //   iii. Perform ? Set(O, Pk, kValue, true).
        //   iv. Set k to k + 1.
        JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
        double k = 0;
        while (k < len) {
            t_key.Update(JSTaggedValue(k));
            JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
            JSHandle<JSTaggedValue> k_value = vec[k];
            JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(obj), k_key, k_value, true);
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            k++;
        }
        // f. Assert: values is now an empty List.
        // g. Return O.
        return obj.GetTaggedValue();
    }

    // 7. NOTE: object is not an Iterable so assume it is already an array-like object.
    // 8. Let arrayLike be object.
    // 9. Let len be ? LengthOfArrayLike(arrayLike).
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    JSTaggedNumber len_temp =
        JSTaggedValue::ToLength(thread, JSObject::GetProperty(thread, object_arg, length_key).GetValue());
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    double len = len_temp.GetNumber();
    // 10. Perform ? AllocateTypedArrayBuffer(O, len).
    TypedArrayHelper::AllocateTypedArrayBuffer(thread, ecma_vm, obj, len);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // 11. Let k be 0.
    // 12. Repeat, while k < len
    //   a. Let Pk be ! ToString(k).
    //   b. Let kValue be ? Get(arrayLike, Pk).
    //   c. Perform ? Set(O, Pk, kValue, true).
    //   d. Set k to k + 1.
    JSMutableHandle<JSTaggedValue> t_key(thread, JSTaggedValue::Undefined());
    double k = 0;
    while (k < len) {
        t_key.Update(JSTaggedValue(k));
        JSHandle<JSTaggedValue> k_key(JSTaggedValue::ToString(thread, t_key));
        JSHandle<JSTaggedValue> k_value = JSObject::GetProperty(thread, object_arg, k_key).GetValue();
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        JSTaggedValue::SetProperty(thread, JSHandle<JSTaggedValue>::Cast(obj), k_key, k_value, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        k++;
    }
    // 13. Return O.
    return obj.GetTaggedValue();
}

// es11 22.2.4.3 TypedArray ( typedArray )
JSTaggedValue TypedArrayHelper::CreateFromTypedArray(EcmaRuntimeCallInfo *argv, const JSHandle<JSObject> &obj,
                                                     const JSHandle<JSTaggedValue> &constructor_name)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 5. Let srcArray be typedArray.
    JSHandle<JSTaggedValue> src_array = builtins_common::GetCallArg(argv, 0);
    JSHandle<JSObject> src_obj(src_array);
    // 6. Let srcData be srcArray.[[ViewedArrayBuffer]].
    JSHandle<JSTaggedValue> src_data(thread, JSTypedArray::Cast(*src_obj)->GetViewedArrayBuffer());
    // 7. If IsDetachedBuffer(srcData) is true, throw a TypeError exception.
    if (builtins::array_buffer::IsDetachedBuffer(src_data.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The srcData is detached buffer.", JSTaggedValue::Exception());
    }
    // 8. Let elementType be the Element Type value in Table 61 for constructorName.
    DataViewType element_type = TypedArrayHelper::GetTypeFromName(thread, constructor_name);
    // 9. Let elementLength be srcArray.[[ArrayLength]].
    // 10. Let srcName be the String value of srcArray.[[TypedArrayName]].
    // 11. Let srcType be the Element Type value in Table 61 for srcName.
    // 12. Let srcElementSize be the Element Size value specified in Table 61 for srcName.
    int32_t element_length = TypedArrayHelper::GetArrayLength(thread, src_obj);
    JSHandle<JSTaggedValue> src_name(thread, JSTypedArray::Cast(*src_obj)->GetTypedArrayName());
    DataViewType src_type = TypedArrayHelper::GetTypeFromName(thread, src_name);
    int32_t src_element_size = TypedArrayHelper::GetSizeFromName(thread, src_name);
    // 13. Let srcByteOffset be srcArray.[[ByteOffset]].
    // 14. Let elementSize be the Element Size value specified in Table 61 for constructorName.
    // 15. Let byteLength be elementSize × elementLength.
    double src_byte_offset = TypedArrayHelper::GetByteOffset(thread, src_obj);
    double element_size = TypedArrayHelper::GetSizeFromName(thread, constructor_name);
    double byte_length = element_size * element_length;
    // 16. If IsSharedArrayBuffer(srcData) is false, then
    //   a. Let bufferConstructor be ? SpeciesConstructor(srcData, %ArrayBuffer%).

    JSMutableHandle<JSTaggedValue> data(thread, JSTaggedValue::Undefined());
    // 18. If elementType is the same as srcType, then
    //   a. Let data be ? CloneArrayBuffer(srcData, srcByteOffset, byteLength, bufferConstructor).
    if (element_type == src_type) {
        data.Update(builtins::array_buffer::CloneArrayBuffer(thread, src_data, src_byte_offset,
                                                             global_const->GetHandledUndefined()));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    } else {
        // 19. Else,
        //   a. Let data be ? AllocateArrayBuffer(bufferConstructor, byteLength).
        JSHandle<JSTaggedValue> buffer_constructor =
            JSObject::SpeciesConstructor(thread, JSHandle<JSObject>(src_data), env->GetArrayBufferFunction());
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        data.Update(builtins::array_buffer::AllocateArrayBuffer(thread, buffer_constructor, byte_length));
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        //   b. If IsDetachedBuffer(srcData) is true, throw a TypeError exception.
        if (builtins::array_buffer::IsDetachedBuffer(src_data.GetTaggedValue())) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "The srcData is detached buffer.", JSTaggedValue::Exception());
        }
        //   d. Let srcByteIndex be srcByteOffset.
        //   e. Let targetByteIndex be 0.
        int32_t src_byte_index = src_byte_offset;
        int32_t target_byte_index = 0;
        //   f. Let count be elementLength.
        //   g. Repeat, while count > 0
        JSMutableHandle<JSTaggedValue> value(thread, JSTaggedValue::Undefined());
        for (int32_t count = element_length; count > 0; count--) {
            // i. Let value be GetValueFromBuffer(srcData, srcByteIndex, srcType, true, Unordered).
            JSTaggedValue tagged_data =
                builtins::array_buffer::GetValueFromBuffer(thread, src_data, src_byte_index, src_type, true);
            value.Update(tagged_data);
            // ii. Perform SetValueInBuffer(data, targetByteIndex, elementType, value, true, Unordered).
            builtins::array_buffer::SetValueInBuffer(thread, data, target_byte_index, element_type, value, true);
            // iii. Set srcByteIndex to srcByteIndex + srcElementSize.
            // iv. Set targetByteIndex to targetByteIndex + elementSize.
            // v. Set count to count - 1.
            src_byte_index = src_byte_index + src_element_size;
            target_byte_index = target_byte_index + element_size;
        }
    }
    // 19. Set O’s [[ViewedArrayBuffer]] internal slot to data.
    // 20. Set O’s [[ByteLength]] internal slot to byteLength.
    // 21. Set O’s [[ByteOffset]] internal slot to 0.
    // 22. Set O’s [[ArrayLength]] internal slot to elementLength.
    JSTypedArray::Cast(*obj)->SetViewedArrayBuffer(thread, data.GetTaggedValue());
    JSTypedArray::Cast(*obj)->SetByteLength(thread, JSTaggedValue(byte_length));
    JSTypedArray::Cast(*obj)->SetByteOffset(thread, JSTaggedValue(0));
    JSTypedArray::Cast(*obj)->SetArrayLength(thread, JSTaggedValue(element_length));
    // 23. Return O.
    return obj.GetTaggedValue();
}

// es11 22.2.4.5 TypedArray ( buffer [ , byteOffset [ , length ] ] )
JSTaggedValue TypedArrayHelper::CreateFromArrayBuffer(EcmaRuntimeCallInfo *argv, const JSHandle<JSObject> &obj,
                                                      const JSHandle<JSTaggedValue> &constructor_name)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    // 5. Let elementSize be the Element Size value specified in Table 61 for constructorName.
    // 6. Let offset be ? ToIndex(byteOffset).
    int32_t element_size = TypedArrayHelper::GetSizeFromName(thread, constructor_name);
    JSHandle<JSTaggedValue> byte_offset = builtins_common::GetCallArg(argv, 1);
    JSTaggedNumber index = JSTaggedValue::ToIndex(thread, byte_offset);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    auto offset = static_cast<int32_t>(index.GetNumber());
    // 7. If offset modulo elementSize ≠ 0, throw a RangeError exception.
    if (offset % element_size != 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "The offset cannot be an integral multiple of elementSize.",
                                     JSTaggedValue::Exception());
    }
    // 8. If length is not undefined, then
    //   a. Let new_length be ? ToIndex(length).
    JSHandle<JSTaggedValue> length = builtins_common::GetCallArg(argv, builtins_common::ArgsPosition::THIRD);
    int32_t new_length = 0;
    if (!length->IsUndefined()) {
        index = JSTaggedValue::ToIndex(thread, length);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        new_length = static_cast<int32_t>(index.GetNumber());
    }
    // 9. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    JSHandle<JSTaggedValue> buffer = builtins_common::GetCallArg(argv, 0);
    if (builtins::array_buffer::IsDetachedBuffer(buffer.GetTaggedValue())) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The srcData is detached buffer.", JSTaggedValue::Exception());
    }
    // 10. Let bufferByteLength be buffer.[[ArrayBufferByteLength]].
    JSTaggedNumber new_length_num =
        JSTaggedNumber::FromIntOrDouble(thread, JSHandle<JSArrayBuffer>(buffer)->GetArrayBufferByteLength());
    int32_t buffer_byte_length = new_length_num.ToInt32();
    // 11. If length is undefined, then
    //   a. If bufferByteLength modulo elementSize ≠ 0, throw a RangeError exception.
    //   b. Let newByteLength be bufferByteLength - offset.
    //   c. If newByteLength < 0, throw a RangeError exception.
    int32_t new_byte_length;
    if (length->IsUndefined()) {
        if (buffer_byte_length % element_size != 0) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "The bufferByteLength cannot be an integral multiple of elementSize.",
                                         JSTaggedValue::Exception());
        }
        new_byte_length = buffer_byte_length - offset;
        if (new_byte_length < 0) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "The newByteLength is less than 0.", JSTaggedValue::Exception());
        }
    } else {
        // 12. Else,
        //   a. Let newByteLength be new_length × elementSize.
        //   b. If offset + newByteLength > bufferByteLength, throw a RangeError exception.
        new_byte_length = new_length * element_size;
        if (offset + new_byte_length > buffer_byte_length) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "The newByteLength is out of range.", JSTaggedValue::Exception());
        }
    }
    // 13. Set O.[[ViewedArrayBuffer]] to buffer.
    // 14. Set O.[[ByteLength]] to newByteLength.
    // 15. Set O.[[ByteOffset]] to offset.
    // 16. Set O.[[ArrayLength]] to newByteLength / elementSize.
    JSTypedArray::Cast(*obj)->SetViewedArrayBuffer(thread, buffer);
    JSTypedArray::Cast(*obj)->SetByteLength(thread, JSTaggedValue(new_byte_length));
    JSTypedArray::Cast(*obj)->SetByteOffset(thread, JSTaggedValue(offset));
    JSTypedArray::Cast(*obj)->SetArrayLength(thread, JSTaggedValue(new_byte_length / element_size));
    // 17. Return O.
    return obj.GetTaggedValue();
}

// es11 22.2.4.2.1 Runtime Semantics: AllocateTypedArray ( constructorName, new_target, defaultProto )
JSHandle<JSObject> TypedArrayHelper::AllocateTypedArray(ObjectFactory *factory, EcmaVM *ecma_vm,
                                                        const JSHandle<JSTaggedValue> &constructor_name,
                                                        const JSHandle<JSTaggedValue> &new_target)
{
    JSThread *thread = ecma_vm->GetJSThread();
    // 1. Let proto be ? GetPrototypeFromConstructor(new_target, defaultProto).
    // 2. Let obj be ! IntegerIndexedObjectCreate(proto).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    JSHandle<JSFunction> typed_array_func = TypedArrayHelper::GetConstructorFromName(thread, constructor_name);
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(typed_array_func, new_target);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    // 3. Assert: obj.[[ViewedArrayBuffer]] is undefined.
    // 4. Set obj.[[TypedArrayName]] to constructorName.

    // 5. If constructorName is "BigInt64Array" or "BigUint64Array", set obj.[[ContentType]] to BigInt.
    // 6. Otherwise, set obj.[[ContentType]] to Number.
    JSTypedArray *js_typed_array = JSTypedArray::Cast(*obj);
    if (JSTaggedValue::SameValue(constructor_name, thread->GlobalConstants()->GetHandledBigInt64ArrayString()) ||
        JSTaggedValue::SameValue(constructor_name, thread->GlobalConstants()->GetHandledBigUint64ArrayString())) {
        js_typed_array->SetContentType(ContentType::BIG_INT);
    } else {
        js_typed_array->SetContentType(ContentType::NUMBER);
    }

    // 7. If length is not present, then
    //   a. Set obj.[[ByteLength]] to 0.
    //   b. Set obj.[[ByteOffset]] to 0.
    //   c. Set obj.[[ArrayLength]] to 0.
    JSTypedArray::Cast(*obj)->SetTypedArrayName(thread, constructor_name);
    JSTypedArray::Cast(*obj)->SetByteLength(thread, JSTaggedValue(0));
    JSTypedArray::Cast(*obj)->SetByteOffset(thread, JSTaggedValue(0));
    JSTypedArray::Cast(*obj)->SetArrayLength(thread, JSTaggedValue(0));
    // 9. Return obj.
    return obj;
}  // namespace panda::ecmascript::base

// es11 22.2.4.2.1 Runtime Semantics: AllocateTypedArray ( constructorName, new_target, defaultProto, length )
JSHandle<JSObject> TypedArrayHelper::AllocateTypedArray(ObjectFactory *factory, EcmaVM *ecma_vm,
                                                        const JSHandle<JSTaggedValue> &constructor_name,
                                                        const JSHandle<JSTaggedValue> &new_target, double length)
{
    JSThread *thread = ecma_vm->GetJSThread();
    // 1. Let proto be ? GetPrototypeFromConstructor(new_target, defaultProto).
    // 2. Let obj be ! IntegerIndexedObjectCreate(proto).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    JSHandle<JSFunction> typed_array_func = TypedArrayHelper::GetConstructorFromName(thread, constructor_name);
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(typed_array_func, new_target);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    // 3. Assert: obj.[[ViewedArrayBuffer]] is undefined.
    // 4. Set obj.[[TypedArrayName]] to constructorName.
    JSTypedArray::Cast(*obj)->SetTypedArrayName(thread, constructor_name);
    // 7. If length is not present, then
    // 8. Else,
    //   a. Perform ? AllocateTypedArrayBuffer(obj, length).
    TypedArrayHelper::AllocateTypedArrayBuffer(thread, ecma_vm, obj, length);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    // 9. Return obj.
    return obj;
}

// es11 22.2.4.2.2 Runtime Semantics: AllocateTypedArrayBuffer ( O, length )
JSHandle<JSObject> TypedArrayHelper::AllocateTypedArrayBuffer(JSThread *thread, EcmaVM *ecma_vm,
                                                              const JSHandle<JSObject> &obj, double length)
{
    JSHandle<GlobalEnv> env = ecma_vm->GetGlobalEnv();
    // 1. Assert: O is an Object that has a [[ViewedArrayBuffer]] internal slot.
    // 2. Assert: O.[[ViewedArrayBuffer]] is undefined.
    // 3. Assert: ! IsNonNegativeInteger(length) is true.
    ASSERT(JSTaggedValue(length).IsInteger());
    ASSERT(length >= 0);
    JSHandle<JSObject> exception(thread, JSTaggedValue::Exception());
    if (length > JSTypedArray::MAX_TYPED_ARRAY_INDEX) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "array length must less than 2^32 - 1", exception);
    }
    // 4. Let constructorName be the String value of O.[[TypedArrayName]].
    JSHandle<JSTaggedValue> constructor_name(thread, JSTypedArray::Cast(*obj)->GetTypedArrayName());
    // 5. Let elementSize be the Element Size value specified in Table 61 for constructorName.
    int32_t element_size = TypedArrayHelper::GetSizeFromName(thread, constructor_name);
    // 6. Let byteLength be elementSize × length.
    double byte_length = element_size * length;
    // 7. Let data be ? AllocateArrayBuffer(%ArrayBuffer%, byteLength).
    JSHandle<JSTaggedValue> constructor = env->GetArrayBufferFunction();
    JSTaggedValue data = builtins::array_buffer::AllocateArrayBuffer(thread, constructor, byte_length);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, exception);
    JSTypedArray *js_typed_array = JSTypedArray::Cast(*obj);
    if (JSTaggedValue::SameValue(constructor_name, thread->GlobalConstants()->GetHandledBigInt64ArrayString()) ||
        JSTaggedValue::SameValue(constructor_name, thread->GlobalConstants()->GetHandledBigUint64ArrayString())) {
        js_typed_array->SetContentType(ContentType::BIG_INT);
    } else {
        js_typed_array->SetContentType(ContentType::NUMBER);
    }
    // 8. Set O.[[ViewedArrayBuffer]] to data.
    // 9. Set O.[[ByteLength]] to byteLength.
    // 10. Set O.[[ByteOffset]] to 0.
    // 11. Set O.[[ArrayLength]] to length.
    JSTypedArray::Cast(*obj)->SetViewedArrayBuffer(thread, data);
    JSTypedArray::Cast(*obj)->SetByteLength(thread, JSTaggedValue(byte_length));
    JSTypedArray::Cast(*obj)->SetByteOffset(thread, JSTaggedValue(0));
    JSTypedArray::Cast(*obj)->SetArrayLength(thread, JSTaggedValue(length));
    // 12. Return O.
    return obj;
}

// es11 22.2.4.7 TypedArraySpeciesCreate ( exemplar, argumentList )
JSHandle<JSObject> TypedArrayHelper::TypedArraySpeciesCreate(
    JSThread *thread, const JSHandle<JSObject> &obj, uint32_t argc,
    const JSTaggedType argv[]  // NOLINT(modernize-avoid-c-arrays)
)
{
    // 1. Assert: exemplar is an Object that has [[TypedArrayName]] and [[ContentType]] internal slots.
    // 2. Let defaultConstructor be the intrinsic object listed in column one of Table 61 for
    // exemplar.[[TypedArrayName]].
    JSHandle<JSTaggedValue> default_constructor =
        TypedArrayHelper::GetConstructor(thread, JSHandle<JSTaggedValue>(obj));
    // 3. Let constructor be ? SpeciesConstructor(exemplar, defaultConstructor).
    JSHandle<JSTaggedValue> this_constructor = JSObject::SpeciesConstructor(thread, obj, default_constructor);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    // 4. Let result be ? TypedArrayCreate(constructor, argumentList).
    return TypedArrayHelper::TypedArrayCreate(thread, this_constructor, argc, argv);
}

// es11 22.2.4.6 TypedArrayCreate ( constructor, argumentList )
JSHandle<JSObject> TypedArrayHelper::TypedArrayCreate(JSThread *thread, const JSHandle<JSTaggedValue> &constructor,
                                                      uint32_t argc,
                                                      const JSTaggedType argv[]  // NOLINT(modernize-avoid-c-arrays)
)
{
    // 1. Let newTypedArray be ? Construct(constructor, argumentList).
    auto info = NewRuntimeCallInfo(thread, constructor, JSTaggedValue::Undefined(), JSTaggedValue::Undefined(), argc);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    info->SetCallArg(argc, argv);
    JSTaggedValue tagged_array = JSFunction::Construct(info.Get());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    if (!tagged_array.IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Failed to construct the Typedarray.",
                                    JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    }
    JSHandle<JSTaggedValue> tagged_array_handle(thread, tagged_array);
    // 2. Perform ? ValidateTypedArray(newTypedArray).
    TypedArrayHelper::ValidateTypedArray(thread, tagged_array_handle);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
    JSHandle<JSObject> new_typed_array(tagged_array_handle);
    // 3. If argumentList is a List of a single Number, then
    //   a. If newTypedArray.[[ArrayLength]] < argumentList[0], throw a TypeError exception.
    if (argc == 1) {
        if (TypedArrayHelper::GetArrayLength(thread, new_typed_array) <
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            JSTaggedValue::ToInt32(thread, JSHandle<JSTaggedValue>(thread, JSTaggedValue(argv[0])))) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "the length of newTypedArray is not a correct value.",
                                        JSHandle<JSObject>(thread, JSTaggedValue::Exception()));
        }
    }
    // 4. Return newTypedArray.
    return new_typed_array;
}

// es11 22.2.3.5.1 Runtime Semantics: ValidateTypedArray ( O )
JSTaggedValue TypedArrayHelper::ValidateTypedArray(JSThread *thread, const JSHandle<JSTaggedValue> &value)
{
    // 1. Perform ? RequireInternalSlot(O, [[TypedArrayName]]).
    // 2. Assert: O has a [[ViewedArrayBuffer]] internal slot.
    if (!value->IsTypedArray()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The O is not a TypedArray.", JSTaggedValue::Exception());
    }
    // 3. Let buffer be O.[[ViewedArrayBuffer]].
    // 4. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    JSTaggedValue buffer = JSHandle<JSTypedArray>::Cast(value)->GetViewedArrayBuffer();
    if (builtins::array_buffer::IsDetachedBuffer(buffer)) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "The ViewedArrayBuffer of O is detached buffer.",
                                    JSTaggedValue::Exception());
    }
    // 5. Return buffer.
    return buffer;
}

int32_t TypedArrayHelper::SortCompare(JSThread *thread, const JSHandle<JSTaggedValue> &callbackfn_handle,
                                      const JSHandle<JSTaggedValue> &buffer, const JSHandle<JSTaggedValue> &first_value,
                                      const JSHandle<JSTaggedValue> &second_value)
{
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // 1. Assert: Both Type(x) and Type(y) is Number.
    ASSERT((first_value->IsNumber() && second_value->IsNumber()) ||
           (first_value->IsBigInt() && second_value->IsBigInt()));
    // 2. If the argument comparefn is not undefined, then
    //   a. Let v be Call(comparefn, undefined, «x, y»).
    //   b. ReturnIfAbrupt(v).
    //   c. If IsDetachedBuffer(buffer) is true, throw a TypeError exception.
    //   d. If v is NaN, return +0.
    //   e. Return v.
    if (!callbackfn_handle->IsUndefined()) {
        JSHandle<JSTaggedValue> this_arg_handle = global_const->GetHandledUndefined();

        auto info = NewRuntimeCallInfo(thread, callbackfn_handle, this_arg_handle, JSTaggedValue::Undefined(), 2);
        info->SetCallArgs(first_value, second_value);
        JSTaggedValue call_result = JSFunction::Call(info.Get());  // 2: two args
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
        if (builtins::array_buffer::IsDetachedBuffer(buffer.GetTaggedValue())) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "The buffer is detached buffer.", 0);
        }
        JSHandle<JSTaggedValue> test_result(thread, call_result);
        JSTaggedNumber v = JSTaggedValue::ToNumber(thread, test_result);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, 0);
        double value = v.GetNumber();
        if (std::isnan(value)) {
            return +0;
        }
        return value;
    }
    // 3. If x and y are both NaN, return +0.
    if (NumberHelper::IsNaN(first_value.GetTaggedValue())) {
        if (NumberHelper::IsNaN(second_value.GetTaggedValue())) {
            return +0;
        }
        // 4. If x is NaN, return 1.
        return 1;
    }
    // 5. If y is NaN, return -1.
    if (NumberHelper::IsNaN(second_value.GetTaggedValue())) {
        return -1;
    }
    ComparisonResult compare_result = JSTaggedValue::Compare(thread, first_value, second_value);
    // 6. If x < y, return -1.
    // 7. If x > y, return 1.
    // 8. If x is -0 and y is +0, return -1.
    // 9. If x is +0 and y is -0, return 1.
    // 10. Return +0.
    if (compare_result == ComparisonResult::LESS) {
        return -1;
    }
    if (compare_result == ComparisonResult::GREAT) {
        return 1;
    }
    JSTaggedNumber x_number = JSTaggedValue::ToNumber(thread, first_value);
    JSTaggedNumber y_number = JSTaggedValue::ToNumber(thread, second_value);
    double e_zero_temp = -0.0;
    auto e_zero = JSTaggedNumber(e_zero_temp);
    double p_zero_temp = +0.0;
    auto p_zero = JSTaggedNumber(p_zero_temp);
    if (JSTaggedNumber::SameValue(x_number, e_zero) && JSTaggedNumber::SameValue(y_number, p_zero)) {
        return -1;
    }
    if (JSTaggedNumber::SameValue(x_number, p_zero) && JSTaggedNumber::SameValue(y_number, e_zero)) {
        return 1;
    }
    return +0;
}
}  // namespace panda::ecmascript::base
