/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_BASE_BUILTINS_BASE_H
#define ECMASCRIPT_BASE_BUILTINS_BASE_H

#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env_constants-inl.h"
#include "plugins/ecmascript/runtime/js_dataview.h"
#include "plugins/ecmascript/runtime/js_symbol.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/runtime_call_id.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "plugins/ecmascript/runtime/vmstat/runtime_stat.h"

namespace panda::ecmascript {

namespace builtins {
#include "plugins/ecmascript/runtime/builtins/generated/builtins_declaration_gen.h"
}  // namespace builtins

namespace builtins_common {

enum ArgsPosition : uint32_t { FIRST = 0, SECOND, THIRD, FOURTH, FIFTH };
JSHandle<TaggedArray> GetArgsArray(EcmaRuntimeCallInfo *msg);
inline JSHandle<JSTaggedValue> GetConstructor(EcmaRuntimeCallInfo *msg)
{
    return msg->GetFunction();
}

inline JSHandle<JSTaggedValue> GetThis(EcmaRuntimeCallInfo *msg)
{
    return msg->GetThis();
}

inline JSHandle<JSTaggedValue> GetNewTarget(EcmaRuntimeCallInfo *msg)
{
    return msg->GetNewTarget();
}

inline JSHandle<JSTaggedValue> GetCallArg(EcmaRuntimeCallInfo *msg, uint32_t position)
{
    if (position >= msg->GetArgsNumber()) {
        JSThread *thread = msg->GetThread();
        return thread->GlobalConstants()->GetHandledUndefined();
    }
    return msg->GetCallArg(position);
}

inline JSTaggedValue GetTaggedInt(int32_t value)
{
    return JSTaggedValue(value);
}

inline JSTaggedValue GetTaggedDouble(double value)
{
    return JSTaggedValue(value);
}

inline JSTaggedValue GetTaggedBoolean(bool value)
{
    return JSTaggedValue(value);
}

inline JSTaggedValue GetTaggedString(JSThread *thread, const char *str)
{
    return thread->GetEcmaVM()->GetFactory()->NewFromCanBeCompressString(str).GetTaggedValue();
}
}  // namespace builtins_common

}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_BASE_BUILTINS_BASE_H
