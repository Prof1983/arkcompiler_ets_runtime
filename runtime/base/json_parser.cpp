/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/base/json_parser.h"

namespace panda::ecmascript::base {
JSHandle<JSTaggedValue> Internalize::InternalizeJsonProperty(JSThread *thread, const JSHandle<JSObject> &holder,
                                                             const JSHandle<JSTaggedValue> &name,
                                                             const JSHandle<JSTaggedValue> &receiver)
{
    JSHandle<JSTaggedValue> obj_handle(holder);
    JSHandle<JSTaggedValue> val = JSTaggedValue::GetProperty(thread, obj_handle, name).GetValue();
    JSHandle<JSTaggedValue> length_key = thread->GlobalConstants()->GetHandledLengthString();
    if (val->IsECMAObject()) {
        JSHandle<JSObject> obj = JSTaggedValue::ToObject(thread, val);
        bool is_array = val->IsArray(thread);
        if (is_array) {
            JSHandle<JSTaggedValue> len_result = JSTaggedValue::GetProperty(thread, val, length_key).GetValue();
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);
            JSTaggedNumber len_number = JSTaggedValue::ToLength(thread, len_result);
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);
            uint32_t length = len_number.ToUint32();
            JSMutableHandle<JSTaggedValue> key_unknow(thread, JSTaggedValue::Undefined());
            JSMutableHandle<JSTaggedValue> key_name(thread, JSTaggedValue::Undefined());
            for (uint32_t i = 0; i < length; i++) {
                // Let prop be ! ToString((I)).
                key_unknow.Update(JSTaggedValue(i));
                key_name.Update(JSTaggedValue::ToString(thread, key_unknow).GetTaggedValue());
                RecurseAndApply(thread, obj, key_name, receiver);
            }
        } else {
            // Let keys be ? EnumerableOwnPropertyNames(val, key).
            JSHandle<TaggedArray> owner_names(JSObject::EnumerableOwnNames(thread, obj));
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSTaggedValue, thread);
            uint32_t names_length = owner_names->GetLength();
            JSMutableHandle<JSTaggedValue> key_name(thread, JSTaggedValue::Undefined());
            for (uint32_t i = 0; i < names_length; i++) {
                key_name.Update(JSTaggedValue::GetProperty(thread, JSHandle<JSTaggedValue>(owner_names), i)
                                    .GetValue()
                                    .GetTaggedValue());
                RecurseAndApply(thread, obj, key_name, receiver);
            }
        }
    }

    // Return ? Call(receiver, holder, « name, val »).

    auto info = NewRuntimeCallInfo(thread, receiver, obj_handle, JSTaggedValue::Undefined(), 2);
    info->SetCallArgs(name, val);
    JSTaggedValue result = JSFunction::Call(info.Get());  // 2: two args
    return JSHandle<JSTaggedValue>(thread, result);
}

bool Internalize::RecurseAndApply(JSThread *thread, const JSHandle<JSObject> &holder,
                                  const JSHandle<JSTaggedValue> &name, const JSHandle<JSTaggedValue> &receiver)
{
    JSHandle<JSTaggedValue> value = InternalizeJsonProperty(thread, holder, name, receiver);
    bool change_result = false;

    // If newElement is undefined, then Perform ? val.[[Delete]](P).
    if (value->IsUndefined()) {
        change_result = JSObject::DeleteProperty(thread, holder, name);
    } else {
        // Perform ? CreateDataProperty(val, P, newElement)
        change_result = JSObject::CreateDataProperty(thread, holder, name, value);
    }
    return change_result;
}
}  // namespace panda::ecmascript::base
