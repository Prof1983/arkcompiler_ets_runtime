/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_BASE_JSON_PARSE_INL_H
#define ECMASCRIPT_BASE_JSON_PARSE_INL_H

#include "plugins/ecmascript/runtime/base/json_parser.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/number_helper.h"
#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/ecma_string-inl.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "libpandabase/utils/utf.h"

namespace panda::ecmascript::base {
constexpr unsigned int UNICODE_DIGIT_LENGTH = 4;
constexpr unsigned int NUMBER_TEN = 10;
constexpr unsigned int NUMBER_SIXTEEN = 16;

constexpr unsigned char CODE_SPACE = 0x20;
constexpr unsigned char ASCII_END = 0X7F;
enum class Tokens : uint8_t {
    // six structural tokens
    OBJECT = 0,
    ARRAY,
    NUMBER,
    STRING,
    LITERAL_TRUE,
    LITERAL_FALSE,
    LITERAL_NULL,
    TOKEN_ILLEGAL,
};

template <typename T>
class JsonParser {
public:
    using Text = const T *;
    explicit JsonParser() = default;
    explicit JsonParser(JSThread *thread) : thread_(thread) {}
    ~JsonParser() = default;
    NO_COPY_SEMANTIC(JsonParser);
    NO_MOVE_SEMANTIC(JsonParser);
    JSHandle<JSTaggedValue> Parse(Text begin, Text end)
    {
        end_ = end - 1;
        current_ = begin;

        auto vm = thread_->GetEcmaVM();
        factory_ = vm->GetFactory();
        env_ = *vm->GetGlobalEnv();

        SkipEndWhiteSpace();
        range_ = end_;
        JSTaggedValue result = ParseJSONText<false>();
        return JSHandle<JSTaggedValue>(thread_, result);
    }

    JSHandle<JSTaggedValue> ParseUtf8(EcmaString *str)
    {
        ASSERT(str != nullptr);
        is_ascii_string_ = true;
        uint32_t len = str->GetUtf8Length();
        PandaVector<T> buf(len);
        str->CopyDataUtf8(buf.data(), len);
        Text begin = buf.data();
        return Parse(begin, begin + str->GetLength());
    }

    JSHandle<JSTaggedValue> ParseUtf16(EcmaString *str)
    {
        ASSERT(str != nullptr);
        uint32_t len = str->GetLength();
        PandaVector<T> buf(len);
        str->CopyDataUtf16(buf.data(), len);
        Text begin = buf.data();
        return Parse(begin, begin + len);
    }

private:
    template <bool IN_OBJOR_ARR = false>
    JSTaggedValue ParseJSONText()
    {
        SkipStartWhiteSpace();
        Tokens token = ParseToken();
        switch (token) {
            case Tokens::OBJECT:
                return ParseObject<IN_OBJOR_ARR>();
            case Tokens::ARRAY:
                return ParseArray<IN_OBJOR_ARR>();
            case Tokens::LITERAL_TRUE:
                return ParseLiteral("true", Tokens::LITERAL_TRUE);
            case Tokens::LITERAL_FALSE:
                return ParseLiteral("false", Tokens::LITERAL_FALSE);
            case Tokens::LITERAL_NULL:
                return ParseLiteral("null", Tokens::LITERAL_NULL);
            case Tokens::NUMBER:
                return ParseNumber<IN_OBJOR_ARR>();
            case Tokens::STRING:
                return ParseString<IN_OBJOR_ARR>();
            default:
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Text in JSON", JSTaggedValue::Exception());
        }
    }

    template <bool IN_OBJ_OR_ARR = false>
    JSTaggedValue ParseNumber()
    {
        if (IN_OBJ_OR_ARR) {
            bool is_fast = true;
            bool is_number = ReadNumberRange(is_fast);
            if (!is_number) {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Number in JSON", JSTaggedValue::Exception());
            }
            if (is_fast) {
                std::string str_num(current_, end_ + 1);
                current_ = end_;
                auto d = std::stod(str_num);
                if (static_cast<int32_t>(d) == d) {
                    return JSTaggedValue(static_cast<int32_t>(d));
                }
                return JSTaggedValue(d);
            }
        }

        Text current = current_;
        bool has_exponent = false;
        if (*current_ == '-') {
            if (UNLIKELY(current_++ == end_)) {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Number in JSON", JSTaggedValue::Exception());
            }
        }
        if (*current_ == '0') {
            if (!CheckZeroBeginNumber(has_exponent)) {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Number in JSON", JSTaggedValue::Exception());
            }
        } else if (*current_ >= '1' && *current_ <= '9') {
            if (!CheckNonZeroBeginNumber(has_exponent)) {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Number in JSON", JSTaggedValue::Exception());
            }
        } else {
            THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Number in JSON", JSTaggedValue::Exception());
        }

        std::string str_num(current, end_ + 1);
        current_ = end_;
        return JSTaggedValue(std::stod(str_num));
    }

    bool ReadJsonStringRange(bool &is_fast_string, bool &is_ascii)
    {
        current_++;
        if (is_ascii_string_) {
            return ReadAsciiStringRange(is_fast_string);
        }
        return ReadStringRange(is_fast_string, is_ascii);
    }

    bool IsFastParseJsonString(bool &is_fast_string, bool &is_ascii)
    {
        current_++;
        if (is_ascii_string_) {
            return IsFastParseString(is_fast_string, is_ascii);
        }
        return IsFastParseAsciiString(is_fast_string);
    }

    bool ParseBackslash(PandaString &res)
    {
        if (current_ == end_) {
            return false;
        }
        current_++;
        switch (*current_) {
            case '\"':
                res += "\"";
                break;
            case '\\':
                res += "\\";
                break;
            case '/':
                res += "/";
                break;
            case 'b':
                res += "\b";
                break;
            case 'f':
                res += "\f";
                break;
            case 'n':
                res += "\n";
                break;
            case 'r':
                res += "\r";
                break;
            case 't':
                res += "\t";
                break;
            case 'u': {
                PandaVector<uint16_t> vec;
                if (UNLIKELY(!ConvertStringUnicode(vec))) {
                    return false;
                }
                std::u16string u16_str;
                u16_str.assign(vec.begin(), vec.end());
                res += base::StringHelper::U16stringToString(u16_str);
                break;
            }
            default:
                return false;
        }
        return true;
    }

    JSTaggedValue SlowParseString()
    {
        end_--;
        PandaString res;
        while (current_ <= end_) {
            if (*current_ == '\\') {
                bool is_legal_char = ParseBackslash(res);
                if (!is_legal_char) {
                    THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected string in JSON", JSTaggedValue::Exception());
                }
            } else {
                res += *current_;
            }
            current_++;
        }
        return factory_->NewFromUtf8Literal(reinterpret_cast<const uint8_t *>(res.c_str()), res.length())
            .GetTaggedValue();
    }

    template <bool IN_OBJOR_ARR = false>
    JSTaggedValue ParseString()
    {
        bool is_fast_string = true;
        bool is_ascii = true;
        bool is_legal = true;
        if (IN_OBJOR_ARR) {
            is_legal = ReadJsonStringRange(is_fast_string, is_ascii);
            if (!is_legal) {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected end Text in JSON", JSTaggedValue::Exception());
            }
            if (is_fast_string) {
                if (is_ascii) {
                    PandaString value(current_, end_);
                    current_ = end_;
                    return factory_
                        ->NewFromUtf8LiteralUnCheck(reinterpret_cast<const uint8_t *>(value.c_str()), value.length(),
                                                    true)
                        .GetTaggedValue();
                }
                std::u16string value(current_, end_);
                current_ = end_;
                return factory_
                    ->NewFromUtf16LiteralUnCheck(reinterpret_cast<const uint16_t *>(value.c_str()), value.length(),
                                                 false)
                    .GetTaggedValue();
            }
        } else {
            if (*end_ != '"' || current_ == end_) {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected end Text in JSON", JSTaggedValue::Exception());
            }
            is_legal = IsFastParseJsonString(is_fast_string, is_ascii);
            if (!is_legal) {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected end Text in JSON", JSTaggedValue::Exception());
            }
            if (LIKELY(is_fast_string)) {
                if (is_ascii) {
                    PandaString value(current_, end_);
                    return factory_
                        ->NewFromUtf8LiteralUnCheck(reinterpret_cast<const uint8_t *>(value.c_str()), value.length(),
                                                    true)
                        .GetTaggedValue();
                }
                std::u16string value(current_, end_);
                return factory_
                    ->NewFromUtf16LiteralUnCheck(reinterpret_cast<const uint16_t *>(value.c_str()), value.length(),
                                                 false)
                    .GetTaggedValue();
            }
        }
        return SlowParseString();
    }

    template <bool IN_OBJOR_ARR = false>
    JSTaggedValue ParseArray()
    {
        if (UNLIKELY(*range_ != ']' && !IN_OBJOR_ARR)) {
            THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Array in JSON", JSTaggedValue::Exception());
        }

        current_++;
        [[maybe_unused]] EcmaHandleScope handle_scope(thread_);
        JSHandle<JSArray> arr = factory_->NewJSArray();
        if (*current_ == ']') {
            return arr.GetTaggedValue();
        }

        JSTaggedValue value;
        uint32_t index = 0;
        while (current_ <= range_) {
            value = ParseJSONText<true>();
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);
            FastRuntimeStub::SetPropertyByIndex<true>(thread_, arr.GetTaggedValue(), index++, value);
            GetNextNonSpaceChar();
            if (*current_ == ',') {
                current_++;
            } else if (*current_ == ']') {
                if (IN_OBJOR_ARR || current_ == range_) {
                    return arr.GetTaggedValue();
                }
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Array in JSON", JSTaggedValue::Exception());
            }
        }
        THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Array in JSON", JSTaggedValue::Exception());
    }

    template <bool IN_OBJOR_ARR = false>
    JSTaggedValue ParseObject()
    {
        if (UNLIKELY(*range_ != '}' && !IN_OBJOR_ARR)) {
            THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Object in JSON", JSTaggedValue::Exception());
        }

        [[maybe_unused]] EcmaHandleScope handle_scope(thread_);
        JSHandle<JSTaggedValue> proto = env_->GetObjectFunction();
        JSHandle<JSObject> result = factory_->NewJSObjectByConstructor(JSHandle<JSFunction>(proto), proto);
        current_++;
        if (*current_ == '}') {
            return result.GetTaggedValue();
        }

        JSMutableHandle<JSTaggedValue> key_handle(thread_, JSTaggedValue::Undefined());
        JSMutableHandle<JSTaggedValue> value(thread_, JSTaggedValue::Undefined());
        while (current_ <= range_) {
            SkipStartWhiteSpace();
            if (*current_ == '"') {
                key_handle.Update(ParseString<true>());
            } else {
                if (*current_ == '}' && (IN_OBJOR_ARR || current_ == range_)) {
                    return result.GetTaggedValue();
                }
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Object in JSON", JSTaggedValue::Exception());
            }
            GetNextNonSpaceChar();
            if (*current_ == ':') {
                current_++;
            } else {
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Object in JSON", JSTaggedValue::Exception());
            }
            value.Update(ParseJSONText<true>());
            FastRuntimeStub::SetPropertyByValue<true>(thread_, JSHandle<JSTaggedValue>(result), key_handle, value);
            GetNextNonSpaceChar();
            if (*current_ == ',') {
                current_++;
            } else if (*current_ == '}') {
                if (IN_OBJOR_ARR || current_ == range_) {
                    return result.GetTaggedValue();
                }
                THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Object in JSON", JSTaggedValue::Exception());
            }
        }
        THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Object in JSON", JSTaggedValue::Exception());
    }

    void SkipEndWhiteSpace()
    {
        while (current_ != end_) {
            if (*end_ == ' ' || *end_ == '\r' || *end_ == '\n' || *end_ == '\t') {
                end_--;
            } else {
                break;
            }
        }
    }

    void SkipStartWhiteSpace()
    {
        while (current_ != end_) {
            if (*current_ == ' ' || *current_ == '\r' || *current_ == '\n' || *current_ == '\t') {
                current_++;
            } else {
                break;
            }
        }
    }

    void GetNextNonSpaceChar()
    {
        current_++;
        SkipStartWhiteSpace();
    }

    Tokens ParseToken()
    {
        switch (*current_) {
            case '{':
                return Tokens::OBJECT;
            case '[':
                return Tokens::ARRAY;
            case '"':
                return Tokens::STRING;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '-':
                return Tokens::NUMBER;
            case 't':
                return Tokens::LITERAL_TRUE;
            case 'f':
                return Tokens::LITERAL_FALSE;
            case 'n':
                return Tokens::LITERAL_NULL;
            default:
                return Tokens::TOKEN_ILLEGAL;
        }
    }

    JSTaggedValue ParseLiteral(const PandaString &str, Tokens literal_token)
    {
        uint32_t str_len = str.size() - 1;
        uint32_t remaining_length = range_ - current_;
        if (UNLIKELY(remaining_length < str_len)) {
            THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Text in JSON", JSTaggedValue::Exception());
        }

        bool is_match = MatchText(str, str_len);
        if (LIKELY(is_match)) {
            switch (literal_token) {
                case Tokens::LITERAL_TRUE:
                    return JSTaggedValue::True();
                case Tokens::LITERAL_FALSE:
                    return JSTaggedValue::False();
                case Tokens::LITERAL_NULL:
                    return JSTaggedValue::Null();
                default:
                    UNREACHABLE();
            }
        }
        THROW_SYNTAX_ERROR_AND_RETURN(thread_, "Unexpected Text in JSON", JSTaggedValue::Exception());
    }

    bool MatchText(const PandaString &str, uint32_t match_len)
    {
        const char *text = str.c_str();
        uint32_t pos = 1;
        while (pos <= match_len) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (current_[pos] != text[pos]) {
                return false;
            }
            pos++;
        }
        current_ += match_len;
        return true;
    }

    bool ReadNumberRange(bool &is_fast)
    {
        Text current = current_;
        if (*current == '0') {
            is_fast = false;
            current++;
        } else if (*current == '-') {
            current++;
            if (*current == '0') {
                is_fast = false;
                current++;
            }
        }

        while (current != range_) {
            if (IsNumberCharacter(*current)) {
                current++;
                continue;
            }
            if (IsNumberSignalCharacter(*current)) {
                is_fast = false;
                current++;
                continue;
            }
            Text end = current;
            while (current != range_) {
                if (*current == ' ' || *current == '\r' || *current == '\n' || *current == '\t') {
                    current++;
                } else if (*current == ',' || *current == ']' || *current == '}') {
                    end_ = end - 1;
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
        end_ = range_ - 1;
        return true;
    }

    bool IsNumberCharacter(T ch)
    {
        return ch >= '0' && ch <= '9';
    }

    bool IsNumberSignalCharacter(T ch)
    {
        return ch == '.' || ch == 'e' || ch == 'E' || ch == '+' || ch == '-';
    }

    bool IsExponentNumber()
    {
        if (IsNumberCharacter(*current_)) {
            return true;
        }
        if (*current_ == '-' || *current_ == '+') {
            if (current_ == end_) {
                return false;
            }
            current_++;
            if (IsNumberCharacter(*current_)) {
                return true;
            }
        }
        return false;
    }

    bool IsDecimalsLegal(bool &has_exponent)
    {
        if (current_ == end_ && !IsNumberCharacter(*++current_)) {
            return false;
        }

        while (current_ != end_) {
            current_++;
            if (IsNumberCharacter(*current_)) {
                continue;
            }
            if (*current_ == 'e' || *current_ == 'E') {
                if (has_exponent || current_ == end_) {
                    return false;
                }
                current_++;
                if (!IsExponentNumber()) {
                    return false;
                }
                has_exponent = true;
            } else {
                return false;
            }
        }
        return true;
    }

    bool IsExponentLegal(bool &has_exponent)
    {
        if (has_exponent || current_ == end_) {
            return false;
        }
        current_++;
        if (!IsExponentNumber()) {
            return false;
        }
        while (current_ != end_) {
            if (!IsNumberCharacter(*current_)) {
                return false;
            }
            current_++;
        }
        return true;
    }

    bool ReadStringRange(bool &is_fast, bool &is_ascii)
    {
        T c = 0;
        Text current = current_;

        while (current != range_) {
            c = *current;
            if (c == '"') {
                end_ = current;
                return true;
            }
            if (UNLIKELY(c == '\\')) {
                if (*(current + 1) == '"') {
                    current++;
                }
                is_fast = false;
            }
            if (!IsLegalAsciiCharacter(c, is_ascii)) {
                return false;
            }
            current++;
        }
        return false;
    }

    bool ReadAsciiStringRange(bool &is_fast)
    {
        T c = 0;
        Text current = current_;

        while (current != range_) {
            c = *current;
            if (c == '"') {
                end_ = current;
                return true;
            }
            if (UNLIKELY(c == '\\')) {
                if (*(current + 1) == '"') {
                    current++;
                }
                is_fast = false;
            } else if (UNLIKELY(c < CODE_SPACE)) {
                return false;
            }
            current++;
        }
        return false;
    }

    bool IsFastParseString(bool &is_fast, bool &is_ascii)
    {
        Text current = current_;
        while (current != end_) {
            if (!IsLegalAsciiCharacter(*current, is_ascii)) {
                return false;
            }
            if (*current == '\\') {
                is_fast = false;
            }
            current++;
        }
        return true;
    }

    bool IsFastParseAsciiString(bool &is_fast)
    {
        Text current = current_;
        while (current != end_) {
            if (*current < CODE_SPACE) {
                return false;
            }
            if (*current == '\\') {
                is_fast = false;
            }
            current++;
        }
        return true;
    }

    bool ConvertStringUnicode(PandaVector<uint16_t> &vec)
    {
        uint32_t remaining_length = end_ - current_;
        if (remaining_length < UNICODE_DIGIT_LENGTH) {
            return false;
        }
        uint16_t res = 0;
        uint32_t exponent = UNICODE_DIGIT_LENGTH;
        for (uint32_t pos = 0; pos < UNICODE_DIGIT_LENGTH; pos++) {
            current_++;
            exponent--;
            if (*current_ >= '0' && *current_ <= '9') {
                res += (*current_ - '0') * pow(NUMBER_SIXTEEN, exponent);
            } else if (*current_ >= 'a' && *current_ <= 'f') {
                res += (*current_ - 'a' + NUMBER_TEN) * pow(NUMBER_SIXTEEN, exponent);
            } else if (*current_ >= 'A' && *current_ <= 'F') {
                res += (*current_ - 'A' + NUMBER_TEN) * pow(NUMBER_SIXTEEN, exponent);
            } else {
                return false;
            }
        }
        if (res < CODE_SPACE) {
            return false;
        }

        vec.emplace_back(res);

        if (*(current_ + 1) == '\\' && *(current_ + 2) == 'u') {  // 2: next two chars
            current_ += 2;                                        // 2: point moves backwards by two digits
            return ConvertStringUnicode(vec);
        }
        return true;
    }

    bool CheckZeroBeginNumber(bool &has_exponent)
    {
        if (current_++ != end_) {
            if (*current_ == '.') {
                if (!IsDecimalsLegal(has_exponent)) {
                    return false;
                }
            } else if (*current_ == 'e' || *current_ == 'E') {
                if (!IsExponentLegal(has_exponent)) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    bool CheckNonZeroBeginNumber(bool &has_exponent)
    {
        while (current_ != end_) {
            current_++;
            if (IsNumberCharacter(*current_)) {
                continue;
            }
            if (*current_ == '.') {
                if (!IsDecimalsLegal(has_exponent)) {
                    return false;
                }
            } else if (*current_ == 'e' || *current_ == 'E') {
                if (!IsExponentLegal(has_exponent)) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    bool IsLegalAsciiCharacter(T c, bool &is_ascii)
    {
        if (c <= ASCII_END) {
            return c >= CODE_SPACE;
        }
        is_ascii = false;
        return true;
    }

    bool is_ascii_string_ {false};
    Text end_ {nullptr};
    Text current_ {nullptr};
    Text range_ {nullptr};
    JSThread *thread_ {nullptr};
    ObjectFactory *factory_ {nullptr};
    GlobalEnv *env_ {nullptr};
};

class Internalize {
public:
    static JSHandle<JSTaggedValue> InternalizeJsonProperty(JSThread *thread, const JSHandle<JSObject> &holder,
                                                           const JSHandle<JSTaggedValue> &name,
                                                           const JSHandle<JSTaggedValue> &receiver);

private:
    static bool RecurseAndApply(JSThread *thread, const JSHandle<JSObject> &holder, const JSHandle<JSTaggedValue> &name,
                                const JSHandle<JSTaggedValue> &receiver);
};
}  // namespace panda::ecmascript::base

#endif  // ECMASCRIPT_BASE_JSON_PARSE_INL_H
