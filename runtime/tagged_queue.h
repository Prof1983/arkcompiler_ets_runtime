/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_TAGGED_QUEUE_H
#define ECMASCRIPT_TAGGED_QUEUE_H

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript {
class TaggedQueue : public TaggedArray {
public:
    static TaggedQueue *Cast(ObjectHeader *object)
    {
        return reinterpret_cast<TaggedQueue *>(object);
    }

    JSTaggedValue Pop(JSThread *thread);
    static TaggedQueue *Push(const JSThread *thread, const JSHandle<TaggedQueue> &queue,
                             const JSHandle<JSTaggedValue> &value)
    {
        uint32_t capacity = queue->GetCapacity().GetArrayLength();
        if (capacity == 0) {
            // If there is no capacity, directly create a queue whose capacity is MIN_CAPACITY. Add elements.
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            JSHandle<TaggedQueue> new_queue = factory->NewTaggedQueue(MIN_CAPACITY);
            new_queue->Set(thread, 0, value.GetTaggedValue());
            new_queue->SetCapacity(thread, JSTaggedValue(MIN_CAPACITY));
            new_queue->SetStart(thread, JSTaggedValue(0));
            new_queue->SetEnd(thread, JSTaggedValue(1));
            return *new_queue;
        }

        uint32_t start = queue->GetStart().GetArrayLength();
        uint32_t end = queue->GetEnd().GetArrayLength();
        uint32_t size = queue->Size();
        if ((end + 1) % capacity == start) {
            // The original queue is full and needs to be expanded.
            if (capacity == MAX_QUEUE_INDEX) {
                // can't grow anymore
                return *queue;
            }
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            // Grow Array for 1.5 times
            uint32_t new_capacity = capacity + (capacity >> 1U);
            new_capacity = new_capacity < capacity ? MAX_QUEUE_INDEX : new_capacity;
            JSHandle<TaggedQueue> new_queue = factory->NewTaggedQueue(new_capacity);
            uint32_t new_end = 0;
            for (uint32_t i = start; new_end < size; i = (i + 1) % capacity) {
                new_queue->Set(thread, new_end, queue->Get(i));
                new_end++;
            }

            new_queue->SetCapacity(thread, JSTaggedValue(new_capacity));
            new_queue->SetStart(thread, JSTaggedValue(0));
            new_queue->Set(thread, new_end++, value.GetTaggedValue());
            new_queue->SetEnd(thread, JSTaggedValue(new_end));
            return *new_queue;
        }
        queue->Set(thread, end, value.GetTaggedValue());
        queue->SetEnd(thread, JSTaggedValue((end + 1) % capacity));
        return *queue;
    }

    // Only for fixed-length queue, without grow capacity
    static inline void PushFixedQueue(const JSThread *thread, const JSHandle<TaggedQueue> &queue,
                                      const JSHandle<JSTaggedValue> &value)
    {
        uint32_t end = queue->GetEnd().GetArrayLength();
        uint32_t capacity = queue->GetCapacity().GetArrayLength();
        ASSERT(capacity != 0);
        queue->Set(thread, end, value.GetTaggedValue());
        queue->SetEnd(thread, JSTaggedValue((end + 1) % capacity));
    }

    inline bool Empty()
    {
        return GetStart() == GetEnd();
    }

    inline JSTaggedValue Front()
    {
        if (Empty()) {
            return JSTaggedValue::Hole();
        }
        uint32_t start = GetStart().GetArrayLength();
        return JSTaggedValue(Get(start));
    }

    inline JSTaggedValue Back()
    {
        if (Empty()) {
            return JSTaggedValue::Hole();
        }
        return JSTaggedValue(Get(GetEnd().GetArrayLength() - 1));
    }

    inline uint32_t Size()
    {
        uint32_t capacity = GetCapacity().GetArrayLength();
        if (capacity == 0) {
            return 0;
        }
        uint32_t end = GetEnd().GetArrayLength();
        uint32_t start = GetStart().GetArrayLength();
        return (end - start + capacity) % capacity;
    }

    inline JSTaggedValue Get(uint32_t index) const
    {
        return TaggedArray::Get(QueueToArrayIndex(index));
    }

    inline void Set(const JSThread *thread, uint32_t index, JSTaggedValue value)
    {
        return TaggedArray::Set(thread, QueueToArrayIndex(index), value);
    }

    inline JSTaggedValue GetStart() const
    {
        return TaggedArray::Get(START_INDEX);
    }

    static const uint32_t MIN_CAPACITY = 2;
    static const uint32_t CAPACITY_INDEX = 0;
    static const uint32_t START_INDEX = 1;
    static const uint32_t END_INDEX = 2;
    static const uint32_t ELEMENTS_START_INDEX = 3;
    static const uint32_t MAX_QUEUE_INDEX = TaggedArray::MAX_ARRAY_INDEX - ELEMENTS_START_INDEX;

private:
    friend class ObjectFactory;

    inline static constexpr uint32_t QueueToArrayIndex(uint32_t index)
    {
        return index + ELEMENTS_START_INDEX;
    }

    inline void SetCapacity(const JSThread *thread, JSTaggedValue capacity)
    {
        TaggedArray::Set(thread, CAPACITY_INDEX, capacity);
    }

    inline JSTaggedValue GetCapacity() const
    {
        return TaggedArray::Get(CAPACITY_INDEX);
    }

    inline void SetStart(const JSThread *thread, JSTaggedValue start)
    {
        TaggedArray::Set(thread, START_INDEX, start);
    }

    inline void SetEnd(const JSThread *thread, JSTaggedValue end)
    {
        TaggedArray::Set(thread, END_INDEX, end);
    }

    inline JSTaggedValue GetEnd() const
    {
        return TaggedArray::Get(END_INDEX);
    }

    static TaggedQueue *Create(JSThread *thread, uint32_t capacity, JSTaggedValue init_val = JSTaggedValue::Hole());
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_TAGGED_QUEUE_H
