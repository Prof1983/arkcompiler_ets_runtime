/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_OBJECT_FACTORY_INL_H
#define ECMASCRIPT_OBJECT_FACTORY_INL_H

#include "object_factory.h"
#include "plugins/ecmascript/runtime/mem/mem_manager-inl.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/lexical_env.h"

namespace panda::ecmascript {
EcmaString *ObjectFactory::AllocNonMovableStringObject(size_t size)
{
    NewObjectHook();
    return reinterpret_cast<EcmaString *>(heap_helper_.AllocateNonMovableOrHugeObject(string_class_, size));
}

EcmaString *ObjectFactory::AllocStringObject(size_t size)
{
    NewObjectHook();
    return reinterpret_cast<EcmaString *>(heap_helper_.AllocateYoungGenerationOrHugeObject(
        string_class_, size, mem::ObjectAllocatorBase::ObjMemInitPolicy::NO_INIT));
}

JSHandle<JSNativePointer> ObjectFactory::NewJSNativePointer(void *external_pointer, const DeleteEntryPoint &call_back,
                                                            void *data, bool non_movable)
{
    NewObjectHook();
    TaggedObject *header;
    if (non_movable) {
        header = heap_helper_.AllocateNonMovableOrHugeObject(js_native_pointer_class_);
    } else {
        header = heap_helper_.AllocateYoungGenerationOrHugeObject(js_native_pointer_class_);
    }
    JSHandle<JSNativePointer> obj(thread_, header);
    obj->SetExternalPointer(external_pointer);
    obj->SetDeleter(call_back);
    obj->SetData(data);
    return obj;
}

LexicalEnv *ObjectFactory::InlineNewLexicalEnv(int num_slots)
{
    NewObjectHook();
    size_t size = LexicalEnv::ComputeSize(num_slots);
    auto header = heap_helper_.AllocateYoungGenerationOrHugeObject(env_class_, size);
    if (UNLIKELY(header == nullptr)) {
        return nullptr;
    }
    LexicalEnv *array = LexicalEnv::Cast(header);
    array->InitializeWithSpecialValue(JSTaggedValue::Hole(), num_slots + LexicalEnv::RESERVED_ENV_LENGTH);
    return array;
}

template <typename T, typename S>
void ObjectFactory::NewJSIntlIcuData(const JSHandle<T> &obj, const S &icu, const DeleteEntryPoint &callback)
{
    S *icu_point = Runtime::GetCurrent()->GetInternalAllocator()->New<S>(icu);
    ASSERT(icu_point != nullptr);
    JSTaggedValue data = obj->GetIcuField();
    if (data.IsHeapObject() && data.IsJSNativePointer()) {
        JSNativePointer *native = JSNativePointer::Cast(data.GetTaggedObject());
        native->ResetExternalPointer(icu_point);
        return;
    }
    JSHandle<JSNativePointer> pointer = NewJSNativePointer(icu_point, callback, vm_);
    obj->SetIcuField(thread_, pointer.GetTaggedValue());
    // push uint8_t* to ecma array_data_list
    vm_->PushToArrayDataList(*pointer);
}
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_OBJECT_FACTORY_INL_H
