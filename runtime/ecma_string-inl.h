/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_STRING_INL_H
#define ECMASCRIPT_STRING_INL_H

#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/object_factory-inl.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript {
/* static */
inline EcmaString *EcmaString::Cast(ObjectHeader *object)
{
    ASSERT(JSTaggedValue(object).IsString());
    return static_cast<EcmaString *>(object);
}

/* static */
inline const EcmaString *EcmaString::ConstCast(const TaggedObject *object)
{
    ASSERT(JSTaggedValue(object).IsString());
    return static_cast<const EcmaString *>(object);
}

/* static */
inline EcmaString *EcmaString::CreateEmptyString(const EcmaVM *vm)
{
    auto string = vm->GetFactory()->AllocNonMovableStringObject(EcmaString::SIZE);
    string->SetLength(0, GetCompressedStringsEnabled());
    string->SetRawHashcode(0);
    return string;
}

/* static */
inline EcmaString *EcmaString::CreateFromUtf8(const uint8_t *utf8_data, uint32_t utf8_len, const EcmaVM *vm,
                                              bool can_be_compress, panda::SpaceType space_type)
{
    if (utf8_len == 0) {
        return vm->GetFactory()->GetEmptyString().GetObject<EcmaString>();
    }
    EcmaString *string = nullptr;
    if (can_be_compress) {
        string = AllocStringObject(utf8_len, true, vm, space_type);
        ASSERT(string != nullptr);

        if (memcpy_s(string->GetDataUtf8Writable(), utf8_len, utf8_data, utf8_len) != EOK) {
            LOG_ECMA(FATAL) << "memcpy_s failed";
            UNREACHABLE();
        }
    } else {
        auto utf16_len = utf::Utf8ToUtf16Size(utf8_data, utf8_len);
        string = AllocStringObject(utf16_len, false, vm, space_type);
        ASSERT(string != nullptr);

        [[maybe_unused]] auto len =
            utf::ConvertRegionUtf8ToUtf16(utf8_data, string->GetDataUtf16Writable(), utf8_len, utf16_len, 0);
        ASSERT(len == utf16_len);
    }

    return string;
}

inline EcmaString *EcmaString::CreateFromUtf16(const uint16_t *utf16_data, uint32_t utf16_len, const EcmaVM *vm,
                                               bool can_be_compress, panda::SpaceType space_type)
{
    if (utf16_len == 0) {
        return vm->GetFactory()->GetEmptyString().GetObject<EcmaString>();
    }
    auto string = AllocStringObject(utf16_len, can_be_compress, vm, space_type);
    ASSERT(string != nullptr);

    if (can_be_compress) {
        CopyUtf16AsUtf8(utf16_data, string->GetDataUtf8Writable(), utf16_len);
    } else {
        uint32_t len = utf16_len * (sizeof(uint16_t) / sizeof(uint8_t));
        if (memcpy_s(string->GetDataUtf16Writable(), len, utf16_data, len) != EOK) {
            LOG_ECMA(FATAL) << "memcpy_s failed";
            UNREACHABLE();
        }
    }

    return string;
}

template <bool VERIFY>
inline uint16_t EcmaString::At(int32_t index) const
{
    int32_t length = GetLength();
    if (VERIFY) {
        if ((index < 0) || (index >= length)) {
            return 0;
        }
    }
    if (!IsUtf16()) {
        Span<const uint8_t> sp(GetDataUtf8(), length);
        return sp[index];
    }
    Span<const uint16_t> sp(GetDataUtf16(), length);
    return sp[index];
}

/* static */
inline EcmaString *EcmaString::AllocStringObject(size_t length, bool compressed, const EcmaVM *vm,
                                                 panda::SpaceType space_type)
{
    size_t size = compressed ? ComputeSizeUtf8(length) : ComputeSizeUtf16(length);
    auto string = space_type == panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT
                      ? reinterpret_cast<EcmaString *>(vm->GetFactory()->AllocNonMovableStringObject(size))
                      : reinterpret_cast<EcmaString *>(vm->GetFactory()->AllocStringObject(size));
    string->SetLength(length, compressed);
    string->SetRawHashcode(0);
    return reinterpret_cast<EcmaString *>(string);
}

void EcmaString::WriteData(EcmaString *src, uint32_t start, uint32_t dest_size, uint32_t length)
{
    if (IsUtf8()) {
        ASSERT(src->IsUtf8());
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (length != 0 && memcpy_s(GetDataUtf8Writable() + start, dest_size, src->GetDataUtf8(), length) != EOK) {
            LOG_ECMA(FATAL) << "memcpy_s failed";
            UNREACHABLE();
        }
    } else if (src->IsUtf8()) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        Span<uint16_t> to(GetDataUtf16Writable() + start, length);
        Span<const uint8_t> from(src->GetDataUtf8(), length);
        for (uint32_t i = 0; i < length; i++) {
            to[i] = from[i];
        }
    } else {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (length != 0 && memcpy_s(GetDataUtf16Writable() + start, ComputeDataSizeUtf16(dest_size),
                                    src->GetDataUtf16(), ComputeDataSizeUtf16(length)) != EOK) {
            LOG_ECMA(FATAL) << "memcpy_s failed";
            UNREACHABLE();
        }
    }
}

void EcmaString::WriteData(char src, uint32_t start)
{
    if (IsUtf8()) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        *(GetDataUtf8Writable() + start) = src;
    } else {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        *(GetDataUtf16Writable() + start) = src;
    }
}
}  // namespace panda::ecmascript

#endif
