/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_method.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "libpandafile/method_data_accessor-inl.h"
#include "compiler/compiler_options.h"
#include "runtime/include/profiling_gen.h"

namespace panda::ecmascript {
JSMethod::~JSMethod()
{
    mem::InternalAllocator<>::GetInternalAllocatorFromRuntime()->Free(ic_mapping_);
}

// It's not allowed '#' token appear in ECMA function(method) name, which discriminates same names in panda methods.
PandaString JSMethod::ParseFunctionName() const
{
    auto *name = GetStringDataAnnotation(Method::AnnotationField::FUNCTION_NAME).data;
    if (name == nullptr) {
        name = GetName().data;
    }
    return utf::Mutf8AsCString(name);
}

void JSMethod::SetCallTypeFromAnnotation()
{
    const panda_file::File *panda_file = GetPandaFile();
    panda_file::File::EntityId field_id = GetFileId();
    panda_file::MethodDataAccessor mda(*panda_file, field_id);
    mda.EnumerateAnnotations([&](panda_file::File::EntityId annotation_id) {
        panda_file::AnnotationDataAccessor ada(*panda_file, annotation_id);
        auto *annotation_name = reinterpret_cast<const char *>(panda_file->GetStringData(ada.GetClassId()).data);
        if (::strcmp("L_ESCallTypeAnnotation;", annotation_name) == 0) {
            uint32_t elem_count = ada.GetCount();
            for (uint32_t i = 0; i < elem_count; i++) {
                panda_file::AnnotationDataAccessor::Elem adae = ada.GetElement(i);
                auto *elem_name = reinterpret_cast<const char *>(panda_file->GetStringData(adae.GetNameId()).data);
                if (::strcmp("callType", elem_name) == 0) {
                    call_type_ = adae.GetScalarValue().GetValue();
                }
            }
        }
    });
}

void JSMethod::InitProfileVector()
{
    const panda_file::File *panda_file = GetPandaFile();
    panda_file::File::EntityId field_id = GetFileId();
    panda_file::MethodDataAccessor mda(*panda_file, field_id);
    auto prof_size = mda.GetProfileSize();
    bool method_too_big = bytecode_array_size_ > compiler::OPTIONS.GetCompilerMaxBytecodeSize();
#ifndef NDEBUG
    auto max_inst_prof_size = profiling::GetOrderedProfileSizes().back();
    auto max_prof_size = bytecode_array_size_ * max_inst_prof_size;
    LOG_IF(!method_too_big && !prof_size && max_prof_size > panda_file::MethodItem::MAX_PROFILE_SIZE, WARNING, RUNTIME)
        << "Method may be compiled but not profiled because profiling size exceeds limit";
#endif
    if (prof_size && !method_too_big) {
        profile_size_ = prof_size.value();
        size_t size = RoundUp(prof_size.value(), BITS_PER_INTPTR);
        // NOLINTNEXTLINE(modernize-avoid-c-arrays)
        profiling_data_ = MakePandaUnique<uint8_t[]>(size);
        std::memset(profiling_data_.get(), 0, size);
    }
}

JSTaggedValue JSMethod::GetLength() const
{
    JSTaggedValue length = JSTaggedValue::Undefined();
    if (GetPandaFile() != nullptr) {
        length = JSTaggedValue(GetNumericalAnnotation(Method::AnnotationField::FUNCTION_LENGTH));
    }
    return length;
}

std::string JSMethod::GetFullName([[maybe_unused]] bool with_signature) const
{
    auto name = ParseFunctionName();
    if (name.empty()) {
        return std::string(Method::GetFullName());
    }
    return std::string(GetClass() != nullptr ? PandaString(GetClass()->GetName()) : "") + "::" + std::string(name);
}
}  // namespace panda::ecmascript
