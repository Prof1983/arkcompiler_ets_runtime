/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_module.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"

namespace panda::ecmascript {
JSHandle<JSTaggedValue> EcmaModule::GetItem(const JSThread *thread, JSHandle<JSTaggedValue> item_name)
{
    JSHandle<NameDictionary> module_items(thread, NameDictionary::Cast(GetNameDictionary().GetTaggedObject()));
    int entry = module_items->FindEntry(item_name.GetTaggedValue());
    if (entry != -1) {
        return JSHandle<JSTaggedValue>(thread, module_items->GetValue(entry));
    }

    return JSHandle<JSTaggedValue>(thread, JSTaggedValue::Undefined());
}

void EcmaModule::AddItem(const JSThread *thread, JSHandle<EcmaModule> module, JSHandle<JSTaggedValue> item_name,
                         JSHandle<JSTaggedValue> item_value)
{
    JSMutableHandle<JSTaggedValue> data(thread, module->GetNameDictionary());
    if (data->IsUndefined()) {
        data.Update(NameDictionary::Create(thread, DEAULT_DICTIONART_CAPACITY));
    }
    JSHandle<NameDictionary> data_dict = JSHandle<NameDictionary>::Cast(data);
    JSHandle<NameDictionary> new_dict =
        NameDictionary::Put(thread, data_dict, item_name, item_value, PropertyAttributes::Default());
    module->SetNameDictionary(thread, new_dict);
}

void EcmaModule::RemoveItem(const JSThread *thread, JSHandle<EcmaModule> module, JSHandle<JSTaggedValue> item_name)
{
    JSHandle<JSTaggedValue> data(thread, module->GetNameDictionary());
    if (data->IsUndefined()) {
        return;
    }
    JSHandle<NameDictionary> module_items(data);
    int entry = module_items->FindEntry(item_name.GetTaggedValue());
    if (entry != -1) {
        JSHandle<NameDictionary> new_dict = NameDictionary::Remove(thread, module_items, entry);
        module->SetNameDictionary(thread, new_dict);
    }
}

void EcmaModule::CopyModuleInternal(const JSThread *thread, JSHandle<EcmaModule> dst_module,
                                    JSHandle<EcmaModule> src_module)
{
    JSHandle<NameDictionary> module_items(thread,
                                          NameDictionary::Cast(src_module->GetNameDictionary().GetTaggedObject()));
    uint32_t num_all_keys = module_items->EntriesCount();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> all_keys = factory->NewTaggedArray(num_all_keys);
    module_items->GetAllKeys(thread, 0, all_keys.GetObject<TaggedArray>());

    JSMutableHandle<JSTaggedValue> item_name(thread, JSTaggedValue::Undefined());
    JSMutableHandle<JSTaggedValue> item_value(thread, JSTaggedValue::Undefined());
    unsigned int capcity = all_keys->GetLength();
    for (unsigned int i = 0; i < capcity; i++) {
        int entry = module_items->FindEntry(all_keys->Get(i));
        if (entry != -1) {
            item_name.Update(all_keys->Get(i));
            item_value.Update(module_items->GetValue(entry));
            EcmaModule::AddItem(thread, dst_module, item_name, item_value);
        }
    }
}

void EcmaModule::DebugPrint(const JSThread *thread, const PandaString &caller)
{
    JSHandle<NameDictionary> module_items(thread, NameDictionary::Cast(GetNameDictionary().GetTaggedObject()));
    uint32_t num_all_keys = module_items->EntriesCount();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<TaggedArray> all_keys = factory->NewTaggedArray(num_all_keys);
    module_items->GetAllKeys(thread, 0, all_keys.GetObject<TaggedArray>());

    unsigned int capcity = all_keys->GetLength();
    for (unsigned int i = 0; i < capcity; i++) {
        int entry = module_items->FindEntry(all_keys->Get(i));
        if (entry != -1) {
            std::cout << "[" << caller << "]" << std::endl
                      << "--itemName: " << ConvertToPandaString(EcmaString::Cast(all_keys->Get(i).GetTaggedObject()))
                      << std::endl
                      << "--itemValue(ObjRef): 0x" << std::hex << module_items->GetValue(entry).GetRawData()
                      << std::endl;
        }
    }
}

ModuleManager::ModuleManager(EcmaVM *vm) : vm_(vm)
{
    ecma_modules_ = NameDictionary::Create(vm_->GetJSThread(), DEAULT_DICTIONART_CAPACITY).GetTaggedValue();
}

// class ModuleManager
void ModuleManager::AddModule(JSHandle<JSTaggedValue> module_name, JSHandle<JSTaggedValue> module)
{
    JSThread *thread = vm_->GetJSThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<NameDictionary> dict(thread, ecma_modules_);
    ecma_modules_ =
        NameDictionary::Put(thread, dict, module_name, module, PropertyAttributes::Default()).GetTaggedValue();
}

void ModuleManager::RemoveModule(JSHandle<JSTaggedValue> module_name)
{
    JSThread *thread = vm_->GetJSThread();
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<NameDictionary> module_items(thread, ecma_modules_);
    int entry = module_items->FindEntry(module_name.GetTaggedValue());
    if (entry != -1) {
        ecma_modules_ = NameDictionary::Remove(thread, module_items, entry).GetTaggedValue();
    }
}

JSHandle<JSTaggedValue> ModuleManager::GetModule(const JSThread *thread,
                                                 [[maybe_unused]] JSHandle<JSTaggedValue> module_name)
{
    int entry = NameDictionary::Cast(ecma_modules_.GetTaggedObject())->FindEntry(module_name.GetTaggedValue());
    if (entry != -1) {
        return JSHandle<JSTaggedValue>(thread, NameDictionary::Cast(ecma_modules_.GetTaggedObject())->GetValue(entry));
    }
    return thread->GlobalConstants()->GetHandledUndefined();
}

PandaString ModuleManager::GenerateModuleFullPath(const std::string &current_path_file,
                                                  const PandaString &relative_file)
{
    // NOLINTNEXTLINE(abseil-string-find-startswith)
    if (relative_file.find("./") != 0 && relative_file.find("../") != 0) {  // not start with "./" or "../"
        return relative_file;                                               // not relative
    }

    auto slash_pos = current_path_file.find_last_of('/');
    if (slash_pos == std::string::npos) {
        return relative_file;  // no need to process
    }

    auto dot_pos = relative_file.find_last_of('.');
    if (dot_pos == std::string::npos) {
        dot_pos = 0;
    }

    PandaString full_path;
    full_path.append(current_path_file.substr(0, slash_pos + 1));  // 1: with "/"
    full_path.append(relative_file.substr(0, dot_pos));
    full_path.append(".abc");  // ".js" -> ".abc"
    return full_path;
}

const PandaString &ModuleManager::GetCurrentExportModuleName()
{
    return module_names_.back();
}

void ModuleManager::SetCurrentExportModuleName(const std::string_view &module_file)
{
    module_names_.emplace_back(PandaString(module_file));  // xx/xx/x.abc
}

void ModuleManager::RestoreCurrentExportModuleName()
{
    auto s = module_names_.size();
    if (s > 0) {
        module_names_.resize(s - 1);
        return;
    }
    UNREACHABLE();
}

const PandaString &ModuleManager::GetPrevExportModuleName()
{
    static const int MINIMUM_COUNT = 2;
    auto count = module_names_.size();
    ASSERT(count >= MINIMUM_COUNT);
    return module_names_[count - MINIMUM_COUNT];
}

void ModuleManager::AddModuleItem(const JSThread *thread, JSHandle<JSTaggedValue> item_name,
                                  JSHandle<JSTaggedValue> value)
{
    PandaString name_str = GetCurrentExportModuleName();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    JSHandle<EcmaString> module_name = factory->NewFromString(name_str);

    JSHandle<JSTaggedValue> module = GetModule(thread, JSHandle<JSTaggedValue>::Cast(module_name));
    if (module->IsUndefined()) {
        JSHandle<EcmaModule> empty_module = factory->NewEmptyEcmaModule();
        EcmaModule::AddItem(thread, empty_module, item_name, value);

        AddModule(JSHandle<JSTaggedValue>::Cast(module_name), JSHandle<JSTaggedValue>::Cast(empty_module));
    } else {
        EcmaModule::AddItem(thread, JSHandle<EcmaModule>(module), item_name, value);
    }
}

JSHandle<JSTaggedValue> ModuleManager::GetModuleItem(const JSThread *thread, JSHandle<JSTaggedValue> module,
                                                     JSHandle<JSTaggedValue> item_name)
{
    return EcmaModule::Cast(module->GetTaggedObject())->GetItem(thread, item_name);  // Assume the module is exist
}

void ModuleManager::CopyModule(const JSThread *thread, JSHandle<JSTaggedValue> src)
{
    ASSERT(src->IsHeapObject());
    JSHandle<EcmaModule> src_module = JSHandle<EcmaModule>::Cast(src);
    PandaString name_str = GetCurrentExportModuleName();  // Assume the srcModule exist when dstModule Execute

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> module_name = factory->NewFromString(name_str);

    JSHandle<JSTaggedValue> dst_module = GetModule(thread, JSHandle<JSTaggedValue>::Cast(module_name));

    if (dst_module->IsUndefined()) {
        JSHandle<EcmaModule> empty_module = factory->NewEmptyEcmaModule();
        EcmaModule::CopyModuleInternal(thread, empty_module, src_module);

        AddModule(JSHandle<JSTaggedValue>::Cast(module_name), JSHandle<JSTaggedValue>::Cast(empty_module));
    } else {
        EcmaModule::CopyModuleInternal(thread, JSHandle<EcmaModule>(dst_module), src_module);
    }
}

void ModuleManager::DebugPrint([[maybe_unused]] const JSThread *thread, [[maybe_unused]] const PandaString &caller)
{
    std::cout << "ModuleStack:";
    for_each(module_names_.cbegin(), module_names_.cend(), [](const PandaString &s) -> void { std::cout << s << " "; });
    std::cout << "\n";
}
}  // namespace panda::ecmascript
