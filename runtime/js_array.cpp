/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/accessor_data.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_invoker.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "interpreter/fast_runtime_stub-inl.h"

namespace panda::ecmascript {
JSTaggedValue JSArray::LengthGetter([[maybe_unused]] JSThread *thread, const JSHandle<JSObject> &self)
{
    return JSArray::Cast(*self)->GetLength();
}

bool JSArray::LengthSetter(JSThread *thread, const JSHandle<JSObject> &self, const JSHandle<JSTaggedValue> &value,
                           bool may_throw)
{
    uint32_t new_len = 0;
    if (!JSTaggedValue::ToArrayLength(thread, value, &new_len) && may_throw) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "array length must less than 2^32 - 1", false);
    }

    if (!IsArrayLengthWritable(thread, self)) {
        if (may_throw) {
            THROW_TYPE_ERROR_AND_RETURN(thread, "Cannot assign to read only property", false);
        }
        return false;
    }

    uint32_t old_len = JSArray::Cast(*self)->GetArrayLength();
    JSArray::SetCapacity(thread, self, old_len, new_len);
    return true;
}

JSHandle<JSTaggedValue> JSArray::ArrayCreate(JSThread *thread, JSTaggedNumber length, panda::SpaceType space_type)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> array_function = env->GetArrayFunction();
    return JSArray::ArrayCreate(thread, length, array_function, space_type);
}

// 9.4.2.2 ArrayCreate(length, proto)
JSHandle<JSTaggedValue> JSArray::ArrayCreate(JSThread *thread, JSTaggedNumber length,
                                             const JSHandle<JSTaggedValue> &new_target, panda::SpaceType space_type)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // Assert: length is an integer Number ≥ 0.
    ASSERT_PRINT(length.IsInteger() && length.GetNumber() >= 0, "length must be positive integer");
    // 2. If length is −0, let length be +0.
    double array_length = JSTaggedValue::ToInteger(thread, JSHandle<JSTaggedValue>(thread, length)).GetDouble();
    if (array_length > MAX_ARRAY_INDEX) {
        JSHandle<JSTaggedValue> exception(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "array length must less than 2^32 - 1", exception);
    }
    uint32_t normal_array_length = length.ToUint32();

    // 8. Set the [[Prototype]] internal slot of A to proto.
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> array_func = env->GetArrayFunction();
    JSHandle<JSObject> obj =
        factory->NewJSObjectByConstructor(JSHandle<JSFunction>(array_func), new_target, space_type);
    // 9. Set the [[Extensible]] internal slot of A to true.
    obj->GetJSHClass()->SetExtensible(true);

    // 10. Perform OrdinaryDefineOwnProperty(A, "length", PropertyDescriptor{[[Value]]: length, [[Writable]]:
    // true, [[Enumerable]]: false, [[Configurable]]: false}).
    JSArray::Cast(*obj)->SetArrayLength(thread, normal_array_length);

    return JSHandle<JSTaggedValue>(obj);
}

// 9.4.2.3 ArraySpeciesCreate(original_array, length)
JSTaggedValue JSArray::ArraySpeciesCreate(JSThread *thread, const JSHandle<JSObject> &original_array,
                                          JSTaggedNumber length)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    // Assert: length is an integer Number ≥ 0.
    ASSERT_PRINT(length.IsInteger() && length.GetNumber() >= 0, "length must be positive integer");
    // If length is −0, let length be +0.
    double array_length = JSTaggedValue::ToInteger(thread, JSHandle<JSTaggedValue>(thread, length)).GetDouble();
    if (array_length == -0) {
        array_length = +0;
    }
    // Let C be undefined.
    // Let is_array be IsArray(original_array).
    JSHandle<JSTaggedValue> original_value(original_array);
    bool is_array = original_value->IsArray(thread);
    // ReturnIfAbrupt(is_array).
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
    // If is_array is true, then
    JSHandle<JSTaggedValue> constructor(thread, JSTaggedValue::Undefined());
    if (is_array) {
        // Let C be Get(original_array, "constructor").
        auto *hclass = original_array->GetJSHClass();
        if (hclass->IsJSArray() && !hclass->HasConstructor()) {
            return JSArray::ArrayCreate(thread, length).GetTaggedValue();
        }
        JSHandle<JSTaggedValue> constructor_key = global_const->GetHandledConstructorString();
        constructor = JSTaggedValue::GetProperty(thread, original_value, constructor_key).GetValue();
        // ReturnIfAbrupt(C).
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
        // If IsConstructor(C) is true, then
        if (constructor->IsConstructor()) {
            // Let thisRealm be the running execution context’s Realm.
            // Let realmC be GetFunctionRealm(C).
            JSHandle<GlobalEnv> realm_c = JSObject::GetFunctionRealm(thread, constructor);
            // ReturnIfAbrupt(realmC).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // If thisRealm and realmC are not the same Realm Record, then
            if (*realm_c != *env) {
                JSTaggedValue realm_array_constructor = realm_c->GetArrayFunction().GetTaggedValue();
                // If SameValue(C, realmC.[[intrinsics]].[[%Array%]]) is true, let C be undefined.
                if (JSTaggedValue::SameValue(constructor.GetTaggedValue(), realm_array_constructor)) {
                    return JSArray::ArrayCreate(thread, length).GetTaggedValue();
                }
            }
        }

        // If Type(C) is Object, then
        if (constructor->IsECMAObject()) {
            // Let C be Get(C, @@species).
            JSHandle<JSTaggedValue> species_symbol = env->GetSpeciesSymbol();
            constructor = JSTaggedValue::GetProperty(thread, constructor, species_symbol).GetValue();
            // ReturnIfAbrupt(C).
            RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
            // If C is null, let C be undefined.
            if (constructor->IsNull()) {
                return JSArray::ArrayCreate(thread, length).GetTaggedValue();
            }
        }
    }

    // If C is undefined, return ArrayCreate(length).
    if (constructor->IsUndefined()) {
        return JSArray::ArrayCreate(thread, length).GetTaggedValue();
    }
    // If IsConstructor(C) is false, throw a TypeError exception.
    if (!constructor->IsConstructor()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Not a constructor", JSTaggedValue::Exception());
    }
    // Return Construct(C, «length»).
    JSHandle<JSTaggedValue> new_target(thread, JSTaggedValue::Undefined());

    auto info = NewRuntimeCallInfo(thread, constructor, JSTaggedValue::Undefined(), new_target, 1);
    info->SetCallArgs(JSTaggedValue(array_length));
    JSTaggedValue result = JSFunction::Construct(info.Get());

    // NOTEIf original_array was created using the standard built-in Array constructor for
    // a Realm that is not the Realm of the running execution context, then a new Array is
    // created using the Realm of the running execution context. This maintains compatibility
    // with Web browsers that have historically had that behaviour for the Array.prototype methods
    // that now are defined using ArraySpeciesCreate.
    return result;
}

void JSArray::SetCapacity(JSThread *thread, const JSHandle<JSObject> &array, uint32_t old_len, uint32_t new_len)
{
    TaggedArray *element = TaggedArray::Cast(array->GetElements().GetTaggedObject());

    if (element->IsDictionaryMode()) {
        ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
        int32_t num_of_elements = array->GetNumberOfElements();
        uint32_t new_num_of_elements = new_len;
        if (new_len < old_len && num_of_elements != 0) {
            JSHandle<NumberDictionary> dict_handle(thread, element);
            JSHandle<TaggedArray> new_arr = factory->NewTaggedArray(num_of_elements);
            GetAllElementKeys(thread, array, 0, new_arr);
            for (uint32_t i = num_of_elements - 1; i >= new_len; i--) {
                JSTaggedValue value = new_arr->Get(i);
                uint32_t output = 0;
                JSTaggedValue::StringToElementIndex(value, &output);
                JSTaggedValue key(static_cast<int>(output));
                int entry = dict_handle->FindEntry(key);
                uint32_t attr = dict_handle->GetAttributes(entry).GetValue();
                PropertyAttributes prop_attr(attr);
                if (prop_attr.IsConfigurable()) {
                    JSHandle<NumberDictionary> new_dict = NumberDictionary::Remove(thread, dict_handle, entry);
                    array->SetElements(thread, new_dict);
                    if (i == 0) {
                        new_num_of_elements = i;
                        break;
                    }
                } else {
                    new_num_of_elements = i + 1;
                    break;
                }
            }
        }
        JSArray::Cast(*array)->SetArrayLength(thread, new_num_of_elements);
        return;
    }
    uint32_t capacity = element->GetLength();
    if (new_len <= capacity) {
        // judge if need to cut down the array size, else fill the unused tail with holes
        array->FillElementsWithHoles(thread, new_len, old_len < capacity ? old_len : capacity);
    }
    if (JSObject::ShouldTransToDict(old_len, new_len)) {
        JSObject::ElementsToDictionary(thread, array);
    } else if (new_len > capacity) {
        JSObject::GrowElementsCapacity(thread, array, new_len);
    }
    JSArray::Cast(*array)->SetArrayLength(thread, new_len);
}

bool JSArray::ArraySetLength(JSThread *thread, const JSHandle<JSObject> &array, const PropertyDescriptor &desc)
{
    JSHandle<JSTaggedValue> length_key_handle(thread->GlobalConstants()->GetHandledLengthString());

    // 1. If the [[Value]] field of Desc is absent, then
    if (!desc.HasValue()) {
        // 1a. Return OrdinaryDefineOwnProperty(A, "length", Desc).
        return JSObject::OrdinaryDefineOwnProperty(thread, array, length_key_handle, desc);
    }
    // 2. Let new_len_desc be a copy of Desc.
    // (Actual copying is not necessary.)
    PropertyDescriptor new_len_desc = desc;
    // 3. - 7. Convert Desc.[[Value]] to new_len.
    uint32_t new_len = 0;
    if (!JSTaggedValue::ToArrayLength(thread, desc.GetValue(), &new_len)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "array length must less than 2^32 - 1", false);
    }
    // 8. Set new_len_desc.[[Value]] to new_len.
    // (Done below, if needed.)
    // 9. Let old_len_desc be OrdinaryGetOwnProperty(A, "length").
    PropertyDescriptor old_len_desc(thread);
    [[maybe_unused]] bool success = GetOwnProperty(thread, array, length_key_handle, old_len_desc);
    // 10. (Assert)
    ASSERT(success);

    // 11. Let old_len be old_len_desc.[[Value]].
    uint32_t old_len = 0;
    JSTaggedValue::ToArrayLength(thread, old_len_desc.GetValue(), &old_len);
    // 12. If new_len >= old_len, then
    if (new_len >= old_len) {
        // 8. Set new_len_desc.[[Value]] to new_len.
        // 12a. Return OrdinaryDefineOwnProperty(A, "length", new_len_desc).
        new_len_desc.SetValue(JSHandle<JSTaggedValue>(thread, JSTaggedValue(new_len)));
        return JSObject::OrdinaryDefineOwnProperty(thread, array, length_key_handle, new_len_desc);
    }
    // 13. If old_len_desc.[[Writable]] is false, return false.
    if (!old_len_desc.IsWritable() ||
        // Also handle the {configurable: true} case since we later use
        // JSArray::SetLength instead of OrdinaryDefineOwnProperty to change
        // the length, and it doesn't have access to the descriptor anymore.
        new_len_desc.IsConfigurable()) {
        return false;
    }
    // 14. If new_len_desc.[[Writable]] is absent or has the value true,
    // let new_writable be true.
    bool new_writable = false;
    if (!new_len_desc.HasWritable() || new_len_desc.IsWritable()) {
        new_writable = true;
    }
    // 15. Else,
    // 15a. Need to defer setting the [[Writable]] attribute to false in case
    //      any elements cannot be deleted.
    // 15b. Let new_writable be false. (It's initialized as "false" anyway.)
    // 15c. Set new_len_desc.[[Writable]] to true.
    // (Not needed.)

    // Most of steps 16 through 19 is implemented by JSArray::SetCapacity.
    JSArray::SetCapacity(thread, array, old_len, new_len);
    // Steps 19d-ii, 20.
    if (!new_writable) {
        PropertyDescriptor readonly(thread);
        readonly.SetWritable(false);
        success = JSObject::DefineOwnProperty(thread, array, length_key_handle, readonly);
        ASSERT_PRINT(success, "DefineOwnProperty of length must be success here!");
    }

    // Steps 19d-v, 21. Return false if there were non-deletable elements.
    uint32_t array_length = JSArray::Cast(*array)->GetArrayLength();
    return array_length == new_len;
}

bool JSArray::PropertyKeyToArrayIndex(JSThread *thread, const JSHandle<JSTaggedValue> &key, uint32_t *output)
{
    return JSTaggedValue::ToArrayLength(thread, key, output) && *output <= JSArray::MAX_ARRAY_INDEX;
}

// 9.4.2.1 [[DefineOwnProperty]] ( P, Desc)
bool JSArray::DefineOwnProperty(JSThread *thread, const JSHandle<JSObject> &array, const JSHandle<JSTaggedValue> &key,
                                const PropertyDescriptor &desc)
{
    // 1. Assert: IsPropertyKey(P) is true.
    ASSERT_PRINT(JSTaggedValue::IsPropertyKey(key), "Key is not a property key!");
    // 2. If P is "length", then
    if (IsLengthString(thread, key)) {
        // a. Return ArraySetLength(A, Desc).
        return ArraySetLength(thread, array, desc);
    }
    // 3. Else if P is an array index, then
    // already do in step 4.
    // 4. Return OrdinaryDefineOwnProperty(A, P, Desc).
    bool success = JSObject::OrdinaryDefineOwnProperty(thread, array, key, desc);
    if (success) {
        JSTaggedValue constructor_key = thread->GlobalConstants()->GetConstructorString();
        if (key.GetTaggedValue() == constructor_key) {
            array->GetJSHClass()->SetHasConstructor(true);
            return true;
        }
    }
    return success;
}

bool JSArray::DefineOwnProperty(JSThread *thread, const JSHandle<JSObject> &array, uint32_t index,
                                const PropertyDescriptor &desc)
{
    return JSObject::OrdinaryDefineOwnProperty(thread, array, index, desc);
}

bool JSArray::IsLengthString(JSThread *thread, const JSHandle<JSTaggedValue> &key)
{
    return key.GetTaggedValue() == thread->GlobalConstants()->GetLengthString();
}

// ecma6 7.3 Operations on Objects
JSHandle<JSArray> JSArray::CreateArrayFromList(JSThread *thread, const JSHandle<TaggedArray> &elements,
                                               ArraySizeT length)
{
    // Assert: elements is a List whose elements are all ECMAScript language values.
    // 2. Let array be ArrayCreate(0) (see 9.4.2.2).

    // 4. For each element e of elements
    auto env = thread->GetEcmaVM()->GetGlobalEnv();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> array_func = env->GetArrayFunction();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(array_func), array_func);
    obj->GetJSHClass()->SetExtensible(true);
    JSArray::Cast(*obj)->SetArrayLength(thread, length);

    obj->SetElements(thread, elements);

    return JSHandle<JSArray>(obj);
}

JSHandle<JSTaggedValue> JSArray::FastGetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                        uint32_t index)
{
    auto result = FastRuntimeStub::FastGetPropertyByIndex(thread, obj.GetTaggedValue(), index);
    return JSHandle<JSTaggedValue>(thread, result);
}

JSHandle<JSTaggedValue> JSArray::FastGetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                        const JSHandle<JSTaggedValue> &key)
{
    auto result = FastRuntimeStub::FastGetPropertyByValue(thread, obj, key);
    return JSHandle<JSTaggedValue>(thread, result);
}

bool JSArray::FastSetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj, uint32_t index,
                                     const JSHandle<JSTaggedValue> &value)
{
    return FastRuntimeStub::FastSetPropertyByIndex(thread, obj.GetTaggedValue(), index, value.GetTaggedValue());
}

bool JSArray::FastSetPropertyByValue(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                     const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value)
{
    return FastRuntimeStub::FastSetPropertyByValue(thread, obj, key, value);
}
}  // namespace panda::ecmascript
