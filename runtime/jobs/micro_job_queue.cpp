/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/jobs/micro_job_queue.h"

#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/jobs/pending_job.h"
#include "plugins/ecmascript/runtime/js_arguments.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_queue-inl.h"
#include "plugins/ecmascript/runtime/tagged_queue.h"
#include "utils/expected.h"

namespace panda::ecmascript::job {
void MicroJobQueue::EnqueueJob(JSThread *thread, JSHandle<MicroJobQueue> job_queue, QueueType queue_type,
                               const JSHandle<JSFunction> &job, const JSHandle<TaggedArray> &argv)
{
    // 1. Assert: Type(queueName) is String and its value is the name of a Job Queue recognized by this implementation.
    // 2. Assert: job is the name of a Job.
    // 3. Assert: arguments is a List that has the same number of elements as the number of parameters required by job.
    // 4. Let callerContext be the running execution context.
    // 5. Let callerRealm be callerContext’s Realm.
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<PendingJob> pending_job(factory->NewPendingJob(job, argv));
    if (queue_type == QueueType::QUEUE_PROMISE) {
        JSHandle<TaggedQueue> promise_queue(thread, job_queue->GetPromiseJobQueue());
        LOG_ECMA(DEBUG) << "promiseQueue start length: " << promise_queue->Size();
        TaggedQueue *new_promise_queue = TaggedQueue::Push(thread, promise_queue, JSHandle<JSTaggedValue>(pending_job));
        job_queue->SetPromiseJobQueue(thread, JSTaggedValue(new_promise_queue));
        LOG_ECMA(DEBUG) << "promiseQueue end length: " << new_promise_queue->Size();
    } else if (queue_type == QueueType::QUEUE_SCRIPT) {
        JSHandle<TaggedQueue> script_queue(thread, job_queue->GetScriptJobQueue());
        TaggedQueue *new_script_queue = TaggedQueue::Push(thread, script_queue, JSHandle<JSTaggedValue>(pending_job));
        job_queue->SetScriptJobQueue(thread, JSTaggedValue(new_script_queue));
    }
}

void MicroJobQueue::ExecutePendingJob(JSThread *thread, JSHandle<MicroJobQueue> job_queue)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSMutableHandle<TaggedQueue> promise_queue(thread, job_queue->GetPromiseJobQueue());
    JSMutableHandle<PendingJob> pending_job(thread, JSTaggedValue::Undefined());
    while (!promise_queue->Empty()) {
        LOG_ECMA(DEBUG) << "promiseQueue start length: " << promise_queue->Size();
        pending_job.Update(promise_queue->Pop(thread));
        LOG_ECMA(DEBUG) << "promiseQueue end length: " << promise_queue->Size();
        PendingJob::ExecutePendingJob(pending_job, thread);
        if (UNLIKELY(thread->HasPendingException())) {
            return;
        }
        promise_queue.Update(job_queue->GetPromiseJobQueue());
    }

    JSHandle<TaggedQueue> script_queue(thread, job_queue->GetScriptJobQueue());
    while (!script_queue->Empty()) {
        pending_job.Update(script_queue->Pop(thread));
        PendingJob::ExecutePendingJob(pending_job, thread);
        if (UNLIKELY(thread->HasPendingException())) {
            return;
        }
    }
}
}  // namespace panda::ecmascript::job
