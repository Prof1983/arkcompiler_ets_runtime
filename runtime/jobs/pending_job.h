/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JOBS_PENDING_JOB_H
#define ECMASCRIPT_JOBS_PENDING_JOB_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/record.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

namespace panda::ecmascript::job {
class PendingJob final : public Record {
public:
    static PendingJob *Cast(ObjectHeader *object)
    {
        ASSERT(JSTaggedValue(object).IsPendingJob());
        return static_cast<PendingJob *>(object);
    }

    static JSTaggedValue ExecutePendingJob(const JSHandle<PendingJob> &pending_job, JSThread *thread)
    {
        JSHandle<JSTaggedValue> job(thread, pending_job->GetJob());
        ASSERT(job->IsCallable());
        JSHandle<JSTaggedValue> this_value(thread, JSTaggedValue::Undefined());
        JSHandle<TaggedArray> argv(thread, pending_job->GetArguments());
        auto info = NewRuntimeCallInfo(thread, job, this_value, JSTaggedValue::Undefined(), argv->GetLength());
        info->SetCallArg(argv->GetLength(), argv->GetData());
        return JSFunction::Call(info.Get());
    }

    ACCESSORS_BASE(Record)
    ACCESSORS(0, Job)
    ACCESSORS(1, Arguments)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};
}  // namespace panda::ecmascript::job
#endif  // ECMASCRIPT_JOBS_PENDING_JOB_H
