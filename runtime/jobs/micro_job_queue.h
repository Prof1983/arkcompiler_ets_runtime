/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JOBS_MICRO_JOB_QUEUE_H
#define ECMASCRIPT_JOBS_MICRO_JOB_QUEUE_H

#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/jobs/pending_job.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/record.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

namespace panda::ecmascript::job {
enum class QueueType : uint8_t {
    QUEUE_PROMISE,
    QUEUE_SCRIPT,
};

class MicroJobQueue final : public Record {
public:
    static MicroJobQueue *Cast(ObjectHeader *object)
    {
        ASSERT(JSTaggedValue(object).IsMicroJobQueue());
        return static_cast<MicroJobQueue *>(object);
    }

    static void EnqueueJob(JSThread *thread, JSHandle<MicroJobQueue> job_queue, QueueType queue_type,
                           const JSHandle<JSFunction> &job, const JSHandle<TaggedArray> &argv);
    static void ExecutePendingJob(JSThread *thread, JSHandle<MicroJobQueue> job_queue);

    ACCESSORS_BASE(Record)
    ACCESSORS(0, PromiseJobQueue)
    ACCESSORS(1, ScriptJobQueue)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};
}  // namespace panda::ecmascript::job
#endif  // ECMASCRIPT_JOBS_MICRO_JOB_QUEUE_H
