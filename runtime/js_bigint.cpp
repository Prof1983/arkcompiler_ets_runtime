/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_bigint.h"
#include <cstdint>
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/object_factory.h"

namespace panda::ecmascript {
class ObjectFactory;
constexpr char DP[] = "0123456789abcdefghijklmnopqrstuvwxyz";  // NOLINT(modernize-avoid-c-arrays)
static int CharToInt(char c)
{
    int res = 0;
    if (c >= '0' && c <= '9') {
        res = c - '0';
    } else if (c >= 'A' && c <= 'Z') {
        res = c - 'A' + 10;  // NOLINT(readability-magic-numbers) 10:res must Greater than 10.
    } else if (c >= 'a' && c <= 'z') {
        res = c - 'a' + 10;  // NOLINT(readability-magic-numbers) 10:res must Greater than 10
    }
    return res;
}

static std::string Division(std::string &num, uint32_t conversion_to_radix, uint32_t current_radix, uint32_t &remain)
{
    ASSERT(conversion_to_radix != 0);
    uint32_t temp = 0;
    remain = 0;
    for (char &c : num) {
        temp = (current_radix * remain + static_cast<uint32_t>(CharToInt(c)));
        c = DP[temp / conversion_to_radix];
        remain = temp % conversion_to_radix;
    }
    int count = 0;
    while (num[count] == '0') {
        count++;
    }
    return num.substr(count);
}

std::string BigIntHelper::Conversion(const std::string &num, uint32_t conversion_to_radix, uint32_t current_radix)
{
    ASSERT(conversion_to_radix != 0);
    std::string new_num = num;
    std::string res;
    uint32_t remain = 0;
    while (!new_num.empty()) {
        new_num = Division(new_num, conversion_to_radix, current_radix, remain);
        res.insert(res.begin(), DP[remain]);
    }
    return res;
}

JSHandle<BigInt> BigIntHelper::SetBigInt(JSThread *thread, const std::string &num_str, uint32_t current_radix)
{
    int flag = 0;
    if (num_str[0] == '-') {
        flag = 1;
    }

    std::string binary_str;
    if (current_radix != BigInt::BINARY) {
        binary_str = Conversion(num_str.substr(flag), BigInt::BINARY, current_radix);
    } else {
        binary_str = num_str.substr(flag);
    }

    JSHandle<BigInt> big_int;
    size_t binary_str_len = binary_str.size();
    size_t len = binary_str_len / BigInt::DATEBITS;
    size_t mod = binary_str_len % BigInt::DATEBITS;
    int index = 0;
    if (mod == 0) {
        index = static_cast<int>(len - 1);
        big_int = BigInt::CreateBigInt(thread, len);
    } else {
        len++;
        index = static_cast<int>(len - 1);
        big_int = BigInt::CreateBigInt(thread, len);
        uint32_t val = 0;
        for (size_t i = 0; i < mod; ++i) {
            val <<= 1U;
            val |= static_cast<uint32_t>(binary_str[i] - '0');
        }
        BigInt::SetDigit(thread, big_int, index, val);
        index--;
    }
    if (flag == 1) {
        big_int->SetSign(true);
    }
    size_t i = mod;
    while (i < binary_str_len) {
        uint32_t val = 0;
        for (size_t j = 0; j < BigInt::DATEBITS && i < binary_str_len; ++j, ++i) {
            val <<= 1U;
            val |= static_cast<uint32_t>(binary_str[i] - '0');
        }
        BigInt::SetDigit(thread, big_int, index, val);
        index--;
    }
    return BigIntHelper::RightTruncate(thread, big_int);
}

JSHandle<BigInt> BigIntHelper::RightTruncate(JSThread *thread, JSHandle<BigInt> x)
{
    int len = static_cast<int>(x->GetLength());
    ASSERT(len != 0);
    if (len == 1 && x->GetDigit(0) == 0) {
        x->SetSign(false);
        return x;
    }
    int index = len - 1;
    if (x->GetDigit(index) != 0) {
        return x;
    }
    while (index >= 0) {
        if (x->GetDigit(index) != 0) {
            break;
        }
        index--;
    }
    JSHandle<TaggedArray> array(thread, x->GetData());
    if (index == -1) {
        array->Trim(thread, 1);
    } else {
        array->Trim(thread, index + 1);
    }
    if (x->IsZero()) {
        x->SetSign(false);
    }
    return x;
}

std::string BigIntHelper::GetBinary(const JSHandle<BigInt> &big_int)
{
    int index = 0;
    auto len = static_cast<int>(big_int->GetLength());
    int str_len = BigInt::DATEBITS * len;
    std::string res(str_len, '0');
    int str_index = str_len - 1;
    while (index < len) {
        int bity_len = BigInt::DATEBITS;
        uint32_t val = big_int->GetDigit(index);
        while (bity_len-- != 0) {
            res[str_index--] = (val & 1U) + '0';
            val = val >> 1UL;
        }
        index++;
    }
    size_t count = 0;
    size_t res_len = res.size();
    for (size_t i = 0; i < res_len; ++i) {
        if (res[i] != '0') {
            break;
        }
        count++;
    }
    if (count == res_len) {
        return "0";
    }
    return res.substr(count);
}

uint32_t BigInt::GetDigit(uint32_t index) const
{
    TaggedArray *tagged_array = TaggedArray::Cast(GetData().GetTaggedObject());
    JSTaggedValue digit = tagged_array->Get(index);
    return static_cast<uint32_t>(digit.GetInt());
}

void BigInt::SetDigit(JSThread *thread, JSHandle<BigInt> big_int, uint32_t index, uint32_t digit)
{
    JSHandle<TaggedArray> digits_handle(thread, big_int->GetData().GetTaggedObject());
    digits_handle->Set(thread, index, JSTaggedValue(static_cast<int32_t>(digit)));
}

uint32_t BigInt::GetLength() const
{
    TaggedArray *tagged_array = TaggedArray::Cast(GetData().GetTaggedObject());
    return tagged_array->GetLength();
}

JSHandle<BigInt> BigInt::CreateBigInt(JSThread *thread, uint32_t size)
{
    ASSERT(size < MAXSIZE);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<BigInt> big_int = factory->NewBigInt();
    JSHandle<TaggedArray> tagged_array = factory->NewTaggedArray(size);
    big_int->SetData(thread, tagged_array.GetTaggedValue());
    return big_int;
}

// 6.1.6.2.13
bool BigInt::Equal(JSThread *thread, const JSTaggedValue &x, const JSTaggedValue &y)
{
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<BigInt> x_handle(thread, x);
    JSHandle<BigInt> y_handle(thread, y);
    return Equal(x_handle, y_handle);
}

bool BigInt::Equal(const JSHandle<BigInt> &x, const JSHandle<BigInt> &y)
{
    if (x->GetSign() != y->GetSign() || x->GetLength() != y->GetLength()) {
        return false;
    }
    for (uint32_t i = 0; i < x->GetLength(); ++i) {
        if (x->GetDigit(i) != y->GetDigit(i)) {
            return false;
        }
    }
    return true;
}

// 6.1.6.2.14
bool BigInt::SameValue(JSThread *thread, const JSTaggedValue &x, const JSTaggedValue &y)
{
    return Equal(thread, x, y);
}

// 6.1.6.2.15
bool BigInt::SameValueZero(JSThread *thread, const JSTaggedValue &x, const JSTaggedValue &y)
{
    return Equal(thread, x, y);
}

void BigInt::InitializationZero(JSThread *thread, JSHandle<BigInt> big_int)
{
    uint32_t len = big_int->GetLength();
    for (uint32_t i = 0; i < len; ++i) {
        SetDigit(thread, big_int, i, 0);
    }
}

JSHandle<BigInt> BigInt::BitwiseOp(JSThread *thread, Operate op, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    uint32_t max_len = 0;
    uint32_t min_len = 0;
    uint32_t xlen = x->GetLength();
    uint32_t ylen = y->GetLength();
    if (xlen > ylen) {
        max_len = xlen;
        min_len = ylen;
    } else {
        max_len = ylen;
        min_len = xlen;
    }
    JSHandle<BigInt> big_int = BigInt::CreateBigInt(thread, max_len);
    InitializationZero(thread, big_int);
    for (size_t i = 0; i < min_len; ++i) {
        if (op == Operate::OR) {
            SetDigit(thread, big_int, i, x->GetDigit(i) | y->GetDigit(i));
        } else if (op == Operate::AND) {
            SetDigit(thread, big_int, i, x->GetDigit(i) & y->GetDigit(i));
        } else {
            ASSERT(op == Operate::XOR);
            SetDigit(thread, big_int, i, x->GetDigit(i) ^ y->GetDigit(i));
        }
    }
    if (op == Operate::OR || op == Operate::XOR) {
        if (xlen > ylen) {
            for (size_t i = ylen; i < xlen; ++i) {
                SetDigit(thread, big_int, i, x->GetDigit(i));
            }
        } else if (ylen > xlen) {
            for (size_t i = xlen; i < ylen; ++i) {
                SetDigit(thread, big_int, i, y->GetDigit(i));
            }
        }
    }
    return BigIntHelper::RightTruncate(thread, big_int);
}

JSHandle<BigInt> OneIsNegativeAND(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    JSHandle<BigInt> y_val = BigInt::BitwiseSubOne(thread, y, y->GetLength());
    uint32_t x_length = x->GetLength();
    uint32_t y_length = y_val->GetLength();
    uint32_t min_len = x_length;
    if (x_length > y_length) {
        min_len = y_length;
    }
    JSHandle<BigInt> new_big_int = BigInt::CreateBigInt(thread, x_length);
    uint32_t i = 0;
    while (i < min_len) {
        uint32_t res = x->GetDigit(i) & ~(y_val->GetDigit(i));
        BigInt::SetDigit(thread, new_big_int, i, res);
        ++i;
    }
    while (i < x_length) {
        BigInt::SetDigit(thread, new_big_int, i, x->GetDigit(i));
        ++i;
    }
    return BigIntHelper::RightTruncate(thread, new_big_int);
}

// 6.1.6.2.20 BigInt::bitwiseAND ( x, y )
JSHandle<BigInt> BigInt::BitwiseAND(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    if (x->GetSign() && y->GetSign()) {
        // (-x) & (-y) == -(((x-1) | (y-1)) + 1)
        JSHandle<BigInt> x_val = BitwiseSubOne(thread, x, x->GetLength());
        JSHandle<BigInt> y_val = BitwiseSubOne(thread, y, y->GetLength());
        JSHandle<BigInt> temp = BitwiseOp(thread, Operate::OR, x_val, y_val);
        JSHandle<BigInt> res = BitwiseAddOne(thread, temp);
        return res;
    }
    if (x->GetSign() != y->GetSign()) {
        // x & (-y) == x & ~(y-1)
        if (!x->GetSign()) {
            return OneIsNegativeAND(thread, x, y);
        }
        return OneIsNegativeAND(thread, y, x);
    }
    return BitwiseOp(thread, Operate::AND, x, y);
}

JSHandle<BigInt> OneIsNegativeXOR(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    JSHandle<BigInt> y_val = BigInt::BitwiseSubOne(thread, y, y->GetLength());
    JSHandle<BigInt> temp = BigInt::BitwiseOp(thread, Operate::XOR, x, y_val);
    JSHandle<BigInt> res = BigInt::BitwiseAddOne(thread, temp);
    return res;
}

// 6.1.6.2.21 BigInt::bitwiseOR ( x, y )
JSHandle<BigInt> BigInt::BitwiseXOR(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    if (x->GetSign() && y->GetSign()) {
        // (-x) ^ (-y) == (x-1) ^ (y-1)
        JSHandle<BigInt> x_val = BitwiseSubOne(thread, x, x->GetLength());
        JSHandle<BigInt> y_val = BitwiseSubOne(thread, y, y->GetLength());
        return BitwiseOp(thread, Operate::XOR, x_val, y_val);
    }
    if (x->GetSign() != y->GetSign()) {
        // x ^ (-y) == -((x ^ (y-1)) + 1)
        if (!x->GetSign()) {
            return OneIsNegativeXOR(thread, x, y);
        }
        return OneIsNegativeXOR(thread, y, x);
    }
    return BitwiseOp(thread, Operate::XOR, x, y);
}

JSHandle<BigInt> BigInt::BitwiseSubOne(JSThread *thread, JSHandle<BigInt> big_int, uint32_t max_len)
{
    ASSERT(!big_int->IsZero());
    ASSERT(max_len >= big_int->GetLength());

    JSHandle<BigInt> new_big_int = BigInt::CreateBigInt(thread, max_len);

    uint32_t big_int_len = big_int->GetLength();
    uint32_t carry = 1;
    for (uint32_t i = 0; i < big_int_len; i++) {
        uint32_t big_int_carry = 0;
        BigInt::SetDigit(thread, new_big_int, i, BigIntHelper::SubHelper(big_int->GetDigit(i), carry, big_int_carry));
        carry = big_int_carry;
    }
    ASSERT(!carry);
    for (uint32_t i = big_int_len; i < max_len; i++) {
        BigInt::SetDigit(thread, new_big_int, i, carry);
    }
    return BigIntHelper::RightTruncate(thread, new_big_int);
}

JSHandle<BigInt> BigInt::BitwiseAddOne(JSThread *thread, JSHandle<BigInt> big_int)
{
    uint32_t big_int_length = big_int->GetLength();

    bool need_expend = true;
    for (uint32_t i = 0; i < big_int_length; i++) {
        if (std::numeric_limits<uint32_t>::max() != big_int->GetDigit(i)) {
            need_expend = false;
            break;
        }
    }
    uint32_t new_length = big_int_length;
    if (need_expend) {
        new_length += 1;
    }
    JSHandle<BigInt> new_big_int = BigInt::CreateBigInt(thread, new_length);

    uint32_t carry = 1;
    for (uint32_t i = 0; i < big_int_length; i++) {
        uint32_t big_int_carry = 0;
        BigInt::SetDigit(thread, new_big_int, i, BigIntHelper::AddHelper(big_int->GetDigit(i), carry, big_int_carry));
        carry = big_int_carry;
    }
    if (need_expend) {
        BigInt::SetDigit(thread, new_big_int, big_int_length, carry);
    } else {
        ASSERT(!carry);
    }
    new_big_int->SetSign(true);
    return BigIntHelper::RightTruncate(thread, new_big_int);
}

JSHandle<BigInt> OneIsNegativeOR(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    uint32_t x_length = x->GetLength();
    uint32_t max_len = x_length;
    if (max_len < y->GetLength()) {
        max_len = y->GetLength();
    }
    JSHandle<BigInt> y_val = BigInt::BitwiseSubOne(thread, y, max_len);
    uint32_t y_length = y_val->GetLength();
    uint32_t min_len = x_length;
    if (min_len > y_length) {
        min_len = y_length;
    }
    JSHandle<BigInt> new_big_int = BigInt::CreateBigInt(thread, y_length);
    uint32_t i = 0;
    while (i < min_len) {
        uint32_t res = ~(x->GetDigit(i)) & y_val->GetDigit(i);
        BigInt::SetDigit(thread, new_big_int, i, res);
        ++i;
    }
    while (i < y_length) {
        BigInt::SetDigit(thread, new_big_int, i, y_val->GetDigit(i));
        ++i;
    }
    JSHandle<BigInt> temp = BigIntHelper::RightTruncate(thread, new_big_int);
    JSHandle<BigInt> res = BigInt::BitwiseAddOne(thread, temp);
    res->SetSign(true);
    return res;
}

// 6.1.6.2.22 BigInt::bitwiseOR ( x, y )
JSHandle<BigInt> BigInt::BitwiseOR(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    if (x->GetSign() && y->GetSign()) {
        // (-x) | (-y) == -(((x-1) & (y-1)) + 1)
        uint32_t max_len = x->GetLength();
        uint32_t y_len = y->GetLength();
        max_len < y_len ? max_len = y_len : 0;
        JSHandle<BigInt> x_val = BitwiseSubOne(thread, x, max_len);
        JSHandle<BigInt> y_val = BitwiseSubOne(thread, y, y_len);
        JSHandle<BigInt> temp = BitwiseOp(thread, Operate::AND, x_val, y_val);
        JSHandle<BigInt> res = BitwiseAddOne(thread, temp);
        res->SetSign(true);
        return res;
    }
    if (x->GetSign() != y->GetSign()) {
        // x | (-y) == -(((y-1) & ~x) + 1)
        if (!x->GetSign()) {
            return OneIsNegativeOR(thread, x, y);
        }
        return OneIsNegativeOR(thread, y, x);
    }
    return BitwiseOp(thread, Operate::OR, x, y);
}

// 6.1.6.2.23 BigInt::toString ( x )
JSHandle<EcmaString> BigInt::ToString(JSThread *thread, JSHandle<BigInt> big_int, uint32_t conversion_to_radix)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    std::string result = big_int->ToStdString(thread, conversion_to_radix);
    return factory->NewFromStdString(result);
}

std::string BigInt::ToStdString(JSThread *thread, uint32_t conversion_to_radix) const
{
    JSHandle<BigInt> this_handle(thread, JSTaggedValue(this));
    std::string result = BigIntHelper::Conversion(BigIntHelper::GetBinary(this_handle), conversion_to_radix, BINARY);
    if (GetSign()) {
        result = "-" + result;
    }
    return result;
}

JSTaggedValue BigInt::NumberToBigInt(JSThread *thread, JSHandle<JSTaggedValue> number)
{
    if (!number->IsInteger()) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "The number cannot be converted to a BigInt because it is not an integer",
                                     JSTaggedValue::Exception());
    }
    double num = number->GetNumber();
    if (num == 0.0) {
        return Int32ToBigInt(thread, 0).GetTaggedValue();
    }

    // Bit operations must be of integer type
    uint64_t bits = 0;
    if (memcpy_s(&bits, sizeof(bits), &num, sizeof(num)) != EOK) {
        LOG_ECMA(FATAL) << "memcpy_s failed";
        UNREACHABLE();
    }
    // Take out bits 62-52 (11 bits in total) and subtract 1023
    // NOLINTNEXTLINE(readability-magic-numbers)
    uint64_t integer_digits = ((bits >> base::DOUBLE_SIGNIFICAND_SIZE) & 0x7FFU) - base::DOUBLE_EXPONENT_BIAS;
    uint32_t may_need_len = integer_digits / BigInt::DATEBITS + 1;

    JSHandle<BigInt> big_int = CreateBigInt(thread, may_need_len);
    big_int->SetSign(num < 0);
    uint64_t mantissa = (bits & base::DOUBLE_SIGNIFICAND_MASK) | base::DOUBLE_HIDDEN_BIT;
    int mantissa_size = base::DOUBLE_SIGNIFICAND_SIZE;

    uint32_t leftover = 0;
    bool is_first_into = true;
    for (int index = static_cast<int>(may_need_len - 1); index >= 0; --index) {
        uint32_t double_num = 0;
        if (is_first_into) {
            is_first_into = false;
            leftover = mantissa_size - static_cast<int>(integer_digits % BigInt::DATEBITS);
            double_num = static_cast<uint32_t>(mantissa >> leftover);
            mantissa = mantissa << (64U - leftover);  // NOLINT(readability-magic-numbers) 64 : double bits size
            BigInt::SetDigit(thread, big_int, index, double_num);
        } else {
            leftover -= BigInt::DATEBITS;
            double_num = static_cast<uint32_t>(mantissa >> BigInt::DATEBITS);
            mantissa = mantissa << BigInt::DATEBITS;
            BigInt::SetDigit(thread, big_int, index, double_num);
        }
    }
    return BigIntHelper::RightTruncate(thread, big_int).GetTaggedValue();
}

JSHandle<BigInt> BigInt::Int32ToBigInt(JSThread *thread, const int &number)
{
    JSHandle<BigInt> big_int = CreateBigInt(thread, 1);
    uint32_t value = 0;
    bool sign = number < 0;
    if (sign) {
        value = static_cast<uint32_t>(-(number + 1)) + 1;
    } else {
        value = number;
    }
    BigInt::SetDigit(thread, big_int, 0, value);
    big_int->SetSign(sign);
    return big_int;
}

JSHandle<BigInt> BigInt::Int64ToBigInt(JSThread *thread, const int64_t &number)
{
    JSHandle<BigInt> big_int = CreateBigInt(thread, 2);  // 2 : one int64_t bits need two uint32_t bits
    uint64_t value = 0;
    bool sign = number < 0;
    if (sign) {
        value = static_cast<uint64_t>(-(number + 1)) + 1;
    } else {
        value = number;
    }
    auto *addr = reinterpret_cast<uint32_t *>(&value);
    BigInt::SetDigit(thread, big_int, 0, *(addr));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    BigInt::SetDigit(thread, big_int, 1, *(addr + 1));
    big_int->SetSign(sign);
    return BigIntHelper::RightTruncate(thread, big_int);
}

JSHandle<BigInt> BigInt::Uint64ToBigInt(JSThread *thread, const uint64_t &number)
{
    JSHandle<BigInt> big_int = CreateBigInt(thread, 2);  // 2 : one int64_t bits need two uint32_t bits
    const auto *addr = reinterpret_cast<const uint32_t *>(&number);
    BigInt::SetDigit(thread, big_int, 0, *(addr));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    BigInt::SetDigit(thread, big_int, 1, *(addr + 1));
    return BigIntHelper::RightTruncate(thread, big_int);
}

void BigInt::BigIntToInt64(JSThread *thread, JSHandle<JSTaggedValue> big_int, int64_t *c_value, bool *lossless)
{
    ASSERT(c_value != nullptr);
    ASSERT(lossless != nullptr);
    JSHandle<BigInt> big_int64(thread, JSTaggedValue::ToBigInt64(thread, big_int));
    RETURN_IF_ABRUPT_COMPLETION(thread);
    if (Equal(thread, big_int64.GetTaggedValue(), big_int.GetTaggedValue())) {
        *lossless = true;
    }
    auto *addr = reinterpret_cast<uint32_t *>(c_value);
    auto len = static_cast<int>(big_int64->GetLength());
    for (int index = len - 1; index >= 0; --index) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        *(addr + index) = big_int64->GetDigit(index);
    }
    if (big_int64->GetSign()) {
        *c_value = ~(static_cast<uint64_t>(static_cast<int64_t>(*c_value - 1)));
    }
}

void BigInt::BigIntToUint64(JSThread *thread, JSHandle<JSTaggedValue> big_int, uint64_t *c_value, bool *lossless)
{
    ASSERT(c_value != nullptr);
    ASSERT(lossless != nullptr);
    JSHandle<BigInt> big_uint64(thread, JSTaggedValue::ToBigUint64(thread, big_int));
    RETURN_IF_ABRUPT_COMPLETION(thread);
    if (Equal(thread, big_uint64.GetTaggedValue(), big_int.GetTaggedValue())) {
        *lossless = true;
    }
    auto *addr = reinterpret_cast<uint32_t *>(c_value);
    auto len = static_cast<int>(big_uint64->GetLength());
    for (int index = len - 1; index >= 0; --index) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        UnalignedStore(addr + index, big_uint64->GetDigit(index));
    }
}

JSHandle<BigInt> BigInt::CreateBigWords(JSThread *thread, bool sign, uint32_t size, const uint64_t *words)
{
    ASSERT(words != nullptr);
    uint32_t need_len = size * 2;  // 2 : uint64_t size to uint32_t size
    JSHandle<BigInt> big_int = CreateBigInt(thread, need_len);
    const auto *digits = reinterpret_cast<const uint32_t *>(words);
    for (uint32_t index = 0; index < need_len; ++index) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        SetDigit(thread, big_int, index, *(digits + index));
    }
    big_int->SetSign(sign);
    return BigIntHelper::RightTruncate(thread, big_int);
}

JSHandle<BigInt> BigInt::Add(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    bool x_sign_flag = x->GetSign();
    bool y_sign_flag = y->GetSign();
    // x + y == x + y
    // -x + -y == -(x + y)
    if (x_sign_flag == y_sign_flag) {
        return BigIntAdd(thread, x, y, x_sign_flag);
    }
    // x + -y == x - y == -(y - x)
    // -x + y == y - x == -(x - y)
    uint32_t x_length = x->GetLength();
    uint32_t y_length = y->GetLength();
    uint32_t i = x_length - 1;
    auto sub_size = static_cast<int>(x_length - y_length);
    if (sub_size > 0) {
        return BigIntSub(thread, x, y, x_sign_flag);
    }

    if (sub_size == 0) {
        while (i > 0 && x->GetDigit(i) == y->GetDigit(i)) {
            i--;
        }
        if ((x->GetDigit(i) > y->GetDigit(i))) {
            return BigIntSub(thread, x, y, x_sign_flag);
        }

        return BigIntSub(thread, y, x, y_sign_flag);
    }

    return BigIntSub(thread, y, x, y_sign_flag);
}
JSHandle<BigInt> BigInt::Subtract(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    bool x_sign_flag = x->GetSign();
    bool y_sign_flag = y->GetSign();
    if (x_sign_flag != y_sign_flag) {
        // x - (-y) == x + y
        // (-x) - y == -(x + y)
        return BigIntAdd(thread, x, y, x_sign_flag);
    }
    // x - y == -(y - x)
    // (-x) - (-y) == y - x == -(x - y)
    uint32_t x_length = x->GetLength();
    uint32_t y_length = y->GetLength();
    uint32_t i = x_length - 1;
    auto sub_size = static_cast<int>(x_length - y_length);
    if (sub_size > 0) {
        return BigIntSub(thread, x, y, x_sign_flag);
    }

    if (sub_size == 0) {
        while (i > 0 && x->GetDigit(i) == y->GetDigit(i)) {
            i--;
        }

        if ((x->GetDigit(i) > y->GetDigit(i))) {
            return BigIntSub(thread, x, y, x_sign_flag);
        }

        return BigIntSub(thread, y, x, !y_sign_flag);
    }

    return BigIntSub(thread, y, x, !y_sign_flag);
}

JSHandle<BigInt> BigInt::BigIntAdd(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y, bool result_sign)
{
    if (x->GetLength() < y->GetLength()) {
        return BigIntAdd(thread, y, x, result_sign);
    }
    JSHandle<BigInt> big_int = BigInt::CreateBigInt(thread, x->GetLength() + 1);
    uint32_t big_int_carry = 0;
    uint32_t i = 0;
    while (i < y->GetLength()) {
        uint32_t new_big_int_carry = 0;
        uint32_t add_plus = BigIntHelper::AddHelper(x->GetDigit(i), y->GetDigit(i), new_big_int_carry);
        add_plus = BigIntHelper::AddHelper(add_plus, big_int_carry, new_big_int_carry);
        SetDigit(thread, big_int, i, add_plus);
        big_int_carry = new_big_int_carry;
        i++;
    }
    while (i < x->GetLength()) {
        uint32_t new_big_int_carry = 0;
        uint32_t add_plus = BigIntHelper::AddHelper(x->GetDigit(i), big_int_carry, new_big_int_carry);
        SetDigit(thread, big_int, i, add_plus);
        big_int_carry = new_big_int_carry;
        i++;
    }
    SetDigit(thread, big_int, i, big_int_carry);
    big_int->SetSign(result_sign);
    return BigIntHelper::RightTruncate(thread, big_int);
}

inline uint32_t BigIntHelper::AddHelper(uint32_t x, uint32_t y, uint32_t &big_int_carry)
{
    uint32_t add_plus = x + y;
    if (add_plus < x) {
        big_int_carry += 1;
    }
    return add_plus;
}

JSHandle<BigInt> BigInt::BigIntSub(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y, bool result_sign)
{
    JSHandle<BigInt> big_int = BigInt::CreateBigInt(thread, x->GetLength());
    uint32_t big_int_carry = 0;
    uint32_t i = 0;
    while (i < y->GetLength()) {
        uint32_t new_big_int_carry = 0;
        uint32_t minu_sub = BigIntHelper::SubHelper(x->GetDigit(i), y->GetDigit(i), new_big_int_carry);
        minu_sub = BigIntHelper::SubHelper(minu_sub, big_int_carry, new_big_int_carry);
        SetDigit(thread, big_int, i, minu_sub);
        big_int_carry = new_big_int_carry;
        i++;
    }
    while (i < x->GetLength()) {
        uint32_t new_big_int_carry = 0;
        uint32_t minu_sub = BigIntHelper::SubHelper(x->GetDigit(i), big_int_carry, new_big_int_carry);
        SetDigit(thread, big_int, i, minu_sub);
        big_int_carry = new_big_int_carry;
        i++;
    }
    big_int->SetSign(result_sign);
    return BigIntHelper::RightTruncate(thread, big_int);
}

JSHandle<BigInt> BigInt::BigIntAddOne(JSThread *thread, JSHandle<BigInt> x)
{
    JSHandle<BigInt> temp = Int32ToBigInt(thread, 1);
    return Add(thread, x, temp);
}

JSHandle<BigInt> BigInt::BigIntSubOne(JSThread *thread, JSHandle<BigInt> x)
{
    JSHandle<BigInt> temp = Int32ToBigInt(thread, 1);
    return Subtract(thread, x, temp);
}

inline uint32_t BigIntHelper::SubHelper(uint32_t x, uint32_t y, uint32_t &big_int_carry)
{
    uint32_t minu_sub = x - y;
    if (minu_sub > x) {
        big_int_carry += 1;
    }
    return minu_sub;
}

ComparisonResult BigInt::Compare(JSThread *thread, const JSTaggedValue &x, const JSTaggedValue &y)
{
    if (!LessThan(thread, x, y)) {
        if (!LessThan(thread, y, x)) {
            return ComparisonResult::EQUAL;
        }
        return ComparisonResult::GREAT;
    }
    return ComparisonResult::LESS;
}

bool BigInt::LessThan(JSThread *thread, const JSTaggedValue &x, const JSTaggedValue &y)
{
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<BigInt> x_handle(thread, x);
    JSHandle<BigInt> y_handle(thread, y);
    return LessThan(x_handle, y_handle);
}

bool BigInt::LessThan(const JSHandle<BigInt> &x, const JSHandle<BigInt> &y)
{
    bool x_sign_flag = x->GetSign();
    bool y_sign_flag = y->GetSign();
    auto min_size = static_cast<int>(x->GetLength() - y->GetLength());
    uint32_t i = x->GetLength() - 1;
    if (x_sign_flag != y_sign_flag) {
        return x_sign_flag;
    }
    if (min_size != 0 && x_sign_flag) {
        return min_size > 0;
    }
    if (min_size != 0 && !x_sign_flag) {
        return min_size <= 0;
    }
    while (i > 0 && x->GetDigit(i) == y->GetDigit(i)) {
        i--;
    }
    if ((x->GetDigit(i) > y->GetDigit(i))) {
        return x_sign_flag;
    }
    if ((x->GetDigit(i) < y->GetDigit(i))) {
        return !x_sign_flag;
    }
    return false;
}

JSHandle<BigInt> BigInt::SignedRightShift(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    bool x_is_null = (x->GetDigit(0) == 0U);
    bool y_is_null = (y->GetDigit(0) == 0U);
    if (x_is_null || y_is_null) {
        return x;
    }
    if (y->GetSign()) {
        return LeftShiftHelper(thread, x, y);
    }
    return RightShiftHelper(thread, x, y);
}

JSHandle<BigInt> BigInt::RightShiftHelper(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    std::string shift_binay = BigIntHelper::GetBinary(x);
    std::string rev_temp = std::string(shift_binay.rbegin(), shift_binay.rend());
    for (uint32_t i = 0; i < y->GetLength(); i++) {
        rev_temp = rev_temp.erase(0, y->GetDigit(i));
    }
    std::string final_binay = std::string(rev_temp.rbegin(), rev_temp.rend());
    if (final_binay.empty()) {
        final_binay = "0";
    }
    JSHandle<BigInt> big_int = BigIntHelper::SetBigInt(thread, final_binay, BINARY);
    if (x->GetSign()) {
        SetDigit(thread, big_int, 0, big_int->GetDigit(0) + 1);
    }
    big_int->SetSign(x->GetSign());
    return BigIntHelper::RightTruncate(thread, big_int);
}

JSHandle<BigInt> BigInt::LeftShift(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    if (y->GetSign()) {
        return RightShiftHelper(thread, x, y);
    }
    return LeftShiftHelper(thread, x, y);
}

JSHandle<BigInt> BigInt::LeftShiftHelper(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    std::string shift_binary = BigIntHelper::GetBinary(x);
    for (size_t i = 0; i < y->GetLength(); i++) {
        shift_binary = shift_binary.append(y->GetDigit(i), '0');
    }
    JSHandle<BigInt> big_int = BigIntHelper::SetBigInt(thread, shift_binary, BINARY);
    big_int->SetSign(x->GetSign());
    return BigIntHelper::RightTruncate(thread, big_int);
}

JSHandle<BigInt> BigInt::UnsignedRightShift(JSThread *thread, [[maybe_unused]] JSHandle<BigInt> x,
                                            [[maybe_unused]] JSHandle<BigInt> y)
{
    [[maybe_unused]] EcmaHandleScope scope(thread);
    JSHandle<BigInt> exception(thread, JSTaggedValue::Exception());
    THROW_TYPE_ERROR_AND_RETURN(thread, "BigInt have no unsigned right shift, use >> instead", exception);
}

JSHandle<BigInt> BigInt::Copy(JSThread *thread, JSHandle<BigInt> x)
{
    uint32_t len = x->GetLength();
    JSHandle<BigInt> temp = CreateBigInt(thread, len);
    for (uint32_t i = 0; i < len; i++) {
        SetDigit(thread, temp, i, x->GetDigit(i));
    }
    temp->SetSign(x->GetSign());
    return temp;
}

JSHandle<BigInt> BigInt::UnaryMinus(JSThread *thread, JSHandle<BigInt> x)
{
    if (x->IsZero()) {
        return x;
    }
    JSHandle<BigInt> y = Copy(thread, x);
    y->SetSign(!y->GetSign());
    return y;
}

// 6.1.6.2.2   BigInt::bitwiseNOT ( x )
JSHandle<BigInt> BigInt::BitwiseNOT(JSThread *thread, JSHandle<BigInt> x)
{
    // ~(-x) == ~(~(x-1)) == x-1
    // ~x == -x-1 == -(x+1)
    JSHandle<BigInt> result = BigIntAddOne(thread, x);
    if (x->GetSign()) {
        result->SetSign(false);
    } else {
        result->SetSign(true);
    }
    return result;
}

JSHandle<BigInt> BigInt::Exponentiate(JSThread *thread, JSHandle<BigInt> base, JSHandle<BigInt> exponent)
{
    if (exponent->GetSign()) {
        JSHandle<BigInt> big_int(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "Exponent must be positive", big_int);
    }
    ASSERT(exponent->GetLength() == 1);
    if (exponent->IsZero()) {
        return BigIntHelper::SetBigInt(thread, "1");
    }

    if (base->IsZero()) {
        return BigIntHelper::SetBigInt(thread, "0");
    }
    uint32_t e_value = exponent->GetDigit(0);
    if (e_value == 1) {
        return base;
    }
    uint32_t j = exponent->GetDigit(0) - 1;
    std::string a = BigIntHelper::GetBinary(base);
    a = BigIntHelper::Conversion(a, DECIMAL, BINARY);
    std::string b = a;
    for (uint32_t i = 0; i < j; ++i) {
        b = BigIntHelper::MultiplyImpl(b, a);
    }
    if ((exponent->GetDigit(0) & 1U) != 0U) {
        if (base->GetSign()) {
            b = "-" + b;
        }
    }
    return BigIntHelper::SetBigInt(thread, b, DECIMAL);
}

std::string BigIntHelper::MultiplyImpl(std::string &a, std::string &b)
{
    auto size1 = static_cast<int>(a.size());
    auto size2 = static_cast<int>(b.size());
    std::string str(size1 + size2, '0');
    for (int i = size2 - 1; i >= 0; --i) {
        int mulflag = 0;
        int addflag = 0;
        for (int j = size1 - 1; j >= 0; --j) {
            int temp1 = (b[i] - '0') * (a[j] - '0') + mulflag;
            // NOLINTNEXTLINE(readability-magic-numbers)
            mulflag = temp1 / 10;  // 10:help to Remove single digits
            // NOLINTNEXTLINE(readability-magic-numbers)
            temp1 = temp1 % 10;  // 10:help to Take single digit
            int temp2 = str[i + j + 1] - '0' + temp1 + addflag;
            // NOLINTNEXTLINE(readability-magic-numbers)
            str[i + j + 1] = static_cast<int8_t>(temp2 % 10 + '0');  // 2 and 10 and 48 is number
                                                                     // NOLINTNEXTLINE(readability-magic-numbers)
            addflag = temp2 / 10;
        }
        str[i] += static_cast<int8_t>(mulflag + addflag);
    }
    if (str[0] == '0') {
        str = str.substr(1, str.size());
    }
    return str;
}

JSHandle<BigInt> BigInt::Multiply(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    if (x->IsZero()) {
        return x;
    }
    if (y->IsZero()) {
        return y;
    }
    std::string left = BigIntHelper::GetBinary(x);
    std::string right = BigIntHelper::GetBinary(y);
    left = BigIntHelper::Conversion(left, DECIMAL, BINARY);
    right = BigIntHelper::Conversion(right, DECIMAL, BINARY);
    std::string ab = BigIntHelper::MultiplyImpl(left, right);
    if (x->GetSign() != y->GetSign()) {
        ab = "-" + ab;
    }
    return BigIntHelper::SetBigInt(thread, ab, DECIMAL);
}

std::string BigIntHelper::DeZero(std::string &a)
{
    size_t i;
    for (i = 0; i < a.length(); i++) {
        // NOLINTNEXTLINE(readability-magic-numbers)
        if (a.at(i) > 48) {  // 48 is ascill of '0'
            break;
        }
    }
    if (i == a.length()) {
        return "0";
    }
    a.erase(0, i);
    return a;
}

Comparestr BigInt::ComString(std::string &a, std::string &b)
{
    if (a.length() > b.length()) {
        return Comparestr::GREATER;
    }
    if (a.length() < b.length()) {
        return Comparestr::LESS;
    }
    for (size_t i = 0; i < a.length(); i++) {
        if (a.at(i) > b.at(i)) {
            return Comparestr::GREATER;
        }
        if (a.at(i) < b.at(i)) {
            return Comparestr::LESS;
        }
    }
    return Comparestr::EQUAL;
}

std::string BigIntHelper::DevStr(std::string &str_value)
{
    size_t i = 0;
    for (i = 0; i < str_value.length(); i++) {
        // NOLINTNEXTLINE(readability-magic-numbers)
        if (str_value.at(i) >= 48 && str_value.at(i) <= 57) {  // 48 and 57 is '0' and '9'
            // NOLINTNEXTLINE(readability-magic-numbers)
            str_value.at(i) -= 48;  // 48:'0'
        }
        // NOLINTNEXTLINE(readability-magic-numbers)
        if (str_value.at(i) >= 97 && str_value.at(i) <= 122) {  // 97 and 122 is 'a' and 'z'
            // NOLINTNEXTLINE(readability-magic-numbers)
            str_value.at(i) -= 87;  // 87 control result is greater than 10
        }
    }
    return str_value;
}

std::string BigIntHelper::Minus(std::string &a, std::string &b)
{
    a = DeZero(a);
    b = DeZero(b);
    size_t i = 0;
    int j = 0;
    std::string res = "0";
    std::string result1;
    std::string result2;
    std::string dsymbol = "-";
    if (BigInt::ComString(a, b) == Comparestr::EQUAL) {
        return res;
    }
    if (BigInt::ComString(a, b) == Comparestr::GREATER) {
        result1 = a;
        result2 = b;
    }
    if (BigInt::ComString(a, b) == Comparestr::LESS) {
        result1 = b;
        result2 = a;
        j = -1;
    }
    reverse(result1.begin(), result1.end());
    reverse(result2.begin(), result2.end());
    result1 = DevStr(result1);
    result2 = DevStr(result2);
    for (i = 0; i < result2.length(); i++) {
        result1.at(i) = result1.at(i) - result2.at(i);
    }
    for (i = 0; i < result1.length() - 1; i++) {
        if (((int8_t)result1.at(i)) < 0) {
            result1.at(i) += BigInt::DECIMAL;
            result1.at(i + 1)--;
        }
    }
    for (i = result1.length() - 1; i != 0; i--) {
        if (result1.at(i) > 0) {
            break;
        }
    }
    result1.erase(i + 1, result1.length());
    for (i = 0; i < result1.length(); i++) {
        // NOLINTNEXTLINE(readability-magic-numbers)
        if (result1.at(i) >= 10) {  // 10:Hexadecimal a
            // NOLINTNEXTLINE(readability-magic-numbers)
            result1.at(i) += 87;  // 87:control result is greater than 97
        }
        // NOLINTNEXTLINE(readability-magic-numbers)
        if (result1.at(i) < 10) {  // 10: 10:Hexadecimal a
            // NOLINTNEXTLINE(readability-magic-numbers)
            result1.at(i) += 48;  // 48:'0'
        }
    }
    reverse(result1.begin(), result1.end());
    if (j == -1) {
        result1.insert(0, dsymbol);
    }
    return result1;
}

std::string BigIntHelper::Divide(std::string &a, std::string &b)
{
    size_t i = 0;
    size_t j = 0;
    std::string result1;
    std::string result2;
    std::string dsy;
    std::string quotient;
    if (BigInt::ComString(a, b) == Comparestr::EQUAL) {
        return "1";
    }
    if (BigInt::ComString(a, b) == Comparestr::LESS) {
        return "0";
    }
    result1 = DeZero(a);
    result2 = DeZero(b);
    dsy = "";
    quotient = "";
    for (i = 0; i < result1.length(); i++) {
        j = 0;
        dsy += result1.at(i);
        dsy = DeZero(dsy);
        while (BigInt::ComString(dsy, b) == Comparestr::EQUAL || BigInt::ComString(dsy, b) == Comparestr::GREATER) {
            dsy = Minus(dsy, b);
            dsy = DeZero(dsy);
            j++;
        }
        quotient += "0";
        quotient.at(i) = static_cast<int8_t>(j);
    }
    for (i = 0; i < quotient.length(); i++) {
        // NOLINTNEXTLINE(readability-magic-numbers)
        if (quotient.at(i) >= 10) {  // 10 is number
            // NOLINTNEXTLINE(readability-magic-numbers)
            quotient.at(i) += 87;  // 87 is number
        }
        // NOLINTNEXTLINE(readability-magic-numbers)
        if (quotient.at(i) < 10) {  // 10 is number
            // NOLINTNEXTLINE(readability-magic-numbers)
            quotient.at(i) += 48;  // 48 is number
        }
    }
    quotient = DeZero(quotient);
    return quotient;
}

JSHandle<BigInt> BigIntHelper::DivideImpl(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    std::string a = Conversion(GetBinary(x), BigInt::DECIMAL, BigInt::BINARY);
    std::string b = Conversion(GetBinary(y), BigInt::DECIMAL, BigInt::BINARY);
    std::string ab = Divide(a, b);
    if (ab == "0") {
        ab = "0";
    } else if (x->GetSign() != y->GetSign()) {
        ab = "-" + ab;
    }
    return SetBigInt(thread, ab, BigInt::DECIMAL);
}

JSHandle<BigInt> BigInt::Divide(JSThread *thread, JSHandle<BigInt> x, JSHandle<BigInt> y)
{
    if (y->IsZero()) {
        JSHandle<BigInt> big_int(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "Division by zero", big_int);
    }
    return BigIntHelper::DivideImpl(thread, x, y);
}

JSHandle<BigInt> BigInt::Remainder(JSThread *thread, JSHandle<BigInt> n, JSHandle<BigInt> d)
{
    if (d->IsZero()) {
        JSHandle<BigInt> big_int(thread, JSTaggedValue::Exception());
        THROW_RANGE_ERROR_AND_RETURN(thread, "Division by zero", big_int);
    }
    if (n->IsZero()) {
        return n;
    }
    JSHandle<BigInt> q = Divide(thread, n, d);
    JSHandle<BigInt> p = Multiply(thread, q, d);
    return Subtract(thread, n, p);
}

JSHandle<BigInt> BigInt::FloorMod(JSThread *thread, JSHandle<BigInt> left_val, JSHandle<BigInt> right_val)
{
    if (left_val->GetSign()) {
        JSHandle<BigInt> quotient_val = Divide(thread, left_val, right_val);
        if (quotient_val->IsZero()) {
            return Add(thread, left_val, right_val);
        }
        JSHandle<BigInt> num = Multiply(thread, quotient_val, right_val);
        if (Equal(num, left_val)) {
            return Int32ToBigInt(thread, 0);
        }
        return Subtract(thread, left_val, Subtract(thread, num, right_val));
    }
    return Remainder(thread, left_val, right_val);
}

JSTaggedValue BigInt::AsUintN(JSThread *thread, JSTaggedNumber &bits, JSHandle<BigInt> big_int)
{
    uint32_t bit = bits.ToUint32();
    if (bit == 0) {
        return Int32ToBigInt(thread, 0).GetTaggedValue();
    }
    if (big_int->IsZero()) {
        return big_int.GetTaggedValue();
    }
    JSHandle<BigInt> exponent = Int32ToBigInt(thread, bit);
    JSHandle<BigInt> base = Int32ToBigInt(thread, 2);  // 2 : base value
    JSHandle<BigInt> t_value = Exponentiate(thread, base, exponent);
    return FloorMod(thread, big_int, t_value).GetTaggedValue();
}

JSTaggedValue BigInt::AsintN(JSThread *thread, JSTaggedNumber &bits, JSHandle<BigInt> big_int)
{
    uint32_t bit = bits.ToUint32();
    if (bit == 0) {
        return Int32ToBigInt(thread, 0).GetTaggedValue();
    }
    if (big_int->IsZero()) {
        return big_int.GetTaggedValue();
    }
    JSHandle<BigInt> exp = Int32ToBigInt(thread, bit);
    JSHandle<BigInt> exponent = Int32ToBigInt(thread, static_cast<int32_t>(bit - 1));
    JSHandle<BigInt> base = Int32ToBigInt(thread, 2);  // 2 : base value
    JSHandle<BigInt> t_value = Exponentiate(thread, base, exp);
    JSHandle<BigInt> mod_value = FloorMod(thread, big_int, t_value);
    JSHandle<BigInt> res_value = Exponentiate(thread, base, exponent);
    // If mod ≥ 2bits - 1, return ℤ(mod - 2bits); otherwise, return (mod).
    if (LessThan(res_value, mod_value) || Equal(res_value, mod_value)) {
        return Subtract(thread, mod_value, t_value).GetTaggedValue();
    }
    return mod_value.GetTaggedValue();
}

static JSTaggedNumber CalculateNumber(const uint64_t &sign, const uint64_t &mantissa, uint64_t &exponent)
{
    exponent = (exponent + base::DOUBLE_EXPONENT_BIAS) << base::DOUBLE_SIGNIFICAND_SIZE;
    uint64_t double_bit = sign | exponent | mantissa;
    double res = 0;
    if (memcpy_s(&res, sizeof(res), &double_bit, sizeof(double_bit)) != EOK) {
        LOG_ECMA(FATAL) << "memcpy_s failed";
        UNREACHABLE();
    }
    return JSTaggedNumber(res);
}

static JSTaggedNumber Rounding(const uint64_t &sign, uint64_t &mantissa, uint64_t &exponent, bool need_round)
{
    if (need_round || (mantissa & 1U) == 1U) {
        ++mantissa;
        if ((mantissa >> base::DOUBLE_SIGNIFICAND_SIZE) != 0) {
            mantissa = 0;
            exponent++;
            if (exponent > base::DOUBLE_EXPONENT_BIAS) {
                return JSTaggedNumber(sign != 0U ? -base::POSITIVE_INFINITY : base::POSITIVE_INFINITY);
            }
        }
    }
    return CalculateNumber(sign, mantissa, exponent);
}

JSTaggedNumber BigInt::BigIntToNumber(JSHandle<BigInt> big_int)
{
    if (big_int->IsZero()) {
        return JSTaggedNumber(0);
    }
    uint32_t big_int_len = big_int->GetLength();
    uint32_t big_int_head = big_int->GetDigit(big_int_len - 1);
    uint32_t bits = BigInt::DATEBITS;
    uint32_t pre_zero = 0;
    while (bits-- != 0U) {
        if (((big_int_head >> bits) | 0U) != 0) {
            break;
        }
        pre_zero++;
    }
    uint32_t big_int_bit_len = big_int_len * BigInt::DATEBITS - pre_zero;
    // if Significant bits greater than 1024 then double is infinity
    bool big_int_sign = big_int->GetSign();
    if (big_int_bit_len > (base::DOUBLE_EXPONENT_BIAS + 1)) {
        return JSTaggedNumber(big_int_sign ? -base::POSITIVE_INFINITY : base::POSITIVE_INFINITY);
    }
    // NOLINTNEXTLINE(readability-magic-numbers)
    uint64_t sign = big_int_sign ? 1ULL << 63ULL : 0;  // 63 : Set the sign bit of sign to 1
    uint32_t need_move_bit = pre_zero + BigInt::DATEBITS + 1;
    // Align to the most significant bit, then right shift 12 bits so that the head of the mantissa is in place
    // NOLINTNEXTLINE(readability-magic-numbers)
    auto mantissa = (static_cast<uint64_t>(big_int_head) << need_move_bit) >> 12ULL;  // 12 mantissa just has 52 bits
    auto remain_mantissa_bits = static_cast<int32_t>(need_move_bit - 12);  // NOLINT(readability-magic-numbers)
    auto exponent = static_cast<uint64_t>(big_int_bit_len - 1);
    auto index = static_cast<int>(big_int_len - 1);
    uint32_t digit = 0;
    if (index > 0) {
        digit = big_int->GetDigit(--index);
    } else {
        return CalculateNumber(sign, mantissa, exponent);
    }
    // pad unset mantissa
    if (static_cast<uint32_t>(remain_mantissa_bits) >= BigInt::DATEBITS) {
        mantissa |= (static_cast<uint64_t>(digit) << (remain_mantissa_bits - BigInt::DATEBITS));
        remain_mantissa_bits -= BigInt::DATEBITS;
        index--;
    }
    if (remain_mantissa_bits > 0 && index >= 0) {
        digit = big_int->GetDigit(index);
        mantissa |= (static_cast<uint64_t>(digit) >> (BigInt::DATEBITS - remain_mantissa_bits));
        remain_mantissa_bits -= BigInt::DATEBITS;
    }
    // After the mantissa is filled, if the bits of BigInt have not been used up, consider the rounding problem
    // The remaining bits of the current digit
    int remain_digit_bits = 1;
    if (remain_mantissa_bits < 0) {
        remain_digit_bits = -remain_mantissa_bits;
    }
    if (remain_mantissa_bits == 0) {
        if (index == 0) {
            return CalculateNumber(sign, mantissa, exponent);
        }
        digit = big_int->GetDigit(index--);
        remain_digit_bits = BigInt::DATEBITS;
    }
    uint32_t temp = 1ULL << (remain_digit_bits - 1);  // NOLINT(hicpp-signed-bitwise)
    if ((digit & temp) == 0U) {
        return CalculateNumber(sign, mantissa, exponent);
    }
    if ((digit & (temp - 1)) != 0) {
        return Rounding(sign, mantissa, exponent, true);
    }
    while (index > 0) {
        if (big_int->GetDigit(index--) != 0) {
            return Rounding(sign, mantissa, exponent, true);
        }
    }
    return Rounding(sign, mantissa, exponent, false);
}

static int CompareToBitsLen(JSHandle<BigInt> big_int, uint32_t num_bit_len, uint32_t &pre_zero)
{
    uint32_t big_int_len = big_int->GetLength();
    uint32_t big_int_head = big_int->GetDigit(big_int_len - 1);
    uint32_t bits = BigInt::DATEBITS;
    while (bits != 0U) {
        bits--;
        if (((big_int_head >> bits) | 0U) != 0) {
            break;
        }
        pre_zero++;
    }

    uint32_t big_int_bit_len = big_int_len * BigInt::DATEBITS - pre_zero;
    bool big_int_sign = big_int->GetSign();
    if (big_int_bit_len > num_bit_len) {
        return big_int_sign ? 0 : 1;
    }

    if (big_int_bit_len < num_bit_len) {
        return big_int_sign ? 1 : 0;
    }
    return -1;
}

ComparisonResult BigInt::CompareWithNumber(JSHandle<BigInt> big_int, JSHandle<JSTaggedValue> number)
{
    double num = number->GetNumber();
    bool number_sign = num < 0;
    if (std::isnan(num)) {
        return ComparisonResult::UNDEFINED;
    }
    if (!std::isfinite(num)) {
        return (!number_sign ? ComparisonResult::LESS : ComparisonResult::GREAT);
    }
    // Bit operations must be of integer type
    uint64_t bits = 0;
    if (memcpy_s(&bits, sizeof(bits), &num, sizeof(num)) != EOK) {
        LOG_ECMA(FATAL) << "memcpy_s failed";
        UNREACHABLE();
    }
    uint32_t exponential = (bits >> base::DOUBLE_SIGNIFICAND_SIZE) & 0x7FFU;  // NOLINT(readability-magic-numbers)

    // Take out bits 62-52 (11 bits in total) and subtract 1023
    auto integer_digits = static_cast<int32_t>(exponential - base::DOUBLE_EXPONENT_BIAS);
    uint64_t mantissa = (bits & base::DOUBLE_SIGNIFICAND_MASK) | base::DOUBLE_HIDDEN_BIT;
    bool big_int_sign = big_int->GetSign();
    // Handling the opposite sign
    if (!number_sign && big_int_sign) {
        return ComparisonResult::LESS;
    }
    if (number_sign && !big_int_sign) {
        return ComparisonResult::GREAT;
    }
    if (big_int->IsZero() && num == 0.0) {
        return ComparisonResult::EQUAL;
    }
    if (big_int->IsZero() && num > 0) {
        return ComparisonResult::LESS;
    }

    if (integer_digits < 0) {
        return big_int_sign ? ComparisonResult::LESS : ComparisonResult::GREAT;
    }

    // Compare the significant bits of BigInt with the significant integer bits of double
    uint32_t pre_zero = 0;
    int res = CompareToBitsLen(big_int, integer_digits + 1, pre_zero);
    if (res == 0) {
        return ComparisonResult::LESS;
    }
    if (res == 1) {
        return ComparisonResult::GREAT;
    }
    int mantissa_size = base::DOUBLE_SIGNIFICAND_SIZE;  // mantissaSize
    uint32_t big_int_len = big_int->GetLength();
    uint32_t leftover = 0;
    bool is_first_into = true;
    for (int index = static_cast<int>(big_int_len - 1); index >= 0; --index) {
        uint32_t double_num = 0;
        uint32_t big_int_num = big_int->GetDigit(index);
        if (is_first_into) {
            is_first_into = false;
            leftover = mantissa_size - BigInt::DATEBITS + pre_zero + 1;
            double_num = static_cast<uint32_t>(mantissa >> leftover);
            mantissa = mantissa << (64U - leftover);  // NOLINT(readability-magic-numbers) 64 : double bits
            if (big_int_num > double_num) {
                return big_int_sign ? ComparisonResult::LESS : ComparisonResult::GREAT;
            }
            if (big_int_num < double_num) {
                return big_int_sign ? ComparisonResult::GREAT : ComparisonResult::LESS;
            }
        } else {
            leftover -= BigInt::DATEBITS;
            double_num = static_cast<uint32_t>(mantissa >> BigInt::DATEBITS);
            mantissa = mantissa << BigInt::DATEBITS;
            if (big_int_num > double_num) {
                return big_int_sign ? ComparisonResult::LESS : ComparisonResult::GREAT;
            }
            if (big_int_num < double_num) {
                return big_int_sign ? ComparisonResult::GREAT : ComparisonResult::LESS;
            }
            leftover -= BigInt::DATEBITS;
        }
    }

    if (mantissa != 0) {
        ASSERT(leftover > 0);
        return big_int_sign ? ComparisonResult::GREAT : ComparisonResult::LESS;
    }
    return ComparisonResult::EQUAL;
}
}  // namespace panda::ecmascript
