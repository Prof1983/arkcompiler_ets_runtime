/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef ECMASCRIPT_ECMA_GLOABL_STORAGE_INL_H
#define ECMASCRIPT_ECMA_GLOABL_STORAGE_INL_H

#include "plugins/ecmascript/runtime/ecma_global_storage.h"

namespace panda::ecmascript {
inline EcmaGlobalStorage::NodeList *EcmaGlobalStorage::NodeList::NodeToNodeList(EcmaGlobalStorage::Node *node)
{
    uintptr_t ptr = ToUintPtr(node) - node->GetIndex() * sizeof(EcmaGlobalStorage::Node);
    return reinterpret_cast<NodeList *>(ptr);
}

EcmaGlobalStorage::Node *EcmaGlobalStorage::NodeList::NewNode(JSTaggedType value)
{
    if (IsFull()) {
        return nullptr;
    }
    Node *node = &node_list_[index_++];
    node->SetPrev(nullptr);
    node->SetNext(used_list_);
    node->SetObject(value);
    node->SetFree(false);

    if (used_list_ != nullptr) {
        used_list_->SetPrev(node);
    }
    used_list_ = node;
    return node;
}

void EcmaGlobalStorage::NodeList::FreeNode(EcmaGlobalStorage::Node *node)
{
    if (node->GetPrev() != nullptr) {
        node->GetPrev()->SetNext(node->GetNext());
    }
    if (node->GetNext() != nullptr) {
        node->GetNext()->SetPrev(node->GetPrev());
    }
    if (node == used_list_) {
        used_list_ = node->GetNext();
    }
    node->SetPrev(nullptr);
    node->SetNext(free_list_);
    node->SetObject(JSTaggedValue::Undefined().GetRawData());
    node->SetFree(true);

    if (free_list_ != nullptr) {
        free_list_->SetPrev(node);
    }
    free_list_ = node;
}

EcmaGlobalStorage::Node *EcmaGlobalStorage::NodeList::GetFreeNode(JSTaggedType value)
{
    Node *node = free_list_;
    if (node != nullptr) {
        free_list_ = node->GetNext();

        node->SetPrev(nullptr);
        node->SetNext(used_list_);
        node->SetObject(value);
        node->SetFree(false);

        if (used_list_ != nullptr) {
            used_list_->SetPrev(node);
        }
        used_list_ = node;
    }
    return node;
}

void EcmaGlobalStorage::NodeList::LinkTo(NodeList *prev)
{
    next_ = nullptr;
    prev_ = prev;
    prev_->next_ = this;
}

void EcmaGlobalStorage::NodeList::RemoveList()
{
    if (next_ != nullptr) {
        next_->SetPrev(prev_);
    }
    if (prev_ != nullptr) {
        prev_->SetNext(next_);
    }
    if (free_next_ != nullptr) {
        free_next_->SetFreePrev(free_prev_);
    }
    if (free_prev_ != nullptr) {
        free_prev_->SetFreeNext(free_next_);
    }
}

uintptr_t EcmaGlobalStorage::NewGlobalHandleImplement(NodeList **storage, NodeList **free_list, bool is_weak,
                                                      JSTaggedType value)
{
    auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
    if (*free_list == nullptr) {
        if ((*storage)->IsFull()) {
            // alloc new block
            auto block = allocator->New<NodeList>(is_weak);
            block->LinkTo(*storage);
            *storage = block;
        }
        Node *node = (*storage)->NewNode(value);
        ASSERT(node != nullptr);
        return node->GetObjectAddress();
    }

    // use node in block first
    Node *node = (*storage)->NewNode(value);
    if (node != nullptr) {
        return node->GetObjectAddress();
    }

    // use free_list node
    node = (*free_list)->GetFreeNode(value);
    ASSERT(node != nullptr);
    if (!(*free_list)->HasFreeNode()) {
        auto next = (*free_list)->GetFreeNext();
        (*free_list)->SetFreeNext(nullptr);
        (*free_list)->SetFreePrev(nullptr);
        if (next != nullptr) {
            next->SetFreePrev(nullptr);
        }
        *free_list = next;
    }
    return node->GetObjectAddress();
}

inline uintptr_t EcmaGlobalStorage::NewGlobalHandle(JSTaggedType value)
{
    return NewGlobalHandleImplement(&last_global_nodes_, &free_list_nodes_, false, value);
}

inline void EcmaGlobalStorage::DisposeGlobalHandle(uintptr_t node_addr)
{
    auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
    Node *node = reinterpret_cast<Node *>(node_addr);
    if (node->IsFree()) {
        return;
    }
    NodeList *list = NodeList::NodeToNodeList(node);
    list->FreeNode(node);

    // If NodeList has no usage node, then delete NodeList
    NodeList **free_list = nullptr;
    NodeList **top = nullptr;
    NodeList **last = nullptr;
    if (list->IsWeak()) {
        free_list = &weak_free_list_nodes_;
        top = &top_weak_global_nodes_;
        last = &last_weak_global_nodes_;
    } else {
        free_list = &free_list_nodes_;
        top = &top_global_nodes_;
        last = &last_global_nodes_;
    }
    if (!list->HasUsagedNode() && (*top != *last)) {
        list->RemoveList();
        if (*free_list == list) {
            *free_list = list->GetNext();
        }
        if (*top == list) {
            *top = list->GetNext();
        }
        if (*last == list) {
            *last = list->GetPrev();
        }
        allocator->Delete(list);
    } else {
        // Add to free_list
        if (list != *free_list && list->GetFreeNext() == nullptr && list->GetFreePrev() == nullptr) {
            list->SetFreeNext(*free_list);
            if (*free_list != nullptr) {
                (*free_list)->SetFreePrev(list);
            }
            *free_list = list;
        }
    }
}

inline uintptr_t EcmaGlobalStorage::SetWeak(uintptr_t node_addr)
{
    auto value = reinterpret_cast<Node *>(node_addr)->GetObject();
    DisposeGlobalHandle(node_addr);
    return NewGlobalHandleImplement(&last_weak_global_nodes_, &weak_free_list_nodes_, true, value);
}

inline bool EcmaGlobalStorage::IsWeak(uintptr_t addr) const
{
    Node *node = reinterpret_cast<Node *>(addr);
    NodeList *list = NodeList::NodeToNodeList(node);
    return list->IsWeak();
}
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_ECMA_GLOABL_STORAGE_INL_H
