/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_TAGGED_HASH_TABLE_INL_H
#define ECMASCRIPT_TAGGED_HASH_TABLE_INL_H

#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/tagged_hash_table.h"

namespace panda::ecmascript {
template <typename Derived>
int TaggedHashTable<Derived>::EntriesCount() const
{
    return Get(NUMBER_OF_ENTRIES_INDEX).GetInt();
}

template <typename Derived>
int TaggedHashTable<Derived>::HoleEntriesCount() const
{
    return Get(NUMBER_OF_HOLE_ENTRIES_INDEX).GetInt();
}

template <typename Derived>
int TaggedHashTable<Derived>::Size() const
{
    return Get(SIZE_INDEX).GetInt();
}

template <typename Derived>
void TaggedHashTable<Derived>::IncreaseEntries(const JSThread *thread)
{
    SetEntriesCount(thread, EntriesCount() + 1);
}

template <typename Derived>
void TaggedHashTable<Derived>::IncreaseHoleEntriesCount(const JSThread *thread, int number)
{
    SetEntriesCount(thread, EntriesCount() - number);
    SetHoleEntriesCount(thread, HoleEntriesCount() + number);
}

template <typename Derived>
void TaggedHashTable<Derived>::SetEntriesCount(const JSThread *thread, int nof)
{
    Set(thread, NUMBER_OF_ENTRIES_INDEX, JSTaggedValue(nof));
}

template <typename Derived>
void TaggedHashTable<Derived>::SetHoleEntriesCount(const JSThread *thread, int nod)
{
    Set(thread, NUMBER_OF_HOLE_ENTRIES_INDEX, JSTaggedValue(nod));
}

template <typename Derived>
void TaggedHashTable<Derived>::SetHashTableSize(const JSThread *thread, int size)
{
    Set(thread, SIZE_INDEX, JSTaggedValue(size));
}

template <typename Derived>
void TaggedHashTable<Derived>::GetAllKeys(const JSThread *thread, int offset, TaggedArray *key_array) const
{
    ASSERT_PRINT(offset + EntriesCount() <= static_cast<int>(key_array->GetLength()),
                 "key_array size is not enough for dictionary");
    int array_index = 0;
    int size = Size();
    for (int hash_index = 0; hash_index < size; hash_index++) {
        JSTaggedValue key = GetKey(hash_index);
        if (!key.IsUndefined() && !key.IsHole()) {
            key_array->Set(thread, array_index + offset, key);
            array_index++;
        }
    }
}

// Find entry for key otherwise return -1.
template <typename Derived>
int TaggedHashTable<Derived>::FindEntry(const JSTaggedValue &key)
{
    size_t size = Size();
    int count = 1;
    JSTaggedValue key_value;
    int hash = Derived::Hash(key);

    for (int entry = GetFirstPosition(hash, size);; entry = GetNextPosition(entry, count++, size)) {
        key_value = GetKey(entry);
        if (key_value.IsHole()) {
            continue;
        }
        if (key_value.IsUndefined()) {
            return -1;
        }
        if (Derived::IsMatch(key, key_value)) {
            return entry;
        }
    }
    return -1;
}

// static
template <typename Derived>
int TaggedHashTable<Derived>::RecalculateTableSize(int current_size, int at_least_size)
{
    // When the filled entries is greater than a quart of current_size
    // it need not to shrink
    if (at_least_size > (current_size / 4)) {  // 4 : quarter
        return current_size;
    }
    // Recalculate table size
    int new_size = ComputeHashTableSize(at_least_size);
    ASSERT_PRINT(new_size > at_least_size, "new size must greater than at_least_size");
    // Don't go lower than room for MIN_SHRINK_SIZE elements.
    if (new_size < MIN_SHRINK_SIZE) {
        return current_size;
    }
    return new_size;
}

// static
template <typename Derived>
JSHandle<Derived> TaggedHashTable<Derived>::Shrink(const JSThread *thread, const JSHandle<Derived> &table,
                                                   int additional_size)
{
    int new_size = RecalculateTableSize(table->Size(), table->EntriesCount() + additional_size);
    if (new_size == table->Size()) {
        return table;
    }

    JSHandle<Derived> new_table = TaggedHashTable::Create(thread, new_size);
    if (new_table.IsEmpty()) {
        return table;
    }

    table->Rehash(thread, *new_table);
    return new_table;
}

template <typename Derived>
bool TaggedHashTable<Derived>::IsNeedGrowHashTable(int num_of_add_entries)
{
    int entries_count = EntriesCount();
    int num_of_del_entries = HoleEntriesCount();
    int current_size = Size();
    int number_filled = entries_count + num_of_add_entries;
    // needn't to grow table:
    //   1. after adding number entries, table have half free entries.
    //   2. deleted entries are less than half of the free entries.
    const int half_free = 2;
    if ((number_filled < current_size) && ((num_of_del_entries <= (current_size - number_filled) / half_free))) {
        int needed_free = number_filled / half_free;
        if (number_filled + needed_free <= current_size) {
            return false;
        }
    }
    return true;
}

template <typename Derived>
void TaggedHashTable<Derived>::AddElement(const JSThread *thread, int entry, const JSHandle<JSTaggedValue> &key,
                                          const JSHandle<JSTaggedValue> &value)
{
    this->SetKey(thread, entry, key.GetTaggedValue());
    this->SetValue(thread, entry, value.GetTaggedValue());
    this->IncreaseEntries(thread);
}

template <typename Derived>
void TaggedHashTable<Derived>::RemoveElement(const JSThread *thread, int entry)
{
    JSTaggedValue default_value(JSTaggedValue::Hole());
    this->SetKey(thread, entry, default_value);
    this->SetValue(thread, entry, default_value);
    this->IncreaseHoleEntriesCount(thread);
}

template <typename Derived>
JSHandle<Derived> TaggedHashTable<Derived>::Insert(const JSThread *thread, JSHandle<Derived> &table,
                                                   const JSHandle<JSTaggedValue> &key,
                                                   const JSHandle<JSTaggedValue> &value)
{
    // Make sure the key object has an identity hash code.
    int hash = Derived::Hash(key.GetTaggedValue());
    int entry = table->FindEntry(key.GetTaggedValue());
    if (entry != -1) {
        table->SetValue(thread, entry, value.GetTaggedValue());
        return table;
    }

    JSHandle<Derived> new_table = GrowHashTable(thread, table);
    new_table->AddElement(thread, new_table->FindInsertIndex(hash), key, value);
    return new_table;
}

template <typename Derived>
JSHandle<Derived> TaggedHashTable<Derived>::Remove(const JSThread *thread, JSHandle<Derived> &table,
                                                   const JSHandle<JSTaggedValue> &key)
{
    int entry = table->FindEntry(key.GetTaggedValue());
    if (entry == -1) {
        return table;
    }

    table->RemoveElement(thread, entry);
    return Derived::Shrink(thread, *table);
}

template <typename Derived>
void TaggedHashTable<Derived>::Rehash(const JSThread *thread, Derived *new_table)
{
    if ((new_table == nullptr) || (new_table->Size() < EntriesCount())) {
        return;
    }
    int current_size = this->Size();
    // Rehash elements to new table
    for (int i = 0; i < current_size; i++) {
        int from_index = Derived::GetKeyIndex(i);
        JSTaggedValue k = this->GetKey(i);
        if (!IsKey(k)) {
            continue;
        }
        int hash = Derived::Hash(k);
        int insertion_index = Derived::GetKeyIndex(new_table->FindInsertIndex(hash));
        JSTaggedValue tv = Get(from_index);
        new_table->Set(thread, insertion_index, tv);
        for (int j = 1; j < Derived::GetEntrySize(); j++) {
            tv = Get(from_index + j);
            new_table->Set(thread, insertion_index + j, tv);
        }
    }
    new_table->SetEntriesCount(thread, EntriesCount());
    new_table->SetHoleEntriesCount(thread, 0);
}

// static
template <typename Derived>
int TaggedHashTable<Derived>::ComputeHashTableSize(uint32_t at_least_size)
{
    //  increase size for hash-collision
    uint32_t raw_size = at_least_size + (at_least_size >> 1UL);
    int new_size = static_cast<int>(helpers::math::GetPowerOfTwoValue32(raw_size));
    return (new_size > MIN_SIZE) ? new_size : MIN_SIZE;
}

template <typename Derived>
JSHandle<Derived> TaggedHashTable<Derived>::GrowHashTable(const JSThread *thread, const JSHandle<Derived> &table,
                                                          int num_of_added_elements)
{
    if (!table->IsNeedGrowHashTable(num_of_added_elements)) {
        return table;
    }
    int new_size = ComputeHashTableSize(table->Size() + num_of_added_elements);
    int length = Derived::GetEntryIndex(new_size);
    JSHandle<Derived> new_table(thread->GetEcmaVM()->GetFactory()->NewDictionaryArray(length));
    new_table->SetHashTableSize(thread, new_size);
    table->Rehash(thread, *new_table);
    return new_table;
}

template <typename Derived>
JSHandle<Derived> TaggedHashTable<Derived>::Create(const JSThread *thread, int entries_count)
{
    ASSERT_PRINT((entries_count > 0), "the size must be greater than zero");
    auto size = static_cast<uint32_t>(entries_count);
    ASSERT_PRINT(helpers::math::IsPowerOfTwo(static_cast<uint32_t>(entries_count)), "the size must be power of two");

    int length = Derived::GetEntryIndex(entries_count);

    JSHandle<Derived> table(thread->GetEcmaVM()->GetFactory()->NewDictionaryArray(length));
    table->SetEntriesCount(thread, 0);
    table->SetHoleEntriesCount(thread, 0);
    table->SetHashTableSize(thread, size);
    return table;
}

template <typename Derived>
void TaggedHashTable<Derived>::SetKey(const JSThread *thread, int entry, const JSTaggedValue &key)
{
    int index = Derived::GetKeyIndex(entry);
    if (UNLIKELY(index < 0 || index > static_cast<int>(GetLength()))) {
        return;
    }
    Set(thread, index, key);
}

template <typename Derived>
JSTaggedValue TaggedHashTable<Derived>::GetKey(int entry) const
{
    int index = Derived::GetKeyIndex(entry);
    if (UNLIKELY((index < 0 || index > static_cast<int>(GetLength())))) {
        return JSTaggedValue::Undefined();
    }
    return Get(index);
}

template <typename Derived>
void TaggedHashTable<Derived>::SetValue(const JSThread *thread, int entry, const JSTaggedValue &value)
{
    int index = Derived::GetValueIndex(entry);
    if (UNLIKELY((index < 0 || index > static_cast<int>(GetLength())))) {
        return;
    }
    Set(thread, index, value);
}

template <typename Derived>
JSTaggedValue TaggedHashTable<Derived>::GetValue(int entry) const
{
    int index = Derived::GetValueIndex(entry);
    if (UNLIKELY((index < 0 || index > static_cast<int>(GetLength())))) {
        return JSTaggedValue::Undefined();
    }
    return Get(index);
}

template <typename Derived>
int TaggedHashTable<Derived>::FindInsertIndex(int hash)
{
    int size = Size();
    int count = 1;
    // GrowHashTable will guarantee the hash table is never full.
    for (int entry = GetFirstPosition(hash, size);; entry = GetNextPosition(entry, count++, size)) {
        if (!IsKey(GetKey(entry))) {
            return entry;
        }
    }
}

template <typename Derived>
JSHandle<Derived> OrderTaggedHashTable<Derived>::Create(const JSThread *thread, int number_of_elements)
{
    JSHandle<Derived> dict = HashTableT::Create(thread, number_of_elements);
    dict->SetNextEnumerationIndex(thread, PropertyAttributes::INTIAL_PROPERTY_INDEX);
    return dict;
}

template <typename Derived>
JSHandle<Derived> OrderTaggedHashTable<Derived>::PutIfAbsent(const JSThread *thread, const JSHandle<Derived> &table,
                                                             const JSHandle<JSTaggedValue> &key,
                                                             const JSHandle<JSTaggedValue> &value,
                                                             const PropertyAttributes &meta_data)
{
    int hash = Derived::Hash(key.GetTaggedValue());

    /* no need to add key if exist */
    int entry = table->FindEntry(key.GetTaggedValue());
    if (entry != -1) {
        return table;
    }
    int enum_index = table->NextEnumerationIndex(thread);
    PropertyAttributes attr(meta_data);
    attr.SetDictionaryOrder(enum_index);
    // Check whether the table should be growed.
    JSHandle<Derived> new_table = HashTableT::GrowHashTable(thread, table);

    // Compute the key object.
    entry = new_table->FindInsertIndex(hash);
    new_table->SetEntry(thread, entry, key.GetTaggedValue(), value.GetTaggedValue(), attr);

    new_table->IncreaseEntries(thread);
    new_table->SetNextEnumerationIndex(thread, enum_index + 1);
    return new_table;
}

template <typename Derived>
JSHandle<Derived> OrderTaggedHashTable<Derived>::Put(const JSThread *thread, const JSHandle<Derived> &table,
                                                     const JSHandle<JSTaggedValue> &key,
                                                     const JSHandle<JSTaggedValue> &value,
                                                     const PropertyAttributes &meta_data)
{
    int hash = Derived::Hash(key.GetTaggedValue());
    int enum_index = table->NextEnumerationIndex(thread);
    PropertyAttributes attr(meta_data);
    attr.SetDictionaryOrder(enum_index);
    int entry = table->FindEntry(key.GetTaggedValue());
    if (entry != -1) {
        table->SetEntry(thread, entry, key.GetTaggedValue(), value.GetTaggedValue(), attr);
        return table;
    }
    // Check whether the table should be extended.
    JSHandle<Derived> new_table = HashTableT::GrowHashTable(thread, table);

    // Compute the key object.
    entry = new_table->FindInsertIndex(hash);
    new_table->SetEntry(thread, entry, key.GetTaggedValue(), value.GetTaggedValue(), attr);

    new_table->IncreaseEntries(thread);
    new_table->SetNextEnumerationIndex(thread, enum_index + 1);
    return new_table;
}

template <typename Derived>
void TaggedHashTable<Derived>::GetAllKeysIntoVector([[maybe_unused]] const JSThread *thread,
                                                    std::vector<JSTaggedValue> &vector) const
{
    int capacity = Size();
    for (int hash_index = 0; hash_index < capacity; hash_index++) {
        JSTaggedValue key = GetKey(hash_index);
        if (!key.IsUndefined() && !key.IsHole()) {
            vector.push_back(key);
        }
    }
}

template <typename Derived>
JSHandle<Derived> OrderTaggedHashTable<Derived>::Remove(const JSThread *thread, const JSHandle<Derived> &table,
                                                        int entry)
{
    if (!(table->IsKey(table->GetKey(entry)))) {
        return table;
    }
    table->ClearEntry(thread, entry);
    table->IncreaseHoleEntriesCount(thread);
    return Shrink(thread, table);
}

template <typename Derived>
int OrderTaggedHashTable<Derived>::NextEnumerationIndex(const JSThread *thread)
{
    int index = GetNextEnumerationIndex();
    auto table = Derived::Cast(this);

    if (!PropertyAttributes::IsValidIndex(index)) {
        std::vector<int> index_order = GetEnumerationOrder();
        int length = index_order.size();
        for (int i = 0; i < length; i++) {
            int old_index = index_order[i];
            int enum_index = PropertyAttributes::INTIAL_PROPERTY_INDEX + i;
            PropertyAttributes attr = table->GetAttributes(old_index);
            attr.SetDictionaryOrder(enum_index);
            table->SetAttributes(thread, old_index, attr);
        }
        index = PropertyAttributes::INTIAL_PROPERTY_INDEX + length;
    }
    return index;
}

template <typename Derived>
std::vector<int> OrderTaggedHashTable<Derived>::GetEnumerationOrder()
{
    std::vector<int> result;
    auto table = Derived::Cast(this);
    int size = table->Size();
    for (int i = 0; i < size; i++) {
        if (table->IsKey(table->GetKey(i))) {
            result.push_back(i);
        }
    }
    std::sort(result.begin(), result.end(), [table](int a, int b) {
        PropertyAttributes attr_a = table->GetAttributes(a);
        PropertyAttributes attr_b = table->GetAttributes(b);
        return attr_a.GetDictionaryOrder() < attr_b.GetDictionaryOrder();
    });
    return result;
}
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_TAGGED_HASH_TABLE_INL_H
