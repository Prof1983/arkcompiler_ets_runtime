/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ecma_string-inl.h"

#include "plugins/ecmascript/runtime/js_symbol.h"

namespace panda::ecmascript {
bool EcmaString::compressed_strings_enabled_ = true;
static constexpr int SMALL_STRING_SIZE = 128;

EcmaString *EcmaString::Concat(const JSHandle<EcmaString> &str1_handle, const JSHandle<EcmaString> &str2_handle,
                               const EcmaVM *vm)
{
    // allocator may trig gc and move src, need to hold it
    EcmaString *string1 = *str1_handle;
    EcmaString *string2 = *str2_handle;

    uint32_t length1 = string1->GetLength();

    uint32_t length2 = string2->GetLength();
    uint32_t new_length = length1 + length2;
    if (new_length == 0) {
        return vm->GetFactory()->GetEmptyString().GetObject<EcmaString>();
    }
    bool compressed = GetCompressedStringsEnabled() && (!string1->IsUtf16() && !string2->IsUtf16());
    auto new_string = AllocStringObject(new_length, compressed, vm);

    // retrieve strings after gc
    string1 = *str1_handle;
    string2 = *str2_handle;
    if (compressed) {
        Span<uint8_t> sp(new_string->GetDataUtf8Writable(), new_length);
        Span<const uint8_t> src1(string1->GetDataUtf8(), length1);
        EcmaString::StringCopy(sp, new_length, src1, length1);

        sp = sp.SubSpan(length1);
        Span<const uint8_t> src2(string2->GetDataUtf8(), length2);
        EcmaString::StringCopy(sp, new_length - length1, src2, length2);
    } else {
        Span<uint16_t> sp(new_string->GetDataUtf16Writable(), new_length);
        if (!string1->IsUtf16()) {
            for (uint32_t i = 0; i < length1; ++i) {
                sp[i] = string1->At<false>(i);
            }
        } else {
            Span<const uint16_t> src1(string1->GetDataUtf16(), length1);
            EcmaString::StringCopy(sp, new_length << 1U, src1, length1 << 1U);
        }
        sp = sp.SubSpan(length1);
        if (!string2->IsUtf16()) {
            for (uint32_t i = 0; i < length2; ++i) {
                sp[i] = string2->At<false>(i);
            }
        } else {
            uint32_t length = length2 << 1U;
            Span<const uint16_t> src2(string2->GetDataUtf16(), length2);
            EcmaString::StringCopy(sp, length, src2, length);
        }
    }

    return new_string;
}

/* static */
EcmaString *EcmaString::FastSubString(const JSHandle<EcmaString> &src, uint32_t start, uint32_t utf16_len,
                                      const EcmaVM *vm)
{
    if (utf16_len == 0) {
        return vm->GetFactory()->GetEmptyString().GetObject<EcmaString>();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    bool can_be_compressed = !src->IsUtf16();

    // allocator may trig gc and move src, need to hold it
    auto string = AllocStringObject(utf16_len, can_be_compressed, vm);

    if (src->IsUtf16()) {
        if (can_be_compressed) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            CopyUtf16AsUtf8(src->GetDataUtf16() + start, string->GetDataUtf8Writable(), utf16_len);
        } else {
            uint32_t len = utf16_len * (sizeof(uint16_t) / sizeof(uint8_t));
            Span<uint16_t> dst(string->GetDataUtf16Writable(), utf16_len);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            Span<const uint16_t> source(src->GetDataUtf16() + start, utf16_len);
            EcmaString::StringCopy(dst, len, source, len);
        }
    } else {
        Span<uint8_t> dst(string->GetDataUtf8Writable(), utf16_len);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        Span<const uint8_t> source(src->GetDataUtf8() + start, utf16_len);
        EcmaString::StringCopy(dst, utf16_len, source, utf16_len);
    }
    return string;
}

template <typename T1, typename T2>
int32_t CompareStringSpan(Span<T1> &lhs_sp, Span<T2> &rhs_sp, int32_t count)
{
    for (int32_t i = 0; i < count; ++i) {
        auto left = static_cast<int32_t>(lhs_sp[i]);
        auto right = static_cast<int32_t>(rhs_sp[i]);
        if (left != right) {
            return left - right;
        }
    }
    return 0;
}

int32_t EcmaString::Compare(const EcmaString *rhs) const
{
    const EcmaString *lhs = this;
    if (lhs == rhs) {
        return 0;
    }
    int32_t lhs_count = lhs->GetLength();
    int32_t rhs_count = rhs->GetLength();
    int32_t count_diff = lhs_count - rhs_count;
    int32_t min_count = (count_diff < 0) ? lhs_count : rhs_count;
    if (!lhs->IsUtf16() && !rhs->IsUtf16()) {
        Span<const uint8_t> lhs_sp(lhs->GetDataUtf8(), lhs_count);
        Span<const uint8_t> rhs_sp(rhs->GetDataUtf8(), rhs_count);
        int32_t char_diff = CompareStringSpan(lhs_sp, rhs_sp, min_count);
        if (char_diff != 0) {
            return char_diff;
        }
    } else if (!lhs->IsUtf16()) {
        Span<const uint8_t> lhs_sp(lhs->GetDataUtf8(), lhs_count);
        Span<const uint16_t> rhs_sp(rhs->GetDataUtf16(), rhs_count);
        int32_t char_diff = CompareStringSpan(lhs_sp, rhs_sp, min_count);
        if (char_diff != 0) {
            return char_diff;
        }
    } else if (!rhs->IsUtf16()) {
        Span<const uint16_t> lhs_sp(lhs->GetDataUtf16(), rhs_count);
        Span<const uint8_t> rhs_sp(rhs->GetDataUtf8(), lhs_count);
        int32_t char_diff = CompareStringSpan(lhs_sp, rhs_sp, min_count);
        if (char_diff != 0) {
            return char_diff;
        }
    } else {
        Span<const uint16_t> lhs_sp(lhs->GetDataUtf16(), lhs_count);
        Span<const uint16_t> rhs_sp(rhs->GetDataUtf16(), rhs_count);
        int32_t char_diff = CompareStringSpan(lhs_sp, rhs_sp, min_count);
        if (char_diff != 0) {
            return char_diff;
        }
    }
    return count_diff;
}

/* static */
template <typename T1, typename T2>
int32_t EcmaString::IndexOf(Span<const T1> &lhs_sp, Span<const T2> &rhs_sp, int32_t pos, int32_t max)
{
    ASSERT(!rhs_sp.empty());
    auto first = static_cast<int32_t>(rhs_sp[0]);
    int32_t i;
    for (i = pos; i <= max; i++) {
        if (static_cast<int32_t>(lhs_sp[i]) != first) {
            i++;
            while (i <= max && static_cast<int32_t>(lhs_sp[i]) != first) {
                i++;
            }
        }
        /* Found first character, now look at the rest of rhs_sp */
        if (i <= max) {
            int j = i + 1;
            int end = j + rhs_sp.size() - 1;

            for (int k = 1; j < end && static_cast<int32_t>(lhs_sp[j]) == static_cast<int32_t>(rhs_sp[k]); j++, k++) {
            }
            if (j == end) {
                /* Found whole string. */
                return i;
            }
        }
    }
    return -1;
}

int32_t EcmaString::IndexOf(const EcmaString *rhs, int32_t pos) const
{
    if (rhs == nullptr) {
        return -1;
    }
    const EcmaString *lhs = this;
    int32_t lhs_count = lhs->GetLength();
    int32_t rhs_count = rhs->GetLength();

    if (pos > lhs_count) {
        return -1;
    }

    if (rhs_count == 0) {
        return pos;
    }

    if (pos < 0) {
        pos = 0;
    }

    int32_t max = lhs_count - rhs_count;
    if (max < 0) {
        return -1;
    }
    if (rhs->IsUtf8() && lhs->IsUtf8()) {
        Span<const uint8_t> lhs_sp(lhs->GetDataUtf8(), lhs_count);
        Span<const uint8_t> rhs_sp(rhs->GetDataUtf8(), rhs_count);
        return EcmaString::IndexOf(lhs_sp, rhs_sp, pos, max);
    }
    if (rhs->IsUtf16() && lhs->IsUtf16()) {  // NOLINT(readability-else-after-return)
        Span<const uint16_t> lhs_sp(lhs->GetDataUtf16(), lhs_count);
        Span<const uint16_t> rhs_sp(rhs->GetDataUtf16(), rhs_count);
        return EcmaString::IndexOf(lhs_sp, rhs_sp, pos, max);
    }
    if (rhs->IsUtf16()) {
        Span<const uint8_t> lhs_sp(lhs->GetDataUtf8(), lhs_count);
        Span<const uint16_t> rhs_sp(rhs->GetDataUtf16(), rhs_count);
        return EcmaString::IndexOf(lhs_sp, rhs_sp, pos, max);
    }
    Span<const uint16_t> lhs_sp(lhs->GetDataUtf16(), lhs_count);
    Span<const uint8_t> rhs_sp(rhs->GetDataUtf8(), rhs_count);
    return EcmaString::IndexOf(lhs_sp, rhs_sp, pos, max);

    return -1;
}

// static
bool EcmaString::CanBeCompressed(const uint8_t *utf8_data, uint32_t utf8_len)
{
    if (!compressed_strings_enabled_) {
        return false;
    }
    bool is_compressed = true;
    uint32_t index = 0;
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    while (index < utf8_len) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (!IsASCIICharacter(utf8_data[index])) {
            is_compressed = false;
            break;
        }
        ++index;
    }
    return is_compressed;
}

/* static */
bool EcmaString::CanBeCompressed(const uint16_t *utf16_data, uint32_t utf16_len)
{
    if (!compressed_strings_enabled_) {
        return false;
    }
    bool is_compressed = true;
    Span<const uint16_t> data(utf16_data, utf16_len);
    for (uint32_t i = 0; i < utf16_len; i++) {
        if (!IsASCIICharacter(data[i])) {
            is_compressed = false;
            break;
        }
    }
    return is_compressed;
}

/* static */
void EcmaString::CopyUtf16AsUtf8(const uint16_t *utf16_from, uint8_t *utf8_to, uint32_t utf16_len)
{
    Span<const uint16_t> from(utf16_from, utf16_len);
    Span<uint8_t> to(utf8_to, utf16_len);
    for (uint32_t i = 0; i < utf16_len; i++) {
        to[i] = from[i];
    }
}

/* static */
bool EcmaString::StringsAreEqual(EcmaString *str1, EcmaString *str2)
{
    if ((str1->IsUtf16() != str2->IsUtf16()) || (str1->GetLength() != str2->GetLength()) ||
        (str1->GetHashcode() != str2->GetHashcode())) {
        return false;
    }

    if (str1->IsUtf16()) {
        Span<const uint16_t> data1(str1->GetDataUtf16(), str1->GetLength());
        Span<const uint16_t> data2(str2->GetDataUtf16(), str1->GetLength());
        return EcmaString::StringsAreEquals(data1, data2);
    }
    Span<const uint8_t> data1(str1->GetDataUtf8(), str1->GetLength());
    Span<const uint8_t> data2(str2->GetDataUtf8(), str1->GetLength());
    return EcmaString::StringsAreEquals(data1, data2);
}

/* static */
bool EcmaString::StringsAreEqualUtf8(const EcmaString *str1, const uint8_t *utf8_data, uint32_t utf8_len,
                                     bool can_be_compress)
{
    if (can_be_compress != str1->IsUtf8()) {
        return false;
    }

    if (can_be_compress && str1->GetLength() != utf8_len) {
        return false;
    }

    if (can_be_compress) {
        Span<const uint8_t> data1(str1->GetDataUtf8(), utf8_len);
        Span<const uint8_t> data2(utf8_data, utf8_len);
        return EcmaString::StringsAreEquals(data1, data2);
    }
    return IsUtf8EqualsUtf16(utf8_data, utf8_len, str1->GetDataUtf16(), str1->GetLength());
}

/* static */
bool EcmaString::StringsAreEqualUtf16(const EcmaString *str1, const uint16_t *utf16_data, uint32_t utf16_len)
{
    bool result = false;
    if (str1->GetLength() != utf16_len) {
        result = false;
    } else if (!str1->IsUtf16()) {
        result = IsUtf8EqualsUtf16(str1->GetDataUtf8(), str1->GetLength(), utf16_data, utf16_len);
    } else {
        Span<const uint16_t> data1(str1->GetDataUtf16(), str1->GetLength());
        Span<const uint16_t> data2(utf16_data, utf16_len);
        result = EcmaString::StringsAreEquals(data1, data2);
    }
    return result;
}

/* static */
template <typename T>
bool EcmaString::StringsAreEquals(Span<const T> &str1, Span<const T> &str2)
{
    ASSERT(str1.Size() <= str2.Size());
    size_t size = str1.Size();
    if (size < SMALL_STRING_SIZE) {
        for (size_t i = 0; i < size; i++) {
            if (str1[i] != str2[i]) {
                return false;
            }
        }
        return true;
    }
    return !memcmp(str1.data(), str2.data(), size);
}

template <typename T>
bool EcmaString::StringCopy(Span<T> &dst, size_t dst_max, Span<const T> &src, size_t count)
{
    ASSERT(dst_max >= count);
    ASSERT(dst.Size() >= src.Size());
    if (src.Size() < SMALL_STRING_SIZE) {
        for (size_t i = 0; i < src.Size(); i++) {
            dst[i] = src[i];
        }
        return true;
    }
    if (memcpy_s(dst.data(), dst_max, src.data(), count) != EOK) {
        LOG_ECMA(FATAL) << "memcpy_s failed";
        UNREACHABLE();
    }
    return true;
}

static int32_t ComputeHashForUtf8(const uint8_t *utf8_data, size_t utf8_len)
{
    if (utf8_data == nullptr) {
        return 0;
    }
    uint32_t hash = 0;
    while (utf8_len-- > 0) {
        constexpr size_t SHIFT = 5;
        hash = (hash << SHIFT) - hash + *utf8_data++;  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    }
    return static_cast<int32_t>(hash);
}

uint32_t EcmaString::ComputeHashcode() const
{
    uint32_t hash;
    if (compressed_strings_enabled_) {
        if (!IsUtf16()) {
            hash = ComputeHashForData(GetDataUtf8(), GetLength());
        } else {
            hash = ComputeHashForData(GetDataUtf16(), GetLength());
        }
    } else {
        ASSERT(static_cast<size_t>(GetLength()) < (std::numeric_limits<size_t>::max() >> 1U));
        hash = ComputeHashForData(GetDataUtf16(), GetLength());
    }
    return hash;
}

/* static */
uint32_t EcmaString::ComputeHashcodeUtf8(const uint8_t *utf8_data, size_t utf8_len, bool can_be_compress)
{
    uint32_t hash;
    if (can_be_compress) {
        hash = ComputeHashForUtf8(utf8_data, utf8_len);
    } else {
        auto utf16_len = utf::Utf8ToUtf16Size(utf8_data, utf8_len);
        PandaVector<uint16_t> tmp_buffer(utf16_len);
        [[maybe_unused]] auto len = utf::ConvertRegionUtf8ToUtf16(utf8_data, tmp_buffer.data(), utf8_len, utf16_len, 0);
        ASSERT(len == utf16_len);
        hash = ComputeHashForData(tmp_buffer.data(), utf16_len);
    }
    return hash;
}

/* static */
uint32_t EcmaString::ComputeHashcodeUtf16(const uint16_t *utf16_data, uint32_t length)
{
    return ComputeHashForData(utf16_data, length);
}

/* static */
bool EcmaString::IsUtf8EqualsUtf16(const uint8_t *utf8_data, size_t utf8_len, const uint16_t *utf16_data,
                                   uint32_t utf16_len)
{
    // length is one more than compared utf16_data, don't need convert all utf8_data to utf16_data
    uint32_t utf8_convert_length = utf16_len + 1;
    PandaVector<uint16_t> tmp_buffer(utf8_convert_length);
    auto len = utf::ConvertRegionUtf8ToUtf16(utf8_data, tmp_buffer.data(), utf8_len, utf8_convert_length, 0);
    if (len != utf16_len) {
        return false;
    }

    Span<const uint16_t> data1(tmp_buffer.data(), len);
    Span<const uint16_t> data2(utf16_data, utf16_len);
    return EcmaString::StringsAreEquals(data1, data2);
}
}  // namespace panda::ecmascript
