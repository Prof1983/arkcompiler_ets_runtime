/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_REGEXP_PARSER_CACHE_H
#define ECMASCRIPT_REGEXP_PARSER_CACHE_H

#include <array>

#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"

namespace panda::ecmascript {
class RegExpParserCache {
public:
    RegExpParserCache();
    ~RegExpParserCache();
    NO_COPY_SEMANTIC(RegExpParserCache);
    NO_MOVE_SEMANTIC(RegExpParserCache);

    static constexpr size_t CACHE_SIZE = 128;

    bool IsInCache(EcmaString *pattern, uint32_t flags);
    std::pair<JSTaggedValue, size_t> GetCache(EcmaString *pattern, uint32_t flags,
                                              PandaVector<PandaString> &group_name);
    void SetCache(EcmaString *pattern, uint32_t flags, JSTaggedValue code_buffer, size_t buffer_size,
                  PandaVector<PandaString> &&group_name);
    void Clear();

private:
    size_t GetHash(EcmaString *pattern, uint32_t flags);

    struct ParserKey {
        EcmaString *pattern {nullptr};
        uint32_t flags {UINT32_MAX};
        JSTaggedValue code_buffer {JSTaggedValue::Hole()};
        size_t buffer_size {0};
        PandaVector<PandaString> new_group_names;
    };

    std::array<ParserKey, CACHE_SIZE> info_ {};
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_REGEXP_PARSER_CACHE_H
