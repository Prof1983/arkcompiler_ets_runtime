/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/regexp/regexp_parser_cache.h"

namespace panda::ecmascript {
RegExpParserCache::RegExpParserCache()
{
    Clear();
}

RegExpParserCache::~RegExpParserCache()
{
    Clear();
}

void RegExpParserCache::Clear()
{
    for (ParserKey &info : info_) {
        info.pattern = nullptr;
        info.flags = UINT32_MAX;  // flags cannot be UINT32_MAX, so it means invalid.
        info.code_buffer = JSTaggedValue::Hole();
        info.buffer_size = 0;
    }
}

size_t RegExpParserCache::GetHash(EcmaString *pattern, const uint32_t flags)
{
    return (pattern->GetHashcode() ^ flags) % CACHE_SIZE;
}

std::pair<JSTaggedValue, size_t> RegExpParserCache::GetCache(EcmaString *pattern, const uint32_t flags,
                                                             PandaVector<PandaString> &group_name)
{
    size_t hash = GetHash(pattern, flags);
    ParserKey &info = info_[hash];
    if (info.flags != flags || !EcmaString::StringsAreEqual(info.pattern, pattern)) {
        return std::pair<JSTaggedValue, size_t>(JSTaggedValue::Hole(), 0);
    }
    group_name = info.new_group_names;
    return std::pair<JSTaggedValue, size_t>(info.code_buffer, info.buffer_size);
}

void RegExpParserCache::SetCache(EcmaString *pattern, const uint32_t flags, const JSTaggedValue code_buffer,
                                 const size_t buffer_size, PandaVector<PandaString> &&group_name)
{
    size_t hash = GetHash(pattern, flags);
    ParserKey &info = info_[hash];
    info.pattern = pattern;
    info.flags = flags;
    info.code_buffer = code_buffer;
    info.buffer_size = buffer_size;
    info.new_group_names = group_name;
}
}  // namespace panda::ecmascript