/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_ACCESSOR_DATA_H
#define ECMASCRIPT_ACCESSOR_DATA_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_native_pointer.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/record.h"

namespace panda::ecmascript {
class AccessorData final : public Record {
public:
    using InternalGetFunc = JSTaggedValue (*)(JSThread *, const JSHandle<JSObject> &);
    using InternalSetFunc = bool (*)(JSThread *, const JSHandle<JSObject> &, const JSHandle<JSTaggedValue> &, bool);

    static AccessorData *Cast(ObjectHeader *object)
    {
        ASSERT(JSTaggedValue(object).IsAccessorData() || JSTaggedValue(object).IsInternalAccessor());
        return static_cast<AccessorData *>(object);
    }

    inline bool IsInternal() const
    {
        return GetClass()->IsInternalAccessor();
    }

    inline bool HasSetter() const
    {
        return !GetSetter().IsUndefined();
    }

    JSTaggedValue CallInternalGet(JSThread *thread, const JSHandle<JSObject> &obj) const
    {
        ASSERT(GetGetter().IsJSNativePointer());
        JSNativePointer *getter = JSNativePointer::Cast(GetGetter().GetTaggedObject());
        auto get_func = reinterpret_cast<InternalGetFunc>(getter->GetExternalPointer());
        return get_func(thread, obj);
    }

    bool CallInternalSet(JSThread *thread, const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &value,
                         bool may_throw = false) const
    {
        ASSERT(GetSetter().IsJSNativePointer());
        JSNativePointer *setter = JSNativePointer::Cast(GetSetter().GetTaggedObject());
        auto set_func = reinterpret_cast<InternalSetFunc>(setter->GetExternalPointer());
        return set_func(thread, obj, value, may_throw);
    }

    ACCESSORS_BASE(Record)
    ACCESSORS(0, Getter)
    ACCESSORS(1, Setter)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};

class CompletionRecord final : public Record {
public:
    enum : uint8_t { NORMAL = 0U, BREAK, CONTINUE, RETURN, THROW };

    static CompletionRecord *Cast(ObjectHeader *object)
    {
        ASSERT(JSTaggedValue(object).IsCompletionRecord());
        return static_cast<CompletionRecord *>(object);
    }

    bool IsThrow() const
    {
        return JSTaggedValue::SameValue(this->GetType(), JSTaggedValue(static_cast<int32_t>(THROW)));
    }

    bool IsReturn() const
    {
        return JSTaggedValue::SameValue(this->GetType(), JSTaggedValue(static_cast<int32_t>(RETURN)));
    }

    bool IsAbrupt() const
    {
        return !JSTaggedValue::SameValue(this->GetType(), JSTaggedValue(static_cast<int32_t>(NORMAL)));
    }

    ACCESSORS_BASE(Record)
    ACCESSORS(0, Type)
    ACCESSORS(1, Value)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_ACCESSOR_DATA_H
