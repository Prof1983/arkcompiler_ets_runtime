/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_iterator.h"
#include "ecma_macros.h"
#include "ecma_vm.h"
#include "plugins/ecmascript/runtime/accessor_data.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "global_env.h"
#include "js_invoker.h"
#include "js_symbol.h"
#include "object_factory.h"

namespace panda::ecmascript {
JSTaggedValue JSIterator::IteratorCloseAndReturn(JSThread *thread, const JSHandle<JSTaggedValue> &iter,
                                                 [[maybe_unused]] const JSHandle<JSTaggedValue> &status)
{
    ASSERT(thread->HasPendingException());
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> record;
    if (thread->GetException().IsObjectWrapper()) {
        JSTaggedValue exception = ObjectWrapper::Cast(thread->GetException().GetTaggedObject())->GetValue();
        record = JSHandle<JSTaggedValue>(
            factory->NewCompletionRecord(CompletionRecord::THROW, JSHandle<JSTaggedValue>(thread, exception)));
    } else {
        record = JSHandle<JSTaggedValue>(factory->NewCompletionRecord(CompletionRecord::NORMAL, status));
    }
    JSHandle<JSTaggedValue> result = JSIterator::IteratorClose(thread, iter, record);
    if (result->IsCompletionRecord()) {
        return CompletionRecord::Cast(result->GetTaggedObject())->GetValue();
    }
    return result.GetTaggedValue();
}

JSHandle<JSTaggedValue> JSIterator::GetIterator(JSThread *thread, const JSHandle<JSTaggedValue> &obj)
{
    // 1.ReturnIfAbrupt(obj).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, obj);
    // 2.If method was not passed, then
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> iterator_symbol = env->GetIteratorSymbol();
    JSHandle<JSTaggedValue> func = JSObject::GetMethod(thread, obj, iterator_symbol);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, obj);

    return GetIterator(thread, obj, func);
}

JSHandle<JSTaggedValue> JSIterator::GetIterator(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                const JSHandle<JSTaggedValue> &method)
{
    // 1.ReturnIfAbrupt(obj).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, obj);
    // 3.Let iterator be Call(method,obj).
    auto info = NewRuntimeCallInfo(thread, method, obj, JSTaggedValue::Undefined(), 0);
    JSTaggedValue ret = JSFunction::Call(info.Get());
    JSHandle<JSTaggedValue> iter(thread, ret);
    // 4.ReturnIfAbrupt(iterator).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, iter);
    // 5.If Type(iterator) is not Object, throw a TypeError exception
    if (!iter->IsECMAObject()) {
        JSHandle<JSTaggedValue> undefined_handle(thread, JSTaggedValue::Undefined());
        THROW_TYPE_ERROR_AND_RETURN(thread, "", undefined_handle);
    }
    return iter;
}

// TODO(frobert): Remove it, deprecated
JSHandle<JSObject> JSIterator::IteratorNextOld(JSThread *thread, const JSHandle<JSTaggedValue> &iter)
{
    // 1.If value was not passed, then Let result be Invoke(iterator, "next", «‍ »).
    JSHandle<JSTaggedValue> key(thread->GlobalConstants()->GetHandledNextString());
    JSHandle<JSTaggedValue> next(JSObject::GetMethod(thread, iter, key));
    auto info = NewRuntimeCallInfo(thread, next, iter, JSTaggedValue::Undefined(), 0);
    JSTaggedValue ret = JSFunction::Call(info.Get());
    JSHandle<JSObject> result(thread, ret);
    // 3.ReturnIfAbrupt(result)
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, result);
    // 4.If Type(result) is not Object, throw a TypeError exception.
    if (!result.GetTaggedValue().IsHeapObject() || !result->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", result);
    }
    return result;
}

// 7.4.2
JSHandle<JSTaggedValue> JSIterator::IteratorNext(JSThread *thread, const JSHandle<JSTaggedValue> &iter,
                                                 const JSHandle<JSTaggedValue> &next_method)
{
    // 1.a. Let result be ? Call(iteratorRecord.[[NextMethod]], iteratorRecord.[[Iterator]]).
    auto info = NewRuntimeCallInfo(thread, next_method, iter, JSTaggedValue::Undefined(), 0);
    JSTaggedValue ret = JSFunction::Call(info.Get());
    JSHandle<JSTaggedValue> result(thread, ret);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, result);

    // 3. If Type(result) is not Object, throw a TypeError exception.
    if (!result->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", result);
    }

    // 4. Return result.
    return result;
}

JSHandle<JSTaggedValue> JSIterator::IteratorNext(JSThread *thread, const JSHandle<JSTaggedValue> &iter,
                                                 const JSHandle<JSTaggedValue> &next_method,
                                                 const JSHandle<JSTaggedValue> &value)
{
    // 2.a. Let result be ? Call(iteratorRecord.[[NextMethod]], iteratorRecord.[[Iterator]], « value »).

    auto info = NewRuntimeCallInfo(thread, next_method, iter, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(value);
    JSTaggedValue ret = JSFunction::Call(info.Get());
    JSHandle<JSTaggedValue> result(thread, ret);
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, result);

    // 3. If Type(result) is not Object, throw a TypeError exception.
    if (!result->IsECMAObject()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "", result);
    }

    // 4. Return result.
    return result;
}
// 7.4.3
bool JSIterator::IteratorComplete(JSThread *thread, const JSHandle<JSTaggedValue> &iter_result)
{
    ASSERT_PRINT(iter_result->IsECMAObject(), "iterResult must be JSObject");
    // Return ToBoolean(Get(iterResult, "done")).
    JSHandle<JSTaggedValue> done_str = thread->GlobalConstants()->GetHandledDoneString();
    JSHandle<JSTaggedValue> done = JSTaggedValue::GetProperty(thread, iter_result, done_str).GetValue();
    if (done->IsException()) {
        return false;
    }
    return done->ToBoolean();
}
// 7.4.4
JSHandle<JSTaggedValue> JSIterator::IteratorValue(JSThread *thread, const JSHandle<JSTaggedValue> &iter_result)
{
    ASSERT_PRINT(iter_result->IsECMAObject(), "iterResult must be JSObject");
    // Return Get(iterResult, "value").
    JSHandle<JSTaggedValue> value_str = thread->GlobalConstants()->GetHandledValueString();
    JSHandle<JSTaggedValue> value = JSTaggedValue::GetProperty(thread, iter_result, value_str).GetValue();
    return value;
}
// 7.4.5
JSHandle<JSTaggedValue> JSIterator::IteratorStep(JSThread *thread, const JSHandle<JSTaggedValue> &iter)
{
    // 1.Let result be IteratorNext(iterator).
    JSHandle<JSTaggedValue> result(IteratorNextOld(thread, iter));
    // 2.ReturnIfAbrupt(result).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, result);
    // 3.Let done be IteratorComplete(result).
    bool done = IteratorComplete(thread, result);
    // 4.ReturnIfAbrupt(done).
    JSHandle<JSTaggedValue> done_handle(thread, JSTaggedValue(done));
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, done_handle);
    // 5.If done is true, return false.
    if (done) {
        JSHandle<JSTaggedValue> false_handle(thread, JSTaggedValue::False());
        return false_handle;
    }
    return result;
}
// 7.4.6
JSHandle<JSTaggedValue> JSIterator::IteratorClose(JSThread *thread, const JSHandle<JSTaggedValue> &iter,
                                                  const JSHandle<JSTaggedValue> &completion)
{
    // 1.Assert: Type(iterator) is Object.
    ASSERT_PRINT(iter->IsECMAObject(), "iter must be JSObject");
    const GlobalEnvConstants *global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> exception_on_thread;
    if (UNLIKELY(thread->HasPendingException())) {
        exception_on_thread = JSHandle<JSTaggedValue>(thread, thread->GetException());
        thread->ClearException();
    }
    JSHandle<JSTaggedValue> return_str(global_const->GetHandledReturnString());
    // 3.Let return be GetMethod(iterator, "return").
    JSHandle<JSTaggedValue> return_func(JSObject::GetMethod(thread, iter, return_str));
    // 4.ReturnIfAbrupt(return).
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, return_func);
    // 5.If return is undefined, return Completion(completion).
    if (return_func->IsUndefined()) {
        if (!exception_on_thread.IsEmpty()) {
            thread->SetException(exception_on_thread.GetTaggedValue());
        }
        return completion;
    }
    // 6.Let innerResult be Call(return, iterator, «‍ »).
    auto info = NewRuntimeCallInfo(thread, return_func, iter, JSTaggedValue::Undefined(), 0);
    JSTaggedValue ret = JSFunction::Call(info.Get());
    if (!exception_on_thread.IsEmpty()) {
        thread->SetException(exception_on_thread.GetTaggedValue());
    }
    JSHandle<JSTaggedValue> inner_result(thread, ret);
    JSHandle<CompletionRecord> completion_record(thread, global_const->GetUndefined());
    if (completion->IsCompletionRecord()) {
        completion_record = JSHandle<CompletionRecord>::Cast(completion);
    }
    // 7.If completion.[[type]] is throw, return Completion(completion).
    if (!completion_record.GetTaggedValue().IsUndefined() && completion_record->IsThrow()) {
        if (!exception_on_thread.IsEmpty()) {
            thread->SetException(exception_on_thread.GetTaggedValue());
        }
        return completion;
    }
    // 8.If innerResult.[[type]] is throw, return Completion(innerResult).
    if (UNLIKELY(thread->HasPendingException())) {
        return inner_result;
    }
    // 9.If Type(innerResult.[[value]]) is not Object, throw a TypeError exception.
    if (!inner_result->IsECMAObject()) {
        JSHandle<JSTaggedValue> undefined_handle(thread, JSTaggedValue::Undefined());
        THROW_TYPE_ERROR_AND_RETURN(thread, "", undefined_handle);
    }
    if (!exception_on_thread.IsEmpty()) {
        thread->SetException(exception_on_thread.GetTaggedValue());
    }
    return completion;
}
// 7.4.7
JSHandle<JSObject> JSIterator::CreateIterResultObject(JSThread *thread, const JSHandle<JSTaggedValue> &value, bool done)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    auto global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> constructor(env->GetObjectFunction());
    JSHandle<JSTaggedValue> value_str = global_const->GetHandledValueString();
    JSHandle<JSTaggedValue> done_str = global_const->GetHandledDoneString();
    JSHandle<JSTaggedValue> done_value(thread, JSTaggedValue(done));
    // 2. Let obj be OrdinaryObjectCreate(%Object.prototype%).
    JSHandle<JSObject> obj(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor));
    // 3. Perform ! CreateDataPropertyOrThrow(obj, "value", value).
    // 4. Perform ! CreateDataPropertyOrThrow(obj, "done", done).
    JSObject::CreateDataPropertyOrThrow(thread, obj, value_str, value);
    JSObject::CreateDataPropertyOrThrow(thread, obj, done_str, done_value);
    ASSERT_NO_ABRUPT_COMPLETION(thread);
    // 5. Return obj.
    return obj;
}
}  // namespace panda::ecmascript
