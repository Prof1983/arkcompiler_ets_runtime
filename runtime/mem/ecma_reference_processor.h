/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PLUGINS_ECMASCRIPT_RUNTIME_MEM_ECMA_REFERENCE_PROCESSOR_H
#define PLUGINS_ECMASCRIPT_RUNTIME_MEM_ECMA_REFERENCE_PROCESSOR_H

#include "runtime/mem/gc/reference-processor/reference_processor.h"

namespace panda {
class ObjectHeader;
class BaseClass;
namespace mem {
enum class GCPhase;
class Reference;
class GC;
}  // namespace mem
namespace ecmascript {
class EcmaVM;
}  // namespace ecmascript
}  // namespace panda

namespace panda::mem::ecmascript {

/// Mechanism for processing JS weak references
class EcmaReferenceProcessor : public panda::mem::ReferenceProcessor {
public:
    explicit EcmaReferenceProcessor(panda::ecmascript::EcmaVM *vm);

    bool IsReference(const BaseClass *base_cls, const ObjectHeader *ref,
                     const ReferenceCheckPredicateT &ref_pred) const override;

    void HandleReference(GC *gc, GCMarkingStackType *objects_stack, const BaseClass *base_class,
                         const ObjectHeader *object, const ReferenceProcessPredicateT &pred) override;

    void ProcessReferences(bool concurrent, bool clear_soft_references, GCPhase gc_phase,
                           const mem::GC::ReferenceClearPredicateT &pred) override;

    panda::mem::Reference *CollectClearedReferences() override
    {
        return nullptr;
    }

    void ScheduleForEnqueue([[maybe_unused]] Reference *cleared_references) override
    {
        UNREACHABLE();
    }

    void Enqueue([[maybe_unused]] panda::mem::Reference *cleared_references) override
    {
        UNREACHABLE();
    }

    size_t GetReferenceQueueSize() const override
    {
        os::memory::LockHolder lock(weak_ref_lock_);
        return dyn_weak_references_.size();
    }

private:
    mutable os::memory::Mutex weak_ref_lock_;
    PandaUnorderedSet<ObjectHeader *> dyn_weak_references_ GUARDED_BY(weak_ref_lock_);
    panda::ecmascript::EcmaVM *vm_;
    GC *gc_;
};

}  // namespace panda::mem::ecmascript
#endif  // PLUGINS_ECMASCRIPT_RUNTIME_MEM_ECMA_REFERENCE_PROCESSOR_H
