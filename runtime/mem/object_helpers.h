/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#ifndef PANDA_RUNTIME_ECMASCRIPT_MEM_OBJECT_HELPERS_H
#define PANDA_RUNTIME_ECMASCRIPT_MEM_OBJECT_HELPERS_H

#include <cstddef>

namespace panda::ecmascript {

size_t GetObjectSize(const void *mem);

}  // namespace panda::ecmascript

#endif  // PANDA_RUNTIME_ECMASCRIPT_MEM_OBJECT_HELPERS_H