/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_MEM_BARRIERS_INL_H
#define ECMASCRIPT_MEM_BARRIERS_INL_H

#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/mem/barriers.h"
#include "plugins/ecmascript/runtime/mem/mem.h"

#include "libpandabase/mem/gc_barrier.h"

namespace panda::ecmascript {
/* static */
template <class T>
inline T Barriers::GetDynValue(const void *obj, size_t offset)
{
    return ObjectAccessor::GetDynValue<T>(obj, offset);
}
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_MEM_BARRIERS_INL_H
