/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_MEM_BARRIERS_H
#define ECMASCRIPT_MEM_BARRIERS_H

namespace panda::ecmascript {
class Barriers {
public:
    template <class T>
    static inline bool AtomicSetDynPrimitive(volatile void *obj, size_t offset, T old_value, T value)
    {
        volatile auto atomic_field = reinterpret_cast<volatile std::atomic<T> *>(ToUintPtr(obj) + offset);
        return std::atomic_compare_exchange_strong_explicit(atomic_field, &old_value, value, std::memory_order_release,
                                                            std::memory_order_relaxed);
    }

    template <class T>
    static inline T GetDynValue(const void *obj, size_t offset);
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_MEM_BARRIERS_H
