/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_MEM_HEAP_MANAGER_INL_H
#define ECMASCRIPT_MEM_HEAP_MANAGER_INL_H

#include "plugins/ecmascript/runtime/mem/mem_manager.h"

#include <ctime>

#include "plugins/ecmascript/runtime/mem/object_xray.h"
#include "plugins/ecmascript/runtime/js_hclass.h"

namespace panda::ecmascript {
TaggedObject *MemManager::AllocateYoungGenerationOrHugeObject(JSHClass *hclass)
{
    size_t size = hclass->GetObjectSize();
    return AllocateYoungGenerationOrHugeObject(hclass, size);
}

TaggedObject *MemManager::AllocateYoungGenerationOrHugeObject(JSHClass *hclass, size_t size,
                                                              mem::ObjectAllocatorBase::ObjMemInitPolicy obj_init)
{
    auto object = heap_manager_->AllocateObject(hclass->GetHClass(), size, TAGGED_OBJECT_ALIGNMENT, thread_, obj_init);
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    return TaggedObject::Cast(object);
}

TaggedObject *MemManager::AllocateDynClassClass(JSHClass *hclass, size_t size)
{
    return AllocateNonMovableOrHugeObject(hclass, size);
}

TaggedObject *MemManager::AllocateNonMovableOrHugeObject(JSHClass *hclass, size_t size)
{
    ObjectHeader *object = nullptr;
    if (hclass == nullptr) {
        object = heap_manager_->AllocateNonMovableObject<true>(nullptr, size, TAGGED_OBJECT_ALIGNMENT, thread_);
    } else {
        object =
            heap_manager_->AllocateNonMovableObject<false>(hclass->GetHClass(), size, TAGGED_OBJECT_ALIGNMENT, thread_);
    }
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    return TaggedObject::Cast(object);
}

void MemManager::SetClass(TaggedObject *header, JSHClass *hclass)
{
    header->SetClass(hclass);
}

TaggedObject *MemManager::AllocateNonMovableOrHugeObject(JSHClass *hclass)
{
    size_t size = hclass->GetObjectSize();
    return AllocateNonMovableOrHugeObject(hclass, size);
}

TaggedObject *MemManager::AllocateOldGenerationOrHugeObject(JSHClass *hclass, size_t size)
{
    auto object = heap_manager_->AllocateObject(hclass->GetHClass(), size, TAGGED_OBJECT_ALIGNMENT, thread_);
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    return TaggedObject::Cast(object);
}

TaggedObject *MemManager::AllocateHugeObject(JSHClass *hclass, size_t size)
{
    auto object = heap_manager_->AllocateObject(hclass->GetHClass(), size, TAGGED_OBJECT_ALIGNMENT, thread_);
    // SUPPRESS_CSA_NEXTLINE(alpha.core.WasteObjHeader)
    return TaggedObject::Cast(object);
}
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_MEM_HEAP_MANAGER_INL_H
