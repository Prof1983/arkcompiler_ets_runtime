/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_TAGGED_OBJECT_HEADER_INL_H
#define ECMASCRIPT_TAGGED_OBJECT_HEADER_INL_H

#include "plugins/ecmascript/runtime/mem/tagged_object.h"

#include <atomic>
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_handle.h"

namespace panda::ecmascript {
inline void TaggedObject::SetClassWithoutBarrier(JSHClass *hclass)
{
    ObjectHeader::SetClass(hclass->GetHClass());
}

inline void TaggedObject::SetClass(JSHClass *hclass)
{
    ObjectAccessor::SetClass(this, hclass->GetHClass());
}

inline void TaggedObject::SetClass(JSHandle<JSHClass> hclass)
{
    ObjectAccessor::SetClass(this, hclass->GetHClass());
}

inline JSHClass *TaggedObject::GetClass() const
{
    return JSHClass::FromHClass(ObjectHeader::ClassAddr<HClass>());
}

inline JSThread *TaggedObject::GetJSThread() const
{
    return JSThread::Cast(ManagedThread::GetCurrent());
}
}  //  namespace panda::ecmascript

#endif  // ECMASCRIPT_TAGGED_OBJECT_HEADER_INL_H
