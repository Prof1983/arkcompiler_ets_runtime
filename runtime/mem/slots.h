/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_MEM_SLOTS_H
#define ECMASCRIPT_MEM_SLOTS_H

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/mem/mem.h"

namespace panda::ecmascript {
class ObjectSlot {
public:
    explicit ObjectSlot(uintptr_t slot_addr) : slot_address_(slot_addr) {}
    ~ObjectSlot() = default;

    DEFAULT_COPY_SEMANTIC(ObjectSlot);
    DEFAULT_MOVE_SEMANTIC(ObjectSlot);

    void Update(TaggedObject *header)
    {
        Update(static_cast<JSTaggedType>(ToUintPtr(header)));
    }

    void Update(JSTaggedType value)
    {
        // NOLINTNEXTLINE(clang-analyzer-core.NullDereference)
        *reinterpret_cast<JSTaggedType *>(slot_address_) = value;
    }

    TaggedObject *GetTaggedObjectHeader() const
    {
        return reinterpret_cast<TaggedObject *>(GetTaggedType());
    }

    JSTaggedType GetTaggedType() const
    {
        // NOLINTNEXTLINE(clang-analyzer-core.NullDereference)
        return *reinterpret_cast<JSTaggedType *>(slot_address_);
    }

    ObjectSlot &operator++()
    {
        slot_address_ += sizeof(JSTaggedType);
        return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    ObjectSlot operator++(int)
    {
        ObjectSlot ret = *this;
        slot_address_ += sizeof(JSTaggedType);
        return ret;
    }

    uintptr_t SlotAddress() const
    {
        return slot_address_;
    }

    bool operator<(const ObjectSlot &other) const
    {
        return slot_address_ < other.slot_address_;
    }
    bool operator<=(const ObjectSlot &other) const
    {
        return slot_address_ <= other.slot_address_;
    }
    bool operator>(const ObjectSlot &other) const
    {
        return slot_address_ > other.slot_address_;
    }
    bool operator>=(const ObjectSlot &other) const
    {
        return slot_address_ >= other.slot_address_;
    }
    bool operator==(const ObjectSlot &other) const
    {
        return slot_address_ == other.slot_address_;
    }
    bool operator!=(const ObjectSlot &other) const
    {
        return slot_address_ != other.slot_address_;
    }

private:
    uintptr_t slot_address_;
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_MEM_SLOTS_H
