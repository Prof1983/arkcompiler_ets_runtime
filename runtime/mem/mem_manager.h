/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_MEM_JS_MEM_MANAGER_H
#define ECMASCRIPT_MEM_JS_MEM_MANAGER_H

#include "mem/heap_manager.h"
#include "plugins/ecmascript/runtime/js_hclass.h"

namespace panda::ecmascript {
class MemManager {
public:
    explicit MemManager(EcmaVM *vm);
    ~MemManager() = default;

    NO_COPY_SEMANTIC(MemManager);
    NO_MOVE_SEMANTIC(MemManager);

    inline TaggedObject *AllocateYoungGenerationOrHugeObject(JSHClass *hclass);
    inline TaggedObject *AllocateYoungGenerationOrHugeObject(
        JSHClass *hclass, size_t size,
        mem::ObjectAllocatorBase::ObjMemInitPolicy obj_init = mem::ObjectAllocatorBase::ObjMemInitPolicy::REQUIRE_INIT);
    inline TaggedObject *AllocateNonMovableOrHugeObject(JSHClass *hclass, size_t size);
    inline TaggedObject *AllocateDynClassClass(JSHClass *hclass, size_t size);
    inline TaggedObject *AllocateNonMovableOrHugeObject(JSHClass *hclass);
    inline TaggedObject *AllocateHugeObject(JSHClass *hclass, size_t size);
    inline TaggedObject *AllocateOldGenerationOrHugeObject(JSHClass *hclass, size_t size);

    inline void SetClass(TaggedObject *header, JSHClass *hclass);

private:
    mem::HeapManager *heap_manager_;
    ManagedThread *thread_;
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_MEM_JS_MEM_MANAGER_H
