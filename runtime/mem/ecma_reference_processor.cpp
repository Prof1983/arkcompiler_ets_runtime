/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ecma_reference_processor.h"
#include "mem/object_helpers.h"
#include "runtime/include/coretypes/class.h"
#include "runtime/mem/gc/gc.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_weak_container.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::mem::ecmascript {

EcmaReferenceProcessor::EcmaReferenceProcessor(panda::ecmascript::EcmaVM *vm) : vm_(vm), gc_(vm->GetGC()) {}

template <typename Callback>
bool EnumerateArrayElements(const panda::ecmascript::TaggedArray *array, const Callback &cb)
{
    for (size_t i = 0; i < array->GetLength(); ++i) {
        panda::ecmascript::JSTaggedValue key = array->Get(i);
        if (!key.IsHeapObject()) {
            continue;
        }
        if (cb(i, key.GetHeapObject())) {
            return true;
        }
    }
    return false;
}

template <typename Container, typename Callback>
bool EnumerateKeys(Container *container, const Callback &cb)
{
    int total_elements = container->NumberOfElements() + container->NumberOfDeletedElements();
    for (int i = 0; i < total_elements; ++i) {
        panda::ecmascript::JSTaggedValue key = container->GetKey(i);
        if (key.IsHole()) {
            continue;
        }
        ASSERT(key.IsHeapObject());
        if (cb(i, key.GetHeapObject())) {
            return true;
        }
    }
    return false;
}

bool EcmaReferenceProcessor::IsReference([[maybe_unused]] const BaseClass *base_cls, const ObjectHeader *ref,
                                         const ReferenceCheckPredicateT &ref_pred) const
{
    auto phase = gc_->GetGCPhase();
    ASSERT(IsMarking(phase));
    if (gc_->GetType() == GCType::G1_GC && (phase == GCPhase::GC_PHASE_MARK || phase == GCPhase::GC_PHASE_REMARK) &&
        !gc_->IsFullGC()) {
        // dont process refs on conc-mark in G1, it can cause a high pause
        LOG(DEBUG, REF_PROC) << "Skip reference: " << ref << " because it's G1 with phase: " << static_cast<int>(phase);
        return false;
    }
    ASSERT(ref->ClassAddr<BaseClass>()->IsDynamicClass());

    auto *hcls = panda::ecmascript::JSHClass::FromHClass(ref->ClassAddr<HClass>());
    if (!hcls->IsWeakContainer()) {
        return false;
    }
    auto is_reference_checker = [this, &ref_pred](int /* unused */, ObjectHeader *key) {
        return ref_pred(key) && !gc_->IsMarked(key);
    };
    auto object_type = hcls->GetObjectType();
    switch (object_type) {
        case panda::ecmascript::JSType::TAGGED_ARRAY:
            // Weak tagged array is used for inline caches.
            // It is reasonable to collect IC only in OOM case else it leads
            // to big perf degradation.
            if (gc_->GetLastGCCause() == GCTaskCause::OOM_CAUSE) {
                return EnumerateArrayElements(static_cast<const panda::ecmascript::TaggedArray *>(ref),
                                              is_reference_checker);
            }
            break;
        case panda::ecmascript::JSType::JS_WEAK_REF: {
            panda::ecmascript::JSTaggedValue referent =
                static_cast<const panda::ecmascript::JSWeakRef *>(ref)->GetReferent();
            return referent.IsHeapObject() ? is_reference_checker(0, referent.GetRawHeapObject()) : false;
        }
        case panda::ecmascript::JSType::LINKED_HASH_MAP:
            return EnumerateKeys(static_cast<const panda::ecmascript::LinkedHashMap *>(ref), is_reference_checker);
        case panda::ecmascript::JSType::LINKED_HASH_SET:
            return EnumerateKeys(static_cast<const panda::ecmascript::LinkedHashSet *>(ref), is_reference_checker);
        default:
            LOG(FATAL, REF_PROC) << "Unknown weak container";
    }
    return false;
}

void EcmaReferenceProcessor::HandleReference([[maybe_unused]] GC *gc,
                                             [[maybe_unused]] GCMarkingStackType *objects_stack,
                                             [[maybe_unused]] const BaseClass *base_class, const ObjectHeader *object,
                                             [[maybe_unused]] const ReferenceProcessPredicateT &pred)
{
    os::memory::LockHolder lock(weak_ref_lock_);
    dyn_weak_references_.insert(const_cast<ObjectHeader *>(object));
}

void EcmaReferenceProcessor::ProcessReferences([[maybe_unused]] bool concurrent,
                                               [[maybe_unused]] bool clear_soft_references,
                                               [[maybe_unused]] GCPhase gc_phase,
                                               const mem::GC::ReferenceClearPredicateT &pred)
{
    os::memory::LockHolder lock(weak_ref_lock_);
    panda::ecmascript::JSThread *thread = vm_->GetAssociatedJSThread();
    while (!dyn_weak_references_.empty()) {
        ObjectHeader *reference = *dyn_weak_references_.begin();
        dyn_weak_references_.erase(*dyn_weak_references_.begin());
        auto *hcls = panda::ecmascript::JSHClass::FromHClass(reference->ClassAddr<HClass>());
        ASSERT(hcls->IsWeakContainer());
        auto object_type = hcls->GetObjectType();
        if (object_type == panda::ecmascript::JSType::TAGGED_ARRAY) {
            auto *array = static_cast<panda::ecmascript::TaggedArray *>(reference);
            auto handler = [this, array](uint32_t index, ObjectHeader *elem) {
                if (!gc_->IsMarked(elem)) {
                    ObjectAccessor::SetDynValueWithoutBarrier(
                        array, array->GetElementOffset(index),
                        panda::ecmascript::JSTaggedValue::Undefined().GetRawData());
                }
                return false;
            };
            EnumerateArrayElements(array, handler);
        } else if (object_type == panda::ecmascript::JSType::JS_WEAK_REF) {
            auto *ref = static_cast<panda::ecmascript::JSWeakRef *>(reference);
            ASSERT(ref->GetReferent().IsHeapObject());
            if (!gc_->IsMarked(ref->GetReferent().GetRawHeapObject())) {
                ObjectAccessor::SetDynValueWithoutBarrier(ref, panda::ecmascript::JSWeakRef::GetReferentOffset(),
                                                          panda::ecmascript::JSTaggedValue::Undefined().GetRawData());
            }
        } else if (object_type == panda::ecmascript::JSType::LINKED_HASH_MAP) {
            auto *map = static_cast<panda::ecmascript::LinkedHashMap *>(reference);
            auto map_handler = [this, map, thread](int index, ObjectHeader *key) {
                if (!gc_->IsMarked(key)) {
                    map->RemoveEntry(thread, index);
                }
                return false;
            };
            EnumerateKeys(map, map_handler);
        } else if (object_type == panda::ecmascript::JSType::LINKED_HASH_SET) {
            auto *set = static_cast<panda::ecmascript::LinkedHashSet *>(reference);
            auto set_handler = [this, set, thread](int index, ObjectHeader *key) {
                if (!gc_->IsMarked(key)) {
                    set->RemoveEntry(thread, index);
                }
                return false;
            };
            EnumerateKeys(set, set_handler);
        } else {
            LOG(FATAL, REF_PROC) << "Unknown weak container";
        }
    }

    panda::ecmascript::WeakRootVisitor gc_update_weak =
        [this, pred](panda::ecmascript::TaggedObject *header) -> panda::ecmascript::TaggedObject * {
        if (pred(header) && !gc_->IsMarked(header)) {
            return nullptr;
        }

        return header;
    };
    vm_->ProcessReferences(gc_update_weak);
}

}  // namespace panda::mem::ecmascript
