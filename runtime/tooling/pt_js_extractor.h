/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLING_JS_EXTRACTOR_H
#define PANDA_TOOLING_JS_EXTRACTOR_H

#include "plugins/ecmascript/runtime/js_method.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "libpandafile/debug_info_extractor.h"
#include "libpandabase/macros.h"
#include "include/tooling/debug_interface.h"

namespace panda::tooling::ecmascript {
using panda::panda_file::DebugInfoExtractor;
using panda::panda_file::File;

class PtJSExtractor : public DebugInfoExtractor {
public:
    explicit PtJSExtractor(const File *pf) : DebugInfoExtractor(pf) {}
    ~PtJSExtractor() override = default;

    template <class Callback>
    bool MatchWithLine(const Callback &cb, int32_t line)
    {
        auto methods = GetMethodIdList();
        for (const auto &method : methods) {
            auto table = GetLineNumberTable(method);
            for (const auto &pair : table) {
                if (static_cast<int32_t>(pair.line) == line) {
                    return cb(method, pair.offset);
                }
            }
        }
        return false;
    }

    template <class Callback>
    bool MatchWithOffset(const Callback &cb, File::EntityId method_id, uint32_t offset)
    {
        auto line_table = GetLineNumberTable(method_id);
        auto column_table = GetColumnNumberTable(method_id);
        size_t line = 0;
        size_t column = 0;

        for (const auto &pair : line_table) {
            if (offset < pair.offset) {
                break;
            }
            if (offset == pair.offset) {
                line = pair.line;
                break;
            }
            line = pair.line;
        }

        for (const auto &pair : column_table) {
            if (offset < pair.offset) {
                break;
            }
            if (offset == pair.offset) {
                column = pair.column;
                break;
            }
            column = pair.column;
        }
        return cb(line, column);
    }

    NO_COPY_SEMANTIC(PtJSExtractor);
    NO_MOVE_SEMANTIC(PtJSExtractor);
};
}  // namespace panda::tooling::ecmascript
#endif
