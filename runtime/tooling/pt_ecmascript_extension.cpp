/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/include/tooling/pt_ecmascript_extension.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/global_env.h"

namespace panda::tooling::ecmascript {
using JSThread = panda::ecmascript::JSThread;

/* static */
VRegValue PtEcmaScriptExtension::TaggedValueToVRegValue(JSTaggedValue value)
{
    return VRegValue(bit_cast<int64_t>(value.GetRawData()));
}

/* static */
JSTaggedValue PtEcmaScriptExtension::VRegValueToTaggedValue(VRegValue value)
{
    return JSTaggedValue(bit_cast<coretypes::TaggedType>(value.GetValue()));
}
}  // namespace panda::tooling::ecmascript
