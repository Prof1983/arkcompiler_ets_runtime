/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "containers_arraylist.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_arraylist.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript::containers {
JSTaggedValue ContainersArrayList::ArrayListConstructor(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayList, Constructor);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> new_target = builtins_common::GetNewTarget(argv);
    if (new_target->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "new target can't be undefined", JSTaggedValue::Exception());
    }
    JSHandle<JSTaggedValue> constructor = builtins_common::GetConstructor(argv);
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), new_target);

    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);

    return obj.GetTaggedValue();
}

JSTaggedValue ContainersArrayList::Add(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayList, Add);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> self = builtins_common::GetThis(argv);

    if (!self->IsJSArrayList()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSArrayList", JSTaggedValue::Exception());
    }

    JSHandle<JSTaggedValue> value(builtins_common::GetCallArg(argv, 0));
    JSArrayList::Add(thread, JSHandle<JSArrayList>::Cast(self), value);

    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, JSTaggedValue::False());
    return JSTaggedValue::True();
}

JSTaggedValue ContainersArrayList::Iterator(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    BUILTINS_API_TRACE(argv->GetThread(), ArrayList, Iterator);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    return JSTaggedValue::Undefined();
}
}  // namespace panda::ecmascript::containers
