/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 */

#define INLINE_ECMA_INTRINSICS
#include "plugins/ecmascript/runtime/intrinsics-inl.h"
#include "intrinsics_gen.cpp"
