/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_ECMA_GLOABL_STORAGE_H
#define ECMASCRIPT_ECMA_GLOABL_STORAGE_H

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "runtime/include/runtime.h"

namespace panda::ecmascript {
class EcmaGlobalStorage {
public:
    static const int32_t GLOBAL_BLOCK_SIZE = 256;

    explicit EcmaGlobalStorage()
    {
        auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
        top_global_nodes_ = last_global_nodes_ = allocator->New<NodeList>(false);
        top_weak_global_nodes_ = last_weak_global_nodes_ = allocator->New<NodeList>(true);
    }

    ~EcmaGlobalStorage()
    {
        auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
        NodeList *next = top_global_nodes_;
        NodeList *current = nullptr;
        while (next != nullptr) {
            current = next;
            next = current->GetNext();
            allocator->Delete(current);
        }

        next = top_weak_global_nodes_;
        while (next != nullptr) {
            current = next;
            next = current->GetNext();
            allocator->Delete(current);
        }
    }

    class Node {
    public:
        JSTaggedType GetObject() const
        {
            return obj_;
        }

        void SetObject(JSTaggedType obj)
        {
            obj_ = obj;
        }

        Node *GetNext() const
        {
            return next_;
        }

        void SetNext(Node *node)
        {
            next_ = node;
        }

        Node *GetPrev() const
        {
            return prev_;
        }

        void SetPrev(Node *node)
        {
            prev_ = node;
        }

        int32_t GetIndex()
        {
            return index_;
        }

        void SetIndex(int32_t index)
        {
            index_ = index;
        }

        void SetFree(bool free)
        {
            is_free_ = free;
        }

        bool IsFree() const
        {
            return is_free_;
        }

        uintptr_t GetObjectAddress() const
        {
            return reinterpret_cast<uintptr_t>(&obj_);
        }

    private:
        JSTaggedType obj_ {};
        Node *next_ {nullptr};
        Node *prev_ {nullptr};
        int32_t index_ {-1};
        bool is_free_ {false};
    };

    class NodeList {
    public:
        explicit NodeList(bool is_weak) : is_weak_(is_weak)
        {
            for (int i = 0; i < GLOBAL_BLOCK_SIZE; i++) {
                node_list_[i].SetIndex(i);
            }
        }
        ~NodeList() = default;

        inline static NodeList *NodeToNodeList(Node *node);

        inline Node *NewNode(JSTaggedType value);
        inline Node *GetFreeNode(JSTaggedType value);
        inline void FreeNode(Node *node);

        inline void LinkTo(NodeList *prev);
        inline void RemoveList();

        inline bool IsFull()
        {
            return index_ >= GLOBAL_BLOCK_SIZE;
        }

        inline bool IsWeak()
        {
            return is_weak_;
        }

        inline bool HasFreeNode()
        {
            return free_list_ != nullptr;
        }

        inline bool HasUsagedNode()
        {
            return !IsFull() || used_list_ != nullptr;
        }

        inline void SetNext(NodeList *next)
        {
            next_ = next;
        }
        inline NodeList *GetNext() const
        {
            return next_;
        }

        inline void SetPrev(NodeList *prev)
        {
            prev_ = prev;
        }
        inline NodeList *GetPrev() const
        {
            return prev_;
        }

        inline void SetFreeNext(NodeList *next)
        {
            free_next_ = next;
        }
        inline NodeList *GetFreeNext() const
        {
            return free_next_;
        }

        inline void SetFreePrev(NodeList *prev)
        {
            free_prev_ = prev;
        }
        inline NodeList *GetFreePrev() const
        {
            return free_prev_;
        }

        template <class Callback>
        inline void IterateUsageGlobal(Callback callback)
        {
            Node *next = used_list_;
            Node *current = nullptr;
            while (next != nullptr) {
                current = next;
                next = current->GetNext();
                ASSERT(current != next);
                callback(current);
            }
        }

        DEFAULT_MOVE_SEMANTIC(NodeList);
        DEFAULT_COPY_SEMANTIC(NodeList);

    private:
        std::array<Node, GLOBAL_BLOCK_SIZE> node_list_;  // all
        Node *free_list_ {nullptr};                      // dispose node
        Node *used_list_ {nullptr};                      // usage node
        int32_t index_ {0};
        bool is_weak_ {false};
        NodeList *next_ {nullptr};
        NodeList *prev_ {nullptr};
        NodeList *free_next_ {nullptr};
        NodeList *free_prev_ {nullptr};
    };

    inline uintptr_t NewGlobalHandle(JSTaggedType value);
    inline void DisposeGlobalHandle(uintptr_t addr);
    inline uintptr_t SetWeak(uintptr_t addr);
    inline bool IsWeak(uintptr_t addr) const;

    template <class Callback>
    void IterateUsageGlobal(Callback callback)
    {
        NodeList *next = top_global_nodes_;
        NodeList *current = nullptr;
        while (next != nullptr) {
            current = next;
            next = current->GetNext();
            ASSERT(current != next);
            current->IterateUsageGlobal(callback);
        }
    }

    template <class Callback>
    void IterateWeakUsageGlobal(Callback callback)
    {
        NodeList *next = top_weak_global_nodes_;
        NodeList *current = nullptr;
        while (next != nullptr) {
            current = next;
            next = current->GetNext();
            ASSERT(current != next);
            current->IterateUsageGlobal(callback);
        }
    }

private:
    NO_COPY_SEMANTIC(EcmaGlobalStorage);
    NO_MOVE_SEMANTIC(EcmaGlobalStorage);

    inline uintptr_t NewGlobalHandleImplement(NodeList **storage, NodeList **free_list, bool is_weak,
                                              JSTaggedType value);

    NodeList *top_global_nodes_ {nullptr};
    NodeList *last_global_nodes_ {nullptr};
    NodeList *free_list_nodes_ {nullptr};

    NodeList *top_weak_global_nodes_ {nullptr};
    NodeList *last_weak_global_nodes_ {nullptr};
    NodeList *weak_free_list_nodes_ {nullptr};
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_ECMA_GLOABL_STORAGE_H
