/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_ECMA_VM_H
#define ECMASCRIPT_ECMA_VM_H

#include <random>
#include <tuple>

#include "include/mem/panda_containers.h"
#include "include/mem/panda_string.h"
#include "plugins/ecmascript/runtime/base/config.h"
//#include "plugins/ecmascript/runtime/builtins.h"
#include "plugins/ecmascript/runtime/ecma_call_profiling_table.h"
#include "plugins/ecmascript/runtime/ecma_string_table.h"
#include "plugins/ecmascript/runtime/global_handle_collection.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/js_method.h"
#include "plugins/ecmascript/runtime/interpreter/js_frame.h"
#include "plugins/ecmascript/runtime/js_runtime_options.h"
#include "plugins/ecmascript/runtime/mem/ecma_string.h"
#include "plugins/ecmascript/runtime/mem/object_xray.h"
#include "plugins/ecmascript/runtime/tooling/pt_js_extractor.h"
#include "include/panda_vm.h"
#include "runtime/single_thread_manager.h"
#include "libpandabase/macros.h"
#include "libpandabase/os/library_loader.h"

#include "source_languages.h"

namespace panda {
class JSNApi;
class RuntimeNotificationManager;
namespace panda_file {
class File;
}  // namespace panda_file

namespace ecmascript {
class ConstantPool;
class GlobalEnv;
class ObjectFactory;
class RegExpParserCache;
class EcmaRuntimeStat;
class MemManager;
class JSNativePointer;
class Program;
class JSPromise;
class JSFinalizationRegistry;
enum class PromiseRejectionEvent : uint32_t;

namespace job {
class MicroJobQueue;
}  // namespace job

#ifndef INL_BUILTS
#define INL_BUILTS
enum class BuiltinId : uint16_t { NONE, MATH_SIN };
#endif
namespace builtins {
class RegExpExecResultCache;
}  // namespace builtins

template <typename T>
class JSHandle;
class JSArrayBuffer;
class JSFunction;
class Program;
class ModuleManager;
class EcmaModule;
using HostPromiseRejectionTracker = void (*)(const EcmaVM *vm, const JSHandle<JSPromise> promise,
                                             const JSHandle<JSTaggedValue> reason,
                                             const PromiseRejectionEvent operation, void *data);
using PromiseRejectCallback = void (*)(void *info);

class EcmaVM final : public PandaVM {
    using PtJSExtractor = tooling::ecmascript::PtJSExtractor;

public:
    static EcmaVM *Cast(PandaVM *object)
    {
        return reinterpret_cast<EcmaVM *>(object);
    }

    PANDA_PUBLIC_API static EcmaVM *Create(const JSRuntimeOptions &options);

    PANDA_PUBLIC_API static bool Destroy(PandaVM *vm);

    explicit EcmaVM(JSRuntimeOptions options);

    static Expected<EcmaVM *, PandaString> Create(Runtime *runtime, const JSRuntimeOptions &options);

    EcmaVM();

    ~EcmaVM() override;

    bool ExecuteFromPf(std::string_view filename, std::string_view entry_point, const std::vector<std::string> &args,
                       bool is_module = false);

    bool ExecuteFromBuffer(const void *buffer, size_t size, std::string_view entry_point,
                           const std::vector<std::string> &args);

    Expected<JSTaggedValue, Runtime::Error> GetInvocableFunction(const panda_file::File &pf,
                                                                 const PandaString &method_name);

    PtJSExtractor *GetDebugInfoExtractor(const panda_file::File *file);

    bool IsInitialized() const
    {
        return vm_initialized_;
    }

    void HandleLdaStr(Frame *frame, BytecodeId string_id) override;

    ObjectFactory *GetFactory() const
    {
        return factory_;
    }

    void SaveProfileInfo() override;

    bool Initialize() override;

    bool InitializeFinish() override;
    void UninitializeThreads() override;
    void PreStartup() override {}
    void PreZygoteFork() override {}
    void PostZygoteFork() override {}
    void InitializeGC() override
    {
        mm_->InitializeGC(this);
    }
    PANDA_PUBLIC_API void StartGC() override
    {
        mm_->StartGC();
    }
    void StopGC() override
    {
        mm_->StopGC();
    }

    void VisitVmRoots(const GCRootVisitor &visitor) override;
    void UpdateVmRefs() override;

    PandaVMType GetPandaVMType() const override
    {
        return PandaVMType::ECMA_VM;
    }

    // Remove global handler for JSFunction corresponding to this method
    void CleanUpTask(Method *method) override;

    LanguageContext GetLanguageContext() const override
    {
        return Runtime::GetCurrent()->GetLanguageContext(panda_file::SourceLang::ECMASCRIPT);
    }

    panda::mem::HeapManager *GetHeapManager() const override
    {
        return mm_->GetHeapManager();
    }

    panda::mem::GC *GetGC() const override
    {
        return mm_->GetGC();
    }

    panda::mem::GCTrigger *GetGCTrigger() const override
    {
        return mm_->GetGCTrigger();
    }

    StringTable *GetStringTable() const override
    {
        UNREACHABLE();
    }

    panda::mem::GCStats *GetGCStats() const override
    {
        return mm_->GetGCStats();
    }

    panda::mem::MemStatsType *GetMemStats() const override
    {
        return mm_->GetMemStats();
    }

    panda::mem::GlobalObjectStorage *GetGlobalObjectStorage() const override
    {
        return mm_->GetGlobalObjectStorage();
    }

    coretypes::String *ResolveString(const panda_file::File &pf, panda_file::File::EntityId id) override
    {
        ASSERT(string_table_ != nullptr);
        auto str = string_table_->ResolveString(pf, id);
        return coretypes::String::Cast(str);
    }

    coretypes::String *ResolveString(ConstantPool *cp, panda_file::File::EntityId id);

    coretypes::String *ResolveString(Frame *frame, panda_file::File::EntityId id) override;

    void HandleReturnFrame() override;

    MonitorPool *GetMonitorPool() const override
    {
        UNREACHABLE();
    }

    SingleThreadManager *GetThreadManager() const override
    {
        return thread_manager_;
    }

    void VisitStringTable(const StringTable::StringVisitor &visitor, mem::VisitGCRootFlags flags) override
    {
        GetEcmaStringTable()->VisitRoots(visitor, flags);
    }

    void VisitStrings(const StringTable::StringVisitor &visitor) override
    {
        VisitStringTable(visitor, mem::VisitGCRootFlags::ACCESS_ROOT_ALL);
    }

    void SweepVmRefs(const GCObjectVisitor &gc_object_visitor) override;

    bool UpdateMovedStrings() override
    {
        return GetEcmaStringTable()->UpdateMoved();
    }

    ManagedThread *GetAssociatedThread() const override
    {
        return thread_;
    }

    JSThread *GetAssociatedJSThread() const
    {
        return thread_;
    }

    CompilerInterface *GetCompiler() const override
    {
        return compiler_;
    }

    Rendezvous *GetRendezvous() const override
    {
        return rendezvous_;
    }

    compiler::RuntimeInterface *GetCompilerRuntimeInterface() const override
    {
        return runtime_iface_;
    }

    ObjectHeader *GetOOMErrorObject() override
    {
        // preallocated OOM is not implemented for JS
        UNREACHABLE();
    }

    panda::mem::ReferenceProcessor *GetReferenceProcessor() const override
    {
        return ecma_reference_processor_.get();
    }

    const RuntimeOptions &GetOptions() const override
    {
        return Runtime::GetOptions();
    }

    static const JSRuntimeOptions &GetJSOptions()
    {
        return options_;
    }

    PANDA_PUBLIC_API JSHandle<GlobalEnv> GetGlobalEnv() const;

    JSHandle<job::MicroJobQueue> GetMicroJobQueue() const;

    bool ExecutePromisePendingJob() const;

    RegExpParserCache *GetRegExpParserCache() const
    {
        ASSERT(reg_exp_parser_cache_ != nullptr);
        return reg_exp_parser_cache_;
    }

    PANDA_PUBLIC_API JSMethod *GetMethodForNativeFunction(const void *func);

    EcmaStringTable *GetEcmaStringTable() const
    {
        ASSERT(string_table_ != nullptr);
        return string_table_;
    }

    bool HasEcmaCallProfileTable() const
    {
        return call_profiling_table_ != nullptr;
    }

    EcmaCallProfilingTable *GetEcmaCallProfileTable() const
    {
        ASSERT(call_profiling_table_ != nullptr);
        return call_profiling_table_;
    }

    JSThread *GetJSThread() const
    {
        return thread_;
    }

    bool ICEnable() const
    {
        return ic_enable_;
    }

    void HandleReferences([[maybe_unused]] const GCTask &task, const mem::GC::ReferenceClearPredicateT &pred) override
    {
        LOG(DEBUG, REF_PROC) << "Start processing cleared references";
        mem::GC *gc = mm_->GetGC();
        gc->ProcessReferences(gc->GetGCPhase(), task, pred);
    }

    void PushToArrayDataList(JSNativePointer *array);
    void RemoveArrayDataList(JSNativePointer *array);

    JSHandle<ecmascript::JSTaggedValue> GetEcmaUncaughtException() const;
    void EnableUserUncaughtErrorHandler();

    template <typename Callback>
    void EnumeratePandaFiles(Callback cb) const
    {
        os::memory::LockHolder lock(panda_file_with_program_lock_);
        for (const auto &iter : panda_file_with_program_) {
            if (!cb(std::get<0>(iter), std::get<1>(iter))) {
                break;
            }
        }
    }

    template <typename Callback>
    void EnumerateProgram(Callback cb, const std::string &panda_file) const
    {
        os::memory::LockHolder lock(panda_file_with_program_lock_);
        for (const auto &iter : panda_file_with_program_) {
            if (panda_file == std::get<1>(iter)->GetFilename()) {
                cb(std::get<0>(iter));
                break;
            }
        }
    }

    EcmaRuntimeStat *GetRuntimeStat() const
    {
        return runtime_stat_;
    }

    void SetRuntimeStatEnable(bool flag);

    bool IsRuntimeStatEnabled() const
    {
        return runtime_stat_enabled_;
    }

    bool IsOptionalLogEnabled() const
    {
        return optional_log_enabled_;
    }

    /// @brief Calls GC::RegisterNativeAllocation and allocates memory in internal space
    [[nodiscard]] void *AllocAndRegisterNative(size_t size);

    /// @brief Calls GC::RegisterNativeFree and frees memory in internal space
    void FreeAndRegisterNative(void *mem, size_t size);

    void Iterate(const RootVisitor &v);

    void CollectGarbage() const;

    void ProcessReferences(const WeakRootVisitor &v0);
    void HandleGCRoutineInMutator() override;

    JSHandle<JSTaggedValue> GetModuleByName(JSHandle<JSTaggedValue> module_name);

    void ExecuteModule(std::string_view module_file, std::string_view entry_point,
                       const std::vector<std::string> &args);

    ModuleManager *GetModuleManager() const
    {
        return module_manager_;
    }

    void SetupRegExpResultCache();

    JSHandle<JSTaggedValue> GetRegExpCache() const
    {
        return JSHandle<JSTaggedValue>(reinterpret_cast<uintptr_t>(&regexp_cache_));
    }

    void SetRegExpCache(JSTaggedValue new_cache)
    {
        regexp_cache_ = new_cache;
    }

    RuntimeNotificationManager *GetNotificationManager() const
    {
        return notification_manager_;
    }

    bool IsBytecodeProfilingEnabled() const override
    {
        return is_profiling_enabled_;
    }

    std::unique_ptr<const panda_file::File> OpenPandaFile(std::string_view location) override;

    coretypes::String *GetNonMovableString(const panda_file::File &pf, panda_file::File::EntityId id) const override;

    const PandaVector<JSMethod *> &GetNativeMethods() const
    {
        return native_methods_;
    }

    void SetData(void *data)
    {
        data_ = data;
    }

    void SetPromiseRejectCallback(PromiseRejectCallback cb)
    {
        promise_reject_callback_ = cb;
    }

    PromiseRejectCallback GetPromiseRejectCallback() const
    {
        return promise_reject_callback_;
    }

    void SetHostPromiseRejectionTracker(HostPromiseRejectionTracker cb)
    {
        host_promise_rejection_tracker_ = cb;
    }

    void PromiseRejectionTracker(const JSHandle<JSPromise> &promise, const JSHandle<JSTaggedValue> &reason,
                                 const PromiseRejectionEvent operation)
    {
        if (host_promise_rejection_tracker_ != nullptr) {
            host_promise_rejection_tracker_(this, promise, reason, operation, data_);
        }
    }

    void RegisterFinalizationRegistry(JSHandle<JSFinalizationRegistry> registry);

    void AddMethodToProfile(JSMethod *method)
    {
        profiles_methods_.insert(method);
    }

    void DumpHeap(PandaOStringStream *o_str) const final;

    PandaString GetClassesFootprint() const final;

    static constexpr size_t GetGlobalEnvOffset()
    {
        return MEMBER_OFFSET(EcmaVM, global_env_);
    }

    std::default_random_engine &GetRandomEngine()
    {
        ASSERT(random_engine_);
        return *random_engine_;
    }

protected:
    bool CheckEntrypointSignature([[maybe_unused]] Method *entrypoint) override
    {
        return true;
    }

    Expected<int, Runtime::Error> InvokeEntrypointImpl(Method *entrypoint,
                                                       const std::vector<std::string> &args) override;

    void HandleUncaughtException() override;

    void PrintJSErrorInfo(const JSHandle<JSTaggedValue> &exception_info);

private:
    static constexpr uint32_t THREAD_SLEEP_TIME = 16 * 1000;
    static constexpr uint32_t THREAD_SLEEP_COUNT = 100;

    void AddPandaFile(const panda_file::File *pf, bool is_module);
    void SetProgram(Program *program, const panda_file::File *pf);
    bool IsFrameworkPandaFile(std::string_view filename) const;

    void SetGlobalEnv(GlobalEnv *global);

    void SetMicroJobQueue(job::MicroJobQueue *queue);

    bool Execute(std::unique_ptr<const panda_file::File> pf, std::string_view entry_point,
                 const std::vector<std::string> &args, bool is_module = false);

    Expected<int, Runtime::Error> InvokeEcmaEntrypoint(const panda_file::File &pf, const PandaString &method_name,
                                                       const std::vector<std::string> &args);

    void InitializeEcmaScriptRunStat();

    void RedirectMethod(const panda_file::File &pf);

    bool VerifyFilePath(const PandaString &file_path) const;

    void ClearBufferData();

    void ClearNativeMethodsData();

    Method *GetNativeMethodWrapper();
    void LoadEcmaStdLib();
    const panda_file::File *GetLastLoadedPandaFile();

    void InitializeRandomEngine()
    {
        ASSERT(!random_engine_);
        std::random_device rd;
        random_engine_.emplace(rd());
    }

    NO_MOVE_SEMANTIC(EcmaVM);
    NO_COPY_SEMANTIC(EcmaVM);

    mem::MemoryManager *mm_ {nullptr};
    PandaUniquePtr<panda::mem::ReferenceProcessor> ecma_reference_processor_;

    SingleThreadManager *thread_manager_ {nullptr};
    Rendezvous *rendezvous_ {nullptr};
    bool is_test_mode_ {false};

    PandaSet<JSMethod *> profiles_methods_;

    // VM startup states.
    static JSRuntimeOptions options_;
    bool ic_enable_ {true};
    bool vm_initialized_ {false};
    bool is_uncaught_exception_registered_ {false};
    bool is_profiling_enabled_ {false};

    // VM memory management.
    EcmaCallProfilingTable *call_profiling_table_ {nullptr};
    EcmaStringTable *string_table_ {nullptr};
    ObjectFactory *factory_ {nullptr};
    PandaVector<JSNativePointer *> array_buffer_data_list_;

    // VM execution states.
    JSThread *thread_ {nullptr};
    RegExpParserCache *reg_exp_parser_cache_ {nullptr};
    JSTaggedValue global_env_ {JSTaggedValue::Hole()};
    JSTaggedValue regexp_cache_ {JSTaggedValue::Hole()};
    JSTaggedValue micro_job_queue_ {JSTaggedValue::Hole()};
    bool runtime_stat_enabled_ {false};
    EcmaRuntimeStat *runtime_stat_ {nullptr};

    // App framework resources.
    JSTaggedValue framework_program_ {JSTaggedValue::Hole()};
    PandaString framework_abc_file_name_;
    const panda_file::File *framework_panda_file_ {nullptr};
    PandaVector<JSMethod *> framework_program_methods_;

    // VM resources.
    PandaVector<JSMethod *> native_methods_ {};
    ModuleManager *module_manager_ {nullptr};
    bool optional_log_enabled_ {false};
    // weak reference need Redirect address
    PandaVector<std::tuple<Program *, const panda_file::File *, bool>> panda_file_with_program_
        GUARDED_BY(panda_file_with_program_lock_);
    mutable os::memory::Mutex panda_file_with_program_lock_;
    PandaMap<panda::PandaString, PandaMap<size_t, panda::PandaMap<panda::compiler::AnyBaseType, size_t>>>
        functions_arg_type_cache_;
    Method *native_method_wrapper_ {nullptr};
    CompilerInterface *compiler_ {nullptr};
    compiler::RuntimeInterface *runtime_iface_ {nullptr};

    // Debugger
    RuntimeNotificationManager *notification_manager_ {nullptr};
    PandaUnorderedMap<const panda_file::File *, std::unique_ptr<PtJSExtractor>> extractor_cache_;

    // Registered Callbacks
    PromiseRejectCallback promise_reject_callback_ {nullptr};
    HostPromiseRejectionTracker host_promise_rejection_tracker_ {nullptr};
    void *data_ {nullptr};
    PandaList<JSFinalizationRegistry *> finalization_registries_;
    // optional for lazy initialization
    std::optional<std::default_random_engine> random_engine_;

    friend class ObjectFactory;
    friend class panda::JSNApi;
    friend class EvalUtils;
};
}  // namespace ecmascript
}  // namespace panda

#endif
