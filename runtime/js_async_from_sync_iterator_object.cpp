/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description:
 */
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/js_async_from_sync_iterator_object.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_iterator.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/generator_helper.h"

namespace panda::ecmascript {

JSTaggedValue JSAsyncFromSyncIteratorValueUnwrapFunction::AsyncFromSyncIteratorValueUnwrapFunction(
    EcmaRuntimeCallInfo *argv)
{
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    // 1. Let F be the active function object.
    JSHandle<JSAsyncFromSyncIteratorValueUnwrapFunction> func(builtins_common::GetConstructor(argv));

    // 2. Return ! CreateIterResultObject(value, F.[[Done]]).
    JSHandle<JSTaggedValue> value = builtins_common::GetCallArg(argv, 0);
    return JSIterator::CreateIterResultObject(thread, value, func->GetDone().IsTrue()).GetTaggedValue();
}

JSTaggedValue JSAsyncFromSyncIteratorObject::CreateAsyncFromSyncIterator(JSThread *thread,
                                                                         const JSHandle<JSTaggedValue> &iterator,
                                                                         const JSHandle<JSTaggedValue> &next_method)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    // 1. Let asyncIterator be ! OrdinaryObjectCreate(%AsyncFromSyncIteratorPrototype%, « [[SyncIteratorRecord]] »).
    JSHandle<JSAsyncFromSyncIteratorObject> async_iterator = factory->NewJSAsyncFromSyncIteratorObject();

    // 2. Set asyncIterator.[[SyncIteratorRecord]] to syncIteratorRecord.
    async_iterator->SetIterator(thread, iterator);
    async_iterator->SetNextMethod(thread, next_method);

    // 3. Let nextMethod be ! Get(asyncIterator, "next").
    // 4. Let iteratorRecord be the Record { [[Iterator]]: asyncIterator, [[NextMethod]]: nextMethod, [[Done]]: false }.
    // 5. Return iteratorRecord.
    return async_iterator.GetTaggedValue();
}

JSTaggedValue JSAsyncFromSyncIteratorObject::AsyncFromSyncIteratorContinuation(
    JSThread *thread, const JSHandle<JSTaggedValue> &result, const JSHandle<PromiseCapability> &promise_capability)
{
    [[maybe_unused]] EcmaHandleScope handle_scope(thread);

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();

    // 1. Let done be IteratorComplete(result).
    JSHandle<JSTaggedValue> done(thread, JSTaggedValue(JSIterator::IteratorComplete(thread, result)));

    // 2. IfAbruptRejectPromise(done, promiseCapability).
    RETURN_REJECT_PROMISE_IF_ABRUPT_THROWN_VALUE(thread, done, promise_capability);

    // 3. Let value be IteratorValue(result).
    JSHandle<JSTaggedValue> value = JSIterator::IteratorValue(thread, result);

    // 4. IfAbruptRejectPromise(value, promiseCapability).
    RETURN_REJECT_PROMISE_IF_ABRUPT_THROWN_VALUE(thread, value, promise_capability);

    // 5. Let valueWrapper be PromiseResolve(%Promise%, value).
    JSHandle<JSTaggedValue> value_wrapper(
        thread, JSPromise::PromiseResolve(thread, JSHandle<JSTaggedValue>::Cast(env->GetPromiseFunction()), value));

    // 6. IfAbruptRejectPromise(valueWrapper, promiseCapability).
    RETURN_REJECT_PROMISE_IF_ABRUPT_THROWN_VALUE(thread, value_wrapper, promise_capability);

    // 7. 7. Let steps be the algorithm steps defined in Async-from-Sync Iterator Value Unwrap Functions.
    // 8. Let length be the number of non-optional parameters of the function definition in Async-from-Sync Iterator
    // Value Unwrap Functions.
    // 9. Let onFulfilled be ! CreateBuiltinFunction(steps, length, "", « [[Done]] »).
    JSHandle<JSAsyncFromSyncIteratorValueUnwrapFunction> on_fulfilled =
        factory->NewJSAsyncFromSyncIteratorValueUnwrapFunction(reinterpret_cast<void *>(
            JSAsyncFromSyncIteratorValueUnwrapFunction::AsyncFromSyncIteratorValueUnwrapFunction));

    // 10. Set onFulfilled.[[Done]] to done.
    on_fulfilled->SetDone(thread, done.GetTaggedValue());

    // 11. Perform ! PerformPromiseThen(valueWrapper, onFulfilled, undefined, promiseCapability).
    [[maybe_unused]] JSTaggedValue then_result = builtins::promise::PerformPromiseThen(
        thread, JSHandle<JSPromise>::Cast(value_wrapper), JSHandle<JSTaggedValue>::Cast(on_fulfilled),
        thread->GlobalConstants()->GetHandledUndefined(), JSHandle<JSTaggedValue>::Cast(promise_capability));

    // 12. Return promiseCapability.[[Promise]].
    return promise_capability->GetPromise();
}
}  // namespace panda::ecmascript
