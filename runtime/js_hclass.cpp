/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_hclass-inl.h"

#include <algorithm>

#include "plugins/ecmascript/runtime/base/config.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/ic/proto_change_details.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_symbol.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"
#include "plugins/ecmascript/runtime/weak_vector-inl.h"

namespace panda::ecmascript {
// class TransitionsDictionary
JSHandle<TransitionsDictionary> TransitionsDictionary::PutIfAbsent(const JSThread *thread,
                                                                   const JSHandle<TransitionsDictionary> &dictionary,
                                                                   const JSHandle<JSTaggedValue> &key,
                                                                   const JSHandle<JSTaggedValue> &value,
                                                                   const JSHandle<JSTaggedValue> &meta_data)
{
    int hash = TransitionsDictionary::Hash(key.GetTaggedValue(), meta_data.GetTaggedValue());

    /* no need to add key if exist */
    int entry = dictionary->FindEntry(key.GetTaggedValue(), meta_data.GetTaggedValue());
    if (entry != -1) {
        return dictionary;
    }

    // Check whether the dictionary should be extended.
    JSHandle<TransitionsDictionary> new_dictionary(HashTableT::GrowHashTable(thread, dictionary));
    // Compute the key object.
    entry = new_dictionary->FindInsertIndex(hash);
    new_dictionary->SetEntry(thread, entry, key.GetTaggedValue(), value.GetTaggedValue(), meta_data.GetTaggedValue());

    new_dictionary->IncreaseEntries(thread);
    return new_dictionary;
}

int TransitionsDictionary::FindEntry(const JSTaggedValue &key, const JSTaggedValue &meta_data)
{
    size_t size = Size();
    uint32_t count = 1;
    uint32_t hash = TransitionsDictionary::Hash(key, meta_data);
    // GrowHashTable will guarantee the hash table is never full.
    for (int entry = GetFirstPosition(hash, size);; entry = GetNextPosition(entry, count++, size)) {
        JSTaggedValue element = GetKey(entry);
        if (element.IsHole()) {
            continue;
        }
        if (element.IsUndefined()) {
            return -1;
        }

        if (TransitionsDictionary::IsMatch(key, meta_data, element, GetAttributes(entry))) {
            return entry;
        }
    }
    return -1;
}

JSHandle<TransitionsDictionary> TransitionsDictionary::Remove(const JSThread *thread,
                                                              const JSHandle<TransitionsDictionary> &table,
                                                              const JSHandle<JSTaggedValue> &key,
                                                              const JSTaggedValue &meta_data)
{
    int entry = table->FindEntry(key.GetTaggedValue(), meta_data);
    if (entry == -1) {
        return table;
    }

    table->RemoveElement(thread, entry);
    return TransitionsDictionary::Shrink(thread, table);
}

void TransitionsDictionary::Rehash(const JSThread *thread, TransitionsDictionary *new_table)
{
    if ((new_table == nullptr) || (new_table->Size() < EntriesCount())) {
        return;
    }
    int size = this->Size();
    // Rehash elements to new table
    for (int i = 0; i < size; i++) {
        int from_index = GetEntryIndex(i);
        JSTaggedValue k = this->GetKey(i);
        if (!IsKey(k)) {
            continue;
        }
        int hash = TransitionsDictionary::Hash(k, this->GetAttributes(i));
        int insertion_index = GetEntryIndex(new_table->FindInsertIndex(hash));
        JSTaggedValue tv = Get(from_index);
        new_table->Set(thread, insertion_index, tv);
        for (int j = 1; j < TransitionsDictionary::ENTRY_SIZE; j++) {
            tv = Get(from_index + j);
            new_table->Set(thread, insertion_index + j, tv);
        }
    }
    new_table->SetEntriesCount(thread, EntriesCount());
    new_table->SetHoleEntriesCount(thread, 0);
}

// class JSHClass
void JSHClass::Initialize(const JSThread *thread, uint32_t size, JSType type, uint32_t inlined_props, uint32_t flags)
{
    new (&hclass_) HClass(flags, panda_file::SourceLang::ECMASCRIPT);
    hclass_.SetManagedObject(this);

    ClearBitField();
    if (JSType::JS_OBJECT_BEGIN <= type && type <= JSType::JS_OBJECT_END) {
        SetObjectSize(size + inlined_props * JSTaggedValue::TaggedTypeSize());
        SetInlinedPropsStart(size);
        auto env = thread->GetEcmaVM()->GetGlobalEnv();
        SetLayout(thread, env->GetEmptyLayoutInfo());
    } else {
        SetObjectSize(size);
        SetLayout(thread, JSTaggedValue::Null());
    }
    SetPrototype(thread, JSTaggedValue::Null());

    SetObjectType(type);
    SetExtensible(true);
    SetIsPrototype(false);
    SetElementRepresentation(Representation::NONE);
    SetTransitions(thread, JSTaggedValue::Null());
    SetParent(thread, JSTaggedValue::Null());
    SetProtoChangeMarker(thread, JSTaggedValue::Null());
    SetProtoChangeDetails(thread, JSTaggedValue::Null());
    SetEnumCache(thread, JSTaggedValue::Null());
}

void JSHClass::Copy(const JSThread *thread, const JSHClass *jshclass)
{
    // HClass fields
    SetBitField(jshclass->GetBitField());
    SetNumberOfProps(jshclass->NumberOfProps());
    SetPrototype(thread, jshclass->GetPrototype());
    hclass_.SetNativeFieldMask(jshclass->GetHClass()->GetNativeFieldMask());
}

JSHandle<JSHClass> JSHClass::Clone(const JSThread *thread, const JSHandle<JSHClass> &jshclass,
                                   bool without_inlined_properties)
{
    JSType type = jshclass->GetObjectType();

    uint32_t num_inlined_props = without_inlined_properties ? 0 : jshclass->GetInlinedProperties();

    uint32_t size;
    if (jshclass->IsJSObject()) {
#ifndef ECMA_DISABLE_FREEOBJECTS
        size = jshclass->GetInlinedPropsStartSize();
#else
        size = jshclass->GetObjectSize() - numInlinedProps * JSTaggedValue::TaggedTypeSize();
#endif
    } else {
        ASSERT(num_inlined_props == 0);
        size = jshclass->GetObjectSize();
    }

    auto factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSHClass> new_jshclass = factory->NewEcmaDynClass(factory->hclass_class_, size, type,
                                                               jshclass->GetHClass()->GetFlags(), num_inlined_props);
    // Copy all
    new_jshclass->Copy(thread, *jshclass);
    new_jshclass->SetParent(thread, JSTaggedValue::Null());
    new_jshclass->SetTransitions(thread, JSTaggedValue::Null());
    new_jshclass->SetProtoChangeDetails(thread, JSTaggedValue::Null());
    new_jshclass->SetProtoChangeMarker(thread, JSTaggedValue::Null());
    new_jshclass->SetEnumCache(thread, JSTaggedValue::Null());
    // reuse Attributes first.
    new_jshclass->SetLayout(thread, jshclass->GetLayout());

    return new_jshclass;
}

// use for transition to dictionary
JSHandle<JSHClass> JSHClass::CloneWithoutInlinedProperties(const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    return Clone(thread, jshclass, true);
}

void JSHClass::TransitionElementsToDictionary(const JSThread *thread, const JSHandle<JSObject> &obj)
{
    // property transition to slow first
    if (!obj->GetJSHClass()->IsDictionaryMode()) {
        JSObject::TransitionToDictionary(thread, obj);
    }
    obj->GetJSHClass()->SetIsDictionaryElement(true);
    obj->GetJSHClass()->SetIsStableElements(false);
}

void JSHClass::AddProperty(const JSThread *thread, const JSHandle<JSObject> &obj, const JSHandle<JSTaggedValue> &key,
                           const PropertyAttributes &attr)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSHClass> jshclass(thread, obj->GetJSHClass());
    JSHClass *new_dyn = jshclass->FindTransitions(key.GetTaggedValue(), JSTaggedValue(attr.GetPropertyMetaData()));
    if (new_dyn != nullptr) {
#if ECMASCRIPT_ENABLE_IC
        JSHClass::NotifyHclassChanged(thread, jshclass, JSHandle<JSHClass>(thread, new_dyn));
#endif
        obj->SetClass(new_dyn);
        return;
    }

    // 2. Create hclass
    JSHandle<JSHClass> new_jshclass = JSHClass::Clone(thread, jshclass);

    // 3. Add Property and meta_data
    int offset = attr.GetOffset();
    new_jshclass->IncNumberOfProps();

    {
        JSMutableHandle<LayoutInfo> layout_info_handle(thread, new_jshclass->GetLayout());

        if (layout_info_handle->NumberOfElements() != offset) {
            layout_info_handle.Update(factory->CopyAndReSort(layout_info_handle, offset, offset + 1));
            new_jshclass->SetLayout(thread, layout_info_handle);
        } else if (layout_info_handle->GetPropertiesCapacity() <= offset) {  // need to Grow
            layout_info_handle.Update(
                factory->ExtendLayoutInfo(layout_info_handle, LayoutInfo::ComputeGrowCapacity(offset)));
            new_jshclass->SetLayout(thread, layout_info_handle);
        }
        layout_info_handle->AddKey(thread, offset, key.GetTaggedValue(), attr);
    }

    // 4. Add newDynclass to old dynclass's transitions.
    AddTransitions(thread, jshclass, new_jshclass, key, attr);

    // 5. update hclass in object.
#if ECMASCRIPT_ENABLE_IC
    JSHClass::NotifyHclassChanged(thread, jshclass, new_jshclass);
#endif
    obj->SetClass(*new_jshclass);
}

JSHandle<JSHClass> JSHClass::TransitionExtension(const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    JSHandle<JSTaggedValue> key(thread->GlobalConstants()->GetHandledPreventExtensionsString());
    {
        auto *new_dyn = jshclass->FindTransitions(key.GetTaggedValue(), JSTaggedValue(0));
        if (new_dyn != nullptr) {
            return JSHandle<JSHClass>(thread, new_dyn);
        }
    }
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 2. new a hclass
    JSHandle<JSHClass> new_jshclass = JSHClass::Clone(thread, jshclass);
    new_jshclass->SetExtensible(false);

    JSTaggedValue attrs = new_jshclass->GetLayout();
    if (!attrs.IsNull()) {
        ASSERT(attrs.IsTaggedArray());
        JSMutableHandle<LayoutInfo> layout_info_handle(thread, attrs);
        layout_info_handle.Update(factory->CopyLayoutInfo(layout_info_handle).GetTaggedValue());
        new_jshclass->SetLayout(thread, layout_info_handle);
    }

    // 3. Add newDynclass to old dynclass's parent's transitions.
    AddExtensionTransitions(thread, jshclass, new_jshclass, key);
    // parent is the same as jshclass, already copy
    return new_jshclass;
}

JSHandle<JSHClass> JSHClass::TransitionProto(const JSThread *thread, const JSHandle<JSHClass> &jshclass,
                                             const JSHandle<JSTaggedValue> &proto)
{
    JSHandle<JSTaggedValue> key(thread->GlobalConstants()->GetHandledPrototypeString());

    {
        auto *new_dyn = jshclass->FindProtoTransitions(key.GetTaggedValue(), proto.GetTaggedValue());
        if (new_dyn != nullptr) {
            return JSHandle<JSHClass>(thread, new_dyn);
        }
    }

    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    // 2. new a hclass
    JSHandle<JSHClass> new_jshclass = JSHClass::Clone(thread, jshclass);
    new_jshclass->SetPrototype(thread, proto.GetTaggedValue());

    JSTaggedValue attrs = new_jshclass->GetLayout();
    {
        JSMutableHandle<LayoutInfo> layout_info_handle(thread, attrs);
        layout_info_handle.Update(factory->CopyLayoutInfo(layout_info_handle).GetTaggedValue());
        new_jshclass->SetLayout(thread, layout_info_handle);
    }

    // 3. Add newJshclass to old jshclass's parent's transitions.
    AddProtoTransitions(thread, jshclass, new_jshclass, key, proto);

    // parent is the same as jshclass, already copy
    return new_jshclass;
}

void JSHClass::SetPrototype(const JSThread *thread, JSTaggedValue proto)
{
    if (proto.IsECMAObject()) {
        JSObject::Cast(proto.GetTaggedObject())->GetJSHClass()->SetIsPrototype(true);
    }
    SetProto(thread, proto);
}

void JSHClass::SetPrototype(const JSThread *thread, const JSHandle<JSTaggedValue> &proto)
{
    SetPrototype(thread, proto.GetTaggedValue());
}

void JSHClass::TransitionToDictionary(const JSThread *thread, const JSHandle<JSObject> &obj)
{
    // 1. new a hclass
    JSHandle<JSHClass> jshclass(thread, obj->GetJSHClass());
    JSHandle<JSHClass> new_jshclass = CloneWithoutInlinedProperties(thread, jshclass);

    {
        // 2. Copy
        new_jshclass->SetNumberOfProps(0);
        new_jshclass->SetIsDictionaryMode(true);
        ASSERT(new_jshclass->GetInlinedProperties() == 0);

        // 3. Add newJshclass to ?
#if ECMASCRIPT_ENABLE_IC
        JSHClass::NotifyHclassChanged(thread, JSHandle<JSHClass>(thread, obj->GetJSHClass()), new_jshclass);
#endif
        obj->SetClass(new_jshclass);
    }
}

JSHandle<JSTaggedValue> JSHClass::EnableProtoChangeMarker(const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    JSTaggedValue proto = jshclass->GetPrototype();
    if (!proto.IsECMAObject()) {
        // Return JSTaggedValue directly. No proto check is needed.
        return JSHandle<JSTaggedValue>(thread, JSTaggedValue(false));
    }
    JSHandle<JSObject> proto_handle(thread, proto);
    JSHandle<JSHClass> proto_dyncalss(thread, proto_handle->GetJSHClass());
    RegisterOnProtoChain(thread, proto_dyncalss);
    JSTaggedValue proto_change_marker = proto_dyncalss->GetProtoChangeMarker();
    if (proto_change_marker.IsProtoChangeMarker()) {
        JSHandle<ProtoChangeMarker> marker_handle(thread,
                                                  ProtoChangeMarker::Cast(proto_change_marker.GetTaggedObject()));
        if (!marker_handle->GetHasChanged()) {
            return JSHandle<JSTaggedValue>(marker_handle);
        }
    }
    JSHandle<ProtoChangeMarker> marker_handle = thread->GetEcmaVM()->GetFactory()->NewProtoChangeMarker();
    marker_handle->SetHasChanged(false);
    proto_dyncalss->SetProtoChangeMarker(thread, marker_handle.GetTaggedValue());
    return JSHandle<JSTaggedValue>(marker_handle);
}

void JSHClass::NotifyHclassChanged(const JSThread *thread, JSHandle<JSHClass> old_hclass, JSHandle<JSHClass> new_hclass)
{
    if (!old_hclass->IsPrototype()) {
        return;
    }
    // The old hclass is the same as new one
    if (old_hclass.GetTaggedValue() == new_hclass.GetTaggedValue()) {
        return;
    }
    new_hclass->SetIsPrototype(true);
    JSHClass::NoticeThroughChain(thread, old_hclass);
    JSHClass::RefreshUsers(thread, old_hclass, new_hclass);
}

void JSHClass::RegisterOnProtoChain(const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    ASSERT(jshclass->IsPrototype());
    JSHandle<JSHClass> user = jshclass;
    JSHandle<ProtoChangeDetails> user_details = GetProtoChangeDetails(thread, user);

    while (true) {
        // Find the prototype chain as far as the hclass has not been registered.
        if (user_details->GetRegisterIndex() != JSTaggedValue(ProtoChangeDetails::UNREGISTERED)) {
            return;
        }

        JSTaggedValue proto = user->GetPrototype();
        if (!proto.IsHeapObject()) {
            return;
        }
        if (proto.IsJSProxy()) {
            return;
        }
        ASSERT(proto.IsECMAObject());
        JSHandle<JSObject> proto_handle(thread, proto);
        JSHandle<ProtoChangeDetails> proto_details =
            GetProtoChangeDetails(thread, JSHandle<JSHClass>(thread, proto_handle->GetJSHClass()));
        JSTaggedValue listeners = proto_details->GetChangeListener();
        JSHandle<ChangeListener> listeners_handle;
        if (listeners == JSTaggedValue(0)) {
            listeners_handle = JSHandle<ChangeListener>(ChangeListener::Create(thread));
        } else {
            listeners_handle = JSHandle<ChangeListener>(thread, listeners);
        }
        uint32_t register_index = 0;
        JSHandle<ChangeListener> new_listeners = ChangeListener::Add(thread, listeners_handle, user, &register_index);
        user_details->SetRegisterIndex(thread, JSTaggedValue(register_index));
        proto_details->SetChangeListener(thread, new_listeners.GetTaggedValue());
        user_details = proto_details;
        user = JSHandle<JSHClass>(thread, proto_handle->GetJSHClass());
    }
}

bool JSHClass::UnregisterOnProtoChain(const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    ASSERT(jshclass->IsPrototype());
    if (!jshclass->GetProtoChangeDetails().IsProtoChangeDetails()) {
        return false;
    }
    if (!jshclass->GetPrototype().IsECMAObject()) {
        JSTaggedValue listeners =
            ProtoChangeDetails::Cast(jshclass->GetProtoChangeDetails().GetTaggedObject())->GetChangeListener();
        return listeners != JSTaggedValue(0);
    }
    JSHandle<ProtoChangeDetails> current_details = GetProtoChangeDetails(thread, jshclass);
    uint32_t index = current_details->GetRegisterIndex().GetArrayLength();
    if (JSTaggedValue(index) == JSTaggedValue(ProtoChangeDetails::UNREGISTERED)) {
        return false;
    }
    JSTaggedValue proto = jshclass->GetPrototype();
    ASSERT(proto.IsECMAObject());
    JSTaggedValue proto_details_value = JSObject::Cast(proto.GetTaggedObject())->GetJSHClass()->GetProtoChangeDetails();
    ASSERT(proto_details_value.IsProtoChangeDetails());
    JSTaggedValue listeners_value =
        ProtoChangeDetails::Cast(proto_details_value.GetTaggedObject())->GetChangeListener();
    ASSERT(listeners_value != JSTaggedValue(0));
    JSHandle<ChangeListener> listeners(thread, listeners_value.GetTaggedObject());
    ASSERT(listeners->Get(index) == jshclass.GetTaggedValue());
    listeners->Delete(thread, index);
    return true;
}

JSHandle<ProtoChangeDetails> JSHClass::GetProtoChangeDetails(const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    JSTaggedValue proto_details = jshclass->GetProtoChangeDetails();
    if (proto_details.IsProtoChangeDetails()) {
        return JSHandle<ProtoChangeDetails>(thread, proto_details);
    }
    JSHandle<ProtoChangeDetails> proto_details_handle = thread->GetEcmaVM()->GetFactory()->NewProtoChangeDetails();
    jshclass->SetProtoChangeDetails(thread, proto_details_handle.GetTaggedValue());
    return proto_details_handle;
}

JSHandle<ProtoChangeDetails> JSHClass::GetProtoChangeDetails(const JSThread *thread, const JSHandle<JSObject> &obj)
{
    JSHandle<JSHClass> jshclass(thread, obj->GetJSHClass());
    return GetProtoChangeDetails(thread, jshclass);
}

void JSHClass::NoticeRegisteredUser([[maybe_unused]] const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    ASSERT(jshclass->IsPrototype());
    JSTaggedValue marker_value = jshclass->GetProtoChangeMarker();
    if (marker_value.IsProtoChangeMarker()) {
        ProtoChangeMarker *proto_change_marker = ProtoChangeMarker::Cast(marker_value.GetTaggedObject());
        proto_change_marker->SetHasChanged(true);
    }
}

void JSHClass::NoticeThroughChain(const JSThread *thread, const JSHandle<JSHClass> &jshclass)
{
    NoticeRegisteredUser(thread, jshclass);
    JSTaggedValue proto_details_value = jshclass->GetProtoChangeDetails();
    if (!proto_details_value.IsProtoChangeDetails()) {
        return;
    }
    JSTaggedValue listeners_value =
        ProtoChangeDetails::Cast(proto_details_value.GetTaggedObject())->GetChangeListener();
    if (!listeners_value.IsTaggedArray()) {
        return;
    }
    ChangeListener *listeners = ChangeListener::Cast(listeners_value.GetTaggedObject());
    for (uint32_t i = 0; i < listeners->GetEnd(); i++) {
        JSTaggedValue temp = listeners->Get(i);
        if (temp.IsJSHClass()) {
            NoticeThroughChain(thread, JSHandle<JSHClass>(thread, listeners->Get(i).GetTaggedObject()));
        }
    }
}

void JSHClass::RefreshUsers(const JSThread *thread, const JSHandle<JSHClass> &old_hclass,
                            const JSHandle<JSHClass> &new_hclass)
{
    ASSERT(old_hclass->IsPrototype());
    ASSERT(new_hclass->IsPrototype());
    bool once_registered = UnregisterOnProtoChain(thread, old_hclass);

    new_hclass->SetProtoChangeDetails(thread, old_hclass->GetProtoChangeDetails());
    old_hclass->SetProtoChangeDetails(thread, JSTaggedValue(0));
    if (once_registered) {
        if (new_hclass->GetProtoChangeDetails().IsProtoChangeDetails()) {
            ProtoChangeDetails::Cast(new_hclass->GetProtoChangeDetails().GetTaggedObject())
                ->SetRegisterIndex(thread, JSTaggedValue(ProtoChangeDetails::UNREGISTERED));
        }
        RegisterOnProtoChain(thread, new_hclass);
    }
}
}  // namespace panda::ecmascript
