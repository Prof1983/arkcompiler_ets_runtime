/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_date_time_format.h"

#include "ecma_macros.h"
#include "global_env.h"
#include "js_array.h"
#include "js_date.h"
#include "js_intl.h"
#include "js_locale.h"
#include "js_object-inl.h"
#include "object_factory.h"

namespace panda::ecmascript {
struct CommonDateFormatPart {
    int32_t f_field = 0;        // NOLINT(misc-non-private-member-variables-in-classes)
    int32_t f_begin_index = 0;  // NOLINT(misc-non-private-member-variables-in-classes)
    int32_t f_end_index = 0;    // NOLINT(misc-non-private-member-variables-in-classes)
    int32_t index = 0;          // NOLINT(misc-non-private-member-variables-in-classes)
    bool is_pre_exist = false;  // NOLINT(misc-non-private-member-variables-in-classes)

    CommonDateFormatPart() = default;
    CommonDateFormatPart(int32_t f_field, int32_t f_begin_index, int32_t f_end_index, int32_t index, bool is_pre_exist)
        : f_field(f_field),
          f_begin_index(f_begin_index),
          f_end_index(f_end_index),
          index(index),
          is_pre_exist(is_pre_exist)
    {
    }

    ~CommonDateFormatPart() = default;

    DEFAULT_COPY_SEMANTIC(CommonDateFormatPart);
    DEFAULT_MOVE_SEMANTIC(CommonDateFormatPart);
};

namespace {
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<std::string> ICU_LONG_SHORT = {"long", "short"};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<std::string> ICU_NARROW_LONG_SHORT = {"narrow", "long", "short"};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<std::string> ICU2_DIGIT_NUMERIC = {"2-digit", "numeric"};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<std::string> ICU_NARROW_LONG_SHORT2_DIGIT_NUMERIC = {"narrow", "long", "short", "2-digit", "numeric"};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_WEEKDAY_PE = {{"EEEEE", "narrow"}, {"EEEE", "long"}, {"EEE", "short"},
                                                     {"ccccc", "narrow"}, {"cccc", "long"}, {"ccc", "short"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_ERA_PE = {{"GGGGG", "narrow"}, {"GGGG", "long"}, {"GGG", "short"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_YEAR_PE = {{"yy", "2-digit"}, {"y", "numeric"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_MONTH_PE = {
    {"MMMMM", "narrow"}, {"MMMM", "long"}, {"MMM", "short"}, {"MM", "2-digit"}, {"M", "numeric"},
    {"LLLLL", "narrow"}, {"LLLL", "long"}, {"LLL", "short"}, {"LL", "2-digit"}, {"L", "numeric"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_DAY_PE = {{"dd", "2-digit"}, {"d", "numeric"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_DAY_PERIOD_PE = {{"BBBBB", "narrow"}, {"bbbbb", "narrow"}, {"BBBB", "long"},
                                                        {"bbbb", "long"},    {"B", "short"},      {"b", "short"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_HOUR_PE = {{"HH", "2-digit"}, {"H", "numeric"},  {"hh", "2-digit"},
                                                  {"h", "numeric"},  {"kk", "2-digit"}, {"k", "numeric"},
                                                  {"KK", "2-digit"}, {"K", "numeric"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_MINUTE_PE = {{"mm", "2-digit"}, {"m", "numeric"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_SECOND_PE = {{"ss", "2-digit"}, {"s", "numeric"}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::vector<IcuPatternEntry> ICU_YIME_ZONE_NAME_PE = {{"zzzz", "long"}, {"z", "short"}};

// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::map<char16_t, HourCycleOption> HOUR_CYCLE_MAP = {
    {'K', HourCycleOption::H11}, {'h', HourCycleOption::H12}, {'H', HourCycleOption::H23}, {'k', HourCycleOption::H24}};
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::map<std::string, HourCycleOption> TO_HOUR_CYCLE_MAP = {{"h11", HourCycleOption::H11},
                                                                  {"h12", HourCycleOption::H12},
                                                                  {"h23", HourCycleOption::H23},
                                                                  {"h24", HourCycleOption::H24}};

// The value of the [[RelevantExtensionKeys]] internal slot is « "ca", "nu", "hc" ».
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
const std::set<std::string> RELEVANT_EXTENSION_KEYS = {"nu", "ca", "hc"};

}  // namespace

icu::Locale *JSDateTimeFormat::GetIcuLocale() const
{
    ASSERT(GetLocaleIcu().IsJSNativePointer());
    auto result = JSNativePointer::Cast(GetLocaleIcu().GetTaggedObject())->GetExternalPointer();
    return reinterpret_cast<icu::Locale *>(result);
}

/* static */
void JSDateTimeFormat::SetIcuLocale(JSThread *thread, JSHandle<JSDateTimeFormat> obj, const icu::Locale &icu_locale,
                                    const DeleteEntryPoint &callback)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    icu::Locale *icu_pointer = Runtime::GetCurrent()->GetInternalAllocator()->New<icu::Locale>(icu_locale);
    ASSERT(icu_pointer != nullptr);
    JSTaggedValue data = obj->GetLocaleIcu();
    if (data.IsHeapObject() && data.IsJSNativePointer()) {
        JSNativePointer *native = JSNativePointer::Cast(data.GetTaggedObject());
        native->ResetExternalPointer(icu_pointer);
        return;
    }
    JSHandle<JSNativePointer> pointer = factory->NewJSNativePointer(icu_pointer, callback, ecma_vm);
    obj->SetLocaleIcu(thread, pointer.GetTaggedValue());
    ecma_vm->PushToArrayDataList(*pointer);
}

void JSDateTimeFormat::FreeIcuLocale(void *pointer, [[maybe_unused]] void *data)
{
    if (pointer == nullptr) {
        return;
    }
    auto icu_locale = reinterpret_cast<icu::Locale *>(pointer);
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(icu_locale);
}

icu::SimpleDateFormat *JSDateTimeFormat::GetIcuSimpleDateFormat() const
{
    ASSERT(GetSimpleDateTimeFormatIcu().IsJSNativePointer());
    auto result = JSNativePointer::Cast(GetSimpleDateTimeFormatIcu().GetTaggedObject())->GetExternalPointer();
    return reinterpret_cast<icu::SimpleDateFormat *>(result);
}

/* static */
void JSDateTimeFormat::SetIcuSimpleDateFormat(JSThread *thread, JSHandle<JSDateTimeFormat> obj,
                                              const icu::SimpleDateFormat &icu_simple_date_time_format,
                                              const DeleteEntryPoint &callback)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    auto allocator = Runtime::GetCurrent()->GetInternalAllocator();
    icu::SimpleDateFormat *icu_pointer = allocator->New<icu::SimpleDateFormat>(icu_simple_date_time_format);
    ASSERT(icu_pointer != nullptr);
    JSTaggedValue data = obj->GetSimpleDateTimeFormatIcu();
    if (data.IsHeapObject() && data.IsJSNativePointer()) {
        JSNativePointer *native = JSNativePointer::Cast(data.GetTaggedObject());
        native->ResetExternalPointer(icu_pointer);
        return;
    }
    JSHandle<JSNativePointer> pointer = factory->NewJSNativePointer(icu_pointer, callback, ecma_vm);
    obj->SetSimpleDateTimeFormatIcu(thread, pointer.GetTaggedValue());
    ecma_vm->PushToArrayDataList(*pointer);
}

void JSDateTimeFormat::FreeSimpleDateFormat(void *pointer, [[maybe_unused]] void *data)
{
    if (pointer == nullptr) {
        return;
    }
    auto icu_simple_date_format = reinterpret_cast<icu::SimpleDateFormat *>(pointer);
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(icu_simple_date_format);
}

JSHandle<EcmaString> JSDateTimeFormat::ToValueString(JSThread *thread, const Value value)
{
    auto global_const = thread->GlobalConstants();
    JSMutableHandle<EcmaString> result(thread, JSTaggedValue::Undefined());
    switch (value) {
        case Value::SHARED:
            result.Update(global_const->GetHandledSharedString().GetTaggedValue());
            break;
        case Value::START_RANGE:
            result.Update(global_const->GetHandledStartRangeString().GetTaggedValue());
            break;
        case Value::END_RANGE:
            result.Update(global_const->GetHandledEndRangeString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

// 13.1.1 InitializeDateTimeFormat (dateTimeFormat, locales, options)
// NOLINTNEXTLINE(readability-function-size)
JSHandle<JSDateTimeFormat> JSDateTimeFormat::InitializeDateTimeFormat(
    JSThread *thread, const JSHandle<JSDateTimeFormat> &date_time_format, const JSHandle<JSTaggedValue> &locales,
    const JSHandle<JSTaggedValue> &options)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();
    const GlobalEnvConstants *global_const = thread->GlobalConstants();

    // 1. Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);

    // 2. Let options be ? ToDateTimeOptions(options, "any", "date").
    JSHandle<JSObject> date_time_options =
        ToDateTimeOptions(thread, options, RequiredOption::ANY, DefaultsOption::DATE);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);

    // 4. Let matcher be ? GetOption(options, "localeMatcher", "string", « "lookup", "best fit" », "best fit").
    auto matcher = JSLocale::GetOptionOfString<LocaleMatcherOption>(
        thread, date_time_options, global_const->GetHandledLocaleMatcherString(),
        {LocaleMatcherOption::LOOKUP, LocaleMatcherOption::BEST_FIT}, {"lookup", "best fit"},
        LocaleMatcherOption::BEST_FIT);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);

    // 6. Let calendar be ? GetOption(options, "calendar", "string", undefined, undefined).
    JSHandle<JSTaggedValue> calendar =
        JSLocale::GetOption(thread, date_time_options, global_const->GetHandledCalendarString(), OptionType::STRING,
                            global_const->GetHandledUndefined(), global_const->GetHandledUndefined());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);
    date_time_format->SetCalendar(thread, calendar);

    // 7. If calendar is not undefined, then
    //    a. If calendar does not match the Unicode Locale Identifier type nonterminal, throw a RangeError exception.
    std::string calendar_str;
    if (!calendar->IsUndefined()) {
        JSHandle<EcmaString> calendar_ecma_str = JSHandle<EcmaString>::Cast(calendar);
        calendar_str = JSLocale::ConvertToStdString(calendar_ecma_str);
        if (!JSLocale::IsNormativeCalendar(calendar_str)) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "invalid calendar", date_time_format);
        }
    }

    // 9. Let numberingSystem be ? GetOption(options, "numberingSystem", "string", undefined, undefined).
    JSHandle<JSTaggedValue> numbering_system = JSLocale::GetOption(
        thread, date_time_options, global_const->GetHandledNumberingSystemString(), OptionType::STRING,
        global_const->GetHandledUndefined(), global_const->GetHandledUndefined());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);
    date_time_format->SetNumberingSystem(thread, numbering_system);

    // 10. If numberingSystem is not undefined, then
    //     a. If numberingSystem does not match the Unicode Locale Identifier type nonterminal, throw a RangeError
    //        exception.
    std::string ns_str;
    if (!numbering_system->IsUndefined()) {
        JSHandle<EcmaString> ns_ecma_str = JSHandle<EcmaString>::Cast(numbering_system);
        ns_str = JSLocale::ConvertToStdString(ns_ecma_str);
        if (!JSLocale::IsWellNumberingSystem(ns_str)) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "invalid numberingSystem", date_time_format);
        }
    }

    // 12. Let hour12 be ? GetOption(options, "hour12", "boolean", undefined, undefined).
    JSHandle<JSTaggedValue> hour12 =
        JSLocale::GetOption(thread, date_time_options, global_const->GetHandledHour12String(), OptionType::BOOLEAN,
                            global_const->GetHandledUndefined(), global_const->GetHandledUndefined());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);

    // 13. Let hourCycle be ? GetOption(options, "hourCycle", "string", « "h11", "h12", "h23", "h24" », undefined).
    auto hour_cycle = JSLocale::GetOptionOfString<HourCycleOption>(
        thread, date_time_options, global_const->GetHandledHourCycleString(),
        {HourCycleOption::H11, HourCycleOption::H12, HourCycleOption::H23, HourCycleOption::H24},
        {"h11", "h12", "h23", "h24"}, HourCycleOption::UNDEFINED);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);

    // 14. If hour12 is not undefined, then
    //     a. Let hourCycle be null.
    if (!hour12->IsUndefined()) {
        hour_cycle = HourCycleOption::UNDEFINED;
    }

    // 16. Let localeData be %DateTimeFormat%.[[LocaleData]].
    JSHandle<TaggedArray> available_locales =
        (requested_locales->GetLength() == 0) ? factory->EmptyArray() : GainAvailableLocales(thread);

    // 17. Let r be ResolveLocale(%DateTimeFormat%.[[AvailableLocales]], requestedLocales, opt, %DateTimeFormat%
    //     .[[RelevantExtensionKeys]], localeData).
    ResolvedLocale resolved_locale =
        JSLocale::ResolveLocale(thread, available_locales, requested_locales, matcher, RELEVANT_EXTENSION_KEYS);

    // 18. Set icuLocale to r.[[locale]].
    icu::Locale icu_locale = resolved_locale.locale_data;
    ASSERT_PRINT(!icu_locale.isBogus(), "icuLocale is bogus");
    UErrorCode status = U_ZERO_ERROR;

    // Set resolvedIcuLocaleCopy to a copy of icuLocale.
    // Set icuLocale.[[ca]] to calendar.
    // Set icuLocale.[[nu]] to numberingSystem.
    icu::Locale resolved_icu_locale_copy(icu_locale);
    if (!calendar->IsUndefined() && JSLocale::IsWellCalendar(icu_locale, calendar_str)) {
        icu_locale.setUnicodeKeywordValue("ca", calendar_str, status);
    }
    if (!numbering_system->IsUndefined() && JSLocale::IsWellNumberingSystem(ns_str)) {
        icu_locale.setUnicodeKeywordValue("nu", ns_str, status);
    }

    // 24. Let timeZone be ? Get(options, "timeZone").
    OperationResult operation_result =
        JSObject::GetProperty(thread, date_time_options, global_const->GetHandledTimeZoneString());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);
    date_time_format->SetTimeZone(thread, operation_result.GetValue());

    // 25. If timeZone is not undefined, then
    //     a. Let timeZone be ? ToString(timeZone).
    //     b. If the result of IsValidTimeZoneName(timeZone) is false, then
    //        i. Throw a RangeError exception.
    std::unique_ptr<icu::TimeZone> icu_time_zone;
    if (!operation_result.GetValue()->IsUndefined()) {
        JSHandle<EcmaString> timezone = JSTaggedValue::ToString(thread, operation_result.GetValue());
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);
        icu_time_zone = ConstructTimeZone(JSLocale::ConvertToStdString(timezone));
        if (icu_time_zone == nullptr) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "invalid timeZone", date_time_format);
        }
    } else {
        // 26. Else,
        //     a. Let timeZone be DefaultTimeZone().
        icu_time_zone = std::unique_ptr<icu::TimeZone>(icu::TimeZone::createDefault());
    }

    // 36.a. Let hcDefault be dataLocaleData.[[hourCycle]].
    std::unique_ptr<icu::DateTimePatternGenerator> generator(
        icu::DateTimePatternGenerator::createInstance(icu_locale, status));
    ASSERT_PRINT(U_SUCCESS(status), "constructGenerator failed");
    HourCycleOption hc_default = OptionToHourCycle(generator->getDefaultHourCycle(status));
    // b. Let hc be dateTimeFormat.[[HourCycle]].
    HourCycleOption hc = hour_cycle;
    if (hc == HourCycleOption::UNDEFINED && resolved_locale.extensions.find("hc") != resolved_locale.extensions.end()) {
        hc = OptionToHourCycle(resolved_locale.extensions.find("hc")->second);
    }

    // c. If hc is null, then
    //    i. Set hc to hcDefault.
    if (hc == HourCycleOption::UNDEFINED) {
        hc = hc_default;
    }
    // d. If hour12 is not undefined, then
    if (!hour12->IsUndefined()) {
        // i. If hour12 is true, then
        if (JSTaggedValue::SameValue(hour12.GetTaggedValue(), JSTaggedValue::True())) {
            // 1. If hcDefault is "h11" or "h23", then
            if (hc_default == HourCycleOption::H11 || hc_default == HourCycleOption::H23) {
                // a. Set hc to "h11".
                hc = HourCycleOption::H11;
            } else {
                // 2. Else,
                //    a. Set hc to "h12".
                hc = HourCycleOption::H12;
            }
        } else {
            // ii. Else,
            //     2. If hcDefault is "h11" or "h23", then
            if (hc_default == HourCycleOption::H11 || hc_default == HourCycleOption::H23) {
                // a. Set hc to "h23".
                hc = HourCycleOption::H23;
            } else {
                // 3. Else,
                //    a. Set hc to "h24".
                hc = HourCycleOption::H24;
            }
        }
    }

    // Set isHourDefined be false when dateTimeFormat.[[Hour]] is not undefined.
    bool is_hour_defined = false;

    // 29. For each row of Table 6, except the header row, in table order, do
    //     a. Let prop be the name given in the Property column of the row.
    //     b. Let value be ? GetOption(options, prop, "string", « the strings given in the Values column of the
    //        row », undefined).
    //     c. Set opt.[[<prop>]] to value.
    std::string skeleton;
    std::vector<IcuPatternDesc> data = GetIcuPatternDesc(hc);
    for (const IcuPatternDesc &item : data) {
        // prop be [[TimeZoneName]]
        if (item.property == "timeZoneName") {
            int second_digits_string = JSLocale::GetNumberOption(
                thread, date_time_options, global_const->GetHandledFractionalSecondDigitsString(), 1, 3, 0);
            skeleton.append(second_digits_string, 'S');
        }
        JSHandle<JSTaggedValue> property(thread, factory->NewFromStdString(item.property).GetTaggedValue());
        std::string value;
        bool is_find = JSLocale::GetOptionOfString(thread, date_time_options, property, item.allowed_values, &value);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);
        if (is_find) {
            skeleton += item.map.find(value)->second;
            // [[Hour]] is defined.
            is_hour_defined = (item.property == "hour") ? true : is_hour_defined;
        }
    }

    // 13.1.3 BasicFormatMatcher (options, formats)
    [[maybe_unused]] auto format_matcher = JSLocale::GetOptionOfString<FormatMatcherOption>(
        thread, date_time_options, global_const->GetHandledFormatMatcherString(),
        {FormatMatcherOption::BASIC, FormatMatcherOption::BEST_FIT}, {"basic", "best fit"},
        FormatMatcherOption::BEST_FIT);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);

    // Let dateStyle be ? GetOption(options, "string", «"full", "long", "medium", "short"», undefined).
    // Set dateTimeFormat.[[dateStyle]]
    auto date_style = JSLocale::GetOptionOfString<DateTimeStyleOption>(
        thread, date_time_options, global_const->GetHandledDateStyleString(),
        {DateTimeStyleOption::FULL, DateTimeStyleOption::LONG, DateTimeStyleOption::MEDIUM, DateTimeStyleOption::SHORT},
        {"full", "long", "medium", "short"}, DateTimeStyleOption::UNDEFINED);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);
    date_time_format->SetDateStyle(thread, JSTaggedValue(static_cast<int32_t>(date_style)));

    // Let timeStyle be ? GetOption(options, "string", «"full", "long", "medium", "short"», undefined).
    // Set dateTimeFormat.[[timeStyle]]
    auto time_style = JSLocale::GetOptionOfString<DateTimeStyleOption>(
        thread, date_time_options, global_const->GetHandledTimeStyleString(),
        {DateTimeStyleOption::FULL, DateTimeStyleOption::LONG, DateTimeStyleOption::MEDIUM, DateTimeStyleOption::SHORT},
        {"full", "long", "medium", "short"}, DateTimeStyleOption::UNDEFINED);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSDateTimeFormat, thread);
    date_time_format->SetTimeStyle(thread, JSTaggedValue(static_cast<int32_t>(time_style)));

    HourCycleOption dtf_hour_cycle = HourCycleOption::UNDEFINED;

    // If dateTimeFormat.[[Hour]] is defined, then
    if (is_hour_defined) {
        // e. Set dateTimeFormat.[[HourCycle]] to hc.
        dtf_hour_cycle = hc;
    } else {
        // 37. Else,
        //     a. Set dateTimeFormat.[[HourCycle]] to undefined.
        dtf_hour_cycle = HourCycleOption::UNDEFINED;
    }

    // Set dateTimeFormat.[[hourCycle]].
    date_time_format->SetHourCycle(thread, JSTaggedValue(static_cast<int32_t>(dtf_hour_cycle)));

    // Set dateTimeFormat.[[icuLocale]].
    JSDateTimeFormat::SetIcuLocale(thread, date_time_format, icu_locale, JSDateTimeFormat::FreeIcuLocale);

    // Creates a Calendar using the given timezone and given locale.
    // Set dateTimeFormat.[[icuSimpleDateFormat]].
    icu::UnicodeString dtf_skeleton(skeleton.c_str());
    status = U_ZERO_ERROR;
    icu::UnicodeString pattern = ChangeHourCyclePattern(
        generator->getBestPattern(dtf_skeleton, UDATPG_MATCH_HOUR_FIELD_LENGTH, status), dtf_hour_cycle);
    ASSERT_PRINT((U_SUCCESS(status) != 0), "get best pattern failed");
    auto simple_date_format_icu(std::make_unique<icu::SimpleDateFormat>(pattern, icu_locale, status));
    if (U_FAILURE(status) != 0) {
        simple_date_format_icu = std::unique_ptr<icu::SimpleDateFormat>();
    }
    ASSERT_PRINT(simple_date_format_icu != nullptr, "invalid icuSimpleDateFormat");
    std::unique_ptr<icu::Calendar> calendar_ptr = BuildCalendar(icu_locale, *icu_time_zone);
    ASSERT_PRINT(calendar_ptr != nullptr, "invalid calendar");
    simple_date_format_icu->adoptCalendar(calendar_ptr.release());
    SetIcuSimpleDateFormat(thread, date_time_format, *simple_date_format_icu, JSDateTimeFormat::FreeSimpleDateFormat);

    // Set dateTimeFormat.[[iso8601]].
    bool iso8601 = strstr(icu_locale.getName(), "calendar=iso8601") != nullptr;
    date_time_format->SetIso8601(thread, JSTaggedValue(iso8601));

    // Set dateTimeFormat.[[locale]].
    if (!hour12->IsUndefined() || hour_cycle != HourCycleOption::UNDEFINED) {
        if ((resolved_locale.extensions.find("hc") != resolved_locale.extensions.end()) &&
            (dtf_hour_cycle != OptionToHourCycle((resolved_locale.extensions.find("hc")->second)))) {
            resolved_icu_locale_copy.setUnicodeKeywordValue("hc", nullptr, status);
            ASSERT_PRINT(U_SUCCESS(status), "resolvedIcuLocaleCopy set hc failed");
        }
    }
    JSHandle<EcmaString> locale_str = JSLocale::ToLanguageTag(thread, resolved_icu_locale_copy);
    date_time_format->SetLocale(thread, locale_str.GetTaggedValue());

    // Set dateTimeFormat.[[boundFormat]].
    date_time_format->SetBoundFormat(thread, JSTaggedValue::Undefined());

    // 39. Return dateTimeFormat.
    return date_time_format;
}

// 13.1.2 ToDateTimeOptions (options, required, defaults)
JSHandle<JSObject> JSDateTimeFormat::ToDateTimeOptions(JSThread *thread, const JSHandle<JSTaggedValue> &options,
                                                       const RequiredOption &required, const DefaultsOption &defaults)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1. If options is undefined, let options be null; otherwise let options be ? ToObject(options).
    JSHandle<JSObject> options_result(thread, JSTaggedValue::Null());
    if (!options->IsUndefined()) {
        options_result = JSTaggedValue::ToObject(thread, options);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread);
    }

    // 2. Let options be ObjectCreate(options).
    options_result = JSObject::ObjectCreate(thread, options_result);

    // 3. Let needDefaults be true.
    bool need_defaults = true;

    // 4. If required is "date" or "any", then
    //    a. For each of the property names "weekday", "year", "month", "day", do
    //      i. Let prop be the property name.
    //      ii. Let value be ? Get(options, prop).
    //      iii. If value is not undefined, let needDefaults be false.
    auto global_const = thread->GlobalConstants();
    if (required == RequiredOption::DATE || required == RequiredOption::ANY) {
        JSHandle<TaggedArray> array = factory->NewTaggedArray(CAPACITY_4);
        array->Set(thread, 0, global_const->GetHandledWeekdayString());
        array->Set(thread, 1, global_const->GetHandledYearString());
        array->Set(thread, 2, global_const->GetHandledMonthString());  // 2 means the third slot
        array->Set(thread, 3, global_const->GetHandledDayString());    // 3 means the fourth slot
        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        uint32_t len = array->GetLength();
        for (uint32_t i = 0; i < len; i++) {
            key.Update(array->Get(thread, i));
            OperationResult operation_result = JSObject::GetProperty(thread, options_result, key);
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread);
            if (!operation_result.GetValue()->IsUndefined()) {
                need_defaults = false;
            }
        }
    }

    // 5. If required is "time" or "any", then
    //    a. For each of the property names "dayPeriod", "hour", "minute", "second", "fractionalSecondDigits", do
    //      i. Let prop be the property name.
    //      ii. Let value be ? Get(options, prop).
    //      iii. If value is not undefined, let needDefaults be false.
    if (required == RequiredOption::TIME || required == RequiredOption::ANY) {
        JSHandle<TaggedArray> array = factory->NewTaggedArray(CAPACITY_5);
        array->Set(thread, 0, global_const->GetHandledDayPeriodString());
        array->Set(thread, 1, global_const->GetHandledHourString());
        array->Set(thread, 2, global_const->GetHandledMinuteString());                  // 2 means the second slot
        array->Set(thread, 3, global_const->GetHandledSecondString());                  // 3 means the third slot
        array->Set(thread, 4, global_const->GetHandledFractionalSecondDigitsString());  // 4 means the fourth slot
        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        uint32_t len = array->GetLength();
        for (uint32_t i = 0; i < len; i++) {
            key.Update(array->Get(thread, i));
            OperationResult operation_result = JSObject::GetProperty(thread, options_result, key);
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread);
            if (!operation_result.GetValue()->IsUndefined()) {
                need_defaults = false;
            }
        }
    }

    // Let dateStyle/timeStyle be ? Get(options, "dateStyle"/"timeStyle").
    OperationResult date_style_result =
        JSObject::GetProperty(thread, options_result, global_const->GetHandledDateStyleString());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread);
    JSHandle<JSTaggedValue> date_style = date_style_result.GetValue();
    OperationResult time_style_result =
        JSObject::GetProperty(thread, options_result, global_const->GetHandledTimeStyleString());
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread);
    JSHandle<JSTaggedValue> time_style = time_style_result.GetValue();

    // If dateStyle is not undefined or timeStyle is not undefined, let needDefaults be false.
    if (!date_style->IsUndefined() || !time_style->IsUndefined()) {
        need_defaults = false;
    }

    // If required is "date"/"time" and timeStyle is not undefined, throw a TypeError exception.
    if (required == RequiredOption::DATE && !time_style->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "timeStyle is not undefined", options_result);
    }
    if (required == RequiredOption::TIME && !date_style->IsUndefined()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "dateStyle is not undefined", options_result);
    }

    // 6. If needDefaults is true and defaults is either "date" or "all", then
    //    a. For each of the property names "year", "month", "day", do
    //       i. Perform ? CreateDataPropertyOrThrow(options, prop, "numeric").
    if (need_defaults && (defaults == DefaultsOption::DATE || defaults == DefaultsOption::ALL)) {
        JSHandle<TaggedArray> array = factory->NewTaggedArray(CAPACITY_3);
        array->Set(thread, 0, global_const->GetHandledYearString());
        array->Set(thread, 1, global_const->GetHandledMonthString());
        array->Set(thread, 2, global_const->GetHandledDayString());  // 2 means the third slot
        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        uint32_t len = array->GetLength();
        for (uint32_t i = 0; i < len; i++) {
            key.Update(array->Get(thread, i));
            JSObject::CreateDataPropertyOrThrow(thread, options_result, key, global_const->GetHandledNumericString());
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread);
        }
    }

    // 7. If needDefaults is true and defaults is either "time" or "all", then
    //    a. For each of the property names "hour", "minute", "second", do
    //       i. Perform ? CreateDataPropertyOrThrow(options, prop, "numeric").
    if (need_defaults && (defaults == DefaultsOption::TIME || defaults == DefaultsOption::ALL)) {
        JSHandle<TaggedArray> array = factory->NewTaggedArray(CAPACITY_3);
        array->Set(thread, 0, global_const->GetHandledHourString());
        array->Set(thread, 1, global_const->GetHandledMinuteString());
        array->Set(thread, 2, global_const->GetHandledSecondString());  // 2 means the third slot
        JSMutableHandle<JSTaggedValue> key(thread, JSTaggedValue::Undefined());
        uint32_t len = array->GetLength();
        for (uint32_t i = 0; i < len; i++) {
            key.Update(array->Get(thread, i));
            JSObject::CreateDataPropertyOrThrow(thread, options_result, key, global_const->GetHandledNumericString());
            RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSObject, thread);
        }
    }

    // 8. Return options.
    return options_result;
}

// 13.1.7 FormatDateTime(dateTimeFormat, x)
JSHandle<EcmaString> JSDateTimeFormat::FormatDateTime(JSThread *thread,
                                                      const JSHandle<JSDateTimeFormat> &date_time_format, double x)
{
    icu::SimpleDateFormat *simple_date_format = date_time_format->GetIcuSimpleDateFormat();
    // 1. Let parts be ? PartitionDateTimePattern(dateTimeFormat, x).
    double x_value = JSDate::TimeClip(x);
    if (std::isnan(x_value)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "Invalid time value", thread->GetEcmaVM()->GetFactory()->GetEmptyString());
    }

    // 2. Let result be the empty String.
    icu::UnicodeString result;

    // 3. Set result to the string-concatenation of result and part.[[Value]].
    simple_date_format->format(x_value, result);

    // 4. Return result.
    return JSLocale::IcuToString(thread, result);
}

// 13.1.8 FormatDateTimeToParts (dateTimeFormat, x)
JSHandle<JSArray> JSDateTimeFormat::FormatDateTimeToParts(JSThread *thread,
                                                          const JSHandle<JSDateTimeFormat> &date_time_format, double x)
{
    icu::SimpleDateFormat *simple_date_format = date_time_format->GetIcuSimpleDateFormat();
    ASSERT(simple_date_format != nullptr);
    UErrorCode status = U_ZERO_ERROR;
    icu::FieldPositionIterator field_position_iter;
    icu::UnicodeString formatted_parts;
    simple_date_format->format(x, formatted_parts, &field_position_iter, status);
    if (U_FAILURE(status) != 0) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "format failed", thread->GetEcmaVM()->GetFactory()->NewJSArray());
    }

    // 2. Let result be ArrayCreate(0).
    JSHandle<JSArray> result(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    if (formatted_parts.isBogus() != 0) {
        return result;
    }

    // 3. Let n be 0.
    int32_t index = 0;
    int32_t pre_edge_pos = 0;
    std::vector<CommonDateFormatPart> parts;
    icu::FieldPosition field_position;
    while (field_position_iter.next(field_position) != 0) {
        int32_t f_field = field_position.getField();
        int32_t f_begin_index = field_position.getBeginIndex();
        int32_t f_end_index = field_position.getEndIndex();
        if (pre_edge_pos < f_begin_index) {
            parts.emplace_back(CommonDateFormatPart(f_field, pre_edge_pos, f_begin_index, index, true));
            ++index;
        }
        parts.emplace_back(CommonDateFormatPart(f_field, f_begin_index, f_end_index, index, false));
        pre_edge_pos = f_end_index;
        ++index;
    }
    int32_t length = formatted_parts.length();
    if (pre_edge_pos < length) {
        parts.emplace_back(CommonDateFormatPart(-1, pre_edge_pos, length, index, true));
    }
    JSMutableHandle<EcmaString> substring(thread, JSTaggedValue::Undefined());

    // 4. For each part in parts, do
    for (auto part : parts) {
        substring.Update(
            JSLocale::IcuToString(thread, formatted_parts, part.f_begin_index, part.f_end_index).GetTaggedValue());
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSArray, thread);
        // Let O be ObjectCreate(%ObjectPrototype%).
        // Perform ! CreateDataPropertyOrThrow(O, "type", part.[[Type]]).
        // Perform ! CreateDataPropertyOrThrow(O, "value", part.[[Value]]).
        // Perform ! CreateDataProperty(result, ! ToString(n), O).
        if (part.is_pre_exist) {
            JSLocale::PutElement(thread, part.index, result, ConvertFieldIdToDateType(thread, -1),
                                 JSHandle<JSTaggedValue>::Cast(substring));
        } else {
            JSLocale::PutElement(thread, part.index, result, ConvertFieldIdToDateType(thread, part.f_field),
                                 JSHandle<JSTaggedValue>::Cast(substring));
        }
    }

    // 5. Return result.
    return result;
}

// 13.1.10 UnwrapDateTimeFormat(dtf)
JSHandle<JSTaggedValue> JSDateTimeFormat::UnwrapDateTimeFormat(JSThread *thread,
                                                               const JSHandle<JSTaggedValue> &date_time_format)
{
    // 1. Assert: Type(dtf) is Object.
    ASSERT_PRINT(date_time_format->IsJSObject(), "dateTimeFormat is not object");

    // 2. If dateTimeFormat does not have an [[InitializedDateTimeFormat]] internal slot
    //    and ? InstanceofOperator(dateTimeFormat, %DateTimeFormat%) is true, then
    //       a. Let dateTimeFormat be ? Get(dateTimeFormat, %Intl%.[[FallbackSymbol]]).
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    bool is_instance_of = JSFunction::InstanceOf(thread, date_time_format, env->GetDateTimeFormatFunction());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, date_time_format);
    if (!date_time_format->IsJSDateTimeFormat() && is_instance_of) {
        JSHandle<JSTaggedValue> key(thread, JSHandle<JSIntl>::Cast(env->GetIntlFunction())->GetFallbackSymbol());
        OperationResult operation_result = JSTaggedValue::GetProperty(thread, date_time_format, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, date_time_format);
        return operation_result.GetValue();
    }

    // 3. Perform ? RequireInternalSlot(dateTimeFormat, [[InitializedDateTimeFormat]]).
    if (!date_time_format->IsJSDateTimeFormat()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "is not JSDateTimeFormat",
                                    JSHandle<JSTaggedValue>(thread, JSTaggedValue::Exception()));
    }

    // 4. Return dateTimeFormat.
    return date_time_format;
}

JSHandle<JSTaggedValue> ToHourCycleEcmaString(JSThread *thread, int32_t hc)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (hc) {
        case static_cast<int32_t>(HourCycleOption::H11):
            result.Update(global_const->GetHandledH11String().GetTaggedValue());
            break;
        case static_cast<int32_t>(HourCycleOption::H12):
            result.Update(global_const->GetHandledH12String().GetTaggedValue());
            break;
        case static_cast<int32_t>(HourCycleOption::H23):
            result.Update(global_const->GetHandledH23String().GetTaggedValue());
            break;
        case static_cast<int32_t>(HourCycleOption::H24):
            result.Update(global_const->GetHandledH24String().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

JSHandle<JSTaggedValue> ToDateTimeStyleEcmaString(JSThread *thread, int32_t style)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    switch (style) {
        case static_cast<int32_t>(DateTimeStyleOption::FULL):
            result.Update(global_const->GetHandledFullString().GetTaggedValue());
            break;
        case static_cast<int32_t>(DateTimeStyleOption::LONG):
            result.Update(global_const->GetHandledLongString().GetTaggedValue());
            break;
        case static_cast<int32_t>(DateTimeStyleOption::MEDIUM):
            result.Update(global_const->GetHandledMediumString().GetTaggedValue());
            break;
        case static_cast<int32_t>(DateTimeStyleOption::SHORT):
            result.Update(global_const->GetHandledShortString().GetTaggedValue());
            break;
        default:
            UNREACHABLE();
    }
    return result;
}

// 13.4.5  Intl.DateTimeFormat.prototype.resolvedOptions ()
void JSDateTimeFormat::ResolvedOptions(JSThread *thread, const JSHandle<JSDateTimeFormat> &date_time_format,
                                       const JSHandle<JSObject> &options)
{  //  Table 8: Resolved Options of DateTimeFormat Instances
    //    Internal Slot	        Property
    //    [[Locale]]	        "locale"
    //    [[Calendar]]	        "calendar"
    //    [[NumberingSystem]]	"numberingSystem"
    //    [[TimeZone]]	        "timeZone"
    //    [[HourCycle]]	        "hourCycle"
    //                          "hour12"
    //    [[Weekday]]	        "weekday"
    //    [[Era]]	            "era"
    //    [[Year]]	            "year"
    //    [[Month]]         	"month"
    //    [[Day]]	            "day"
    //    [[Hour]]	            "hour"
    //    [[Minute]]	        "minute"
    //    [[Second]]        	"second"
    //    [[TimeZoneName]]	    "timeZoneName"
    auto global_const = thread->GlobalConstants();
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();

    // 5. For each row of Table 8, except the header row, in table order, do
    //    Let p be the Property value of the current row.
    // [[Locale]]
    JSHandle<JSTaggedValue> locale(thread, date_time_format->GetLocale());
    JSHandle<JSTaggedValue> property = global_const->GetHandledLocaleString();
    JSObject::CreateDataPropertyOrThrow(thread, options, property, locale);
    // [[Calendar]]
    JSMutableHandle<JSTaggedValue> calendar_value(thread, date_time_format->GetCalendar());
    icu::SimpleDateFormat *icu_simple_date_format = date_time_format->GetIcuSimpleDateFormat();
    const icu::Calendar *calendar = icu_simple_date_format->getCalendar();
    std::string icu_calendar = calendar->getType();
    if (icu_calendar == "gregorian") {
        if (date_time_format->GetIso8601() == JSTaggedValue::True()) {
            calendar_value.Update(global_const->GetHandledIso8601String().GetTaggedValue());
        } else {
            calendar_value.Update(global_const->GetHandledGregoryString().GetTaggedValue());
        }
    } else if (icu_calendar == "ethiopic-amete-alem") {
        calendar_value.Update(global_const->GetHandledEthioaaString().GetTaggedValue());
    }
    property = global_const->GetHandledCalendarString();
    JSObject::CreateDataPropertyOrThrow(thread, options, property, calendar_value);
    // [[NumberingSystem]]
    JSHandle<JSTaggedValue> numbering_system(thread, date_time_format->GetNumberingSystem());
    if (numbering_system->IsUndefined()) {
        numbering_system = global_const->GetHandledLatnString();
    }
    property = global_const->GetHandledNumberingSystemString();
    JSObject::CreateDataPropertyOrThrow(thread, options, property, numbering_system);
    // [[TimeZone]]
    JSMutableHandle<JSTaggedValue> timezone_value(thread, date_time_format->GetTimeZone());
    const icu::TimeZone &icu_tz = calendar->getTimeZone();
    icu::UnicodeString timezone;
    icu_tz.getID(timezone);
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString canonical_timezone;
    icu::TimeZone::getCanonicalID(timezone, canonical_timezone, status);
    if (U_SUCCESS(status) != 0) {
        if ((canonical_timezone == UNICODE_STRING_SIMPLE("Etc/UTC")) != 0 ||
            (canonical_timezone == UNICODE_STRING_SIMPLE("Etc/GMT")) != 0) {
            timezone_value.Update(global_const->GetUTCString());
        } else {
            timezone_value.Update(JSLocale::IcuToString(thread, canonical_timezone).GetTaggedValue());
        }
    }
    property = global_const->GetHandledTimeZoneString();
    JSObject::CreateDataPropertyOrThrow(thread, options, property, timezone_value);
    // [[HourCycle]]
    // For web compatibility reasons, if the property "hourCycle" is set, the "hour12" property should be set to true
    // when "hourCycle" is "h11" or "h12", or to false when "hourCycle" is "h23" or "h24".
    // i. Let hc be dtf.[[HourCycle]].
    JSHandle<JSTaggedValue> hc_value;
    HourCycleOption hc = static_cast<HourCycleOption>(date_time_format->GetHourCycle().GetInt());
    if (hc != HourCycleOption::UNDEFINED) {
        property = global_const->GetHandledHourCycleString();
        hc_value = ToHourCycleEcmaString(thread, date_time_format->GetHourCycle().GetInt());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, hc_value);
        if (hc == HourCycleOption::H11 || hc == HourCycleOption::H12) {
            JSHandle<JSTaggedValue> true_value(thread, JSTaggedValue::True());
            hc_value = true_value;
        } else if (hc == HourCycleOption::H23 || hc == HourCycleOption::H24) {
            JSHandle<JSTaggedValue> false_value(thread, JSTaggedValue::False());
            hc_value = false_value;
        }
        property = global_const->GetHandledHour12String();
        JSObject::CreateDataPropertyOrThrow(thread, options, property, hc_value);
    }
    // [[DateStyle]], [[TimeStyle]].
    icu::UnicodeString pattern_unicode;
    icu_simple_date_format->toPattern(pattern_unicode);
    std::string pattern;
    pattern_unicode.toUTF8String(pattern);
    if (date_time_format->GetDateStyle() == JSTaggedValue(static_cast<int32_t>(DateTimeStyleOption::UNDEFINED)) &&
        date_time_format->GetTimeStyle() == JSTaggedValue(static_cast<int32_t>(DateTimeStyleOption::UNDEFINED))) {
        for (const auto &item : BuildIcuPatternDescs()) {
            // fractionalSecondsDigits need to be added before timeZoneName.
            if (item.property == "timeZoneName") {
                int tmp_result = count(pattern.begin(), pattern.end(), 'S');
                int fsd = (tmp_result >= STRING_LENGTH_3) ? STRING_LENGTH_3 : tmp_result;
                if (fsd > 0) {
                    JSHandle<JSTaggedValue> fsd_value(thread, JSTaggedValue(fsd));
                    property = global_const->GetHandledFractionalSecondDigitsString();
                    JSObject::CreateDataPropertyOrThrow(thread, options, property, fsd_value);
                }
            }
            property = JSHandle<JSTaggedValue>::Cast(factory->NewFromStdString(item.property));
            for (const auto &pair : item.pairs) {
                if (pattern.find(pair.first) != std::string::npos) {
                    hc_value = JSHandle<JSTaggedValue>::Cast(factory->NewFromStdString(pair.second));
                    JSObject::CreateDataPropertyOrThrow(thread, options, property, hc_value);
                    break;
                }
            }
        }
    }
    if (date_time_format->GetDateStyle() != JSTaggedValue(static_cast<int32_t>(DateTimeStyleOption::UNDEFINED))) {
        property = global_const->GetHandledDateStyleString();
        hc_value = ToDateTimeStyleEcmaString(thread, date_time_format->GetDateStyle().GetInt());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, hc_value);
    }
    if (date_time_format->GetTimeStyle() != JSTaggedValue(static_cast<int32_t>(DateTimeStyleOption::UNDEFINED))) {
        property = global_const->GetHandledTimeStyleString();
        hc_value = ToDateTimeStyleEcmaString(thread, date_time_format->GetTimeStyle().GetInt());
        JSObject::CreateDataPropertyOrThrow(thread, options, property, hc_value);
    }
}

// Use dateInterval(x, y) construct datetimeformatrange
icu::FormattedDateInterval JSDateTimeFormat::ConstructDTFRange(JSThread *thread, const JSHandle<JSDateTimeFormat> &dtf,
                                                               double x, double y)
{
    std::unique_ptr<icu::DateIntervalFormat> date_interval_format(ConstructDateIntervalFormat(dtf));
    if (date_interval_format == nullptr) {
        icu::FormattedDateInterval empty_value;
        THROW_TYPE_ERROR_AND_RETURN(thread, "create dateIntervalFormat failed", empty_value);
    }
    UErrorCode status = U_ZERO_ERROR;
    icu::DateInterval date_interval(x, y);
    icu::FormattedDateInterval formatted = date_interval_format->formatToValue(date_interval, status);
    return formatted;
}

JSHandle<EcmaString> JSDateTimeFormat::NormDateTimeRange(JSThread *thread, const JSHandle<JSDateTimeFormat> &dtf,
                                                         double x, double y)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<EcmaString> result = factory->GetEmptyString();
    // 1. Let x be TimeClip(x).
    x = JSDate::TimeClip(x);
    // 2. If x is NaN, throw a RangeError exception.
    if (std::isnan(x)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "x is NaN", result);
    }
    // 3. Let y be TimeClip(y).
    y = JSDate::TimeClip(y);
    // 4. If y is NaN, throw a RangeError exception.
    if (std::isnan(y)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "y is NaN", result);
    }

    icu::FormattedDateInterval formatted = ConstructDTFRange(thread, dtf, x, y);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(EcmaString, thread);

    // Formatted to string.
    bool output_range = false;
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString format_result = formatted.toString(status);
    if (U_FAILURE(status) != 0) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "format to string failed",
                                    thread->GetEcmaVM()->GetFactory()->GetEmptyString());
    }
    icu::ConstrainedFieldPosition cfpos;
    while (formatted.nextPosition(cfpos, status) != 0) {
        if (cfpos.getCategory() == UFIELD_CATEGORY_DATE_INTERVAL_SPAN) {
            output_range = true;
            break;
        }
    }
    result = JSLocale::IcuToString(thread, format_result);
    if (!output_range) {
        return FormatDateTime(thread, dtf, x);
    }
    return result;
}

JSHandle<JSArray> JSDateTimeFormat::NormDateTimeRangeToParts(JSThread *thread, const JSHandle<JSDateTimeFormat> &dtf,
                                                             double x, double y)
{
    JSHandle<JSArray> result(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    // 1. Let x be TimeClip(x).
    x = JSDate::TimeClip(x);
    // 2. If x is NaN, throw a RangeError exception.
    if (std::isnan(x)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "x is invalid time value", result);
    }
    // 3. Let y be TimeClip(y).
    y = JSDate::TimeClip(y);
    // 4. If y is NaN, throw a RangeError exception.
    if (std::isnan(y)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "y is invalid time value", result);
    }

    icu::FormattedDateInterval formatted = ConstructDTFRange(thread, dtf, x, y);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSArray, thread);
    return ConstructFDateIntervalToJSArray(thread, formatted);
}

JSHandle<TaggedArray> JSDateTimeFormat::GainAvailableLocales(JSThread *thread)
{
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> date_time_format_locales = env->GetDateTimeFormatLocales();
    const char *key = "calendar";
    const char *path = nullptr;
    if (date_time_format_locales->IsUndefined()) {
        JSHandle<TaggedArray> available_locales = JSLocale::GetAvailableLocales(thread, key, path);
        env->SetDateTimeFormatLocales(thread, available_locales);
        return available_locales;
    }
    return JSHandle<TaggedArray>::Cast(date_time_format_locales);
}

JSHandle<JSArray> JSDateTimeFormat::ConstructFDateIntervalToJSArray(JSThread *thread,
                                                                    const icu::FormattedDateInterval &formatted)
{
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString formatted_value = formatted.toTempString(status);
    // Let result be ArrayCreate(0).
    JSHandle<JSArray> array(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    // Let index be 0.
    int index = 0;
    int32_t pre_end_pos = 0;
    std::array<int32_t, 2> begin {};
    std::array<int32_t, 2> end {};
    begin[0] = begin[1] = end[0] = end[1] = 0;
    std::vector<CommonDateFormatPart> parts;

    /**
     * From ICU header file document @unumberformatter.h
     * Sets a constraint on the field category.
     *
     * When this instance of ConstrainedFieldPosition is passed to FormattedValue#nextPosition,
     * positions are skipped unless they have the given category.
     *
     * Any previously set constraints are cleared.
     *
     * For example, to loop over only the number-related fields:
     *
     *     ConstrainedFieldPosition cfpo;
     *     cfpo.constrainCategory(UFIELDCATEGORY_NUMBER_FORMAT);
     *     while (fmtval.nextPosition(cfpo, status)) {
     *         // handle the number-related field position
     *     }
     */
    JSMutableHandle<EcmaString> substring(thread, JSTaggedValue::Undefined());
    icu::ConstrainedFieldPosition cfpos;
    while (formatted.nextPosition(cfpos, status) != 0) {
        int32_t f_category = cfpos.getCategory();
        int32_t f_field = cfpos.getField();
        int32_t f_start = cfpos.getStart();
        int32_t f_limit = cfpos.getLimit();

        // 2 means the number of elements in category
        if (f_category == UFIELD_CATEGORY_DATE_INTERVAL_SPAN && (f_field == 0 || f_field == 1)) {
            begin[f_field] = f_start;
            end[f_field] = f_limit;
        }
        if (f_category == UFIELD_CATEGORY_DATE) {
            if (pre_end_pos < f_start) {
                parts.emplace_back(CommonDateFormatPart(f_field, pre_end_pos, f_start, index, true));
                index++;
            }
            parts.emplace_back(CommonDateFormatPart(f_field, f_start, f_limit, index, false));
            pre_end_pos = f_limit;
            ++index;
        }
    }
    if (U_FAILURE(status) != 0) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "format date interval error", array);
    }
    int32_t length = formatted_value.length();
    if (length > pre_end_pos) {
        parts.emplace_back(CommonDateFormatPart(-1, pre_end_pos, length, index, true));
    }
    for (auto part : parts) {
        substring.Update(
            JSLocale::IcuToString(thread, formatted_value, part.f_begin_index, part.f_end_index).GetTaggedValue());
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSArray, thread);
        JSHandle<JSObject> element;
        if (part.is_pre_exist) {
            element = JSLocale::PutElement(thread, part.index, array, ConvertFieldIdToDateType(thread, -1),
                                           JSHandle<JSTaggedValue>::Cast(substring));
        } else {
            element = JSLocale::PutElement(thread, part.index, array, ConvertFieldIdToDateType(thread, part.f_field),
                                           JSHandle<JSTaggedValue>::Cast(substring));
        }
        JSHandle<JSTaggedValue> value = JSHandle<JSTaggedValue>::Cast(
            ToValueString(thread, TrackValue(part.f_begin_index, part.f_end_index, begin, end)));
        JSObject::SetProperty(thread, element, thread->GlobalConstants()->GetHandledSourceString(), value, true);
    }
    return array;
}

Value JSDateTimeFormat::TrackValue(int32_t beginning, int32_t ending, std::array<int32_t, 2> begin,
                                   std::array<int32_t, 2> end)
{
    Value value = Value::SHARED;
    if ((begin[0] <= beginning) && (beginning <= end[0]) && (begin[0] <= ending) && (ending <= end[0])) {
        value = Value::START_RANGE;
    } else if ((begin[1] <= beginning) && (beginning <= end[1]) && (begin[1] <= ending) && (ending <= end[1])) {
        value = Value::END_RANGE;
    }
    return value;
}

std::vector<IcuPatternDesc> BuildIcuPatternDescs()
{
    std::vector<IcuPatternDesc> items = {IcuPatternDesc("weekday", ICU_WEEKDAY_PE, ICU_NARROW_LONG_SHORT),
                                         IcuPatternDesc("era", ICU_ERA_PE, ICU_NARROW_LONG_SHORT),
                                         IcuPatternDesc("year", ICU_YEAR_PE, ICU2_DIGIT_NUMERIC),
                                         IcuPatternDesc("month", ICU_MONTH_PE, ICU_NARROW_LONG_SHORT2_DIGIT_NUMERIC),
                                         IcuPatternDesc("day", ICU_DAY_PE, ICU2_DIGIT_NUMERIC),
                                         IcuPatternDesc("dayPeriod", ICU_DAY_PERIOD_PE, ICU_NARROW_LONG_SHORT),
                                         IcuPatternDesc("hour", ICU_HOUR_PE, ICU2_DIGIT_NUMERIC),
                                         IcuPatternDesc("minute", ICU_MINUTE_PE, ICU2_DIGIT_NUMERIC),
                                         IcuPatternDesc("second", ICU_SECOND_PE, ICU2_DIGIT_NUMERIC),
                                         IcuPatternDesc("timeZoneName", ICU_YIME_ZONE_NAME_PE, ICU_LONG_SHORT)};
    return items;
}

std::vector<IcuPatternDesc> InitializePattern(const IcuPatternDesc &hour_data)
{
    std::vector<IcuPatternDesc> result;
    std::vector<IcuPatternDesc> items = BuildIcuPatternDescs();
    auto item = items.begin();
    while (static_cast<int32_t>(item != items.end()) != 0) {
        if (item->property != "hour") {
            result.emplace_back(IcuPatternDesc(item->property, item->pairs, item->allowed_values));
        } else {
            result.emplace_back(hour_data);
        }
        item++;
    }
    return result;
}

std::vector<IcuPatternDesc> JSDateTimeFormat::GetIcuPatternDesc(const HourCycleOption &hour_cycle)
{
    if (hour_cycle == HourCycleOption::H11) {
        Pattern h11("KK", "K");
        return h11.Get();
    }
    if (hour_cycle == HourCycleOption::H12) {
        Pattern h12("hh", "h");
        return h12.Get();
    }
    if (hour_cycle == HourCycleOption::H23) {
        Pattern h23("HH", "H");
        return h23.Get();
    }
    if (hour_cycle == HourCycleOption::H24) {
        Pattern h24("kk", "k");
        return h24.Get();
    }
    if (hour_cycle == HourCycleOption::UNDEFINED) {
        Pattern pattern("jj", "j");
        return pattern.Get();
    }
    UNREACHABLE();
}

icu::UnicodeString JSDateTimeFormat::ChangeHourCyclePattern(const icu::UnicodeString &pattern, HourCycleOption hc)
{
    if (hc == HourCycleOption::UNDEFINED || hc == HourCycleOption::EXCEPTION) {
        return pattern;
    }
    icu::UnicodeString result;
    char16_t key = u'\0';
    auto map_iter =
        std::find_if(HOUR_CYCLE_MAP.begin(), HOUR_CYCLE_MAP.end(),
                     [hc](const std::map<char16_t, HourCycleOption>::value_type item) { return item.second == hc; });
    if (map_iter != HOUR_CYCLE_MAP.end()) {
        key = map_iter->first;
    }
    bool need_change = true;
    char16_t last = u'\0';
    for (int32_t i = 0; i < pattern.length(); i++) {
        char16_t ch = pattern.charAt(i);
        if (ch == '\'') {
            need_change = !need_change;
            result.append(ch);
        } else if (HOUR_CYCLE_MAP.find(ch) != HOUR_CYCLE_MAP.end()) {
            result = (need_change && last == u'd') ? result.append(' ') : result;
            result.append(need_change ? key : ch);
        } else {
            result.append(ch);
        }
        last = ch;
    }
    return result;
}

std::unique_ptr<icu::SimpleDateFormat> JSDateTimeFormat::CreateICUSimpleDateFormat(const icu::Locale &icu_locale,
                                                                                   const icu::UnicodeString &skeleton,
                                                                                   icu::DateTimePatternGenerator *gn,
                                                                                   HourCycleOption hc)
{
    // See https://github.com/tc39/ecma402/issues/225
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString pattern =
        ChangeHourCyclePattern(gn->getBestPattern(skeleton, UDATPG_MATCH_HOUR_FIELD_LENGTH, status), hc);
    ASSERT_PRINT((U_SUCCESS(status) != 0), "get best pattern failed");

    status = U_ZERO_ERROR;
    auto date_format(std::make_unique<icu::SimpleDateFormat>(pattern, icu_locale, status));
    if (U_FAILURE(status) != 0) {
        return std::unique_ptr<icu::SimpleDateFormat>();
    }
    ASSERT_PRINT(date_format != nullptr, "dateFormat failed");
    return date_format;
}

std::unique_ptr<icu::Calendar> JSDateTimeFormat::BuildCalendar(const icu::Locale &locale,
                                                               const icu::TimeZone &time_zone)
{
    UErrorCode status = U_ZERO_ERROR;
    std::unique_ptr<icu::Calendar> calendar(icu::Calendar::createInstance(time_zone, locale, status));
    ASSERT_PRINT(U_SUCCESS(status), "buildCalendar failed");
    ASSERT_PRINT(calendar.get() != nullptr, "calender is nullptr");

    /**
     * Return the class ID for this class.
     *
     * This is useful only for comparing to a return value from getDynamicClassID(). For example:
     *
     *     Base* polymorphic_pointer = createPolymorphicObject();
     *     if (polymorphic_pointer->getDynamicClassID() ==
     *     Derived::getStaticClassID()) ...
     */
    if (calendar->getDynamicClassID() == icu::GregorianCalendar::getStaticClassID()) {
        auto gregorian_calendar = static_cast<icu::GregorianCalendar *>(calendar.get());
        // ECMAScript start time, value = -(2**53)
        const double begin_time = -9007199254740992;
        gregorian_calendar->setGregorianChange(begin_time, status);
        ASSERT(U_SUCCESS(status));
    }
    return calendar;
}

std::unique_ptr<icu::TimeZone> JSDateTimeFormat::ConstructTimeZone(const std::string &timezone)
{
    if (timezone.empty()) {
        return std::unique_ptr<icu::TimeZone>();
    }
    std::string canonicalized = ConstructFormattedTimeZoneID(timezone);

    std::unique_ptr<icu::TimeZone> tz(icu::TimeZone::createTimeZone(canonicalized.c_str()));
    if (!JSLocale::IsValidTimeZoneName(*tz)) {
        return std::unique_ptr<icu::TimeZone>();
    }
    return tz;
}

std::map<std::string, std::string> JSDateTimeFormat::GetSpecialTimeZoneMap()
{
    std::vector<std::string> specical_time_zones = {
        "America/Argentina/ComodRivadavia"
        "America/Knox_IN"
        "Antarctica/McMurdo"
        "Australia/ACT"
        "Australia/LHI"
        "Australia/NSW"
        "Antarctica/DumontDUrville"
        "Brazil/DeNoronha"
        "CET"
        "CST6CDT"
        "Chile/EasterIsland"
        "EET"
        "EST"
        "EST5EDT"
        "GB"
        "GB-Eire"
        "HST"
        "MET"
        "MST"
        "MST7MDT"
        "Mexico/BajaNorte"
        "Mexico/BajaSur"
        "NZ"
        "NZ-CHAT"
        "PRC"
        "PST8PDT"
        "ROC"
        "ROK"
        "UCT"
        "W-SU"
        "WET"};
    std::map<std::string, std::string> map;
    for (const auto &item : specical_time_zones) {
        std::string upper(item);
        transform(upper.begin(), upper.end(), upper.begin(), toupper);
        map.insert({upper, item});
    }
    return map;
}

std::string JSDateTimeFormat::ConstructFormattedTimeZoneID(const std::string &input)
{
    std::string result = input;
    transform(result.begin(), result.end(), result.begin(), toupper);
    static const std::vector<std::string> TZ_STYLE_ENTRY = {"GMT",     "ETC/UTC", "ETC/UCT", "GMT0",
                                                            "ETC/GMT", "GMT+0",   "GMT-0"};
    // NOLINTNEXTLINE(abseil-string-find-startswith)
    if (result.find("SYSTEMV/") == 0) {
        result.replace(0, STRING_LENGTH_8, "SystemV/");
    }  // NOLINTNEXTLINE(abseil-string-find-startswith)
    else if (result.find("US/") == 0) {
        result = (result.length() == STRING_LENGTH_3) ? result : "US/" + ToTitleCaseTimezonePosition(input.substr(3));
    }  // NOLINTNEXTLINE(abseil-string-find-startswith)
    else if (result.find("ETC/GMT") == 0 && result.length() > STRING_LENGTH_7) {
        result = ConstructGMTTimeZoneID(input);
    } else if (count(TZ_STYLE_ENTRY.begin(), TZ_STYLE_ENTRY.end(), result) != 0) {
        result = "UTC";
    }
    return result;
}

std::string JSDateTimeFormat::ToTitleCaseFunction(const std::string &input)
{
    std::string result(input);
    transform(result.begin(), result.end(), result.begin(), tolower);
    result[0] = toupper(result[0]);
    return result;
}

bool JSDateTimeFormat::IsValidTimeZoneInput(const std::string &input)
{
    std::regex r("[a-zA-Z_-/]*");
    bool is_valid = regex_match(input, r);
    return is_valid;
}

std::string JSDateTimeFormat::ToTitleCaseTimezonePosition(const std::string &input)
{
    if (!IsValidTimeZoneInput(input)) {
        return std::string();
    }
    std::vector<std::string> title_entry;
    std::vector<std::string> char_entry;
    int32_t left_position = 0;
    int32_t title_length = 0;
    for (size_t i = 0; i < input.length(); i++) {
        if (input[i] == '_' || input[i] == '-' || input[i] == '/') {
            std::string s(1, input[i]);
            char_entry.emplace_back(s);
            title_length = i - left_position;
            title_entry.emplace_back(input.substr(left_position, title_length));
            left_position = i + 1;
        } else {
            continue;
        }
    }
    std::string result;
    for (size_t i = 0; i < title_entry.size() - 1; i++) {
        std::string title_value = ToTitleCaseFunction(title_entry[i]);
        if (title_value == "Of" || title_value == "Es" || title_value == "Au") {
            title_value[0] = tolower(title_value[0]);
        }
        result += title_value + char_entry[i];
    }
    result = result + ToTitleCaseFunction(title_entry[title_entry.size() - 1]);
    return result;
}

std::string JSDateTimeFormat::ConstructGMTTimeZoneID(const std::string &input)
{
    if (input.length() < STRING_LENGTH_8 || input.length() > STRING_LENGTH_10) {
        return "";
    }
    std::string ret = "Etc/GMT";
    if (regex_match(input.substr(7), std::regex("[+-][1][0-4]")) ||
        (regex_match(input.substr(7), std::regex("[+-][0-9]")) || input.substr(7) == "0")) {
        return ret + input.substr(7);
    }
    return "";
}

std::string JSDateTimeFormat::ToHourCycleString(int32_t hc)
{
    auto map_iter = std::find_if(TO_HOUR_CYCLE_MAP.begin(), TO_HOUR_CYCLE_MAP.end(),
                                 [hc](const std::map<std::string, const HourCycleOption>::value_type &item) {
                                     return static_cast<int32_t>(item.second) == hc;
                                 });
    if (map_iter != TO_HOUR_CYCLE_MAP.end()) {
        return map_iter->first;
    }
    return "";
}

HourCycleOption JSDateTimeFormat::OptionToHourCycle(const std::string &hc)
{
    auto iter = TO_HOUR_CYCLE_MAP.find(hc);
    if (iter != TO_HOUR_CYCLE_MAP.end()) {
        return iter->second;
    }
    return HourCycleOption::UNDEFINED;
}

HourCycleOption JSDateTimeFormat::OptionToHourCycle(UDateFormatHourCycle hc)
{
    HourCycleOption hc_option = HourCycleOption::UNDEFINED;
    switch (hc) {
        case UDAT_HOUR_CYCLE_11:
            hc_option = HourCycleOption::H11;
            break;
        case UDAT_HOUR_CYCLE_12:
            hc_option = HourCycleOption::H12;
            break;
        case UDAT_HOUR_CYCLE_23:
            hc_option = HourCycleOption::H23;
            break;
        case UDAT_HOUR_CYCLE_24:
            hc_option = HourCycleOption::H24;
            break;
        default:
            UNREACHABLE();
    }
    return hc_option;
}

JSHandle<JSTaggedValue> JSDateTimeFormat::ConvertFieldIdToDateType(JSThread *thread, int32_t field_id)
{
    JSMutableHandle<JSTaggedValue> result(thread, JSTaggedValue::Undefined());
    auto global_const = thread->GlobalConstants();
    if (field_id == -1) {
        result.Update(global_const->GetHandledLiteralString().GetTaggedValue());
    } else if (field_id == UDAT_YEAR_FIELD || field_id == UDAT_EXTENDED_YEAR_FIELD) {
        result.Update(global_const->GetHandledYearString().GetTaggedValue());
    } else if (field_id == UDAT_YEAR_NAME_FIELD) {
        result.Update(global_const->GetHandledYearNameString().GetTaggedValue());
    } else if (field_id == UDAT_MONTH_FIELD || field_id == UDAT_STANDALONE_MONTH_FIELD) {
        result.Update(global_const->GetHandledMonthString().GetTaggedValue());
    } else if (field_id == UDAT_DATE_FIELD) {
        result.Update(global_const->GetHandledDayString().GetTaggedValue());
    } else if (field_id == UDAT_HOUR_OF_DAY1_FIELD || field_id == UDAT_HOUR_OF_DAY0_FIELD ||
               field_id == UDAT_HOUR1_FIELD || field_id == UDAT_HOUR0_FIELD) {
        result.Update(global_const->GetHandledHourString().GetTaggedValue());
    } else if (field_id == UDAT_MINUTE_FIELD) {
        result.Update(global_const->GetHandledMinuteString().GetTaggedValue());
    } else if (field_id == UDAT_SECOND_FIELD) {
        result.Update(global_const->GetHandledSecondString().GetTaggedValue());
    } else if (field_id == UDAT_DAY_OF_WEEK_FIELD || field_id == UDAT_DOW_LOCAL_FIELD ||
               field_id == UDAT_STANDALONE_DAY_FIELD) {
        result.Update(global_const->GetHandledWeekdayString().GetTaggedValue());
    } else if (field_id == UDAT_AM_PM_FIELD || field_id == UDAT_AM_PM_MIDNIGHT_NOON_FIELD ||
               field_id == UDAT_FLEXIBLE_DAY_PERIOD_FIELD) {
        result.Update(global_const->GetHandledDayPeriodString().GetTaggedValue());
    } else if (field_id == UDAT_TIMEZONE_FIELD || field_id == UDAT_TIMEZONE_RFC_FIELD ||
               field_id == UDAT_TIMEZONE_GENERIC_FIELD || field_id == UDAT_TIMEZONE_SPECIAL_FIELD ||
               field_id == UDAT_TIMEZONE_LOCALIZED_GMT_OFFSET_FIELD || field_id == UDAT_TIMEZONE_ISO_FIELD ||
               field_id == UDAT_TIMEZONE_ISO_LOCAL_FIELD) {
        result.Update(global_const->GetHandledTimeZoneNameString().GetTaggedValue());
    } else if (field_id == UDAT_ERA_FIELD) {
        result.Update(global_const->GetHandledEraString().GetTaggedValue());
    } else if (field_id == UDAT_FRACTIONAL_SECOND_FIELD) {
        result.Update(global_const->GetHandledFractionalSecondString().GetTaggedValue());
    } else if (field_id == UDAT_RELATED_YEAR_FIELD) {
        result.Update(global_const->GetHandledRelatedYearString().GetTaggedValue());
    } else if (field_id == UDAT_QUARTER_FIELD || field_id == UDAT_STANDALONE_QUARTER_FIELD) {
        UNREACHABLE();
    }
    return result;
}

std::unique_ptr<icu::DateIntervalFormat> JSDateTimeFormat::ConstructDateIntervalFormat(
    const JSHandle<JSDateTimeFormat> &dtf)
{
    icu::SimpleDateFormat *icu_simple_date_format = dtf->GetIcuSimpleDateFormat();
    icu::Locale locale = *(dtf->GetIcuLocale());
    std::string hc_string = ToHourCycleString(dtf->GetHourCycle().GetInt());
    UErrorCode status = U_ZERO_ERROR;
    // Sets the Unicode value for a Unicode keyword.
    if (!hc_string.empty()) {
        locale.setUnicodeKeywordValue("hc", hc_string, status);
    }
    icu::UnicodeString pattern;
    // Return a pattern string describing this date format.
    pattern = icu_simple_date_format->toPattern(pattern);
    // Utility to return a unique skeleton from a given pattern.
    icu::UnicodeString skeleton = icu::DateTimePatternGenerator::staticGetSkeleton(pattern, status);
    // Construct a DateIntervalFormat from skeleton and a given locale.
    std::unique_ptr<icu::DateIntervalFormat> date_interval_format(
        icu::DateIntervalFormat::createInstance(skeleton, locale, status));
    if (U_FAILURE(status) != 0) {
        return nullptr;
    }
    date_interval_format->setTimeZone(icu_simple_date_format->getTimeZone());
    return date_interval_format;
}
}  // namespace panda::ecmascript
