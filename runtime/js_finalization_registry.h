/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_JS_FINALIZATION_REGISTRY_H
#define ECMASCRIPT_JS_FINALIZATION_REGISTRY_H

#include <limits>
#include "js_object.h"
#include "js_tagged_value-inl.h"

namespace panda::ecmascript {
class JSFinalizationRegistry : public JSObject {
public:
    CAST_CHECK(JSFinalizationRegistry, IsJSFinalizationRegistry);

    static void Register(const JSThread *thread, const JSHandle<JSFinalizationRegistry> &registry,
                         const JSHandle<JSTaggedValue> &obj, const JSHandle<JSTaggedValue> &callback_arg,
                         const JSHandle<JSTaggedValue> &token);
    static bool Unregister(JSThread *thread, const JSHandle<JSFinalizationRegistry> &registry,
                           const JSHandle<JSTaggedValue> &token);
    static void CallCleanupCallback(JSThread *thread, JSHandle<JSFinalizationRegistry> registry);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, CleanupCallback)
    ACCESSORS(1, Cells)  // Array of triplets (WeakRef(object), callback_argument, WeakRef(unregistration_token))
    ACCESSORS_FINISH(2)

private:
    uint32_t GetLength() const;
    JSTaggedValue GetObject(uint32_t idx);
    void SetObject(const JSThread *thread, uint32_t idx, JSTaggedValue obj);
    JSTaggedValue GetCallbackArg(uint32_t idx);
    void SetCallbackArg(const JSThread *thread, uint32_t idx, JSTaggedValue arg);
    JSTaggedValue GetToken(uint32_t idx);
    void SetToken(const JSThread *thread, uint32_t idx, JSTaggedValue token);
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_FINALIZATION_REGISTRY_H
