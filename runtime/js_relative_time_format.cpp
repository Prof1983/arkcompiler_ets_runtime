/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_relative_time_format.h"

#include "unicode/decimfmt.h"
#include "unicode/numfmt.h"
#include "unicode/unum.h"

namespace panda::ecmascript {
enum class StyleOption : uint8_t { LONG = 0x01, SHORT, NARROW, EXCEPTION };
enum class NumericOption : uint8_t { ALWAYS = 0x01, AUTO, EXCEPTION };

// 14.1.1 InitializeRelativeTimeFormat ( relativeTimeFormat, locales, options )
JSHandle<JSRelativeTimeFormat> JSRelativeTimeFormat::InitializeRelativeTimeFormat(
    JSThread *thread, const JSHandle<JSRelativeTimeFormat> &relative_time_format,
    const JSHandle<JSTaggedValue> &locales, const JSHandle<JSTaggedValue> &options)
{
    EcmaVM *ecma_vm = thread->GetEcmaVM();
    ObjectFactory *factory = ecma_vm->GetFactory();

    // 1.Let requestedLocales be ? CanonicalizeLocaleList(locales).
    JSHandle<TaggedArray> requested_locales = JSLocale::CanonicalizeLocaleList(thread, locales);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSRelativeTimeFormat, thread);

    // 2&3. If options is undefined, then Let options be ObjectCreate(null). else Let options be ? ToObject(options).
    JSHandle<JSObject> rtf_options;
    if (!options->IsUndefined()) {
        rtf_options = JSTaggedValue::ToObject(thread, options);
        RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSRelativeTimeFormat, thread);
    } else {
        rtf_options = factory->OrdinaryNewJSObjectCreate(JSHandle<JSTaggedValue>(thread, JSTaggedValue::Null()));
    }

    // 5. Let matcher be ? GetOption(options, "localeMatcher", "string", «"lookup", "best fit"», "best fit").
    auto global_const = thread->GlobalConstants();
    LocaleMatcherOption matcher =
        JSLocale::GetOptionOfString(thread, rtf_options, global_const->GetHandledLocaleMatcherString(),
                                    {LocaleMatcherOption::LOOKUP, LocaleMatcherOption::BEST_FIT},
                                    {"lookup", "best fit"}, LocaleMatcherOption::BEST_FIT);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSRelativeTimeFormat, thread);

    // 7. Let numberingSystem be ? GetOption(options, "numberingSystem", "string", undefined, undefined).
    JSHandle<JSTaggedValue> property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledNumberingSystemString());
    JSHandle<JSTaggedValue> undefined_value(thread, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> numbering_system_value =
        JSLocale::GetOption(thread, rtf_options, property, OptionType::STRING, undefined_value, undefined_value);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSRelativeTimeFormat, thread);

    // Check whether numberingSystem is well formed and set to %RelativeTimeFormat%.[[numberingSystem]]
    std::string numbering_system_std_str;
    if (!numbering_system_value->IsUndefined()) {
        JSHandle<EcmaString> numbering_system_string = JSHandle<EcmaString>::Cast(numbering_system_value);
        if (numbering_system_string->IsUtf16()) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "invalid numberingSystem", relative_time_format);
        }
        numbering_system_std_str = JSLocale::ConvertToStdString(numbering_system_string);
        if (!JSLocale::IsNormativeNumberingSystem(numbering_system_std_str)) {
            THROW_RANGE_ERROR_AND_RETURN(thread, "invalid numberingSystem", relative_time_format);
        }
    } else {
        relative_time_format->SetNumberingSystem(thread, global_const->GetHandledLatnString());
    }

    // 10. Let localeData be %RelativeTimeFormat%.[[LocaleData]].
    // 11. Let r be ResolveLocale(%RelativeTimeFormat%.[[AvailableLocales]], requestedLocales, opt,
    // %RelativeTimeFormat%.[[RelevantExtensionKeys]], localeData).
    JSHandle<TaggedArray> available_locales;
    if (requested_locales->GetLength() == 0) {
        available_locales = factory->EmptyArray();
    } else {
        available_locales = JSLocale::GetAvailableLocales(thread, "calendar", nullptr);
    }
    std::set<std::string> relevant_extension_keys {"nu"};
    ResolvedLocale r =
        JSLocale::ResolveLocale(thread, available_locales, requested_locales, matcher, relevant_extension_keys);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSRelativeTimeFormat, thread);
    icu::Locale icu_locale = r.locale_data;

    // 12. Let locale be r.[[Locale]].
    JSHandle<EcmaString> locale_str = JSLocale::ToLanguageTag(thread, icu_locale);

    // 13. Set relativeTimeFormat.[[Locale]] to locale.
    relative_time_format->SetLocale(thread, locale_str.GetTaggedValue());

    // 15. Set relativeTimeFormat.[[NumberingSystem]] to r.[[nu]].
    UErrorCode status = U_ZERO_ERROR;
    if (!numbering_system_std_str.empty()) {
        if (JSLocale::IsWellNumberingSystem(numbering_system_std_str)) {
            icu_locale.setUnicodeKeywordValue("nu", numbering_system_std_str, status);
            ASSERT(U_SUCCESS(status));
        }
    }

    // 16. Let s be ? GetOption(options, "style", "string", «"long", "short", "narrow"», "long").
    property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledStyleString());
    StyleOption style_option = JSLocale::GetOptionOfString(thread, rtf_options, property,
                                                           {StyleOption::LONG, StyleOption::SHORT, StyleOption::NARROW},
                                                           {"long", "short", "narrow"}, StyleOption::LONG);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSRelativeTimeFormat, thread);

    // 17. Set relativeTimeFormat.[[Style]] to s.
    JSTaggedValue style_value(static_cast<uint8_t>(style_option));
    relative_time_format->SetStyle(thread, JSHandle<JSTaggedValue>(thread, style_value));

    // 18. Let numeric be ? GetOption(options, "numeric", "string", ?"always", "auto"?, "always").
    property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledNumericString());
    NumericOption numeric_option =
        JSLocale::GetOptionOfString(thread, rtf_options, property, {NumericOption::ALWAYS, NumericOption::AUTO},
                                    {"always", "auto"}, NumericOption::ALWAYS);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSRelativeTimeFormat, thread);

    // 19. Set relativeTimeFormat.[[Numeric]] to numeric.
    JSTaggedValue numeric_value(static_cast<uint8_t>(numeric_option));
    relative_time_format->SetNumeric(thread, JSHandle<JSTaggedValue>(thread, numeric_value));

    // 20. Let relativeTimeFormat.[[NumberFormat]] be ! Construct(%NumberFormat%, « locale »).
    icu::NumberFormat *icu_number_format = icu::NumberFormat::createInstance(icu_locale, UNUM_DECIMAL, status);
    if (U_FAILURE(status) != 0) {
        delete icu_number_format;
        THROW_RANGE_ERROR_AND_RETURN(thread, "icu Number Format Error", relative_time_format);
    }

    // Trans StyleOption to ICU Style
    UDateRelativeDateTimeFormatterStyle u_style;
    switch (style_option) {
        case StyleOption::LONG:
            u_style = UDAT_STYLE_LONG;
            break;
        case StyleOption::SHORT:
            u_style = UDAT_STYLE_SHORT;
            break;
        case StyleOption::NARROW:
            u_style = UDAT_STYLE_NARROW;
            break;
        default:
            UNREACHABLE();
    }
    icu::RelativeDateTimeFormatter rtf_formatter(icu_locale, icu_number_format, u_style, UDISPCTX_CAPITALIZATION_NONE,
                                                 status);
    if (U_FAILURE(status) != 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "icu Formatter Error", relative_time_format);
    }

    std::string numbering_system = JSLocale::GetNumberingSystem(icu_locale);
    auto result = factory->NewFromStdString(numbering_system);
    relative_time_format->SetNumberingSystem(thread, result);

    // Set RelativeTimeFormat.[[IcuRelativeTimeFormatter]]
    factory->NewJSIntlIcuData(relative_time_format, rtf_formatter, JSRelativeTimeFormat::FreeIcuRTFFormatter);

    // 22. Return relativeTimeFormat.
    return relative_time_format;
}

// 14.1.2  SingularRelativeTimeUnit ( unit )
bool SingularUnitToIcuUnit(JSThread *thread, const JSHandle<EcmaString> &unit, URelativeDateTimeUnit *unit_enum)
{
    // 1. Assert: Type(unit) is String.
    ASSERT(JSHandle<JSTaggedValue>::Cast(unit)->IsString());

    // 2. If unit is "seconds" or "second", return "second".
    // 3. If unit is "minutes" or "minute", return "minute".
    // 4. If unit is "hours" or "hour", return "hour".
    // 5. If unit is "days" or "day", return "day".
    // 6. If unit is "weeks" or "week", return "week".
    // 7. If unit is "months" or "month", return "month".
    // 8. If unit is "quarters" or "quarter", return "quarter".
    // 9. If unit is "years" or "year", return "year".
    auto global_const = thread->GlobalConstants();
    JSHandle<EcmaString> second = JSHandle<EcmaString>::Cast(global_const->GetHandledSecondString());
    JSHandle<EcmaString> minute = JSHandle<EcmaString>::Cast(global_const->GetHandledMinuteString());
    JSHandle<EcmaString> hour = JSHandle<EcmaString>::Cast(global_const->GetHandledHourString());
    JSHandle<EcmaString> day = JSHandle<EcmaString>::Cast(global_const->GetHandledDayString());
    JSHandle<EcmaString> week = JSHandle<EcmaString>::Cast(global_const->GetHandledWeekString());
    JSHandle<EcmaString> month = JSHandle<EcmaString>::Cast(global_const->GetHandledMonthString());
    JSHandle<EcmaString> quarter = JSHandle<EcmaString>::Cast(global_const->GetHandledQuarterString());
    JSHandle<EcmaString> year = JSHandle<EcmaString>::Cast(global_const->GetHandledYearString());

    JSHandle<EcmaString> seconds = JSHandle<EcmaString>::Cast(global_const->GetHandledSecondsString());
    JSHandle<EcmaString> minutes = JSHandle<EcmaString>::Cast(global_const->GetHandledMinutesString());
    JSHandle<EcmaString> hours = JSHandle<EcmaString>::Cast(global_const->GetHandledHoursString());
    JSHandle<EcmaString> days = JSHandle<EcmaString>::Cast(global_const->GetHandledDaysString());
    JSHandle<EcmaString> weeks = JSHandle<EcmaString>::Cast(global_const->GetHandledWeeksString());
    JSHandle<EcmaString> months = JSHandle<EcmaString>::Cast(global_const->GetHandledMonthsString());
    JSHandle<EcmaString> quarters = JSHandle<EcmaString>::Cast(global_const->GetHandledQuartersString());
    JSHandle<EcmaString> years = JSHandle<EcmaString>::Cast(global_const->GetHandledYearsString());

    if (EcmaString::StringsAreEqual(*second, *unit) || EcmaString::StringsAreEqual(*seconds, *unit)) {
        *unit_enum = UDAT_REL_UNIT_SECOND;
    } else if (EcmaString::StringsAreEqual(*minute, *unit) || EcmaString::StringsAreEqual(*minutes, *unit)) {
        *unit_enum = UDAT_REL_UNIT_MINUTE;
    } else if (EcmaString::StringsAreEqual(*hour, *unit) || EcmaString::StringsAreEqual(*hours, *unit)) {
        *unit_enum = UDAT_REL_UNIT_HOUR;
    } else if (EcmaString::StringsAreEqual(*day, *unit) || EcmaString::StringsAreEqual(*days, *unit)) {
        *unit_enum = UDAT_REL_UNIT_DAY;
    } else if (EcmaString::StringsAreEqual(*week, *unit) || EcmaString::StringsAreEqual(*weeks, *unit)) {
        *unit_enum = UDAT_REL_UNIT_WEEK;
    } else if (EcmaString::StringsAreEqual(*month, *unit) || EcmaString::StringsAreEqual(*months, *unit)) {
        *unit_enum = UDAT_REL_UNIT_MONTH;
    } else if (EcmaString::StringsAreEqual(*quarter, *unit) || EcmaString::StringsAreEqual(*quarters, *unit)) {
        *unit_enum = UDAT_REL_UNIT_QUARTER;
    } else if (EcmaString::StringsAreEqual(*year, *unit) || EcmaString::StringsAreEqual(*years, *unit)) {
        *unit_enum = UDAT_REL_UNIT_YEAR;
    } else {
        return false;
    }
    // 11. else return unit.
    return true;
}

// Unwrap RelativeTimeFormat
JSHandle<JSTaggedValue> JSRelativeTimeFormat::UnwrapRelativeTimeFormat(JSThread *thread,
                                                                       const JSHandle<JSTaggedValue> &rtf)
{
    ASSERT_PRINT(rtf->IsJSObject(), "rtf is not a JSObject");

    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    bool is_instance_of = JSFunction::InstanceOf(thread, rtf, env->GetRelativeTimeFormatFunction());
    RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, rtf);
    if (!rtf->IsJSRelativeTimeFormat() && is_instance_of) {
        JSHandle<JSTaggedValue> key(thread, JSHandle<JSIntl>::Cast(env->GetIntlFunction())->GetFallbackSymbol());
        OperationResult operation_result = JSTaggedValue::GetProperty(thread, rtf, key);
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, rtf);
        return operation_result.GetValue();
    }

    // Perform ? RequireInternalSlot(relativeTimeFormat, [[InitializedRelativeTimeFormat]]).
    if (!rtf->IsJSRelativeTimeFormat()) {
        RETURN_VALUE_IF_ABRUPT_COMPLETION(thread, rtf);
    }
    return rtf;
}

// CommonFormat
icu::FormattedRelativeDateTime GetIcuFormatted(JSThread *thread,
                                               const JSHandle<JSRelativeTimeFormat> &relative_time_format, double value,
                                               const JSHandle<EcmaString> &unit)
{
    icu::RelativeDateTimeFormatter *formatter = relative_time_format->GetIcuRTFFormatter();
    ASSERT_PRINT(formatter != nullptr, "rtfFormatter is null");

    // If isFinite(value) is false, then throw a RangeError exception.
    if (!std::isfinite(value)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "", icu::FormattedRelativeDateTime());
    }

    // 10. If unit is not one of "second", "minute", "hour", "day", "week", "month", "quarter", or "year", throw a
    // RangeError exception.
    URelativeDateTimeUnit unit_enum;
    if (!SingularUnitToIcuUnit(thread, unit, &unit_enum)) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "", icu::FormattedRelativeDateTime());
    }
    UErrorCode status = U_ZERO_ERROR;
    NumericOption numeric = static_cast<NumericOption>(relative_time_format->GetNumeric().GetNumber());

    icu::FormattedRelativeDateTime formatted;
    switch (numeric) {
        case NumericOption::ALWAYS:
            formatted = formatter->formatNumericToValue(value, unit_enum, status);
            ASSERT_PRINT(U_SUCCESS(status), "icu format to value error");
            break;
        case NumericOption::AUTO:
            formatted = formatter->formatToValue(value, unit_enum, status);
            ASSERT_PRINT(U_SUCCESS(status), "icu format to value error");
            break;
        default:
            UNREACHABLE();
    }
    return formatted;
}

// 14.1.2 SingularRelativeTimeUnit ( unit )
JSHandle<EcmaString> SingularUnitString(JSThread *thread, const JSHandle<EcmaString> &unit)
{
    auto global_const = thread->GlobalConstants();
    JSHandle<EcmaString> second = JSHandle<EcmaString>::Cast(global_const->GetHandledSecondString());
    JSHandle<EcmaString> minute = JSHandle<EcmaString>::Cast(global_const->GetHandledMinuteString());
    JSHandle<EcmaString> hour = JSHandle<EcmaString>::Cast(global_const->GetHandledHourString());
    JSHandle<EcmaString> day = JSHandle<EcmaString>::Cast(global_const->GetHandledDayString());
    JSHandle<EcmaString> week = JSHandle<EcmaString>::Cast(global_const->GetHandledWeekString());
    JSHandle<EcmaString> month = JSHandle<EcmaString>::Cast(global_const->GetHandledMonthString());
    JSHandle<EcmaString> quarter = JSHandle<EcmaString>::Cast(global_const->GetHandledQuarterString());
    JSHandle<EcmaString> year = JSHandle<EcmaString>::Cast(global_const->GetHandledYearString());
    JSHandle<EcmaString> seconds = JSHandle<EcmaString>::Cast(global_const->GetHandledSecondsString());
    JSHandle<EcmaString> minutes = JSHandle<EcmaString>::Cast(global_const->GetHandledMinutesString());
    JSHandle<EcmaString> hours = JSHandle<EcmaString>::Cast(global_const->GetHandledHoursString());
    JSHandle<EcmaString> days = JSHandle<EcmaString>::Cast(global_const->GetHandledDaysString());
    JSHandle<EcmaString> weeks = JSHandle<EcmaString>::Cast(global_const->GetHandledWeeksString());
    JSHandle<EcmaString> months = JSHandle<EcmaString>::Cast(global_const->GetHandledMonthsString());
    JSHandle<EcmaString> quarters = JSHandle<EcmaString>::Cast(global_const->GetHandledQuartersString());
    JSHandle<EcmaString> years = JSHandle<EcmaString>::Cast(global_const->GetHandledYearsString());

    // 2. If unit is "seconds" or "second", return "second".
    if (EcmaString::StringsAreEqual(*second, *unit) || EcmaString::StringsAreEqual(*seconds, *unit)) {
        return second;
    }
    // 3. If unit is "minutes" or "minute", return "minute".
    if (EcmaString::StringsAreEqual(*minute, *unit) || EcmaString::StringsAreEqual(*minutes, *unit)) {
        return minute;
    }
    // 4. If unit is "hours" or "hour", return "hour".
    if (EcmaString::StringsAreEqual(*hour, *unit) || EcmaString::StringsAreEqual(*hours, *unit)) {
        return hour;
    }
    // 5. If unit is "days" or "day", return "day".
    if (EcmaString::StringsAreEqual(*day, *unit) || EcmaString::StringsAreEqual(*days, *unit)) {
        return day;
    }
    // 6. If unit is "weeks" or "week", return "week".
    if (EcmaString::StringsAreEqual(*week, *unit) || EcmaString::StringsAreEqual(*weeks, *unit)) {
        return week;
    }
    // 7. If unit is "months" or "month", return "month".
    if (EcmaString::StringsAreEqual(*month, *unit) || EcmaString::StringsAreEqual(*months, *unit)) {
        return month;
    }
    // 8. If unit is "quarters" or "quarter", return "quarter".
    if (EcmaString::StringsAreEqual(*quarter, *unit) || EcmaString::StringsAreEqual(*quarters, *unit)) {
        return quarter;
    }
    // 9. If unit is "years" or "year", return "year".
    if (EcmaString::StringsAreEqual(*year, *unit) || EcmaString::StringsAreEqual(*years, *unit)) {
        return year;
    }

    JSHandle<JSTaggedValue> undefined_value(thread, JSTaggedValue::Undefined());
    return JSHandle<EcmaString>::Cast(undefined_value);
}

// 14.1.5 FormatRelativeTime ( relativeTimeFormat, value, unit )
JSHandle<EcmaString> JSRelativeTimeFormat::Format(JSThread *thread, double value, const JSHandle<EcmaString> &unit,
                                                  const JSHandle<JSRelativeTimeFormat> &relative_time_format)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    icu::FormattedRelativeDateTime formatted = GetIcuFormatted(thread, relative_time_format, value, unit);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(EcmaString, thread);
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString u_string = formatted.toString(status);
    if (U_FAILURE(status) != 0) {
        THROW_RANGE_ERROR_AND_RETURN(thread, "icu formatted toString error", factory->GetEmptyString());
    }
    JSHandle<EcmaString> string =
        factory->NewFromUtf16(reinterpret_cast<const uint16_t *>(u_string.getBuffer()), u_string.length());
    return string;
}

void FormatToArray(JSThread *thread, const JSHandle<JSArray> &array, const icu::FormattedRelativeDateTime &formatted,
                   double value, const JSHandle<EcmaString> &unit)
{
    UErrorCode status = U_ZERO_ERROR;
    icu::UnicodeString formatted_text = formatted.toString(status);
    if (U_FAILURE(status) != 0) {
        THROW_TYPE_ERROR(thread, "formattedRelativeDateTime toString failed");
    }

    icu::ConstrainedFieldPosition cfpo;
    // Set constrainCategory to UFIELD_CATEGORY_NUMBER which is specified for UNumberFormatFields
    cfpo.constrainCategory(UFIELD_CATEGORY_NUMBER);
    int32_t index = 0;
    int32_t previous_limit = 0;
    auto global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> tagged_value(thread, JSTaggedValue(value));
    JSMutableHandle<JSTaggedValue> type_string(thread, JSTaggedValue::Undefined());
    JSHandle<JSTaggedValue> unit_string = global_const->GetHandledUnitString();
    std::vector<std::pair<int32_t, int32_t>> separator_fields;
    /**
     * From ICU header file document @unumberformatter.h
     * Sets a constraint on the field category.
     *
     * When this instance of ConstrainedFieldPosition is passed to FormattedValue#nextPosition,
     * positions are skipped unless they have the given category.
     *
     * Any previously set constraints are cleared.
     *
     * For example, to loop over only the number-related fields:
     *
     *     ConstrainedFieldPosition cfpo;
     *     cfpo.constrainCategory(UFIELDCATEGORY_NUMBER_FORMAT);
     *     while (fmtval.nextPosition(cfpo, status)) {
     *         // handle the number-related field position
     *     }
     */
    while ((formatted.nextPosition(cfpo, status) != 0)) {
        int32_t field_id = cfpo.getField();
        int32_t start = cfpo.getStart();
        int32_t limit = cfpo.getLimit();
        // Special case when fieldId is UNUM_GROUPING_SEPARATOR_FIELD
        if (static_cast<UNumberFormatFields>(field_id) == UNUM_GROUPING_SEPARATOR_FIELD) {
            separator_fields.emplace_back(std::pair<int32_t, int32_t>(start, limit));
            continue;
        }
        // If start greater than previousLimit, means a literal type exists before number fields
        // so add a literal type with value of formattedText.sub(0, start)
        if (start > previous_limit) {
            type_string.Update(global_const->GetLiteralString());
            JSHandle<EcmaString> substring = JSLocale::IcuToString(thread, formatted_text, previous_limit, start);
            JSLocale::PutElement(thread, index++, array, type_string, JSHandle<JSTaggedValue>::Cast(substring));
            RETURN_IF_ABRUPT_COMPLETION(thread);
        }
        // Add part when type is unit
        // Iterate former grouping separator vector and add unit element to array
        for (auto &separator_field : separator_fields) {
            if (separator_field.first > start) {
                // Add Integer type element
                JSHandle<EcmaString> res_string =
                    JSLocale::IcuToString(thread, formatted_text, start, separator_field.first);
                type_string.Update(
                    JSLocale::GetNumberFieldType(thread, tagged_value.GetTaggedValue(), field_id).GetTaggedValue());
                JSHandle<JSObject> record = JSLocale::PutElement(thread, index++, array, type_string,
                                                                 JSHandle<JSTaggedValue>::Cast(res_string));
                RETURN_IF_ABRUPT_COMPLETION(thread);
                JSObject::CreateDataPropertyOrThrow(thread, record, unit_string, JSHandle<JSTaggedValue>::Cast(unit));
                RETURN_IF_ABRUPT_COMPLETION(thread);
                // Add Group type element
                res_string =
                    JSLocale::IcuToString(thread, formatted_text, separator_field.first, separator_field.second);
                type_string.Update(
                    JSLocale::GetNumberFieldType(thread, tagged_value.GetTaggedValue(), UNUM_GROUPING_SEPARATOR_FIELD)
                        .GetTaggedValue());
                record = JSLocale::PutElement(thread, index++, array, type_string,
                                              JSHandle<JSTaggedValue>::Cast(res_string));
                RETURN_IF_ABRUPT_COMPLETION(thread);
                JSObject::CreateDataPropertyOrThrow(thread, record, unit_string, JSHandle<JSTaggedValue>::Cast(unit));
                RETURN_IF_ABRUPT_COMPLETION(thread);
                start = separator_field.second;
            }
        }
        // Add current field unit
        JSHandle<EcmaString> sub_string = JSLocale::IcuToString(thread, formatted_text, start, limit);
        type_string.Update(
            JSLocale::GetNumberFieldType(thread, tagged_value.GetTaggedValue(), field_id).GetTaggedValue());
        JSHandle<JSObject> record =
            JSLocale::PutElement(thread, index++, array, type_string, JSHandle<JSTaggedValue>::Cast(sub_string));
        RETURN_IF_ABRUPT_COMPLETION(thread);
        JSObject::CreateDataPropertyOrThrow(thread, record, unit_string, JSHandle<JSTaggedValue>::Cast(unit));
        RETURN_IF_ABRUPT_COMPLETION(thread);
        previous_limit = limit;
    }
    // If iterated length is smaller than formattedText.length, means a literal type exists after number fields
    // so add a literal type with value of formattedText.sub(previousLimit, formattedText.length)
    if (formatted_text.length() > previous_limit) {
        type_string.Update(global_const->GetLiteralString());
        JSHandle<EcmaString> substring =
            JSLocale::IcuToString(thread, formatted_text, previous_limit, formatted_text.length());
        JSLocale::PutElement(thread, index, array, type_string, JSHandle<JSTaggedValue>::Cast(substring));
        RETURN_IF_ABRUPT_COMPLETION(thread);
    }
}

// 14.1.6 FormatRelativeTimeToParts ( relativeTimeFormat, value, unit )
JSHandle<JSArray> JSRelativeTimeFormat::FormatToParts(JSThread *thread, double value, const JSHandle<EcmaString> &unit,
                                                      const JSHandle<JSRelativeTimeFormat> &relative_time_format)
{
    icu::FormattedRelativeDateTime formatted = GetIcuFormatted(thread, relative_time_format, value, unit);
    RETURN_HANDLE_IF_ABRUPT_COMPLETION(JSArray, thread);
    JSHandle<EcmaString> singular_unit = SingularUnitString(thread, unit);
    JSHandle<JSArray> array = JSHandle<JSArray>::Cast(JSArray::ArrayCreate(thread, JSTaggedNumber(0)));
    FormatToArray(thread, array, formatted, value, singular_unit);
    return array;
}

void JSRelativeTimeFormat::ResolvedOptions(JSThread *thread, const JSHandle<JSRelativeTimeFormat> &relative_time_format,
                                           const JSHandle<JSObject> &options)
{
    if (relative_time_format->GetIcuRTFFormatter() != nullptr) {
        [[maybe_unused]] icu::RelativeDateTimeFormatter *formatter = relative_time_format->GetIcuRTFFormatter();
    } else {
        THROW_RANGE_ERROR(thread, "rtf is not initialized");
    }

    auto global_const = thread->GlobalConstants();
    // [[locale]]
    JSHandle<JSTaggedValue> property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledLocaleString());
    JSHandle<EcmaString> locale(thread, relative_time_format->GetLocale());
    PropertyDescriptor locale_desc(thread, JSHandle<JSTaggedValue>::Cast(locale), true, true, true);
    JSObject::DefineOwnProperty(thread, options, property, locale_desc);

    // [[Style]]
    property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledStyleString());
    StyleOption style = static_cast<StyleOption>(relative_time_format->GetStyle().GetNumber());
    JSHandle<JSTaggedValue> style_value;
    if (style == StyleOption::LONG) {
        style_value = global_const->GetHandledLongString();
    } else if (style == StyleOption::SHORT) {
        style_value = global_const->GetHandledShortString();
    } else if (style == StyleOption::NARROW) {
        style_value = global_const->GetHandledNarrowString();
    }
    PropertyDescriptor style_desc(thread, style_value, true, true, true);
    JSObject::DefineOwnProperty(thread, options, property, style_desc);

    // [[Numeric]]
    property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledNumericString());
    NumericOption numeric = static_cast<NumericOption>(relative_time_format->GetNumeric().GetNumber());
    JSHandle<JSTaggedValue> numeric_value;
    if (numeric == NumericOption::ALWAYS) {
        numeric_value = global_const->GetHandledAlwaysString();
    } else if (numeric == NumericOption::AUTO) {
        numeric_value = global_const->GetHandledAutoString();
    } else {
        THROW_RANGE_ERROR(thread, "numeric is exception");
    }
    PropertyDescriptor numeric_desc(thread, numeric_value, true, true, true);
    JSObject::DefineOwnProperty(thread, options, property, numeric_desc);

    // [[NumberingSystem]]
    property = JSHandle<JSTaggedValue>::Cast(global_const->GetHandledNumberingSystemString());
    JSHandle<JSTaggedValue> numbering_system(thread, relative_time_format->GetNumberingSystem());
    PropertyDescriptor numbering_system_desc(thread, numbering_system, true, true, true);
    JSObject::DefineOwnProperty(thread, options, property, numbering_system_desc);
}
}  // namespace panda::ecmascript