/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_IC_IC_HANDLER_H
#define ECMASCRIPT_IC_IC_HANDLER_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/mem/tagged_object.h"

namespace panda::ecmascript {
class HandlerBase {
public:
    static constexpr uint32_t KIND_BIT_LENGTH = 3;
    enum HandlerKind {
        NONE = 0,
        FIELD,
        ELEMENT,
        DICTIONARY,
        NON_EXIST,
    };

    using KindBit = BitField<HandlerKind, 0, KIND_BIT_LENGTH>;
    using InlinedPropsBit = KindBit::NextFlag;
    using AccessorBit = InlinedPropsBit::NextFlag;
    using InternalAccessorBit = AccessorBit::NextFlag;
    using IsJSArrayBit = InternalAccessorBit::NextFlag;
    using IsKeyIntBit = IsJSArrayBit::NextFlag;
    using ObservedOverflowArrayBit = IsKeyIntBit::NextFlag;
    using OffsetBit = ObservedOverflowArrayBit::NextField<uint32_t, PropertyAttributes::OFFSET_BITFIELD_NUM>;

    HandlerBase() = default;
    virtual ~HandlerBase() = default;

    DEFAULT_MOVE_SEMANTIC(HandlerBase);
    DEFAULT_COPY_SEMANTIC(HandlerBase);

    static inline bool IsAccessor(uint32_t handler)
    {
        return AccessorBit::Get(handler);
    }

    static inline bool IsInternalAccessor(uint32_t handler)
    {
        return InternalAccessorBit::Get(handler);
    }

    static inline bool IsNonExist(uint32_t handler)
    {
        return GetKind(handler) == HandlerKind::NON_EXIST;
    }

    static inline bool IsField(uint32_t handler)
    {
        return GetKind(handler) == HandlerKind::FIELD;
    }

    static inline bool IsElement(uint32_t handler)
    {
        return GetKind(handler) == HandlerKind::ELEMENT;
    }

    static inline bool IsDictionary(uint32_t handler)
    {
        return GetKind(handler) == HandlerKind::DICTIONARY;
    }

    static inline bool IsInlinedProps(uint32_t handler)
    {
        return InlinedPropsBit::Get(handler);
    }

    static inline HandlerKind GetKind(uint32_t handler)
    {
        return KindBit::Get(handler);
    }

    static inline bool IsJSArray(uint32_t handler)
    {
        return IsJSArrayBit::Get(handler);
    }

    static inline bool IsArrayOverflowed(uint32_t handler)
    {
        return ObservedOverflowArrayBit::Get(handler);
    }

    static inline void SetArrayOverflowed(uint32_t &handler)
    {
        ObservedOverflowArrayBit::Set<uint32_t>(true, &handler);
    }

    static inline bool IsKeyInt(uint32_t handler)
    {
        return IsKeyIntBit::Get(handler);
    }

    static inline void SetKeyNotInt(uint32_t &handler)
    {
        IsKeyIntBit::Set<uint32_t>(false, &handler);
    }

    static inline int GetOffset(uint32_t handler)
    {
        return OffsetBit::Get(handler);
    }
};

class LoadHandler final : public HandlerBase {
public:
    static inline JSHandle<JSTaggedValue> LoadProperty(const JSThread *thread, const ObjectOperator &op);
    static inline JSHandle<JSTaggedValue> LoadElement(const JSThread *thread, bool is_int);
};

class StoreHandler final : public HandlerBase {
public:
    static inline JSHandle<JSTaggedValue> StoreProperty(const JSThread *thread, const ObjectOperator &op);
    static inline JSHandle<JSTaggedValue> StoreElement(const JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                       bool is_int);
};

class TransitionHandler : public TaggedObject {
public:
    static TransitionHandler *Cast(TaggedObject *object)
    {
        ASSERT(JSTaggedValue(object).IsTransitionHandler());
        return static_cast<TransitionHandler *>(object);
    }

    static inline JSHandle<JSTaggedValue> StoreTransition(const JSThread *thread, const ObjectOperator &op);
    inline void SetArrayOverflowed(const JSThread *thread)
    {
        uint32_t info = GetHandlerInfo().GetInt();
        HandlerBase::SetArrayOverflowed(info);
        SetHandlerInfo(thread, JSHandle<JSTaggedValue>(thread, JSTaggedValue(info)));
    }

    ACCESSORS_START(TaggedObjectSize())
    ACCESSORS(0, HandlerInfo)
    ACCESSORS(1, TransitionHClass)
    ACCESSORS_FINISH(2)

    DECL_DUMP()
};

class PrototypeHandler : public TaggedObject {
public:
    static PrototypeHandler *Cast(TaggedObject *object)
    {
        ASSERT(JSTaggedValue(object).IsPrototypeHandler());
        return static_cast<PrototypeHandler *>(object);
    }

    static inline JSHandle<JSTaggedValue> LoadPrototype(const JSThread *thread, const ObjectOperator &op,
                                                        const JSHandle<JSHClass> &hclass);
    static inline JSHandle<JSTaggedValue> StorePrototype(const JSThread *thread, const ObjectOperator &op,
                                                         const JSHandle<JSHClass> &hclass);

    ACCESSORS_START(TaggedObjectSize())
    ACCESSORS(0, HandlerInfo)
    ACCESSORS(1, ProtoCell)
    ACCESSORS(2, Holder)
    ACCESSORS_FINISH(3)

    DECL_DUMP()
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_IC_IC_HANDLER_H
