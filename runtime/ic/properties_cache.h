/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_IC_PROPERTIES_CACHE_H
#define ECMASCRIPT_IC_PROPERTIES_CACHE_H

#include <array>

#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/ecma_macros.h"

namespace panda::ecmascript {
class EcmaVM;
class PropertiesCache {
public:
    inline int Get(JSHClass *js_hclass, JSTaggedValue key);
    inline void Set(JSHClass *js_hclass, JSTaggedValue key, int index);
    inline void Clear();

    static const int NOT_FOUND = -1;
    static const uint32_t CACHE_LENGTH_BIT = 10;
    static const uint32_t CACHE_LENGTH = (1U << CACHE_LENGTH_BIT);
    static const uint32_t CACHE_LENGTH_MASK = CACHE_LENGTH - 1;

    struct PropertyKey {
        JSHClass *hclass {nullptr};
        JSTaggedValue key {JSTaggedValue::Hole()};
        int results {NOT_FOUND};

        static constexpr uint32_t GetHClassOffset()
        {
            return MEMBER_OFFSET(PropertyKey, hclass);
        }

        static constexpr uint32_t GetKeyOffset()
        {
            return MEMBER_OFFSET(PropertyKey, key);
        }

        static constexpr uint32_t GetResultsOffset()
        {
            return MEMBER_OFFSET(PropertyKey, results);
        }
    };

    DEFAULT_MOVE_SEMANTIC(PropertiesCache);
    DEFAULT_COPY_SEMANTIC(PropertiesCache);

private:
    PropertiesCache()
    {
        for (uint32_t i = 0; i < CACHE_LENGTH; ++i) {
            keys_[i].hclass = nullptr;
            keys_[i].key = JSTaggedValue::Hole();
            keys_[i].results = NOT_FOUND;
        }
    }
    ~PropertiesCache() = default;

    static inline int Hash(JSHClass *cls, JSTaggedValue key);

    std::array<PropertyKey, CACHE_LENGTH> keys_ {};

    friend class JSThread;
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_IC_PROPERTIES_CACHE_H
