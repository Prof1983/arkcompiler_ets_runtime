/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/ic/ic_handler-inl.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/tagged_array-inl.h"

namespace panda::ecmascript {
void ProfileTypeAccessor::AddElementHandler(JSHandle<JSTaggedValue> dynclass, JSHandle<JSTaggedValue> handler) const
{
    auto profile_data = profile_type_info_->Get(slot_id_);
    if (profile_data.IsHole()) {
        return;
    }
    auto index = slot_id_;
    if (profile_data.IsUndefined()) {
        profile_type_info_->Set(thread_, index, dynclass.GetTaggedValue());
        profile_type_info_->Set(thread_, index + 1, handler.GetTaggedValue());
        return;
    }
    // clear key ic
    if (profile_data.IsString() || profile_data.IsSymbol()) {
        profile_type_info_->Set(thread_, index, dynclass.GetTaggedValue());
        profile_type_info_->Set(thread_, index + 1, handler.GetTaggedValue());
        return;
    }
    AddHandlerWithoutKey(dynclass, handler);
}

void ProfileTypeAccessor::AddHandlerWithoutKey(JSHandle<JSTaggedValue> dynclass, JSHandle<JSTaggedValue> handler) const
{
    if (IsNamedGlobalIC(GetKind())) {
        profile_type_info_->Set(thread_, slot_id_, handler.GetTaggedValue());
        return;
    }
    auto index = slot_id_;
    auto profile_data = profile_type_info_->Get(index);
    if (profile_data.IsHole()) {
        return;
    }
    while (index < slot_id_ + 4U && !profile_type_info_->Get(index).IsUndefined()) {
        index += 2U;
    }
    if (index == slot_id_ + 4U) {
        profile_type_info_->Set(thread_, slot_id_, JSTaggedValue::Hole());
        profile_type_info_->Set(thread_, slot_id_ + 1, JSTaggedValue::Hole());
        profile_type_info_->Set(thread_, slot_id_ + 2, JSTaggedValue::Hole());
        profile_type_info_->Set(thread_, slot_id_ + 3, JSTaggedValue::Hole());
        return;
    }
    profile_type_info_->Set(thread_, index, dynclass.GetTaggedValue());
    profile_type_info_->Set(thread_, index + 1, handler.GetTaggedValue());
}

void ProfileTypeAccessor::AddHandlerWithKey(JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> dynclass,
                                            JSHandle<JSTaggedValue> handler) const
{
    if (IsValueGlobalIC(GetKind())) {
        AddGlobalHandlerKey(key, handler);
        return;
    }
    auto profile_data = profile_type_info_->Get(slot_id_);
    if (profile_data.IsUndefined() || profile_type_info_->Get(slot_id_ + 1).IsUndefined()) {
        profile_type_info_->Set(thread_, slot_id_, key.GetTaggedValue());
        profile_type_info_->Set(thread_, slot_id_ + 1U, dynclass.GetTaggedValue());
        profile_type_info_->Set(thread_, slot_id_ + 2U, handler.GetTaggedValue());
        return;
    }
    // for element ic, profileData may dynclass or taggedarray
    if (key.GetTaggedValue() != profile_data) {
        profile_type_info_->Set(thread_, slot_id_, JSTaggedValue::Hole());
        profile_type_info_->Set(thread_, slot_id_ + 1, JSTaggedValue::Hole());
        profile_type_info_->Set(thread_, slot_id_ + 2, JSTaggedValue::Hole());
        return;
    }
}

void ProfileTypeAccessor::AddGlobalHandlerKey(JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> handler) const
{
    uint32_t index = slot_id_;
    JSTaggedValue index_val = profile_type_info_->Get(index);
    if (index_val.IsUndefined()) {
        profile_type_info_->Set(thread_, index, key.GetTaggedValue());
        profile_type_info_->Set(thread_, index + 1, handler.GetTaggedValue());
        return;
    }
    index += 2U;
    if (index_val.IsUndefined()) {
        profile_type_info_->Set(thread_, index, key.GetTaggedValue());
        profile_type_info_->Set(thread_, index + 1, handler.GetTaggedValue());
        return;
    }
    profile_type_info_->Set(thread_, slot_id_, JSTaggedValue::Hole());
}

void ProfileTypeAccessor::SetAsMega() const
{
    profile_type_info_->Set(thread_, slot_id_, JSTaggedValue::Hole());
    if (!IsNamedGlobalIC(GetKind())) {
        profile_type_info_->Set(thread_, slot_id_ + 1, JSTaggedValue::Hole());
    }
}

std::string ICKindToString(ICKind kind)
{
    switch (kind) {
        case ICKind::NAMED_LOAD_IC:
            return "NamedLoadIC";
        case ICKind::NAMED_STORE_IC:
            return "NamedStoreIC";
        case ICKind::LOAD_IC:
            return "LoadIC";
        case ICKind::STORE_IC:
            return "StoreIC";
        case ICKind::NAMED_GLOBAL_LOAD_IC:
            return "NamedGlobalLoadIC";
        case ICKind::NAMED_GLOBAL_STORE_IC:
            return "NamedGlobalStoreIC";
        case ICKind::TRY_NAMED_GLOBAL_LOAD_IC:
            return "TryNamedGlobalLoadIC";
        case ICKind::TRY_NAMED_GLOBAL_STORE_IC:
            return "TryNamedGlobalStoreIC";
        case ICKind::GLOBAL_LOAD_IC:
            return "GlobalLoadIC";
        case ICKind::GLOBAL_STORE_IC:
            return "GlobalStoreIC";
        default:
            UNREACHABLE();
    }
    UNREACHABLE();
}

std::string ProfileTypeAccessor::ICStateToString(ProfileTypeAccessor::ICState state)
{
    switch (state) {
        case ICState::UNINIT:
            return "uninit";
        case ICState::MONO:
            return "mono";
        case ICState::POLY:
            return "poly";
        case ICState::MEGA:
            return "mega";
        default:
            UNREACHABLE();
    }
    UNREACHABLE();
}

ProfileTypeAccessor::ICState ProfileTypeAccessor::GetICState() const
{
    auto profile_data = profile_type_info_->Get(slot_id_);
    if (profile_data.IsUndefined()) {
        return ICState::UNINIT;
    }

    if (profile_data.IsHole()) {
        return ICState::MEGA;
    }

    switch (kind_) {
        case ICKind::NAMED_LOAD_IC:
        case ICKind::NAMED_STORE_IC:
            if (profile_data.IsWeak()) {
                return ICState::MONO;
            }
            ASSERT(profile_data.IsTaggedArray());
            return ICState::POLY;
        case ICKind::LOAD_IC:
        case ICKind::STORE_IC: {
            if (profile_data.IsWeak()) {
                return ICState::MONO;
            }
            if (profile_data.IsTaggedArray()) {
                TaggedArray *array = TaggedArray::Cast(profile_data.GetHeapObject());
                return array->GetLength() == MONO_CASE_NUM ? ICState::MONO : ICState::POLY;  // 2 : test case
            }
            profile_data = profile_type_info_->Get(slot_id_ + 1);
            TaggedArray *array = TaggedArray::Cast(profile_data.GetHeapObject());
            return array->GetLength() == MONO_CASE_NUM ? ICState::MONO : ICState::POLY;  // 2 : test case
        }
        case ICKind::NAMED_GLOBAL_LOAD_IC:
        case ICKind::NAMED_GLOBAL_STORE_IC:
            ASSERT(profile_data.IsPropertyBox());
            return ICState::MONO;
        case ICKind::GLOBAL_LOAD_IC:
        case ICKind::GLOBAL_STORE_IC: {
            ASSERT(profile_data.IsTaggedArray());
            TaggedArray *array = TaggedArray::Cast(profile_data.GetHeapObject());
            return array->GetLength() == MONO_CASE_NUM ? ICState::MONO : ICState::POLY;  // 2 : test case
        }
        default:
            UNREACHABLE();
    }
    return ICState::UNINIT;
}
}  // namespace panda::ecmascript
