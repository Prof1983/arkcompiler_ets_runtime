/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_IC_PROPERTY_BOX_H
#define ECMASCRIPT_IC_PROPERTY_BOX_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/property_attributes.h"

namespace panda::ecmascript {
class PropertyBox : public TaggedObject {
public:
    static PropertyBox *Cast(ObjectHeader *object)
    {
        ASSERT(JSTaggedValue(object).IsPropertyBox());
        return static_cast<PropertyBox *>(object);
    }

    void Clear(const JSThread *thread);

    inline bool IsInvalid() const
    {
        return GetValue().IsHole();
    }

    ACCESSORS_START(TaggedObjectSize())
    ACCESSORS(0, Value)
    ACCESSORS_FINISH(1)

    DECL_DUMP()
};
}  // namespace panda::ecmascript

#endif
