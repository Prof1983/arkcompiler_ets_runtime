/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_IC_IC_HANDLER_INL_H
#define ECMASCRIPT_IC_IC_HANDLER_INL_H

#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "ic_handler.h"

namespace panda::ecmascript {
JSHandle<JSTaggedValue> LoadHandler::LoadElement(const JSThread *thread, bool is_int)
{
    uint32_t handler = 0;
    KindBit::Set<uint32_t>(HandlerKind::ELEMENT, &handler);
    IsKeyIntBit::Set<uint32_t>(is_int, &handler);
    return JSHandle<JSTaggedValue>(thread, JSTaggedValue(handler));
}

JSHandle<JSTaggedValue> LoadHandler::LoadProperty(const JSThread *thread, const ObjectOperator &op)
{
    uint32_t handler = 0;
    ASSERT(!op.IsElement());
    if (!op.IsFound()) {
        KindBit::Set<uint32_t>(HandlerKind::NON_EXIST, &handler);
        return JSHandle<JSTaggedValue>(thread, JSTaggedValue(handler));
    }
    ASSERT(op.IsFastMode());

    JSTaggedValue val = op.GetValue();
    if (val.IsPropertyBox()) {
        return JSHandle<JSTaggedValue>(thread, val);
    }
    bool has_accessor = op.IsAccessorDescriptor();
    AccessorBit::Set<uint32_t>(has_accessor, &handler);
    if (!has_accessor) {
        KindBit::Set<uint32_t>(HandlerKind::FIELD, &handler);
    }

    if (op.IsInlinedProps()) {
        InlinedPropsBit::Set<uint32_t>(true, &handler);
        JSHandle<JSObject> holder = JSHandle<JSObject>::Cast(op.GetHolder());
        auto index = holder->GetJSHClass()->GetInlinedPropertiesIndex(op.GetIndex());
        OffsetBit::Set<uint32_t>(index, &handler);
        return JSHandle<JSTaggedValue>(thread, JSTaggedValue(handler));
    }
    if (op.IsFastMode()) {
        OffsetBit::Set<uint32_t>(op.GetIndex(), &handler);
        return JSHandle<JSTaggedValue>(thread, JSTaggedValue(handler));
    }
    UNREACHABLE();
}

JSHandle<JSTaggedValue> PrototypeHandler::LoadPrototype(const JSThread *thread, const ObjectOperator &op,
                                                        const JSHandle<JSHClass> &hclass)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<JSTaggedValue> handler_info = LoadHandler::LoadProperty(thread, op);
    JSHandle<PrototypeHandler> handler = factory->NewPrototypeHandler();
    handler->SetHandlerInfo(thread, handler_info);
    if (op.IsFound()) {
        handler->SetHolder(thread, op.GetHolder());
    }
    auto result = JSHClass::EnableProtoChangeMarker(thread, hclass);
    handler->SetProtoCell(thread, result);
    return JSHandle<JSTaggedValue>::Cast(handler);
}

JSHandle<JSTaggedValue> PrototypeHandler::StorePrototype(const JSThread *thread, const ObjectOperator &op,
                                                         const JSHandle<JSHClass> &hclass)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<PrototypeHandler> handler = factory->NewPrototypeHandler();
    JSHandle<JSTaggedValue> handler_info = StoreHandler::StoreProperty(thread, op);
    handler->SetHandlerInfo(thread, handler_info);
    handler->SetHolder(thread, op.GetHolder());
    auto result = JSHClass::EnableProtoChangeMarker(thread, hclass);
    handler->SetProtoCell(thread, result);
    return JSHandle<JSTaggedValue>::Cast(handler);
}

JSHandle<JSTaggedValue> StoreHandler::StoreElement(const JSThread *thread, JSHandle<JSTaggedValue> receiver,
                                                   bool is_int)
{
    uint32_t handler = 0;
    KindBit::Set<uint32_t>(HandlerKind::ELEMENT, &handler);
    IsKeyIntBit::Set<uint32_t>(is_int, &handler);

    if (receiver->IsJSArray()) {
        IsJSArrayBit::Set<uint32_t>(true, &handler);
    }
    return JSHandle<JSTaggedValue>(thread, JSTaggedValue(handler));
}

JSHandle<JSTaggedValue> StoreHandler::StoreProperty(const JSThread *thread, const ObjectOperator &op)
{
    ASSERT(!op.IsElement());
    uint32_t handler = 0;
    JSTaggedValue val = op.GetValue();
    if (val.IsPropertyBox()) {
        return JSHandle<JSTaggedValue>(thread, val);
    }
    bool has_setter = op.IsAccessorDescriptor();
    AccessorBit::Set<uint32_t>(has_setter, &handler);
    if (!has_setter) {
        KindBit::Set<uint32_t>(HandlerKind::FIELD, &handler);
    }
    if (op.IsInlinedProps()) {
        InlinedPropsBit::Set<uint32_t>(true, &handler);
        JSHandle<JSObject> receiver = JSHandle<JSObject>::Cast(op.GetReceiver());
        auto index = receiver->GetJSHClass()->GetInlinedPropertiesIndex(op.GetIndex());
        OffsetBit::Set<uint32_t>(index, &handler);
        return JSHandle<JSTaggedValue>(thread, JSTaggedValue(handler));
    }
    ASSERT(op.IsFastMode());
    OffsetBit::Set<uint32_t>(op.GetIndex(), &handler);
    return JSHandle<JSTaggedValue>(thread, JSTaggedValue(handler));
}

JSHandle<JSTaggedValue> TransitionHandler::StoreTransition(const JSThread *thread, const ObjectOperator &op)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<TransitionHandler> handler = factory->NewTransitionHandler();
    JSHandle<JSTaggedValue> handler_info = StoreHandler::StoreProperty(thread, op);
    handler->SetHandlerInfo(thread, handler_info);
    auto hclass = JSObject::Cast(op.GetReceiver()->GetHeapObject())->GetJSHClass();
    handler->SetTransitionHClass(thread, JSTaggedValue(hclass));
    return JSHandle<JSTaggedValue>::Cast(handler);
}
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_IC_IC_HANDLER_INL_H
