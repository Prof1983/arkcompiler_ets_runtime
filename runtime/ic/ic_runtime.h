/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_IC_IC_RUNTIME_H
#define ECMASCRIPT_IC_IC_RUNTIME_H

#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/accessor_data.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_handle.h"

namespace panda::ecmascript {
class ProfileTypeInfo;
class JSThread;
class ObjectOperator;

class ICRuntime {
public:
    ICRuntime(JSThread *thread, JSHandle<ProfileTypeInfo> profile_type_info, uint32_t slot_id, ICKind kind)
        : thread_(thread), ic_accessor_(thread, profile_type_info, slot_id, kind)
    {
    }

    ~ICRuntime() = default;

    void UpdateLoadHandler(const ObjectOperator &op, JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> receiver);
    void UpdateStoreHandler(const ObjectOperator &op, JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> receiver);

    JSThread *GetThread() const
    {
        return thread_;
    }

    void UpdateReceiverHClass(JSHandle<JSTaggedValue> receiver_h_class)
    {
        receiver_h_class_ = receiver_h_class;
    }

    ICKind GetICKind() const
    {
        return ic_accessor_.GetKind();
    }

    void TraceIC(JSHandle<JSTaggedValue> receiver, JSHandle<JSTaggedValue> key) const;

    NO_MOVE_SEMANTIC(ICRuntime);
    NO_COPY_SEMANTIC(ICRuntime);

protected:
    JSThread *thread_;                             // NOLINT(misc-non-private-member-variables-in-classes)
    JSHandle<JSTaggedValue> receiver_h_class_ {};  // NOLINT(misc-non-private-member-variables-in-classes)
    ProfileTypeAccessor ic_accessor_;              // NOLINT(misc-non-private-member-variables-in-classes)
};

class LoadICRuntime : public ICRuntime {
public:
    LoadICRuntime(JSThread *thread, JSHandle<ProfileTypeInfo> profile_type_info, uint32_t slot_id, ICKind kind)
        : ICRuntime(thread, profile_type_info, slot_id, kind)
    {
    }

    ~LoadICRuntime() = default;

    JSTaggedValue LoadMiss(JSHandle<JSTaggedValue> receiver, JSHandle<JSTaggedValue> key);

    NO_MOVE_SEMANTIC(LoadICRuntime);
    NO_COPY_SEMANTIC(LoadICRuntime);
};

class StoreICRuntime : public ICRuntime {
public:
    StoreICRuntime(JSThread *thread, JSHandle<ProfileTypeInfo> profile_type_info, uint32_t slot_id, ICKind kind)
        : ICRuntime(thread, profile_type_info, slot_id, kind)
    {
    }

    ~StoreICRuntime() = default;

    JSTaggedValue StoreMiss(JSHandle<JSTaggedValue> receiver, JSHandle<JSTaggedValue> key,
                            JSHandle<JSTaggedValue> value);

    NO_MOVE_SEMANTIC(StoreICRuntime);
    NO_COPY_SEMANTIC(StoreICRuntime);
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_IC_IC_RUNTIME_H
