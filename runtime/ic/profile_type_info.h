/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_IC_PROFILE_TYPE_INFO_H
#define ECMASCRIPT_IC_PROFILE_TYPE_INFO_H

#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

namespace panda::ecmascript {
enum class ICKind {
    NAMED_LOAD_IC,
    NAMED_STORE_IC,
    LOAD_IC,
    STORE_IC,
    NAMED_GLOBAL_LOAD_IC,
    NAMED_GLOBAL_STORE_IC,
    TRY_NAMED_GLOBAL_LOAD_IC,
    TRY_NAMED_GLOBAL_STORE_IC,
    GLOBAL_LOAD_IC,
    GLOBAL_STORE_IC,
};

static inline bool IsNamedGlobalIC(ICKind kind)
{
    return (kind == ICKind::NAMED_GLOBAL_LOAD_IC) || (kind == ICKind::NAMED_GLOBAL_STORE_IC) ||
           (kind == ICKind::TRY_NAMED_GLOBAL_LOAD_IC) || (kind == ICKind::TRY_NAMED_GLOBAL_STORE_IC);
}

static inline bool IsValueGlobalIC(ICKind kind)
{
    return (kind == ICKind::GLOBAL_LOAD_IC) || (kind == ICKind::GLOBAL_STORE_IC);
}

static inline bool IsValueNormalIC(ICKind kind)
{
    return (kind == ICKind::LOAD_IC) || (kind == ICKind::STORE_IC);
}

static inline bool IsValueIC(ICKind kind)
{
    return IsValueNormalIC(kind) || IsValueGlobalIC(kind);
}

static inline bool IsNamedNormalIC(ICKind kind)
{
    return (kind == ICKind::NAMED_LOAD_IC) || (kind == ICKind::NAMED_STORE_IC);
}

static inline bool IsNamedIC(ICKind kind)
{
    return IsNamedNormalIC(kind) || IsNamedGlobalIC(kind);
}

static inline bool IsGlobalLoadIC(ICKind kind)
{
    return (kind == ICKind::NAMED_GLOBAL_LOAD_IC) || (kind == ICKind::GLOBAL_LOAD_IC) ||
           (kind == ICKind::TRY_NAMED_GLOBAL_LOAD_IC);
}

static inline bool IsGlobalStoreIC(ICKind kind)
{
    return (kind == ICKind::NAMED_GLOBAL_STORE_IC) || (kind == ICKind::GLOBAL_STORE_IC) ||
           (kind == ICKind::TRY_NAMED_GLOBAL_STORE_IC);
}

static inline bool IsGlobalIC(ICKind kind)
{
    return IsValueGlobalIC(kind) || IsNamedGlobalIC(kind);
}

std::string ICKindToString(ICKind kind);

class ProfileTypeInfo : public TaggedArray {
public:
    static const uint32_t MAX_FUNC_CACHE_INDEX = std::numeric_limits<uint32_t>::max();
    // Maximal function's bytecode size in byte to create IC for it.
    static constexpr uint32_t MAX_FUNCTION_SIZE = 1024;

    static ProfileTypeInfo *Cast(TaggedObject *object)
    {
        ASSERT(JSTaggedValue(object).IsTaggedArray());
        return static_cast<ProfileTypeInfo *>(object);
    }
};

class ProfileTypeAccessor {
public:
    static constexpr size_t CACHE_MAX_LEN = 8;
    static constexpr size_t MONO_CASE_NUM = 2;
    static constexpr size_t POLY_CASE_NUM = 4;

    enum ICState {
        UNINIT,
        MONO,
        POLY,
        MEGA,
    };

    ProfileTypeAccessor(JSThread *thread, JSHandle<ProfileTypeInfo> profile_type_info, uint32_t slot_id, ICKind kind)
        : thread_(thread), profile_type_info_(profile_type_info), slot_id_(slot_id), kind_(kind)
    {
    }
    ~ProfileTypeAccessor() = default;

    ICState GetICState() const;
    static std::string ICStateToString(ICState state);
    void AddHandlerWithoutKey(JSHandle<JSTaggedValue> dynclass, JSHandle<JSTaggedValue> handler) const;
    void AddElementHandler(JSHandle<JSTaggedValue> dynclass, JSHandle<JSTaggedValue> handler) const;
    void AddHandlerWithKey(JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> dynclass,
                           JSHandle<JSTaggedValue> handler) const;
    void AddGlobalHandlerKey(JSHandle<JSTaggedValue> key, JSHandle<JSTaggedValue> handler) const;

    JSTaggedValue GetWeakRef(JSTaggedValue value) const
    {
        return JSTaggedValue(value.CreateAndGetWeakRef());
    }

    JSTaggedValue GetRefFromWeak(const JSTaggedValue &value) const
    {
        return JSTaggedValue(value.GetWeakReferent());
    }
    void SetAsMega() const;

    ICKind GetKind() const
    {
        return kind_;
    }

    NO_MOVE_SEMANTIC(ProfileTypeAccessor);
    NO_COPY_SEMANTIC(ProfileTypeAccessor);

private:
    JSThread *thread_;
    JSHandle<ProfileTypeInfo> profile_type_info_;
    uint32_t slot_id_;
    ICKind kind_;
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_IC_PROFILE_TYPE_INFO_H
