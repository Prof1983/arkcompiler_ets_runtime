/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ic/ic_runtime.h"
#include "plugins/ecmascript/runtime/global_dictionary-inl.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/ic/ic_handler-inl.h"
#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/interpreter/slow_runtime_stub.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_hclass-inl.h"
#include "plugins/ecmascript/runtime/js_proxy.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/object_factory-inl.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
namespace panda::ecmascript {
#define TRACE_IC 0  // NOLINT(cppcoreguidelines-macro-usage)

void ICRuntime::UpdateLoadHandler(const ObjectOperator &op, JSHandle<JSTaggedValue> key,
                                  JSHandle<JSTaggedValue> receiver)
{
    if (IsNamedIC(GetICKind())) {
        key = JSHandle<JSTaggedValue>();
    }
    JSHandle<JSTaggedValue> handler_value;
    JSHandle<JSHClass> hclass(GetThread(), JSHandle<JSObject>::Cast(receiver)->GetClass());
    if (op.IsElement()) {
        if (!op.IsFound() && hclass->IsDictionaryElement()) {
            return;
        }
        handler_value = LoadHandler::LoadElement(thread_, key.IsEmpty() ? false : key->IsInt());
    } else {
        if (!op.IsFound()) {
            handler_value = PrototypeHandler::LoadPrototype(thread_, op, hclass);
        } else if (!op.IsOnPrototype()) {
            handler_value = LoadHandler::LoadProperty(thread_, op);
        } else {
            // do not support global prototype ic
            if (IsGlobalLoadIC(GetICKind())) {
                return;
            }
            handler_value = PrototypeHandler::LoadPrototype(thread_, op, hclass);
        }
    }

    if (key.IsEmpty()) {
        ic_accessor_.AddHandlerWithoutKey(JSHandle<JSTaggedValue>::Cast(hclass), handler_value);
    } else if (op.IsElement()) {
        // do not support global element ic
        if (IsGlobalLoadIC(GetICKind())) {
            return;
        }
        ic_accessor_.AddElementHandler(JSHandle<JSTaggedValue>::Cast(hclass), handler_value);
    } else {
        ic_accessor_.AddHandlerWithKey(key, JSHandle<JSTaggedValue>::Cast(hclass), handler_value);
    }
}

void ICRuntime::UpdateStoreHandler(const ObjectOperator &op, JSHandle<JSTaggedValue> key,
                                   JSHandle<JSTaggedValue> receiver)
{
    if (IsNamedIC(GetICKind())) {
        key = JSHandle<JSTaggedValue>();
    }
    JSHandle<JSTaggedValue> handler_value;
    if (op.IsElement()) {
        handler_value = StoreHandler::StoreElement(thread_, receiver, key.IsEmpty() ? false : key->IsInt());
    } else {
        ASSERT(op.IsFound());
        if (op.IsOnPrototype()) {
            // do not support global prototype ic
            if (IsGlobalStoreIC(GetICKind())) {
                return;
            }
            JSHandle<JSHClass> hclass(thread_, JSHandle<JSObject>::Cast(receiver)->GetClass());
            handler_value = PrototypeHandler::StorePrototype(thread_, op, hclass);
        } else if (op.IsTransition()) {
            handler_value = TransitionHandler::StoreTransition(thread_, op);
        } else {
            handler_value = StoreHandler::StoreProperty(thread_, op);
        }
    }

    if (key.IsEmpty()) {
        ic_accessor_.AddHandlerWithoutKey(receiver_h_class_, handler_value);
    } else if (op.IsElement()) {
        // do not support global element ic
        if (IsGlobalStoreIC(GetICKind())) {
            return;
        }
        ic_accessor_.AddElementHandler(receiver_h_class_, handler_value);
    } else {
        ic_accessor_.AddHandlerWithKey(key, receiver_h_class_, handler_value);
    }
}

void ICRuntime::TraceIC([[maybe_unused]] JSHandle<JSTaggedValue> receiver,
                        [[maybe_unused]] JSHandle<JSTaggedValue> key) const
{
#if TRACE_IC
    auto kind = ICKindToString(GetICKind());
    auto state = ProfileTypeAccessor::ICStateToString(ic_accessor_.GetICState());
    if (key->IsString()) {
        LOG(ERROR, RUNTIME) << kind << " miss key is: " << JSHandle<EcmaString>::Cast(key)->GetCString().get()
                            << ", receiver is " << receiver->GetHeapObject()->GetClass()->IsDictionaryMode()
                            << ", state is " << state;
    } else {
        LOG(ERROR, RUNTIME) << kind << " miss "
                            << ", state is "
                            << ", receiver is " << receiver->GetHeapObject()->GetClass()->IsDictionaryMode() << state;
    }
#endif
}

JSTaggedValue LoadICRuntime::LoadMiss(JSHandle<JSTaggedValue> receiver, JSHandle<JSTaggedValue> key)
{
    if (receiver->IsTypedArray() || !receiver->IsJSObject()) {
        return JSTaggedValue::GetProperty(thread_, receiver, key).GetValue().GetTaggedValue();
    }

    ObjectOperator op(GetThread(), receiver, key);
    auto result = JSHandle<JSTaggedValue>(thread_, JSObject::GetProperty(GetThread(), &op));
    if (!op.IsFound() &&
        (GetICKind() == ICKind::NAMED_GLOBAL_LOAD_IC || GetICKind() == ICKind::TRY_NAMED_GLOBAL_LOAD_IC)) {
        return SlowRuntimeStub::ThrowReferenceError(GetThread(), key.GetTaggedValue(), " is not definded");
    }
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(GetThread());
    // ic-switch
    if (!GetThread()->GetEcmaVM()->ICEnable()) {
        ic_accessor_.SetAsMega();
        return result.GetTaggedValue();
    }
#ifndef NDEBUG
    TraceIC(receiver, key);
#endif
    // do not cache element
    if (!op.IsFastMode() && op.IsFound()) {
        ic_accessor_.SetAsMega();
        return result.GetTaggedValue();
    }

    UpdateLoadHandler(op, key, receiver);
    return result.GetTaggedValue();
}

JSTaggedValue StoreICRuntime::StoreMiss(JSHandle<JSTaggedValue> receiver, JSHandle<JSTaggedValue> key,
                                        JSHandle<JSTaggedValue> value)
{
    if (receiver->IsTypedArray() || !receiver->IsJSObject()) {
        if (UNLIKELY(!JSTaggedValue::IsPropertyKey(key))) {
            key = JSHandle<JSTaggedValue>(JSTaggedValue::ToString(thread_, key));
        }
        JSTaggedValue::SetProperty(GetThread(), receiver, key, value, true);
        RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(GetThread());
        return JSTaggedValue::Undefined();
    }

    if (GetICKind() == ICKind::TRY_NAMED_GLOBAL_STORE_IC) {
        bool found = false;
        FastRuntimeStub::GetGlobalOwnProperty(receiver.GetTaggedValue(), key.GetTaggedValue(), &found);
        if (!found) {
            return SlowRuntimeStub::ThrowReferenceError(GetThread(), key.GetTaggedValue(), " is not defined");
        }
    }
    UpdateReceiverHClass(JSHandle<JSTaggedValue>(GetThread(), JSHandle<JSObject>::Cast(receiver)->GetClass()));

    ObjectOperator op(GetThread(), receiver, key);
    JSHandle<JSHClass> old_hc(thread_, JSObject::Cast(receiver.GetTaggedValue())->GetJSHClass());
    bool success = JSObject::SetProperty(&op, value, true);
    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread_);

    auto new_hc = JSObject::Cast(receiver.GetTaggedValue())->GetJSHClass();
    // Update object operator if there was transition to dictionary mode
    if (UNLIKELY(!old_hc->IsDictionaryMode() && new_hc->IsDictionaryMode())) {
        op = ObjectOperator(GetThread(), receiver, key);
    }

    if (!success &&
        (GetICKind() == ICKind::NAMED_GLOBAL_STORE_IC || GetICKind() == ICKind::TRY_NAMED_GLOBAL_STORE_IC)) {
        return SlowRuntimeStub::ThrowReferenceError(GetThread(), key.GetTaggedValue(), " is not defined");
    }
    // ic-switch
    if (!GetThread()->GetEcmaVM()->ICEnable()) {
        ic_accessor_.SetAsMega();
        return success ? JSTaggedValue::Undefined() : JSTaggedValue::Exception();
    }
#ifndef NDEBUG
    TraceIC(receiver, key);
#endif
    // do not cache element
    if (!op.IsFastMode()) {
        ic_accessor_.SetAsMega();
        return success ? JSTaggedValue::Undefined() : JSTaggedValue::Exception();
    }
    if (success) {
        UpdateStoreHandler(op, key, receiver);
        return JSTaggedValue::Undefined();
    }
    return JSTaggedValue::Exception();
}
}  // namespace panda::ecmascript
