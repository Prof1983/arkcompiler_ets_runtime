/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/ic/proto_change_details.h"
#include "plugins/ecmascript/runtime/weak_vector-inl.h"

namespace panda::ecmascript {
JSHandle<ChangeListener> ChangeListener::Add(const JSThread *thread, const JSHandle<ChangeListener> &array,
                                             const JSHandle<JSHClass> &value, uint32_t *index)
{
    if (!array->Full()) {
        uint32_t array_index = array->PushBack(thread, value.GetTaggedValue());
        if (array_index != TaggedArray::MAX_ARRAY_INDEX) {
            if (index != nullptr) {
                *index = array_index;
            }
            return array;
        }
        UNREACHABLE();
    }
    // if exist hole, use it.
    uint32_t hole_index = CheckHole(array);
    if (hole_index != TaggedArray::MAX_ARRAY_INDEX) {
        array->Set(thread, hole_index, value.GetTaggedValue());
        if (index != nullptr) {
            *index = hole_index;
        }
        return array;
    }
    // the vector is full and no hole exists.
    JSHandle<WeakVector> new_array = WeakVector::Grow(thread, JSHandle<WeakVector>(array), array->GetCapacity() + 1);
    uint32_t array_index = new_array->PushBack(thread, value.GetTaggedValue());
    ASSERT(array_index != TaggedArray::MAX_ARRAY_INDEX);
    if (index != nullptr) {
        *index = array_index;
    }
    return JSHandle<ChangeListener>(new_array);
}

uint32_t ChangeListener::CheckHole(const JSHandle<ChangeListener> &array)
{
    for (uint32_t i = 0; i < array->GetEnd(); i++) {
        JSTaggedValue value = array->Get(i);
        if (value == JSTaggedValue::Hole() || value == JSTaggedValue::Undefined()) {
            return i;
        }
    }
    return TaggedArray::MAX_ARRAY_INDEX;
}

JSTaggedValue ChangeListener::Get(uint32_t index)
{
    JSTaggedValue value = WeakVector::Get(index);
    if (!value.IsHeapObject()) {
        return value;
    }
    return value;
}
}  // namespace panda::ecmascript
