/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_IC_IC_RUNTIME_STUB_INL_H_
#define ECMASCRIPT_IC_IC_RUNTIME_STUB_INL_H_

#include "ic_runtime_stub.h"
#include "ic_handler.h"
#include "ic_runtime.h"
#include "profile_type_info.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_hclass-inl.h"
#include "plugins/ecmascript/runtime/global_dictionary-inl.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_proxy.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/object_factory-inl.h"
#include "plugins/ecmascript/runtime/js_handle.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/ic/proto_change_details.h"

#include "plugins/ecmascript/runtime/vmstat/runtime_stat.h"
#include "plugins/ecmascript/runtime/runtime_call_id.h"

namespace panda::ecmascript {
bool ICRuntimeStub::HaveICForFunction(JSFunction *func)
{
    return !func->GetProfileTypeInfo().IsUndefined();
}

uint32_t ICRuntimeStub::MapSlotId(JSFunction *func, uint32_t slot_id)
{
    uint8_t *mapping = func->GetMethod()->GetICMapping();
    ASSERT(mapping != nullptr);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return mapping[slot_id];
}

ProfileTypeInfo *ICRuntimeStub::GetRuntimeProfileTypeInfo(JSFunction *func)
{
    JSTaggedValue profile_type_info = func->GetProfileTypeInfo();
    return ProfileTypeInfo::Cast(profile_type_info.GetTaggedObject());
}

JSTaggedValue ICRuntimeStub::LoadGlobalICByName(JSThread *thread, JSFunction *func, JSTaggedValue global_value,
                                                JSTaggedValue key, uint32_t slot_id, bool is_try_load)
{
    INTERPRETER_TRACE(thread, LoadGlobalICByName);
    ASSERT(HaveICForFunction(func));
    slot_id = MapSlotId(func, slot_id);
    ProfileTypeInfo *profile_type_info = GetRuntimeProfileTypeInfo(func);
    JSTaggedValue handler = profile_type_info->Get(slot_id);
    if (handler.IsHeapObject()) {
        auto result = LoadGlobal(handler);
        if (!result.IsHole()) {
            return result;
        }
    }

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto key_handle = JSHandle<JSTaggedValue>(thread, key);
    auto receiver_handle = JSHandle<JSTaggedValue>(thread, global_value);
    auto profile_info_handle = JSHandle<JSTaggedValue>(thread, profile_type_info);
    LoadICRuntime ic_runtime(thread, JSHandle<ProfileTypeInfo>::Cast(profile_info_handle), slot_id,
                             is_try_load ? ICKind::TRY_NAMED_GLOBAL_LOAD_IC : ICKind::NAMED_GLOBAL_LOAD_IC);
    return ic_runtime.LoadMiss(receiver_handle, key_handle);
}

JSTaggedValue ICRuntimeStub::StoreGlobalICByName(JSThread *thread, JSFunction *func, JSTaggedValue global_value,
                                                 JSTaggedValue key, JSTaggedValue value, uint32_t slot_id,
                                                 bool is_try_store)
{
    INTERPRETER_TRACE(thread, StoreGlobalICByName);
    ASSERT(HaveICForFunction(func));
    slot_id = MapSlotId(func, slot_id);
    ProfileTypeInfo *profile_type_info = GetRuntimeProfileTypeInfo(func);
    JSTaggedValue handler = profile_type_info->Get(slot_id);
    if (handler.IsHeapObject()) {
        auto result = StoreGlobal(thread, value, handler);
        if (!result.IsHole()) {
            return result;
        }
    }

    [[maybe_unused]] EcmaHandleScope handle_scope(thread);
    auto key_handle = JSHandle<JSTaggedValue>(thread, key);
    auto receiver_handle = JSHandle<JSTaggedValue>(thread, global_value);
    auto value_handle = JSHandle<JSTaggedValue>(thread, value);
    auto profile_info_handle = JSHandle<JSTaggedValue>(thread, profile_type_info);
    StoreICRuntime ic_runtime(thread, JSHandle<ProfileTypeInfo>::Cast(profile_info_handle), slot_id,
                              is_try_store ? ICKind::TRY_NAMED_GLOBAL_STORE_IC : ICKind::NAMED_GLOBAL_STORE_IC);
    return ic_runtime.StoreMiss(receiver_handle, key_handle, value_handle);
}

JSTaggedValue ICRuntimeStub::CheckPolyHClass(JSTaggedValue cached_value, JSHClass *hclass)
{
    if (!cached_value.IsWeak()) {
        ASSERT(cached_value.IsTaggedArray());
        TaggedArray *array = TaggedArray::Cast(cached_value.GetHeapObject());
        uint32_t length = array->GetLength();
        for (uint32_t i = 0; i < length; i += 2) {  // 2 means one ic, two slot
            auto result = array->Get(i);
            if (result != JSTaggedValue::Undefined() && result.GetWeakReferent() == hclass) {
                return array->Get(i + 1);
            }
        }
    }
    return JSTaggedValue::Hole();
}

JSTaggedValue ICRuntimeStub::LoadICByName(JSThread *thread, JSFunction *func, JSTaggedValue receiver, JSTaggedValue key,
                                          uint32_t slot_id)
{
    INTERPRETER_TRACE(thread, LoadICByName);
    ASSERT(HaveICForFunction(func));
    slot_id = MapSlotId(func, slot_id);
    ProfileTypeInfo *profile_type_info = GetRuntimeProfileTypeInfo(func);
    JSTaggedValue hclass_value = profile_type_info->Get(slot_id);
    if (hclass_value.IsHole()) {
        return JSTaggedValue::Hole();
    }
    if (LIKELY(receiver.IsHeapObject())) {
        auto hclass = receiver.GetTaggedObject()->GetClass();
        size_t index = slot_id;
        do {
            if (!hclass_value.IsHeapObject()) {
                break;
            }
            if (LIKELY(hclass_value.GetTaggedObject() == hclass)) {
                JSTaggedValue handler = profile_type_info->Get(index + 1);
                if (UNLIKELY(handler.IsUndefined())) {
                    // handler is collected by GC.
                    // Clear profile data to fill it again in LoadMiss.
                    profile_type_info->Set(thread, index, JSTaggedValue::Undefined());
                    break;
                }
                return LoadICWithHandler(thread, receiver, receiver, handler);
            }
            index += 2U;
            hclass_value = profile_type_info->Get(index);
        } while (index < slot_id + SlotSizeForICByName());
    }
    return LoadMissedICByName(thread, profile_type_info, receiver, key, slot_id);
}

JSTaggedValue ICRuntimeStub::LoadICByValue(JSThread *thread, JSFunction *func, JSTaggedValue receiver,
                                           JSTaggedValue key, uint32_t slot_id)
{
    INTERPRETER_TRACE(thread, LoadICByValue);
    ASSERT(HaveICForFunction(func));
    slot_id = MapSlotId(func, slot_id);
    ProfileTypeInfo *profile_type_info = GetRuntimeProfileTypeInfo(func);
    JSTaggedValue first_value = profile_type_info->Get(slot_id);
    if (first_value.IsHole()) {
        return JSTaggedValue::Hole();
    }
    if (receiver.IsHeapObject() && first_value.IsHeapObject()) {
        auto hclass = receiver.GetTaggedObject()->GetClass();
        // Check key
        if (first_value == key) {
            JSTaggedValue hclass_value = profile_type_info->Get(slot_id + 1);
            if (hclass_value.GetTaggedObject() == hclass) {
                JSTaggedValue handler = profile_type_info->Get(slot_id + 2);
                if (UNLIKELY(handler.IsUndefined())) {
                    // handler is collected by GC.
                    // Clear profile data to fill it again in LoadMiss.
                    profile_type_info->Set(thread, slot_id, JSTaggedValue::Undefined());
                    profile_type_info->Set(thread, slot_id + 1, JSTaggedValue::Undefined());
                } else {
                    return LoadICWithHandler(thread, receiver, receiver, handler);
                }
            }
        }
        // Check element
        if (first_value.GetTaggedObject() == hclass) {
            ASSERT(HandlerBase::IsElement(profile_type_info->Get(slot_id + 1).GetInt()));
            if (!key.IsInt()) {
                JSTaggedValue handler = profile_type_info->Get(slot_id + 1);
                auto handler_info = static_cast<uint32_t>(handler.GetInt());
                HandlerBase::SetKeyNotInt(handler_info);
                profile_type_info->Set(thread, slot_id + 1, JSTaggedValue(handler_info));
            }
            return LoadElement(JSObject::Cast(receiver.GetHeapObject()), key);
        }
        size_t index = slot_id + 2;
        while (index < SlotSizeForICByValue()) {
            first_value = profile_type_info->Get(index);
            if (first_value.GetRawData() == ToUintPtr(hclass)) {
                ASSERT(HandlerBase::IsElement(profile_type_info->Get(index + 1).GetInt()));
                if (!key.IsInt()) {
                    JSTaggedValue handler = profile_type_info->Get(index + 1);
                    auto handler_info = static_cast<uint32_t>(handler.GetInt());
                    HandlerBase::SetKeyNotInt(handler_info);
                    profile_type_info->Set(thread, index + 1, JSTaggedValue(handler_info));
                }
                return LoadElement(JSObject::Cast(receiver.GetHeapObject()), key);
            }
            index += 2;
        }
    }
    return LoadMissedICByValue(thread, profile_type_info, receiver, key, slot_id);
}

JSTaggedValue ICRuntimeStub::StoreICByValue(JSThread *thread, JSFunction *func, JSTaggedValue receiver,
                                            JSTaggedValue key, JSTaggedValue value, uint32_t slot_id)
{
    INTERPRETER_TRACE(thread, StoreICByValue);
    ASSERT(HaveICForFunction(func));
    slot_id = MapSlotId(func, slot_id);
    ProfileTypeInfo *profile_type_info = GetRuntimeProfileTypeInfo(func);
    JSTaggedValue first_value = profile_type_info->Get(slot_id);
    if (first_value.IsHole()) {
        return JSTaggedValue::Hole();
    }
    if (receiver.IsHeapObject() && first_value.IsHeapObject()) {
        auto hclass = receiver.GetTaggedObject()->GetClass();
        // Check key
        if (first_value == key) {
            JSTaggedValue hclass_value = profile_type_info->Get(slot_id + 1);
            if (hclass_value.GetTaggedObject() == hclass) {
                JSTaggedValue handler = profile_type_info->Get(slot_id + 2);
                if (UNLIKELY(handler.IsUndefined())) {
                    // handler is collected by GC.
                    // Clear profile data to fill it again in StoreMiss.
                    profile_type_info->Set(thread, slot_id, JSTaggedValue::Undefined());
                    profile_type_info->Set(thread, slot_id + 1, JSTaggedValue::Undefined());
                } else {
                    return StoreICWithHandler(thread, receiver, receiver, value, handler);
                }
            }
        }
        // Check element
        if (first_value.GetTaggedObject() == hclass) {
            return StoreElement(thread, JSObject::Cast(receiver.GetHeapObject()), key, value, profile_type_info,
                                slot_id + 1);
        }
        size_t index = slot_id + 2;
        while (index < SlotSizeForICByValue()) {
            first_value = profile_type_info->Get(index);
            if (first_value.GetRawData() == ToUintPtr(hclass)) {
                return StoreElement(thread, JSObject::Cast(receiver.GetHeapObject()), key, value, profile_type_info,
                                    index + 1);
            }
            index += 2;
        }
    }

    return StoreMissedICByValue(thread, profile_type_info, receiver, key, value, slot_id);
}

JSTaggedValue ICRuntimeStub::StoreICByName(JSThread *thread, JSFunction *func, JSTaggedValue receiver,
                                           JSTaggedValue key, JSTaggedValue value, uint32_t slot_id)
{
    INTERPRETER_TRACE(thread, StoreICByName);
    ASSERT(HaveICForFunction(func));
    slot_id = MapSlotId(func, slot_id);
    ProfileTypeInfo *profile_type_info = GetRuntimeProfileTypeInfo(func);
    JSTaggedValue hclass_value = profile_type_info->Get(slot_id);
    if (hclass_value.IsHole()) {
        return JSTaggedValue::Hole();
    }
    if (receiver.IsHeapObject()) {
        auto hclass = receiver.GetTaggedObject()->GetClass();
        size_t index = slot_id;
        do {
            if (!hclass_value.IsHeapObject()) {
                break;
            }
            if (LIKELY(hclass_value.GetTaggedObject() == hclass)) {
                JSTaggedValue handler = profile_type_info->Get(index + 1);
                if (UNLIKELY(handler.IsUndefined())) {
                    // handler is collected by GC.
                    // Clear profile data to fill it again in StoreMiss.
                    profile_type_info->Set(thread, index, JSTaggedValue::Undefined());
                    break;
                }
                return StoreICWithHandler(thread, receiver, receiver, value, handler);
            }
            index += 2U;
            hclass_value = profile_type_info->Get(index);
        } while (index < slot_id + SlotSizeForICByName());
    }
    return StoreMissedICByName(thread, profile_type_info, receiver, key, value, slot_id);
}

JSTaggedValue ICRuntimeStub::StoreICWithHandler(JSThread *thread, JSTaggedValue receiver, JSTaggedValue holder,
                                                JSTaggedValue value, JSTaggedValue handler)
{
    INTERPRETER_TRACE(thread, StoreICWithHandler);
    if (handler.IsInt()) {
        auto handler_info = static_cast<uint32_t>(handler.GetInt());
        if (HandlerBase::IsField(handler_info)) {
            StoreField(thread, JSObject::Cast(receiver.GetHeapObject()), value, handler_info);
            return JSTaggedValue::Undefined();
        }
        ASSERT(HandlerBase::IsAccessor(handler_info) || HandlerBase::IsInternalAccessor(handler_info));
        auto accessor = LoadFromField(JSObject::Cast(holder.GetHeapObject()), handler_info);
        return FastRuntimeStub::CallSetter(thread, JSTaggedValue(receiver), value, accessor);
    }
    if (handler.IsTransitionHandler()) {
        StoreWithTransition(thread, JSObject::Cast(receiver.GetHeapObject()), value, handler);
        return JSTaggedValue::Undefined();
    }
    if (handler.IsPrototypeHandler()) {
        return StorePrototype(thread, receiver, value, handler);
    }
    if (handler.IsPropertyBox()) {
        return StoreGlobal(thread, value, handler);
    }
    return JSTaggedValue::Undefined();
}

JSTaggedValue ICRuntimeStub::StorePrototype(JSThread *thread, JSTaggedValue receiver, JSTaggedValue value,
                                            JSTaggedValue handler)
{
    INTERPRETER_TRACE(thread, StorePrototype);
    ASSERT(handler.IsPrototypeHandler());
    PrototypeHandler *prototype_handler = PrototypeHandler::Cast(handler.GetTaggedObject());
    auto cell_value = prototype_handler->GetProtoCell();
    ASSERT(cell_value.IsProtoChangeMarker());
    ProtoChangeMarker *cell = ProtoChangeMarker::Cast(cell_value.GetHeapObject());
    if (cell->GetHasChanged()) {
        return JSTaggedValue::Hole();
    }
    auto holder = prototype_handler->GetHolder();
    JSTaggedValue handler_info = prototype_handler->GetHandlerInfo();
    return StoreICWithHandler(thread, receiver, holder, value, handler_info);
}

void ICRuntimeStub::StoreWithTransition(JSThread *thread, JSObject *receiver, JSTaggedValue value,
                                        JSTaggedValue handler)
{
    INTERPRETER_TRACE(thread, StoreWithTransition);
    TransitionHandler *transition_handler = TransitionHandler::Cast(handler.GetTaggedObject());
    JSHClass *new_h_class = JSHClass::Cast(transition_handler->GetTransitionHClass().GetTaggedObject());
    receiver->SetClass(new_h_class);
    uint32_t handler_info = transition_handler->GetHandlerInfo().GetInt();
    ASSERT(HandlerBase::IsField(handler_info));

    if (!HandlerBase::IsInlinedProps(handler_info)) {
        TaggedArray *array = TaggedArray::Cast(receiver->GetProperties().GetHeapObject());
        int capacity = array->GetLength();
        int index = HandlerBase::GetOffset(handler_info);
        if (index >= capacity) {
            [[maybe_unused]] EcmaHandleScope handle_scope(thread);
            transition_handler->SetArrayOverflowed(thread);
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            JSHandle<TaggedArray> properties;
            JSHandle<JSObject> obj_handle(thread, receiver);
            JSHandle<JSTaggedValue> value_handle(thread, value);
            if (capacity == 0) {
                capacity = JSObject::MIN_PROPERTIES_LENGTH;
                properties = factory->NewTaggedArray(capacity);
            } else {
                auto array_handle = JSHandle<TaggedArray>(thread, array);
                properties = factory->CopyArray(array_handle, capacity, JSObject::ComputePropertyCapacity(capacity));
            }
            properties->Set(thread, index, value_handle);
            obj_handle->SetProperties(thread, properties);
            return;
        }
        array->Set(thread, index, value);
        return;
    }
    StoreField(thread, receiver, value, handler_info);
}

void ICRuntimeStub::StoreField(JSThread *thread, JSObject *receiver, JSTaggedValue value, uint32_t handler)
{
    INTERPRETER_TRACE(thread, StoreField);
    int index = HandlerBase::GetOffset(handler);
    if (HandlerBase::IsInlinedProps(handler)) {
        SET_VALUE_WITH_BARRIER(thread, receiver, index * JSTaggedValue::TaggedTypeSize(), value);
        return;
    }
    TaggedArray *array = TaggedArray::Cast(receiver->GetProperties().GetHeapObject());
    ASSERT(index < static_cast<int>(array->GetLength()));
    array->Set(thread, index, value);
}

JSTaggedValue ICRuntimeStub::LoadFromField(JSObject *receiver, uint32_t handler_info)
{
    int index = HandlerBase::GetOffset(handler_info);
    if (HandlerBase::IsInlinedProps(handler_info)) {
        return JSTaggedValue(GET_VALUE(receiver, index * JSTaggedValue::TaggedTypeSize()));
    }
    return TaggedArray::Cast(receiver->GetProperties().GetHeapObject())->Get(index);
}

JSTaggedValue ICRuntimeStub::LoadGlobal(JSTaggedValue handler)
{
    ASSERT(handler.IsPropertyBox());
    PropertyBox *cell = PropertyBox::Cast(handler.GetHeapObject());
    if (cell->IsInvalid()) {
        return JSTaggedValue::Hole();
    }
    JSTaggedValue ret = cell->GetValue();
    ASSERT(!ret.IsAccessorData());
    return ret;
}

JSTaggedValue ICRuntimeStub::StoreGlobal(JSThread *thread, JSTaggedValue value, JSTaggedValue handler)
{
    INTERPRETER_TRACE(thread, StoreGlobal);
    ASSERT(handler.IsPropertyBox());
    PropertyBox *cell = PropertyBox::Cast(handler.GetHeapObject());
    if (cell->IsInvalid()) {
        return JSTaggedValue::Hole();
    }
    ASSERT(!cell->GetValue().IsAccessorData());
    cell->SetValue(thread, value);
    return JSTaggedValue::Undefined();
}

JSTaggedValue ICRuntimeStub::LoadPrototype(JSThread *thread, JSTaggedValue receiver, JSTaggedValue handler)
{
    INTERPRETER_TRACE(thread, LoadPrototype);
    ASSERT(handler.IsPrototypeHandler());
    PrototypeHandler *prototype_handler = PrototypeHandler::Cast(handler.GetTaggedObject());
    auto cell_value = prototype_handler->GetProtoCell();
    if (cell_value.IsFalse()) {
        // property was not found and object has no prototype
        return JSTaggedValue::Hole();
    }
    ASSERT(cell_value.IsProtoChangeMarker());
    ProtoChangeMarker *cell = ProtoChangeMarker::Cast(cell_value.GetHeapObject());
    if (cell->GetHasChanged()) {
        return JSTaggedValue::Hole();
    }
    auto holder = prototype_handler->GetHolder();
    JSTaggedValue handler_info = prototype_handler->GetHandlerInfo();
    return LoadICWithHandler(thread, receiver, holder, handler_info);
}

JSTaggedValue ICRuntimeStub::LoadICWithHandler(JSThread *thread, JSTaggedValue receiver, JSTaggedValue holder,
                                               JSTaggedValue handler)
{
    INTERPRETER_TRACE(thread, LoadICWithHandler);
    if (LIKELY(handler.IsInt())) {
        auto handler_info = static_cast<uint32_t>(handler.GetInt());
        if (LIKELY(HandlerBase::IsField(handler_info))) {
            return LoadFromField(JSObject::Cast(holder.GetHeapObject()), handler_info);
        }
        if (HandlerBase::IsNonExist(handler_info)) {
            return JSTaggedValue::Hole();
        }
        ASSERT(HandlerBase::IsAccessor(handler_info) || HandlerBase::IsInternalAccessor(handler_info));
        auto accessor = LoadFromField(JSObject::Cast(holder.GetHeapObject()), handler_info);
        return FastRuntimeStub::CallGetter(thread, receiver, holder, accessor);
    }

    if (handler.IsPrototypeHandler()) {
        return LoadPrototype(thread, receiver, handler);
    }

    return LoadGlobal(handler);
}

JSTaggedValue ICRuntimeStub::LoadElement(JSObject *receiver, JSTaggedValue key)
{
    auto index = TryToElementsIndex(key);
    if (index >= JSObject::MAX_ELEMENT_INDEX) {
        return JSTaggedValue::Hole();
    }
    uint32_t element_index = index;
    TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetHeapObject());
    if (elements->GetLength() <= element_index) {
        return JSTaggedValue::Hole();
    }

    JSTaggedValue value = elements->Get(element_index);
    // TaggedArray elements
    return value;
}

JSTaggedValue ICRuntimeStub::StoreElement(JSThread *thread, JSObject *receiver, JSTaggedValue key, JSTaggedValue value,
                                          ProfileTypeInfo *profile_type_info, uint32_t slot_id)
{
    INTERPRETER_TRACE(thread, StoreElement);
    JSTaggedValue handler = profile_type_info->Get(slot_id);
    auto handler_info = static_cast<uint32_t>(handler.GetInt());
    ASSERT(HandlerBase::IsElement(handler_info));
    if (!key.IsInt()) {
        HandlerBase::SetKeyNotInt(handler_info);
        profile_type_info->Set(thread, slot_id, JSTaggedValue(handler_info));
    }
    auto index = TryToElementsIndex(key);
    if (index >= JSObject::MAX_ELEMENT_INDEX) {
        return JSTaggedValue::Hole();
    }
    uint32_t element_index = index;
    if (HandlerBase::IsJSArray(handler_info)) {
        JSArray *arr = JSArray::Cast(receiver);
        uint32_t old_length = arr->GetArrayLength();
        if (element_index >= old_length) {
            arr->SetArrayLength(thread, element_index + 1);
        }
    }
    TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetHeapObject());
    uint32_t capacity = elements->GetLength();
    if (element_index >= capacity) {
        HandlerBase::SetArrayOverflowed(handler_info);
        profile_type_info->Set(thread, slot_id, JSTaggedValue(handler_info));
        if (JSObject::ShouldTransToDict(capacity, element_index)) {
            return JSTaggedValue::Hole();
        }
        [[maybe_unused]] EcmaHandleScope handle_scope(thread);
        JSHandle<JSObject> receiver_handle(thread, receiver);
        JSHandle<JSTaggedValue> value_handle(thread, value);
        elements = *JSObject::GrowElementsCapacity(thread, receiver_handle,
                                                   JSObject::ComputeElementCapacity(element_index + 1));
        receiver_handle->SetElements(thread, JSTaggedValue(elements));
        elements->Set(thread, element_index, value_handle);
        return JSTaggedValue::Undefined();
    }
    elements->Set(thread, element_index, value);
    return JSTaggedValue::Undefined();
}

uint32_t ICRuntimeStub::TryToElementsIndex(JSTaggedValue key)
{
    if (LIKELY(key.IsInt())) {
        return key.GetInt();
    }

    if (key.IsString()) {
        uint32_t index = 0;
        if (JSTaggedValue::StringToElementIndex(key, &index)) {
            return index;
        }
    }

    if (key.IsDouble()) {
        double number = key.GetDouble();
        auto integer = static_cast<uint32_t>(number);
        if (number == integer) {
            return integer;
        }
    }

    return JSObject::MAX_ELEMENT_INDEX;
}
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_IC_IC_RUNTIME_STUB_INL_H_
