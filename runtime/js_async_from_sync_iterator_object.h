/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description:
 */
#ifndef ECMASCRIPT_JS_ASYNC_FROM_SYNC_ITERATOR_OBJECT_H
#define ECMASCRIPT_JS_ASYNC_FROM_SYNC_ITERATOR_OBJECT_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "include/object_header.h"
#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "plugins/ecmascript/runtime/js_function.h"

namespace panda::ecmascript {
class JSAsyncFromSyncIteratorValueUnwrapFunction : public JSFunction {
public:
    CAST_CHECK(JSAsyncFromSyncIteratorValueUnwrapFunction, IsJSAsyncFromSyncIteratorValueUnwrapFunction);

    // 27.1.4.2.4 Async-from-Sync Iterator Value Unwrap Functions
    static JSTaggedValue AsyncFromSyncIteratorValueUnwrapFunction(EcmaRuntimeCallInfo *argv);

    ACCESSORS_BASE(JSFunction)
    ACCESSORS(0, Done)
    ACCESSORS_FINISH(1)

    DECL_DUMP()
};

class JSAsyncFromSyncIteratorObject : public JSObject {
public:
    CAST_CHECK(JSAsyncFromSyncIteratorObject, IsJSAsyncFromSyncIteratorObject);

    // 27.1.4.1 CreateAsyncFromSyncIterator ( syncIteratorRecord )
    static JSTaggedValue CreateAsyncFromSyncIterator(JSThread *thread, const JSHandle<JSTaggedValue> &iterator,
                                                     const JSHandle<JSTaggedValue> &next_method);

    // 27.1.4.4 AsyncFromSyncIteratorContinuation ( result, promiseCapability )
    static JSTaggedValue AsyncFromSyncIteratorContinuation(JSThread *thread, const JSHandle<JSTaggedValue> &result,
                                                           const JSHandle<PromiseCapability> &promise_capability);

    ACCESSORS_BASE(JSObject)
    ACCESSORS(0, Iterator)
    ACCESSORS(1, NextMethod)
    ACCESSORS_FINISH(2)
};
}  // namespace panda::ecmascript

#endif  // ECMASCRIPT_JS_ASYNC_FROM_SYNC_ITERATOR_OBJECT_H
