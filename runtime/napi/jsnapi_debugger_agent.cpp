/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "jsnapi_debugger_agent.h"

#include "plugins/ecmascript/runtime/ecma_macros.h"

namespace panda {
JSNApiDebuggerAgent::JSNApiDebuggerAgent(os::memory::Mutex &mutex, const char *library_path, EcmaVM *vm,
                                         bool is_debug_mode)
    : LibraryAgent(mutex, library_path, "StartDebug", "StopDebug"), vm_(vm), is_debug_mode_(is_debug_mode)
{
}

bool JSNApiDebuggerAgent::CallLoadCallback(void *resolved_function)
{
    ASSERT(resolved_function);

    using StartDebugger = bool (*)(const std::string &, EcmaVM *, bool);
    if (!reinterpret_cast<StartDebugger>(resolved_function)("PandaDebugger", vm_, is_debug_mode_)) {
        LOG_ECMA(ERROR) << "'StartDebug' has failed";
        return false;
    }

    return true;
}

bool JSNApiDebuggerAgent::CallUnloadCallback(void *resolved_function)
{
    ASSERT(resolved_function);

    using StopDebug = void (*)(const std::string &);
    reinterpret_cast<StopDebug>(resolved_function)("PandaDebugger");

    return true;
}
}  // namespace panda
