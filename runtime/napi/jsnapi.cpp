/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "jsnapi_helper-inl.h"
#include "jsnapi_debugger_agent.h"

#include <array>
#include <cstdint>

#include "runtime/include/thread_scopes.h"
#include "plugins/ecmascript/runtime/base/builtins_base.h"
#include "plugins/ecmascript/runtime/base/json_parser.h"
#include "plugins/ecmascript/runtime/base/json_stringifier.h"
#include "plugins/ecmascript/runtime/base/string_helper.h"
#include "plugins/ecmascript/runtime/base/typed_array_helper-inl.h"
#include "plugins/ecmascript/runtime/ecma_global_storage-inl.h"
#include "plugins/ecmascript/runtime/ecma_language_context.h"
#include "plugins/ecmascript/runtime/ecma_module.h"
#include "plugins/ecmascript/runtime/ecma_string.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/internal_call_params.h"
#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/jobs/micro_job_queue.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_dataview.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_map.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/js_promise.h"
#include "plugins/ecmascript/runtime/js_regexp.h"
#include "plugins/ecmascript/runtime/js_runtime_options.h"
#include "plugins/ecmascript/runtime/js_serializer.h"
#include "plugins/ecmascript/runtime/js_set.h"
#include "plugins/ecmascript/runtime/js_tagged_number.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array.h"

#include "generated/base_options.h"
#include "include/runtime_notification.h"
#include "libpandabase/os/library_loader.h"
#include "utils/pandargs.h"

namespace panda {
using ecmascript::EcmaString;
using ecmascript::ErrorType;
using ecmascript::FastRuntimeStub;
using ecmascript::GlobalEnv;
using ecmascript::GlobalEnvConstants;
using ecmascript::JSArray;
using ecmascript::JSArrayBuffer;
using ecmascript::JSDataView;
using ecmascript::JSDate;
using ecmascript::JSFunction;
using ecmascript::JSFunctionBase;
using ecmascript::JSFunctionExtraInfo;
using ecmascript::JSHClass;
using ecmascript::JSMap;
using ecmascript::JSMethod;
using ecmascript::JSNativePointer;
using ecmascript::JSObject;
using ecmascript::JSPrimitiveRef;
using ecmascript::JSPromise;
using ecmascript::JSRegExp;
using ecmascript::JSRuntimeOptions;
using ecmascript::JSSet;
using ecmascript::JSSymbol;
using ecmascript::JSTaggedNumber;
using ecmascript::JSTaggedType;
using ecmascript::JSTaggedValue;
using ecmascript::JSThread;
using ecmascript::JSTypedArray;
using ecmascript::ObjectFactory;
using ecmascript::OperationResult;
using ecmascript::PromiseCapability;
using ecmascript::PropertyDescriptor;
using ecmascript::TaggedArray;
using ecmascript::base::JsonParser;
using ecmascript::base::JsonStringifier;
using ecmascript::base::StringHelper;
using ecmascript::job::MicroJobQueue;
using ecmascript::job::QueueType;
template <typename T>
using JSHandle = ecmascript::JSHandle<T>;

namespace {
constexpr uint32_t INTERNAL_POOL_SIZE = 0;
constexpr uint32_t CODE_POOL_SIZE = 2000000;
constexpr uint32_t COMPILER_POOL_SIZE = 2000000;
// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
constexpr std::string_view ENTRY_POINTER = "_GLOBAL::func_main_0";

LoadableAgentHandle S_DEBUGGER_AGENT;  // NOLINT(fuchsia-statically-constructed-objects)
}  // namespace

// ------------------------------------ Panda -----------------------------------------------
bool JSNApi::CreateRuntime(const RuntimeOption &option)
{
    JSRuntimeOptions runtime_options;

    // GC
    runtime_options.SetGcType(option.GetGcType());
    runtime_options.SetRunGcInPlace(true);
    runtime_options.SetArkProperties(option.GetArkProperties());
    runtime_options.SetExplicitConcurrentGcEnabled(option.GetExplicitConcurrentGcEnabled());
    // Mem
    runtime_options.SetHeapSizeLimit(option.GetGcPoolSize());
    runtime_options.SetInternalMemorySizeLimit(INTERNAL_POOL_SIZE);
    runtime_options.SetCodeCacheSizeLimit(CODE_POOL_SIZE);
    runtime_options.SetCompilerMemorySizeLimit(COMPILER_POOL_SIZE);
    runtime_options.SetInternalAllocatorType("malloc");

    // Boot
    runtime_options.SetBootPandaFiles({});
    runtime_options.SetLoadRuntimes({"ecmascript"});

    // Dfx
    base_options::Options base_options("");
    base_options.SetLogLevel(option.GetLogLevel());
    arg_list_t log_components;
    log_components.emplace_back("all");
    base_options.SetLogComponents(log_components);
    Logger::Initialize(base_options);

    if (option.GetLogBufPrint() != nullptr) {
        runtime_options.SetMobileLog(reinterpret_cast<void *>(option.GetLogBufPrint()));
    }

    runtime_options.SetEnableArkTools(option.GetEnableArkTools());
    static EcmaLanguageContext lc_ecma;
    if (!Runtime::Create(runtime_options, {&lc_ecma})) {
        std::cerr << "Error: cannot create runtime" << std::endl;
        return false;
    }
    return true;
}

bool JSNApi::DestroyRuntime()
{
    return Runtime::Destroy();
}

EcmaVM *JSNApi::CreateJSVM(const RuntimeOption &option)
{
    auto runtime = Runtime::GetCurrent();
    if (runtime == nullptr) {
        // Only Ark js app
        if (!CreateRuntime(option)) {
            return nullptr;
        }
        runtime = Runtime::GetCurrent();
        return EcmaVM::Cast(runtime->GetPandaVM());
    }
    JSRuntimeOptions runtime_options;
    runtime_options.SetArkProperties(option.GetArkProperties());
    // GC
    runtime_options.SetGcType("g1-gc");
    runtime_options.SetGcTriggerType("no-gc-for-start-up");  // A non-production gc strategy. Prohibit stw-gc 10 times.

    return EcmaVM::Cast(EcmaVM::Create(runtime_options));
}

void JSNApi::DestroyJSVM(EcmaVM *ecma_vm)
{
    ecma_vm->GetNotificationManager()->VmDeathEvent();
    auto runtime = Runtime::GetCurrent();
    if (runtime != nullptr) {
        PandaVM *main_vm = runtime->GetPandaVM();
        // Only Ark js app
        if (main_vm == ecma_vm) {
            DestroyRuntime();
        } else {
            EcmaVM::Destroy(ecma_vm);
        }
    }
}

void JSNApi::TriggerGC(const EcmaVM *vm, TriggerGcType gc_type)
{
    if (vm->GetJSThread() != nullptr && vm->IsInitialized()) {
        ScopedManagedCodeThread s(vm->GetAssociatedThread());
        switch (gc_type) {
            case TriggerGcType::SEMI_GC:
                vm->CollectGarbage();
                break;
            case TriggerGcType::OLD_GC:
                vm->CollectGarbage();
                break;
            case TriggerGcType::COMPRESS_FULL_GC:
                vm->CollectGarbage();
                break;
            default:
                break;
        }
    }
}

void JSNApi::ThrowException(const EcmaVM *vm, Local<JSValueRef> error)
{
    auto thread = vm->GetJSThread();
    thread->SetException(JSNApiHelper::ToJSTaggedValue(*error));
}

bool JSNApi::StartDebugger(const char *library_path, EcmaVM *vm, bool is_debug_mode)
{
    auto agent = JSNApiDebuggerAgent::LoadInstance(library_path, vm, is_debug_mode);
    if (!agent) {
        LOG_ECMA(ERROR) << "Could not load JSN debugger agent";
        return false;
    }

    S_DEBUGGER_AGENT = std::move(agent);

    return true;
}

bool JSNApi::StopDebugger([[maybe_unused]] const char *library_path)
{
    S_DEBUGGER_AGENT.reset();
    return true;
}

bool JSNApi::Execute(EcmaVM *vm, Local<StringRef> file_name, Local<StringRef> entry)
{
    return Execute(vm, file_name->ToString(), entry->ToString());
}

bool JSNApi::Execute(EcmaVM *vm, const std::string &file_name, const std::string &entry)
{
    std::vector<std::string> argv;
    LOG_ECMA(DEBUG) << "start to execute ark file" << file_name;
    if (!vm->ExecuteFromPf(file_name, entry, argv)) {
        LOG_ECMA(ERROR) << "Cannot execute ark file" << file_name;
        LOG_ECMA(ERROR) << "Cannot execute ark file '" << file_name << "' with entry '" << entry << "'" << std::endl;
        return false;
    }
    return true;
}

bool JSNApi::Execute(EcmaVM *vm, const uint8_t *data, int32_t size, Local<StringRef> entry)
{
    return Execute(vm, data, size, entry->ToString());
}

bool JSNApi::Execute(EcmaVM *vm, const uint8_t *data, int32_t size, const std::string &entry)
{
    std::vector<std::string> argv;
    if (!vm->ExecuteFromBuffer(data, size, entry, argv)) {
        LOG_ECMA(ERROR) << "Cannot execute panda file from memory "
                        << "' with entry '" << entry << "'" << std::endl;
        return false;
    }
    return true;
}

Local<ObjectRef> JSNApi::GetAndClearUncaughtException(const EcmaVM *vm)
{
    return JSNApiHelper::ToLocal<ObjectRef>(vm->GetEcmaUncaughtException());
}

Local<ObjectRef> JSNApi::GetUncaughtException(const EcmaVM *vm)
{
    return JSNApiHelper::ToLocal<ObjectRef>(vm->GetEcmaUncaughtException());
}

void JSNApi::EnableUserUncaughtErrorHandler(EcmaVM *vm)
{
    return vm->EnableUserUncaughtErrorHandler();
}

[[maybe_unused]] static JSThread *GetCurrentThread()
{
    auto thread = Thread::GetCurrent();
    if (thread == nullptr || thread->GetThreadType() != Thread::ThreadType::THREAD_TYPE_MANAGED) {
        LOG(FATAL, RUNTIME) << "Unexpected thread type";
    }
    return JSThread::Cast(ManagedThread::CastFromThread(thread));
}

Local<ObjectRef> JSNApi::GetGlobalObject(const EcmaVM *vm)
{
    JSHandle<GlobalEnv> global_env = vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> global(vm->GetJSThread(), global_env->GetGlobalObject());
    return JSNApiHelper::ToLocal<ObjectRef>(global);
}

void JSNApi::ExecutePendingJob(const EcmaVM *vm)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    vm->ExecutePromisePendingJob();
}

uintptr_t JSNApi::GetHandleAddr(const EcmaVM *vm, uintptr_t local_address)
{
    if (local_address == 0) {
        return 0;
    }
    JSTaggedType value = *(reinterpret_cast<JSTaggedType *>(local_address));
    return ecmascript::EcmaHandleScope::NewHandle(vm->GetJSThread(), value);
}

uintptr_t JSNApi::GetGlobalHandleAddr(const EcmaVM *vm, uintptr_t local_address)
{
    if (local_address == 0) {
        return 0;
    }
    JSTaggedType value = *(reinterpret_cast<JSTaggedType *>(local_address));
    return vm->GetJSThread()->GetEcmaGlobalStorage()->NewGlobalHandle(value);
}

uintptr_t JSNApi::SetWeak(const EcmaVM *vm, uintptr_t local_address)
{
    if (local_address == 0) {
        return 0;
    }
    return vm->GetJSThread()->GetEcmaGlobalStorage()->SetWeak(local_address);
}

bool JSNApi::IsWeak(const EcmaVM *vm, uintptr_t local_address)
{
    if (local_address == 0) {
        return false;
    }
    return vm->GetJSThread()->GetEcmaGlobalStorage()->IsWeak(local_address);
}

void JSNApi::DisposeGlobalHandleAddr(const EcmaVM *vm, uintptr_t addr)
{
    if (addr == 0) {
        return;
    }
    vm->GetJSThread()->GetEcmaGlobalStorage()->DisposeGlobalHandle(addr);
}

void *JSNApi::SerializeValue(const EcmaVM *vm, Local<JSValueRef> value, Local<JSValueRef> transfer)
{
    ecmascript::JSThread *thread = vm->GetJSThread();
    ecmascript::Serializer serializer(thread);
    JSHandle<JSTaggedValue> ark_value = JSNApiHelper::ToJSHandle(value);
    JSHandle<JSTaggedValue> ark_transfer = JSNApiHelper::ToJSHandle(transfer);
    std::unique_ptr<ecmascript::SerializationData> data;
    if (serializer.WriteValue(thread, ark_value, ark_transfer)) {
        data = serializer.Release();
    }
    return reinterpret_cast<void *>(data.release());
}

Local<JSValueRef> JSNApi::DeserializeValue(const EcmaVM *vm, void *recoder)
{
    ecmascript::JSThread *thread = vm->GetJSThread();
    std::unique_ptr<ecmascript::SerializationData> data(reinterpret_cast<ecmascript::SerializationData *>(recoder));
    ecmascript::Deserializer deserializer(thread, data.release());
    JSHandle<JSTaggedValue> result = deserializer.ReadValue();
    return Local<JSValueRef>(JSNApiHelper::ToLocal<ObjectRef>(result));
}

void JSNApi::DeleteSerializationData(void *data)
{
    auto *value = reinterpret_cast<ecmascript::SerializationData *>(data);
    delete value;
}

void HostPromiseRejectionTracker(const EcmaVM *vm, const JSHandle<JSPromise> promise,
                                 const JSHandle<JSTaggedValue> reason,
                                 const ecmascript::PromiseRejectionEvent operation, void *data)
{
    ecmascript::PromiseRejectCallback promise_reject_callback = vm->GetPromiseRejectCallback();
    if (promise_reject_callback != nullptr) {
        Local<JSValueRef> promise_val = JSNApiHelper::ToLocal<JSValueRef>(JSHandle<JSTaggedValue>::Cast(promise));
        PromiseRejectInfo promise_reject_info(promise_val, JSNApiHelper::ToLocal<JSValueRef>(reason),
                                              static_cast<PromiseRejectInfo::PromiseRejectionEvent>(operation), data);
        promise_reject_callback(reinterpret_cast<void *>(&promise_reject_info));
    }
}

void JSNApi::SetHostPromiseRejectionTracker(EcmaVM *vm, void *cb, void *data)
{
    vm->SetHostPromiseRejectionTracker(HostPromiseRejectionTracker);
    vm->SetPromiseRejectCallback(reinterpret_cast<ecmascript::PromiseRejectCallback>(cb));
    vm->SetData(data);
}

void JSNApi::SetHostEnqueueJob(const EcmaVM *vm, Local<JSValueRef> cb)
{
    JSHandle<JSFunction> fun = JSHandle<JSFunction>::Cast(JSNApiHelper::ToJSHandle(cb));
    JSHandle<TaggedArray> array = vm->GetFactory()->EmptyArray();
    JSHandle<MicroJobQueue> job = vm->GetMicroJobQueue();
    MicroJobQueue::EnqueueJob(vm->GetJSThread(), job, QueueType::QUEUE_PROMISE, fun, array);
}

PromiseRejectInfo::PromiseRejectInfo(Local<JSValueRef> promise, Local<JSValueRef> reason,
                                     PromiseRejectInfo::PromiseRejectionEvent operation, void *data)
    : promise_(promise), reason_(reason), operation_(operation), data_(data)
{
}

Local<JSValueRef> PromiseRejectInfo::GetPromise() const
{
    return promise_;
}

Local<JSValueRef> PromiseRejectInfo::GetReason() const
{
    return reason_;
}

PromiseRejectInfo::PromiseRejectionEvent PromiseRejectInfo::GetOperation() const
{
    return operation_;
}

void *PromiseRejectInfo::GetData() const
{
    return data_;
}

bool JSNApi::ExecuteModuleFromBuffer(EcmaVM *vm, const void *data, int32_t size, const std::string &file)
{
    auto module_manager = vm->GetModuleManager();
    module_manager->SetCurrentExportModuleName(file);
    // Update Current Module
    std::vector<std::string> argv;
    if (!vm->ExecuteFromBuffer(data, size, ENTRY_POINTER, argv)) {
        std::cerr << "Cannot execute panda file from memory" << std::endl;
        module_manager->RestoreCurrentExportModuleName();
        return false;
    }

    // Restore Current Module
    module_manager->RestoreCurrentExportModuleName();
    return true;
}

Local<ObjectRef> JSNApi::GetExportObject(EcmaVM *vm, const std::string &file, const std::string &item_name)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    auto module_manager = vm->GetModuleManager();
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<JSTaggedValue> module_name(factory->NewFromStdStringUnCheck(file, true));
    JSHandle<JSTaggedValue> module_obj = module_manager->GetModule(vm->GetJSThread(), module_name);
    JSHandle<JSTaggedValue> item_string(factory->NewFromStdString(item_name));
    JSHandle<JSTaggedValue> export_obj = module_manager->GetModuleItem(vm->GetJSThread(), module_obj, item_string);
    return JSNApiHelper::ToLocal<ObjectRef>(export_obj);
}

// ----------------------------------- HandleScope -------------------------------------
LocalScope::LocalScope(const EcmaVM *vm) : thread_(vm->GetJSThread())
{
    auto thread = reinterpret_cast<JSThread *>(thread_);
    prev_next_ = thread->GetHandleScopeStorageNext();
    prev_end_ = thread->GetHandleScopeStorageEnd();
    prev_handle_storage_index_ = thread->GetCurrentHandleStorageIndex();
    thread->HandleScopeCountAdd();
}

LocalScope::LocalScope(const EcmaVM *vm, JSTaggedType value) : thread_(vm->GetJSThread())
{
    auto thread = reinterpret_cast<JSThread *>(thread_);
    ecmascript::EcmaHandleScope::NewHandle(thread, value);
    prev_next_ = thread->GetHandleScopeStorageNext();
    prev_end_ = thread->GetHandleScopeStorageEnd();
    prev_handle_storage_index_ = thread->GetCurrentHandleStorageIndex();
    thread->HandleScopeCountAdd();
}

LocalScope::~LocalScope()
{
    auto thread = reinterpret_cast<JSThread *>(thread_);
    thread->HandleScopeCountDec();
    thread->SetHandleScopeStorageNext(static_cast<JSTaggedType *>(prev_next_));
    if (thread->GetHandleScopeStorageEnd() != prev_end_) {
        thread->SetHandleScopeStorageEnd(static_cast<JSTaggedType *>(prev_end_));
        thread->ShrinkHandleStorage(prev_handle_storage_index_);
    }
}

// ----------------------------------- EscapeLocalScope ------------------------------
EscapeLocalScope::EscapeLocalScope(const EcmaVM *vm) : LocalScope(vm, 0U)
{
    auto thread = vm->GetJSThread();
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    escape_handle_ = ToUintPtr(thread->GetHandleScopeStorageNext() - 1);
}

// ----------------------------------- NumberRef ---------------------------------------
Local<NumberRef> NumberRef::New(const EcmaVM *vm, double input)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> number(thread, JSTaggedValue(input));
    return JSNApiHelper::ToLocal<NumberRef>(number);
}

double NumberRef::Value()
{
    return JSTaggedNumber(JSNApiHelper::ToJSTaggedValue(this)).GetNumber();
}

// ----------------------------------- BooleanRef ---------------------------------------
Local<BooleanRef> BooleanRef::New(const EcmaVM *vm, bool input)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> boolean(thread, JSTaggedValue(input));
    return JSNApiHelper::ToLocal<BooleanRef>(boolean);
}

bool BooleanRef::Value()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsTrue();
}

// ----------------------------------- IntegerRef ---------------------------------------
Local<IntegerRef> IntegerRef::New(const EcmaVM *vm, int input)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> integer(thread, JSTaggedValue(input));
    return JSNApiHelper::ToLocal<IntegerRef>(integer);
}

Local<IntegerRef> IntegerRef::NewFromUnsigned(const EcmaVM *vm, unsigned int input)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> integer(thread, JSTaggedValue(input));
    return JSNApiHelper::ToLocal<IntegerRef>(integer);
}

int IntegerRef::Value()
{
    return JSNApiHelper::ToJSTaggedValue(this).GetInt();
}

// ----------------------------------- StringRef ----------------------------------------
Local<StringRef> StringRef::NewFromUtf8(const EcmaVM *vm, const char *utf8, int length)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    ObjectFactory *factory = vm->GetFactory();
    if (length < 0) {
        JSHandle<JSTaggedValue> current(factory->NewFromString(utf8));
        return JSNApiHelper::ToLocal<StringRef>(current);
    }
    JSHandle<JSTaggedValue> current(factory->NewFromUtf8(reinterpret_cast<const uint8_t *>(utf8), length));
    return JSNApiHelper::ToLocal<StringRef>(current);
}

std::string StringRef::ToString()
{
    return StringHelper::ToStdString(EcmaString::Cast(JSNApiHelper::ToJSTaggedValue(this).GetTaggedObject()));
}

int32_t StringRef::Length()
{
    return EcmaString::Cast(JSNApiHelper::ToJSTaggedValue(this).GetTaggedObject())->GetLength();
}

int32_t StringRef::Utf8Length()
{
    return EcmaString::Cast(JSNApiHelper::ToJSTaggedValue(this).GetTaggedObject())->GetUtf8Length();
}

int StringRef::WriteUtf8(char *buffer, int length)
{
    return EcmaString::Cast(JSNApiHelper::ToJSTaggedValue(this).GetTaggedObject())
        ->CopyDataUtf8(reinterpret_cast<uint8_t *>(buffer), length);
}

// ----------------------------------- SymbolRef -----------------------------------------
Local<SymbolRef> SymbolRef::New(const EcmaVM *vm, Local<StringRef> description)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<JSSymbol> symbol = factory->NewJSSymbol();
    JSTaggedValue desc = JSNApiHelper::ToJSTaggedValue(*description);
    symbol->SetDescription(vm->GetJSThread(), desc);
    return JSNApiHelper::ToLocal<SymbolRef>(JSHandle<JSTaggedValue>(symbol));
}

Local<StringRef> SymbolRef::GetDescription(const EcmaVM *vm)
{
    JSTaggedValue description = JSSymbol::Cast(JSNApiHelper::ToJSTaggedValue(this).GetTaggedObject())->GetDescription();
    if (!description.IsString()) {
        auto constants = vm->GetJSThread()->GlobalConstants();
        return JSNApiHelper::ToLocal<StringRef>(constants->GetHandledEmptyString());
    }
    JSHandle<JSTaggedValue> description_handle(vm->GetJSThread(), description);
    return JSNApiHelper::ToLocal<StringRef>(description_handle);
}

// -------------------------------- NativePointerRef ------------------------------------
Local<NativePointerRef> NativePointerRef::New(const EcmaVM *vm, void *native_pointer)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<JSNativePointer> obj = factory->NewJSNativePointer(native_pointer);
    return JSNApiHelper::ToLocal<NativePointerRef>(JSHandle<JSTaggedValue>(obj));
}

Local<NativePointerRef> NativePointerRef::New(const EcmaVM *vm, void *native_pointer, NativePointerCallback call_back,
                                              void *data)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<JSNativePointer> obj = factory->NewJSNativePointer(native_pointer, call_back, data);
    return JSNApiHelper::ToLocal<NativePointerRef>(JSHandle<JSTaggedValue>(obj));
}

void *NativePointerRef::Value()
{
    JSHandle<JSTaggedValue> native_pointer = JSNApiHelper::ToJSHandle(this);
    return JSHandle<JSNativePointer>(native_pointer)->GetExternalPointer();
}

// ----------------------------------- ObjectRef ----------------------------------------
Local<ObjectRef> ObjectRef::New(const EcmaVM *vm)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<GlobalEnv> global_env = vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> constructor = global_env->GetObjectFunction();
    JSHandle<JSTaggedValue> object(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), constructor));
    RETURN_VALUE_IF_ABRUPT(vm->GetJSThread(), Local<ObjectRef>(JSValueRef::Exception(vm)));
    return JSNApiHelper::ToLocal<ObjectRef>(object);
}

bool ObjectRef::Set(const EcmaVM *vm, Local<JSValueRef> key, Local<JSValueRef> value)
{
    ScopedManagedCodeThread s(vm->GetAssociatedThread());
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> key_value = JSNApiHelper::ToJSHandle(key);
    JSHandle<JSTaggedValue> value_value = JSNApiHelper::ToJSHandle(value);
    bool result = JSTaggedValue::SetProperty(vm->GetJSThread(), obj, key_value, value_value);
    RETURN_VALUE_IF_ABRUPT(vm->GetJSThread(), false);
    return result;
}

bool ObjectRef::Set(const EcmaVM *vm, uint32_t key, Local<JSValueRef> value)
{
    Local<JSValueRef> key_value = Local<JSValueRef>(NumberRef::New(vm, key));
    return Set(vm, key_value, value);
}

bool ObjectRef::SetAccessorProperty(const EcmaVM *vm, Local<JSValueRef> key, Local<FunctionRef> getter,
                                    Local<FunctionRef> setter, PropertyAttribute attribute)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> getter_value = JSNApiHelper::ToJSHandle(getter);
    JSHandle<JSTaggedValue> setter_value = JSNApiHelper::ToJSHandle(setter);
    PropertyDescriptor desc(thread, attribute.IsWritable(), attribute.IsEnumerable(), attribute.IsConfigurable());
    desc.SetValue(JSNApiHelper::ToJSHandle(attribute.GetValue(vm)));
    desc.SetSetter(setter_value);
    desc.SetGetter(getter_value);
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> key_value = JSNApiHelper::ToJSHandle(key);
    bool result = JSTaggedValue::DefineOwnProperty(thread, obj, key_value, desc);
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return result;
}

Local<JSValueRef> ObjectRef::Get(const EcmaVM *vm, Local<JSValueRef> key)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> key_value = JSNApiHelper::ToJSHandle(key);
    OperationResult ret = JSTaggedValue::GetProperty(thread, obj, key_value);
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    if (!ret.GetPropertyMetaData().IsFound()) {
        return JSValueRef::Undefined(vm);
    }
    return JSNApiHelper::ToLocal<JSValueRef>(ret.GetValue());
}

Local<JSValueRef> ObjectRef::Get(const EcmaVM *vm, int32_t key)
{
    Local<JSValueRef> key_value = IntegerRef::New(vm, key);
    return Get(vm, key_value);
}

bool ObjectRef::GetOwnProperty(const EcmaVM *vm, Local<JSValueRef> key, PropertyAttribute &property)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> key_value = JSNApiHelper::ToJSHandle(key);
    PropertyDescriptor desc(thread);
    bool ret = JSObject::GetOwnProperty(thread, JSHandle<JSObject>(obj), key_value, desc);
    if (!ret) {
        return false;
    }
    property.SetValue(JSNApiHelper::ToLocal<JSValueRef>(desc.GetValue()));
    if (desc.HasGetter()) {
        property.SetGetter(JSNApiHelper::ToLocal<JSValueRef>(desc.GetGetter()));
    }
    if (desc.HasSetter()) {
        property.SetSetter(JSNApiHelper::ToLocal<JSValueRef>(desc.GetSetter()));
    }
    if (desc.HasWritable()) {
        property.SetWritable(desc.IsWritable());
    }
    if (desc.HasEnumerable()) {
        property.SetEnumerable(desc.IsEnumerable());
    }
    if (desc.HasConfigurable()) {
        property.SetConfigurable(desc.IsConfigurable());
    }

    return true;
}

Local<ArrayRef> ObjectRef::GetOwnPropertyNames(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> obj(JSNApiHelper::ToJSHandle(this));
    JSHandle<TaggedArray> array(JSTaggedValue::GetOwnPropertyKeys(thread, obj));
    JSHandle<JSTaggedValue> js_array(JSArray::CreateArrayFromList(thread, array));
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<ArrayRef>(js_array);
}

Local<ArrayRef> ObjectRef::GetOwnEnumerablePropertyNames(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSObject> obj(JSNApiHelper::ToJSHandle(this));
    JSHandle<TaggedArray> array(JSObject::EnumerableOwnNames(thread, obj));
    JSHandle<JSTaggedValue> js_array(JSArray::CreateArrayFromList(thread, array));
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<ArrayRef>(js_array);
}

Local<JSValueRef> ObjectRef::GetPrototype(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSObject> object(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> prototype(thread, object->GetPrototype(thread));
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<JSValueRef>(prototype);
}

bool ObjectRef::DefineProperty(const EcmaVM *vm, Local<JSValueRef> key, PropertyAttribute attribute)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> object(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> key_value(JSNApiHelper::ToJSHandle(key));
    PropertyDescriptor desc(thread, attribute.IsWritable(), attribute.IsEnumerable(), attribute.IsConfigurable());
    desc.SetValue(JSNApiHelper::ToJSHandle(attribute.GetValue(vm)));
    bool result = object->DefinePropertyOrThrow(thread, object, key_value, desc);
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return result;
}

bool ObjectRef::Has(const EcmaVM *vm, Local<JSValueRef> key)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> object(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> key_value(JSNApiHelper::ToJSHandle(key));
    bool result = object->HasProperty(thread, object, key_value);
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return result;
}

bool ObjectRef::Has(const EcmaVM *vm, uint32_t key)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> object(JSNApiHelper::ToJSHandle(this));
    bool result = object->HasProperty(thread, object, key);
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return result;
}

bool ObjectRef::Delete(const EcmaVM *vm, Local<JSValueRef> key)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> object(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> key_value(JSNApiHelper::ToJSHandle(key));
    bool result = object->DeleteProperty(thread, object, key_value);
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return result;
}

bool ObjectRef::Delete(const EcmaVM *vm, uint32_t key)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> object(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> key_handle(thread, JSTaggedValue(key));
    bool result = object->DeleteProperty(thread, object, key_handle);
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return result;
}

void ObjectRef::SetNativePointerFieldCount(int32_t count)
{
    JSHandle<JSObject> object(JSNApiHelper::ToJSHandle(this));
    object->SetNativePointerFieldCount(count);
}

int32_t ObjectRef::GetNativePointerFieldCount()
{
    JSHandle<JSObject> object(JSNApiHelper::ToJSHandle(this));
    return object->GetNativePointerFieldCount();
}

void *ObjectRef::GetNativePointerField(int32_t index)
{
    JSHandle<JSObject> object(JSNApiHelper::ToJSHandle(this));
    return object->GetNativePointerField(index);
}

void ObjectRef::SetNativePointerField(int32_t index, void *native_pointer, NativePointerCallback call_back, void *data)
{
    JSHandle<JSObject> object(JSNApiHelper::ToJSHandle(this));
    object->SetNativePointerField(index, native_pointer, call_back, data);
}

// ----------------------------------- FunctionRef --------------------------------------
Local<FunctionRef> FunctionRef::New(EcmaVM *vm, FunctionCallback native_func, void *data)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<GlobalEnv> env = vm->GetGlobalEnv();
    JSHandle<JSFunction> current(factory->NewJSFunction(env, reinterpret_cast<void *>(Callback::RegisterCallback)));
    JSHandle<JSNativePointer> func_callback = factory->NewJSNativePointer(reinterpret_cast<void *>(native_func));
    JSHandle<JSNativePointer> vm_caddress = factory->NewJSNativePointer(vm);
    JSHandle<JSNativePointer> data_caddress = factory->NewJSNativePointer(data);
    JSHandle<JSFunctionExtraInfo> extra_info(factory->NewFunctionExtraInfo(func_callback, vm_caddress, data_caddress));
    current->SetFunctionExtraInfo(thread, extra_info.GetTaggedValue());
    return JSNApiHelper::ToLocal<FunctionRef>(JSHandle<JSTaggedValue>(current));
}

Local<FunctionRef> FunctionRef::New(EcmaVM *vm, FunctionCallback native_func, Deleter deleter, void *data)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<GlobalEnv> env = vm->GetGlobalEnv();
    JSHandle<JSFunction> current(factory->NewJSFunction(env, reinterpret_cast<void *>(Callback::RegisterCallback)));
    JSHandle<JSNativePointer> func_callback = factory->NewJSNativePointer(reinterpret_cast<void *>(native_func));
    JSHandle<JSNativePointer> vm_caddress = factory->NewJSNativePointer(vm);
    JSHandle<JSNativePointer> data_caddress = factory->NewJSNativePointer(data, deleter, nullptr);
    vm->PushToArrayDataList(*data_caddress);
    JSHandle<JSFunctionExtraInfo> extra_info(factory->NewFunctionExtraInfo(func_callback, vm_caddress, data_caddress));
    current->SetFunctionExtraInfo(thread, extra_info.GetTaggedValue());
    return JSNApiHelper::ToLocal<FunctionRef>(JSHandle<JSTaggedValue>(current));
}

Local<FunctionRef> FunctionRef::NewWithProperty(EcmaVM *vm, FunctionCallback native_func, void *data)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<GlobalEnv> env = vm->GetGlobalEnv();
    JSHandle<JSFunction> current =
        factory->NewJSFunction(env, reinterpret_cast<void *>(Callback::RegisterCallbackWithProperty));
    JSHandle<JSNativePointer> func_callback = factory->NewJSNativePointer(reinterpret_cast<void *>(native_func));
    JSHandle<JSNativePointer> vm_caddress = factory->NewJSNativePointer(vm);
    JSHandle<JSNativePointer> data_caddress = factory->NewJSNativePointer(data);
    JSHandle<JSFunctionExtraInfo> extra_info(factory->NewFunctionExtraInfo(func_callback, vm_caddress, data_caddress));
    current->SetFunctionExtraInfo(thread, extra_info.GetTaggedValue());
    return JSNApiHelper::ToLocal<FunctionRef>(JSHandle<JSTaggedValue>(current));
}

Local<FunctionRef> FunctionRef::NewClassFunction(EcmaVM *vm, FunctionCallbackWithNewTarget native_func, Deleter deleter,
                                                 void *data)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<GlobalEnv> env = vm->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithoutName());
    JSMethod *method =
        vm->GetMethodForNativeFunction(reinterpret_cast<void *>(Callback::RegisterCallbackWithNewTarget));
    JSHandle<JSFunction> current =
        factory->NewJSFunctionByDynClass(method, dynclass, ecmascript::FunctionKind::CLASS_CONSTRUCTOR);

    auto global_const = thread->GlobalConstants();
    JSHandle<JSTaggedValue> accessor = global_const->GetHandledFunctionPrototypeAccessor();
    current->SetPropertyInlinedProps(thread, JSFunction::CLASS_PROTOTYPE_INLINE_PROPERTY_INDEX,
                                     accessor.GetTaggedValue());

    JSHandle<JSNativePointer> func_callback = factory->NewJSNativePointer(reinterpret_cast<void *>(native_func));
    JSHandle<JSNativePointer> vm_caddress = factory->NewJSNativePointer(vm);
    JSHandle<JSNativePointer> data_caddress(thread, JSTaggedValue::Undefined());
    if (deleter == nullptr) {
        data_caddress = factory->NewJSNativePointer(data);
    } else {
        data_caddress = factory->NewJSNativePointer(data, deleter, nullptr);
        vm->PushToArrayDataList(*data_caddress);
    }
    JSHandle<JSFunctionExtraInfo> extra_info(factory->NewFunctionExtraInfo(func_callback, vm_caddress, data_caddress));
    current->SetFunctionExtraInfo(thread, extra_info.GetTaggedValue());

    JSHandle<JSObject> cls_prototype = JSFunction::NewJSFunctionPrototype(thread, factory, current);
    cls_prototype.GetTaggedValue().GetTaggedObject()->GetClass()->SetClassPrototype(true);
    JSHandle<JSTaggedValue>::Cast(current)->GetTaggedObject()->GetClass()->SetClassConstructor(true);
    current->SetClassConstructor(true);
    JSHandle<JSTaggedValue> parent = env->GetFunctionPrototype();
    JSObject::SetPrototype(thread, JSHandle<JSObject>::Cast(current), parent);
    current->SetHomeObject(thread, cls_prototype);
    return JSNApiHelper::ToLocal<FunctionRef>(JSHandle<JSTaggedValue>(current));
}

Local<JSValueRef> FunctionRef::Call(const EcmaVM *vm, Local<JSValueRef> this_obj,
                                    const Local<JSValueRef> argv[],  // NOLINT(modernize-avoid-c-arrays)
                                    int32_t length)
{
    JSThread *thread = vm->GetJSThread();
    if (!IsFunction()) {
        return JSValueRef::Undefined(vm);
    }
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> func = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> this_value;
    if (this_obj.IsEmpty()) {
        this_value = JSNApiHelper::ToJSHandle(this_obj);
    } else {
        this_value = JSHandle<JSTaggedValue>(thread, thread->GlobalConstants()->GetUndefined());
    }

    auto info = NewRuntimeCallInfo(thread, func, this_value, JSTaggedValue::Undefined(), length);
    for (int32_t i = 0; i < length; ++i) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        info->SetCallArg(i, JSNApiHelper::ToJSHandle(argv[i]).GetTaggedValue());
    }
    JSTaggedValue result = JSFunction::Call(info.Get());

    RETURN_VALUE_IF_ABRUPT_NOT_CLEAR_EXCEPTION(thread, JSValueRef::Exception(vm));
    JSHandle<JSTaggedValue> result_value(thread, result);

    vm->ExecutePromisePendingJob();
    RETURN_VALUE_IF_ABRUPT_NOT_CLEAR_EXCEPTION(thread, JSValueRef::Exception(vm));

    return JSNApiHelper::ToLocal<JSValueRef>(result_value);
}

Local<JSValueRef> FunctionRef::Constructor(const EcmaVM *vm,
                                           const Local<JSValueRef> argv[],  // NOLINT(modernize-avoid-c-arrays)
                                           int32_t length)
{
    JSThread *thread = vm->GetJSThread();
    if (!IsFunction()) {
        return JSValueRef::Undefined(vm);
    }
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> func = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> new_target = func;
    auto info = NewRuntimeCallInfo(thread, func, JSTaggedValue::Undefined(), new_target, length);
    for (int32_t i = 0; i < length; ++i) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        info->SetCallArg(i, JSNApiHelper::ToJSHandle(argv[i]).GetTaggedValue());
    }
    JSTaggedValue result = JSFunction::Construct(info.Get());
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    JSHandle<JSTaggedValue> result_value(vm->GetJSThread(), result);
    return JSNApiHelper::ToLocal<JSValueRef>(result_value);
}

Local<JSValueRef> FunctionRef::GetFunctionPrototype(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> func = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> prototype(thread, JSHandle<JSFunction>(func)->GetFunctionPrototype());
    return JSNApiHelper::ToLocal<JSValueRef>(prototype);
}

bool FunctionRef::Inherit(const EcmaVM *vm, Local<FunctionRef> parent)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> parent_value = JSNApiHelper::ToJSHandle(parent);
    JSHandle<JSObject> parent_handle = JSHandle<JSObject>::Cast(parent_value);
    JSHandle<JSObject> this_handle = JSHandle<JSObject>::Cast(JSNApiHelper::ToJSHandle(this));
    // Set this.__proto__ to parent
    bool res = JSObject::SetPrototype(thread, this_handle, parent_value);
    if (!res) {
        return false;
    }
    // Set this.Prototype.__proto__ to parent.Prototype
    JSHandle<JSTaggedValue> parent_proto_type(thread, JSFunction::PrototypeGetter(thread, parent_handle));
    JSHandle<JSTaggedValue> this_proto_type(thread, JSFunction::PrototypeGetter(thread, this_handle));
    return JSObject::SetPrototype(thread, JSHandle<JSObject>::Cast(this_proto_type), parent_proto_type);
}

void FunctionRef::SetName(const EcmaVM *vm, Local<StringRef> name)
{
    JSThread *thread = vm->GetJSThread();
    JSFunction *func = JSFunction::Cast(JSNApiHelper::ToJSTaggedValue(this).GetTaggedObject());
    JSTaggedValue key = JSNApiHelper::ToJSTaggedValue(*name);
    JSFunction::SetFunctionNameNoPrefix(thread, func, key);
}

Local<StringRef> FunctionRef::GetName(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSFunctionBase> func = JSHandle<JSFunctionBase>(thread, JSNApiHelper::ToJSTaggedValue(this));
    JSHandle<JSTaggedValue> name = JSFunctionBase::GetFunctionName(thread, func);
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<StringRef>(name);
}

bool FunctionRef::IsNative(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSFunctionBase> func = JSHandle<JSFunctionBase>(thread, JSNApiHelper::ToJSTaggedValue(this));
    JSMethod *method = func->GetMethod();
    return method->IsNative();
}

// ----------------------------------- ArrayRef ----------------------------------------
Local<ArrayRef> ArrayRef::New(const EcmaVM *vm, int32_t length)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSTaggedNumber array_len(length);
    JSHandle<JSTaggedValue> array = JSArray::ArrayCreate(thread, array_len);
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<ArrayRef>(array);
}

int32_t ArrayRef::Length([[maybe_unused]] const EcmaVM *vm)
{
    return JSArray::Cast(JSNApiHelper::ToJSTaggedValue(this).GetTaggedObject())->GetArrayLength();
}

Local<JSValueRef> ArrayRef::GetValueAt(const EcmaVM *vm, Local<JSValueRef> obj, uint32_t index)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> object = JSNApiHelper::ToJSHandle(obj);
    JSHandle<JSTaggedValue> result = JSArray::FastGetPropertyByValue(thread, object, index);
    return JSNApiHelper::ToLocal<JSValueRef>(result);
}

bool ArrayRef::SetValueAt(const EcmaVM *vm, Local<JSValueRef> obj, uint32_t index, Local<JSValueRef> value)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> object_handle = JSNApiHelper::ToJSHandle(obj);
    JSHandle<JSTaggedValue> value_handle = JSNApiHelper::ToJSHandle(value);
    return JSArray::FastSetPropertyByValue(thread, object_handle, index, value_handle);
}
// ---------------------------------- Promise --------------------------------------
Local<PromiseCapabilityRef> PromiseCapabilityRef::New(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<GlobalEnv> global_env = vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> constructor(global_env->GetPromiseFunction());
    JSHandle<JSTaggedValue> capability(JSPromise::NewPromiseCapability(thread, constructor));
    return JSNApiHelper::ToLocal<PromiseCapabilityRef>(capability);
}

Local<PromiseRef> PromiseCapabilityRef::GetPromise(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<PromiseCapability> capacity(JSNApiHelper::ToJSHandle(this));
    return JSNApiHelper::ToLocal<PromiseRef>(JSHandle<JSTaggedValue>(thread, capacity->GetPromise()));
}

bool PromiseCapabilityRef::Resolve(const EcmaVM *vm, Local<JSValueRef> value)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    const GlobalEnvConstants *constants = thread->GlobalConstants();

    JSHandle<JSTaggedValue> arg = JSNApiHelper::ToJSHandle(value);
    JSHandle<PromiseCapability> capacity(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> resolve(thread, capacity->GetResolve());
    JSHandle<JSTaggedValue> undefined(thread, constants->GetUndefined());

    auto info = NewRuntimeCallInfo(thread, resolve, undefined, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(arg);
    JSFunction::Call(info.Get());
    RETURN_VALUE_IF_ABRUPT(thread, false);

    vm->ExecutePromisePendingJob();
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return true;
}

bool PromiseCapabilityRef::Reject(const EcmaVM *vm, Local<JSValueRef> reason)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    const GlobalEnvConstants *constants = thread->GlobalConstants();

    JSHandle<JSTaggedValue> arg = JSNApiHelper::ToJSHandle(reason);
    JSHandle<PromiseCapability> capacity(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> reject(thread, capacity->GetReject());
    JSHandle<JSTaggedValue> undefined(thread, constants->GetUndefined());

    auto info = NewRuntimeCallInfo(thread, reject, undefined, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(arg);
    JSFunction::Call(info.Get());
    RETURN_VALUE_IF_ABRUPT(thread, false);

    vm->ExecutePromisePendingJob();
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return true;
}

Local<PromiseRef> PromiseRef::Catch(const EcmaVM *vm, Local<FunctionRef> handler)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    const GlobalEnvConstants *constants = thread->GlobalConstants();

    JSHandle<JSTaggedValue> promise = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> catch_key(thread, constants->GetPromiseCatchString());
    JSHandle<JSTaggedValue> reject = JSNApiHelper::ToJSHandle(handler);
    auto info =
        ecmascript::NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), promise, JSTaggedValue::Undefined(), 1);
    info->SetCallArgs(reject);
    JSTaggedValue result = JSFunction::Invoke(info.Get(), catch_key);

    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<PromiseRef>(JSHandle<JSTaggedValue>(thread, result));
}

Local<PromiseRef> PromiseRef::Then(const EcmaVM *vm, Local<FunctionRef> handler)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    const GlobalEnvConstants *constants = thread->GlobalConstants();

    JSHandle<JSTaggedValue> promise = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> then_key(thread, constants->GetPromiseThenString());
    JSHandle<JSTaggedValue> resolver = JSNApiHelper::ToJSHandle(handler);
    auto info =
        ecmascript::NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), promise, JSTaggedValue::Undefined(), 2);
    info->SetCallArgs(resolver, JSTaggedValue::Undefined());
    JSTaggedValue result = JSFunction::Invoke(info.Get(), then_key);

    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<PromiseRef>(JSHandle<JSTaggedValue>(thread, result));
}

Local<PromiseRef> PromiseRef::Then(const EcmaVM *vm, Local<FunctionRef> on_fulfilled, Local<FunctionRef> on_rejected)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    const GlobalEnvConstants *constants = thread->GlobalConstants();

    JSHandle<JSTaggedValue> promise = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> then_key(thread, constants->GetPromiseThenString());
    JSHandle<JSTaggedValue> resolve = JSNApiHelper::ToJSHandle(on_fulfilled);
    JSHandle<JSTaggedValue> reject = JSNApiHelper::ToJSHandle(on_rejected);
    auto info =
        ecmascript::NewRuntimeCallInfo(thread, JSTaggedValue::Undefined(), promise, JSTaggedValue::Undefined(), 2);
    info->SetCallArgs(resolve, reject);
    JSTaggedValue result = JSFunction::Invoke(info.Get(), then_key);

    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<PromiseRef>(JSHandle<JSTaggedValue>(thread, result));
}
// ---------------------------------- Promise -------------------------------------

// ---------------------------------- Buffer -----------------------------------
Local<ArrayBufferRef> ArrayBufferRef::New(const EcmaVM *vm, int32_t length)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    ObjectFactory *factory = vm->GetFactory();

    JSHandle<JSArrayBuffer> array_buffer = factory->NewJSArrayBuffer(length);
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<ArrayBufferRef>(JSHandle<JSTaggedValue>(array_buffer));
}

Local<ArrayBufferRef> ArrayBufferRef::New(const EcmaVM *vm, void *buffer, int32_t length, const Deleter &deleter,
                                          void *data)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    ObjectFactory *factory = vm->GetFactory();

    JSHandle<JSArrayBuffer> array_buffer =
        factory->NewJSArrayBuffer(buffer, length, reinterpret_cast<ecmascript::DeleteEntryPoint>(deleter), data);
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<ArrayBufferRef>(JSHandle<JSTaggedValue>(array_buffer));
}

int32_t ArrayBufferRef::ByteLength(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSArrayBuffer> array_buffer(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> length(thread, array_buffer->GetArrayBufferByteLength());
    if (!length->IsNumber()) {
        return 0;
    }
    return length->GetNumber();
}

void *ArrayBufferRef::GetBuffer()
{
    JSHandle<JSArrayBuffer> array_buffer(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue buffer_data = array_buffer->GetArrayBufferData();
    if (!buffer_data.IsJSNativePointer()) {
        return nullptr;
    }
    return JSNativePointer::Cast(buffer_data.GetTaggedObject())->GetExternalPointer();
}
// ---------------------------------- Buffer -----------------------------------

// ---------------------------------- DataView -----------------------------------
Local<DataViewRef> DataViewRef::New(const EcmaVM *vm, Local<ArrayBufferRef> array_buffer, int32_t byte_offset,
                                    int32_t byte_length)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    ObjectFactory *factory = vm->GetFactory();

    JSHandle<JSArrayBuffer> buffer(JSNApiHelper::ToJSHandle(array_buffer));
    JSHandle<JSDataView> data_view = factory->NewJSDataView(buffer, byte_offset, byte_length);
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<DataViewRef>(JSHandle<JSTaggedValue>(data_view));
}

int32_t DataViewRef::ByteLength()
{
    JSHandle<JSDataView> data_view(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue length = data_view->GetByteLength();
    if (!length.IsNumber()) {
        return 0;
    }
    return length.GetNumber();
}

int32_t DataViewRef::ByteOffset()
{
    JSHandle<JSDataView> data_view(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue offset = data_view->GetByteOffset();
    if (!offset.IsNumber()) {
        return 0;
    }
    return offset.GetNumber();
}

Local<ArrayBufferRef> DataViewRef::GetArrayBuffer(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSDataView> data_view(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> array_buffer(thread, data_view->GetViewedArrayBuffer());
    return JSNApiHelper::ToLocal<ArrayBufferRef>(array_buffer);
}
// ---------------------------------- DataView -----------------------------------

// ---------------------------------- TypedArray -----------------------------------
int32_t TypedArrayRef::ByteLength([[maybe_unused]] const EcmaVM *vm)
{
    JSHandle<JSTypedArray> typed_array(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue length = typed_array->GetByteLength();
    if (!length.IsNumber()) {
        return 0;
    }
    return length.GetNumber();
}

int32_t TypedArrayRef::ByteOffset([[maybe_unused]] const EcmaVM *vm)
{
    JSHandle<JSTypedArray> typed_array(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue length = typed_array->GetByteOffset();
    if (!length.IsNumber()) {
        return 0;
    }
    return length.GetNumber();
}

int32_t TypedArrayRef::ArrayLength([[maybe_unused]] const EcmaVM *vm)
{
    JSHandle<JSTypedArray> typed_array(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue length = typed_array->GetArrayLength();
    if (!length.IsNumber()) {
        return 0;
    }
    return length.GetNumber();
}

Local<ArrayBufferRef> TypedArrayRef::GetArrayBuffer(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSObject> type_array(JSNApiHelper::ToJSHandle(this));
    JSHandle<JSTaggedValue> array_buffer(thread, JSTypedArray::Cast(*type_array)->GetViewedArrayBuffer());
    return JSNApiHelper::ToLocal<ArrayBufferRef>(array_buffer);
}

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TYPED_ARRAY_NEW(Type)                                                                            \
    Local<Type##Ref> Type##Ref::New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset, \
                                    int32_t length)                                                      \
    {                                                                                                    \
        JSThread *thread = vm->GetJSThread();                                                            \
        ScopedManagedCodeThread s(thread);                                                               \
        JSHandle<GlobalEnv> env = vm->GetGlobalEnv();                                                    \
                                                                                                         \
        JSHandle<JSTaggedValue> func = env->Get##Type##Function();                                       \
        JSHandle<JSArrayBuffer> arrayBuffer(JSNApiHelper::ToJSHandle(buffer));                           \
        uint32_t argc = 3;                                                                               \
        auto info = NewRuntimeCallInfo(thread, func, JSTaggedValue::Undefined(), func, argc);            \
        info->SetCallArgs(arrayBuffer, JSTaggedValue(byte_offset), JSTaggedValue(length));               \
        JSTaggedValue result = JSFunction::Construct(info.Get());                                        \
        RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));                                       \
        JSHandle<JSTaggedValue> resultHandle(thread, result);                                            \
        return JSNApiHelper::ToLocal<Type##Ref>(resultHandle);                                           \
    }

TYPED_ARRAY_ALL(TYPED_ARRAY_NEW)

#undef TYPED_ARRAY_NEW
// ---------------------------------- TypedArray -----------------------------------

// ---------------------------------- Error ---------------------------------------
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define EXCEPTION_ERROR_NEW(name, type)                                                     \
    Local<JSValueRef> Exception::name(const EcmaVM *vm, Local<StringRef> message)           \
    {                                                                                       \
        JSThread *thread = vm->GetJSThread();                                               \
        ScopedManagedCodeThread s(thread);                                                  \
        ObjectFactory *factory = vm->GetFactory();                                          \
                                                                                            \
        JSHandle<EcmaString> messageValue(JSNApiHelper::ToJSHandle(message));               \
        JSHandle<JSTaggedValue> result(factory->NewJSError(ErrorType::type, messageValue)); \
        RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));                          \
        return JSNApiHelper::ToLocal<JSValueRef>(result);                                   \
    }

EXCEPTION_ERROR_ALL(EXCEPTION_ERROR_NEW)

#undef EXCEPTION_ERROR_NEW
// ---------------------------------- Error ---------------------------------------

// ---------------------------------- JSON ------------------------------------------
Local<JSValueRef> JSON::Parse(const EcmaVM *vm, Local<StringRef> string)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    auto ecma_str = EcmaString::Cast(JSNApiHelper::ToJSTaggedValue(*string).GetTaggedObject());
    JSHandle<JSTaggedValue> result;
    if (ecma_str->IsUtf8()) {
        JsonParser<uint8_t> parser(thread);
        result = parser.ParseUtf8(EcmaString::Cast(JSNApiHelper::ToJSTaggedValue(*string).GetTaggedObject()));
    } else {
        JsonParser<uint16_t> parser(thread);
        result = parser.ParseUtf16(EcmaString::Cast(JSNApiHelper::ToJSTaggedValue(*string).GetTaggedObject()));
    }
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<JSValueRef>(result);
}

Local<JSValueRef> JSON::Stringify(const EcmaVM *vm, Local<JSValueRef> json)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    auto constants = thread->GlobalConstants();
    JsonStringifier stringifier(thread);
    JSHandle<JSTaggedValue> str = stringifier.Stringify(
        JSNApiHelper::ToJSHandle(json), constants->GetHandledUndefined(), constants->GetHandledUndefined());
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<JSValueRef>(str);
}

Local<StringRef> RegExpRef::GetOriginalSource(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSRegExp> reg_exp(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue source = reg_exp->GetOriginalSource();
    if (!source.IsString()) {
        auto constants = thread->GlobalConstants();
        return JSNApiHelper::ToLocal<StringRef>(constants->GetHandledEmptyString());
    }
    JSHandle<JSTaggedValue> source_handle(thread, source);
    return JSNApiHelper::ToLocal<StringRef>(source_handle);
}

Local<DateRef> DateRef::New(const EcmaVM *vm, double time)
{
    JSThread *thread = vm->GetJSThread();
    ObjectFactory *factory = vm->GetFactory();
    JSHandle<GlobalEnv> global_env = vm->GetGlobalEnv();
    JSHandle<JSTaggedValue> date_function = global_env->GetDateFunction();
    JSHandle<JSDate> date_object =
        JSHandle<JSDate>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(date_function), date_function));
    date_object->SetTimeValue(thread, JSTaggedValue(time));
    return JSNApiHelper::ToLocal<DateRef>(JSHandle<JSTaggedValue>(date_object));
}

Local<StringRef> DateRef::ToString(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSDate> date(JSNApiHelper::ToJSHandle(this));
    JSTaggedValue date_str = date->ToString(thread);
    if (!date_str.IsString()) {
        auto constants = thread->GlobalConstants();
        return JSNApiHelper::ToLocal<StringRef>(constants->GetHandledEmptyString());
    }
    JSHandle<JSTaggedValue> date_str_handle(thread, date_str);
    return JSNApiHelper::ToLocal<StringRef>(date_str_handle);
}

double DateRef::GetTime()
{
    JSHandle<JSDate> date(JSNApiHelper::ToJSHandle(this));
    if (!date->IsDate()) {
        LOG(ERROR, RUNTIME) << "Not a Date Object";
    }
    return date->GetTime().GetDouble();
}

int32_t MapRef::GetSize()
{
    JSHandle<JSMap> map(JSNApiHelper::ToJSHandle(this));
    return map->GetSize();
}

int32_t SetRef::GetSize()
{
    JSHandle<JSSet> set(JSNApiHelper::ToJSHandle(this));
    return set->GetSize();
}

// ----------------------------------- FunctionCallback ---------------------------------
JSTaggedValue Callback::RegisterCallback(ecmascript::EcmaRuntimeCallInfo *info)
{
    // Constructor
    JSThread *thread = info->GetThread();
    [[maybe_unused]] panda::ecmascript::EcmaHandleScope handle_scope(thread);
    JSHandle<JSTaggedValue> constructor = ecmascript::builtins_common::GetConstructor(info);
    if (!constructor->IsJSFunction()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSFunction> function(constructor);
    JSHandle<JSTaggedValue> extra_info_value(thread, function->GetFunctionExtraInfo());
    if (!extra_info_value->IsJSFunctionExtraInfo()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSFunctionExtraInfo> extra_info(extra_info_value);
    // vm
    JSHandle<JSTaggedValue> vm_value(thread, extra_info->GetVm());
    if (!vm_value->IsHeapObject()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSNativePointer> vm_obj(vm_value);
    auto *vm = reinterpret_cast<EcmaVM *>(vm_obj->GetExternalPointer());

    // data
    JSHandle<JSTaggedValue> data(thread, extra_info->GetData());
    if (!data->IsHeapObject()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSNativePointer> data_obj(data);
    // callBack
    JSHandle<JSTaggedValue> call_back(thread, extra_info->GetCallback());
    if (!call_back->IsHeapObject()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSNativePointer> call_back_obj(call_back);
    FunctionCallback native_func = (reinterpret_cast<FunctionCallback>(call_back_obj->GetExternalPointer()));

    // this
    JSHandle<JSTaggedValue> this_value(ecmascript::builtins_common::GetThis(info));

    // arguments
    std::vector<Local<JSValueRef>> arguments;
    uint32_t length = info->GetArgsNumber();
    for (uint32_t i = 0; i < length; ++i) {
        arguments.emplace_back(JSNApiHelper::ToLocal<JSValueRef>(ecmascript::builtins_common::GetCallArg(info, i)));
    }

    ScopedNativeCodeThread s(thread);
    Local<JSValueRef> result = native_func(vm, JSNApiHelper::ToLocal<JSValueRef>(this_value), arguments.data(),
                                           arguments.size(), data_obj->GetExternalPointer());
    return JSNApiHelper::ToJSHandle(result).GetTaggedValue();
}

JSTaggedValue Callback::RegisterCallbackWithProperty(ecmascript::EcmaRuntimeCallInfo *info)
{
    // Constructor
    JSThread *thread = info->GetThread();
    JSHandle<JSTaggedValue> constructor = ecmascript::builtins_common::GetConstructor(info);
    if (!constructor->IsJSFunction()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSFunction> function(constructor);
    JSHandle<JSTaggedValue> extra_info_value(thread, function->GetFunctionExtraInfo());
    if (!extra_info_value->IsJSFunctionExtraInfo()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSFunctionExtraInfo> extra_info(extra_info_value);
    EcmaVM *vm = thread->GetEcmaVM();
    // data
    JSHandle<JSTaggedValue> data(thread, extra_info->GetData());
    if (!data->IsHeapObject()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSNativePointer> data_obj(data);
    // callBack
    JSHandle<JSTaggedValue> call_back(thread, extra_info->GetCallback());
    if (!call_back->IsHeapObject()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSNativePointer> call_back_obj(call_back);
    FunctionCallback native_func = (reinterpret_cast<FunctionCallback>(call_back_obj->GetExternalPointer()));

    // constructor
    JSHandle<JSTaggedValue> this_value(ecmascript::builtins_common::GetConstructor(info));

    // arguments
    std::vector<Local<JSValueRef>> arguments;
    uint32_t length = info->GetArgsNumber();
    for (uint32_t i = 0; i < length; ++i) {
        arguments.emplace_back(JSNApiHelper::ToLocal<JSValueRef>(ecmascript::builtins_common::GetCallArg(info, i)));
    }

    ScopedNativeCodeThread s(thread);
    Local<JSValueRef> result = native_func(vm, JSNApiHelper::ToLocal<JSValueRef>(this_value), arguments.data(),
                                           arguments.size(), data_obj->GetExternalPointer());
    return JSNApiHelper::ToJSHandle(result).GetTaggedValue();
}

JSTaggedValue Callback::RegisterCallbackWithNewTarget(ecmascript::EcmaRuntimeCallInfo *info)
{
    // Constructor
    JSThread *thread = info->GetThread();
    JSHandle<JSTaggedValue> constructor = ecmascript::builtins_common::GetConstructor(info);
    if (!constructor->IsJSFunction()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSFunction> function(constructor);
    JSHandle<JSTaggedValue> extra_info_value(thread, function->GetFunctionExtraInfo());
    if (!extra_info_value->IsJSFunctionExtraInfo()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSFunctionExtraInfo> extra_info(extra_info_value);
    EcmaVM *vm = thread->GetEcmaVM();
    // data
    JSHandle<JSTaggedValue> data(thread, extra_info->GetData());
    if (!data->IsHeapObject()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSNativePointer> data_obj(data);
    // callBack
    JSHandle<JSTaggedValue> call_back(thread, extra_info->GetCallback());
    if (!call_back->IsHeapObject()) {
        return JSTaggedValue::False();
    }
    JSHandle<JSNativePointer> call_back_obj(call_back);
    FunctionCallbackWithNewTarget native_func =
        (reinterpret_cast<FunctionCallbackWithNewTarget>(call_back_obj->GetExternalPointer()));

    // new_target
    JSHandle<JSTaggedValue> new_target(ecmascript::builtins_common::GetNewTarget(info));

    // this
    JSHandle<JSTaggedValue> this_value(ecmascript::builtins_common::GetThis(info));

    // arguments
    std::vector<Local<JSValueRef>> arguments;
    uint32_t length = info->GetArgsNumber();
    for (uint32_t i = 0; i < length; ++i) {
        arguments.emplace_back(JSNApiHelper::ToLocal<JSValueRef>(ecmascript::builtins_common::GetCallArg(info, i)));
    }

    ScopedNativeCodeThread s(thread);
    Local<JSValueRef> result =
        native_func(vm, JSNApiHelper::ToLocal<JSValueRef>(this_value), JSNApiHelper::ToLocal<JSValueRef>(new_target),
                    arguments.data(), arguments.size(), data_obj->GetExternalPointer());
    return JSNApiHelper::ToJSHandle(result).GetTaggedValue();
}

// -------------------------------------  JSExecutionScope ------------------------------
JSExecutionScope::JSExecutionScope(const EcmaVM *vm)
{
    (void)vm;
}

JSExecutionScope::~JSExecutionScope()
{
    last_current_thread_ = nullptr;
    is_revert_ = false;
}

// ----------------------------------- JSValueRef --------------------------------------
Local<PrimitiveRef> JSValueRef::Undefined(const EcmaVM *vm)
{
    return JSNApiHelper::ToLocal<PrimitiveRef>(JSHandle<JSTaggedValue>(vm->GetJSThread(), JSTaggedValue::Undefined()));
}

Local<PrimitiveRef> JSValueRef::Null(const EcmaVM *vm)
{
    return JSNApiHelper::ToLocal<PrimitiveRef>(JSHandle<JSTaggedValue>(vm->GetJSThread(), JSTaggedValue::Null()));
}

Local<PrimitiveRef> JSValueRef::True(const EcmaVM *vm)
{
    return JSNApiHelper::ToLocal<PrimitiveRef>(JSHandle<JSTaggedValue>(vm->GetJSThread(), JSTaggedValue::True()));
}

Local<PrimitiveRef> JSValueRef::False(const EcmaVM *vm)
{
    return JSNApiHelper::ToLocal<PrimitiveRef>(JSHandle<JSTaggedValue>(vm->GetJSThread(), JSTaggedValue::False()));
}

Local<JSValueRef> JSValueRef::Exception(const EcmaVM *vm)
{
    return JSNApiHelper::ToLocal<JSValueRef>(JSHandle<JSTaggedValue>(vm->GetJSThread(), JSTaggedValue::Exception()));
}

Local<ObjectRef> JSValueRef::ToObject(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    if (IsUndefined() || IsNull()) {
        return Exception(vm);
    }
    JSHandle<JSTaggedValue> obj(JSTaggedValue::ToObject(thread, JSNApiHelper::ToJSHandle(this)));
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<ObjectRef>(obj);
}

Local<StringRef> JSValueRef::ToString(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    if (!obj->IsString()) {
        obj = JSHandle<JSTaggedValue>(JSTaggedValue::ToString(thread, obj));
        RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    }
    return JSNApiHelper::ToLocal<StringRef>(obj);
}

Local<NativePointerRef> JSValueRef::ToNativePointer(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<NativePointerRef>(obj);
}

bool JSValueRef::BooleaValue()
{
    return JSNApiHelper::ToJSTaggedValue(this).ToBoolean();
}

int64_t JSValueRef::IntegerValue(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSTaggedNumber number = JSTaggedValue::ToInteger(thread, JSNApiHelper::ToJSHandle(this));
    RETURN_VALUE_IF_ABRUPT(thread, 0);
    return number.GetNumber();
}

uint32_t JSValueRef::Uint32Value(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    uint32_t number = JSTaggedValue::ToUint32(thread, JSNApiHelper::ToJSHandle(this));
    RETURN_VALUE_IF_ABRUPT(thread, 0);
    return number;
}

int32_t JSValueRef::Int32Value(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    int32_t number = JSTaggedValue::ToInt32(thread, JSNApiHelper::ToJSHandle(this));
    RETURN_VALUE_IF_ABRUPT(thread, 0);
    return number;
}

Local<BooleanRef> JSValueRef::ToBoolean(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> boolean_obj = JSHandle<JSTaggedValue>(thread, JSTaggedValue(obj->ToBoolean()));
    return JSNApiHelper::ToLocal<BooleanRef>(boolean_obj);
}

Local<NumberRef> JSValueRef::ToNumber(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> number(thread, JSTaggedValue::ToNumber(thread, obj));
    RETURN_VALUE_IF_ABRUPT(thread, JSValueRef::Exception(vm));
    return JSNApiHelper::ToLocal<NumberRef>(number);
}

bool JSValueRef::IsStrictEquals(const EcmaVM *vm, Local<JSValueRef> value)
{
    JSThread *thread = vm->GetJSThread();
    JSHandle<JSTaggedValue> x_value = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> y_value = JSNApiHelper::ToJSHandle(value);
    return JSTaggedValue::StrictEqual(thread, x_value, y_value);
}

Local<StringRef> JSValueRef::Typeof(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSTaggedValue value = FastRuntimeStub::FastTypeOf(thread, JSNApiHelper::ToJSTaggedValue(this));
    return JSNApiHelper::ToLocal<StringRef>(JSHandle<JSTaggedValue>(thread, value));
}

bool JSValueRef::InstanceOf(const EcmaVM *vm, Local<JSValueRef> value)
{
    JSThread *thread = vm->GetJSThread();
    ScopedManagedCodeThread s(thread);
    JSHandle<JSTaggedValue> origin = JSNApiHelper::ToJSHandle(this);
    JSHandle<JSTaggedValue> target = JSNApiHelper::ToJSHandle(value);
    bool result = JSObject::InstanceOf(thread, origin, target);
    RETURN_VALUE_IF_ABRUPT(thread, false);
    return result;
}

bool JSValueRef::IsUndefined()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsUndefined();
}

bool JSValueRef::IsNull()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsNull();
}

bool JSValueRef::IsHole()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsHole();
}

bool JSValueRef::IsTrue()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsTrue();
}

bool JSValueRef::IsFalse()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsFalse();
}

bool JSValueRef::IsNumber()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsNumber();
}

bool JSValueRef::IsInt()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsInt();
}

bool JSValueRef::WithinInt32()
{
    return JSNApiHelper::ToJSTaggedValue(this).WithinInt32();
}

bool JSValueRef::IsBoolean()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsBoolean();
}

bool JSValueRef::IsString()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsString();
}

bool JSValueRef::IsSymbol()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsSymbol();
}

bool JSValueRef::IsObject()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsECMAObject();
}

bool JSValueRef::IsArray(const EcmaVM *vm)
{
    JSThread *thread = vm->GetJSThread();
    return JSNApiHelper::ToJSTaggedValue(this).IsArray(thread);
}

bool JSValueRef::IsConstructor()
{
    JSTaggedValue value = JSNApiHelper::ToJSTaggedValue(this);
    return value.IsHeapObject() && value.IsConstructor();
}

bool JSValueRef::IsFunction()
{
    JSTaggedValue value = JSNApiHelper::ToJSTaggedValue(this);
    return value.IsHeapObject() && value.IsCallable();
}

bool JSValueRef::IsProxy()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSProxy();
}

bool JSValueRef::IsException()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsException();
}

bool JSValueRef::IsPromise()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSPromise();
}

bool JSValueRef::IsDataView()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsDataView();
}

bool JSValueRef::IsTypedArray()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsTypedArray();
}

bool JSValueRef::IsNativePointer()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSNativePointer();
}

bool JSValueRef::IsDate()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsDate();
}

bool JSValueRef::IsError()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSError();
}

bool JSValueRef::IsMap()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSMap();
}

bool JSValueRef::IsSet()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSSet();
}

bool JSValueRef::IsWeakMap()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSWeakMap();
}

bool JSValueRef::IsWeakSet()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSWeakSet();
}

bool JSValueRef::IsRegExp()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSRegExp();
}

bool JSValueRef::IsArrayIterator()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSArrayIterator();
}

bool JSValueRef::IsStringIterator()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsStringIterator();
}

bool JSValueRef::IsSetIterator()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSSetIterator();
}

bool JSValueRef::IsMapIterator()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSMapIterator();
}

bool JSValueRef::IsArrayBuffer()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsArrayBuffer();
}

bool JSValueRef::IsUint8Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSUint8Array();
}

bool JSValueRef::IsInt8Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSInt8Array();
}

bool JSValueRef::IsUint8ClampedArray()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSUint8ClampedArray();
}

bool JSValueRef::IsInt16Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSInt16Array();
}

bool JSValueRef::IsUint16Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSUint16Array();
}

bool JSValueRef::IsInt32Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSInt32Array();
}

bool JSValueRef::IsUint32Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSUint32Array();
}

bool JSValueRef::IsFloat32Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSFloat32Array();
}

bool JSValueRef::IsFloat64Array()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSFloat64Array();
}

bool JSValueRef::IsJSPrimitiveRef()
{
    return JSNApiHelper::ToJSTaggedValue(this).IsJSPrimitiveRef();
}

bool JSValueRef::IsJSPrimitiveNumber()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    return JSPrimitiveRef::Cast(obj->GetHeapObject())->IsNumber();
}

bool JSValueRef::IsJSPrimitiveInt()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    return JSPrimitiveRef::Cast(obj->GetHeapObject())->IsInt();
}

bool JSValueRef::IsJSPrimitiveBoolean()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    return JSPrimitiveRef::Cast(obj->GetHeapObject())->IsBoolean();
}

bool JSValueRef::IsJSPrimitiveString()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    return JSPrimitiveRef::Cast(obj->GetHeapObject())->IsString();
}

bool JSValueRef::IsJSPrimitiveSymbol()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    return JSPrimitiveRef::Cast(obj->GetHeapObject())->IsSymbol();
}

bool JSValueRef::IsGeneratorObject()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    bool rst = obj->IsGeneratorObject();
    return rst;
}

bool JSValueRef::IsAsyncFunction()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    bool rst = obj->IsJSAsyncFunction();
    return rst;
}

bool JSValueRef::IsArgumentsObject()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    bool rst = obj->IsArguments();
    return rst;
}

bool JSValueRef::IsGeneratorFunction()
{
    JSHandle<JSTaggedValue> obj = JSNApiHelper::ToJSHandle(this);
    bool rst = obj->IsGeneratorFunction();
    return rst;
}
}  // namespace panda
