/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_NAPI_JSNAPI_DEBUGGER_AGENT_H
#define ECMASCRIPT_NAPI_JSNAPI_DEBUGGER_AGENT_H

#include "plugins/ecmascript/runtime/napi/include/jsnapi.h"
#include "runtime/include/loadable_agent.h"

namespace panda {
class JSNApiDebuggerAgent : public LibraryAgent, public LibraryAgentLoader<JSNApiDebuggerAgent, false> {
public:
    JSNApiDebuggerAgent(os::memory::Mutex &mutex, const char *library_path, EcmaVM *vm, bool is_debug_mode);

private:
    bool CallLoadCallback(void *resolved_function) override;
    bool CallUnloadCallback(void *resolved_function) override;

    EcmaVM *vm_;
    bool is_debug_mode_;
};
}  // namespace panda

#endif  // ECMASCRIPT_NAPI_JSNAPI_DEBUGGER_AGENT_H
