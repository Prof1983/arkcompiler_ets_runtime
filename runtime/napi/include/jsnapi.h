/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_NAPI_INCLUDE_JSNAPI_H
#define ECMASCRIPT_NAPI_INCLUDE_JSNAPI_H

#include <cassert>
#include <cstdint>
#include <string>
#include <vector>

#include "plugins/ecmascript/runtime/common.h"
#include "libpandabase/macros.h"

namespace panda {
class JSNApiHelper;
class EscapeLocalScope;
class PromiseRejectInfo;
template <typename T>
class Global;
class JSNApi;
class PrimitiveRef;
class ArrayRef;
class StringRef;
class ObjectRef;
class FunctionRef;
class NumberRef;
class BooleanRef;
class NativePointerRef;
namespace test {
class JSNApiTests;
}  // namespace test

namespace ecmascript {
class EcmaVM;
class JSRuntimeOptions;
}  // namespace ecmascript

using Deleter = void (*)(void *buffer, void *data);
using EcmaVM = ecmascript::EcmaVM;
using JSTaggedType = uint64_t;

static constexpr uint32_t DEFAULT_GC_POOL_SIZE = 256 * 1024 * 1024;

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define ECMA_DISALLOW_COPY(className)      \
    className(const className &) = delete; \
    className &operator=(const className &) = delete

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define ECMA_DISALLOW_MOVE(className) \
    className(className &&) = delete; \
    className &operator=(className &&) = delete

template <typename T>
class PUBLIC_API Local {  // NOLINT(cppcoreguidelines-special-member-functions, hicpp-special-member-functions)
public:
    inline Local() = default;

    template <typename S>
    // NOLINTNEXTLINE(google-explicit-constructor)
    inline Local(const Local<S> &current) : address_(reinterpret_cast<uintptr_t>(*current))
    {
        // Check
    }

    Local(const EcmaVM *vm, const Global<T> &current);

    ~Local() = default;

    inline T *operator*() const
    {
        return GetAddress();
    }

    inline T *operator->() const
    {
        return GetAddress();
    }

    inline bool IsEmpty() const
    {
        return GetAddress() == nullptr;
    }

    inline bool CheckException() const
    {
        return IsEmpty() || GetAddress()->IsException();
    }

    inline bool IsException() const
    {
        return !IsEmpty() && GetAddress()->IsException();
    }

    inline bool IsNull() const
    {
        return IsEmpty() || GetAddress()->IsHole();
    }

private:
    explicit inline Local(uintptr_t addr) : address_(addr) {}
    inline T *GetAddress() const
    {
        return reinterpret_cast<T *>(address_);
    };
    uintptr_t address_ = 0U;
    friend JSNApiHelper;
    friend EscapeLocalScope;
};

template <typename T>
class PUBLIC_API Global {  // NOLINTNEXTLINE(cppcoreguidelines-special-member-functions
public:
    inline Global() = default;

    inline Global(const Global &that)
    {
        Update(that);
    }

    // NOLINTNEXTLINE(bugprone-unhandled-self-assignment)
    inline Global &operator=(const Global &that)
    {
        Update(that);
        return *this;
    }

    inline Global(Global &&that)
    {
        Update(that);
    }

    inline Global &operator=(Global &&that)
    {
        Update(that);
        return *this;
    }
    // Non-empty initial value.
    explicit Global(const EcmaVM *vm);

    template <typename S>
    Global(const EcmaVM *vm, const Local<S> &current);
    template <typename S>
    Global(const EcmaVM *vm, const Global<S> &current);
    ~Global() = default;

    Local<T> ToLocal(const EcmaVM *vm) const
    {
        return Local<T>(vm, *this);
    }

    void Empty()
    {
        address_ = 0;
    }

    // This method must be called before Global is released.
    void FreeGlobalHandleAddr();

    inline T *operator*() const
    {
        return GetAddress();
    }

    inline T *operator->() const
    {
        return GetAddress();
    }

    inline bool IsEmpty() const
    {
        return GetAddress() == nullptr;
    }

    inline bool CheckException() const
    {
        return IsEmpty() || GetAddress()->IsException();
    }

    void SetWeak();

    bool IsWeak() const;

private:
    inline T *GetAddress() const
    {
        return reinterpret_cast<T *>(address_);
    };
    inline void Update(const Global &that);
    uintptr_t address_ = 0U;
    const EcmaVM *vm_ {nullptr};
};

// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions, hicpp-special-member-functions)
class PUBLIC_API LocalScope {
public:
    explicit LocalScope(const EcmaVM *vm);
    virtual ~LocalScope();

protected:
    inline LocalScope(const EcmaVM *vm, JSTaggedType value);

private:
    void *prev_next_ = nullptr;
    void *prev_end_ = nullptr;
    int prev_handle_storage_index_ {-1};
    void *thread_ = nullptr;
};

class PUBLIC_API EscapeLocalScope final : public LocalScope {
public:
    explicit EscapeLocalScope(const EcmaVM *vm);
    ~EscapeLocalScope() override = default;

    ECMA_DISALLOW_COPY(EscapeLocalScope);
    ECMA_DISALLOW_MOVE(EscapeLocalScope);

    template <typename T>
    inline Local<T> Escape(Local<T> current)
    {
        ASSERT(!already_escape_);
        already_escape_ = true;
        *(reinterpret_cast<T *>(escape_handle_)) = **current;
        return Local<T>(escape_handle_);
    }

private:
    bool already_escape_ = false;
    uintptr_t escape_handle_ = 0U;
};

class PUBLIC_API JSExecutionScope {
public:
    explicit JSExecutionScope(const EcmaVM *vm);
    ~JSExecutionScope();
    ECMA_DISALLOW_COPY(JSExecutionScope);
    ECMA_DISALLOW_MOVE(JSExecutionScope);

private:
    void *last_current_thread_ = nullptr;
    bool is_revert_ = false;
};

class PUBLIC_API JSValueRef {
public:
    static Local<PrimitiveRef> Undefined(const EcmaVM *vm);
    static Local<PrimitiveRef> Null(const EcmaVM *vm);
    static Local<PrimitiveRef> True(const EcmaVM *vm);
    static Local<PrimitiveRef> False(const EcmaVM *vm);
    static Local<JSValueRef> Exception(const EcmaVM *vm);

    bool BooleaValue();
    int64_t IntegerValue(const EcmaVM *vm);
    uint32_t Uint32Value(const EcmaVM *vm);
    int32_t Int32Value(const EcmaVM *vm);

    Local<NumberRef> ToNumber(const EcmaVM *vm);
    Local<BooleanRef> ToBoolean(const EcmaVM *vm);
    Local<StringRef> ToString(const EcmaVM *vm);
    Local<ObjectRef> ToObject(const EcmaVM *vm);
    Local<NativePointerRef> ToNativePointer(const EcmaVM *vm);

    bool IsUndefined();
    bool IsNull();
    bool IsHole();
    bool IsTrue();
    bool IsFalse();
    bool IsNumber();
    bool IsInt();
    bool WithinInt32();
    bool IsBoolean();
    bool IsString();
    bool IsSymbol();
    bool IsObject();
    bool IsArray(const EcmaVM *vm);
    bool IsConstructor();
    bool IsFunction();
    bool IsProxy();
    bool IsException();
    bool IsPromise();
    bool IsDataView();
    bool IsTypedArray();
    bool IsNativePointer();
    bool IsDate();
    bool IsError();
    bool IsMap();
    bool IsSet();
    bool IsWeakMap();
    bool IsWeakSet();
    bool IsRegExp();
    bool IsArrayIterator();
    bool IsStringIterator();
    bool IsSetIterator();
    bool IsMapIterator();
    bool IsArrayBuffer();
    bool IsUint8Array();
    bool IsInt8Array();
    bool IsUint8ClampedArray();
    bool IsInt16Array();
    bool IsUint16Array();
    bool IsInt32Array();
    bool IsUint32Array();
    bool IsFloat32Array();
    bool IsFloat64Array();
    bool IsJSPrimitiveRef();
    bool IsJSPrimitiveNumber();
    bool IsJSPrimitiveInt();
    bool IsJSPrimitiveBoolean();
    bool IsJSPrimitiveString();

    bool IsGeneratorObject();
    bool IsJSPrimitiveSymbol();

    bool IsArgumentsObject();
    bool IsGeneratorFunction();
    bool IsAsyncFunction();

    bool IsStrictEquals(const EcmaVM *vm, Local<JSValueRef> value);
    Local<StringRef> Typeof(const EcmaVM *vm);
    bool InstanceOf(const EcmaVM *vm, Local<JSValueRef> value);

private:
    JSTaggedType value_;
    friend JSNApi;
    template <typename T>
    friend class Global;
    template <typename T>
    friend class Local;
};

class PUBLIC_API PrimitiveRef : public JSValueRef {};

class PUBLIC_API IntegerRef : public PrimitiveRef {
public:
    static Local<IntegerRef> New(const EcmaVM *vm, int input);
    static Local<IntegerRef> NewFromUnsigned(const EcmaVM *vm, unsigned int input);
    int Value();
};

class PUBLIC_API NumberRef : public PrimitiveRef {
public:
    static Local<NumberRef> New(const EcmaVM *vm, double input);
    double Value();
};

class PUBLIC_API BooleanRef : public PrimitiveRef {
public:
    static Local<BooleanRef> New(const EcmaVM *vm, bool input);
    bool Value();
};

class PUBLIC_API StringRef : public PrimitiveRef {
public:
    static inline StringRef *Cast(JSValueRef *value)
    {
        // check
        return static_cast<StringRef *>(value);
    }
    static Local<StringRef> NewFromUtf8(const EcmaVM *vm, const char *utf8, int length = -1);
    std::string ToString();
    int32_t Length();
    int32_t Utf8Length();
    int WriteUtf8(char *buffer, int length);
};

class PUBLIC_API SymbolRef : public PrimitiveRef {
public:
    static Local<SymbolRef> New(const EcmaVM *vm, Local<StringRef> description);
    Local<StringRef> GetDescription(const EcmaVM *vm);
};

using NativePointerCallback = void (*)(void *value, void *hint);
class PUBLIC_API NativePointerRef : public JSValueRef {
public:
    static Local<NativePointerRef> New(const EcmaVM *vm, void *native_pointer);
    static Local<NativePointerRef> New(const EcmaVM *vm, void *native_pointer, NativePointerCallback call_back,
                                       void *data);
    void *Value();
};

// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions, hicpp-special-member-functions)
class PUBLIC_API PropertyAttribute {
public:
    static PropertyAttribute Default()
    {
        return PropertyAttribute();
    }
    PropertyAttribute() = default;
    PropertyAttribute(Local<JSValueRef> value, bool w, bool e, bool c)
        : value_(value),
          writable_(w),
          enumerable_(e),
          configurable_(c),
          has_writable_(true),
          has_enumerable_(true),
          has_configurable_(true)
    {
    }
    ~PropertyAttribute() = default;

    bool IsWritable() const
    {
        return writable_;
    }
    void SetWritable(bool flag)
    {
        writable_ = flag;
        has_writable_ = true;
    }
    bool IsEnumerable() const
    {
        return enumerable_;
    }
    void SetEnumerable(bool flag)
    {
        enumerable_ = flag;
        has_enumerable_ = true;
    }
    bool IsConfigurable() const
    {
        return configurable_;
    }
    void SetConfigurable(bool flag)
    {
        configurable_ = flag;
        has_configurable_ = true;
    }
    bool HasWritable() const
    {
        return has_writable_;
    }
    bool HasConfigurable() const
    {
        return has_configurable_;
    }
    bool HasEnumerable() const
    {
        return has_enumerable_;
    }
    Local<JSValueRef> GetValue(const EcmaVM *vm) const
    {
        if (value_.IsEmpty()) {
            return Local<JSValueRef>(JSValueRef::Undefined(vm));
        }
        return value_;
    }
    void SetValue(Local<JSValueRef> value)
    {
        value_ = value;
    }
    inline bool HasValue() const
    {
        return !value_.IsEmpty();
    }
    Local<JSValueRef> GetGetter(const EcmaVM *vm) const
    {
        if (getter_.IsEmpty()) {
            return Local<JSValueRef>(JSValueRef::Undefined(vm));
        }
        return getter_;
    }
    void SetGetter(Local<JSValueRef> value)
    {
        getter_ = value;
    }
    bool HasGetter() const
    {
        return !getter_.IsEmpty();
    }
    Local<JSValueRef> GetSetter(const EcmaVM *vm) const
    {
        if (setter_.IsEmpty()) {
            return Local<JSValueRef>(JSValueRef::Undefined(vm));
        }
        return setter_;
    }
    void SetSetter(Local<JSValueRef> value)
    {
        setter_ = value;
    }
    bool HasSetter() const
    {
        return !setter_.IsEmpty();
    }

private:
    Local<JSValueRef> value_;
    Local<JSValueRef> getter_;
    Local<JSValueRef> setter_;
    bool writable_ = false;
    bool enumerable_ = false;
    bool configurable_ = false;
    bool has_writable_ = false;
    bool has_enumerable_ = false;
    bool has_configurable_ = false;
};

class PUBLIC_API ObjectRef : public JSValueRef {
public:
    static inline ObjectRef *Cast(JSValueRef *value)
    {
        // check
        return static_cast<ObjectRef *>(value);
    }
    static Local<ObjectRef> New(const EcmaVM *vm);
    bool Set(const EcmaVM *vm, Local<JSValueRef> key, Local<JSValueRef> value);
    bool Set(const EcmaVM *vm, uint32_t key, Local<JSValueRef> value);
    bool SetAccessorProperty(const EcmaVM *vm, Local<JSValueRef> key, Local<FunctionRef> getter,
                             Local<FunctionRef> setter, PropertyAttribute attribute = PropertyAttribute::Default());
    Local<JSValueRef> Get(const EcmaVM *vm, Local<JSValueRef> key);
    Local<JSValueRef> Get(const EcmaVM *vm, int32_t key);

    bool GetOwnProperty(const EcmaVM *vm, Local<JSValueRef> key, PropertyAttribute &property);
    Local<ArrayRef> GetOwnPropertyNames(const EcmaVM *vm);
    Local<ArrayRef> GetOwnEnumerablePropertyNames(const EcmaVM *vm);
    Local<JSValueRef> GetPrototype(const EcmaVM *vm);

    bool DefineProperty(const EcmaVM *vm, Local<JSValueRef> key, PropertyAttribute attribute);

    bool Has(const EcmaVM *vm, Local<JSValueRef> key);
    bool Has(const EcmaVM *vm, uint32_t key);

    bool Delete(const EcmaVM *vm, Local<JSValueRef> key);
    bool Delete(const EcmaVM *vm, uint32_t key);

    void SetNativePointerFieldCount(int32_t count);
    int32_t GetNativePointerFieldCount();
    void *GetNativePointerField(int32_t index);
    void SetNativePointerField(int32_t index, void *native_pointer = nullptr, NativePointerCallback call_back = nullptr,
                               void *data = nullptr);
};

using FunctionCallback = Local<JSValueRef> (*)(EcmaVM *, Local<JSValueRef>,
                                               const Local<JSValueRef>[],  // NOLINT(modernize-avoid-c-arrays)
                                               int32_t, void *);
using FunctionCallbackWithNewTarget =
    Local<JSValueRef> (*)(EcmaVM *, Local<JSValueRef>, Local<JSValueRef>,
                          const Local<JSValueRef>[],  // NOLINT(modernize-avoid-c-arrays)
                          int32_t, void *);
class PUBLIC_API FunctionRef : public ObjectRef {
public:
    static Local<FunctionRef> New(EcmaVM *vm, FunctionCallback native_func, void *data);
    static Local<FunctionRef> New(EcmaVM *vm, FunctionCallback native_func, Deleter deleter, void *data);
    static Local<FunctionRef> NewWithProperty(EcmaVM *vm, FunctionCallback native_func, void *data);
    static Local<FunctionRef> NewClassFunction(EcmaVM *vm, FunctionCallbackWithNewTarget native_func, Deleter deleter,
                                               void *data);
    Local<JSValueRef> Call(const EcmaVM *vm, Local<JSValueRef> this_obj,
                           const Local<JSValueRef> argv[],  // NOLINT(modernize-avoid-c-arrays)
                           int32_t length);
    Local<JSValueRef> Constructor(const EcmaVM *vm,
                                  const Local<JSValueRef> argv[],  // NOLINT(modernize-avoid-c-arrays)
                                  int32_t length);
    Local<JSValueRef> GetFunctionPrototype(const EcmaVM *vm);
    // Inherit Prototype from parent function
    // set this.Prototype.__proto__ to parent.Prototype, set this.__proto__ to parent function
    bool Inherit(const EcmaVM *vm, Local<FunctionRef> parent);
    void SetName(const EcmaVM *vm, Local<StringRef> name);
    Local<StringRef> GetName(const EcmaVM *vm);
    bool IsNative(const EcmaVM *vm);
};

class PUBLIC_API ArrayRef : public ObjectRef {
public:
    static Local<ArrayRef> New(const EcmaVM *vm, int32_t length = 0);
    int32_t Length(const EcmaVM *vm);
    static bool SetValueAt(const EcmaVM *vm, Local<JSValueRef> obj, uint32_t index, Local<JSValueRef> value);
    static Local<JSValueRef> GetValueAt(const EcmaVM *vm, Local<JSValueRef> obj, uint32_t index);
};

class PUBLIC_API PromiseRef : public ObjectRef {
public:
    Local<PromiseRef> Catch(const EcmaVM *vm, Local<FunctionRef> handler);
    Local<PromiseRef> Then(const EcmaVM *vm, Local<FunctionRef> handler);
    Local<PromiseRef> Then(const EcmaVM *vm, Local<FunctionRef> on_fulfilled, Local<FunctionRef> on_rejected);
};

class PUBLIC_API PromiseCapabilityRef : public ObjectRef {
public:
    static Local<PromiseCapabilityRef> New(const EcmaVM *vm);
    bool Resolve(const EcmaVM *vm, Local<JSValueRef> value);
    bool Reject(const EcmaVM *vm, Local<JSValueRef> reason);
    Local<PromiseRef> GetPromise(const EcmaVM *vm);
};

class PUBLIC_API ArrayBufferRef : public ObjectRef {
public:
    static Local<ArrayBufferRef> New(const EcmaVM *vm, int32_t length);
    static Local<ArrayBufferRef> New(const EcmaVM *vm, void *buffer, int32_t length, const Deleter &deleter,
                                     void *data);

    int32_t ByteLength(const EcmaVM *vm);
    void *GetBuffer();
};

class PUBLIC_API DataViewRef : public ObjectRef {
public:
    static Local<DataViewRef> New(const EcmaVM *vm, Local<ArrayBufferRef> array_buffer, int32_t byte_offset,
                                  int32_t byte_length);
    int32_t ByteLength();
    int32_t ByteOffset();
    Local<ArrayBufferRef> GetArrayBuffer(const EcmaVM *vm);
};

class PUBLIC_API TypedArrayRef : public ObjectRef {
public:
    int32_t ByteLength(const EcmaVM *vm);
    int32_t ByteOffset(const EcmaVM *vm);
    int32_t ArrayLength(const EcmaVM *vm);
    Local<ArrayBufferRef> GetArrayBuffer(const EcmaVM *vm);
};

class PUBLIC_API Int8ArrayRef : public TypedArrayRef {
public:
    static Local<Int8ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset, int32_t length);
};

class PUBLIC_API Uint8ArrayRef : public TypedArrayRef {
public:
    static Local<Uint8ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                    int32_t length);
};

class PUBLIC_API Uint8ClampedArrayRef : public TypedArrayRef {
public:
    static Local<Uint8ClampedArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                           int32_t length);
};

class PUBLIC_API Int16ArrayRef : public TypedArrayRef {
public:
    static Local<Int16ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                    int32_t length);
};

class PUBLIC_API Uint16ArrayRef : public TypedArrayRef {
public:
    static Local<Uint16ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                     int32_t length);
};

class PUBLIC_API Int32ArrayRef : public TypedArrayRef {
public:
    static Local<Int32ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                    int32_t length);
};

class PUBLIC_API Uint32ArrayRef : public TypedArrayRef {
public:
    static Local<Uint32ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                     int32_t length);
};

class PUBLIC_API Float32ArrayRef : public TypedArrayRef {
public:
    static Local<Float32ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                      int32_t length);
};

class PUBLIC_API Float64ArrayRef : public TypedArrayRef {
public:
    static Local<Float64ArrayRef> New(const EcmaVM *vm, Local<ArrayBufferRef> buffer, int32_t byte_offset,
                                      int32_t length);
};

class PUBLIC_API RegExpRef : public ObjectRef {
public:
    Local<StringRef> GetOriginalSource(const EcmaVM *vm);
};

class PUBLIC_API DateRef : public ObjectRef {
public:
    static Local<DateRef> New(const EcmaVM *vm, double time);
    Local<StringRef> ToString(const EcmaVM *vm);
    double GetTime();
};

class PUBLIC_API MapRef : public ObjectRef {
public:
    int32_t GetSize();
};

class PUBLIC_API SetRef : public ObjectRef {
public:
    int32_t GetSize();
};

class PUBLIC_API JSON {
public:
    static Local<JSValueRef> Parse(const EcmaVM *vm, Local<StringRef> string);
    static Local<JSValueRef> Stringify(const EcmaVM *vm, Local<JSValueRef> json);
};

class PUBLIC_API Exception {
public:
    static Local<JSValueRef> Error(const EcmaVM *vm, Local<StringRef> message);
    static Local<JSValueRef> RangeError(const EcmaVM *vm, Local<StringRef> message);
    static Local<JSValueRef> ReferenceError(const EcmaVM *vm, Local<StringRef> message);
    static Local<JSValueRef> SyntaxError(const EcmaVM *vm, Local<StringRef> message);
    static Local<JSValueRef> TypeError(const EcmaVM *vm, Local<StringRef> message);
    static Local<JSValueRef> EvalError(const EcmaVM *vm, Local<StringRef> message);
};

using LogPrint = int (*)(int id, int level, const char *tag, const char *fmt, const char *message);

class PUBLIC_API RuntimeOption {
public:
    enum class PUBLIC_API GcType : uint8_t { EPSILON, GEN_GC, STW, G1_GC };
    enum class PUBLIC_API LogLevel : uint8_t {
        DEBUG = 3,
        INFO = 4,
        WARN = 5,
        ERROR = 6,
        FATAL = 7,
    };

    void SetExplicitConcurrentGcEnabled(bool value)
    {
        is_explicit_concurrent_gc_enabled_ = value;
    }

    void SetGcType(GcType type)
    {
        gc_type_ = type;
    }

    void SetGcPoolSize(uint32_t size)
    {
        gc_pool_size_ = size;
    }

    void SetLogLevel(LogLevel log_level)
    {
        log_level_ = log_level;
    }

    void SetLogBufPrint(LogPrint out)
    {
        log_buf_print_ = out;
    }

    void SetDebuggerLibraryPath(const std::string &path)
    {
        debugger_library_path_ = path;
    }

    void SetEnableArkTools(bool value)
    {
        enable_ark_tools_ = value;
    }

    void SetArkProperties(int prop)
    {
        ark_properties_ = prop;
    }

private:
    std::string GetGcType() const
    {
        std::string gc_type;
        switch (gc_type_) {
            case GcType::GEN_GC:
                gc_type = "gen-gc";
                break;
            case GcType::STW:
                gc_type = "stw";
                break;
            case GcType::EPSILON:
                gc_type = "epsilon";
                break;
            case GcType::G1_GC:
                gc_type = "g1-gc";
                break;
            default:
                UNREACHABLE();
        }
        return gc_type;
    }

    std::string GetLogLevel() const
    {
        std::string log_level;
        switch (log_level_) {
            case LogLevel::INFO:
            case LogLevel::WARN:
                log_level = "info";
                break;
            case LogLevel::ERROR:
                log_level = "error";
                break;
            case LogLevel::FATAL:
                log_level = "fatal";
                break;
            case LogLevel::DEBUG:
            default:
                log_level = "debug";
                break;
        }

        return log_level;
    }

    bool GetExplicitConcurrentGcEnabled() const
    {
        return is_explicit_concurrent_gc_enabled_;
    }

    uint32_t GetGcPoolSize() const
    {
        return gc_pool_size_;
    }

    LogPrint GetLogBufPrint() const
    {
        return log_buf_print_;
    }

    std::string GetDebuggerLibraryPath() const
    {
        return debugger_library_path_;
    }

    bool GetEnableArkTools() const
    {
        return enable_ark_tools_;
    }

    int GetArkProperties() const
    {
        return ark_properties_;
    }

    GcType gc_type_ = GcType::EPSILON;
    LogLevel log_level_ = LogLevel::DEBUG;
    bool is_explicit_concurrent_gc_enabled_ {false};
    uint32_t gc_pool_size_ = DEFAULT_GC_POOL_SIZE;
    LogPrint log_buf_print_ {nullptr};
    std::string debugger_library_path_ {};
    bool enable_ark_tools_ {false};
    int ark_properties_ {-1};
    friend JSNApi;
};

class PUBLIC_API PromiseRejectInfo {
public:
    enum class PUBLIC_API PromiseRejectionEvent : uint32_t { REJECT = 0, HANDLE };
    PromiseRejectInfo(Local<JSValueRef> promise, Local<JSValueRef> reason,
                      PromiseRejectInfo::PromiseRejectionEvent operation, void *data);
    ~PromiseRejectInfo() = default;
    Local<JSValueRef> GetPromise() const;
    Local<JSValueRef> GetReason() const;
    PromiseRejectInfo::PromiseRejectionEvent GetOperation() const;
    void *GetData() const;

    DEFAULT_MOVE_SEMANTIC(PromiseRejectInfo);
    DEFAULT_COPY_SEMANTIC(PromiseRejectInfo);

private:
    Local<JSValueRef> promise_ {};
    Local<JSValueRef> reason_ {};
    PromiseRejectionEvent operation_ = PromiseRejectionEvent::REJECT;
    void *data_ {nullptr};
};

class PUBLIC_API JSNApi {
public:
    // JSVM
    enum class PUBLIC_API TriggerGcType : uint8_t { SEMI_GC, OLD_GC, COMPRESS_FULL_GC };
    static EcmaVM *CreateJSVM(const RuntimeOption &option);
    static void DestroyJSVM(EcmaVM *ecma_vm);

    // JS code
    static bool Execute(EcmaVM *vm, Local<StringRef> file_name, Local<StringRef> entry);
    static bool Execute(EcmaVM *vm, const std::string &file_name, const std::string &entry);
    static bool Execute(EcmaVM *vm, const uint8_t *data, int32_t size, Local<StringRef> entry);
    static bool Execute(EcmaVM *vm, const uint8_t *data, int32_t size, const std::string &entry);
    static bool ExecuteModuleFromBuffer(EcmaVM *vm, const void *data, int32_t size, const std::string &file);
    static Local<ObjectRef> GetExportObject(EcmaVM *vm, const std::string &file, const std::string &item_name);

    // ObjectRef Operation
    static Local<ObjectRef> GetGlobalObject(const EcmaVM *vm);
    static void ExecutePendingJob(const EcmaVM *vm);

    // Memory
    static void TriggerGC(const EcmaVM *vm, TriggerGcType gc_type = TriggerGcType::SEMI_GC);
    // Exception
    static void ThrowException(const EcmaVM *vm, Local<JSValueRef> error);
    static Local<ObjectRef> GetUncaughtException(const EcmaVM *vm);
    static Local<ObjectRef> GetAndClearUncaughtException(const EcmaVM *vm);
    static void EnableUserUncaughtErrorHandler(EcmaVM *vm);

    static bool StartDebugger(const char *library_path, EcmaVM *vm, bool is_debug_mode);
    static bool StopDebugger(const char *library_path);
    // Serialize & Deserialize.
    static void *SerializeValue(const EcmaVM *vm, Local<JSValueRef> value, Local<JSValueRef> transfer);
    static Local<JSValueRef> DeserializeValue(const EcmaVM *vm, void *recoder);
    static void DeleteSerializationData(void *data);
    static void SetOptions(const ecmascript::JSRuntimeOptions &options);
    static void SetHostPromiseRejectionTracker(EcmaVM *vm, void *cb, void *data);
    static void SetHostEnqueueJob(const EcmaVM *vm, Local<JSValueRef> cb);

private:
    static bool CreateRuntime(const RuntimeOption &option);
    static bool DestroyRuntime();

    static uintptr_t GetHandleAddr(const EcmaVM *vm, uintptr_t local_address);
    static uintptr_t GetGlobalHandleAddr(const EcmaVM *vm, uintptr_t local_address);
    static uintptr_t SetWeak(const EcmaVM *vm, uintptr_t local_address);
    static bool IsWeak(const EcmaVM *vm, uintptr_t local_address);
    static void DisposeGlobalHandleAddr(const EcmaVM *vm, uintptr_t addr);
    template <typename T>
    friend class Global;
    template <typename T>
    friend class Local;
    friend class test::JSNApiTests;
};

template <typename T>
template <typename S>
Global<T>::Global(const EcmaVM *vm, const Local<S> &current) : vm_(vm)
{
    if (!current.IsEmpty()) {
        address_ = JSNApi::GetGlobalHandleAddr(vm_, reinterpret_cast<uintptr_t>(*current));
    }
}

template <typename T>
template <typename S>
Global<T>::Global(const EcmaVM *vm, const Global<S> &current) : vm_(vm)
{
    if (!current.IsEmpty()) {
        address_ = JSNApi::GetGlobalHandleAddr(vm_, reinterpret_cast<uintptr_t>(*current));
    }
}

template <typename T>
void Global<T>::Update(const Global &that)
{
    if (address_ != 0) {
        JSNApi::DisposeGlobalHandleAddr(vm_, address_);
    }
    address_ = that.address_;
    vm_ = that.vm_;
}

template <typename T>
void Global<T>::FreeGlobalHandleAddr()
{
    if (address_ == 0) {
        return;
    }
    JSNApi::DisposeGlobalHandleAddr(vm_, address_);
    address_ = 0;
}

template <typename T>
void Global<T>::SetWeak()
{
    address_ = JSNApi::SetWeak(vm_, address_);
}

template <typename T>
bool Global<T>::IsWeak() const
{
    return JSNApi::IsWeak(vm_, address_);
}

// ---------------------------------- Local --------------------------------------------
template <typename T>
Local<T>::Local(const EcmaVM *vm, const Global<T> &current)
{
    address_ = JSNApi::GetHandleAddr(vm, reinterpret_cast<uintptr_t>(*current));
}
}  // namespace panda
#endif  // ECMASCRIPT_NAPI_INCLUDE_JSNAPI_H
