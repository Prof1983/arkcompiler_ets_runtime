/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "runtime/profiling/profiling.h"
#include "plugins/ecmascript/runtime/compiler/ecmascript_runtime_interface.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/global_dictionary-inl.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/ecmascript_environment.h"
#include "plugins/ecmascript/compiler/ecmascript_extensions/thread_environment_api.h"
#include "serializer/serializer.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_hclass-inl.h"
#include "plugins/ecmascript/runtime/ic/ic_handler.h"
#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/ic/proto_change_details.h"
#include "runtime/mem/refstorage/global_object_storage.h"

namespace panda::ecmascript {

EcmaRuntimeInterface::EcmaRuntimeInterface(const EcmaVM *ecma_vm, mem::InternalAllocatorPtr internal_allocator)
    : ecma_vm_(ecma_vm), internal_allocator_(internal_allocator)
{
}
EcmaRuntimeInterface::~EcmaRuntimeInterface()
{
    os::memory::LockHolder lock(mutex_h_);
    for (auto it : handles_by_method_table_) {
        internal_allocator_->template Delete(it.second);
    }
}

size_t EcmaRuntimeInterface::GetLanguageExtensionSize([[maybe_unused]] Arch arch) const
{
    return cross_values::GetEcmascriptEnvironmentSize(arch);
}

std::string EcmaRuntimeInterface::GetMethodFullName(MethodPtr method, [[maybe_unused]] bool with_signature) const
{
    ASSERT(panda::panda_file::IsDynamicLanguage(MethodCast(method)->GetClass()->GetSourceLang()));
    return std::string(static_cast<ecmascript::JSMethod *>(MethodCast(method))->GetFullName());
}

uint64_t EcmaRuntimeInterface::DynamicCastDoubleToInt(double value, size_t bits) const
{
    return ecmascript::base::NumberHelper::DoubleToInt(value, bits);
}

uint8_t EcmaRuntimeInterface::GetDynamicNumFixedArgs() const
{
    return 3;  // func, new_target, this
}

uint32_t EcmaRuntimeInterface::GetFunctionTargetOffset([[maybe_unused]] Arch arch) const
{
    return cross_values::GetJsfunctionMethodOffset(arch);
}

uintptr_t EcmaRuntimeInterface::GetGlobalVarAddress(MethodPtr method, size_t id)
{
    auto thread = TryGetJSThread();
    // Thread can be removed during destroy runtime
    if (thread == nullptr) {
        return 0;
    }

    ScopedMutatorLock lock;

    auto [dict, entry] = GetGlobalDictionaryEntry(thread, method, id);
    if (entry == -1) {
        return 0;
    }

    JSTaggedValue res(dict->GetSafeBox(entry));
    // we can't set an attribute in compiler thread.
    if (res.IsUndefined() || res.IsHole() || !res.IsPropertyBox()) {
        return 0;
    }

    ASSERT(res.IsPropertyBox());
    return reinterpret_cast<uintptr_t>(res.GetHeapObject());
}

#include "plugins/ecmascript/runtime/builtins/generated/builtins_resolve_inlinable_gen.inl"

EcmaRuntimeInterface::MethodProfile EcmaRuntimeInterface::GetMethodProfile(MethodPtr method, bool from_vector) const
{
    if (from_vector) {
        if (auto prof_vector = JsMethodCast(method)->GetProfilingVector(); prof_vector != nullptr) {
            return reinterpret_cast<EcmaRuntimeInterface::MethodProfile>(prof_vector);
        }
        return profiling::INVALID_PROFILE;
    }
    auto name = GetMethodFullName(method, false);
    auto prof = profile_.find(std::string(name));
    if (prof == profile_.end()) {
        LOG(DEBUG, COMPILER) << "Profile not found for " << name;
    }
    return (prof != profile_.end()) ? reinterpret_cast<EcmaRuntimeInterface::MethodProfile>(
                                          const_cast<EcmaProfileElement::value_type *>(prof->second.data()))
                                    : profiling::INVALID_PROFILE;
}

EcmaRuntimeInterface::BytecodeProfile EcmaRuntimeInterface::GetBytecodeProfile(MethodProfile profile,
                                                                               const uint8_t *bc_inst,
                                                                               [[maybe_unused]] size_t pc) const
{
    if (profile == nullptr) {
        return 0;
    }
    auto profile_id = BytecodeInstruction(bc_inst).GetProfileId();
    if (profile_id == -1) {
        return 0;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return reinterpret_cast<uintptr_t>(reinterpret_cast<uint8_t *>(profile) + profile_id);
}

bool EcmaRuntimeInterface::CanInlineLdStObjByIndex(const BytecodeInstruction *bc_inst, size_t pc,
                                                   MethodProfile method_profile) const
{
    auto profile = GetBytecodeProfile(method_profile, bc_inst->GetAddress(), pc);
    auto ecma_prof = reinterpret_cast<uint8_t *>(profile);
    panda::ecmascript::ObjByIndexOperationProfile p(ecma_prof);
    return p.GetOperandType(0).GetType() == panda::ecmascript::ProfilingIndexedAccessBits::OBJECT_ARRAY_ACCESS;
}

profiling::CallKind EcmaRuntimeInterface::GetCallProfile(MethodPtr method, uint32_t pc, ArenaVector<uintptr_t> *methods,
                                                         bool is_aot)
{
    auto profile = GetMethodProfile(method, !is_aot);
    if (profile == nullptr) {
        return profiling::CallKind::UNKNOWN;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto bc_inst = MethodCast(method)->GetInstructions() + pc;
    auto profile_id = BytecodeInstruction(bc_inst).GetProfileId();
    if (profile_id == -1) {
        return profiling::CallKind::UNKNOWN;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto call_prof = CallProfile::FromBuffer(reinterpret_cast<uint8_t *>(profile) + profile_id);
    auto call_kind = call_prof->GetCallKind();
    if (call_kind != profiling::CallKind::UNKNOWN && call_kind != profiling::CallKind::MEGAMORPHIC) {
        for (auto ptr : call_prof->GetCalleesPtr(ecma_vm_->GetEcmaCallProfileTable())) {
            if (ptr == CallProfile::UNKNOWN) {
                break;
            }
            methods->push_back(ptr);
        }
    }
    return call_kind;
}

Expected<bool, const char *> EcmaRuntimeInterface::AddProfile(std::string_view fname)
{
    std::ifstream stm(fname.data(), std::ios::binary);
    if (!stm.is_open()) {
        return Unexpected("Cannot open profile file");
    }

    std::vector<uint8_t> buffer {std::istreambuf_iterator<char>(stm), std::istreambuf_iterator<char>()};
    auto res = serializer::BufferToType(buffer.data(), buffer.size(), profile_);
    if (!res) {
        LOG(FATAL, RUNTIME) << "Failed to deserialize: " << res.Error();
    }

    return true;
}

inline compiler::AnyBaseType GetTypeOfType(panda::ecmascript::ProfilingTypeOfBits::Type type,
                                           profiling::AnyInputType *allowed_input_type, bool *is_type_profiled)
{
    if (type == panda::ecmascript::ProfilingTypeOfBits::NONE) {
        return compiler::AnyBaseType::UNDEFINED_TYPE;
    }

    *is_type_profiled = true;
    switch (type) {
        case panda::ecmascript::ProfilingTypeOfBits::NUMBER: {
            *allowed_input_type = profiling::AnyInputType::INTEGER;
            return compiler::AnyBaseType::ECMASCRIPT_DOUBLE_TYPE;
        }
        case panda::ecmascript::ProfilingTypeOfBits::SYMBOL:
            return compiler::AnyBaseType::ECMASCRIPT_SYMBOL_TYPE;
        case panda::ecmascript::ProfilingTypeOfBits::BOOLEAN:
            return compiler::AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE;
        case panda::ecmascript::ProfilingTypeOfBits::STRING:
            return compiler::AnyBaseType::ECMASCRIPT_STRING_TYPE;
        case panda::ecmascript::ProfilingTypeOfBits::FUNCTION:
            return compiler::AnyBaseType::ECMASCRIPT_CALLABLE_TYPE;
        case panda::ecmascript::ProfilingTypeOfBits::UNDEFINED:
            return compiler::AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE;
        case panda::ecmascript::ProfilingTypeOfBits::BIGINT:
            return compiler::AnyBaseType::ECMASCRIPT_BIGINT_TYPE;
        default:
            return compiler::AnyBaseType::UNDEFINED_TYPE;
    }
    return compiler::AnyBaseType::UNDEFINED_TYPE;
}

static bool IsEqualsInst(const BytecodeInstruction *bc_inst)
{
    switch (bc_inst->GetOpcode()) {
        case BytecodeInstruction::Opcode::ECMA_EQDYN_PREF_V8_PROF16:
        case BytecodeInstruction::Opcode::ECMA_NOTEQDYN_PREF_V8_PROF16:
        case BytecodeInstruction::Opcode::ECMA_STRICTEQDYN_PREF_V8_PROF16:
        case BytecodeInstruction::Opcode::ECMA_STRICTNOTEQDYN_PREF_V8_PROF16:
            return true;
        default:
            return false;
    }
}

compiler::AnyBaseType EcmaRuntimeInterface::GetProfilingAnyType(RuntimeInterface::BytecodeProfile profile,
                                                                const BytecodeInstruction *bc_inst, unsigned index,
                                                                profiling::AnyInputType *allowed_input_type,
                                                                bool *is_type_profiled)
{
    auto kind = profiling::GetProfileKind(bc_inst->GetOpcode());
    auto ecma_prof = reinterpret_cast<uint8_t *>(profile);
    ProfilingTypeBits::Type type = ProfilingTypeBits::NONE;
    switch (kind) {
        case profiling::ProfilingKind::TYPE_OF: {
            panda::ecmascript::TypeOfOperationProfile p(ecma_prof);
            return GetTypeOfType(p.GetOperandType(index).GetType(), allowed_input_type, is_type_profiled);
        }
        case profiling::ProfilingKind::UNARY_ARITH: {
            UnaryOperationProfile p(ecma_prof);
            type = p.GetOperandType(index).GetType();
            if (type != ProfilingTypeBits::NONE) {
                *is_type_profiled = true;
            }

            if (type == ProfilingTypeBits::STRING) {
                return compiler::AnyBaseType::ECMASCRIPT_STRING_TYPE;
            }
            if ((type & ProfilingTypeBits::HEAP_OBJECT) != 0) {
                return compiler::AnyBaseType::UNDEFINED_TYPE;
            }
            break;
        }
        case profiling::ProfilingKind::BINARY_ARITH: {
            BinaryOperationProfile p(ecma_prof);
            type = p.GetOperandType(index).GetType();
            if (type != ProfilingTypeBits::NONE) {
                *is_type_profiled = true;
            }
            auto other_operand_type = p.GetOperandType(1 - index).GetType();
            if (type == ProfilingTypeBits::STRING && other_operand_type == ProfilingTypeBits::STRING) {
                return compiler::AnyBaseType::ECMASCRIPT_STRING_TYPE;
            }
            auto object_count = ((type & ProfilingTypeBits::HEAP_OBJECT) != 0 ? 1 : 0) +
                                ((other_operand_type & ProfilingTypeBits::HEAP_OBJECT) != 0 ? 1 : 0);
            // For opcodes like eq non-numeric types are common, do not inline them if object was seen as one of inputs
            // For arithmetic opcodes if we saw double and object, assume double type and deoptimize without method
            // destruction in case of object
            if (object_count == 2 || (object_count == 1 && IsEqualsInst(bc_inst))) {
                *is_type_profiled = true;
                return compiler::AnyBaseType::UNDEFINED_TYPE;
            }
            break;
        }
        default:
            LOG(FATAL, COMMON) << "Unknown profile";
    }

    if (type == ProfilingTypeBits::NONE) {
        return compiler::AnyBaseType::UNDEFINED_TYPE;
    }

    if (type == ProfilingTypeBits::BOOLEAN) {
        return compiler::AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE;
    }
    auto accept_special = (type & ProfilingTypeBits::SPECIAL) != 0 || (type & ProfilingTypeBits::BOOLEAN) != 0;
    if (accept_special && IsEqualsInst(bc_inst)) {
        // some instructions treat NaN and undefined or true and 1 differently
        return compiler::AnyBaseType::UNDEFINED_TYPE;
    }
    if ((type & ProfilingTypeBits::SPECIAL_INT) == type) {
        if (accept_special) {
            *allowed_input_type = profiling::AnyInputType::SPECIAL;
        }
        return compiler::AnyBaseType::ECMASCRIPT_INT_TYPE;
    }

    // If object was seen only in one of inputs and we have seen a number as input,
    // build AnyTypeCheck with Double type for this input
    if ((type & ProfilingTypeBits::DOUBLE_INTEGER) != 0) {
        uint8_t allowed_type_mask = 0;
        if (accept_special) {
            allowed_type_mask |= profiling::AnyInputType::SPECIAL;
        }
        if ((type & ProfilingTypeBits::INTEGER) != 0) {
            allowed_type_mask |= profiling::AnyInputType::INTEGER;
        }
        *allowed_input_type = static_cast<profiling::AnyInputType>(allowed_type_mask);
        return compiler::AnyBaseType::ECMASCRIPT_DOUBLE_TYPE;
    }

    return compiler::AnyBaseType::UNDEFINED_TYPE;
}

compiler::AnyBaseType EcmaRuntimeInterface::ResolveSpecialAnyTypeByConstant(coretypes::TaggedValue any_const)
{
    JSTaggedValue js_any_const(any_const);
    ASSERT(js_any_const.IsSpecial());
    if (js_any_const == JSTaggedValue::Hole()) {
        return compiler::AnyBaseType::ECMASCRIPT_HOLE_TYPE;
    }
    if (js_any_const == JSTaggedValue::Undefined()) {
        return compiler::AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE;
    }
    if ((js_any_const == JSTaggedValue::False()) || (js_any_const == JSTaggedValue::True())) {
        return compiler::AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE;
    }
    if (js_any_const == JSTaggedValue::Null()) {
        return compiler::AnyBaseType::ECMASCRIPT_NULL_TYPE;
    }
    return compiler::AnyBaseType::UNDEFINED_TYPE;
}

size_t EcmaRuntimeInterface::GetGlobalConstStringOffsetForAnyType(compiler::AnyBaseType type, Arch arch) const
{
    auto global_const_array_offset =
        cross_values::GetJsthreadGlobalConstantsOffset(arch) + cross_values::GetGlobalConstConstantsOffset(arch);
    switch (type) {
        case compiler::AnyBaseType::ECMASCRIPT_UNDEFINED_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::UNDEFINED_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_STRING_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::STRING_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_INT_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::NUMBER_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_DOUBLE_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::NUMBER_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_SYMBOL_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::SYMBOL_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_CALLABLE_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::FUNCTION_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_BOOLEAN_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::BOOLEAN_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_NULL_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::OBJECT_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_BIGINT_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::BIGINT_STRING_INDEX);
        case compiler::AnyBaseType::ECMASCRIPT_HEAP_OBJECT_TYPE:
            return global_const_array_offset + panda::ecmascript::GlobalEnvConstants::GetGlobalConstantOffset(
                                                   panda::ecmascript::ConstantIndex::OBJECT_STRING_INDEX);
        default:
            UNREACHABLE();
    }
    return 0;
}
RuntimeInterface::NewObjDynInfo EcmaRuntimeInterface::GetNewObjDynInfo(uintptr_t ctor) const
{
    static constexpr NewObjDynInfo SLOW_PATH = {NewObjDynInfo::AllocatorType::SLOW_PATH,
                                                NewObjDynInfo::ResolverType::RESOLVE};
    [[maybe_unused]] ScopedMutatorLock lock;
    JSObject *ctor_ptr = *reinterpret_cast<JSObject **>(ctor);
    if (ctor_ptr == nullptr || !ctor_ptr->IsJSFunction()) {
        return SLOW_PATH;
    }
    auto js_fun = static_cast<JSFunction *>(ctor_ptr);
    if (!js_fun->IsConstructor()) {
        return SLOW_PATH;
    }

    auto method = js_fun->GetMethod();

    if (method->IsNative()) {
        if (js_fun->IsBuiltinConstructor()) {
            return {NewObjDynInfo::AllocatorType::UNDEFINED, NewObjDynInfo::ResolverType::USE_CTOR_RESULT};
        }
        return SLOW_PATH;
    }
    if (!js_fun->IsBase()) {
        if (js_fun->IsDerivedConstructor()) {
            return {NewObjDynInfo::AllocatorType::UNDEFINED, NewObjDynInfo::ResolverType::RESOLVE};
        }
        return SLOW_PATH;
    }
    return {NewObjDynInfo::AllocatorType::ALLOC_OBJ, NewObjDynInfo::ResolverType::RESOLVE};
}

bool EcmaRuntimeInterface::AddProfileInfo(PandaRuntimeInterface::MethodPtr m,
                                          ArenaVector<NamedAccessProfileData> *profile,
                                          ProfileTypeInfo *profile_type_info, uint8_t slot, uintptr_t key)
{
    JSTaggedValue hclass_value = profile_type_info->Get(slot);
    if (!hclass_value.IsHeapObject()) {
        return false;
    }
    auto klass = static_cast<JSHClass *>(hclass_value.GetHeapObject())->GetHClass();
    JSTaggedValue handler = profile_type_info->Get(slot + 1);
    if (UNLIKELY(handler.IsUndefined())) {
        return false;
    }
    auto method = MethodCast(m);
    if (LIKELY(handler.IsInt())) {
        auto handler_info = static_cast<uint32_t>(handler.GetInt());
        if (LIKELY(HandlerBase::IsField(handler_info))) {
            if (HandlerBase::IsArrayOverflowed(handler_info)) {
                return false;
            }
            int index = HandlerBase::GetOffset(handler_info);
            bool is_inlined = HandlerBase::IsInlinedProps(handler_info);
            NamedAccessProfileType type =
                is_inlined ? NamedAccessProfileType::FIELD_INLINED : NamedAccessProfileType::FIELD;
            auto offset = is_inlined ? static_cast<uint32_t>(index * JSTaggedValue::TaggedTypeSize())
                                     : static_cast<uint32_t>(index);
            AddObjectHandle(method, hclass_value.GetHeapObject());
            profile->push_back({klass, 0, key, offset, type});
            return true;
        }
        return false;
    }
    if (handler.IsTransitionHandler()) {
        TransitionHandler *transition_handler = TransitionHandler::Cast(handler.GetTaggedObject());
        JSHClass *new_h_class = JSHClass::Cast(transition_handler->GetTransitionHClass().GetTaggedObject());
        uint32_t handler_info = transition_handler->GetHandlerInfo().GetInt();
        ASSERT(HandlerBase::IsField(handler_info));
        if (HandlerBase::IsArrayOverflowed(handler_info)) {
            return false;
        }
        int index = HandlerBase::GetOffset(handler_info);
        bool is_inlined = HandlerBase::IsInlinedProps(handler_info);
        NamedAccessProfileType type =
            is_inlined ? NamedAccessProfileType::TRANSITION_INLINED : NamedAccessProfileType::TRANSITION;
        auto offset =
            is_inlined ? static_cast<uint32_t>(index * JSTaggedValue::TaggedTypeSize()) : static_cast<uint32_t>(index);
        AddObjectHandle(method, hclass_value.GetHeapObject());
        AddObjectHandle(method, new_h_class);
        profile->push_back({klass, reinterpret_cast<uintptr_t>(new_h_class->GetHClass()), key, offset, type});
        return true;
    }
    if (handler.IsPrototypeHandler()) {
        PrototypeHandler *prototype_handler = PrototypeHandler::Cast(handler.GetTaggedObject());
        auto cell_value = prototype_handler->GetProtoCell();
        if (cell_value.IsFalse()) {
            // For access to undefined property of object without prototype tagged false is stored to proto cell
            return false;
        }
        ASSERT(cell_value.IsProtoChangeMarker());
        ProtoChangeMarker *cell = ProtoChangeMarker::Cast(cell_value.GetHeapObject());
        if (cell->GetHasChanged()) {
            return false;
        }
        JSTaggedValue handler_value = prototype_handler->GetHandlerInfo();
        if (!handler_value.IsInt()) {
            return false;
        }
        uint32_t handler_info = handler_value.GetInt();
        if (!HandlerBase::IsField(handler_info)) {
            return false;
        }
        int index = HandlerBase::GetOffset(handler_info);
        bool is_inlined = HandlerBase::IsInlinedProps(handler_info);
        NamedAccessProfileType type =
            is_inlined ? NamedAccessProfileType::PROTOTYPE_INLINED : NamedAccessProfileType::PROTOTYPE;
        auto offset =
            is_inlined ? static_cast<uint32_t>(index * JSTaggedValue::TaggedTypeSize()) : static_cast<uint32_t>(index);
        auto ref_addr = AddFixedObjectHandle(method, handler.GetHeapObject());
        if (ref_addr == 0) {
            return false;
        }
        AddObjectHandle(method, hclass_value.GetHeapObject());
        profile->push_back({klass, ref_addr, key, offset, type});
        return true;
    }
    return false;
}

bool EcmaRuntimeInterface::AddElementInfo(ArenaVector<NamedAccessProfileData> *profile,
                                          ProfileTypeInfo *profile_type_info, uint8_t slot)
{
    JSTaggedValue hclass_value = profile_type_info->Get(slot);
    if (!hclass_value.IsHeapObject()) {
        return false;
    }
    ASSERT(hclass_value.IsJSHClass());
    auto klass = static_cast<JSHClass *>(hclass_value.GetHeapObject())->GetHClass();
    JSTaggedValue handler = profile_type_info->Get(slot + 1);
    if (UNLIKELY(handler.IsUndefined())) {
        return false;
    }
    ASSERT(handler.IsInt());
    auto handler_info = static_cast<uint32_t>(handler.GetInt());
    // TODO(pishin) support for double
    if (UNLIKELY(!HandlerBase::IsKeyInt(handler_info))) {
        return false;
    }
    if (HandlerBase::IsArrayOverflowed(handler_info)) {
        return false;
    }

    if (HandlerBase::IsJSArray(handler_info)) {
        profile->push_back({klass, 0, 0, 0, NamedAccessProfileType::ARRAY_ELEMENT});
    } else {
        profile->push_back({klass, 0, 0, 0, NamedAccessProfileType::ELEMENT});
    }
    return true;
}

bool EcmaRuntimeInterface::AddProfileValueInfo(PandaRuntimeInterface::MethodPtr m,
                                               ArenaVector<NamedAccessProfileData> *profile,
                                               ProfileTypeInfo *profile_type_info, uint8_t slot)
{
    JSTaggedValue first_value = profile_type_info->Get(slot);
    if (!first_value.IsHeapObject()) {
        return false;
    }
    if (first_value.IsJSHClass()) {
        if (AddElementInfo(profile, profile_type_info, slot)) {
            AddElementInfo(profile, profile_type_info, slot + 2);
            return true;
        }
        return false;
    }
    auto ref_addr = AddFixedObjectHandle(MethodCast(m), first_value.GetHeapObject());
    if (ref_addr == 0) {
        return false;
    }
    return AddProfileInfo(m, profile, profile_type_info, slot + 1, ref_addr);
}

inline ProfileTypeInfo *EcmaRuntimeInterface::GetProfileTypeInfo(PandaRuntimeInterface::MethodPtr m, uintptr_t slot_id,
                                                                 uint8_t *slot) const
{
    // ProfileTypeInfo is manage object
    // We need to have lock in caller method, because we return ProfileTypeInfo
    ASSERT(ecma_vm_->GetMutatorLock()->HasLock());
    uint8_t *mapping = JsMethodCast(m)->GetICMapping();
    if (mapping == nullptr) {
        return nullptr;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    *slot = mapping[slot_id];
    if (auto func = GetJSFunctionByMethod(m); func != nullptr) {
        auto pti = func->GetProfileTypeInfo();
        if (pti.IsHeapObject()) {
            return ProfileTypeInfo::Cast(pti.GetTaggedObject());
        }
    }
    return nullptr;
}

inline ProfileTypeInfo *EcmaRuntimeInterface::GetProfileTypeInfo(uintptr_t func_address, uintptr_t slot_id,
                                                                 uint8_t *slot) const
{
    ASSERT(func_address != 0);
    // ProfileTypeInfo is manage object
    // We need to have lock in caller method, because we return ProfileTypeInfo
    ASSERT(ecma_vm_->GetMutatorLock()->HasLock());
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto js_func = TaggedValue(reinterpret_cast<uint64_t *>(func_address)[0]);
    if (!js_func.IsHeapObject()) {
        return nullptr;
    }
    auto func = JSFunction::Cast(js_func.GetHeapObject());
    uint8_t *mapping = func->GetMethod()->GetICMapping();
    if (mapping == nullptr) {
        return nullptr;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    *slot = mapping[slot_id];
    auto profile_info_obj = func->GetProfileTypeInfo();
    if (profile_info_obj.IsUndefined()) {
        return nullptr;
    }

    return ProfileTypeInfo::Cast(profile_info_obj.GetTaggedObject());
}

template <typename Func>
bool EcmaRuntimeInterface::GetProfileDataForNamedAccessImpl(PandaRuntimeInterface::MethodPtr m, Func func,
                                                            uintptr_t slot_id,
                                                            ArenaVector<NamedAccessProfileData> *profile)
{
    if (profile == nullptr) {
        return false;
    }
    profile->clear();
    ScopedMutatorLock lock;
    uint8_t slot;
    ProfileTypeInfo *profile_type_info = GetProfileTypeInfo(func, slot_id, &slot);
    if (profile_type_info == nullptr) {
        return false;
    }
    if (AddProfileInfo(m, profile, profile_type_info, slot)) {
        AddProfileInfo(m, profile, profile_type_info, slot + 2);
        return true;
    }
    return false;
}

template <typename Func>
bool EcmaRuntimeInterface::GetProfileDataForValueAccessImpl(PandaRuntimeInterface::MethodPtr m, Func func,
                                                            uintptr_t slot_id,
                                                            ArenaVector<NamedAccessProfileData> *profile)
{
    if (profile == nullptr) {
        return false;
    }
    profile->clear();
    ScopedMutatorLock lock;
    uint8_t slot;
    ProfileTypeInfo *profile_type_info = GetProfileTypeInfo(func, slot_id, &slot);
    if (profile_type_info == nullptr) {
        return false;
    }

    return AddProfileValueInfo(m, profile, profile_type_info, slot);
}

void EcmaRuntimeInterface::CleanFunction(Method *method)
{
    os::memory::LockHolder lock(mutex_);

    auto it = js_function_table_.find(method);
    if (it == js_function_table_.end()) {
        return;
    }
    auto func = it->second;
    js_function_table_.erase(it);
    ASSERT(ecma_vm_ != nullptr);
    ecma_vm_->GetGlobalObjectStorage()->Remove(func);
}

size_t EcmaRuntimeInterface::GetLexicalEnvParentEnvIndex() const
{
    return LexicalEnv::PARENT_ENV_INDEX;
}

size_t EcmaRuntimeInterface::GetLexicalEnvStartDataIndex() const
{
    return LexicalEnv::RESERVED_ENV_LENGTH;
}

size_t EcmaRuntimeInterface::GetTaggedArrayElementSize() const
{
    return JSTaggedValue::TaggedTypeSize();
}

void EcmaRuntimeInterface::CleanObjectHandles(Method *method)
{
    os::memory::LockHolder lock(mutex_h_);
    auto it = handles_by_method_table_.find(method);
    auto gos = ecma_vm_->GetGlobalObjectStorage();
    if (it == handles_by_method_table_.end()) {
        return;
    }
    auto vector = it->second;
    // Find handles for the method
    for (auto *ref : *vector) {
        gos->Remove(ref);
    }
    handles_by_method_table_.erase(it);
    internal_allocator_->template Delete(vector);
}

void EcmaRuntimeInterface::AddObjectHandle(Method *method, ObjectHeader *obj)
{
    os::memory::LockHolder lock(mutex_h_);
    auto it = handles_by_method_table_.find(method);
    auto gos = ecma_vm_->GetGlobalObjectStorage();
    // Create new Vector for the method
    if (it == handles_by_method_table_.end()) {
        auto vector = internal_allocator_->template New<PandaVector<panda::mem::Reference *>>();
        auto func_ref = gos->Add(obj, panda::mem::Reference::ObjectType::GLOBAL);
        vector->push_back(func_ref);
        handles_by_method_table_.insert({method, vector});
        return;
    }
    auto vector = it->second;
    // Find the object in the vector
    for (auto *ref : *vector) {
        if (obj == gos->Get(ref)) {
            return;
        }
    }
    auto func_ref = gos->Add(obj, panda::mem::Reference::ObjectType::GLOBAL);
    vector->push_back(func_ref);
}

uintptr_t EcmaRuntimeInterface::AddFixedObjectHandle(Method *method, ObjectHeader *obj)
{
    os::memory::LockHolder lock(mutex_h_);
    auto it = handles_by_method_table_.find(method);
    auto gos = ecma_vm_->GetGlobalObjectStorage();
    // Create new Vector for the method
    if (it == handles_by_method_table_.end()) {
        auto vector = internal_allocator_->template New<PandaVector<panda::mem::Reference *>>();
        auto func_ref = gos->Add(obj, panda::mem::Reference::ObjectType::GLOBAL_FIXED);
        if (func_ref == nullptr) {
            return 0;
        }
        vector->push_back(func_ref);
        handles_by_method_table_.insert({method, vector});
        return gos->GetAddressForRef(func_ref);
    }
    auto vector = it->second;
    // Find the object in the vector
    for (auto *ref : *vector) {
        if (obj == gos->Get(ref)) {
            return gos->GetAddressForRef(ref);
        }
    }
    auto func_ref = gos->Add(obj, panda::mem::Reference::ObjectType::GLOBAL_FIXED);
    if (func_ref == nullptr) {
        return 0;
    }
    vector->push_back(func_ref);
    return gos->GetAddressForRef(func_ref);
}

template <typename Func>
PandaRuntimeInterface::GlobalVarInfo EcmaRuntimeInterface::GetGlobalVarInfoImpl(PandaRuntimeInterface::MethodPtr method,
                                                                                Func func, size_t id,
                                                                                uintptr_t slot_id) const
{
    auto thread = TryGetJSThread();
    if (thread == nullptr) {
        return {};
    }
    ScopedMutatorLock lock;
    auto [dict, entry] = GetGlobalDictionaryEntry(thread, method, id);
    if (entry == -1) {
        return {};
    }
    PropertyAttributes attr = dict->GetAttributes(entry);
    if (attr.IsAccessor()) {
        return {};
    }
    if (!attr.IsConfigurable()) {
        if (!attr.IsWritable()) {
            auto value = dict->GetValue(entry);
            if (!value.IsHeapObject()) {
                return {GlobalVarInfo::Type::CONSTANT, value.GetRawData()};
            }
        }
        JSTaggedValue res(dict->GetSafeBox(entry));
        if (!res.IsHeapObject()) {
            return {};
        }

        ASSERT(res.IsPropertyBox());
        auto address = reinterpret_cast<uintptr_t>(res.GetHeapObject());
        return {GlobalVarInfo::Type::NON_CONFIGURABLE, address};
    }

    uint8_t slot;
    ProfileTypeInfo *profile_type_info = GetProfileTypeInfo(func, slot_id, &slot);
    if (profile_type_info == nullptr) {
        return {};
    }
    JSTaggedValue handler = profile_type_info->Get(slot);
    if (!handler.IsHeapObject()) {
        return {};
    }
    if (PropertyBox::Cast(handler.GetHeapObject())->GetValue().IsHole()) {
        // box was invalidated
        return {};
    }
    auto address = reinterpret_cast<uintptr_t>(handler.GetHeapObject());
    return {GlobalVarInfo::Type::FIELD, address};
}

JSThread *EcmaRuntimeInterface::TryGetJSThread() const
{
    if (ecma_vm_ == nullptr) {
        return nullptr;
    }
    auto thread = ecma_vm_->GetJSThread();
    if (thread == nullptr || !thread->IsThreadAlive()) {
        return nullptr;
    }
    return thread;
}

std::pair<GlobalDictionary *, int> EcmaRuntimeInterface::GetGlobalDictionaryEntry(JSThread *thread, MethodPtr method,
                                                                                  size_t id) const
{
    auto panda_file = JsMethodCast(method)->GetPandaFile();
    JSTaggedValue constant_pool(JSTaggedValue::VALUE_HOLE);

    auto func = [&](Program *p) { constant_pool = p->GetConstantPool(); };
    ecma_vm_->EnumerateProgram(func, panda_file->GetFilename());
    if (constant_pool.IsUndefined()) {
        return {nullptr, -1};
    }

    ASSERT(!constant_pool.IsHole());

    JSTaggedValue key = ConstantPool::Cast(constant_pool.GetHeapObject())->GetObjectFromCache(id);
    auto global_obj = thread->GetGlobalObject();
    auto js_obj = JSObject::Cast(global_obj.GetTaggedObject());
    TaggedArray *array = TaggedArray::Cast(js_obj->GetProperties().GetTaggedObject());
    if (array->GetLength() == 0) {
        return {nullptr, -1};
    }

    auto dict = GlobalDictionary::Cast(array);
    return {dict, dict->FindEntry(key)};
}

JSFunction *EcmaRuntimeInterface::GetJSFunctionByMethod(PandaRuntimeInterface::MethodPtr m) const
{
    // JSFunction is manage object
    // We need to have lock in caller method, because we return JSFunction
    ASSERT(ecma_vm_->GetMutatorLock()->HasLock());
    os::memory::LockHolder lock(mutex_);
    auto it = js_function_table_.find(MethodCast(m));
    if (it == js_function_table_.end()) {
        return nullptr;
    }
    auto func = JSFunction::Cast(ecma_vm_->GetGlobalObjectStorage()->Get(it->second));
    ASSERT(func->GetCallTarget() == MethodCast(m));
    return func;
}

PandaRuntimeInterface::MethodPtr EcmaRuntimeInterface::GetMethodByIdAndSaveJsFunction(
    PandaRuntimeInterface::MethodPtr parent_method, MethodId id)
{
    ScopedMutatorLock lock;
    auto func = GetJSFunctionByMethod(parent_method);
    if (func == nullptr) {
        return nullptr;
    }
    auto constpool = ConstantPool::Cast(func->GetConstantPool().GetHeapObject());
    auto target = JSFunction::Cast(constpool->GetObjectFromCache(id).GetHeapObject());
    auto method = target->GetCallTarget();

    // Store JsFunction for recursive static inlining and futher optimizations
    auto gos = ecma_vm_->GetGlobalObjectStorage();
    auto func_ref = gos->Add(JSTaggedValue(target).GetHeapObject(), panda::mem::Reference::ObjectType::GLOBAL);
    AddFunctionInMap(method, func_ref);

    return method;
}

void *EcmaRuntimeInterface::GetConstantPool(PandaRuntimeInterface::MethodPtr method)
{
    ScopedMutatorLock lock;
    auto func = GetJSFunctionByMethod(method);
    if (func == nullptr) {
        return nullptr;
    }
    return func->GetConstantPool().GetHeapObject();
}

void *EcmaRuntimeInterface::GetConstantPool(uintptr_t func_address)
{
    ASSERT(func_address != 0);
    ScopedMutatorLock lock;
    auto js_func = JSTaggedValue(*reinterpret_cast<JSTaggedType *>(func_address));
    return JSFunction::Cast(js_func.GetHeapObject())->GetConstantPool().GetHeapObject();
}

}  // namespace panda::ecmascript
