/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_object-inl.h"

#include "libpandabase/utils/bit_utils.h"
#include "linked_hash_table-inl.h"
#include "object_factory.h"

namespace panda::ecmascript {
template <typename Derived, typename HashObject>
JSHandle<Derived> LinkedHashTable<Derived, HashObject>::Create(const JSThread *thread, JSType table_type, bool is_weak,
                                                               int number_of_elements)
{
    ASSERT_PRINT(number_of_elements > 0, "size must be a non-negative integer");
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    auto capacity = static_cast<uint32_t>(number_of_elements);
    ASSERT_PRINT(helpers::math::IsPowerOfTwo(capacity), "capacity must be pow of '2'");
    int length = ELEMENTS_START_INDEX + number_of_elements + number_of_elements * (HashObject::ENTRY_SIZE + 1);

    auto table = JSHandle<Derived>(factory->NewLinkedHashTable(length, table_type, is_weak));
    table->SetNumberOfElements(thread, 0);
    table->SetNumberOfDeletedElements(thread, 0);
    table->SetCapacity(thread, capacity);
    return table;
}

template <typename Derived, typename HashObject>
JSHandle<Derived> LinkedHashTable<Derived, HashObject>::Insert(const JSThread *thread, const JSHandle<Derived> &table,
                                                               const JSHandle<JSTaggedValue> &key,
                                                               const JSHandle<JSTaggedValue> &value)
{
    ASSERT(IsKey(key.GetTaggedValue()));
    int hash = LinkedHash::Hash(key.GetTaggedValue());
    int entry = table->FindElement(key.GetTaggedValue(), hash);
    if (entry != -1) {
        table->SetValue(thread, entry, value.GetTaggedValue());
        return table;
    }

    JSHandle<Derived> new_table = GrowCapacity(thread, table);

    int bucket = new_table->HashToBucket(hash);
    entry = new_table->NumberOfElements() + new_table->NumberOfDeletedElements();
    new_table->InsertNewEntry(thread, bucket, entry);
    new_table->SetKey(thread, entry, key.GetTaggedValue());
    // The ENTRY_VALUE_INDEX of LinkedHashSet is 0. SetValue will cause the overwitten key.
    if (std::is_same_v<LinkedHashMap, Derived>) {
        new_table->SetValue(thread, entry, value.GetTaggedValue());
    }
    new_table->SetNumberOfElements(thread, new_table->NumberOfElements() + 1);

    return new_table;
}

template <typename Derived, typename HashObject>
void LinkedHashTable<Derived, HashObject>::Rehash(const JSThread *thread, const JSHandle<Derived> &new_table)
{
    ASSERT_PRINT(*new_table != nullptr && new_table->Capacity() > NumberOfElements(), "can not rehash to new table");
    // Rehash elements to new table
    int number_of_all_elements = NumberOfElements() + NumberOfDeletedElements();
    int des_entry = 0;
    int current_deleted_elements = 0;
    SetNextTable(thread, new_table.GetTaggedValue());
    for (int i = 0; i < number_of_all_elements; i++) {
        int from_index = EntryToIndex(i);
        JSTaggedValue key = GetElement(from_index);
        if (key.IsHole()) {
            // store num_of_deleted_element before entry i; it will be used when iterator update.
            current_deleted_elements++;
            SetDeletedNum(thread, i, JSTaggedValue(current_deleted_elements));
            continue;
        }

        if (key.IsWeak()) {
            // If the key is a weak reference, we use the weak referent to calculate the new index in the new table.
            key.RemoveWeakTag();
        }

        int bucket = new_table->HashToBucket(LinkedHash::Hash(key));
        new_table->InsertNewEntry(thread, bucket, des_entry);
        int des_index = new_table->EntryToIndex(des_entry);
        for (int j = 0; j < HashObject::ENTRY_SIZE; j++) {
            new_table->SetElement(thread, des_index + j, GetElement(from_index + j));
        }
        des_entry++;
    }
    new_table->SetNumberOfElements(thread, NumberOfElements());
    new_table->SetNumberOfDeletedElements(thread, 0);
}

template <typename Derived, typename HashObject>
JSHandle<Derived> LinkedHashTable<Derived, HashObject>::GrowCapacity(const JSThread *thread,
                                                                     const JSHandle<Derived> &table,
                                                                     int number_of_added_elements)
{
    if (table->HasSufficientCapacity(number_of_added_elements)) {
        return table;
    }
    int new_capacity = ComputeCapacity(table->NumberOfElements() + number_of_added_elements);
    auto table_class = table->GetClass();
    JSHandle<Derived> new_table =
        Create(thread, table_class->GetObjectType(), table_class->IsWeakContainer(), new_capacity);
    table->Rehash(thread, new_table);
    return new_table;
}

template <typename Derived, typename HashObject>
JSHandle<Derived> LinkedHashTable<Derived, HashObject>::Remove(const JSThread *thread, const JSHandle<Derived> &table,
                                                               const JSHandle<JSTaggedValue> &key)
{
    ASSERT(IsKey(key.GetTaggedValue()));
    int hash = LinkedHash::Hash(key.GetTaggedValue());
    int entry = table->FindElement(key.GetTaggedValue(), hash);
    if (entry == -1) {
        return table;
    }

    table->RemoveEntry(thread, entry);
    return Shrink(thread, table);
}

template <typename Derived, typename HashObject>
JSHandle<Derived> LinkedHashTable<Derived, HashObject>::Shrink(const JSThread *thread, const JSHandle<Derived> &table,
                                                               int additional_capacity)
{
    int new_capacity = ComputeCapacityWithShrink(table->Capacity(), table->NumberOfElements() + additional_capacity);
    if (new_capacity == table->Capacity()) {
        return table;
    }

    auto table_class = table->GetClass();
    JSHandle<Derived> new_table =
        Create(thread, table_class->GetObjectType(), table_class->IsWeakContainer(), new_capacity);
    if (new_table.IsEmpty()) {
        // No enough memory. Use the origin table.
        return table;
    }

    table->Rehash(thread, new_table);
    return new_table;
}

// LinkedHashMap
JSHandle<LinkedHashMap> LinkedHashMap::Create(const JSThread *thread, bool is_weak, int number_of_elements)
{
    return LinkedHashTable<LinkedHashMap, LinkedHashMapObject>::Create(thread, JSType::LINKED_HASH_MAP, is_weak,
                                                                       number_of_elements);
}

JSHandle<LinkedHashMap> LinkedHashMap::Delete(const JSThread *thread, const JSHandle<LinkedHashMap> &obj,
                                              const JSHandle<JSTaggedValue> &key)
{
    return LinkedHashTable<LinkedHashMap, LinkedHashMapObject>::Remove(thread, obj, key);
}

JSHandle<LinkedHashMap> LinkedHashMap::Set(const JSThread *thread, const JSHandle<LinkedHashMap> &obj,
                                           const JSHandle<JSTaggedValue> &key, const JSHandle<JSTaggedValue> &value)
{
    return LinkedHashTable<LinkedHashMap, LinkedHashMapObject>::Insert(thread, obj, key, value);
}

JSTaggedValue LinkedHashMap::Get(JSTaggedValue key, int hash) const
{
    ASSERT(IsKey(key));
    int entry = FindElement(key, hash);
    if (entry == -1) {
        return JSTaggedValue::Undefined();
    }
    return GetValue(entry);
}

bool LinkedHashMap::Has(JSTaggedValue key, int hash) const
{
    ASSERT(IsKey(key));
    int entry = FindElement(key, hash);
    return entry != -1;
}

void LinkedHashMap::Clear(const JSThread *thread)
{
    int number_of_elements = NumberOfElements() + NumberOfDeletedElements();
    for (int entry = 0; entry < number_of_elements; entry++) {
        SetKey(thread, entry, JSTaggedValue::Hole());
        SetValue(thread, entry, JSTaggedValue::Hole());
    }
    SetNumberOfElements(thread, 0);
    SetNumberOfDeletedElements(thread, number_of_elements);
}

JSHandle<LinkedHashMap> LinkedHashMap::Shrink(const JSThread *thread, const JSHandle<LinkedHashMap> &table,
                                              int additional_capacity)
{
    return LinkedHashTable<LinkedHashMap, LinkedHashMapObject>::Shrink(thread, table, additional_capacity);
}

// LinkedHashSet
JSHandle<LinkedHashSet> LinkedHashSet::Create(const JSThread *thread, bool is_weak, int number_of_elements)
{
    return LinkedHashTable<LinkedHashSet, LinkedHashSetObject>::Create(thread, JSType::LINKED_HASH_SET, is_weak,
                                                                       number_of_elements);
}

JSHandle<LinkedHashSet> LinkedHashSet::Delete(const JSThread *thread, const JSHandle<LinkedHashSet> &obj,
                                              const JSHandle<JSTaggedValue> &key)
{
    return LinkedHashTable<LinkedHashSet, LinkedHashSetObject>::Remove(thread, obj, key);
}

JSHandle<LinkedHashSet> LinkedHashSet::Add(const JSThread *thread, const JSHandle<LinkedHashSet> &obj,
                                           const JSHandle<JSTaggedValue> &key)
{
    return LinkedHashTable<LinkedHashSet, LinkedHashSetObject>::Insert(thread, obj, key, key);
}

bool LinkedHashSet::Has(JSTaggedValue key, int hash) const
{
    ASSERT(IsKey(key));
    int entry = FindElement(key, hash);
    return entry != -1;
}

void LinkedHashSet::Clear(const JSThread *thread)
{
    int number_of_elements = NumberOfElements() + NumberOfDeletedElements();
    for (int entry = 0; entry < number_of_elements; entry++) {
        SetKey(thread, entry, JSTaggedValue::Hole());
    }
    SetNumberOfElements(thread, 0);
    SetNumberOfDeletedElements(thread, number_of_elements);
}

JSHandle<LinkedHashSet> LinkedHashSet::Shrink(const JSThread *thread, const JSHandle<LinkedHashSet> &table,
                                              int additional_capacity)
{
    return LinkedHashTable<LinkedHashSet, LinkedHashSetObject>::Shrink(thread, table, additional_capacity);
}

int LinkedHash::Hash(JSTaggedValue key)
{
    if (key.IsDouble() && key.GetDouble() == 0.0) {
        key = JSTaggedValue(0);
    }
    if (key.IsSymbol()) {
        auto symbol_string = JSSymbol::Cast(key.GetHeapObject());
        return static_cast<JSTaggedNumber>(symbol_string->GetHashField()).GetInt();
    }
    if (key.IsString()) {
        auto key_string = reinterpret_cast<EcmaString *>(key.GetHeapObject());
        return key_string->GetHashcode();
    }
    if (key.IsECMAObject()) {
        int32_t hash = ECMAObject::Cast(key.GetHeapObject())->GetHash();
        if (hash == 0) {
            uint64_t key_value = key.GetRawData();
            hash = GetHash32(reinterpret_cast<uint8_t *>(&key_value), sizeof(key_value) / sizeof(uint8_t));
            ECMAObject::Cast(key.GetHeapObject())->SetHash(hash);
        }
        return hash;
    }

    // Int, Double, Special and HeapObject(except symbol and string)
    uint64_t key_value = key.GetRawData();
    return GetHash32(reinterpret_cast<uint8_t *>(&key_value), sizeof(key_value) / sizeof(uint8_t));
}
}  // namespace panda::ecmascript
