/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description: ecmascript language context
 */

#ifndef PANDA_RUNTIME_ECMASCRIPT_LANGUAGE_CONTEXT_INL_H
#define PANDA_RUNTIME_ECMASCRIPT_LANGUAGE_CONTEXT_INL_H

#include "plugins/ecmascript/runtime/ecma_language_context.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
#include "plugins/ecmascript/runtime/interpreter/js_frame.h"

namespace panda {
inline void EcmaLanguageContext::SetExceptionToVReg([[maybe_unused]] interpreter::AccVRegister &vreg,
                                                    [[maybe_unused]] ObjectHeader *obj) const
{
    ecmascript::JSTaggedValue value(obj);
    if (value.IsObjectWrapper()) {
        value = ecmascript::ObjectWrapper::Cast(value.GetTaggedObject())->GetValue();
    }
    vreg.SetValue(value.GetRawData());
}

inline uint32_t EcmaLanguageContext::GetFrameExtSize() const
{
    return ecmascript::JSExtFrame::GetExtSize();
}
}  // namespace panda

#endif  // PANDA_RUNTIME_ECMASCRIPT_LANGUAGE_CONTEXT_INL_H
