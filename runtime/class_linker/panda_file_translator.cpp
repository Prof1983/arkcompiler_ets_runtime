/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "panda_file_translator.h"

#include <string>
#include <string_view>
#include <vector>

#include "plugins/ecmascript/runtime/class_info_extractor.h"
#include "plugins/ecmascript/runtime/class_linker/program_object-inl.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/ic/profile_type_info.h"
#include "plugins/ecmascript/runtime/interpreter/interpreter.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_thread.h"
#include "plugins/ecmascript/runtime/literal_data_extractor.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_array.h"
#include "plugins/ecmascript/runtime/ic/ic_runtime_stub.h"
#include "libpandabase/mem/mem.h"
#include "libpandabase/utils/logger.h"
#include "libpandabase/utils/utf.h"
#include "libpandafile/bytecode_instruction-inl.h"
#include "libpandafile/class_data_accessor-inl.h"

#include "libpandabase/os/filesystem.h"

namespace panda::ecmascript {
PandaFileTranslator::PandaFileTranslator(EcmaVM *vm)
    : ecma_vm_(vm), factory_(vm->GetFactory()), thread_(vm->GetJSThread())
{
}

JSHandle<Program> PandaFileTranslator::TranslatePandaFile(EcmaVM *vm, const panda_file::File &pf,
                                                          const PandaString &method_name)
{
    PandaFileTranslator translator(vm);
    translator.TranslateClasses(pf, method_name);
    auto result = translator.GenerateProgram(pf);
    return JSHandle<Program>(translator.thread_, result);
}

void PandaFileTranslator::QuickPandaFile(EcmaVM *vm, const panda_file::File &pf)
{
    PandaFileTranslator translator(vm);
    translator.TranslateClasses(pf, "");
}

template <class T, class... Args>
static T *InitializeMemory(T *mem, Args... args)
{
    return new (mem) T(std::forward<Args>(args)...);
}

const JSMethod *PandaFileTranslator::FindMethods(uint32_t offset) const
{
    auto pred = [offset](const JSMethod &method) { return method.GetFileId().GetOffset() == offset; };
    auto it = std::find_if(methods_->begin(), methods_->end(), pred);
    if (it != methods_->end()) {
        return &*it;
    }
    return nullptr;
}

void PandaFileTranslator::TranslateClasses(const panda_file::File &pf, const PandaString &method_name)
{
    Span<const uint32_t> class_indexes = pf.GetClasses();
    methods_ = MakePandaUnique<PandaList<JSMethod>>();

    panda_file::File::StringData sd = {static_cast<uint32_t>(method_name.size()),
                                       reinterpret_cast<const uint8_t *>(method_name.c_str())};

    auto aot_manager = Runtime::GetCurrent()->GetClassLinker()->GetAotManager();
    auto filename = os::GetAbsolutePath(pf.GetFilename());
    auto aot_pfile = aot_manager->FindPandaFile(filename);

    for (const uint32_t index : class_indexes) {
        panda_file::File::EntityId class_id(index);
        if (pf.IsExternal(class_id)) {
            continue;
        }
        panda_file::ClassDataAccessor cda(pf, class_id);
        auto descriptor = cda.GetDescriptor();

        compiler::AotClass aot_class =
            (aot_pfile != nullptr) ? aot_pfile->GetClass(class_id.GetOffset()) : compiler::AotClass::Invalid();

        cda.EnumerateMethods([this, &aot_class, &sd, &pf, &descriptor](panda_file::MethodDataAccessor &mda) {
            TranslateMethod(aot_class, sd, pf, descriptor, mda);
        });
    }
}

void PandaFileTranslator::TranslateMethod(const compiler::AotClass &aot_class, const panda_file::File::StringData &sd,
                                          const panda_file::File &pf, const uint8_t *descriptor,
                                          panda_file::MethodDataAccessor &mda)
{
    auto code_id = mda.GetCodeId();
    ASSERT(code_id.has_value());

    panda_file::CodeDataAccessor code_data_accessor(pf, code_id.value());
    uint32_t code_size = code_data_accessor.GetCodeSize();

    if (main_method_index_ == 0 && pf.GetStringData(mda.GetNameId()) == sd) {
        main_method_index_ = mda.GetMethodId().GetOffset();
    }

    panda_file::ProtoDataAccessor pda(pf, mda.GetProtoId());

    auto *class_linker = Runtime::GetCurrent()->GetClassLinker();
    auto *extension = class_linker->GetExtension(panda::panda_file::SourceLang::ECMASCRIPT);
    auto klass = extension->GetClass(descriptor);
    if (klass == nullptr) {
        // Case for ark_aot. This is workaround.
        klass = class_linker->LoadClass(&pf, descriptor, panda::panda_file::SourceLang::ECMASCRIPT);
    }

    ASSERT(klass != nullptr);  // Very important assert.

    methods_->emplace_back(klass, &pf, mda.GetMethodId(), code_data_accessor.GetCodeId(), mda.GetAccessFlags(),
                           code_data_accessor.GetNumArgs(), reinterpret_cast<const uint16_t *>(pda.GetShorty().Data()));
    JSMethod *method = &methods_->back();

    if (aot_class.IsValid()) {
        auto entry = aot_class.FindMethodCodeEntry(methods_->size() - 1);
        if (entry != nullptr) {
            method->SetCompiledEntryPoint(entry);
        } else {
            method->SetInterpreterEntryPoint();
        }
    } else {
        method->SetInterpreterEntryPoint();
    }

    method->SetCallTypeFromAnnotation();
    method->InitProfileVector();
    const uint8_t *insns = code_data_accessor.GetInstructions();

    if (translated_code_.find(insns) == translated_code_.end()) {
        translated_code_.insert(insns);
        if (pf.GetHeader()->quickened_flag != 0U) {
            TranslateBytecode<true>(code_size, insns, pf, method);
        } else {
            TranslateBytecode<false>(code_size, insns, pf, method);
        }
    }
}

Program *PandaFileTranslator::GenerateProgram(const panda_file::File &pf)
{
    EcmaHandleScope handle_scope(thread_);

    JSHandle<Program> program = factory_->NewProgram();
    JSHandle<EcmaString> location =
        factory_->NewFromStdStringUnCheck(pf.GetFilename(), true, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);

    // +1 for program
    JSHandle<ConstantPool> constpool = factory_->NewConstantPool(constpool_index_ + 1);
    program->SetConstantPool(thread_, constpool.GetTaggedValue());
    program->SetLocation(thread_, location.GetTaggedValue());

    JSHandle<GlobalEnv> env = ecma_vm_->GetGlobalEnv();
    JSHandle<JSHClass> dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithProto());
    JSHandle<JSHClass> normal_dynclass = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithoutProto());
    JSHandle<JSHClass> async_dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncFunctionClass());
    JSHandle<JSHClass> generator_dynclass = JSHandle<JSHClass>::Cast(env->GetGeneratorFunctionClass());
    JSHandle<JSHClass> asyncgenerator_dynclass = JSHandle<JSHClass>::Cast(env->GetAsyncGeneratorFunctionClass());

    for (const auto &it : constpool_map_) {
        ConstPoolValue value(it.second);
        if (value.GetConstpoolType() == ConstPoolType::STRING) {
            panda_file::File::EntityId id(it.first);
            auto found_str = pf.GetStringData(id);
            auto string =
                factory_->GetRawStringFromStringTable(found_str.data, found_str.utf16_length, found_str.is_ascii,
                                                      panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            if (string == nullptr) {
                LOG(FATAL, ECMASCRIPT) << "Not enough memory";
            }
            constpool->Set(thread_, value.GetConstpoolIndex(), JSTaggedValue(string));
        } else if (value.GetConstpoolType() == ConstPoolType::BASE_FUNCTION) {
            ASSERT(main_method_index_ != it.first);
            panda_file::File::EntityId id(it.first);
            auto method = const_cast<JSMethod *>(FindMethods(it.first));
            ASSERT(method != nullptr);

            JSHandle<JSFunction> js_func = factory_->NewJSFunctionByDynClass(
                method, dynclass, FunctionKind::BASE_CONSTRUCTOR, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            constpool->Set(thread_, value.GetConstpoolIndex(), js_func.GetTaggedValue());
            js_func->SetConstantPool(thread_, constpool.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::NC_FUNCTION) {
            ASSERT(main_method_index_ != it.first);
            panda_file::File::EntityId id(it.first);
            auto method = const_cast<JSMethod *>(FindMethods(it.first));
            ASSERT(method != nullptr);

            JSHandle<JSFunction> js_func =
                factory_->NewJSFunctionByDynClass(method, normal_dynclass, FunctionKind::NORMAL_FUNCTION,
                                                  panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            constpool->Set(thread_, value.GetConstpoolIndex(), js_func.GetTaggedValue());
            js_func->SetConstantPool(thread_, constpool.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::GENERATOR_FUNCTION) {
            ASSERT(main_method_index_ != it.first);
            panda_file::File::EntityId id(it.first);
            auto method = const_cast<JSMethod *>(FindMethods(it.first));
            ASSERT(method != nullptr);

            JSHandle<JSFunction> js_func =
                factory_->NewJSFunctionByDynClass(method, generator_dynclass, FunctionKind::GENERATOR_FUNCTION,
                                                  panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            // 26.3.4.3 prototype
            // Whenever a GeneratorFunction instance is created another ordinary object is also created and
            // is the initial value of the generator function's "prototype" property.
            JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
            JSHandle<JSObject> initial_generator_func_prototype = factory_->NewJSObjectByConstructor(
                JSHandle<JSFunction>(obj_fun), obj_fun, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            JSObject::SetPrototype(thread_, initial_generator_func_prototype, env->GetGeneratorPrototype());
            js_func->SetProtoOrDynClass(thread_, initial_generator_func_prototype);

            constpool->Set(thread_, value.GetConstpoolIndex(), js_func.GetTaggedValue());
            js_func->SetConstantPool(thread_, constpool.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::ASYNC_FUNCTION) {
            ASSERT(main_method_index_ != it.first);
            panda_file::File::EntityId id(it.first);
            auto method = const_cast<JSMethod *>(FindMethods(it.first));
            ASSERT(method != nullptr);

            JSHandle<JSFunction> js_func = factory_->NewJSFunctionByDynClass(
                method, async_dynclass, FunctionKind::ASYNC_FUNCTION, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            constpool->Set(thread_, value.GetConstpoolIndex(), js_func.GetTaggedValue());
            js_func->SetConstantPool(thread_, constpool.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::ASYNC_GENERATOR_FUNCTION) {
            ASSERT(main_method_index_ != it.first);
            panda_file::File::EntityId id(it.first);
            auto method = const_cast<JSMethod *>(FindMethods(it.first));
            ASSERT(method != nullptr);

            JSHandle<JSFunction> js_func = factory_->NewJSFunctionByDynClass(
                method, asyncgenerator_dynclass, FunctionKind::ASYNC_GENERATOR_FUNCTION,
                panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            constpool->Set(thread_, value.GetConstpoolIndex(), js_func.GetTaggedValue());
            js_func->SetConstantPool(thread_, constpool.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::CLASS_FUNCTION) {
            ASSERT(main_method_index_ != it.first);
            panda_file::File::EntityId id(it.first);
            auto method = const_cast<JSMethod *>(FindMethods(it.first));
            ASSERT(method != nullptr);
            JSHandle<ClassInfoExtractor> class_info_extractor = factory_->NewClassInfoExtractor(method);
            constpool->Set(thread_, value.GetConstpoolIndex(), class_info_extractor.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::METHOD) {
            ASSERT(main_method_index_ != it.first);
            panda_file::File::EntityId id(it.first);
            auto method = const_cast<JSMethod *>(FindMethods(it.first));
            ASSERT(method != nullptr);

            JSHandle<JSFunction> js_func =
                factory_->NewJSFunctionByDynClass(method, normal_dynclass, FunctionKind::NORMAL_FUNCTION,
                                                  panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
            constpool->Set(thread_, value.GetConstpoolIndex(), js_func.GetTaggedValue());
            js_func->SetConstantPool(thread_, constpool.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::OBJECT_LITERAL) {
            size_t index = it.first;
            JSMutableHandle<TaggedArray> elements(thread_, JSTaggedValue::Undefined());
            JSMutableHandle<TaggedArray> properties(thread_, JSTaggedValue::Undefined());
            LiteralDataExtractor::ExtractObjectDatas(thread_, &pf, index, elements, properties, this);
            JSHandle<JSObject> obj = JSObject::CreateObjectFromProperties(
                thread_, properties, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);

            JSMutableHandle<JSTaggedValue> key(thread_, JSTaggedValue::Undefined());
            JSMutableHandle<JSTaggedValue> value_handle(thread_, JSTaggedValue::Undefined());
            size_t elements_len = elements->GetLength();
            for (size_t i = 0; i < elements_len; i += 2) {  // 2: Each literal buffer contains a pair of key-value.
                key.Update(elements->Get(i));
                if (key->IsHole()) {
                    break;
                }
                value_handle.Update(elements->Get(i + 1));
                JSObject::DefinePropertyByLiteral(thread_, obj, key, value_handle);
            }
            constpool->Set(thread_, value.GetConstpoolIndex(), obj.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::ARRAY_LITERAL) {
            size_t index = it.first;
            JSHandle<TaggedArray> literal =
                LiteralDataExtractor::GetDatasIgnoreType(thread_, &pf, static_cast<size_t>(index));
            uint32_t length = literal->GetLength();

            JSHandle<JSArray> arr(
                JSArray::ArrayCreate(thread_, JSTaggedNumber(length), panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT));
            arr->SetElements(thread_, literal);
            constpool->Set(thread_, value.GetConstpoolIndex(), arr.GetTaggedValue());
        } else if (value.GetConstpoolType() == ConstPoolType::TAGGED_ARRAY) {
            size_t index = it.first;
            JSHandle<TaggedArray> literal =
                LiteralDataExtractor::GetDatasIgnoreType(thread_, &pf, static_cast<size_t>(index), this);
            constpool->Set(thread_, value.GetConstpoolIndex(), literal.GetTaggedValue());
        }
    }
    {
        auto method = const_cast<JSMethod *>(FindMethods(main_method_index_));
        ASSERT(method != nullptr);
        JSHandle<JSFunction> main_func = factory_->NewJSFunctionByDynClass(
            method, dynclass, FunctionKind::BASE_CONSTRUCTOR, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
        main_func->SetConstantPool(thread_, constpool.GetTaggedValue());
        program->SetMainFunction(thread_, main_func.GetTaggedValue());
        program->SetMethodsData(methods_.release());
        // link program
        constpool->Set(thread_, constpool_index_, program.GetTaggedValue());
    }

    DefineClassInConstPool(constpool);
    return *program;
}

template <bool IS_QUICKENED = false>
void PandaFileTranslator::FixInstructionId32(const BytecodeInstruction &inst, [[maybe_unused]] uint32_t index,
                                             uint32_t fix_order) const
{
    using EnumT = std::conditional_t<IS_QUICKENED, BytecodeInstruction::ECMASCRIPT_Opcode, BytecodeInstruction::Opcode>;
    using R = BytecodeInstructionResolver<IS_QUICKENED>;
    constexpr int PAYLOAD_OFFSET = IS_QUICKENED ? 1 : 2;

    // NOLINTNEXTLINE(hicpp-use-auto)
    auto pc = const_cast<uint8_t *>(inst.GetAddress());
    auto format = inst.GetFormat<EnumT>();
    switch (format) {
        case BytecodeInstruction::Format::ID32: {
            uint8_t size = sizeof(uint32_t);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (memcpy_s(pc + FixInstructionIndex::FIX_ONE, size, &index, size) != EOK) {
                LOG_ECMA(FATAL) << "memcpy_s failed";
                UNREACHABLE();
            }
            break;
        }
        case BytecodeInstruction::Format::PREF_ID32: {
            uint8_t size = sizeof(uint32_t);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (memcpy_s(pc + FixInstructionIndex::FIX_TWO, size, &index, size) != EOK) {
                LOG_ECMA(FATAL) << "memcpy_s failed";
                UNREACHABLE();
            }
            break;
        }
        case R::template Get<BytecodeInstruction::Format::PREF_ID16_V8>(): {
            uint8_t size = sizeof(uint16_t);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (memcpy_s(pc + PAYLOAD_OFFSET, size, &index, size) != EOK) {
                LOG_ECMA(FATAL) << "memcpy_s failed";
                UNREACHABLE();
            }
            break;
        }
        // Create RegexpWithLiteral
        case R::template Get<BytecodeInstruction::Format::PREF_ID32_IMM8>():
        /* The following 3 formats matches for Ld/StLexDyn where the string_id should be fixed and the remaining
         * immediates are untouched */
        case R::template Get<BytecodeInstruction::Format::PREF_ID32_IMM4_IMM4>():
        case R::template Get<BytecodeInstruction::Format::PREF_ID32_IMM8_IMM8>():
        case R::template Get<BytecodeInstruction::Format::PREF_ID32_IMM16_IMM16>():
        case R::template Get<BytecodeInstruction::Format::PREF_ID32_V8>():
        case R::template Get<BytecodeInstruction::Format::PREF_ID32_V8_V8>(): {
            uint8_t size = sizeof(uint32_t);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (memcpy_s(pc + PAYLOAD_OFFSET, size, &index, size) != EOK) {
                LOG_ECMA(FATAL) << "memcpy_s failed";
                UNREACHABLE();
            }
            break;
        }
        case R::template Get<BytecodeInstruction::Format::PREF_ID16>():
        case R::template Get<BytecodeInstruction::Format::PREF_IMM16>(): {
            ASSERT(static_cast<uint16_t>(index) == index);
            uint16_t u16_index = index;
            uint8_t size = sizeof(uint16_t);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            if (memcpy_s(pc + PAYLOAD_OFFSET, size, &u16_index, size) != EOK) {
                LOG_ECMA(FATAL) << "memcpy_s failed";
                UNREACHABLE();
            }
            break;
        }
        case R::template Get<BytecodeInstruction::Format::PREF_ID16_IMM16_V8_V8>(): {
            // Usually, we fix one part of instruction one time. But as for instruction DefineClassWithBuffer,
            // which use both method id and literal buffer id.Using fix_order indicates fix Location.
            if (fix_order == 0) {
                uint8_t size = sizeof(uint16_t);
                ASSERT(static_cast<uint16_t>(index) == index);
                // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                if (memcpy_s(pc + PAYLOAD_OFFSET, size, &index, size) != EOK) {
                    LOG_ECMA(FATAL) << "memcpy_s failed";
                    UNREACHABLE();
                }
                break;
            }
            if (fix_order == 1) {
                ASSERT(static_cast<uint16_t>(index) == index);
                uint16_t u16_index = index;
                uint8_t size = sizeof(uint16_t);
                // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                if (memcpy_s(pc + PAYLOAD_OFFSET + 2, size, &u16_index, size) != EOK) {
                    LOG_ECMA(FATAL) << "memcpy_s failed";
                    UNREACHABLE();
                }
                break;
            }
            break;
        }
        default:
            UNREACHABLE();
    }
}

template <bool IS_QUICKENED>
void PandaFileTranslator::TranslateBytecode(uint32_t ins_sz, const uint8_t *ins_arr, const panda_file::File &pf,
                                            const JSMethod *method)
{
    using EnumT = std::conditional_t<IS_QUICKENED, BytecodeInstruction::ECMASCRIPT_Opcode, BytecodeInstruction::Opcode>;
    using R = BytecodeInstructionResolver<IS_QUICKENED>;

    auto bc_ins = BytecodeInstruction(ins_arr);
    auto bc_ins_last = bc_ins.JumpTo(ins_sz);

    while (bc_ins.GetAddress() != bc_ins_last.GetAddress()) {
        if (bc_ins.HasFlag<EnumT>(BytecodeInstruction::Flags::STRING_ID) &&
            BytecodeInstruction::HasId(bc_ins.GetFormat<EnumT>(), 0)) {
            auto index = GetOrInsertConstantPool(ConstPoolType::STRING, bc_ins.GetId<EnumT>().AsFileId().GetOffset());
            FixInstructionId32<IS_QUICKENED>(bc_ins, index);
        } else {
            auto opcode = bc_ins.GetOpcode<EnumT>();
            switch (opcode) {
                uint32_t index;
                uint32_t method_id;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINEFUNCDYN_PREF_ID16_V8>():
                    method_id = pf.ResolveMethodIndex(method->GetFileId(), bc_ins.GetId<EnumT>().AsIndex()).GetOffset();
                    index = GetOrInsertConstantPool(ConstPoolType::BASE_FUNCTION, method_id);
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINENCFUNCDYN_PREF_ID16_V8>():
                    method_id = pf.ResolveMethodIndex(method->GetFileId(), bc_ins.GetId<EnumT>().AsIndex()).GetOffset();
                    index = GetOrInsertConstantPool(ConstPoolType::NC_FUNCTION, method_id);
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINEGENERATORFUNC_PREF_ID16_V8>():
                    method_id = pf.ResolveMethodIndex(method->GetFileId(), bc_ins.GetId<EnumT>().AsIndex()).GetOffset();
                    index = GetOrInsertConstantPool(ConstPoolType::GENERATOR_FUNCTION, method_id);
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINEASYNCFUNC_PREF_ID16_V8>():
                    method_id = pf.ResolveMethodIndex(method->GetFileId(), bc_ins.GetId<EnumT>().AsIndex()).GetOffset();
                    index = GetOrInsertConstantPool(ConstPoolType::ASYNC_FUNCTION, method_id);
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINEASYNCGENERATORFUNC_PREF_ID16_V8>():
                    method_id = pf.ResolveMethodIndex(method->GetFileId(), bc_ins.GetId<EnumT>().AsIndex()).GetOffset();
                    index = GetOrInsertConstantPool(ConstPoolType::ASYNC_GENERATOR_FUNCTION, method_id);
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINEMETHOD_PREF_ID16_V8>():
                    method_id = pf.ResolveMethodIndex(method->GetFileId(), bc_ins.GetId<EnumT>().AsIndex()).GetOffset();
                    index = GetOrInsertConstantPool(ConstPoolType::METHOD, method_id);
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_CREATEOBJECTWITHBUFFER_PREF_ID16>():
                case R::template Get<BytecodeInstruction::Opcode::ECMA_CREATEOBJECTHAVINGMETHOD_PREF_ID16>():
                    index = GetOrInsertConstantPool(ConstPoolType::OBJECT_LITERAL,
                                                    bc_ins.GetId<EnumT>().AsFileId().GetOffset());
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_CREATEARRAYWITHBUFFER_PREF_ID16>():
                    index = GetOrInsertConstantPool(ConstPoolType::ARRAY_LITERAL,
                                                    bc_ins.GetId<EnumT>().AsFileId().GetOffset());
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                case R::template Get<BytecodeInstruction::Opcode::ECMA_LDEVALBINDINGS_PREF_ID16>():
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINECLASSPRIVATEFIELDS_PREF_ID16_V8>(): {
                    index = GetOrInsertConstantPool(ConstPoolType::TAGGED_ARRAY,
                                                    bc_ins.GetId<EnumT>().AsFileId().GetOffset());
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    break;
                }
                case R::template Get<BytecodeInstruction::Opcode::ECMA_DEFINECLASSWITHBUFFER_PREF_ID16_IMM16_V8_V8>():
                    method_id = pf.ResolveMethodIndex(method->GetFileId(), bc_ins.GetId<EnumT>().AsIndex()).GetOffset();
                    index = GetOrInsertConstantPool(ConstPoolType::CLASS_FUNCTION, method_id);
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index);
                    index = GetOrInsertConstantPool(
                        ConstPoolType::TAGGED_ARRAY,
                        bc_ins.GetImm<R::template Get<BytecodeInstruction::Format::PREF_ID16_IMM16_V8_V8>()>());
                    FixInstructionId32<IS_QUICKENED>(bc_ins, index, 1);
                    break;
                default:
                    break;
            }
        }
        UpdateICOffset<IS_QUICKENED>(const_cast<JSMethod *>(method), ins_sz, bc_ins);
        bc_ins = bc_ins.GetNext<EnumT>();
    }
}

template <bool IS_QUICKENED>
void PandaFileTranslator::UpdateICOffset(JSMethod *method, uint32_t ins_sz, const BytecodeInstruction &inst) const
{
    using EnumT = std::conditional_t<IS_QUICKENED, BytecodeInstruction::ECMASCRIPT_Opcode, BytecodeInstruction::Opcode>;
    using R = BytecodeInstructionResolver<IS_QUICKENED>;

    auto opcode = inst.GetOpcode<EnumT>();
    uint32_t bc_offset = inst.GetAddress() - method->GetInstructions();
    uint32_t slot_size = 0;
    using ICMappingType = JSMethod::ICMappingType;
    static_assert(std::is_pointer<ICMappingType>::value);
    using ICMappingElementType = std::remove_pointer<ICMappingType>::type;
    ICMappingType ic_mapping = method->GetICMapping();

    if (method->GetSlotSize() > std::numeric_limits<ICMappingElementType>::max()) {
        if (ic_mapping != nullptr) {
            mem::InternalAllocator<>::GetInternalAllocatorFromRuntime()->Free(ic_mapping);
            method->SetICMapping(nullptr);
        }
        return;
    }
    switch (opcode) {
        case R::template Get<BytecodeInstruction::Opcode::ECMA_TRYLDGLOBALBYNAME_PREF_ID32>():
        case R::template Get<BytecodeInstruction::Opcode::ECMA_TRYSTGLOBALBYNAME_PREF_ID32>():
        case R::template Get<BytecodeInstruction::Opcode::ECMA_LDGLOBALVAR_PREF_ID32>():
        case R::template Get<BytecodeInstruction::Opcode::ECMA_STGLOBALVAR_PREF_ID32>():
            slot_size = ICRuntimeStub::SlotSizeForGlobalICByName();
            break;
        case R::template Get<BytecodeInstruction::Opcode::ECMA_LDOBJBYVALUE_PREF_V8>():
        case R::template Get<BytecodeInstruction::Opcode::ECMA_STOBJBYVALUE_PREF_V8_V8>():
        case R::template Get<BytecodeInstruction::Opcode::ECMA_STOWNBYVALUE_PREF_V8_V8>():
            slot_size = ICRuntimeStub::SlotSizeForICByValue();
            break;
        case R::template Get<BytecodeInstruction::Opcode::ECMA_LDOBJBYNAME_PREF_ID32>():
        case R::template Get<BytecodeInstruction::Opcode::ECMA_STOBJBYNAME_PREF_ID32_V8>():
        case R::template Get<BytecodeInstruction::Opcode::ECMA_STOWNBYNAME_PREF_ID32_V8>():
            slot_size = ICRuntimeStub::SlotSizeForICByName();
            break;
        default:
            return;
    }

    if (ic_mapping == nullptr && ins_sz < ProfileTypeInfo::MAX_FUNCTION_SIZE) {
        ic_mapping =
            mem::InternalAllocator<>::GetInternalAllocatorFromRuntime()->AllocArray<ICMappingElementType>(ins_sz);
        memset_s(ic_mapping, ins_sz, 0, ins_sz);
        method->SetICMapping(ic_mapping);
    }
    if (ic_mapping != nullptr) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        ic_mapping[bc_offset] = method->GetSlotSize();
        method->AddSlotSize(slot_size);
    }
}

uint32_t PandaFileTranslator::GetOrInsertConstantPool(ConstPoolType type, uint32_t offset)
{
    auto it = constpool_map_.find(offset);
    if (it != constpool_map_.cend()) {
        ConstPoolValue value(it->second);
        return value.GetConstpoolIndex();
    }
    ASSERT(constpool_index_ != UINT32_MAX);
    uint32_t index = constpool_index_++;
    ConstPoolValue value(type, index);
    constpool_map_.insert({offset, value.GetValue()});
    return index;
}

JSHandle<JSFunction> PandaFileTranslator::DefineMethodInLiteral(uint32_t method_id, FunctionKind kind) const
{
    auto method = const_cast<JSMethod *>(FindMethods(method_id));
    ASSERT(method != nullptr);

    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSHClass> function_class;
    if (kind == FunctionKind::NORMAL_FUNCTION) {
        function_class = JSHandle<JSHClass>::Cast(env->GetFunctionClassWithoutProto());
    } else {
        function_class = JSHandle<JSHClass>::Cast(env->GetGeneratorFunctionClass());
    }
    JSHandle<JSFunction> js_func = factory_->NewJSFunctionByDynClass(method, function_class, kind,
                                                                     panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);

    if (kind == FunctionKind::GENERATOR_FUNCTION) {
        JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
        JSHandle<JSObject> initial_generator_func_prototype = factory_->NewJSObjectByConstructor(
            JSHandle<JSFunction>(obj_fun), obj_fun, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
        JSObject::SetPrototype(thread_, initial_generator_func_prototype, env->GetGeneratorPrototype());
        js_func->SetProtoOrDynClass(thread_, initial_generator_func_prototype);
    } else if (kind == FunctionKind::ASYNC_GENERATOR_FUNCTION) {
        JSHandle<JSTaggedValue> obj_fun = env->GetObjectFunction();
        JSHandle<JSObject> initial_async_generator_func_prototype = factory_->NewJSObjectByConstructor(
            JSHandle<JSFunction>(obj_fun), obj_fun, panda::SpaceType::SPACE_TYPE_NON_MOVABLE_OBJECT);
        JSObject::SetPrototype(thread_, initial_async_generator_func_prototype, env->GetAsyncGeneratorPrototype());
        js_func->SetProtoOrDynClass(thread_, initial_async_generator_func_prototype);
    }
    js_func->SetupFunctionLength(thread_);
    return js_func;
}

void PandaFileTranslator::DefineClassInConstPool(const JSHandle<ConstantPool> &constpool) const
{
    uint32_t length = constpool->GetLength();
    uint32_t index = 0;
    while (index < length - 1) {
        JSTaggedValue value = constpool->Get(index);
        if (!value.IsClassInfoExtractor()) {
            index++;
            continue;
        }

        // Here, using a law: when inserting ctor in index of constantpool, the index + 1 location will be inserted by
        // corresponding class literal. Because translator fixes ECMA_DEFINECLASSWITHBUFFER two consecutive times.
        JSTaggedValue next_value = constpool->Get(index + 1);
        ASSERT(next_value.IsTaggedArray());

        JSHandle<ClassInfoExtractor> extractor(thread_, value);
        JSHandle<TaggedArray> literal(thread_, next_value);
        ClassInfoExtractor::BuildClassInfoExtractorFromLiteral(thread_, extractor, literal);
        JSHandle<JSFunction> cls = ClassHelper::DefineClassTemplate(thread_, extractor, constpool);
        constpool->Set(thread_, index, cls);
        index += 2;  // 2: pair of extractor and literal
    }
}
}  // namespace panda::ecmascript
