/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ECMASCRIPT_CLASS_LINKER_PROGRAM_H
#define ECMASCRIPT_CLASS_LINKER_PROGRAM_H

#include "plugins/ecmascript/runtime/ecma_macros.h"
#include "plugins/ecmascript/runtime/js_tagged_value-inl.h"

namespace panda::ecmascript {
class JSThread;

class Program : public ECMAObject {
public:
    DECL_CAST(Program)

    ACCESSORS_BASE(ECMAObject)
    ACCESSORS(0, Location)
    ACCESSORS(1, ConstantPool)
    ACCESSORS(2, MainFunction)
    ACCESSORS_NATIVE_FIELD(3, MethodsData, PandaList<JSMethod>)
    ACCESSORS_FINISH(4)

    inline void FreeMethodData();

    DECL_DUMP()
};

class LexicalFunction : public ECMAObject {
public:
    DECL_CAST(LexicalFunction)

    uint32_t GetNumVregs() const
    {
        return static_cast<uint32_t>(GetNumberVRegs().GetInt());
    }

    uint32_t GetNumICSlots() const
    {
        return static_cast<uint32_t>(GetNumberICSlots().GetInt());
    }

    inline const uint8_t *GetInstructions() const;

    ACCESSORS_BASE(ECMAObject)
    ACCESSORS(0, Name)
    ACCESSORS(1, NumberVRegs)
    ACCESSORS(2, NumberICSlots)
    ACCESSORS(3, Bytecode)
    ACCESSORS(4, Program)
    ACCESSORS_FINISH(5)

    DECL_DUMP()
};

class ConstantPool : public TaggedArray {
public:
    static ConstantPool *Cast(ObjectHeader *object)
    {
        ASSERT(JSTaggedValue(object).IsTaggedArray());
        return static_cast<ConstantPool *>(object);
    }

    static ConstantPool *Cast(JSTaggedType cp)
    {
        ASSERT(JSTaggedValue(cp).IsTaggedArray());
        return static_cast<ConstantPool *>(JSTaggedValue(cp).GetHeapObject());
    }

    inline JSTaggedValue GetObjectFromCache(uint32_t index) const;
    DECL_DUMP()
};
}  // namespace panda::ecmascript
#endif  // ECMASCRIPT_CLASS_LINKER_PROGRAM_H
