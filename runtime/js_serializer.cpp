/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_serializer.h"

#include <malloc.h>
#include <vector>

#include "plugins/ecmascript/runtime/base/array_helper.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_arraybuffer.h"
#include "plugins/ecmascript/runtime/js_hclass.h"
#include "plugins/ecmascript/runtime/js_regexp.h"
#include "plugins/ecmascript/runtime/js_set.h"
#include "plugins/ecmascript/runtime/js_typed_array.h"
#include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
#include "libpandabase/mem/mem.h"
#include "securec.h"

namespace panda::ecmascript {
constexpr size_t INITIAL_CAPACITY = 64;
constexpr int CAPACITY_INCREASE_RATE = 2;

bool JSSerializer::WriteType(SerializationUID id)
{
    auto raw_id = static_cast<uint8_t>(id);
    return WriteRawData(&raw_id, sizeof(raw_id));
}

// Write JSTaggedValue could be either a pointer to a HeapObject or a value
bool JSSerializer::SerializeJSTaggedValue(const JSHandle<JSTaggedValue> &value)
{
    [[maybe_unused]] EcmaHandleScope scope(thread_);
    if (!value->IsHeapObject()) {
        if (!WritePrimitiveValue(value)) {
            return false;
        }
    } else {
        if (!WriteTaggedObject(value)) {
            return false;
        }
    }
    return true;
}

// Write JSTaggedValue that is pure value
bool JSSerializer::WritePrimitiveValue(const JSHandle<JSTaggedValue> &value)
{
    if (value->IsNull()) {
        return WriteType(SerializationUID::JS_NULL);
    }
    if (value->IsUndefined()) {
        return WriteType(SerializationUID::JS_UNDEFINED);
    }
    if (value->IsTrue()) {
        return WriteType(SerializationUID::JS_TRUE);
    }
    if (value->IsFalse()) {
        return WriteType(SerializationUID::JS_FALSE);
    }
    if (value->IsInt()) {
        return WriteInt(value->GetInt());
    }
    if (value->IsDouble()) {
        return WriteDouble(value->GetDouble());
    }
    if (value->IsHole()) {
        return WriteType(SerializationUID::HOLE);
    }
    return false;
}

bool JSSerializer::WriteInt(int32_t value)
{
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::INT32)) {
        return false;
    }
    if (!WriteRawData(&value, sizeof(value))) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteInt(uint32_t value)
{
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::UINT32)) {
        return false;
    }
    if (!WriteRawData(&value, sizeof(value))) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteDouble(double value)
{
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::DOUBLE)) {
        return false;
    }
    if (!WriteRawData(&value, sizeof(value))) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteBoolean(bool value)
{
    if (value) {
        return WriteType(SerializationUID::C_TRUE);
    }
    return WriteType(SerializationUID::C_FALSE);
}

// Write length for marking how many bytes should be read
bool JSSerializer::WriteLength(uint32_t length)
{
    return WriteRawData(&length, sizeof(length));
}

bool JSSerializer::WriteRawData(const void *data, size_t length)
{
    if (length <= 0) {
        return false;
    }
    if ((buffer_size_ + length) > buffer_capacity_) {
        if (!AllocateBuffer(length)) {
            return false;
        }
    }
    errno_t rc;
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    rc = memcpy_s(buffer_ + buffer_size_, buffer_capacity_ - buffer_size_, data, length);
    if (rc != EOK) {
        LOG(ERROR, RUNTIME) << "Failed to memcpy_s Data";
        return false;
    }
    buffer_size_ += length;
    return true;
}

bool JSSerializer::AllocateBuffer(size_t bytes)
{
    // Get internal heap size
    if (size_limit_ == 0) {
        uint64_t heap_size = thread_->GetEcmaVM()->GetJSOptions().GetInternalMemorySizeLimit();
        size_limit_ = heap_size;
    }
    size_t old_size = buffer_size_;
    size_t new_size = old_size + bytes;
    if (new_size > size_limit_) {
        return false;
    }
    if (buffer_capacity_ == 0) {
        if (bytes < INITIAL_CAPACITY) {
            // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
            buffer_ = reinterpret_cast<uint8_t *>(malloc(INITIAL_CAPACITY));
            if (buffer_ != nullptr) {
                buffer_capacity_ = INITIAL_CAPACITY;
                return true;
            }
            return false;
        }
        // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
        buffer_ = reinterpret_cast<uint8_t *>(malloc(bytes));
        if (buffer_ != nullptr) {
            buffer_capacity_ = bytes;
            return true;
        }
        return false;
    }
    if (new_size > buffer_capacity_) {
        if (!ExpandBuffer(new_size)) {
            return false;
        }
    }
    return true;
}

bool JSSerializer::ExpandBuffer([[maybe_unused]] size_t requested_size)
{
    size_t new_capacity = buffer_capacity_ * CAPACITY_INCREASE_RATE;
    new_capacity = std::max(new_capacity, requested_size);
    if (new_capacity > size_limit_) {
        return false;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
    auto *new_buffer = reinterpret_cast<uint8_t *>(malloc(new_capacity));
    if (new_buffer == nullptr) {
        return false;
    }
    errno_t rc;
    rc = memcpy_s(new_buffer, new_capacity, buffer_, buffer_size_);
    if (rc != EOK) {
        LOG(ERROR, RUNTIME) << "Failed to memcpy_s Data";
        // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
        free(new_buffer);
        return false;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
    free(buffer_);
    buffer_ = new_buffer;
    buffer_capacity_ = new_capacity;
    return true;
}

// Transfer ownership of buffer, should not use this Serializer after release
std::pair<uint8_t *, size_t> JSSerializer::ReleaseBuffer()
{
    auto res = std::make_pair(buffer_, buffer_size_);
    buffer_ = nullptr;
    buffer_size_ = 0;
    buffer_capacity_ = 0;
    object_id_ = 0;
    return res;
}

bool JSSerializer::IsSerialized(uintptr_t addr) const
{
    return reference_map_.find(addr) != reference_map_.end();
}

bool JSSerializer::WriteIfSerialized(uintptr_t addr)
{
    size_t old_size = buffer_size_;
    auto iter = reference_map_.find(addr);
    if (iter == reference_map_.end()) {
        return false;
    }
    uint64_t id = iter->second;
    if (!WriteType(SerializationUID::TAGGED_OBJECT_REFERNCE)) {
        return false;
    }
    if (!WriteRawData(&id, sizeof(uint64_t))) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

// Write HeapObject
bool JSSerializer::WriteTaggedObject(const JSHandle<JSTaggedValue> &value)
{
    uintptr_t addr = reinterpret_cast<uintptr_t>(value.GetTaggedValue().GetTaggedObject());
    bool serialized = IsSerialized(addr);
    if (serialized) {
        return WriteIfSerialized(addr);
    }
    reference_map_.insert(std::pair(addr, object_id_));
    object_id_++;

    TaggedObject *tagged_object = value->GetTaggedObject();
    JSType type = tagged_object->GetClass()->GetObjectType();
    switch (type) {
        case JSType::JS_ERROR:
        case JSType::JS_EVAL_ERROR:
        case JSType::JS_RANGE_ERROR:
        case JSType::JS_REFERENCE_ERROR:
        case JSType::JS_TYPE_ERROR:
        case JSType::JS_URI_ERROR:
        case JSType::JS_SYNTAX_ERROR:
            return WriteJSError(value);
        case JSType::JS_DATE:
            return WriteJSDate(value);
        case JSType::JS_ARRAY:
            return WriteJSArray(value);
        case JSType::JS_MAP:
            return WriteJSMap(value);
        case JSType::JS_SET:
            return WriteJSSet(value);
        case JSType::JS_REG_EXP:
            return WriteJSRegExp(value);
        case JSType::JS_INT8_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_INT8_ARRAY);
        case JSType::JS_UINT8_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_UINT8_ARRAY);
        case JSType::JS_UINT8_CLAMPED_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_UINT8_CLAMPED_ARRAY);
        case JSType::JS_INT16_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_INT16_ARRAY);
        case JSType::JS_UINT16_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_UINT16_ARRAY);
        case JSType::JS_INT32_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_INT32_ARRAY);
        case JSType::JS_UINT32_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_UINT32_ARRAY);
        case JSType::JS_FLOAT32_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_FLOAT32_ARRAY);
        case JSType::JS_FLOAT64_ARRAY:
            return WriteJSTypedArray(value, SerializationUID::JS_FLOAT64_ARRAY);
        case JSType::JS_ARRAY_BUFFER:
            return WriteJSArrayBuffer(value);
        case JSType::STRING:
            return WriteEcmaString(value);
        case JSType::JS_OBJECT:
            return WritePlainObject(value);
        default:
            break;
    }
    return false;
}

bool JSSerializer::WriteJSError(const JSHandle<JSTaggedValue> &value)
{
    size_t old_size = buffer_size_;
    TaggedObject *tagged_object = value->GetTaggedObject();
    JSType error_type = tagged_object->GetClass()->GetObjectType();
    if (!WriteJSErrorHeader(error_type)) {
        return false;
    }
    auto global_const = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> handle_msg = global_const->GetHandledMessageString();
    JSHandle<JSTaggedValue> msg = JSObject::GetProperty(thread_, value, handle_msg).GetValue();
    // Write error message
    if (!SerializeJSTaggedValue(msg)) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteJSErrorHeader(JSType type)
{
    switch (type) {
        case JSType::JS_ERROR:
            return WriteType(SerializationUID::JS_ERROR);
        case JSType::JS_EVAL_ERROR:
            return WriteType(SerializationUID::EVAL_ERROR);
        case JSType::JS_RANGE_ERROR:
            return WriteType(SerializationUID::RANGE_ERROR);
        case JSType::JS_REFERENCE_ERROR:
            return WriteType(SerializationUID::REFERENCE_ERROR);
        case JSType::JS_TYPE_ERROR:
            return WriteType(SerializationUID::TYPE_ERROR);
        case JSType::JS_URI_ERROR:
            return WriteType(SerializationUID::URI_ERROR);
        case JSType::JS_SYNTAX_ERROR:
            return WriteType(SerializationUID::SYNTAX_ERROR);
        default:
            UNREACHABLE();
    }
    return false;
}

bool JSSerializer::WriteJSDate(const JSHandle<JSTaggedValue> &value)
{
    JSHandle<JSDate> date = JSHandle<JSDate>::Cast(value);
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::JS_DATE)) {
        return false;
    }
    if (!WritePlainObject(value)) {
        buffer_size_ = old_size;
        return false;
    }
    double time_value = date->GetTimeValue().GetDouble();
    if (!WriteDouble(time_value)) {
        buffer_size_ = old_size;
        return false;
    }
    double local_offset = date->GetLocalOffset().GetDouble();
    if (!WriteDouble(local_offset)) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteJSArray(const JSHandle<JSTaggedValue> &value)
{
    JSHandle<JSArray> array = JSHandle<JSArray>::Cast(value);
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::JS_ARRAY)) {
        return false;
    }
    if (!WritePlainObject(value)) {
        buffer_size_ = old_size;
        return false;
    }
    uint32_t array_length = static_cast<uint32_t>(array->GetLength().GetInt());
    if (!WriteInt(array_length)) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteEcmaString(const JSHandle<JSTaggedValue> &value)
{
    JSHandle<EcmaString> string = JSHandle<EcmaString>::Cast(value);
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::ECMASTRING)) {
        return false;
    }
    size_t length = string->GetLength();
    if (!WriteLength(static_cast<uint32_t>(length))) {
        buffer_size_ = old_size;
        return false;
    }
    const uint8_t *data = string->GetDataUtf8();
    const uint8_t str_end = '\0';
    if (!WriteRawData(data, length) || !WriteRawData(&str_end, sizeof(uint8_t))) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteJSMap(const JSHandle<JSTaggedValue> &value)
{
    JSHandle<JSMap> map = JSHandle<JSMap>::Cast(value);
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::JS_MAP)) {
        return false;
    }
    if (!WritePlainObject(value)) {
        buffer_size_ = old_size;
        return false;
    }
    int size = map->GetSize();
    if (!WriteLength(static_cast<uint32_t>(size))) {
        buffer_size_ = old_size;
        return false;
    }
    for (int i = 0; i < size; i++) {
        JSHandle<JSTaggedValue> key(thread_, map->GetKey(i));
        if (!SerializeJSTaggedValue(key)) {
            buffer_size_ = old_size;
            return false;
        }
        JSHandle<JSTaggedValue> val(thread_, map->GetValue(i));
        if (!SerializeJSTaggedValue(val)) {
            buffer_size_ = old_size;
            return false;
        }
    }
    return true;
}

bool JSSerializer::WriteJSSet(const JSHandle<JSTaggedValue> &value)
{
    JSHandle<JSSet> set = JSHandle<JSSet>::Cast(value);
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::JS_SET)) {
        return false;
    }
    if (!WritePlainObject(value)) {
        buffer_size_ = old_size;
        return false;
    }
    int size = set->GetSize();
    if (!WriteLength(static_cast<uint32_t>(size))) {
        buffer_size_ = old_size;
        return false;
    }
    for (int i = 0; i < size; i++) {
        JSHandle<JSTaggedValue> val(thread_, set->GetValue(i));
        if (!SerializeJSTaggedValue(val)) {
            buffer_size_ = old_size;
            return false;
        }
    }
    return true;
}

bool JSSerializer::WriteJSRegExp(const JSHandle<JSTaggedValue> &value)
{
    JSHandle<JSRegExp> reg_exp = JSHandle<JSRegExp>::Cast(value);
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::JS_REG_EXP)) {
        return false;
    }
    if (!WritePlainObject(value)) {
        buffer_size_ = old_size;
        return false;
    }
    uint32_t buffer_size = static_cast<uint32_t>(reg_exp->GetLength().GetInt());
    if (!WriteLength(buffer_size)) {
        buffer_size_ = old_size;
        return false;
    }
    // Write Accessor(ByteCodeBuffer) which is a pointer to a Dynbuffer
    JSHandle<JSTaggedValue> buffer_value(thread_, reg_exp->GetByteCodeBuffer());
    JSHandle<JSNativePointer> np = JSHandle<JSNativePointer>::Cast(buffer_value);
    void *dyn_buffer = np->GetExternalPointer();
    if (!WriteRawData(dyn_buffer, buffer_size)) {
        buffer_size_ = old_size;
        return false;
    }
    // Write Accessor(OriginalSource)
    JSHandle<JSTaggedValue> original_source(thread_, reg_exp->GetOriginalSource());
    if (!SerializeJSTaggedValue(original_source)) {
        buffer_size_ = old_size;
        return false;
    }
    // Write Accessor(OriginalFlags)
    JSHandle<JSTaggedValue> original_flags(thread_, reg_exp->GetOriginalFlags());
    if (!SerializeJSTaggedValue(original_flags)) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteJSTypedArray(const JSHandle<JSTaggedValue> &value, SerializationUID u_id)
{
    JSHandle<JSTypedArray> typed_array = JSHandle<JSTypedArray>::Cast(value);
    size_t old_size = buffer_size_;
    if (!WriteType(u_id)) {
        return false;
    }
    if (!WritePlainObject(value)) {
        buffer_size_ = old_size;
        return false;
    }
    // Write ACCESSORS(ViewedArrayBuffer) which is a pointer to an ArrayBuffer
    JSHandle<JSTaggedValue> viewed_array_buffer(thread_, typed_array->GetViewedArrayBuffer());
    if (!WriteJSArrayBuffer(viewed_array_buffer)) {
        buffer_size_ = old_size;
        return false;
    }
    // Write ACCESSORS(TypedArrayName)
    JSHandle<JSTaggedValue> typed_array_name(thread_, typed_array->GetTypedArrayName());
    if (!SerializeJSTaggedValue(typed_array_name)) {
        buffer_size_ = old_size;
        return false;
    }
    // Write ACCESSORS(ByteLength)
    JSTaggedValue byte_length = typed_array->GetByteLength();
    if (!WriteRawData(&byte_length, sizeof(JSTaggedValue))) {
        buffer_size_ = old_size;
        return false;
    }
    // Write ACCESSORS(ByteOffset)
    JSTaggedValue byte_offset = typed_array->GetByteOffset();
    if (!WriteRawData(&byte_offset, sizeof(JSTaggedValue))) {
        buffer_size_ = old_size;
        return false;
    }
    // Write ACCESSORS(ArrayLength)
    JSTaggedValue array_length = typed_array->GetArrayLength();
    if (!WriteRawData(&array_length, sizeof(JSTaggedValue))) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteNativeFunctionPointer(const JSHandle<JSTaggedValue> &value)
{
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::NATIVE_FUNCTION_POINTER)) {
        return false;
    }
    JSTaggedValue pointer = value.GetTaggedValue();
    if (!WriteRawData(&pointer, sizeof(JSTaggedValue))) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

bool JSSerializer::WriteJSArrayBuffer(const JSHandle<JSTaggedValue> &value)
{
    size_t old_size = buffer_size_;
    JSHandle<JSArrayBuffer> array_buffer = JSHandle<JSArrayBuffer>::Cast(value);

    if (array_buffer->IsDetach()) {
        return false;
    }

    if (!WriteType(SerializationUID::JS_ARRAY_BUFFER)) {
        return false;
    }

    // Write Accessors(ArrayBufferByteLength)
    JSTaggedValue tagged_length = array_buffer->GetArrayBufferByteLength();
    if (!WriteRawData(&tagged_length, sizeof(JSTaggedValue))) {
        buffer_size_ = old_size;
        return false;
    }

    // write Accessor shared which indicate the C memeory is shared
    bool shared = array_buffer->GetShared().ToBoolean();
    if (!WriteBoolean(shared)) {
        buffer_size_ = old_size;
        return false;
    }

    if (shared) {
        JSHandle<JSNativePointer> np(thread_, array_buffer->GetArrayBufferData());
        void *buffer = np->GetExternalPointer();
        auto buffer_addr = reinterpret_cast<uint64_t>(buffer);
        if (!WriteRawData(&buffer_addr, sizeof(uint64_t))) {
            buffer_size_ = old_size;
            return false;
        }
    } else {
        uint32_t byte_length = JSTaggedNumber(tagged_length).ToUint32();
        // Write Accessors(ArrayBufferData) which is a pointer to a DynBuffer
        JSHandle<JSNativePointer> np(thread_, array_buffer->GetArrayBufferData());
        void *buffer = np->GetExternalPointer();
        if (!WriteRawData(buffer, byte_length)) {
            buffer_size_ = old_size;
            return false;
        }
    }

    // write obj properties
    if (!WritePlainObject(value)) {
        buffer_size_ = old_size;
        return false;
    }

    return true;
}

bool JSSerializer::WritePlainObject(const JSHandle<JSTaggedValue> &obj_value)
{
    JSHandle<JSObject> obj = JSHandle<JSObject>::Cast(obj_value);
    size_t old_size = buffer_size_;
    if (!WriteType(SerializationUID::JS_PLAIN_OBJECT)) {
        return false;
    }
    // Get the number of elements stored in obj
    uint32_t elements_length = obj->GetNumberOfElements();
    if (!WriteLength(elements_length)) {
        buffer_size_ = old_size;
        return false;
    }
    std::vector<JSTaggedValue> key_vector;
    JSObject::GetALLElementKeysIntoVector(thread_, obj, key_vector);
    // Write elements' description attributes and value
    if (key_vector.size() != elements_length) {
        buffer_size_ = old_size;
        return false;
    }
    for (uint32_t i = 0; i < elements_length; i++) {
        JSHandle<JSTaggedValue> key(thread_, key_vector[i]);
        if (!SerializeJSTaggedValue(key)) {
            buffer_size_ = old_size;
            return false;
        }
        PropertyDescriptor desc(thread_);
        JSObject::OrdinaryGetOwnProperty(thread_, obj, key, desc);
        if (!WriteDesc(desc)) {
            buffer_size_ = old_size;
            return false;
        }
        JSHandle<JSTaggedValue> value = desc.GetValue();
        if (!SerializeJSTaggedValue(value)) {
            buffer_size_ = old_size;
            return false;
        }
    }
    // Get the number of k-v form properties stored in obj
    key_vector.clear();
    uint32_t properties_length = obj->GetNumberOfKeys();
    if (!WriteLength(properties_length)) {
        buffer_size_ = old_size;
        return false;
    }
    JSObject::GetAllKeys(thread_, obj, key_vector);
    if (key_vector.size() != properties_length) {
        buffer_size_ = old_size;
        return false;
    }
    // Write keys' description attributes and related values
    for (uint32_t i = 0; i < properties_length; i++) {
        if (key_vector.empty()) {
            buffer_size_ = old_size;
            return false;
        }
        JSHandle<JSTaggedValue> key(thread_, key_vector[i]);
        if (!SerializeJSTaggedValue(key)) {
            buffer_size_ = old_size;
            return false;
        }
        PropertyDescriptor desc(thread_);
        JSObject::OrdinaryGetOwnProperty(thread_, obj, key, desc);
        if (!WriteDesc(desc)) {
            buffer_size_ = old_size;
            return false;
        }
        JSHandle<JSTaggedValue> value = desc.GetValue();
        if (!SerializeJSTaggedValue(value)) {
            buffer_size_ = old_size;
            return false;
        }
    }
    return true;
}

bool JSSerializer::WriteDesc(const PropertyDescriptor &desc)
{
    size_t old_size = buffer_size_;
    bool is_writable = desc.IsWritable();
    if (!WriteBoolean(is_writable)) {
        buffer_size_ = old_size;
        return false;
    }
    bool is_enumerable = desc.IsEnumerable();
    if (!WriteBoolean(is_enumerable)) {
        buffer_size_ = old_size;
        return false;
    }
    bool is_configurable = desc.IsConfigurable();
    if (!WriteBoolean(is_configurable)) {
        buffer_size_ = old_size;
        return false;
    }
    bool has_writable = desc.HasWritable();
    if (!WriteBoolean(has_writable)) {
        buffer_size_ = old_size;
        return false;
    }
    bool has_enumerable = desc.HasEnumerable();
    if (!WriteBoolean(has_enumerable)) {
        buffer_size_ = old_size;
        return false;
    }
    bool has_configurable = desc.HasConfigurable();
    if (!WriteBoolean(has_configurable)) {
        buffer_size_ = old_size;
        return false;
    }
    return true;
}

SerializationUID JSDeserializer::ReadType()
{
    SerializationUID uid;
    if (position_ >= end_) {
        return SerializationUID::UNKNOWN;
    }
    uid = static_cast<SerializationUID>(*position_);
    if (uid < SerializationUID::JS_NULL || uid > SerializationUID::NATIVE_FUNCTION_POINTER) {
        return SerializationUID::UNKNOWN;
    }
    position_++;  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return uid;
}

bool JSDeserializer::ReadInt(int32_t *value)
{
    size_t len = sizeof(int32_t);
    if (len > static_cast<size_t>(end_ - position_)) {
        return false;
    }
    if (memcpy_s(value, len, position_, len) != EOK) {
        UNREACHABLE();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    position_ += len;
    return true;
}

bool JSDeserializer::ReadInt(uint32_t *value)
{
    size_t len = sizeof(uint32_t);
    if (len > static_cast<size_t>(end_ - position_)) {
        return false;
    }
    if (memcpy_s(value, len, position_, len) != EOK) {
        UNREACHABLE();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    position_ += len;
    return true;
}

bool JSDeserializer::ReadObjectId(uint64_t *object_id)
{
    size_t len = sizeof(uint64_t);
    if (len > static_cast<size_t>(end_ - position_)) {
        return false;
    }
    if (memcpy_s(object_id, len, position_, len) != EOK) {
        UNREACHABLE();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    position_ += len;
    return true;
}

bool JSDeserializer::ReadDouble(double *value)
{
    size_t len = sizeof(double);
    if (len > static_cast<size_t>(end_ - position_)) {
        return false;
    }
    if (memcpy_s(value, len, position_, len) != EOK) {
        UNREACHABLE();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    position_ += len;
    return true;
}

JSDeserializer::~JSDeserializer()
{
    // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
    free(begin_);
    begin_ = nullptr;
}

JSHandle<JSTaggedValue> JSDeserializer::DeserializeJSTaggedValue()
{
    SerializationUID uid = ReadType();
    if (uid == SerializationUID::UNKNOWN) {
        return JSHandle<JSTaggedValue>();
    }
    switch (uid) {
        case SerializationUID::JS_NULL:
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Null());
        case SerializationUID::JS_UNDEFINED:
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Undefined());
        case SerializationUID::JS_TRUE:
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue::True());
        case SerializationUID::JS_FALSE:
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue::False());
        case SerializationUID::HOLE:
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue::Hole());
        case SerializationUID::INT32: {
            int32_t value;
            if (!ReadInt(&value)) {
                return JSHandle<JSTaggedValue>();
            }
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue(value));
        }
        case SerializationUID::UINT32: {
            uint32_t value;
            if (!ReadInt(&value)) {
                return JSHandle<JSTaggedValue>();
            }
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue(value));
        }
        case SerializationUID::DOUBLE: {
            double value;
            if (!ReadDouble(&value)) {
                return JSHandle<JSTaggedValue>();
            }
            return JSHandle<JSTaggedValue>(thread_, JSTaggedValue(value));
        }
        case SerializationUID::JS_ERROR:
        case SerializationUID::EVAL_ERROR:
        case SerializationUID::RANGE_ERROR:
        case SerializationUID::REFERENCE_ERROR:
        case SerializationUID::TYPE_ERROR:
        case SerializationUID::URI_ERROR:
        case SerializationUID::SYNTAX_ERROR:
            return ReadJSError(uid);
        case SerializationUID::JS_DATE:
            return ReadJSDate();
        case SerializationUID::JS_PLAIN_OBJECT:
            return ReadPlainObject();
        case SerializationUID::JS_ARRAY:
            return ReadJSArray();
        case SerializationUID::ECMASTRING:
            return ReadEcmaString();
        case SerializationUID::JS_MAP:
            return ReadJSMap();
        case SerializationUID::JS_SET:
            return ReadJSSet();
        case SerializationUID::JS_REG_EXP:
            return ReadJSRegExp();
        case SerializationUID::JS_INT8_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_INT8_ARRAY);
        case SerializationUID::JS_UINT8_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_UINT8_ARRAY);
        case SerializationUID::JS_UINT8_CLAMPED_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_UINT8_CLAMPED_ARRAY);
        case SerializationUID::JS_INT16_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_INT16_ARRAY);
        case SerializationUID::JS_UINT16_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_UINT16_ARRAY);
        case SerializationUID::JS_INT32_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_INT32_ARRAY);
        case SerializationUID::JS_UINT32_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_UINT32_ARRAY);
        case SerializationUID::JS_FLOAT32_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_FLOAT32_ARRAY);
        case SerializationUID::JS_FLOAT64_ARRAY:
            return ReadJSTypedArray(SerializationUID::JS_FLOAT64_ARRAY);
        case SerializationUID::NATIVE_FUNCTION_POINTER:
            return ReadNativeFunctionPointer();
        case SerializationUID::JS_ARRAY_BUFFER:
            return ReadJSArrayBuffer();
        case SerializationUID::TAGGED_OBJECT_REFERNCE:
            return ReadReference();
        default:
            return JSHandle<JSTaggedValue>();
    }
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSError(SerializationUID uid)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    base::ErrorType error_type;
    switch (uid) {
        case SerializationUID::JS_ERROR:
            error_type = base::ErrorType::ERROR;
            break;
        case SerializationUID::EVAL_ERROR:
            error_type = base::ErrorType::EVAL_ERROR;
            break;
        case SerializationUID::RANGE_ERROR:
            error_type = base::ErrorType::RANGE_ERROR;
            break;
        case SerializationUID::REFERENCE_ERROR:
            error_type = base::ErrorType::REFERENCE_ERROR;
            break;
        case SerializationUID::TYPE_ERROR:
            error_type = base::ErrorType::TYPE_ERROR;
            break;
        case SerializationUID::URI_ERROR:
            error_type = base::ErrorType::URI_ERROR;
            break;
        case SerializationUID::SYNTAX_ERROR:
            error_type = base::ErrorType::URI_ERROR;
            break;
        default:
            UNREACHABLE();
    }
    JSHandle<JSTaggedValue> msg = DeserializeJSTaggedValue();
    JSHandle<EcmaString> handle_msg(msg);
    JSHandle<JSTaggedValue> error_tag = JSHandle<JSTaggedValue>::Cast(factory->NewJSError(error_type, handle_msg));
    reference_map_.insert(std::pair(object_id_++, error_tag));
    return error_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSDate()
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> date_function = env->GetDateFunction();
    JSHandle<JSDate> date =
        JSHandle<JSDate>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(date_function), date_function));
    JSHandle<JSTaggedValue> date_tag = JSHandle<JSTaggedValue>::Cast(date);
    reference_map_.insert(std::pair(object_id_++, date_tag));
    if (!JudgeType(SerializationUID::JS_PLAIN_OBJECT) || !DefinePropertiesAndElements(date_tag)) {
        return JSHandle<JSTaggedValue>();
    }
    double time_value;
    if (!JudgeType(SerializationUID::DOUBLE) || !ReadDouble(&time_value)) {
        return JSHandle<JSTaggedValue>();
    }
    date->SetTimeValue(thread_, JSTaggedValue(time_value));
    double local_offset;
    if (!JudgeType(SerializationUID::DOUBLE) || !ReadDouble(&local_offset)) {
        return JSHandle<JSTaggedValue>();
    }
    date->SetLocalOffset(thread_, JSTaggedValue(local_offset));
    return date_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSArray()
{
    JSHandle<JSArray> js_array = thread_->GetEcmaVM()->GetFactory()->NewJSArray();
    JSHandle<JSTaggedValue> array_tag = JSHandle<JSTaggedValue>::Cast(js_array);
    reference_map_.insert(std::pair(object_id_++, array_tag));
    if (!JudgeType(SerializationUID::JS_PLAIN_OBJECT) || !DefinePropertiesAndElements(array_tag)) {
        return JSHandle<JSTaggedValue>();
    }
    uint32_t arr_length;
    if (!JudgeType(SerializationUID::UINT32) || !ReadInt(&arr_length)) {
        return JSHandle<JSTaggedValue>();
    }
    js_array->SetLength(thread_, JSTaggedValue(arr_length));
    return array_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadEcmaString()
{
    uint32_t string_length;
    if (!ReadInt(&string_length)) {
        return JSHandle<JSTaggedValue>();
    }
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    auto *string = reinterpret_cast<uint8_t *>(GetBuffer(string_length + 1));
    if (string == nullptr) {
        return JSHandle<JSTaggedValue>();
    }

    JSHandle<EcmaString> ecma_string = factory->NewFromUtf8(string, string_length);
    auto string_tag = JSHandle<JSTaggedValue>(ecma_string);
    reference_map_.insert(std::pair(object_id_++, string_tag));
    return string_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadPlainObject()
{
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> obj_func(thread_, env->GetObjectFunction().GetObject<JSFunction>());
    JSHandle<JSObject> js_object =
        thread_->GetEcmaVM()->GetFactory()->NewJSObjectByConstructor(JSHandle<JSFunction>(obj_func), obj_func);
    JSHandle<JSTaggedValue> obj_tag = JSHandle<JSTaggedValue>::Cast(js_object);
    reference_map_.insert(std::pair(object_id_++, obj_tag));
    if (!DefinePropertiesAndElements(obj_tag)) {
        return JSHandle<JSTaggedValue>();
    }
    return obj_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSMap()
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> map_function = env->GetMapFunction();
    JSHandle<JSMap> js_map =
        JSHandle<JSMap>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(map_function), map_function));
    JSHandle<JSTaggedValue> map_tag = JSHandle<JSTaggedValue>::Cast(js_map);
    reference_map_.insert(std::pair(object_id_++, map_tag));
    if (!JudgeType(SerializationUID::JS_PLAIN_OBJECT) || !DefinePropertiesAndElements(map_tag)) {
        return JSHandle<JSTaggedValue>();
    }
    uint32_t size;
    if (!ReadInt(&size)) {
        return JSHandle<JSTaggedValue>();
    }
    JSHandle<LinkedHashMap> linked_map = LinkedHashMap::Create(thread_);
    js_map->SetLinkedMap(thread_, linked_map);
    for (uint32_t i = 0; i < size; i++) {
        JSHandle<JSTaggedValue> key = DeserializeJSTaggedValue();
        if (key.IsEmpty()) {
            return JSHandle<JSTaggedValue>();
        }
        JSHandle<JSTaggedValue> value = DeserializeJSTaggedValue();
        if (value.IsEmpty()) {
            return JSHandle<JSTaggedValue>();
        }
        JSMap::Set(thread_, js_map, key, value);
    }
    return map_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSSet()
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> set_function = env->GetSetFunction();
    JSHandle<JSSet> js_set =
        JSHandle<JSSet>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(set_function), set_function));
    JSHandle<JSTaggedValue> set_tag = JSHandle<JSTaggedValue>::Cast(js_set);
    reference_map_.insert(std::pair(object_id_++, set_tag));
    if (!JudgeType(SerializationUID::JS_PLAIN_OBJECT) || !DefinePropertiesAndElements(set_tag)) {
        return JSHandle<JSTaggedValue>();
    }
    uint32_t size;
    if (!ReadInt(&size)) {
        return JSHandle<JSTaggedValue>();
    }
    JSHandle<LinkedHashSet> linked_set = LinkedHashSet::Create(thread_);
    js_set->SetLinkedSet(thread_, linked_set);
    for (uint32_t i = 0; i < size; i++) {
        JSHandle<JSTaggedValue> key = DeserializeJSTaggedValue();
        if (key.IsEmpty()) {
            return JSHandle<JSTaggedValue>();
        }
        JSSet::Add(thread_, js_set, key);
    }
    return set_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSRegExp()
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> regexp_function = env->GetRegExpFunction();
    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(regexp_function), regexp_function);
    JSHandle<JSRegExp> reg_exp = JSHandle<JSRegExp>::Cast(obj);
    JSHandle<JSTaggedValue> regexp_tag = JSHandle<JSTaggedValue>::Cast(reg_exp);
    reference_map_.insert(std::pair(object_id_++, regexp_tag));
    if (!JudgeType(SerializationUID::JS_PLAIN_OBJECT) || !DefinePropertiesAndElements(regexp_tag)) {
        return JSHandle<JSTaggedValue>();
    }
    uint32_t buffer_size;
    if (!ReadInt(&buffer_size)) {
        return JSHandle<JSTaggedValue>();
    }
    void *buffer = GetBuffer(buffer_size);
    if (buffer == nullptr) {
        return JSHandle<JSTaggedValue>();
    }
    factory->NewJSRegExpByteCodeData(reg_exp, buffer, buffer_size);
    JSHandle<JSTaggedValue> original_source = DeserializeJSTaggedValue();
    reg_exp->SetOriginalSource(thread_, original_source);
    JSHandle<JSTaggedValue> original_flags = DeserializeJSTaggedValue();
    reg_exp->SetOriginalFlags(thread_, original_flags);
    return regexp_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSTypedArray(SerializationUID uid)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread_->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSTaggedValue> target;
    JSHandle<JSObject> obj;
    JSHandle<JSTaggedValue> obj_tag;
    switch (uid) {
        case SerializationUID::JS_INT8_ARRAY: {
            target = env->GetInt8ArrayFunction();
            break;
        }
        case SerializationUID::JS_UINT8_ARRAY: {
            target = env->GetUint8ArrayFunction();
            break;
        }
        case SerializationUID::JS_UINT8_CLAMPED_ARRAY: {
            target = env->GetUint8ClampedArrayFunction();
            break;
        }
        case SerializationUID::JS_INT16_ARRAY: {
            target = env->GetInt16ArrayFunction();
            break;
        }
        case SerializationUID::JS_UINT16_ARRAY: {
            target = env->GetUint16ArrayFunction();
            break;
        }
        case SerializationUID::JS_INT32_ARRAY: {
            target = env->GetInt32ArrayFunction();
            break;
        }
        case SerializationUID::JS_UINT32_ARRAY: {
            target = env->GetUint32ArrayFunction();
            break;
        }
        case SerializationUID::JS_FLOAT32_ARRAY: {
            target = env->GetFloat32ArrayFunction();
            break;
        }
        case SerializationUID::JS_FLOAT64_ARRAY: {
            target = env->GetFloat64ArrayFunction();
            break;
        }
        default:
            UNREACHABLE();
    }
    JSHandle<JSTypedArray> typed_array =
        JSHandle<JSTypedArray>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(target), target));
    obj = JSHandle<JSObject>::Cast(typed_array);
    obj_tag = JSHandle<JSTaggedValue>::Cast(obj);
    reference_map_.insert(std::pair(object_id_++, obj_tag));
    if (!JudgeType(SerializationUID::JS_PLAIN_OBJECT) || !DefinePropertiesAndElements(obj_tag)) {
        return JSHandle<JSTaggedValue>();
    }

    JSHandle<JSTaggedValue> viewed_array_buffer = DeserializeJSTaggedValue();
    if (viewed_array_buffer.IsEmpty()) {
        return JSHandle<JSTaggedValue>();
    }
    typed_array->SetViewedArrayBuffer(thread_, viewed_array_buffer);

    JSHandle<JSTaggedValue> typed_array_name = DeserializeJSTaggedValue();
    if (typed_array_name.IsEmpty()) {
        return JSHandle<JSTaggedValue>();
    }
    typed_array->SetTypedArrayName(thread_, typed_array_name);

    JSTaggedValue byte_length;
    if (!ReadJSTaggedValue(&byte_length) || !byte_length.IsNumber()) {
        return JSHandle<JSTaggedValue>();
    }
    typed_array->SetByteLength(thread_, byte_length);

    JSTaggedValue byte_offset;
    if (!ReadJSTaggedValue(&byte_offset) || !byte_offset.IsNumber()) {
        return JSHandle<JSTaggedValue>();
    }
    typed_array->SetByteOffset(thread_, byte_offset);

    JSTaggedValue array_length;
    if (!ReadJSTaggedValue(&array_length) || !byte_offset.IsNumber()) {
        return JSHandle<JSTaggedValue>();
    }
    typed_array->SetArrayLength(thread_, array_length);
    return obj_tag;
}

JSHandle<JSTaggedValue> JSDeserializer::ReadNativeFunctionPointer()
{
    JSTaggedValue pointer;
    if (!ReadJSTaggedValue(&pointer)) {
        return JSHandle<JSTaggedValue>();
    }
    return JSHandle<JSTaggedValue>(thread_, pointer);
}

JSHandle<JSTaggedValue> JSDeserializer::ReadJSArrayBuffer()
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    // read access length
    JSTaggedValue tagged_length;
    if (!ReadJSTaggedValue(&tagged_length)) {
        return JSHandle<JSTaggedValue>();
    }
    // read access shared
    bool shared = false;
    if (!ReadBoolean(&shared)) {
        return JSHandle<JSTaggedValue>();
    }
    // create jsarraybuffer
    JSHandle<JSTaggedValue> array_buffer_tag;
    uint32_t byte_length = JSTaggedNumber(tagged_length).ToUint32();
    if (shared) {
        auto *buffer_addr = static_cast<uint64_t *>(GetBuffer(sizeof(uint64_t)));
        void *buffer_data = ToVoidPtr(*buffer_addr);
        JSHandle<JSArrayBuffer> array_buffer = factory->NewJSArrayBuffer(buffer_data, byte_length, nullptr, nullptr);
        array_buffer_tag = JSHandle<JSTaggedValue>::Cast(array_buffer);
        reference_map_.insert(std::pair(object_id_++, array_buffer_tag));
    } else {
        void *from_buffer = GetBuffer(byte_length);
        if (from_buffer == nullptr) {
            return array_buffer_tag;
        }
        JSHandle<JSArrayBuffer> array_buffer = factory->NewJSArrayBuffer(byte_length);
        array_buffer_tag = JSHandle<JSTaggedValue>::Cast(array_buffer);
        reference_map_.insert(std::pair(object_id_++, array_buffer_tag));
        JSHandle<JSNativePointer> np(thread_, array_buffer->GetArrayBufferData());
        void *to_buffer = np->GetExternalPointer();
        if (memcpy_s(to_buffer, byte_length, from_buffer, byte_length) != EOK) {
            UNREACHABLE();
        }
    }
    // read jsarraybuffer properties
    if (!JudgeType(SerializationUID::JS_PLAIN_OBJECT) || !DefinePropertiesAndElements(array_buffer_tag)) {
        return JSHandle<JSTaggedValue>();
    }

    return array_buffer_tag;
}

bool JSDeserializer::ReadJSTaggedValue(JSTaggedValue *value)
{
    size_t len = sizeof(JSTaggedValue);
    if (len > static_cast<size_t>(end_ - position_)) {
        return false;
    }
    if (memcpy_s(value, len, position_, len) != EOK) {
        UNREACHABLE();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    position_ += len;
    return true;
}

void *JSDeserializer::GetBuffer(uint32_t buffer_size)
{
    const uint8_t *buffer = nullptr;
    if (buffer_size > static_cast<size_t>(end_ - position_)) {
        return nullptr;
    }
    buffer = position_;
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    position_ += buffer_size;
    auto *ret_buffer = const_cast<uint8_t *>(buffer);
    return static_cast<void *>(ret_buffer);
}

JSHandle<JSTaggedValue> JSDeserializer::ReadReference()
{
    uint64_t obj_id;
    if (!ReadObjectId(&obj_id)) {
        return JSHandle<JSTaggedValue>();
    }
    auto obj_iter = reference_map_.find(obj_id);
    if (obj_iter == reference_map_.end()) {
        return JSHandle<JSTaggedValue>();
    }
    return obj_iter->second;
}

bool JSDeserializer::JudgeType(SerializationUID target_uid)
{
    return ReadType() != target_uid;
}

bool JSDeserializer::DefinePropertiesAndElements(const JSHandle<JSTaggedValue> &obj)
{
    uint32_t element_length;
    if (!ReadInt(&element_length)) {
        return false;
    }
    for (uint32_t i = 0; i < element_length; i++) {
        JSHandle<JSTaggedValue> key = DeserializeJSTaggedValue();
        if (key.IsEmpty()) {
            return false;
        }
        PropertyDescriptor desc(thread_);
        if (!ReadDesc(&desc)) {
            return false;
        }
        JSHandle<JSTaggedValue> value = DeserializeJSTaggedValue();
        if (value.IsEmpty()) {
            return false;
        }
        desc.SetValue(value);
        if (!JSTaggedValue::DefineOwnProperty(thread_, obj, key, desc)) {
            return false;
        }
    }

    uint32_t property_length;
    if (!ReadInt(&property_length)) {
        return false;
    }
    for (uint32_t i = 0; i < property_length; i++) {
        JSHandle<JSTaggedValue> key = DeserializeJSTaggedValue();
        if (key.IsEmpty()) {
            return false;
        }
        PropertyDescriptor desc(thread_);
        if (!ReadDesc(&desc)) {
            return false;
        }
        JSHandle<JSTaggedValue> value = DeserializeJSTaggedValue();
        if (value.IsEmpty()) {
            return false;
        }
        desc.SetValue(value);
        if (!JSTaggedValue::DefineOwnProperty(thread_, obj, key, desc)) {
            return false;
        }
    }
    return true;
}

bool JSDeserializer::ReadDesc(PropertyDescriptor *desc)
{
    bool is_writable = false;
    if (!ReadBoolean(&is_writable)) {
        return false;
    }
    bool is_enumerable = false;
    if (!ReadBoolean(&is_enumerable)) {
        return false;
    }
    bool is_configurable = false;
    if (!ReadBoolean(&is_configurable)) {
        return false;
    }
    bool has_writable = false;
    if (!ReadBoolean(&has_writable)) {
        return false;
    }
    bool has_enumerable = false;
    if (!ReadBoolean(&has_enumerable)) {
        return false;
    }
    bool has_configurable = false;
    if (!ReadBoolean(&has_configurable)) {
        return false;
    }
    if (has_writable) {
        desc->SetWritable(is_writable);
    }
    if (has_enumerable) {
        desc->SetEnumerable(is_enumerable);
    }
    if (has_configurable) {
        desc->SetConfigurable(is_configurable);
    }
    return true;
}

bool JSDeserializer::ReadBoolean(bool *value)
{
    SerializationUID uid = ReadType();
    if (uid == SerializationUID::C_TRUE) {
        *value = true;
        return true;
    }
    if (uid == SerializationUID::C_FALSE) {
        *value = false;
        return true;
    }
    return false;
}

bool Serializer::WriteValue(JSThread *thread, const JSHandle<JSTaggedValue> &value,
                            const JSHandle<JSTaggedValue> &transfer)
{
    if (data_ != nullptr) {
        return false;
    }
    // NOLINTNEXTLINE(modernize-make-unique)
    data_.reset(new SerializationData);
    if (!PrepareTransfer(thread, transfer)) {
        return false;
    }
    if (!value_serializer_.SerializeJSTaggedValue(value)) {
        return false;
    }
    if (!FinalizeTransfer(thread, transfer)) {
        return false;
    }
    std::pair<uint8_t *, size_t> pair = value_serializer_.ReleaseBuffer();
    data_->value_.reset(pair.first);
    data_->data_size_ = pair.second;
    return true;
}

std::unique_ptr<SerializationData> Serializer::Release()
{
    return std::move(data_);
}

bool Serializer::PrepareTransfer(JSThread *thread, const JSHandle<JSTaggedValue> &transfer)
{
    if (transfer->IsUndefined()) {
        return true;
    }
    if (!transfer->IsJSArray()) {
        return false;
    }
    auto len = static_cast<int32_t>(base::ArrayHelper::GetArrayLength(thread, transfer));
    int32_t k = 0;
    while (k < len) {
        bool exists = JSTaggedValue::HasProperty(thread, transfer, k);
        if (exists) {
            JSHandle<JSTaggedValue> element = JSArray::FastGetPropertyByValue(thread, transfer, k);
            if (!element->IsArrayBuffer()) {
                return false;
            }
            array_buffer_idxs_.emplace_back(k);
        }
        k++;
    }
    return true;
}

bool Serializer::FinalizeTransfer(JSThread *thread, const JSHandle<JSTaggedValue> &transfer)
{
    for (int idx : array_buffer_idxs_) {
        JSHandle<JSTaggedValue> element = JSArray::FastGetPropertyByValue(thread, transfer, idx);
        JSArrayBuffer::Cast(element->GetHeapObject())->Detach(thread);
    }
    return true;
}

JSHandle<JSTaggedValue> Deserializer::ReadValue()
{
    return value_deserializer_.DeserializeJSTaggedValue();
}
}  // namespace panda::ecmascript
