/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/js_tagged_value.h"
#include "js_map.h"
#include "linked_hash_table-inl.h"
#include "object_factory.h"
#include "utils/bit_utils.h"

namespace panda::ecmascript {
void JSMap::Set(JSThread *thread, const JSHandle<JSMap> &map, const JSHandle<JSTaggedValue> &key,
                const JSHandle<JSTaggedValue> &value)
{
    if (!LinkedHashMap::IsKey(key.GetTaggedValue())) {
        THROW_TYPE_ERROR(thread, "the value must be Key of JSSet");
    }
    JSHandle<LinkedHashMap> map_handle(thread, LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject()));

    JSHandle<LinkedHashMap> new_map = LinkedHashMap::Set(thread, map_handle, key, value);
    map->SetLinkedMap(thread, new_map);
}

bool JSMap::Delete(const JSThread *thread, const JSHandle<JSMap> &map, const JSHandle<JSTaggedValue> &key)
{
    JSHandle<LinkedHashMap> map_handle(thread, LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject()));
    if (!LinkedHashMap::IsKey(key.GetTaggedValue())) {
        return false;
    }
    int hash = LinkedHash::Hash(key.GetTaggedValue());
    int entry = map_handle->FindElement(key.GetTaggedValue(), hash);
    if (entry == -1) {
        return false;
    }
    map_handle->RemoveEntry(thread, entry);

    JSHandle<LinkedHashMap> new_map = LinkedHashMap::Shrink(thread, map_handle);
    map->SetLinkedMap(thread, new_map);
    return true;
}

void JSMap::Clear(const JSThread *thread, const JSHandle<JSMap> &map)
{
    LinkedHashMap *linked_map = LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject());
    linked_map->Clear(thread);
}

bool JSMap::Has(JSTaggedValue key, int hash) const
{
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->Has(key, hash);
}

JSTaggedValue JSMap::Get(JSTaggedValue key, int hash) const
{
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->Get(key, hash);
}

int JSMap::GetSize() const
{
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->NumberOfElements();
}

JSTaggedValue JSMap::GetKey(int entry) const
{
    ASSERT_PRINT(entry >= 0 && entry < GetSize(), "entry must be non-negative integer less than capacity");
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->GetKey(entry);
}

JSTaggedValue JSMap::GetValue(int entry) const
{
    ASSERT_PRINT(entry >= 0 && entry < GetSize(), "entry must be non-negative integer less than capacity");
    return LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->GetValue(entry);
}
}  // namespace panda::ecmascript
