/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/runtime/object_operator.h"
#include "plugins/ecmascript/runtime/ecma_vm.h"
#include "plugins/ecmascript/runtime/accessor_data.h"
#include "plugins/ecmascript/runtime/global_dictionary-inl.h"
#include "plugins/ecmascript/runtime/global_env.h"
#include "plugins/ecmascript/runtime/ic/property_box.h"
#include "plugins/ecmascript/runtime/interpreter/fast_runtime_stub-inl.h"
#include "plugins/ecmascript/runtime/js_array.h"
#include "plugins/ecmascript/runtime/js_function.h"
#include "plugins/ecmascript/runtime/js_hclass-inl.h"
#include "plugins/ecmascript/runtime/js_object-inl.h"
#include "plugins/ecmascript/runtime/js_primitive_ref.h"
#include "plugins/ecmascript/runtime/mem/ecma_string.h"
#include "plugins/ecmascript/runtime/object_factory.h"
#include "plugins/ecmascript/runtime/tagged_dictionary.h"
#include "plugins/ecmascript/runtime/global_dictionary.h"

namespace panda::ecmascript {
void ObjectOperator::HandleKey(const JSHandle<JSTaggedValue> &key)
{
    if (key->IsInt()) {
        int32_t key_int = key->GetInt();
        if (key_int >= 0) {
            element_index_ = static_cast<uint32_t>(key_int);
            return;
        }
        key_ = JSHandle<JSTaggedValue>::Cast(base::NumberHelper::NumberToString(thread_, JSTaggedValue(key_int)));
        return;
    }

    if (key->IsString()) {
        uint32_t index = 0;
        if (JSTaggedValue::ToElementIndex(key.GetTaggedValue(), &index)) {
            ASSERT(index < JSObject::MAX_ELEMENT_INDEX);
            element_index_ = index;
            return;
        }
        if (EcmaString::Cast(key->GetTaggedObject())->IsInternString()) {
            key_ = key;
            return;
        }
        key_ = JSHandle<JSTaggedValue>(thread_, thread_->GetEcmaVM()->GetFactory()->InternString(key));
        return;
    }

    if (key->IsDouble()) {
        double number = key->GetDouble();
        if (number >= 0 && number < JSObject::MAX_ELEMENT_INDEX) {
            auto integer = static_cast<uint32_t>(number);
            if (integer == number) {
                element_index_ = static_cast<uint32_t>(number);
                return;
            }
        }
        key_ = JSHandle<JSTaggedValue>::Cast(base::NumberHelper::NumberToString(thread_, key.GetTaggedValue()));
        return;
    }

    if (key->IsSymbol()) {
        key_ = key;
        return;
    }

    JSHandle<JSTaggedValue> key_handle(thread_, JSTaggedValue::ToPrimitive(thread_, key, PREFER_STRING));
    if (key->IsSymbol()) {
        key_ = key_handle;
        return;
    }
    key_ = JSHandle<JSTaggedValue>(thread_,
                                   thread_->GetEcmaVM()->GetFactory()->InternString(
                                       JSHandle<JSTaggedValue>::Cast(JSTaggedValue::ToString(thread_, key_handle))));
}

void ObjectOperator::UpdateHolder()
{
    if (holder_->IsString() &&
        (IsElement() && element_index_ < EcmaString::Cast(holder_->GetTaggedObject())->GetLength())) {
        holder_.Update(JSPrimitiveRef::StringCreate(thread_, holder_).GetTaggedValue());
    } else {
        holder_.Update(JSTaggedValue::ToPrototypeOrObj(thread_, holder_).GetTaggedValue());
    }
}

void ObjectOperator::StartLookUp(OperatorType type)
{
    UpdateHolder();

    if (type == OperatorType::OWN) {
        LookupPropertyInHolder();
    } else {
        LookupProperty();
    }
}

void ObjectOperator::StartGlobalLookUp(OperatorType type)
{
    UpdateHolder();

    if (type == OperatorType::OWN) {
        GlobalLookupPropertyInHolder();
    } else {
        GlobalLookupProperty();
    }
}

ObjectOperator::ObjectOperator(JSThread *thread, const JSHandle<JSTaggedValue> &key, OperatorType type)
    : thread_(thread),
      holder_(thread, thread->GetEcmaVM()->GetGlobalEnv()->GetGlobalObject()),
      receiver_(thread, thread->GetEcmaVM()->GetGlobalEnv()->GetGlobalObject())
{
    HandleKey(key);
    StartGlobalLookUp(type);
}

ObjectOperator::ObjectOperator(JSThread *thread, const JSHandle<JSObject> &holder, const JSHandle<JSTaggedValue> &key,
                               OperatorType type)
    : thread_(thread), holder_(thread, holder.GetTaggedValue()), receiver_(thread, holder.GetTaggedValue())
{
    HandleKey(key);
    StartLookUp(type);
}

ObjectOperator::ObjectOperator(JSThread *thread, const JSHandle<JSTaggedValue> &holder,
                               const JSHandle<JSTaggedValue> &key, OperatorType type)
    : thread_(thread), holder_(thread, holder.GetTaggedValue()), receiver_(thread, holder.GetTaggedValue())
{
    HandleKey(key);
    StartLookUp(type);
}

ObjectOperator::ObjectOperator(JSThread *thread, const JSHandle<JSTaggedValue> &holder, uint32_t index,
                               OperatorType type)
    : thread_(thread),
      holder_(thread, holder.GetTaggedValue()),
      receiver_(thread, holder.GetTaggedValue()),
      element_index_(index)
{
    StartLookUp(type);
}

ObjectOperator::ObjectOperator(JSThread *thread, const JSHandle<JSTaggedValue> &holder,
                               const JSHandle<JSTaggedValue> &receiver, const JSHandle<JSTaggedValue> &key,
                               OperatorType type)
    : thread_(thread), holder_(thread, holder.GetTaggedValue()), receiver_(thread, receiver.GetTaggedValue())
{
    SetHasReceiver(true);
    HandleKey(key);
    StartLookUp(type);
}

// op for fast path
ObjectOperator::ObjectOperator(JSThread *thread, const JSTaggedValue &receiver, const JSTaggedValue &name,
                               OperatorType type)
    : thread_(thread), holder_(thread, receiver), receiver_(thread, receiver), key_(thread, name)
{
    ASSERT(name.IsStringOrSymbol());
    StartLookUp(type);
}
JSHandle<JSTaggedValue> ObjectOperator::FastGetValue()
{
    ASSERT(IsFound() && !value_.IsEmpty());
    if (value_->IsPropertyBox()) {
        value_.Update(PropertyBox::Cast(value_->GetTaggedObject())->GetValue());
    }
    if (!IsAccessorDescriptor()) {
        return value_;
    }
    AccessorData *accessor = AccessorData::Cast(value_->GetTaggedObject());
    ASSERT(!accessor->IsInternal());
    // 8. Return Call(getter, Receiver).
    return JSHandle<JSTaggedValue>(thread_, JSObject::CallGetter(thread_, accessor, receiver_));
}
ObjectOperator::ObjectOperator(JSThread *thread, const JSTaggedValue &receiver, const JSTaggedValue &name,
                               const PropertyAttributes &attr)
    : thread_(thread), receiver_(thread, receiver), key_(thread, name)
{
    SetAttr(attr);
}
void ObjectOperator::FastAdd(JSThread *thread, const JSTaggedValue &receiver, const JSTaggedValue &name,
                             const JSHandle<JSTaggedValue> &value, const PropertyAttributes &attr)
{
    ObjectOperator op(thread, receiver, name, attr);
    op.AddPropertyInternal(value);
}

void ObjectOperator::ToPropertyDescriptor(PropertyDescriptor &desc) const
{
    if (!IsFound()) {
        return;
    }

    if (!IsAccessorDescriptor()) {
        desc.SetWritable(IsWritable());
        JSTaggedValue val = GetValue();
        desc.SetValue(JSHandle<JSTaggedValue>(thread_, val));
    } else {
        JSTaggedValue value = GetValue();
        if (value.IsPropertyBox()) {
            value = PropertyBox::Cast(value.GetTaggedObject())->GetValue();
        }

        AccessorData *accessor = AccessorData::Cast(value.GetTaggedObject());

        if (UNLIKELY(accessor->IsInternal())) {
            desc.SetWritable(IsWritable());
            auto val = accessor->CallInternalGet(thread_, JSHandle<JSObject>::Cast(GetHolder()));
            desc.SetValue(JSHandle<JSTaggedValue>(thread_, val));
        } else {
            desc.SetGetter(JSHandle<JSTaggedValue>(thread_, accessor->GetGetter()));
            desc.SetSetter(JSHandle<JSTaggedValue>(thread_, accessor->GetSetter()));
        }
    }

    desc.SetEnumerable(IsEnumerable());
    desc.SetConfigurable(IsConfigurable());
}

void ObjectOperator::GlobalLookupProperty()
{
    GlobalLookupPropertyInHolder();
    if (IsFound()) {
        return;
    }
    JSTaggedValue proto = JSObject::Cast(holder_->GetTaggedObject())->GetPrototype(thread_);
    if (!proto.IsHeapObject()) {
        return;
    }
    holder_.Update(proto);
    if (holder_->IsJSProxy()) {
        return;
    }
    SetIsOnPrototype(true);
    LookupProperty();
}

void ObjectOperator::LookupProperty()
{
    while (true) {
        LookupPropertyInHolder();
        if (IsFound()) {
            return;
        }

        JSTaggedValue proto = holder_.GetObject<JSObject>()->GetPrototype(thread_);
        if (!proto.IsHeapObject()) {
            return;
        }

        holder_.Update(proto);
        if (holder_->IsJSProxy()) {
            return;
        }

        SetIsOnPrototype(true);
    }
}

void ObjectOperator::LookupGlobal(const JSHandle<JSObject> &obj)
{
    ASSERT(obj->IsJSGlobalObject());
    if (IsElement()) {
        LookupElementInlinedProps(obj);
        return;
    }
    TaggedArray *array = TaggedArray::Cast(obj->GetProperties().GetTaggedObject());
    if (array->GetLength() == 0) {
        return;
    }
    GlobalDictionary *dict = GlobalDictionary::Cast(array);
    int entry = dict->FindEntry(key_.GetTaggedValue());
    if (entry == -1) {
        return;
    }
    JSTaggedValue value(dict->GetBox(entry));
    uint32_t attr = dict->GetAttributes(entry).GetValue();
    SetFound(entry, value, attr, true);
}

void ObjectOperator::LookupPropertyInlinedProps(const JSHandle<JSObject> &obj)
{
    if (IsElement()) {
        LookupElementInlinedProps(obj);
        return;
    }

    if (obj->IsJSGlobalObject()) {
        TaggedArray *array = TaggedArray::Cast(obj->GetProperties().GetTaggedObject());
        if (array->GetLength() == 0) {
            return;
        }

        GlobalDictionary *dict = GlobalDictionary::Cast(array);
        int entry = dict->FindEntry(key_.GetTaggedValue());
        if (entry == -1) {
            return;
        }

        JSTaggedValue value(dict->GetBox(entry));
        PropertyAttributes attr = dict->GetAttributes(entry);
        SetFound(entry, value, attr.GetValue(), !attr.IsAccessor());
        return;
    }

    TaggedArray *array = TaggedArray::Cast(obj->GetProperties().GetTaggedObject());
    if (!array->IsDictionaryMode()) {
        JSHClass *jshclass = obj->GetJSHClass();
        JSTaggedValue attrs = jshclass->GetLayout();
        LayoutInfo *layout_info = LayoutInfo::Cast(attrs.GetTaggedObject());
        int props_number = jshclass->NumberOfProps();
        int entry = layout_info->FindElementWithCache(thread_, jshclass, key_.GetTaggedValue(), props_number);
        if (entry == -1) {
            return;
        }
        PropertyAttributes attr(layout_info->GetAttr(entry));
        ASSERT(entry == static_cast<int>(attr.GetOffset()));
        JSTaggedValue value;
        if (attr.IsInlinedProps()) {
            value = obj->GetPropertyInlinedProps(entry);
        } else {
            entry -= jshclass->GetInlinedProperties();
            value = array->Get(entry);
        }

        SetFound(entry, value, attr.GetValue(), true);
        return;
    }

    NameDictionary *dict = NameDictionary::Cast(array);
    int entry = dict->FindEntry(key_.GetTaggedValue());
    if (entry == -1) {
        return;
    }

    JSTaggedValue value = dict->GetValue(entry);
    uint32_t attr = dict->GetAttributes(entry).GetValue();
    SetFound(entry, value, attr, false);
}

void ObjectOperator::TransitionForAttributeChanged(const JSHandle<JSObject> &receiver, PropertyAttributes attr)
{
    if (IsElement()) {
        uint32_t index = GetIndex();
        if (!receiver->GetJSHClass()->IsDictionaryElement()) {
            JSObject::ElementsToDictionary(thread_, receiver);
            auto dict = NumberDictionary::Cast(receiver->GetElements().GetTaggedObject());
            index = dict->FindEntry(JSTaggedValue(index));
            PropertyAttributes origin = dict->GetAttributes(index);
            attr.SetDictionaryOrder(origin.GetDictionaryOrder());
            dict->SetAttributes(thread_, index, attr);
        } else {
            auto dict = NumberDictionary::Cast(receiver->GetElements().GetTaggedObject());
            dict->SetAttributes(thread_, index, attr);
        }
        // update found result
        UpdateFound(index, attr.GetValue(), false, true);
    } else if (receiver->IsJSGlobalObject()) {
        JSHandle<GlobalDictionary> dict_handle(thread_, receiver->GetProperties());
        GlobalDictionary::InvalidatePropertyBox(thread_, dict_handle, GetIndex(), attr);
    } else {
        uint32_t index = GetIndex();
        if (!receiver->GetJSHClass()->IsDictionaryMode()) {
            JSHandle<NameDictionary> dict(JSObject::TransitionToDictionary(thread_, receiver));
            index = dict->FindEntry(key_.GetTaggedValue());
            PropertyAttributes origin = dict->GetAttributes(index);
            attr.SetDictionaryOrder(origin.GetDictionaryOrder());
            dict->SetAttributes(thread_, index, attr);
        } else {
            auto dict = NameDictionary::Cast(receiver->GetProperties().GetTaggedObject());
            dict->SetAttributes(thread_, index, attr);
        }
        // update found result
        UpdateFound(index, attr.GetValue(), false, true);
    }
}

bool ObjectOperator::UpdateValueAndDetails(const JSHandle<JSObject> &receiver, const JSHandle<JSTaggedValue> &value,
                                           PropertyAttributes attr, bool attr_changed)
{
    bool is_internal_accessor = IsAccessorDescriptor() && AccessorData::Cast(GetValue().GetHeapObject())->IsInternal();
    if (attr_changed) {
        TransitionForAttributeChanged(receiver, attr);
    }
    return UpdateDataValue(receiver, value, is_internal_accessor);
}

bool ObjectOperator::UpdateDataValue(const JSHandle<JSObject> &receiver, const JSHandle<JSTaggedValue> &value,
                                     bool is_internal_accessor, bool may_throw)
{
    if (IsElement()) {
        TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetTaggedObject());
        if (!elements->IsDictionaryMode()) {
            elements->Set(thread_, GetIndex(), value.GetTaggedValue());
            return true;
        }

        NumberDictionary *dict = NumberDictionary::Cast(elements);
        dict->UpdateValue(thread_, GetIndex(), value.GetTaggedValue());
        return true;
    }

    if (receiver->IsJSGlobalObject()) {
        // need update cell type ?
        auto *dict = GlobalDictionary::Cast(receiver->GetProperties().GetTaggedObject());
        PropertyBox *cell = dict->GetBox(GetIndex());
        cell->SetValue(thread_, value.GetTaggedValue());
        return true;
    }

    if (is_internal_accessor) {
        auto accessor = AccessorData::Cast(GetValue().GetHeapObject());
        if (accessor->HasSetter()) {
            return accessor->CallInternalSet(thread_, JSHandle<JSObject>(receiver), value, may_throw);
        }
    }

    JSHandle<TaggedArray> properties(thread_, receiver->GetProperties().GetHeapObject());
    if (!properties->IsDictionaryMode()) {
        PropertyAttributes attr = GetAttr();
        if (attr.IsInlinedProps()) {
            receiver->SetPropertyInlinedProps(thread_, GetIndex(), value.GetTaggedValue());
        } else {
            properties->Set(thread_, GetIndex(), value.GetTaggedValue());
        }
    } else {
        properties.GetObject<NameDictionary>()->UpdateValue(thread_, GetIndex(), value.GetTaggedValue());
    }
    return true;
}

bool ObjectOperator::WriteDataProperty(const JSHandle<JSObject> &receiver, const PropertyDescriptor &desc)
{
    PropertyAttributes attr = GetAttr();
    bool attr_changed = false;

    // composed new attribute from desc
    if (desc.HasConfigurable() && attr.IsConfigurable() != desc.IsConfigurable()) {
        attr.SetConfigurable(desc.IsConfigurable());
        attr_changed = true;
    }
    if (desc.HasEnumerable() && attr.IsEnumerable() != desc.IsEnumerable()) {
        attr.SetEnumerable(desc.IsEnumerable());
        attr_changed = true;
    }

    if (IsAccessorDescriptor() && value_->IsPropertyBox()) {
        value_.Update(PropertyBox::Cast(value_->GetTaggedObject())->GetValue());
    }

    if (!desc.IsAccessorDescriptor()) {
        if (desc.HasWritable() && attr.IsWritable() != desc.IsWritable()) {
            attr.SetWritable(desc.IsWritable());
            attr_changed = true;
        }
        if (!desc.HasValue()) {
            if (attr_changed) {
                TransitionForAttributeChanged(receiver, attr);
            }
            return true;
        }

        if (IsAccessorDescriptor()) {
            auto accessor = AccessorData::Cast(GetValue().GetHeapObject());
            if (!accessor->IsInternal() || !accessor->HasSetter()) {
                attr.SetIsAccessor(false);
                attr_changed = true;
            }
        }

        return UpdateValueAndDetails(receiver, desc.GetValue(), attr, attr_changed);
    }

    if (IsAccessorDescriptor() && !IsElement()) {
        TaggedArray *properties = TaggedArray::Cast(receiver->GetProperties().GetTaggedObject());
        if (!properties->IsDictionaryMode()) {
            // as some accessorData is in global_env, we need to new accessorData.
            JSHandle<AccessorData> accessor = thread_->GetEcmaVM()->GetFactory()->NewAccessorData();

            if (desc.HasGetter()) {
                accessor->SetGetter(thread_, desc.GetGetter().GetTaggedValue());
            } else {
                accessor->SetGetter(thread_, JSHandle<AccessorData>::Cast(value_)->GetGetter());
            }
            if (desc.HasSetter()) {
                accessor->SetSetter(thread_, desc.GetSetter().GetTaggedValue());
            } else {
                accessor->SetSetter(thread_, JSHandle<AccessorData>::Cast(value_)->GetSetter());
            }

            JSHandle<NameDictionary> dict(JSObject::TransitionToDictionary(thread_, receiver));
            int entry = dict->FindEntry(key_.GetTaggedValue());
            ASSERT(entry != -1);
            attr.SetDictionaryOrder(dict->GetAttributes(entry).GetDictionaryOrder());
            dict->UpdateValueAndAttributes(thread_, entry, accessor.GetTaggedValue(), attr);
            return true;
        }
    }

    JSHandle<AccessorData> accessor = IsAccessorDescriptor() ? JSHandle<AccessorData>::Cast(value_)
                                                             : thread_->GetEcmaVM()->GetFactory()->NewAccessorData();
    if (desc.HasGetter()) {
        accessor->SetGetter(thread_, desc.GetGetter().GetTaggedValue());
    }

    if (desc.HasSetter()) {
        accessor->SetSetter(thread_, desc.GetSetter().GetTaggedValue());
    }

    if (!IsAccessorDescriptor()) {
        attr.SetIsAccessor(true);
        attr_changed = true;
    }

    JSHandle<JSTaggedValue> value = JSHandle<JSTaggedValue>::Cast(accessor);
    return UpdateValueAndDetails(receiver, value, attr, attr_changed);
}

void ObjectOperator::DeletePropertyInHolder()
{
    if (IsElement()) {
        return DeleteElementInHolder();
    }

    JSObject::DeletePropertyInternal(thread_, JSHandle<JSObject>(holder_), key_, GetIndex());
}

bool ObjectOperator::AddProperty(const JSHandle<JSObject> &receiver, const JSHandle<JSTaggedValue> &value,
                                 PropertyAttributes attr)
{
    if (IsElement()) {
        bool ret = JSObject::AddElementInternal(thread_, receiver, element_index_, value, attr);
        bool is_dict = receiver->GetJSHClass()->IsDictionaryElement();
        SetFound(element_index_, value.GetTaggedValue(), attr.GetValue(), !is_dict);
        return ret;
    }

    ResetState();
    receiver_.Update(receiver.GetTaggedValue());
    SetAttr(attr.GetValue());
    AddPropertyInternal(value);
    return true;
}

void ObjectOperator::WriteElement(const JSHandle<JSObject> &receiver, JSTaggedValue value) const
{
    ASSERT(IsElement() && GetIndex() < JSObject::MAX_ELEMENT_INDEX);

    TaggedArray *elements = TaggedArray::Cast(receiver->GetElements().GetTaggedObject());
    if (!elements->IsDictionaryMode()) {
        elements->Set(thread_, index_, value);
        receiver->GetJSHClass()->UpdateRepresentation(value);
        return;
    }

    NumberDictionary *dictionary = NumberDictionary::Cast(elements);
    dictionary->UpdateValue(thread_, GetIndex(), value);
}

void ObjectOperator::DeleteElementInHolder() const
{
    JSHandle<JSObject> obj(holder_);

    TaggedArray *elements = TaggedArray::Cast(obj->GetElements().GetTaggedObject());
    if (!elements->IsDictionaryMode()) {
        elements->Set(thread_, index_, JSTaggedValue::Hole());
        JSObject::ElementsToDictionary(thread_, JSHandle<JSObject>(holder_));
    } else {
        JSHandle<NumberDictionary> dict_handle(thread_, elements);
        JSHandle<NumberDictionary> new_dict = NumberDictionary::Remove(thread_, dict_handle, GetIndex());
        obj->SetElements(thread_, new_dict);
    }
}

void ObjectOperator::SetFound(uint32_t index, JSTaggedValue value, uint32_t attr, bool mode, bool transition)
{
    SetIndex(index);
    SetValue(value);
    SetFastMode(mode);
    SetIsTransition(transition);
    SetAttr(attr);
}

void ObjectOperator::UpdateFound(uint32_t index, uint32_t attr, bool mode, bool transition)
{
    SetIndex(index);
    SetFastMode(mode);
    SetIsTransition(transition);
    SetAttr(attr);
}

void ObjectOperator::ResetState()
{
    // index may used by element
    SetIndex(NOT_FOUND_INDEX);
    SetValue(JSTaggedValue::Undefined());
    SetFastMode(false);
    SetAttr(0);
    SetIsOnPrototype(false);
    SetHasReceiver(false);
}

void ObjectOperator::LookupElementInlinedProps(const JSHandle<JSObject> &obj)
{
    // if is js string, do special.
    if (obj->IsJSPrimitiveRef() && JSPrimitiveRef::Cast(obj.GetTaggedValue().GetTaggedObject())->IsString()) {
        PropertyDescriptor desc(thread_);
        bool status = JSPrimitiveRef::StringGetIndexProperty(thread_, obj, element_index_, &desc);
        if (status) {
            PropertyAttributes attr(desc);
            SetFound(element_index_, desc.GetValue().GetTaggedValue(), attr.GetValue(), true);
            return;
        }
    }
    {
        TaggedArray *elements = TaggedArray::Cast(obj->GetElements().GetTaggedObject());
        if (elements->GetLength() == 0) {
            return;  // Empty Array
        }

        if (!elements->IsDictionaryMode()) {
            if (elements->GetLength() <= element_index_) {
                return;
            }

            JSTaggedValue value = elements->Get(element_index_);
            if (value.IsHole()) {
                return;
            }
            SetFound(element_index_, value, PropertyAttributes::GetDefaultAttributes(), true);
        } else {
            NumberDictionary *dictionary = NumberDictionary::Cast(obj->GetElements().GetTaggedObject());
            JSTaggedValue key(static_cast<int>(element_index_));
            int entry = dictionary->FindEntry(key);
            if (entry == -1) {
                return;
            }

            uint32_t attr = dictionary->GetAttributes(entry).GetValue();
            SetFound(entry, dictionary->GetValue(entry), attr, false);
        }
    }
}

void ObjectOperator::AddPropertyInternal(const JSHandle<JSTaggedValue> &value)
{
    ObjectFactory *factory = thread_->GetEcmaVM()->GetFactory();
    JSHandle<JSObject> obj(GetReceiver());
    PropertyAttributes attr = GetAttr();
    if (obj->IsJSGlobalObject()) {
        JSMutableHandle<GlobalDictionary> dict(thread_, obj->GetProperties());
        if (dict->GetLength() == 0) {
            dict.Update(GlobalDictionary::Create(thread_));
        }

        // Add PropertyBox to global dictionary
        JSHandle<PropertyBox> cell_handle = factory->NewPropertyBox(key_);
        cell_handle->SetValue(thread_, value.GetTaggedValue());
        PropertyBoxType cell_type = value->IsUndefined() ? PropertyBoxType::UNDEFINED : PropertyBoxType::CONSTANT;
        attr.SetBoxType(cell_type);

        JSHandle<GlobalDictionary> properties =
            GlobalDictionary::PutIfAbsent(thread_, dict, key_, JSHandle<JSTaggedValue>(cell_handle), attr);
        obj->SetProperties(thread_, properties);
        // index and fastMode is not essential for global obj;
        SetFound(0, cell_handle.GetTaggedValue(), attr.GetValue(), true);
        return;
    }

    attr = FastRuntimeStub::AddPropertyByName(thread_, obj, key_, value, attr);
    if (obj->GetJSHClass()->IsDictionaryMode()) {
        SetFound(0, value.GetTaggedValue(), attr.GetValue(), false);
    } else {
        uint32_t index =
            attr.IsInlinedProps() ? attr.GetOffset() : attr.GetOffset() - obj->GetJSHClass()->GetInlinedProperties();
        SetFound(index, value.GetTaggedValue(), attr.GetValue(), true, true);
    }
}

void ObjectOperator::DefineSetter(const JSHandle<JSTaggedValue> &value)
{
    ASSERT(IsAccessorDescriptor());
    JSHandle<AccessorData> accessor = JSHandle<AccessorData>::Cast(value_);
    accessor->SetSetter(thread_, value.GetTaggedValue());
    UpdateDataValue(JSHandle<JSObject>::Cast(receiver_), JSHandle<JSTaggedValue>::Cast(accessor), false);
}

void ObjectOperator::DefineGetter(const JSHandle<JSTaggedValue> &value)
{
    ASSERT(IsAccessorDescriptor());
    JSHandle<AccessorData> accessor = JSHandle<AccessorData>::Cast(value_);
    accessor->SetGetter(thread_, value.GetTaggedValue());
    UpdateDataValue(JSHandle<JSObject>::Cast(receiver_), JSHandle<JSTaggedValue>::Cast(accessor), false);
}
}  // namespace panda::ecmascript
